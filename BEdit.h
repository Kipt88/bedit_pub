// BEdit.h - Public domain - no warranty //

#ifndef BEdit_INCLUDED
#define BEdit_INCLUDED

#define BEdit_VersionString "0.2.3-wip"

#ifndef BEAssert
#  include <assert.h>
#  define BEAssert(cnd) assert(cnd)
#endif

#ifndef BEAlloc
// NOTE(Jens): At this time memory is never freed, keep track of the allocations if you want them freed afterwards.
#  include <stdlib.h>
#  define BEAlloc(allocator, type) ((void)allocator, (type*)malloc(sizeof(type)))
#  define BEAllocArray(allocator, type, count) ((void)allocator, (type*)malloc(sizeof(type) * count))
#  define BEAllocatorType void*
#endif

#ifndef BEPublicAPI
#  define BEPublicAPI
#endif

#ifndef BECustomScalarTypes

#include <stdint.h>

typedef uint8_t BEu8;
typedef uint16_t BEu16;
typedef uint32_t BEu32;
typedef uint64_t BEu64;

typedef int8_t BEs8;
typedef int16_t BEs16;
typedef int32_t BEs32;
typedef int64_t BEs64;

typedef float BEf32;
typedef double BEf64;

typedef BEs32 BEbool;

#endif


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// NOTE(Jens): Compilation...

// NOTE(Jens): Minimalistic example (most error checking omitted):
//
// #include "BEdit.h"
// #include <stdio.h>
// 
// BELoadFileResult loadFile(void* userdata, BEAllocatorType allocator, BEu32 pathLen, const char* nullTerminatedPath)
// {
//     FILE* file = fopen(nullTerminatedPath, "rb");
//     if (file)
//     {
//         fseek(file, 0, SEEK_END);
//         long size = ftell(file);
//         fseek(file, 0, SEEK_SET);
//
//         void* data = BEAllocArray(allocator, BEu8, size);
//         fread(data, size, 1, file);
//         fclose(file);
//
//         return { .fileSizeInBytes = size, .fileContent = data };
//     }
//     return { .error = 1 };
// }
//
// int main(int argc, char* argv[])
// {
//     // NOTE(Jens): Reads a layout file and writes the instructions to one file and the data section to another.
//
//     BECompilationContext compilation;
//     BEFileLoadError err = BEStartCompilation(&compilation, 0, { 0, loadFile }, 0, argv[1]);
//     if (!err)
//     {
//         FILE* out = fopen(argv[2], "wb");
//
//         BEu8 buf[256];
//         BECompilationEvent event;
//         
//         do
//         {
//             BEu32 size = sizeof(buf);
//             event = BEGenerateInstructions(&compilation, &size, buf);
//             fwrite(buf, size, 1, out);
//         }
//         while (event.type != BECompilationEventType_Complete);
//         
//         BEDataSection dataSection = BEFinalizeCompilation(&compilation, 0);
//         
//         FILE* outDataSection = fopen(argv[3], "wb");
//         fwrite(dataSection.data, dataSection.size, 1, outDataSection);
//     }
// }
//

enum BECompilationEventType
{
    BECompilationEventType_Continue,
    BECompilationEventType_Complete,
    
    BECompilationEventType_FirstError,
    BECompilationEventType_Error_FileLoadFailed,
    BECompilationEventType_Error_InvalidToken,
    
    BECompilationEventType_Error_InvalidStatement,
    BECompilationEventType_Error_InvalidStatement_ExpectedSemicolon,
    BECompilationEventType_Error_InvalidStatement_ExpectedComma,
    BECompilationEventType_Error_InvalidStatement_ExpectedDot,
    BECompilationEventType_Error_InvalidStatement_ExpectedEquals,
    BECompilationEventType_Error_InvalidStatement_ExpectedOpenSquareBracket,
    BECompilationEventType_Error_InvalidStatement_ExpectedCloseSquareBracket,
    BECompilationEventType_Error_InvalidStatement_ExpectedOpenParenthesis,
    BECompilationEventType_Error_InvalidStatement_ExpectedCloseParenthesis,
    BECompilationEventType_Error_InvalidStatement_ExpectedOpenCurlyBracket,
    BECompilationEventType_Error_InvalidStatement_ExpectedCloseCurlyBracket,
    BECompilationEventType_Error_InvalidStatement_IntegerExpression,
    BECompilationEventType_Error_InvalidStatement_DuplicateEnumMemberNames,
    BECompilationEventType_Error_InvalidStatement_DuplicateTypedefs,
    BECompilationEventType_Error_InvalidStatement_ExpectedVariable,
    BECompilationEventType_Error_InvalidStatement_ExpectedAssignmentOperator,
    BECompilationEventType_Error_InvalidStatement_ExpectedCommaOrCloseParenthesis,
    BECompilationEventType_Error_InvalidStatement_ExpectedCommaOrCloseCurlyBracket,
    BECompilationEventType_Error_InvalidStatement_ExpectedIdentifier,
    BECompilationEventType_Error_InvalidStatement_ExpectedQuotedString,
    BECompilationEventType_Error_InvalidStatement_ExpectedStructName,
    BECompilationEventType_Error_InvalidStatement_ExpectedIdentifierOrOpenParenthesis,
    BECompilationEventType_Error_InvalidStatement_ExpectedKeyword_while,
    BECompilationEventType_Error_InvalidStatement_ExpectedKeyword_default,
    BECompilationEventType_Error_InvalidStatement_ExpectedKeyword_byteorder,
    BECompilationEventType_Error_InvalidStatement_ExpectedKeyword_var,
    BECompilationEventType_Error_InvalidStatement_ExpectedKeyword_little_or_big,
    BECompilationEventType_Error_InvalidStatement_ExpectedKeyword_var_or_semicolon,
    BECompilationEventType_Error_InvalidStatement_ExpectedKeyword_s_or_u,
    BECompilationEventType_Error_InvalidStatement_ExpectedStatementStart,
    BECompilationEventType_Error_InvalidStatement_ExpectedType,
    BECompilationEventType_Error_InvalidStatement_ExpectedDisplayKeywordOrByteOrder,
    BECompilationEventType_Error_InvalidStatement_InvalidEnumSize,
    BECompilationEventType_Error_InvalidStatement_DuplicateParameterName,
    BECompilationEventType_Error_InvalidStatement_DuplicateStructName,
    BECompilationEventType_Error_InvalidStatement_ConstantLookupFailure,
    BECompilationEventType_Error_InvalidStatement_ExpectedCategoryName,
    BECompilationEventType_Error_InvalidStatement_ExpectedMemberName,
    BECompilationEventType_Error_InvalidStatement_DuplicateDisplayKeyword,
    BECompilationEventType_Error_InvalidStatement_DuplicateByteOrder,
    BECompilationEventType_Error_InvalidStatement_TooManyArguments,
    BECompilationEventType_Error_InvalidStatement_NotEnoughArguments,
    BECompilationEventType_Error_InvalidStatement_ConstantExpression_DivZero,
    BECompilationEventType_Error_InvalidStatement_ConstantExpression_ModZero,
    BECompilationEventType_Error_InvalidStatement_ExpectedOpenParenthesisOrAssignmentOperator,
    BECompilationEventType_Error_InvalidStatement_ExpectedArrayMember,
    BECompilationEventType_Error_InvalidStatement_DuplicateVariableName,
};
typedef enum BECompilationEventType BECompilationEventType;

typedef BEs64 BEFileLoadError;

struct BECompilationEvent
{
    BECompilationEventType type;
    
    BEu32 sourceFileIndex;
    BEu32 sourceLineIndex;
    BEu32 sourceColIndex;
    BEu32 sourceByteIndex;
    
    union
    {
        BEFileLoadError fileLoadError; // NOTE(Jens): Valid if type == BECompilationEventType_Error_FileLoadFailed
    };
};
typedef struct BECompilationEvent BECompilationEvent;

struct BELoadFileResult
{
    BEFileLoadError error; // IMPORTANT(Jens): Set to zero if no error.
    BEu64 lastFileWrite;   // NOTE(Jens): Used by BEdit to determine if file has been modified since compilation. Set to zero if you don't care.
    BEu32 fileSizeInBytes;
    void* fileContent;
};
typedef struct BELoadFileResult BELoadFileResult;

typedef BELoadFileResult (*BEFileLoaderFunc)(void* context, BEAllocatorType allocator, BEu32 pathLen, const char* nullTerminatedFilename);

struct BEFileLoader
{
    void* context;
    BEFileLoaderFunc loadFile;
};
typedef struct BEFileLoader BEFileLoader;

struct BEDataSection
{
    BEu32 size;
    BEu8* data;
};
typedef struct BEDataSection BEDataSection;

typedef struct BECompilationContext BECompilationContext;
typedef struct BEInstruction BEInstruction;

BEPublicAPI char* BEGetCompilationErrorDescription(BECompilationEventType event);

BEPublicAPI BEFileLoadError BEStartCompilation(BECompilationContext* out, BEAllocatorType allocator, BEFileLoader fileLoader, BEu32 pathLen, const char* nullTerminatedFilename);

BEPublicAPI BECompilationEvent BEGenerateInstructions(BECompilationContext* context, BEu32* capacityIn_sizeOut, BEu8* encodedInstructionsOut);

BEPublicAPI BEu32 BEGetFileCount(BECompilationContext* context);
BEPublicAPI char* BEGetFileContent(BECompilationContext* context, BEu32 fileIndex, BEu32* outLength);
BEPublicAPI char* BEGetFilename(BECompilationContext* context, BEu32 fileIndex);

BEPublicAPI BEDataSection BEFinalizeCompilation(BECompilationContext* context, BEAllocatorType outputAllocator);

BEPublicAPI BEInstruction BEDecodeInstruction(BEu32* outSize, BEu8* bytesStart);


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// NOTE(Jens): Runtime...

typedef enum BE_RuntimeEventType BE_RuntimeEventType;
enum BE_RuntimeEventType
{
    BE_RuntimeEventType_Generic,
    BE_RuntimeEventType_Breakpoint,
    BE_RuntimeEventType_Print,
    BE_RuntimeEventType_ScalarExport,
    BE_RuntimeEventType_StartStruct,
    BE_RuntimeEventType_EndStruct,
    BE_RuntimeEventType_PlotX,
    BE_RuntimeEventType_PlotY,
    
    BE_RuntimeEventType_Error_AssertTriggered,
    BE_RuntimeEventType_Error_DivByZero,
    BE_RuntimeEventType_Error_ModByZero,
    BE_RuntimeEventType_Error_FetchMemoryInvalidMemoryRange,
    BE_RuntimeEventType_Error_FetchMemoryTooBig,
    BE_RuntimeEventType_Error_CategoryTypeNotFound,
    BE_RuntimeEventType_Error_MemberInvalidMemoryRange,
    BE_RuntimeEventType_Error_FetchMemberSizeIsZero,
    BE_RuntimeEventType_Error_FetchMemberTooBig,
    BE_RuntimeEventType_Error_FetchMemberBadArrayIndex,
    BE_RuntimeEventType_Error_FetchMemberBadFloatSize,
    BE_RuntimeEventType_Error_FetchMemberNoSuchMember,
    BE_RuntimeEventType_Error_MemberInvalidArrayCount,
    
    BE_RuntimeEventType_End,
    BE_RuntimeEventType_Count,
    
    BE_RuntimeEventType_Error_First = BE_RuntimeEventType_Error_AssertTriggered,
    BE_RuntimeEventType_Error_Last = BE_RuntimeEventType_Error_MemberInvalidArrayCount,
};

typedef enum BE_Stack BE_Stack;
enum BE_Stack
{
	BE_Stack_Invalid,
	BE_Stack_Global,
	BE_Stack_Local,
    BE_Stack_Parent,
};

typedef struct BE_StackEntry BE_StackEntry;
struct BE_StackEntry
{
	BE_Stack stack;
	BEu32 index;
};

enum
{
    BEFetchMemoryFlag_BigEndian = (1 << 0),
    BEFetchMemoryFlag_Signed    = (1 << 1),
};
typedef BEu32 BEFetchMemoryFlags;

struct BEFetchMemberLocation
{
    BEu32 memberIndex;
    BE_StackEntry arrayIndex;
};
typedef struct BEFetchMemberLocation BEFetchMemberLocation;

typedef enum BE_InstructionType BE_InstructionType;
enum BE_InstructionType
{
	BE_InstructionType_None,
	BE_InstructionType_Set,
	
	// NOTE(Jens): Unary
	BE_InstructionType_Negate,
	BE_InstructionType_LogicalNot,
	BE_InstructionType_BitwiseNot,
	BE_InstructionType_Abs,
	BE_InstructionType_Copy,
	
	// NOTE(Jens): Binary
	BE_InstructionType_Add,
	BE_InstructionType_Sub,
	BE_InstructionType_Mul,
	BE_InstructionType_Div,
	BE_InstructionType_Mod,
	BE_InstructionType_Less,
	BE_InstructionType_LessOrEqual,
	BE_InstructionType_Equal,
	BE_InstructionType_Greater,
	BE_InstructionType_GreaterOrEqual,
	BE_InstructionType_NotEqual,
	BE_InstructionType_BitshiftLeft,
	BE_InstructionType_BitshiftRight,
	BE_InstructionType_BitwiseAnd,
	BE_InstructionType_BitwiseOr,
	BE_InstructionType_BitwiseXOr,
	BE_InstructionType_LogicalAnd,
	BE_InstructionType_LogicalOr,
	
	// NOTE(Jens): Float unary
	BE_InstructionType_FNegate,
	BE_InstructionType_FAbs,
	
    // NOTE(Jens): Float binary
    BE_InstructionType_FAdd,
	BE_InstructionType_FSub,
	BE_InstructionType_FMul,
	BE_InstructionType_FDiv,
	BE_InstructionType_FLess,
	BE_InstructionType_FLessOrEqual,
	BE_InstructionType_FEqual,
	BE_InstructionType_FGreater,
	BE_InstructionType_FGreaterOrEqual,
	BE_InstructionType_FNotEqual,
    
	BE_InstructionType_PushFrame,
	BE_InstructionType_PopFrame,
	
	BE_InstructionType_PushStruct,
    BE_InstructionType_PushStructInternal,
	BE_InstructionType_PopStruct,
    
    BE_InstructionType_StructIndex,
	
    BE_InstructionType_ExportScalar_u,
	BE_InstructionType_ExportScalar_s,
	BE_InstructionType_ExportScalar_f,
	BE_InstructionType_ExportScalar_string,
    BE_InstructionType_ExportScalar_raw,
    
    BE_InstructionType_FetchArrayCount,
	BE_InstructionType_FetchMemberValue,
    BE_InstructionType_FetchMemberValueFloat,
	BE_InstructionType_FetchMemory,
    
	BE_InstructionType_Jump,
    BE_InstructionType_JumpDynamic,
    BE_InstructionType_JumpConditional,
    BE_InstructionType_JumpDynamicConditional,
    
    BE_InstructionType_FetchSizeOfFile,
    
    BE_InstructionType_IntToFloat,
    
    // NOTE(Jens): Hiding related
    BE_InstructionType_HideMembers,
    BE_InstructionType_ShowMembers,
    
    // NOTE(Jens): Category related
    BE_InstructionType_FetchStartOfType,
    BE_InstructionType_HasType,
    
	// NOTE(Jens): Debug helpers
	BE_InstructionType_Assert,
	BE_InstructionType_Print,
	BE_InstructionType_Breakpoint,
	
    // NOTE(Jens): Plot related
    BE_InstructionType_CreatePlot,
    BE_InstructionType_Plot_F8_X,
    BE_InstructionType_Plot_F8_Y,
    
	BE_InstructionType_Count,
	
	BE_InstructionType_FirstUnary = BE_InstructionType_Negate,
	BE_InstructionType_LastUnary = BE_InstructionType_Copy,
	
	BE_InstructionType_FirstBinary = BE_InstructionType_Add,
	BE_InstructionType_LastBinary = BE_InstructionType_LogicalOr,
	
};

typedef struct BEInstruction BEInstruction;
struct BEInstruction
{
	BE_InstructionType type;
	BE_StackEntry result;
	
	union
	{
		BE_StackEntry unaryValue;
		
		struct
		{
			BE_StackEntry left;
			BE_StackEntry right;
		} binary;
		
		struct
		{
			BEs64 value;
		} set;
		
		struct
		{
			BEu32 pathCount;
			BEFetchMemberLocation path[16];
		} fetchMember;
		
		struct
		{
			BEu32 pathCount;
			BEFetchMemberLocation path[16];
		} fetchArrayCount;
		
        struct
        {
            BE_StackEntry size;
            BEFetchMemoryFlags flags;
        } fetchMemory;
        
        struct
        {
            BEu32 categoryIndex;
            BE_StackEntry typeNumber;
        } categoryQuery;
        
		struct
		{
			BEu32 memberIndex;
            BE_StackEntry arrayCount;
		} beginStruct;
        
		struct
		{
			BEu32 sizeRequiredInBytes;
		} pushFrame;
		
		struct
		{
            BE_StackEntry dynamicNewInstructionIndex; // NOTE(Jens): If BE_StackId_Invalid, use static `newInstructionIndex`.
			BEu32 newInstructionIndex;
            
			BE_StackEntry condition; // NOTE(Jens): Stack is BE_StackId_Invalid if unconditional. If `true` or invalid does the jump.
		} jump;
		
		struct
		{
			BE_StackEntry condition;
		} assert;
		
		struct
		{
            BEu64 prefixCharacterCount;
            char* prefixCharacters;
			
			BE_StackEntry value;
		} print;
		
		struct
		{
            BEs32 displayType; // NOTE(Jens): If negative indicates enum display with 'enumTypeIndex = 1 - displayType'.
            BEu32 isBigEndian;
            BEu32 memberIndex;
			BE_StackEntry size;
			BE_StackEntry arrayCount; // NOTE(Jens): Invalid if not an array.
		} scalarExport;
		
        struct
        {
            BEu32 index;
        } structIndex;
        
        struct
        {
            BE_StackEntry source;
        } conversion;
        
        struct 
        {
            BE_StackEntry plotId;
            BE_StackEntry value_f64;
        } plot;
	};
};

typedef enum
{
    BE_GlobalSymbol_MemberOffset,
    BE_GlobalSymbol_Reserved1,
    BE_GlobalSymbol_Reserved2,
    
    BE_GlobalSymbol_Count
} BE_GlobalSymbol;

typedef enum
{
    BE_LocalSymbol_ReturnAddress,
    BE_LocalSymbol_ArrayIndex,
    BE_LocalSymbol_Reserved1,
    
    BE_LocalSymbol_Count
} BE_LocalSymbol;

enum
{
    BE_ReservedSymbolCount = BE_GlobalSymbol_Count
};

typedef enum BE_ScalarBaseType BE_ScalarBaseType;
enum BE_ScalarBaseType
{
    BE_ScalarBaseType_u,
    BE_ScalarBaseType_s,
    BE_ScalarBaseType_f,
    BE_ScalarBaseType_string,
    BE_ScalarBaseType_raw,
    BE_ScalarBaseType_hidden,
    
    BE_ScalarBaseType_Count,
};

typedef enum BE_ScalarDisplayType BE_ScalarDisplayType;
enum BE_ScalarDisplayType
{
    BE_ScalarDisplayType_decimal,
    BE_ScalarDisplayType_binary,
    BE_ScalarDisplayType_octal,
    BE_ScalarDisplayType_hex,
};

#define BEInvalidEnumIndex ((BEu32)-1)

typedef struct BE_RuntimeEvent_ScalarExport BE_RuntimeEvent_ScalarExport;
struct BE_RuntimeEvent_ScalarExport
{
    BEu32 enumIndex;
    BE_ScalarBaseType baseType;
    BE_ScalarDisplayType displayType;
    
    BEu64 appearIndex;
    BEu32 structIndex;
    BEu32 memberIndex;
    BEbool isBigEndian;
    BEu64 size;
    BEu64 address;
    BEu64 arrayCount;
};

#define BEInvalidParentStructIndex ((BEu32)-1)

typedef struct BE_RuntimeEvent_StartStruct BE_RuntimeEvent_StartStruct;
struct BE_RuntimeEvent_StartStruct
{
    BEu64 appearIndex;
    BEu32 arrayIndex;
    BEu32 structIndex;
    BEu32 parentStructIndex; // IMPORTANT(Jens): Will be BEInvalidParentStruct if this struct is root (i.e. the type being 'layout'-ed).
    BEu32 memberIndex;
    BEu32 arrayCount;
    BEbool isInternal;
};

typedef struct BE_RuntimeEvent_Print BE_RuntimeEvent_Print;
struct BE_RuntimeEvent_Print
{
    BEu32 msgSize;
    BEbool hasValue;
    
    char* msg;
    BEs64 value;
};

typedef struct BE_RuntimeEvent_PlotValue BE_RuntimeEvent_PlotValue;
struct BE_RuntimeEvent_PlotValue
{
    BEs64 plotId;
    BEf64 value;
};

struct BERuntimeEvent
{
    BE_RuntimeEventType type;
    BEInstruction instruction;
    
    // NOTE(Jens): Only set if sources have been included during compilation (which is default) and type is not BE_RuntimeEventType_Generic.
    BEu32 sourceFileIndex;
    BEu32 sourceLineIndex;
    BEu32 sourceColIndex;
    BEu32 sourceByteIndex;
    
    union
    {
        BE_RuntimeEvent_ScalarExport scalarExport;
        BE_RuntimeEvent_StartStruct startStruct;
        BE_RuntimeEvent_Print print;
        BE_RuntimeEvent_PlotValue plotValue;
    };
};

typedef struct BESourceLocation BESourceLocation;
struct BESourceLocation
{
    BEu32 fileIndex;
    BEu32 sizeInBytes;
    BEu32 byteIndex;
    BEu32 lineIndex;
    BEu32 bytesSinceStartOfLine;
};

typedef struct BERuntimeEvent BERuntimeEvent;
typedef struct BERuntime BERuntime;

BEPublicAPI char* BEGetRuntimeErrorDescription(BE_RuntimeEventType event);

BEPublicAPI BERuntime BEStartRuntime(BEAllocatorType allocator, BEu32 stackCapacity, BEu8* stack, BEu8* dataSection, BEu32 instructionsSizeInBytes, BEu8* encodedInstructions, BEs64 sizeOfFile, BEu8* binaryFile);
BEPublicAPI BERuntimeEvent BEStepOne(BERuntime* runtime);
BEPublicAPI BEbool BERuntimeHasCompleted(BERuntime* runtime);

BEPublicAPI char* BEGetStructName(BERuntime* runtime, BEu32* size, BEu32 structIndex);
BEPublicAPI char* BEGetStructMemberName(BERuntime* runtime, BEu32* size, BEu32 structIndex, BEu32 memberIndex);

BEPublicAPI char* BEGetEnumName(BERuntime* runtime, BEu32* size, BEu32 enumIndex);
BEPublicAPI char* BEGetEnumMemberName(BERuntime* runtime, BEu32* size, BEu32 enumIndex, BEu32 memberIndex);
BEPublicAPI BEu32 BEGetEnumMemberCount(BERuntime* runtime, BEu32 enumIndex);
BEPublicAPI BEs64* BEGetEnumMemberValues(BERuntime* runtime, BEu32 enumIndex);

BEPublicAPI BESourceLocation BEGetSourceLocationForInstructionAddress(BERuntime* runtime, BEu32 address);
BEPublicAPI char* BEGetSourceFilenameForFileIndex(BERuntime* runtime, BEu32* size, BEu32 fileIndex);

BEPublicAPI BEs64* BEGetPtr(BERuntime* runtime, BE_StackEntry entry);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// NOTE(Jens): Private API that's needed for header starts here. Expect content below to be undocumented and change without prior notice. //

// NOTE(Jens): Generic...

typedef struct { char str[4]; } BE_punct;
typedef struct BE_String BE_String;

typedef struct BE_NameMap BE_NameMap;
typedef struct BE_NameMapNode BE_NameMapNode;

typedef struct BE_StackU32 BE_StackU32;

// NOTE(Jens): Tokenizing and lexing...

typedef struct BE_GlobalConstants BE_GlobalConstants;
typedef struct BE_GlobalConstantNode BE_GlobalConstantNode;

typedef struct BE_ParsedStructNode BE_ParsedStructNode;
typedef struct BE_ParsedStructs BE_ParsedStructs;
typedef struct BE_ParsedCategoryNode BE_ParsedCategoryNode;
typedef struct BE_ParsedTypedefs BE_ParsedTypedefs;
typedef struct BE_ParsedTypedefNode BE_ParsedTypedefNode;
typedef struct BE_ParsedEnums BE_ParsedEnums;
typedef struct BE_ParsedEnumNode BE_ParsedEnumNode;
typedef struct BE_ParsedMemberNode BE_ParsedMemberNode;

typedef struct BE_ParserScopes BE_ParserScopes;
typedef struct BE_ParserScope BE_ParserScope;

typedef struct BE_MemberPath BE_MemberPath;
typedef struct BE_MemberPathNode BE_MemberPathNode;

typedef struct BE_Imports BE_Imports;
typedef struct BE_ImportStack BE_ImportStack;

typedef struct BE_Expression BE_Expression;
typedef struct BE_ExpressionTerm BE_ExpressionTerm;
typedef struct BE_Expressions BE_Expressions;
typedef struct BE_BinaryOperator BE_BinaryOperator;
typedef struct BE_UnaryOperator BE_UnaryOperator;
typedef struct BE_ParsingMemberAccess BE_ParsingMemberAccess;
typedef struct BE_ParsingExpression BE_ParsingExpression;

typedef struct BE_TypeDeclaration BE_TypeDeclaration;
typedef struct BE_EnumMemberList BE_EnumMemberList;

typedef enum BE_ScalarBaseType BE_ScalarBaseType;
typedef enum BE_ScalarByteOrder BE_ScalarByteOrder;
typedef enum BE_ScalarDisplayType BE_ScalarDisplayType;
typedef enum BE_TypeVariant BE_TypeVariant;

typedef union BE_Statement BE_Statement;
typedef struct BE_PrintStatement BE_PrintStatement;
typedef struct BE_AssertStatement BE_AssertStatement;
typedef struct BE_VariableDeclaration BE_VariableDeclaration;
typedef struct BE_MemberDeclaration BE_MemberDeclaration;
typedef struct BE_TypedefDeclaration BE_TypedefDeclaration;
typedef struct BE_LayoutStatement BE_LayoutStatement;
typedef struct BE_EnumDeclaration BE_EnumDeclaration;
typedef struct BE_ImportStatement BE_ImportStatement;
typedef struct BE_DefaultDirective BE_DefaultDirective;
typedef struct BE_StructHeader BE_StructHeader;
typedef struct BE_Assignment BE_Assignment;
typedef struct BE_If BE_If;
typedef struct BE_ForLoop BE_ForLoop;
typedef struct BE_WhileLoop BE_WhileLoop;
typedef struct BE_EndOfDoWhileLoop BE_EndOfDoWhileLoop;
typedef struct BE_PlotStatement BE_PlotStatement;

typedef struct BE_AddressSpecification BE_AddressSpecification;
typedef struct BE_ParsingExpressions BE_ParsingExpressions;
typedef struct BE_ParsingMemberAccesses BE_ParsingMemberAccesses;

typedef struct BE_ParsingLoadExpression BE_ParsingLoadExpression;
typedef struct BE_ParsingLoadExpressions BE_ParsingLoadExpressions;


// NOTE(Jens): Instruction generation...

typedef struct BE_InstructionListNode BE_InstructionListNode;
typedef struct BE_InstructionRefStack BE_InstructionRefStack;
typedef struct BE_InstructionIfChain BE_InstructionIfChain;
typedef struct BE_InstructionRefs BE_InstructionRefs;

typedef struct BE_InstructionGenerator BE_InstructionGenerator;
typedef struct BE_InstructionListNode BE_InstructionListNode;
typedef struct BE_InstructionList BE_InstructionList;

typedef struct BE_StructStartAddresses BE_StructStartAddresses;
typedef struct BE_StructStartAddressNode BE_StructStartAddressNode;

typedef struct BE_PendingJump BE_PendingJump;
typedef struct BE_InstructionEncoder BE_InstructionEncoder;

typedef struct BE_EncodedInstructionChunk BE_EncodedInstructionChunk;
typedef struct BE_EncodedInstructions BE_EncodedInstructions;
typedef struct BE_InstructionLocationChunk BE_InstructionLocationChunk;
typedef struct BE_InstructionLocations BE_InstructionLocations;

typedef struct BE_FileParsing BE_FileParsing;
typedef struct BE_FileParsings BE_FileParsings;


// NOTE(Jens): Runtime...

//

typedef struct BE_MemberNodeRoot BE_MemberNodeRoot;
typedef struct BE_MemberNodeStructArray BE_MemberNodeStructArray;
typedef struct BE_MemberNodeStruct BE_MemberNodeStruct;
typedef struct BE_MemberNodeScalar BE_MemberNodeScalar;
typedef struct BE_MemberNode BE_MemberNode;


#if 0

struct Foo
{
    u(4) bar;
};

struct Bar
{
    Foo foo[2];
    u(4) kek[13];
};

#endif

/*

 NOTE(Jens): Overview of runtime member tree.
 
- *...* indicates value of 'runtime->currentMembers'.
- 'Type'    is BE_MemberNodeRoot.
- 'Type[]'  is BE_MemberNodeStructArray.
- 'Type[n]' is BE_MemberNodeStruct.
- '?' indicates an allocated, but not fully initialized node.

Struct index (structIndex = 'Bar', memberCount = 2)
=>

.             *Bar*
.           /       \
.          ?         ?

Start struct member (memberIndex = 0, arrayCount = 2)
=>
.              Bar
.           /       \
.        *?[]*       ?
.       /     \
.     ?[0]  ?[1]

Struct index (structIndex = 'Foo', memberCount = 1, arrayIndex = 0)
=>
.              Bar
.           /       \
.         Foo[]      ?
.       /     \
.    *Foo[0]* Foo[1]
.      |       |
.      ?       ?

Export scalar 'bar' (memberIndex = 0, arrayCount = 0)
=>
.              Bar
.           /       \
.         Foo[]      ?
.       /     \
.   *Foo[0]* Foo[1]
.      |       |
.     bar      ?

Struct index (structIndex = 'Foo', memberCount = 1, arrayIndex = 1)
=>
.              Bar
.           /       \
.         Foo[]      ?
.       /     \
.    Foo[0] *Foo[1]*
.      |       |
.     bar      ?

Export scalar 'bar' (memberIndex = 0, arrayCount = 0)
=>
.              Bar
.           /       \
.         Foo[]      ?
.       /     \
.    Foo[0] *Foo[1]*
.      |       |
.     bar     bar

End struct 'Foo'
=>
.             *Bar*
.           /       \
.         Foo[]      ?
.       /     \
.    Foo[0]  Foo[1]
.      |       |
.     bar     bar

Export scalar 'kek' (memberIndex = 1, arrayCount = 13)
=>
.             *Bar*
.           /       \
.         Foo[]    kek[13]
.       /     \
.    Foo[0]  Foo[1]
.      |       |
.     bar     bar

*/

struct BE_MemberNodeRoot
{
    BEu32 structIndex;
    BEu32 memberCount;
    BEu32 memberCapacity;
    BE_MemberNode* members;
};

struct BE_MemberNodeStructArray
{
    BEu32 structIndex;
    BEu32 memberCount;
    BEu32 memberCapacity;
    BEu64 arrayCount;
    BEu64 arrayCapacity;
    BEbool isInternal;
    
    BE_MemberNode* array; // NOTE(Jens): count = Max(1, arrayCount).
};

struct BE_MemberNodeStruct
{
    BE_MemberNode* members; // NOTE(Jens): count = memberCount.
};

struct BE_MemberNodeScalar
{
    BEFetchMemoryFlags flags;
    BE_ScalarBaseType baseType;
    BEu64 arrayCount;
    BEu64 address;
    BEu64 size;
};

typedef enum BE_MemberNodeType BE_MemberNodeType;
enum BE_MemberNodeType
{
    BE_MemberNodeType_Root,
    BE_MemberNodeType_StructArray,
    BE_MemberNodeType_Struct,
    BE_MemberNodeType_Scalar,
};

struct BE_MemberNode
{
    BE_MemberNode* parent;
    BEu64 appearCount;
    BE_MemberNodeType type;
    
    union
    {
        BE_MemberNodeRoot value_Root;
        BE_MemberNodeStructArray value_StructArray;
        BE_MemberNodeStruct value_Struct;
        BE_MemberNodeScalar value_Scalar;
    };
};

struct BERuntime
{
    BEAllocatorType allocator;
    
    BEu32 stackCapacity;
    BEu32 frameCount;
    BEu32 currentAddress;
    BEu32 hideMembersCounter;
    BEu32 instructionCount;
    BEu32 createdPlotCount;
    
    BEu32 instructionsSizeInBytes;
    BEu8* encodedInstructions;
    
    BEs64 sizeOfFile;
    BEu8* binaryFile;
    
    BEu8* stack;
    BEu8* startOfFrame;
    BEu8* categoriesSection;
    BEu8* typesSection;
    BEu8* enumsSection;
    BEu8* sourcesSection;
    
    BE_MemberNode* currentMembers;
};

enum BE_ScalarByteOrder
{
    BE_ScalarByteOrder_little,
    BE_ScalarByteOrder_big,
};

struct BE_ParserScopes
{
    BE_ParserScope* top;
    BE_ParserScope* free;
};

struct BE_String
{
	BEu64 count;
	char* characters;
};

struct BE_NameMap
{
    BEu32 count;
    BE_NameMapNode* root;
};

struct BE_Imports
{
    BEu32 depth;
    BE_NameMap imported;
    BE_ImportStack* top;
    BE_ImportStack* freeStack;
};

struct BE_GlobalConstants
{
    BE_GlobalConstantNode* root;
};

struct BE_ParsedTypedefs
{
    BE_ParsedTypedefNode* root;
    BEu32 count;
};

struct BE_ParsedStructs
{
    BE_ParsedStructNode* byName;
    BE_ParsedStructNode* byOrderHead;
    BE_ParsedStructNode* byOrderTail;
    
    BE_ParsedCategoryNode* categoriesByName;
    BE_ParsedCategoryNode* categoriesByOrderHead;
    BE_ParsedCategoryNode* categoriesByOrderTail;
    
    BEu32 structCount;
    BEu32 categoryCount;
};

struct BE_ParsedEnums
{
    BE_ParsedEnumNode* root;
    BE_ParsedEnumNode* byOrderHead;
    BE_ParsedEnumNode* byOrderTail;
    
    BEu32 count;
};

struct BE_Expression
{
    BEbool isFloat;
    BE_ExpressionTerm* termsSentinel;
    BEu32 currentExpressionPrecedence;
};

struct BE_PrintStatement
{
    BE_String message;
    BE_Expression expression;
};

struct BE_AssertStatement
{
    BE_Expression expression;
};

struct BE_VariableDeclaration
{
    BE_String name;
    BEu32 index;
    BE_Expression initializationExpression;
};

enum BE_TypeVariant
{
    BE_TypeVariant_Scalar,
    BE_TypeVariant_Struct,
    BE_TypeVariant_Category,
    BE_TypeVariant_Typedef,
};

struct BE_TypeDeclaration
{
    BE_TypeVariant variant;
    BE_Expression alignmentExpression;
    
    BEu32 argumentReadCount;
    BEu32 argumentCount;
    BE_Expression* arguments;
    
    union
    {
        struct
        {
            BE_ParsedEnumNode* enumType;
            BE_ScalarBaseType baseType;
            BE_Expression sizeExpression;
            BE_ScalarDisplayType displayType;
            BE_ScalarByteOrder byteOrder;
        } scalarVariant;
        
        struct
        {
            BE_ParsedStructNode* type;
        } structVariant;
        
        struct
        {
            BE_ParsedCategoryNode* category;
            BE_Expression categoryIdentifier;
        } categoryVariant;
        
        struct
        {
            BE_ParsedTypedefNode* def;
        } typedefVariant;
    };
};

struct BE_TypedefDeclaration
{
    BE_TypeDeclaration type;
    BE_String name;
};

struct BE_MemberDeclaration
{
    BE_TypeDeclaration type;
    BEu32 memberIndex;
    BEbool isHidden;
    BEbool isExternal;
    BEbool isInternal;
    BEbool isArray; // IMPORTANT(Jens): 'arrayCount' member is evaluated and freed, use this to determine if member has 'arrayCount' expression.
    BE_String name;
    BE_Expression address;
    BE_Expression arrayCount;
};

struct BE_LayoutStatement
{
    BEbool isInImportedFile;
    BEu32 typeIndex;
};

struct BE_EnumDeclaration
{
    BE_String name;
    BEbool isAnonymous;
    BEu32 memberCount;
    BEu32 size;
    BE_EnumMemberList* members;
};

struct BE_ImportStatement
{
    BE_String unescapedFile;
};

struct BE_DefaultDirective
{
    BEbool bigEndian;
};

struct BE_StructHeader
{
    BE_String name;
    BE_String category;
    BEs64 categoryNumber;
    BEu32 structIndex;
    BE_NameMap parameters;
};

struct BE_Assignment
{
    BEbool isGlobal;
    
    // NOTE(Jens): If 'variableIndex' < BE_GlobalSymbol_WriteableEnd then a BE_GlobalSymbol, else index is offset by BE_GlobalSymbol_WriteableEnd.
    BEu32 variableIndex;
    
    BE_punct op;
    BE_Expression rhs;
};

struct BE_If
{
    BEbool isElseIf;
    BE_Expression condition;
};

struct BE_ForLoop
{
    BE_VariableDeclaration variable;
    BE_Expression endCondition;
    BE_Assignment assignment;
};

struct BE_WhileLoop
{
    BE_Expression condition;
};

struct BE_EndOfDoWhileLoop
{
    BE_Expression condition;
};

struct BE_MemberPath
{
    BE_MemberPathNode* head;
    BE_MemberPathNode* tail;
    
    BEu32 count;
};

typedef struct BE_Plot BE_Plot;
struct BE_Plot
{
    BE_Expression plotId;
    BE_Expression value_f64;
};

typedef struct BE_Plot_XY BE_Plot_XY;
struct BE_Plot_XY
{
    BE_Expression plotId;
    BE_Expression x;
    BE_Expression y;
};

typedef struct BE_PlotMember BE_PlotMember;
struct BE_PlotMember
{
    BE_Expression plotId;
    BE_MemberPath member;
};

union BE_Statement
{
    BE_PrintStatement print;
    BE_AssertStatement assert;
    BE_VariableDeclaration variableDeclaration;
    BE_MemberDeclaration memberDeclaration;
    BE_TypedefDeclaration typedefDeclaration;
    BE_LayoutStatement layout;
    BE_EnumDeclaration enumDeclaration;
    BE_ImportStatement import;
    BE_DefaultDirective defaultDirective;
    BE_StructHeader structHeader;
    BE_Assignment assignment;
    BE_If ifStatement;
    BE_ForLoop forLoop;
    BE_WhileLoop whileLoop;
    BE_EndOfDoWhileLoop endOfDoWhile;
    BE_Plot plot;
    BE_Plot_XY plotXY;
    BE_PlotMember plotMember;
};

struct BE_AddressSpecification
{
    BEu32 returnMode;
    BEbool isExternal;
    BE_Expression expression;
};

struct BE_Expressions
{
    BE_ExpressionTerm* freeTerms;
    BE_UnaryOperator* freeUnaryOperators;
};

struct BE_ParsingExpressions
{
    BE_Expressions expressions;
    BE_ParsingExpression* top;
    BE_ParsingExpression* free;
};

struct BE_ParsingMemberAccesses
{
    BE_ParsingMemberAccess* top;
    BE_ParsingMemberAccess* free;
};

struct BE_ParsingLoadExpressions
{
    BE_ParsingLoadExpression* top;
    BE_ParsingLoadExpression* free;
};

typedef struct BE_LocalVariableScope BE_LocalVariableScope;
typedef struct BE_LocalVariables BE_LocalVariables;
struct BE_LocalVariables
{
    BE_LocalVariableScope* topScope;
};

typedef struct BE_ParserState BE_ParserState;
struct BE_ParserState
{
    BEAllocatorType allocator;
    
    BEu32 mode;
    BE_ParsedStructNode* parsingStruct;
    BE_ParserScopes scopes;
    BEbool previousStatementWasEndOfIf;
    
    BE_Imports imports;
    BE_LocalVariables globalVariables;
    BE_GlobalConstants globalConstants;
    BE_ParsedStructs structs;
    BE_ParsedTypedefs typedefs;
    BE_ParsedEnums enums;
    BE_ScalarByteOrder defaultByteOrder;
    
    BE_Statement statement;
    
    BE_ParsedCategoryNode* category;
    BE_Expression categoryNumber;
    
    BE_Expression tmpExpression;
    
    BE_AddressSpecification addressSpecification;
    BE_ParsingExpressions parsingExpressions;
    BE_ParsingMemberAccesses parsingMemberAccesses;
    BE_ParsingLoadExpressions loadExpressions;
    
    BE_GlobalConstantNode* constantLookup;
    
    BE_TypeDeclaration* typeDeclaration;
    BEu32 typeDeclarationReturnMode;
    
    BE_LocalVariableScope* freeLocalVariableScopes;
};

struct BE_InstructionRefs
{
    BE_InstructionRefStack* freeRefs;
    BE_InstructionIfChain* freeIfChains;
    
    BE_InstructionRefStack* stack;
    BE_InstructionIfChain* ifChains;
};

struct BE_InstructionList
{
    BE_InstructionListNode* head;
    BE_InstructionListNode* tail;
    
    BEu32 count;
};

struct BE_StructStartAddresses
{
    BEu32 globalHighestStackEntryUsed;
    
    BE_StructStartAddressNode* head;
    BE_StructStartAddressNode* tail;
};

struct BE_InstructionGenerator
{
    BEu32 instructionIndex;
    BEu32 parameterCount;
    
    BE_InstructionRefs instructionRefs;
    
    BEbool previousStatementWasEndOfIf;
    
    BE_StackU32* freeStackU32;
    BE_StackU32* startOfIterationStack;
    BE_StackU32* startOfScopeEntrySize;
    
    BEu32 pendingInstructionStackCount;
    BE_InstructionList pendingInstructions;
    
    BE_StructStartAddresses structStartAddresses;
    
    BEu32 stackIndex;
    BEu32 stackEntryCount[3];
    BEu32 maxStackEntryCount[3];
};

struct BE_EncodedInstructions
{
    BEu32 writtenBytes;
    
    BE_EncodedInstructionChunk* head;
    BE_EncodedInstructionChunk* tail;
};

struct BE_InstructionLocations
{
    BEu32 count;
    BE_InstructionLocationChunk* head;
    BE_InstructionLocationChunk* tail;
};

struct BE_InstructionEncoder
{
    BE_PendingJump* freePendingJumps;
    BE_PendingJump* pendingJumps;
    
    BEu32 totalWrittenBytes;
    BEu32 totalWrittenBytesLastFlush;
    
    BE_EncodedInstructions freeEncodedInstructions;
    BE_EncodedInstructions written;
    BE_InstructionLocations instructionAddresses;
};

typedef struct
{
    BEAllocatorType allocator;
    BE_InstructionListNode* free;
} BE_InstructionAllocator;

typedef struct BE_ImportedFilesChunk BE_ImportedFilesChunk;
typedef struct BE_ImportedFiles BE_ImportedFiles;
struct BE_ImportedFiles
{
    BE_ImportedFilesChunk* head;
    BE_ImportedFilesChunk* tail;
    BEu32 count;
};

typedef struct BE_FileParsings BE_FileParsings;
struct BE_FileParsings
{
    BEu32 depth;
    BE_FileParsing* top;
    BE_FileParsing* free;
    
    BE_ImportedFiles imported;
};

typedef struct BE_SourceLocationChunk BE_SourceLocationChunk;
typedef struct BE_SourceLocations BE_SourceLocations;

struct BE_SourceLocations
{
    BE_SourceLocationChunk* head;
    BE_SourceLocationChunk* tail;
    BEu32 count;
};

struct BECompilationContext
{
    BEFileLoader fileLoader;
    
    BE_ParserState parser;
    BE_Expressions expressions;
    BE_InstructionAllocator instructionAlloc;
    BE_InstructionGenerator instructionGenerator;
    BE_InstructionEncoder encoder;
    BE_FileParsings parsings;
    BE_SourceLocations sourceLocations;
    
    BE_EncodedInstructions pendingResult;
    BEu32 readBytes;
};

typedef enum
{
    BETokenSemantics_None,
    
    BETokenSemantics_Keyword,
    BETokenSemantics_TypeKeyword,
    BETokenSemantics_Type,
    BETokenSemantics_File,
    BETokenSemantics_Text,
    BETokenSemantics_Literal,
    BETokenSemantics_Member,
    BETokenSemantics_Variable,
    BETokenSemantics_Category,
} BETokenSemantics;


#endif
