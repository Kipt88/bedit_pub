

#include "BEdit.h"

static void DebugPrintInstruction(BEInstruction* instruction, struct BERuntime* runtime);

#include "BEdit.c"

typedef struct
{
    unsigned int failedCount;
} TestResult;

typedef void (*TestFunc)(TestResult* testResult);
#define Test(name) static void name(TestResult* testResult)

#define TestFailed() TestFailed_(testResult)
static void TestFailed_(TestResult* testResult)
{
    ++testResult->failedCount;
}

#define TestAssert(cnd) do { if (!(cnd)) { TestFailed(); return; } } while (0)

typedef struct
{
    const char* name;
    TestFunc func;
} TestData;

//
// NOTE(Jens): Tokenizer tests:
//

static char GetLetterForTokenType(BE_TokenType type)
{
    char rVal = '!';
    
    switch (type)
    {
        case BE_TokenType_NumberLiteral:
        {
            rVal = 'N';
        } break;
        
        case BE_TokenType_FloatLiteral:
        {
            rVal = 'F';
        } break;
        
        case BE_TokenType_Identifier:
        {
            rVal = 'I';
        } break;
        
        case BE_TokenType_QuotedString:
        {
            rVal = 'S';
        } break;
        
        case BE_TokenType_Punctuation:
        {
            rVal = 'P';
        } break;
        
        case BE_TokenType_Whitespace:
        {
            rVal = 'w';
        } break;
        
        case BE_TokenType_LineComment:
        {
            rVal = 'l';
        } break;
        
        case BE_TokenType_MultilineComment:
        {
            rVal = 'm';
        } break;
    }
    
    return rVal;
}

static void TestTokenizer(TestResult* testResult, const char* text, const char* tokenEnds)
{
    BE_TokenizerState tokenizer;
    BE_ClearStructToZero(tokenizer);
    
    for (const char *textIt = text, *endsIt = tokenEnds;
         *endsIt; 
         ++textIt, ++endsIt)
    {
        BE_TokenType tokenType;
        BE_TokenData tokenData;
        
        if (BE_FeedTokenizer(&tokenizer, *textIt, &tokenType, &tokenData))
        {
            TestAssert(*endsIt == GetLetterForTokenType(tokenType));
        }
        else
        {
            TestAssert(*endsIt == ' ');
        }
    }
}

static void TestTokenizer_Integers(TestResult* testResult, const char* text, const char* tokenEnds, BEu64* values)
{
    BE_TokenizerState tokenizer = { 0 };
    unsigned int integerIndex = 0;
    
    for (const char *textIt = text, *endsIt = tokenEnds;
         *endsIt; 
         ++textIt, ++endsIt)
    {
        BE_TokenType tokenType;
        BE_TokenData tokenData;
        
        if (BE_FeedTokenizer(&tokenizer, *textIt, &tokenType, &tokenData))
        {
            BEbool isSameType = (*endsIt == GetLetterForTokenType(tokenType));
            TestAssert(isSameType);
            
            if (isSameType && tokenType == BE_TokenType_NumberLiteral)
            {
                BEbool isSameValue = (tokenData.literalValue == values[integerIndex++]);
                TestAssert(isSameValue);
            }
        }
        else
        {
            TestAssert(*endsIt == ' ');
        }
    }
}

static void TestTokenizer_Floats(TestResult* testResult, const char* text, const char* tokenEnds, BEf64* values)
{
    BE_TokenizerState tokenizer = { 0 };
    unsigned int integerIndex = 0;
    
    BEf64 eps = 0.001;
    
    for (const char *textIt = text, *endsIt = tokenEnds;
         *endsIt; 
         ++textIt, ++endsIt)
    {
        BE_TokenType tokenType;
        BE_TokenData tokenData;
        
        if (BE_FeedTokenizer(&tokenizer, *textIt, &tokenType, &tokenData))
        {
            BEbool isSameType = (*endsIt == GetLetterForTokenType(tokenType));
            TestAssert(isSameType);
            
            if (isSameType && tokenType == BE_TokenType_FloatLiteral)
            {
                BEf64 value = values[integerIndex++];
                BEbool isSameValue = (tokenData.literalFloatValue - eps <= value && value <= tokenData.literalFloatValue + eps);
                TestAssert(isSameValue);
            }
        }
        else
        {
            TestAssert(*endsIt == ' ');
        }
    }
}

static void TestTokenizer_Punctuations(TestResult* testResult, const char* text, const char* tokenEnds, BE_punct* values)
{
    BE_TokenizerState tokenizer = { 0 };
    unsigned int integerIndex = 0;
    
    for (const char *textIt = text, *endsIt = tokenEnds;
         *endsIt; 
         ++textIt, ++endsIt)
    {
        BE_TokenType tokenType;
        BE_TokenData tokenData;
        
        if (BE_FeedTokenizer(&tokenizer, *textIt, &tokenType, &tokenData))
        {
            BEbool isSameType = (*endsIt == GetLetterForTokenType(tokenType));
            TestAssert(isSameType);
            
            if (isSameType && tokenType == BE_TokenType_Punctuation)
            {
                BEbool isSameValue = BE_PunctuationsAreSame(tokenData.punctuation, values[integerIndex++]);
                TestAssert(isSameValue);
            }
        }
        else
        {
            TestAssert(*endsIt == ' ');
        }
    }
}

Test(Tokenizer_NumberLiterals)
{
    {
        const char* text      = "123 010 0b1001 0x123AffE";
        const char* tokenEnds = "   Nw  Nw     Nw        N";
        BEu64 integers[] = {
            123,
            010,
            9,
            0x123affe
        };
        
        TestTokenizer_Integers(testResult, text, tokenEnds, integers);
    }
    
    {
        const char* text      = "123 0'10 0b10'01 0x12'3Af'fE";
        const char* tokenEnds = "   Nw   Nw      Nw          N";
        BEu64 integers[] = {
            123,
            010,
            9,
            0x123affe
        };
        
        TestTokenizer_Integers(testResult, text, tokenEnds, integers);
    }
}

Test(Tokenizer_FloatLiterals)
{
    const char* text      = "123. 0.10 1.0 .12'";
    const char* tokenEnds = "    Fw   Fw  Fw   F";
    BEf64 floats[] = {
        123.0,
        0.10,
        1.0,
        0.12
    };
    
    TestTokenizer_Floats(testResult, text, tokenEnds, floats);
}

Test(Tokenizer_StringLiterals)
{
    const char* text      = "'foobar' \"bar\\nfoo\" \"foo '\\\" bar\"";
    // NOTE(Jens): Unescaped 'foobar' "bar\nfoo" "foo '\" bar"
    const char* tokenEnds = "        Sw         Sw            S";
    
    TestTokenizer(testResult, text, tokenEnds);
}

Test(Tokenizer_Punctuations)
{
    const char* text      = "|| && != == << >> <= >= = -- ++ -= += *= /= <<= >>= ^= |= &= %= ~ | & ! @ % ? , . { } ( ) [ ] + / * - < > ^ ; $ : #";
    const char* tokenEnds = "  Pw Pw Pw Pw Pw Pw Pw PwPw Pw Pw Pw Pw Pw Pw  Pw  Pw Pw Pw Pw PwPwPwPwPwPwPwPwPwPwPwPwPwPwPwPwPwPwPwPwPwPwPwPwPwPwP";
    BE_punct punctuations[47];
    punctuations[ 0] = BE_Punctuation("||");
    punctuations[ 1] = BE_Punctuation("&&");
    punctuations[ 2] = BE_Punctuation("!=");
    punctuations[ 3] = BE_Punctuation("==");
    punctuations[ 4] = BE_Punctuation("<<");
    punctuations[ 5] = BE_Punctuation(">>");
    punctuations[ 6] = BE_Punctuation("<=");
    punctuations[ 7] = BE_Punctuation(">=");
    punctuations[ 8] = BE_Punctuation("=");
    punctuations[ 9] = BE_Punctuation("--");
    punctuations[10] = BE_Punctuation("++");
    punctuations[11] = BE_Punctuation("-=");
    punctuations[12] = BE_Punctuation("+=");
    punctuations[13] = BE_Punctuation("*=");
    punctuations[14] = BE_Punctuation("/=");
    punctuations[15] = BE_Punctuation("<<=");
    punctuations[16] = BE_Punctuation(">>=");
    punctuations[17] = BE_Punctuation("^=");
    punctuations[18] = BE_Punctuation("|=");
    punctuations[19] = BE_Punctuation("&=");
    punctuations[20] = BE_Punctuation("%=");
    punctuations[21] = BE_Punctuation("~");
    punctuations[22] = BE_Punctuation("|");
    punctuations[23] = BE_Punctuation("&");
    punctuations[24] = BE_Punctuation("!");
    punctuations[25] = BE_Punctuation("@");
    punctuations[26] = BE_Punctuation("%");
    punctuations[27] = BE_Punctuation("?");
    punctuations[28] = BE_Punctuation(",");
    punctuations[29] = BE_Punctuation(".");
    punctuations[30] = BE_Punctuation("{");
    punctuations[31] = BE_Punctuation("}");
    punctuations[32] = BE_Punctuation("(");
    punctuations[33] = BE_Punctuation(")");
    punctuations[34] = BE_Punctuation("[");
    punctuations[35] = BE_Punctuation("]");
    punctuations[36] = BE_Punctuation("+");
    punctuations[37] = BE_Punctuation("/");
    punctuations[38] = BE_Punctuation("*");
    punctuations[39] = BE_Punctuation("-");
    punctuations[40] = BE_Punctuation("<");
    punctuations[41] = BE_Punctuation(">");
    punctuations[42] = BE_Punctuation("^");
    punctuations[43] = BE_Punctuation(";");
    punctuations[44] = BE_Punctuation("$");
    punctuations[45] = BE_Punctuation(":");
    punctuations[46] = BE_Punctuation("#");
    
    TestTokenizer_Punctuations(testResult, text, tokenEnds, punctuations);
}

Test(Tokenizer_Comments)
{
    const char* text      = 
        "// foob ar \n"
        "/* lkj /** nested **/ and later */";
    
    const char* tokenEnds =
        "            "
        "l                                 m";
    
    TestTokenizer(testResult, text, tokenEnds);
}

Test(Tokenizer_Identifier)
{
    const char* text      = "The big brown fox _12 a2_a2r";
    const char* tokenEnds = "   Iw  Iw    Iw  Iw  Iw     I";
    
    TestTokenizer(testResult, text, tokenEnds);
}

Test(Tokenizer_Invalid)
{
    const char* text      = "00 /* /**/";
    const char* tokenEnds = " !Nw      !";
    
    TestTokenizer(testResult, text, tokenEnds);
}

//
// NOTE(Jens): Parser tests:
//

static BE_ParserState StartParsing(void)
{
    BEAllocatorType allocator = { 0 };
    BE_ParserState parser = BE_CreateParserState(allocator);
    
    return parser;
}

static BEbool ParseSingleStatement(BE_ParserState* parser, BE_StatementType* statementType, BE_Statement* statement, BEu32 tokenCount, BE_String* tokens, BE_TokenType* tokenTypes, BE_TokenData* tokenData)
{
    BEbool didParse = 0;
    
    for (BEu32 i = 0; i < tokenCount; ++i)
    {
        if (BE_FeedParser(parser, statementType, statement, tokenTypes[i], tokenData[i], tokens[i].count, tokens[i].characters))
        {
            if (i == tokenCount - 1)
            {
                didParse = 1;
            }
            else
            {
                break;
            }
        }
    }
    
    return didParse;
}

static void AddGlobalVariable(BE_ParserState* parser, BE_String name)
{
    BE_AddLocalVariable(parser->allocator, &parser->globalVariables, name);
}

static BE_ParsedStructNode* AddStruct(BEAllocatorType allocator, BE_ParsedStructs* structs, BE_String name, BEu32 paramCount, BE_String* paramNames)
{
    BE_NameMap parameters;
    BE_ClearStructToZero(parameters);
    
    for (BEu32 i = 0; i < paramCount; ++i)
    {
        BE_NameMapNode** paramSlot = BE_FindNameMapSlotFor(&parameters, paramNames[i]);
        
        if (*paramSlot)
        {
            // NOTE(Jens): Duplicate parameter names.
            return 0;
        }
        else
        {
            *paramSlot = BEAlloc(allocator, BE_NameMapNode);
            BE_ClearStructToZero(**paramSlot);
            
            (*paramSlot)->index = parameters.count++;
            (*paramSlot)->value = paramNames[i];
        }
    }
    
    BE_ParsedStructNode* rVal = BE_AddStruct(allocator, structs, name, parameters);
    return rVal;
}

BEPrivateAPI BE_ParsedStructNode* AddParsedStruct(BEAllocatorType allocator, BE_ParsedStructs* structs, BE_String structName, BE_String categoryName, BEs64 categoryNumber, BEu32 paramCount, BE_String* paramNames)
{
    BE_ParsedStructNode* structNode = AddStruct(allocator, structs, structName, paramCount, paramNames);
    if (structNode)
    {
        if (categoryName.count != 0)
        {
            BE_ParsedCategoryNode* categoryNode = BE_AcquireCategory(allocator, structs, categoryName);
            BE_AddTypeToCategory(structNode, categoryNode, categoryNumber);
        }
        
        BE_LocalVariableScope* freelist = 0;
        BE_PushLocalVariableScope(allocator, &freelist, &structNode->localVariables);
    }
    
    return structNode;
}

static BE_ParsedEnumNode* AddEnum(BE_ParserState* parser, BE_EnumDeclaration* enumDeclaration)
{
    BE_GlobalConstantNode** rootSlot = 0;
    
    if (enumDeclaration->isAnonymous || enumDeclaration->name.count == 0)
    {
        rootSlot = &parser->globalConstants.root;
    }
    else
    {
        rootSlot = BE_FindGlobalConstantSlot(&parser->globalConstants.root, enumDeclaration->name);
        
        if (!*rootSlot)
        {
            *rootSlot = BEAlloc(parser->allocator, BE_GlobalConstantNode);
            BE_ClearStructToZero(**rootSlot);
            (*rootSlot)->name = enumDeclaration->name;
        }
        
        rootSlot = &(*rootSlot)->children;
    }
    
    for (BE_EnumMemberList* it = enumDeclaration->members; it; it = it->prev)
    {
        BE_GlobalConstantNode** slot = BE_FindGlobalConstantSlot(rootSlot, it->name);
        
        BEAssert(!*slot); // NOTE(Jens): Duplicate enum member names.
        
        *slot = BEAlloc(parser->allocator, BE_GlobalConstantNode);
        BE_ClearStructToZero(**slot);
        (*slot)->name = it->name;
        (*slot)->value = it->value;
        (*slot)->hasValue = 1;
    }
    
    BE_ParsedEnumNode* rVal = 0;
    
    if (enumDeclaration->name.count)
    {
        rVal = BE_AddEnum(parser->allocator, &parser->enums, enumDeclaration->name, enumDeclaration->size);
    }
    
    return rVal;
}

#define I(index, id) \
tokens[index] = BE_StrLiteral(id); tokenTypes[index] = BE_TokenType_Identifier

#define P(index, punc) \
tokens[index] = BE_StrLiteral(punc); tokenTypes[index] = BE_TokenType_Punctuation; tokenData[index].punctuation = BE_Punctuation(punc)

#define Q(index, str) \
tokens[index] = BE_StrLiteral("\"" str "\""); tokenTypes[index] = BE_TokenType_QuotedString

#define N(index, value) \
tokens[index] = BE_StrLiteral(#value); tokenTypes[index] = BE_TokenType_NumberLiteral; tokenData[index].literalValue = value

#define F(index, value) \
tokens[index] = BE_StrLiteral(#value); tokenTypes[index] = BE_TokenType_FloatLiteral; tokenData[index].literalFloatValue = value

Test(Parser_Print_Msg)
{
    // NOTE(Jens): print("hello");
    
    BE_String tokens[5];
    BE_TokenType tokenTypes[5];
    BE_TokenData tokenData[5];
    
    I(0, "print");
    P(1, "(");
    Q(2, "hello");
    P(3, ")");
    P(4, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_Print);
    TestAssert(BE_StringsAreEqual(statement.print.message, tokens[2]));
    TestAssert(!BE_ExpressionIsValid(&statement.print.expression));
}

Test(Parser_Print_Expr)
{
    // NOTE(Jens): print(19);
    
    BE_String tokens[5];
    BE_TokenType tokenTypes[5];
    BE_TokenData tokenData[5];
    
    I(0, "print");
    P(1, "(");
    N(2, 19);
    P(3, ")");
    P(4, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_Print);
    TestAssert(statement.print.message.count == 0);
    TestAssert(BE_ExpressionIsValid(&statement.print.expression));
}

Test(Parser_Print_Msg_Expr)
{
    // NOTE(Jens): print("hello", 19);
    
    BE_String tokens[7];
    BE_TokenType tokenTypes[7];
    BE_TokenData tokenData[7];
    
    I(0, "print");
    P(1, "(");
    Q(2, "hello");
    P(3, ",");
    N(4, 19);
    P(5, ")");
    P(6, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_Print);
    TestAssert(BE_StringsAreEqual(statement.print.message, tokens[2]));
    TestAssert(BE_ExpressionIsValid(&statement.print.expression));
}

Test(Parser_Assert)
{
    // NOTE(Jens): assert(13);
    
    BE_String tokens[5];
    BE_TokenType tokenTypes[5];
    BE_TokenData tokenData[5];
    
    I(0, "assert");
    P(1, "(");
    N(2, 13);
    P(3, ")");
    P(4, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_Assert);
    TestAssert(BE_ExpressionIsValid(&statement.assert.expression));
}

Test(Parser_VariableDeclaration)
{
    // NOTE(Jens): var foo;
    
    BE_String tokens[3];
    BE_TokenType tokenTypes[3];
    BE_TokenData tokenData[3];
    
    I(0, "var");
    I(1, "foo");
    P(2, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_VariableDeclaration);
    TestAssert(!BE_ExpressionIsValid(&statement.variableDeclaration.initializationExpression));
}

Test(Parser_VariableDeclaration_Initializer)
{
    // NOTE(Jens): var foo = 11;
    
    BE_String tokens[5];
    BE_TokenType tokenTypes[5];
    BE_TokenData tokenData[5];
    
    I(0, "var");
    I(1, "foo");
    P(2, "=");
    N(3, 11);
    P(4, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_VariableDeclaration);
    TestAssert(BE_ExpressionIsValid(&statement.variableDeclaration.initializationExpression));
}

Test(Parser_Typedef)
{
    // NOTE(Jens): typedef u(4) U32;
    
    BE_String tokens[7];
    BE_TokenType tokenTypes[7];
    BE_TokenData tokenData[7];
    
    I(0, "typedef");
    I(1, "u");
    P(2, "(");
    N(3, 4);
    P(4, ")");
    I(5, "U32");
    P(6, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_TypedefDeclaration);
    TestAssert(BE_StringsAreEqual(statement.typedefDeclaration.name, BE_StrLiteral("U32")));
    TestAssert(BE_ExpressionIsValid(&statement.typedefDeclaration.type.scalarVariant.sizeExpression));
    TestAssert(!BE_ExpressionIsValid(&statement.typedefDeclaration.type.alignmentExpression));
}

Test(Parser_Layout)
{
    // NOTE(Jens): <<< struct Foo { ... }; >>> layout Foo;
    
    BE_String tokens[3];
    BE_TokenType tokenTypes[3];
    BE_TokenData tokenData[3];
    
    I(0, "layout");
    I(1, "Foo");
    P(2, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    
    BE_StructHeader structHeader;
    BE_ClearStructToZero(structHeader);
    
    structHeader.name = BE_StrLiteral("Foo");
    
    parser.parsingStruct = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_Layout);
    TestAssert(statement.layout.typeIndex == 0);
}

Test(Parser_EnumDeclaration)
{
    // NOTE(Jens): enum Foobar { Bar, Foo = 7 };
    
    BE_String tokens[10];
    BE_TokenType tokenTypes[10];
    BE_TokenData tokenData[10];
    
    I(0, "enum");
    I(1, "Foobar");
    P(2, "{");
    I(3, "Bar");
    P(4, ",");
    I(5, "Foo");
    P(6, "=");
    N(7, 7);
    P(8, "}");
    P(9, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_EnumDeclaration);
    TestAssert(BE_StringsAreEqual(statement.enumDeclaration.name, tokens[1]));
    TestAssert(!statement.enumDeclaration.isAnonymous);
    TestAssert(statement.enumDeclaration.memberCount == 2);
    
    BE_EnumMemberList* firstMember = statement.enumDeclaration.members->prev;
    BE_EnumMemberList* secondMember = statement.enumDeclaration.members;
    
    TestAssert(BE_StringsAreEqual(firstMember->name, tokens[3]));
    TestAssert(firstMember->value == 0);
    
    TestAssert(BE_StringsAreEqual(secondMember->name, tokens[5]));
    TestAssert(secondMember->value == (BEs64)tokenData[7].literalValue);
}

Test(Parser_EnumDeclaration_Sized)
{
    // NOTE(Jens): enum Foobar(4) { Bar, Foo = 7 };
    
    BE_String tokens[13];
    BE_TokenType tokenTypes[13];
    BE_TokenData tokenData[13];
    
    I( 0, "enum");
    I( 1, "Foobar");
    P( 2, "(");
    N( 3, 4);
    P( 4, ")");
    P( 5, "{");
    I( 6, "Bar");
    P( 7, ",");
    I( 8, "Foo");
    P( 9, "=");
    N(10, 7);
    P(11, "}");
    P(12, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_EnumDeclaration);
    TestAssert(BE_StringsAreEqual(statement.enumDeclaration.name, tokens[1]));
    TestAssert(!statement.enumDeclaration.isAnonymous);
    TestAssert(statement.enumDeclaration.size == 4);
    TestAssert(statement.enumDeclaration.memberCount == 2);
    
    BE_EnumMemberList* firstMember = statement.enumDeclaration.members->prev;
    BE_EnumMemberList* secondMember = statement.enumDeclaration.members;
    
    TestAssert(BE_StringsAreEqual(firstMember->name, tokens[6]));
    TestAssert(firstMember->value == 0);
    
    TestAssert(BE_StringsAreEqual(secondMember->name, tokens[8]));
    TestAssert(secondMember->value == (BEs64)tokenData[10].literalValue);
}

Test(Parser_EnumDeclaration_TrailingComma)
{
    // NOTE(Jens): enum Foobar { Bar, Foo = 7, };
    
    BE_String tokens[11];
    BE_TokenType tokenTypes[11];
    BE_TokenData tokenData[11];
    
    I( 0, "enum");
    I( 1, "Foobar");
    P( 2, "{");
    I( 3, "Bar");
    P( 4, ",");
    I( 5, "Foo");
    P( 6, "=");
    N( 7, 7);
    P( 8, ",");
    P( 9, "}");
    P(10, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_EnumDeclaration);
    TestAssert(BE_StringsAreEqual(statement.enumDeclaration.name, tokens[1]));
    TestAssert(!statement.enumDeclaration.isAnonymous);
    TestAssert(statement.enumDeclaration.memberCount == 2);
    
    BE_EnumMemberList* firstMember = statement.enumDeclaration.members->prev;
    BE_EnumMemberList* secondMember = statement.enumDeclaration.members;
    
    TestAssert(BE_StringsAreEqual(firstMember->name, tokens[3]));
    TestAssert(firstMember->value == 0);
    
    TestAssert(BE_StringsAreEqual(secondMember->name, tokens[5]));
    TestAssert(secondMember->value == (BEs64)tokenData[7].literalValue);
}

Test(Parser_EnumDeclaration_Anonymous)
{
    // NOTE(Jens): enum anonymous Foobar { Bar, Foo = 7, };
    
    BE_String tokens[12];
    BE_TokenType tokenTypes[12];
    BE_TokenData tokenData[12];
    
    I( 0, "enum");
    I( 1, "anonymous");
    I( 2, "Foobar");
    P( 3, "{");
    I( 4, "Bar");
    P( 5, ",");
    I( 6, "Foo");
    P( 7, "=");
    N( 8, 7);
    P( 9, ",");
    P(10, "}");
    P(11, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_EnumDeclaration);
    TestAssert(BE_StringsAreEqual(statement.enumDeclaration.name, tokens[2]));
    TestAssert(statement.enumDeclaration.isAnonymous);
    TestAssert(statement.enumDeclaration.memberCount == 2);
    
    BE_EnumMemberList* firstMember = statement.enumDeclaration.members->prev;
    BE_EnumMemberList* secondMember = statement.enumDeclaration.members;
    
    TestAssert(BE_StringsAreEqual(firstMember->name, tokens[4]));
    TestAssert(firstMember->value == 0);
    
    TestAssert(BE_StringsAreEqual(secondMember->name, tokens[6]));
    TestAssert(secondMember->value == (BEs64)tokenData[8].literalValue);
}

Test(Parser_EnumDeclaration_Nameless)
{
    // NOTE(Jens): enum { Bar, Foo = 7, };
    
    BE_String tokens[10];
    BE_TokenType tokenTypes[10];
    BE_TokenData tokenData[10];
    
    I(0, "enum");
    P(1, "{");
    I(2, "Bar");
    P(3, ",");
    I(4, "Foo");
    P(5, "=");
    N(6, 7);
    P(7, ",");
    P(8, "}");
    P(9, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_EnumDeclaration);
    TestAssert(statement.enumDeclaration.name.count == 0);
    TestAssert(statement.enumDeclaration.memberCount == 2);
    
    BE_EnumMemberList* firstMember = statement.enumDeclaration.members->prev;
    BE_EnumMemberList* secondMember = statement.enumDeclaration.members;
    
    TestAssert(BE_StringsAreEqual(firstMember->name, tokens[2]));
    TestAssert(firstMember->value == 0);
    
    TestAssert(BE_StringsAreEqual(secondMember->name, tokens[4]));
    TestAssert(secondMember->value == (BEs64)tokenData[6].literalValue);
}

Test(Parser_Import)
{
    // NOTE(Jens): import("a\\file.bet");
    
    BE_String tokens[5];
    BE_TokenType tokenTypes[5];
    BE_TokenData tokenData[5];
    
    I(0, "import");
    P(1, "(");
    Q(2, "a\\file.bet");
    P(3, ")");
    P(4, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_Import);
    TestAssert(BE_StringsAreEqual(statement.import.unescapedFile, BE_StrLiteral("a\\file.bet")));
}

Test(Parser_DefaultDirective_LittleEndian)
{
    // NOTE(Jens): #default(byteorder = little);
    
    BE_String tokens[8];
    BE_TokenType tokenTypes[8];
    BE_TokenData tokenData[8];
    
    P(0, "#");
    I(1, "default");
    P(2, "(");
    I(3, "byteorder");
    P(4, "=");
    I(5, "little");
    P(6, ")");
    P(7, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_DefaultDirective);
    TestAssert(!statement.defaultDirective.bigEndian);
}

Test(Parser_DefaultDirective_BigEndian)
{
    // NOTE(Jens): #default(byteorder = big);
    
    BE_String tokens[8];
    BE_TokenType tokenTypes[8];
    BE_TokenData tokenData[8];
    
    P(0, "#");
    I(1, "default");
    P(2, "(");
    I(3, "byteorder");
    P(4, "=");
    I(5, "big");
    P(6, ")");
    P(7, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_DefaultDirective);
    TestAssert(statement.defaultDirective.bigEndian);
}

Test(Parser_Struct)
{
    // NOTE(Jens): struct Foo {
    
    BE_String tokens[3];
    BE_TokenType tokenTypes[3];
    BE_TokenData tokenData[3];
    
    I(0, "struct");
    I(1, "Foo");
    P(2, "{");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_StructHeader);
    TestAssert(BE_StringsAreEqual(statement.structHeader.name, tokens[1]));
    TestAssert(statement.structHeader.category.count == 0);
}

Test(Parser_Struct_Category)
{
    // NOTE(Jens): struct (Bar, 123) Foo {
    
    BE_String tokens[8];
    BE_TokenType tokenTypes[8];
    BE_TokenData tokenData[8];
    
    I(0, "struct");
    P(1, "(");
    I(2, "Bar");
    P(3, ",");
    N(4, 123);
    P(5, ")");
    I(6, "Foo");
    P(7, "{");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_StructHeader);
    TestAssert(BE_StringsAreEqual(statement.structHeader.name, tokens[6]));
    TestAssert(BE_StringsAreEqual(statement.structHeader.category, tokens[2]));
    TestAssert(statement.structHeader.categoryNumber == (BEs64)tokenData[4].literalValue);
}

Test(Parser_Struct_Params)
{
    // NOTE(Jens): struct Foo(var x, var y) {
    
    BE_String tokens[10];
    BE_TokenType tokenTypes[10];
    BE_TokenData tokenData[10];
    
    I(0, "struct");
    I(1, "Foo");
    P(2, "(");
    I(3, "var");
    I(4, "x");
    P(5, ",");
    I(6, "var");
    I(7, "y");
    P(8, ")");
    P(9, "{");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_StructHeader);
    TestAssert(BE_StringsAreEqual(statement.structHeader.name, tokens[1]));
    TestAssert(statement.structHeader.parameters.count == 2);
    TestAssert(BE_FindMappedName(&statement.structHeader.parameters, tokens[4]));
    TestAssert(BE_FindMappedName(&statement.structHeader.parameters, tokens[7]));
}

Test(Parser_Struct_Category_Params)
{
    // NOTE(Jens): struct(Bar, 123) Foo(var x, var y) {
    
    BE_String tokens[15];
    BE_TokenType tokenTypes[15];
    BE_TokenData tokenData[15];
    
    I( 0, "struct");
    P( 1, "(");
    I( 2, "Bar");
    P( 3, ",");
    N( 4, 123);
    P( 5, ")");
    I( 6, "Foo");
    P( 7, "(");
    I( 8, "var");
    I( 9, "x");
    P(10, ",");
    I(11, "var");
    I(12, "y");
    P(13, ")");
    P(14, "{");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_StructHeader);
    TestAssert(BE_StringsAreEqual(statement.structHeader.name, tokens[6]));
    TestAssert(BE_StringsAreEqual(statement.structHeader.category, tokens[2]));
    TestAssert(statement.structHeader.categoryNumber == (BEs64)tokenData[4].literalValue);
    TestAssert(statement.structHeader.parameters.count == 2);
    TestAssert(BE_FindMappedName(&statement.structHeader.parameters, tokens[9]));
    TestAssert(BE_FindMappedName(&statement.structHeader.parameters, tokens[12]));
}

Test(Parser_MemberOffset_Assignment)
{
    // NOTE(Jens): @ = 123;
    
    BE_String tokens[4];
    BE_TokenType tokenTypes[4];
    BE_TokenData tokenData[4];
    
    P(0, "@");
    P(1, "=");
    N(2, 123);
    P(3, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_Assignment);
    TestAssert(statement.assignment.variableIndex == BE_GlobalSymbol_MemberOffset);
    TestAssert(BE_PunctuationsAreSame(statement.assignment.op, BE_Punctuation("=")));
    TestAssert(BE_ExpressionIsValid(&statement.assignment.rhs));
}

Test(Parser_Variable_Assignment)
{
    // NOTE(Jens): foo = 123;
    
    BE_String tokens[4];
    BE_TokenType tokenTypes[4];
    BE_TokenData tokenData[4];
    
    I(0, "foo");
    P(1, "=");
    N(2, 123);
    P(3, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    AddGlobalVariable(&parser, BE_StrLiteral("foo"));
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_Assignment);
    TestAssert(statement.assignment.variableIndex == BE_GlobalSymbol_Count + 0);
    TestAssert(BE_PunctuationsAreSame(statement.assignment.op, BE_Punctuation("=")));
    TestAssert(BE_ExpressionIsValid(&statement.assignment.rhs));
}

Test(Parser_Variable_Assignment_BitshiftLeft)
{
    // NOTE(Jens): foo <<= 123;
    
    BE_String tokens[4];
    BE_TokenType tokenTypes[4];
    BE_TokenData tokenData[4];
    
    I(0, "foo");
    P(1, "<<=");
    N(2, 123);
    P(3, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    AddGlobalVariable(&parser, BE_StrLiteral("foo"));
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_Assignment);
    TestAssert(statement.assignment.variableIndex == BE_GlobalSymbol_Count + 0);
    TestAssert(BE_PunctuationsAreSame(statement.assignment.op, BE_Punctuation("<<=")));
    TestAssert(BE_ExpressionIsValid(&statement.assignment.rhs));
}

Test(Parser_Variable_Assignment_BitshiftRight)
{
    // NOTE(Jens): foo >>= 123;
    
    BE_String tokens[4];
    BE_TokenType tokenTypes[4];
    BE_TokenData tokenData[4];
    
    I(0, "foo");
    P(1, ">>=");
    N(2, 123);
    P(3, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    AddGlobalVariable(&parser, BE_StrLiteral("foo"));
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_Assignment);
    TestAssert(statement.assignment.variableIndex == BE_GlobalSymbol_Count + 0);
    TestAssert(BE_PunctuationsAreSame(statement.assignment.op, BE_Punctuation(">>=")));
    TestAssert(BE_ExpressionIsValid(&statement.assignment.rhs));
}

Test(Parser_Variable_Assignment_Xor)
{
    // NOTE(Jens): foo ^= 123;
    
    BE_String tokens[4];
    BE_TokenType tokenTypes[4];
    BE_TokenData tokenData[4];
    
    I(0, "foo");
    P(1, "^=");
    N(2, 123);
    P(3, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    AddGlobalVariable(&parser, BE_StrLiteral("foo"));
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_Assignment);
    TestAssert(statement.assignment.variableIndex == BE_GlobalSymbol_Count + 0);
    TestAssert(BE_PunctuationsAreSame(statement.assignment.op, BE_Punctuation("^=")));
    TestAssert(BE_ExpressionIsValid(&statement.assignment.rhs));
}

Test(Parser_Variable_Assignment_Or)
{
    // NOTE(Jens): foo |= 123;
    
    BE_String tokens[4];
    BE_TokenType tokenTypes[4];
    BE_TokenData tokenData[4];
    
    I(0, "foo");
    P(1, "|=");
    N(2, 123);
    P(3, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    AddGlobalVariable(&parser, BE_StrLiteral("foo"));
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_Assignment);
    TestAssert(statement.assignment.variableIndex == BE_GlobalSymbol_Count + 0);
    TestAssert(BE_PunctuationsAreSame(statement.assignment.op, BE_Punctuation("|=")));
    TestAssert(BE_ExpressionIsValid(&statement.assignment.rhs));
}

Test(Parser_Variable_Assignment_And)
{
    // NOTE(Jens): foo &= 123;
    
    BE_String tokens[4];
    BE_TokenType tokenTypes[4];
    BE_TokenData tokenData[4];
    
    I(0, "foo");
    P(1, "&=");
    N(2, 123);
    P(3, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    AddGlobalVariable(&parser, BE_StrLiteral("foo"));
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_Assignment);
    TestAssert(statement.assignment.variableIndex == BE_GlobalSymbol_Count + 0);
    TestAssert(BE_PunctuationsAreSame(statement.assignment.op, BE_Punctuation("&=")));
    TestAssert(BE_ExpressionIsValid(&statement.assignment.rhs));
}

Test(Parser_Variable_Assignment_Mod)
{
    // NOTE(Jens): foo %= 123;
    
    BE_String tokens[4];
    BE_TokenType tokenTypes[4];
    BE_TokenData tokenData[4];
    
    I(0, "foo");
    P(1, "%=");
    N(2, 123);
    P(3, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    AddGlobalVariable(&parser, BE_StrLiteral("foo"));
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_Assignment);
    TestAssert(statement.assignment.variableIndex == BE_GlobalSymbol_Count + 0);
    TestAssert(BE_PunctuationsAreSame(statement.assignment.op, BE_Punctuation("%=")));
    TestAssert(BE_ExpressionIsValid(&statement.assignment.rhs));
}

Test(Parser_Variable_Assignment_Div)
{
    // NOTE(Jens): foo /= 123;
    
    BE_String tokens[4];
    BE_TokenType tokenTypes[4];
    BE_TokenData tokenData[4];
    
    I(0, "foo");
    P(1, "/=");
    N(2, 123);
    P(3, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    AddGlobalVariable(&parser, BE_StrLiteral("foo"));
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_Assignment);
    TestAssert(statement.assignment.variableIndex == BE_GlobalSymbol_Count + 0);
    TestAssert(BE_PunctuationsAreSame(statement.assignment.op, BE_Punctuation("/=")));
    TestAssert(BE_ExpressionIsValid(&statement.assignment.rhs));
}

Test(Parser_Variable_Assignment_Mul)
{
    // NOTE(Jens): foo *= 123;
    
    BE_String tokens[4];
    BE_TokenType tokenTypes[4];
    BE_TokenData tokenData[4];
    
    I(0, "foo");
    P(1, "*=");
    N(2, 123);
    P(3, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    AddGlobalVariable(&parser, BE_StrLiteral("foo"));
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_Assignment);
    TestAssert(statement.assignment.variableIndex == BE_GlobalSymbol_Count + 0);
    TestAssert(BE_PunctuationsAreSame(statement.assignment.op, BE_Punctuation("*=")));
    TestAssert(BE_ExpressionIsValid(&statement.assignment.rhs));
}

Test(Parser_Variable_Assignment_Sub)
{
    // NOTE(Jens): foo -= 123;
    
    BE_String tokens[4];
    BE_TokenType tokenTypes[4];
    BE_TokenData tokenData[4];
    
    I(0, "foo");
    P(1, "-=");
    N(2, 123);
    P(3, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    AddGlobalVariable(&parser, BE_StrLiteral("foo"));
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_Assignment);
    TestAssert(statement.assignment.variableIndex == BE_GlobalSymbol_Count + 0);
    TestAssert(BE_PunctuationsAreSame(statement.assignment.op, BE_Punctuation("-=")));
    TestAssert(BE_ExpressionIsValid(&statement.assignment.rhs));
}

Test(Parser_Variable_Assignment_Add)
{
    // NOTE(Jens): foo += 123;
    
    BE_String tokens[4];
    BE_TokenType tokenTypes[4];
    BE_TokenData tokenData[4];
    
    I(0, "foo");
    P(1, "+=");
    N(2, 123);
    P(3, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    AddGlobalVariable(&parser, BE_StrLiteral("foo"));
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_Assignment);
    TestAssert(statement.assignment.variableIndex == BE_GlobalSymbol_Count + 0);
    TestAssert(BE_PunctuationsAreSame(statement.assignment.op, BE_Punctuation("+=")));
    TestAssert(BE_ExpressionIsValid(&statement.assignment.rhs));
}

Test(Parser_Variable_Assignment_ArrayCount)
{
    // NOTE(Jens): struct Foo { u(4) data[123]; var i; ... i = array_count(data); 
    
    BE_String tokens[7];
    BE_TokenType tokenTypes[7];
    BE_TokenData tokenData[7];
    
    I(0, "i");
    P(1, "=");
    I(2, "array_count");
    P(3, "(");
    I(4, "data");
    P(5, ")");
    P(6, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    
    BE_StructHeader structHeader = { 0 };
    structHeader.name = BE_StrLiteral("Foo");
    
    parser.parsingStruct = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    
    BE_AddLocalVariable(parser.allocator, &parser.parsingStruct->localVariables, BE_StrLiteral("i"));
    
    {
        BE_MemberDeclaration member = { 0 };
        member.type.scalarVariant.baseType = BE_ScalarBaseType_u;
        member.name = BE_StrLiteral("data");
        BE_InitExpression(parser.allocator, &parser.parsingExpressions.expressions, &member.arrayCount, 0);
        BE_PushConstantInteger(&member.arrayCount, 123);
        member.isArray = 1;
        BE_AddStructMember(parser.allocator, parser.parsingStruct, &member);
    }
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_Assignment);
    TestAssert(statement.assignment.variableIndex == BE_GlobalSymbol_Count + 0);
    TestAssert(BE_PunctuationsAreSame(statement.assignment.op, BE_Punctuation("=")));
    TestAssert(BE_ExpressionIsValid(&statement.assignment.rhs));
}

Test(Parser_Variable_Assignment_CreatePlot)
{
    // NOTE(Jens): plot = create_plot();
    
    BE_String tokens[6];
    BE_TokenType tokenTypes[6];
    BE_TokenData tokenData[6];
    
    I(0, "plot");
    P(1, "=");
    I(2, "create_plot");
    P(3, "(");
    P(4, ")");
    P(5, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    AddGlobalVariable(&parser, BE_StrLiteral("plot"));
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_Assignment);
    TestAssert(statement.assignment.variableIndex == BE_GlobalSymbol_Count + 0);
    TestAssert(BE_PunctuationsAreSame(statement.assignment.op, BE_Punctuation("=")));
    TestAssert(BE_ExpressionIsValid(&statement.assignment.rhs));
}

Test(Parser_Plot_X)
{
    // NOTE(Jens): plot_x(1, 123.0);
    
    BE_String tokens[7];
    BE_TokenType tokenTypes[7];
    BE_TokenData tokenData[7];
    
    I(0, "plot_x");
    P(1, "(");
    N(2, 1);
    P(3, ",");
    F(4, 123.0);
    P(5, ")");
    P(6, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_Plot_X);
    TestAssert(BE_ExpressionIsValid(&statement.plot.plotId));
    TestAssert(BE_ExpressionIsValid(&statement.plot.value_f64));
}

Test(Parser_Plot_Y)
{
    // NOTE(Jens): plot_y(1, 456.0);
    
    BE_String tokens[7];
    BE_TokenType tokenTypes[7];
    BE_TokenData tokenData[7];
    
    I(0, "plot_y");
    P(1, "(");
    N(2, 1);
    P(3, ",");
    F(4, 456.0);
    P(5, ")");
    P(6, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_Plot_Y);
    TestAssert(BE_ExpressionIsValid(&statement.plotXY.plotId));
    TestAssert(BE_ExpressionIsValid(&statement.plot.plotId));
    TestAssert(BE_ExpressionIsValid(&statement.plot.value_f64));
}

Test(Parser_Plot_XY)
{
    // NOTE(Jens): plot_xy(1, 123, 456);
    
    BE_String tokens[9];
    BE_TokenType tokenTypes[9];
    BE_TokenData tokenData[9];
    
    I(0, "plot_xy");
    P(1, "(");
    N(2, 1);
    P(3, ",");
    N(4, 123);
    P(5, ",");
    N(6, 456);
    P(7, ")");
    P(8, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_Plot_XY);
    TestAssert(BE_ExpressionIsValid(&statement.plotXY.plotId));
    TestAssert(BE_ExpressionIsValid(&statement.plotXY.x));
    TestAssert(BE_ExpressionIsValid(&statement.plotXY.y));
}

Test(Parser_PlotMember_X)
{
    // NOTE(Jens): <<< struct Foo { f(4) data[36]; ... >>> plot_array_x(123, data);
    
    BE_String tokens[7];
    BE_TokenType tokenTypes[7];
    BE_TokenData tokenData[7];
    
    I(0, "plot_array_x");
    P(1, "(");
    N(2, 123);
    P(3, ",");
    I(4, "data");
    P(5, ")");
    P(6, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    
    BE_StructHeader structHeader;
    BE_ClearStructToZero(structHeader);
    
    structHeader.name = BE_StrLiteral("Foo");
    
    parser.parsingStruct = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    
    {
        BE_MemberDeclaration member;
        BE_ClearStructToZero(member); // NOTE(Jens): Zero-initialization is scalar type.
        member.type.scalarVariant.baseType = BE_ScalarBaseType_f;
        member.name = BE_StrLiteral("data");
        BE_InitExpression(parser.allocator, &parser.parsingExpressions.expressions, &member.arrayCount, 0);
        BE_PushConstantInteger(&member.arrayCount, 36);
        member.isArray = 1;
        BE_AddStructMember(parser.allocator, parser.parsingStruct, &member);
    }
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_Plot_Member_X);
    TestAssert(statement.plotMember.member.count == 1);
    TestAssert(statement.plotMember.member.head->memberIndex == 0);
    TestAssert(!BE_ExpressionIsValid(&statement.plotMember.member.head->arrayIndex));
}

Test(Parser_PlotMember_Y)
{
    // NOTE(Jens): <<< struct Foo { f(4) data[36]; ... >>> plot_array_y(123, data);
    
    BE_String tokens[7];
    BE_TokenType tokenTypes[7];
    BE_TokenData tokenData[7];
    
    I(0, "plot_array_y");
    P(1, "(");
    N(2, 123);
    P(3, ",");
    I(4, "data");
    P(5, ")");
    P(6, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    
    BE_StructHeader structHeader;
    BE_ClearStructToZero(structHeader);
    
    structHeader.name = BE_StrLiteral("Foo");
    
    parser.parsingStruct = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    
    {
        BE_MemberDeclaration member;
        BE_ClearStructToZero(member); // NOTE(Jens): Zero-initialization is scalar type.
        member.type.scalarVariant.baseType = BE_ScalarBaseType_f;
        member.name = BE_StrLiteral("data");
        BE_InitExpression(parser.allocator, &parser.parsingExpressions.expressions, &member.arrayCount, 0);
        BE_PushConstantInteger(&member.arrayCount, 36);
        member.isArray = 1;
        BE_AddStructMember(parser.allocator, parser.parsingStruct, &member);
    }
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_Plot_Member_Y);
    TestAssert(statement.plotMember.member.count == 1);
    TestAssert(statement.plotMember.member.head->memberIndex == 0);
    TestAssert(!BE_ExpressionIsValid(&statement.plotMember.member.head->arrayIndex));
}

Test(Parser_If)
{
    // NOTE(Jens): if (123) {
    
    BE_String tokens[5];
    BE_TokenType tokenTypes[5];
    BE_TokenData tokenData[5];
    
    I(0, "if");
    P(1, "(");
    N(2, 123);
    P(3, ")");
    P(4, "{");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_If);
}

Test(Parser_ElseIf)
{
    // NOTE(Jens): if (...) { ... } else if (123) {
    
    BE_String tokens[6];
    BE_TokenType tokenTypes[6];
    BE_TokenData tokenData[6];
    
    I(0, "else");
    I(1, "if");
    P(2, "(");
    N(3, 123);
    P(4, ")");
    P(5, "{");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    parser.previousStatementWasEndOfIf = 1;
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_If);
    TestAssert(statement.ifStatement.isElseIf);
}

Test(Parser_Else)
{
    // NOTE(Jens): if (...) { ... } else {
    
    BE_String tokens[2];
    BE_TokenType tokenTypes[2];
    BE_TokenData tokenData[2];
    
    I(0, "else");
    P(1, "{");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    parser.previousStatementWasEndOfIf = 1;
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_Else);
}

Test(Parser_For)
{
    // NOTE(Jens): for (var i = 2; i < 7; i += 1) {
    
    BE_String tokens[16];
    BE_TokenType tokenTypes[16];
    BE_TokenData tokenData[16];
    
    I( 0, "for");
    P( 1, "(");
    I( 2, "var");
    I( 3, "i");
    P( 4, "=");
    N( 5, 2);
    P( 6, ";");
    I( 7, "i");
    P( 8, "<");
    N( 9, 7);
    P(10, ";");
    I(11, "i");
    P(12, "+=");
    N(13, 1);
    P(14, ")");
    P(15, "{");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_For);
    TestAssert(BE_StringsAreEqual(statement.forLoop.variable.name, tokens[3]));
    TestAssert(BE_ExpressionIsValid(&statement.forLoop.endCondition));
    TestAssert(BE_ExpressionIsValid(&statement.forLoop.assignment.rhs));
}

Test(Parser_For_NoVar)
{
    // NOTE(Jens): for (; @ < 7; @ += 1) {
    
    BE_String tokens[12];
    BE_TokenType tokenTypes[12];
    BE_TokenData tokenData[12];
    
    I( 0, "for");
    P( 1, "(");
    P( 2, ";");
    P( 3, "@");
    P( 4, "<");
    N( 5, 7);
    P( 6, ";");
    P( 7, "@");
    P( 8, "+=");
    N( 9, 1);
    P(10, ")");
    P(11, "{");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_For);
    TestAssert(statement.forLoop.variable.name.count == 0);
    TestAssert(BE_ExpressionIsValid(&statement.forLoop.endCondition));
    TestAssert(BE_ExpressionIsValid(&statement.forLoop.assignment.rhs));
}

Test(Parser_For_NoCondition)
{
    // NOTE(Jens): for (var i; ; i += 1) {
    
    BE_String tokens[11];
    BE_TokenType tokenTypes[11];
    BE_TokenData tokenData[11];
    
    I( 0, "for");
    P( 1, "(");
    I( 2, "var");
    I( 3, "i");
    P( 4, ";");
    P( 5, ";");
    I( 6, "i");
    P( 7, "+=");
    N( 8, 1);
    P( 9, ")");
    P(10, "{");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_For);
    TestAssert(BE_StringsAreEqual(statement.forLoop.variable.name, tokens[3]));
    TestAssert(!BE_ExpressionIsValid(&statement.forLoop.endCondition));
    TestAssert(BE_ExpressionIsValid(&statement.forLoop.assignment.rhs));
}

Test(Parser_For_NoAssignment)
{
    // NOTE(Jens): for (var i; i < 7; ) {
    
    BE_String tokens[11];
    BE_TokenType tokenTypes[11];
    BE_TokenData tokenData[11];
    
    I( 0, "for");
    P( 1, "(");
    I( 2, "var");
    I( 3, "i");
    P( 4, ";");
    I( 5, "i");
    P( 6, "<");
    N( 7, 7);
    P( 8, ";");
    P( 9, ")");
    P(10, "{");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_For);
    TestAssert(BE_StringsAreEqual(statement.forLoop.variable.name, tokens[3]));
    TestAssert(BE_ExpressionIsValid(&statement.forLoop.endCondition));
    TestAssert(!BE_ExpressionIsValid(&statement.forLoop.assignment.rhs));
}

Test(Parser_For_NoVar_NoCondition)
{
    // NOTE(Jens): for (; ; @ += 1) {
    
    BE_String tokens[9];
    BE_TokenType tokenTypes[9];
    BE_TokenData tokenData[9];
    
    I(0, "for");
    P(1, "(");
    P(2, ";");
    P(3, ";");
    P(4, "@");
    P(5, "+=");
    N(6, 1);
    P(7, ")");
    P(8, "{");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_For);
    TestAssert(statement.forLoop.variable.name.count == 0);
    TestAssert(!BE_ExpressionIsValid(&statement.forLoop.endCondition));
    TestAssert(BE_ExpressionIsValid(&statement.forLoop.assignment.rhs));
}

Test(Parser_For_NoVar_NoAssignment)
{
    // NOTE(Jens): for (; 1; ) {
    
    BE_String tokens[7];
    BE_TokenType tokenTypes[7];
    BE_TokenData tokenData[7];
    
    I(0, "for");
    P(1, "(");
    P(2, ";");
    N(3, 1);
    P(4, ";");
    P(5, ")");
    P(6, "{");
    
    tokens[0]     = BE_StrLiteral("for");
    tokenTypes[0] = BE_TokenType_Identifier;
    
    tokens[1]     = BE_StrLiteral("(");
    tokenTypes[1] = BE_TokenType_Punctuation;
    tokenData[1].punctuation = BE_Punctuation("(");
    
    tokens[2]     = BE_StrLiteral(";");
    tokenTypes[2] = BE_TokenType_Punctuation;
    tokenData[2].punctuation = BE_Punctuation(";");
    
    tokens[3]     = BE_StrLiteral("1");
    tokenTypes[3] = BE_TokenType_NumberLiteral;
    tokenData[3].literalValue = 1;
    
    tokens[4]     = BE_StrLiteral(";");
    tokenTypes[4] = BE_TokenType_Punctuation;
    tokenData[4].punctuation = BE_Punctuation(";");
    
    tokens[5]     = BE_StrLiteral(")");
    tokenTypes[5] = BE_TokenType_Punctuation;
    tokenData[5].punctuation = BE_Punctuation(")");
    
    tokens[6]     = BE_StrLiteral("{");
    tokenTypes[6] = BE_TokenType_Punctuation;
    tokenData[6].punctuation = BE_Punctuation("{");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_For);
    TestAssert(statement.forLoop.variable.name.count == 0);
    TestAssert(BE_ExpressionIsValid(&statement.forLoop.endCondition));
    TestAssert(!BE_ExpressionIsValid(&statement.forLoop.assignment.rhs));
}

Test(Parser_For_NoAssignment_NoCondition)
{
    // NOTE(Jens): for (var i; ; ) {
    
    BE_String tokens[8];
    BE_TokenType tokenTypes[8];
    BE_TokenData tokenData[8];
    
    I(0, "for");
    P(1, "(");
    I(2, "var");
    I(3, "i");
    P(4, ";");
    P(5, ";");
    P(6, ")");
    P(7, "{");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_For);
    TestAssert(BE_StringsAreEqual(statement.forLoop.variable.name, tokens[3]));
    TestAssert(!BE_ExpressionIsValid(&statement.forLoop.endCondition));
    TestAssert(!BE_ExpressionIsValid(&statement.forLoop.assignment.rhs));
}

Test(Parser_For_NoVar_NoAssignment_NoCondition)
{
    // NOTE(Jens): for (;;) {
    
    BE_String tokens[6];
    BE_TokenType tokenTypes[6];
    BE_TokenData tokenData[6];
    
    I(0, "for");
    P(1, "(");
    P(2, ";");
    P(3, ";");
    P(4, ")");
    P(5, "{");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_For);
    TestAssert(statement.forLoop.variable.name.count == 0);
    TestAssert(!BE_ExpressionIsValid(&statement.forLoop.endCondition));
    TestAssert(!BE_ExpressionIsValid(&statement.forLoop.assignment.rhs));
}

Test(Parser_While)
{
    // NOTE(Jens): while (1) {
    
    BE_String tokens[5];
    BE_TokenType tokenTypes[5];
    BE_TokenData tokenData[5];
    
    I(0, "while");
    P(1, "(");
    N(2, 1);
    P(3, ")");
    P(4, "{");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_While);
    TestAssert(BE_ExpressionIsValid(&statement.whileLoop.condition));
}

Test(Parser_Do)
{
    // NOTE(Jens): do {
    
    BE_String tokens[2];
    BE_TokenType tokenTypes[2];
    BE_TokenData tokenData[2];
    
    I(0, "do");
    P(1, "{");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_Do);
}

Test(Parser_EndOf_Struct)
{
    // NOTE(Jens): struct ... };
    
    BE_String tokens[2];
    BE_TokenType tokenTypes[2];
    BE_TokenData tokenData[2];
    
    P(0, "}");
    P(1, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    BE_PushParsingScope(parser.allocator, &parser.scopes)->type = BE_ParserScopeType_Struct;
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_EndOf_Struct);
}

Test(Parser_EndOf_For)
{
    // NOTE(Jens): for (...) { ... }
    
    BE_String tokens[1];
    BE_TokenType tokenTypes[1];
    BE_TokenData tokenData[1];
    
    P(0, "}");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    BE_PushParsingScope(parser.allocator, &parser.scopes)->type = BE_ParserScopeType_For;
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_EndOf_For);
}

Test(Parser_EndOf_While)
{
    // NOTE(Jens): while (...) { ... }
    
    BE_String tokens[1];
    BE_TokenType tokenTypes[1];
    BE_TokenData tokenData[1];
    
    P(0, "}");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    BE_PushParsingScope(parser.allocator, &parser.scopes)->type = BE_ParserScopeType_While;
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_EndOf_While);
}

Test(Parser_EndOf_DoWhile)
{
    // NOTE(Jens): do ... } while (1);
    
    BE_String tokens[6];
    BE_TokenType tokenTypes[6];
    BE_TokenData tokenData[6];
    
    P(0, "}");
    I(1, "while");
    P(2, "(");
    N(3, 1);
    P(4, ")");
    P(5, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    BE_PushParsingScope(parser.allocator, &parser.scopes)->type = BE_ParserScopeType_Do;
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_EndOf_DoWhile);
    TestAssert(BE_ExpressionIsValid(&statement.endOfDoWhile.condition));
}

Test(Parser_EndOf_If)
{
    // NOTE(Jens): if { ... }
    
    BE_String tokens[1];
    BE_TokenType tokenTypes[1];
    BE_TokenData tokenData[1];
    
    P(0, "}");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    BE_PushParsingScope(parser.allocator, &parser.scopes)->type = BE_ParserScopeType_If;
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_EndOf_If);
}

Test(Parser_EndOf_Else)
{
    // NOTE(Jens): if { ... }
    
    BE_String tokens[1];
    BE_TokenType tokenTypes[1];
    BE_TokenData tokenData[1];
    
    P(0, "}");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    BE_PushParsingScope(parser.allocator, &parser.scopes)->type = BE_ParserScopeType_Else;
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_EndOf_Else);
}

Test(Parser_EnumRead_Unnamed)
{
    // NOTE(Jens): enum { Bar_Foo, Foo_Bar }; ... var i = Foo_Bar;
    
    BE_String tokens[5];
    BE_TokenType tokenTypes[5];
    BE_TokenData tokenData[5];
    
    I(0, "var");
    I(1, "i");
    P(2, "=");
    I(3, "Foo_Bar");
    P(4, ";");
    
    BE_EnumMemberList enumMembers[2];
    enumMembers[0].name = BE_StrLiteral("Bar_Foo");
    enumMembers[0].value = 1;
    enumMembers[0].prev = enumMembers + 1;
    
    enumMembers[1].name = BE_StrLiteral("Foo_Bar");
    enumMembers[1].value = 2;
    enumMembers[1].prev = 0;
    
    BE_EnumDeclaration enumDeclaration;
    BE_ClearStructToZero(enumDeclaration);
    
    enumDeclaration.memberCount = BE_ArrayCount(enumMembers);
    enumDeclaration.members = &enumMembers[0];
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    AddEnum(&parser, &enumDeclaration);
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_VariableDeclaration);
}

Test(Parser_EnumRead_Anonymous)
{
    // NOTE(Jens): enum anonymous Foo { Bar_Foo, Foo_Bar }; ... var i = Foo_Bar;
    
    BE_String tokens[5];
    BE_TokenType tokenTypes[5];
    BE_TokenData tokenData[5];
    
    I(0, "var");
    I(1, "i");
    P(2, "=");
    I(3, "Foo_Bar");
    P(4, ";");
    
    BE_EnumMemberList enumMembers[2];
    enumMembers[0].name = BE_StrLiteral("Bar_Foo");
    enumMembers[0].value = 1;
    enumMembers[0].prev = enumMembers + 1;
    
    enumMembers[1].name = BE_StrLiteral("Foo_Bar");
    enumMembers[1].value = 2;
    enumMembers[1].prev = 0;
    
    BE_EnumDeclaration enumDeclaration;
    BE_ClearStructToZero(enumDeclaration);
    
    enumDeclaration.name = BE_StrLiteral("Foo");
    enumDeclaration.isAnonymous = 1;
    enumDeclaration.memberCount = BE_ArrayCount(enumMembers);
    enumDeclaration.members = &enumMembers[0];
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    AddEnum(&parser, &enumDeclaration);
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_VariableDeclaration);
}

Test(Parser_EnumRead_Qualified)
{
    // NOTE(Jens): enum Foo { Foo, Bar }; ... var i = Foo.Bar;
    
    BE_String tokens[7];
    BE_TokenType tokenTypes[7];
    BE_TokenData tokenData[7];
    
    I(0, "var");
    I(1, "i");
    P(2, "=");
    I(3, "Foo");
    P(4, ".");
    I(5, "Bar");
    P(6, ";");
    
    BE_EnumMemberList enumMembers[2];
    enumMembers[0].name = BE_StrLiteral("Foo");
    enumMembers[0].value = 1;
    enumMembers[0].prev = enumMembers + 1;
    
    enumMembers[1].name = BE_StrLiteral("Bar");
    enumMembers[1].value = 2;
    enumMembers[1].prev = 0;
    
    BE_EnumDeclaration enumDeclaration;
    BE_ClearStructToZero(enumDeclaration);
    
    enumDeclaration.name = BE_StrLiteral("Foo");
    enumDeclaration.memberCount = BE_ArrayCount(enumMembers);
    enumDeclaration.members = &enumMembers[0];
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    AddEnum(&parser, &enumDeclaration);
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_VariableDeclaration);
}

Test(Parser_size_of_file)
{
    // NOTE(Jens): var i = size_of_file;
    
    BE_String tokens[5];
    BE_TokenType tokenTypes[5];
    BE_TokenData tokenData[5];
    
    I(0, "var");
    I(1, "i");
    P(2, "=");
    I(3, "size_of_file");
    P(4, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_VariableDeclaration);
}

Test(Parser_has_type)
{
    // NOTE(Jens): struct(MyType, 1) Foo { ... }; ... var i = has_type(MyType, 1);
    
    BE_String tokens[10];
    BE_TokenType tokenTypes[10];
    BE_TokenData tokenData[10];
    
    I(0, "var");
    I(1, "i");
    P(2, "=");
    I(3, "has_type");
    P(4, "(");
    I(5, "MyType");
    P(6, ",");
    N(7, 1);
    P(8, ")");
    P(9, ";");
    
    BE_StructHeader structHeader;
    BE_ClearStructToZero(structHeader);
    
    structHeader.name = BE_StrLiteral("Foo");
    structHeader.category = BE_StrLiteral("MyType");
    structHeader.categoryNumber = 1;
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_VariableDeclaration);
}

Test(Parser_sizeof)
{
    // NOTE(Jens): struct Foo { ... }; ... var i = sizeof(Foo);
    
    BE_String tokens[8];
    BE_TokenType tokenTypes[8];
    BE_TokenData tokenData[8];
    
    I(0, "var");
    I(1, "i");
    P(2, "=");
    I(3, "sizeof");
    P(4, "(");
    I(5, "Foo");
    P(6, ")");
    P(7, ";");
    
    BE_StructHeader structHeader;
    BE_ClearStructToZero(structHeader);
    
    structHeader.name = BE_StrLiteral("Foo");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_VariableDeclaration);
}

Test(Parser_sizeof_AddressExpression)
{
    // NOTE(Jens): struct Foo { ... }; ... var i = sizeof(@(123) Foo);
    
    BE_String tokens[12];
    BE_TokenType tokenTypes[12];
    BE_TokenData tokenData[12];
    
    I( 0, "var");
    I( 1, "i");
    P( 2, "=");
    I( 3, "sizeof");
    P( 4, "(");
    P( 5, "@");
    P( 6, "(");
    N( 7, 123);
    P( 8, ")");
    I( 9, "Foo");
    P(10, ")");
    P(11, ";");
    
    BE_StructHeader structHeader;
    BE_ClearStructToZero(structHeader);
    
    structHeader.name = BE_StrLiteral("Foo");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_VariableDeclaration);
}

Test(Parser_load_Signed)
{
    // NOTE(Jens): var i = load(s(4));
    
    BE_String tokens[11];
    BE_TokenType tokenTypes[11];
    BE_TokenData tokenData[11];
    
    I( 0, "var");
    I( 1, "i");
    P( 2, "=");
    I( 3, "load");
    P( 4, "(");
    I( 5, "s");
    P( 6, "(");
    N( 7, 4);
    P( 8, ")");
    P( 9, ")");
    P(10, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_VariableDeclaration);
}

Test(Parser_load_Unsigned)
{
    // NOTE(Jens): var i = load(u(4));
    
    BE_String tokens[11];
    BE_TokenType tokenTypes[11];
    BE_TokenData tokenData[11];
    
    I( 0, "var");
    I( 1, "i");
    P( 2, "=");
    I( 3, "load");
    P( 4, "(");
    I( 5, "u");
    P( 6, "(");
    N( 7, 4);
    P( 8, ")");
    P( 9, ")");
    P(10, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_VariableDeclaration);
}

Test(Parser_load_AddressExpression)
{
    // NOTE(Jens): var i = load(@(123) u(4));
    
    BE_String tokens[15];
    BE_TokenType tokenTypes[15];
    BE_TokenData tokenData[15];
    
    I( 0, "var");
    I( 1, "i");
    P( 2, "=");
    I( 3, "load");
    P( 4, "(");
    P( 5, "@");
    P( 6, "(");
    N( 7, 123);
    P( 8, ")");
    I( 9, "u");
    P(10, "(");
    N(11, 4);
    P(12, ")");
    P(13, ")");
    P(14, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_VariableDeclaration);
}

Test(Parser_load_Nested)
{
    // NOTE(Jens): var i = load(@(load(s(2))) u(load(s(1))));
    
    BE_String tokens[27];
    BE_TokenType tokenTypes[27];
    BE_TokenData tokenData[27];
    
    I( 0, "var");
    I( 1, "i");
    P( 2, "=");
    I( 3, "load");
    P( 4, "(");
    P( 5, "@");
    P( 6, "(");
    I( 7, "load");
    P( 8, "(");
    I( 9, "s");
    P(10, "(");
    N(11, 2);
    P(12, ")");
    P(13, ")");
    P(14, ")");
    I(15, "u");
    P(16, "(");
    I(17, "load");
    P(18, "(");
    I(19, "s");
    P(20, "(");
    N(21, 1);
    P(22, ")");
    P(23, ")");
    P(24, ")");
    P(25, ")");
    P(26, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_VariableDeclaration);
}

Test(Parser_ParameterAccess)
{
    // NOTE(Jens): <<< struct Foo(var x) { ... >>> var i = x;
    
    BE_String tokens[5];
    BE_TokenType tokenTypes[5];
    BE_TokenData tokenData[5];
    
    I(0, "var");
    I(1, "i");
    P(2, "=");
    I(3, "x");
    P(4, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    
    BE_StructHeader structHeader;
    BE_ClearStructToZero(structHeader);
    
    structHeader.name = BE_StrLiteral("Foo");
    BE_String parameter = BE_StrLiteral("x");
    
    parser.parsingStruct = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 1, &parameter);
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_VariableDeclaration);
}

Test(Parser_MemberAccess)
{
    // NOTE(Jens): <<< struct Foo { <scalar-type> member; ... >>> var i = member;
    
    BE_String tokens[5];
    BE_TokenType tokenTypes[5];
    BE_TokenData tokenData[5];
    
    I(0, "var");
    I(1, "i");
    P(2, "=");
    I(3, "member");
    P(4, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    
    BE_StructHeader structHeader;
    BE_ClearStructToZero(structHeader);
    
    structHeader.name = BE_StrLiteral("Foo");
    
    parser.parsingStruct = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    
    BE_MemberDeclaration member;
    BE_ClearStructToZero(member); // NOTE(Jens): Zero-initialization is scalar type.
    
    member.name = BE_StrLiteral("member");
    BE_AddStructMember(parser.allocator, parser.parsingStruct, &member);
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_VariableDeclaration);
}

Test(Parser_MemberAccess_Array)
{
    // NOTE(Jens): <<< struct Foo { <scalar-type> member[10]; ... >>> var i = member[2];
    
    BE_String tokens[8];
    BE_TokenType tokenTypes[8];
    BE_TokenData tokenData[8];
    
    I(0, "var");
    I(1, "i");
    P(2, "=");
    I(3, "member");
    P(4, "[");
    N(5, 2);
    P(6, "]");
    P(7, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    
    BE_StructHeader structHeader;
    BE_ClearStructToZero(structHeader);
    
    structHeader.name = BE_StrLiteral("Foo");
    
    parser.parsingStruct = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    
    BE_MemberDeclaration member;
    BE_ClearStructToZero(member); // NOTE(Jens): Zero-initialization is scalar type.
    
    member.name = BE_StrLiteral("member");
    BE_InitExpression(parser.allocator, &parser.parsingExpressions.expressions, &member.arrayCount, 0);
    BE_PushConstantInteger(&member.arrayCount, 10);
    member.isArray = 1;
    BE_AddStructMember(parser.allocator, parser.parsingStruct, &member);
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_VariableDeclaration);
}

Test(Parser_MemberAccess_NestedStruct)
{
    // NOTE(Jens): <<< struct Bar { <scalar-type> bar; }; struct Foo { Bar member; ... >>> var i = member.bar;
    
    BE_String tokens[7];
    BE_TokenType tokenTypes[7];
    BE_TokenData tokenData[7];
    
    I(0, "var");
    I(1, "i");
    P(2, "=");
    I(3, "member");
    P(4, ".");
    I(5, "bar");
    P(6, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    
    BE_ParsedStructNode* structBar;
    BE_ParsedStructNode* structFoo;
    
    {
        BE_StructHeader structHeader;
        BE_ClearStructToZero(structHeader);
        structHeader.name = BE_StrLiteral("Bar");
        structBar = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
        
        BE_MemberDeclaration member;
        BE_ClearStructToZero(member); // NOTE(Jens): Zero-initialization is scalar type.
        member.name = BE_StrLiteral("bar");
        BE_AddStructMember(parser.allocator, structBar, &member);
    }
    
    {
        BE_StructHeader structHeader;
        BE_ClearStructToZero(structHeader);
        structHeader.name = BE_StrLiteral("Foo");
        structFoo = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
        
        BE_MemberDeclaration member;
        BE_ClearStructToZero(member);
        member.name = BE_StrLiteral("member");
        member.type.variant = BE_TypeVariant_Struct;
        member.type.structVariant.type = structBar;
        BE_AddStructMember(parser.allocator, structFoo, &member);
    }
    
    parser.parsingStruct = structFoo;
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_VariableDeclaration);
}

Test(Parser_MemberAccess_NestedStruct_Array_Scalar)
{
    // NOTE(Jens): <<< struct Bar { <scalar-type> bar; }; struct Foo { Bar member[10]; ... >>> var i = member[2].bar;
    
    BE_String tokens[10];
    BE_TokenType tokenTypes[10];
    BE_TokenData tokenData[10];
    
    I(0, "var");
    I(1, "i");
    P(2, "=");
    I(3, "member");
    P(4, "[");
    N(5, 2);
    P(6, "]");
    P(7, ".");
    I(8, "bar");
    P(9, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    
    BE_ParsedStructNode* structBar;
    BE_ParsedStructNode* structFoo;
    
    {
        BE_StructHeader structHeader;
        BE_ClearStructToZero(structHeader);
        structHeader.name = BE_StrLiteral("Bar");
        structBar = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
        
        BE_MemberDeclaration member;
        BE_ClearStructToZero(member); // NOTE(Jens): Zero-initialization is scalar type.
        member.name = BE_StrLiteral("bar");
        BE_AddStructMember(parser.allocator, structBar, &member);
    }
    
    {
        BE_StructHeader structHeader;
        BE_ClearStructToZero(structHeader);
        structHeader.name = BE_StrLiteral("Foo");
        structFoo = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
        
        BE_MemberDeclaration member;
        BE_ClearStructToZero(member);
        member.name = BE_StrLiteral("member");
        member.type.variant = BE_TypeVariant_Struct;
        member.type.structVariant.type = structBar;
        BE_InitExpression(parser.allocator, &parser.parsingExpressions.expressions, &member.arrayCount, 0);
        BE_PushConstantInteger(&member.arrayCount, 10);
        member.isArray = 1;
        BE_AddStructMember(parser.allocator, structFoo, &member);
    }
    
    parser.parsingStruct = structFoo;
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_VariableDeclaration);
}

Test(Parser_MemberAccess_NestedStruct_Array_Array)
{
    // NOTE(Jens): <<< struct Bar { <scalar-type> bar[10]; }; struct Foo { Bar member[10]; ... >>> var i = member[2].bar[2];
    
    BE_String tokens[13];
    BE_TokenType tokenTypes[13];
    BE_TokenData tokenData[13];
    
    I( 0, "var");
    I( 1, "i");
    P( 2, "=");
    I( 3, "member");
    P( 4, "[");
    N( 5, 2);
    P( 6, "]");
    P( 7, ".");
    I( 8, "bar");
    P( 9, "[");
    N(10, 2);
    P(11, "]");
    P(12, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    
    BE_ParsedStructNode* structBar;
    BE_ParsedStructNode* structFoo;
    
    {
        BE_StructHeader structHeader;
        BE_ClearStructToZero(structHeader);
        structHeader.name = BE_StrLiteral("Bar");
        structBar = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
        
        BE_MemberDeclaration member;
        BE_ClearStructToZero(member); // NOTE(Jens): Zero-initialization is scalar type.
        member.name = BE_StrLiteral("bar");
        BE_InitExpression(parser.allocator, &parser.parsingExpressions.expressions, &member.arrayCount, 0);
        BE_PushConstantInteger(&member.arrayCount, 10);
        member.isArray = 1;
        BE_AddStructMember(parser.allocator, structBar, &member);
    }
    
    {
        BE_StructHeader structHeader;
        BE_ClearStructToZero(structHeader);
        structHeader.name = BE_StrLiteral("Foo");
        structFoo = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
        
        BE_MemberDeclaration member;
        BE_ClearStructToZero(member);
        member.name = BE_StrLiteral("member");
        member.type.variant = BE_TypeVariant_Struct;
        member.type.structVariant.type = structBar;
        BE_InitExpression(parser.allocator, &parser.parsingExpressions.expressions, &member.arrayCount, 0);
        BE_PushConstantInteger(&member.arrayCount, 10);
        member.isArray = 1;
        BE_AddStructMember(parser.allocator, structFoo, &member);
    }
    
    parser.parsingStruct = structFoo;
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_VariableDeclaration);
}

Test(Parser_Member_Scalar_u)
{
    // NOTE(Jens): <<< struct Foo { ... >>> u(4) foo;
    
    BE_String tokens[6];
    BE_TokenType tokenTypes[6];
    BE_TokenData tokenData[6];
    
    I(0, "u");
    P(1, "(");
    N(2, 4);
    P(3, ")");
    I(4, "foo");
    P(5, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    
    BE_StructHeader structHeader;
    BE_ClearStructToZero(structHeader);
    
    structHeader.name = BE_StrLiteral("Foo");
    BE_ParsedStructNode* parsedStruct = parser.parsingStruct = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_MemberDeclaration);
    TestAssert(statement.memberDeclaration.type.variant == BE_TypeVariant_Scalar);
    TestAssert(statement.memberDeclaration.type.scalarVariant.baseType == BE_ScalarBaseType_u);
    TestAssert(parsedStruct->memberCount == 1);
}

Test(Parser_Member_Scalar_big_u)
{
    // NOTE(Jens): <<< struct Foo { ... >>> u(4, big) foo;
    
    BE_String tokens[8];
    BE_TokenType tokenTypes[8];
    BE_TokenData tokenData[8];
    
    I(0, "u");
    P(1, "(");
    N(2, 4);
    P(3, ",");
    I(4, "big");
    P(5, ")");
    I(6, "foo");
    P(7, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    
    BE_StructHeader structHeader;
    BE_ClearStructToZero(structHeader);
    
    structHeader.name = BE_StrLiteral("Foo");
    BE_ParsedStructNode* parsedStruct = parser.parsingStruct = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_MemberDeclaration);
    TestAssert(statement.memberDeclaration.type.variant == BE_TypeVariant_Scalar);
    TestAssert(statement.memberDeclaration.type.scalarVariant.baseType == BE_ScalarBaseType_u);
    TestAssert(statement.memberDeclaration.type.scalarVariant.byteOrder == BE_ScalarByteOrder_big);
    TestAssert(parsedStruct->memberCount == 1);
}

Test(Parser_Member_Scalar_default_big_u)
{
    // NOTE(Jens): <<< #default(byteorder = big); struct Foo { ... >>> u(4) foo;
    
    BE_String tokens[6];
    BE_TokenType tokenTypes[6];
    BE_TokenData tokenData[6];
    
    I(0, "u");
    P(1, "(");
    N(2, 4);
    P(3, ")");
    I(4, "foo");
    P(5, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    
    BE_StructHeader structHeader;
    BE_ClearStructToZero(structHeader);
    
    parser.defaultByteOrder = BE_ScalarByteOrder_big;
    
    structHeader.name = BE_StrLiteral("Foo");
    BE_ParsedStructNode* parsedStruct = parser.parsingStruct = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_MemberDeclaration);
    TestAssert(statement.memberDeclaration.type.variant == BE_TypeVariant_Scalar);
    TestAssert(statement.memberDeclaration.type.scalarVariant.baseType == BE_ScalarBaseType_u);
    TestAssert(statement.memberDeclaration.type.scalarVariant.byteOrder == BE_ScalarByteOrder_big);
    TestAssert(parsedStruct->memberCount == 1);
}

Test(Parser_Member_Scalar_s)
{
    // NOTE(Jens): <<< struct Foo { ... >>> s(4) foo;
    
    BE_String tokens[6];
    BE_TokenType tokenTypes[6];
    BE_TokenData tokenData[6];
    
    I(0, "s");
    P(1, "(");
    N(2, 4);
    P(3, ")");
    I(4, "foo");
    P(5, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    
    BE_StructHeader structHeader;
    BE_ClearStructToZero(structHeader);
    
    structHeader.name = BE_StrLiteral("Foo");
    parser.parsingStruct = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_MemberDeclaration);
    TestAssert(statement.memberDeclaration.type.variant == BE_TypeVariant_Scalar);
    TestAssert(statement.memberDeclaration.type.scalarVariant.baseType == BE_ScalarBaseType_s);
}

Test(Parser_Member_Scalar_f)
{
    // NOTE(Jens): <<< struct Foo { ... >>> f(4) foo;
    
    BE_String tokens[6];
    BE_TokenType tokenTypes[6];
    BE_TokenData tokenData[6];
    
    I(0, "f");
    P(1, "(");
    N(2, 4);
    P(3, ")");
    I(4, "foo");
    P(5, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    
    BE_StructHeader structHeader;
    BE_ClearStructToZero(structHeader);
    
    structHeader.name = BE_StrLiteral("Foo");
    parser.parsingStruct = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_MemberDeclaration);
    TestAssert(statement.memberDeclaration.type.variant == BE_TypeVariant_Scalar);
    TestAssert(statement.memberDeclaration.type.scalarVariant.baseType == BE_ScalarBaseType_f);
}

Test(Parser_Member_Scalar_raw)
{
    // NOTE(Jens): <<< struct Foo { ... >>> raw(4) foo;
    
    BE_String tokens[6];
    BE_TokenType tokenTypes[6];
    BE_TokenData tokenData[6];
    
    I(0, "raw");
    P(1, "(");
    N(2, 4);
    P(3, ")");
    I(4, "foo");
    P(5, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    
    BE_StructHeader structHeader;
    BE_ClearStructToZero(structHeader);
    
    structHeader.name = BE_StrLiteral("Foo");
    parser.parsingStruct = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_MemberDeclaration);
    TestAssert(statement.memberDeclaration.type.variant == BE_TypeVariant_Scalar);
    TestAssert(statement.memberDeclaration.type.scalarVariant.baseType == BE_ScalarBaseType_raw);
}

Test(Parser_Member_Scalar_enum)
{
    // NOTE(Jens): <<< enum Bar { ... }; struct Foo { ... >>> Bar(4) foo;
    
    BE_String tokens[6];
    BE_TokenType tokenTypes[6];
    BE_TokenData tokenData[6];
    
    I(0, "Bar");
    P(1, "(");
    N(2, 4);
    P(3, ")");
    I(4, "foo");
    P(5, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    
    BE_StructHeader structHeader;
    BE_ClearStructToZero(structHeader);
    
    BE_EnumDeclaration enumDeclaration;
    BE_ClearStructToZero(enumDeclaration);
    enumDeclaration.name = BE_StrLiteral("Bar");
    
    AddEnum(&parser, &enumDeclaration);
    
    structHeader.name = BE_StrLiteral("Foo");
    parser.parsingStruct = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_MemberDeclaration);
    TestAssert(statement.memberDeclaration.type.variant == BE_TypeVariant_Scalar);
    TestAssert(statement.memberDeclaration.type.scalarVariant.baseType == BE_ScalarBaseType_s);
    TestAssert(statement.memberDeclaration.type.scalarVariant.enumType);
}

Test(Parser_Member_Scalar_enum_NoSize)
{
    // NOTE(Jens): <<< enum Bar(4) { ... }; struct Foo { ... >>> Bar foo;
    
    BE_String tokens[3];
    BE_TokenType tokenTypes[3];
    BE_TokenData tokenData[3];
    
    I(0, "Bar");
    I(1, "foo");
    P(2, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    
    BE_StructHeader structHeader;
    BE_ClearStructToZero(structHeader);
    
    BE_EnumDeclaration enumDeclaration;
    BE_ClearStructToZero(enumDeclaration);
    enumDeclaration.name = BE_StrLiteral("Bar");
    enumDeclaration.size = 4;
    
    AddEnum(&parser, &enumDeclaration);
    
    structHeader.name = BE_StrLiteral("Foo");
    parser.parsingStruct = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_MemberDeclaration);
    TestAssert(statement.memberDeclaration.type.variant == BE_TypeVariant_Scalar);
    TestAssert(statement.memberDeclaration.type.scalarVariant.baseType == BE_ScalarBaseType_s);
    TestAssert(statement.memberDeclaration.type.scalarVariant.enumType);
}

Test(Parser_Member_Scalar_enum_OverwriteSize)
{
    // NOTE(Jens): <<< enum Bar(4) { ... }; struct Foo { ... >>> Bar(8) foo;
    
    BE_String tokens[6];
    BE_TokenType tokenTypes[6];
    BE_TokenData tokenData[6];
    
    I(0, "Bar");
    P(1, "(");
    N(2, 8);
    P(3, ")");
    I(4, "foo");
    P(5, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    
    BE_StructHeader structHeader;
    BE_ClearStructToZero(structHeader);
    
    BE_EnumDeclaration enumDeclaration;
    BE_ClearStructToZero(enumDeclaration);
    enumDeclaration.name = BE_StrLiteral("Bar");
    enumDeclaration.size = 4;
    
    AddEnum(&parser, &enumDeclaration);
    
    structHeader.name = BE_StrLiteral("Foo");
    parser.parsingStruct = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_MemberDeclaration);
    TestAssert(statement.memberDeclaration.type.variant == BE_TypeVariant_Scalar);
    TestAssert(statement.memberDeclaration.type.scalarVariant.baseType == BE_ScalarBaseType_s);
    TestAssert(statement.memberDeclaration.type.scalarVariant.enumType);
}

Test(Parser_Member_Scalar_string)
{
    // NOTE(Jens): <<< struct Foo { ... >>> string(4) foo;
    
    BE_String tokens[6];
    BE_TokenType tokenTypes[6];
    BE_TokenData tokenData[6];
    
    I(0, "string");
    P(1, "(");
    N(2, 4);
    P(3, ")");
    I(4, "foo");
    P(5, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    
    BE_StructHeader structHeader;
    BE_ClearStructToZero(structHeader);
    
    structHeader.name = BE_StrLiteral("Foo");
    parser.parsingStruct = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_MemberDeclaration);
    TestAssert(statement.memberDeclaration.type.variant == BE_TypeVariant_Scalar);
    TestAssert(statement.memberDeclaration.type.scalarVariant.baseType == BE_ScalarBaseType_string);
}

Test(Parser_Member_Scalar_hidden)
{
    // NOTE(Jens): <<< struct Foo { ... >>> hidden(4) foo;
    
    BE_String tokens[6];
    BE_TokenType tokenTypes[6];
    BE_TokenData tokenData[6];
    
    I(0, "hidden");
    P(1, "(");
    N(2, 4);
    P(3, ")");
    I(4, "foo");
    P(5, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    
    BE_StructHeader structHeader;
    BE_ClearStructToZero(structHeader);
    
    structHeader.name = BE_StrLiteral("Foo");
    parser.parsingStruct = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_MemberDeclaration);
    TestAssert(statement.memberDeclaration.type.variant == BE_TypeVariant_Scalar);
    TestAssert(statement.memberDeclaration.type.scalarVariant.baseType == BE_ScalarBaseType_hidden);
}

Test(Parser_Member_Struct)
{
    // NOTE(Jens): <<< struct Bar { ... }; ... struct Foo { ... >>> Bar foo;
    
    BE_String tokens[3];
    BE_TokenType tokenTypes[3];
    BE_TokenData tokenData[3];
    
    I(0, "Bar");
    I(1, "foo");
    P(2, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    BE_ParsedStructNode* parsedStruct = 0;
    
    {
        BE_StructHeader structHeader;
        BE_ClearStructToZero(structHeader);
        
        structHeader.name = BE_StrLiteral("Foo");
        parsedStruct = parser.parsingStruct = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    }
    
    {
        BE_StructHeader structHeader;
        BE_ClearStructToZero(structHeader);
        
        structHeader.name = BE_StrLiteral("Bar");
        AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    }
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_MemberDeclaration);
    TestAssert(statement.memberDeclaration.type.variant == BE_TypeVariant_Struct);
    TestAssert(BE_StringsAreEqual(statement.memberDeclaration.type.structVariant.type->name, tokens[0]));
    TestAssert(parsedStruct->memberCount == 1);
}

Test(Parser_Member_Struct_Internal)
{
    // NOTE(Jens): <<< struct Bar { ... }; ... struct Foo { ... >>> internal Bar foo;
    
    BE_String tokens[4];
    BE_TokenType tokenTypes[4];
    BE_TokenData tokenData[4];
    
    I(0, "internal");
    I(1, "Bar");
    I(2, "foo");
    P(3, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    BE_ParsedStructNode* parsedStruct = 0;
    
    {
        BE_StructHeader structHeader;
        BE_ClearStructToZero(structHeader);
        
        structHeader.name = BE_StrLiteral("Foo");
        parsedStruct = parser.parsingStruct = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    }
    
    {
        BE_StructHeader structHeader;
        BE_ClearStructToZero(structHeader);
        
        structHeader.name = BE_StrLiteral("Bar");
        AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    }
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_MemberDeclaration);
    TestAssert(statement.memberDeclaration.type.variant == BE_TypeVariant_Struct);
    TestAssert(statement.memberDeclaration.isInternal);
    TestAssert(BE_StringsAreEqual(statement.memberDeclaration.type.structVariant.type->name, tokens[1]));
    TestAssert(parsedStruct->memberCount == 1);
}

Test(Parser_Member_hidden)
{
    // NOTE(Jens): <<< struct Bar { ... }; ... struct Foo { ... >>> hidden Bar foo;
    
    BE_String tokens[4];
    BE_TokenType tokenTypes[4];
    BE_TokenData tokenData[4];
    
    I(0, "hidden");
    I(1, "Bar");
    I(2, "foo");
    P(3, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    
    {
        BE_StructHeader structHeader;
        BE_ClearStructToZero(structHeader);
        
        structHeader.name = BE_StrLiteral("Foo");
        parser.parsingStruct = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    }
    
    {
        BE_StructHeader structHeader;
        BE_ClearStructToZero(structHeader);
        
        structHeader.name = BE_StrLiteral("Bar");
        AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    }
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_MemberDeclaration);
    TestAssert(statement.memberDeclaration.type.variant == BE_TypeVariant_Struct);
    TestAssert(statement.memberDeclaration.isHidden);
    TestAssert(BE_StringsAreEqual(statement.memberDeclaration.type.structVariant.type->name, tokens[1]));
}

Test(Parser_Member_Struct_Arguments)
{
    // NOTE(Jens): <<< struct Bar(var x, var y) { ... }; ... struct Foo { ... >>> Bar(1, 2) foo;
    
    BE_String tokens[8];
    BE_TokenType tokenTypes[8];
    BE_TokenData tokenData[8];
    
    I(0, "Bar");
    P(1, "(");
    N(2, 1);
    P(3, ",");
    N(4, 2);
    P(5, ")");
    I(6, "foo");
    P(7, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    
    {
        BE_StructHeader structHeader;
        BE_ClearStructToZero(structHeader);
        
        structHeader.name = BE_StrLiteral("Foo");
        parser.parsingStruct = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    }
    
    BE_String paramNames[2];
    paramNames[0] = BE_StrLiteral("x");
    paramNames[1] = BE_StrLiteral("y");
    
    {
        BE_StructHeader structHeader;
        BE_ClearStructToZero(structHeader);
        
        structHeader.name = BE_StrLiteral("Bar");
        AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, BE_ArrayCount(paramNames), paramNames);
    }
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_MemberDeclaration);
    TestAssert(statement.memberDeclaration.type.variant == BE_TypeVariant_Struct);
    TestAssert(BE_StringsAreEqual(statement.memberDeclaration.type.structVariant.type->name, tokens[0]));
}

Test(Parser_Member_Category)
{
    // NOTE(Jens): <<< struct(Cat, 12) Bar { ... }; ... struct Foo { ... >>> Cat[12] foo;
    
    BE_String tokens[6];
    BE_TokenType tokenTypes[6];
    BE_TokenData tokenData[6];
    
    I(0, "Cat");
    P(1, "[");
    N(2, 12);
    P(3, "]");
    I(4, "foo");
    P(5, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    
    {
        BE_StructHeader structHeader;
        BE_ClearStructToZero(structHeader);
        
        structHeader.name = BE_StrLiteral("Foo");
        parser.parsingStruct = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    }
    
    {
        BE_StructHeader structHeader;
        BE_ClearStructToZero(structHeader);
        
        structHeader.name = BE_StrLiteral("Bar");
        structHeader.category = BE_StrLiteral("Cat");
        structHeader.categoryNumber = 12;
        AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    }
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_MemberDeclaration);
    TestAssert(statement.memberDeclaration.type.variant == BE_TypeVariant_Category);
    TestAssert(BE_StringsAreEqual(statement.memberDeclaration.type.categoryVariant.category->name, tokens[0]));
}

Test(Parser_Member_CategoryOrStruct_Struct)
{
    // NOTE(Jens): <<< struct(Cat, 12) Bar { ... }; struct Cat { ... }; ... struct Foo { ... >>> Cat foo;
    
    BE_String tokens[3];
    BE_TokenType tokenTypes[3];
    BE_TokenData tokenData[3];
    
    I(0, "Cat");
    I(1, "foo");
    P(2, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    
    {
        BE_StructHeader structHeader;
        BE_ClearStructToZero(structHeader);
        
        structHeader.name = BE_StrLiteral("Cat");
        AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    }
    
    {
        BE_StructHeader structHeader;
        BE_ClearStructToZero(structHeader);
        
        structHeader.name = BE_StrLiteral("Bar");
        structHeader.category = BE_StrLiteral("Cat");
        structHeader.categoryNumber = 12;
        AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    }
    
    {
        BE_StructHeader structHeader;
        BE_ClearStructToZero(structHeader);
        
        structHeader.name = BE_StrLiteral("Foo");
        parser.parsingStruct = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    }
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_MemberDeclaration);
    TestAssert(statement.memberDeclaration.type.variant == BE_TypeVariant_Struct);
    TestAssert(BE_StringsAreEqual(statement.memberDeclaration.type.structVariant.type->name, tokens[0]));
}

Test(Parser_Member_CategoryOrStruct_Category)
{
    // NOTE(Jens): <<< struct(Cat, 12) Bar { ... }; struct Cat { ... }; ... struct Foo { ... >>> Cat[12] foo;
    
    BE_String tokens[6];
    BE_TokenType tokenTypes[6];
    BE_TokenData tokenData[6];
    
    I(0, "Cat");
    P(1, "[");
    N(2, 12);
    P(3, "]");
    I(4, "foo");
    P(5, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    
    {
        BE_StructHeader structHeader;
        BE_ClearStructToZero(structHeader);
        
        structHeader.name = BE_StrLiteral("Cat");
        AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    }
    
    {
        BE_StructHeader structHeader;
        BE_ClearStructToZero(structHeader);
        
        structHeader.name = BE_StrLiteral("Bar");
        structHeader.category = BE_StrLiteral("Cat");
        structHeader.categoryNumber = 12;
        AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    }
    
    {
        BE_StructHeader structHeader;
        BE_ClearStructToZero(structHeader);
        
        structHeader.name = BE_StrLiteral("Foo");
        parser.parsingStruct = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    }
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_MemberDeclaration);
    TestAssert(statement.memberDeclaration.type.variant == BE_TypeVariant_Category);
    TestAssert(BE_StringsAreEqual(statement.memberDeclaration.type.categoryVariant.category->name, tokens[0]));
}

Test(Parser_Member_Category_Arguments)
{
    // NOTE(Jens): <<< struct(Cat, 12) Bar(var x, var y) { ... }; ... struct Foo { ... >>> Cat[12](1, 2) foo;
    
    BE_String tokens[11];
    BE_TokenType tokenTypes[11];
    BE_TokenData tokenData[11];
    
    I( 0, "Cat");
    P( 1, "[");
    N( 2, 12);
    P( 3, "]");
    P( 4, "(");
    N( 5, 1);
    P( 6, ",");
    N( 7, 2);
    P( 8, ")");
    I( 9, "foo");
    P(10, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    
    {
        BE_StructHeader structHeader;
        BE_ClearStructToZero(structHeader);
        
        structHeader.name = BE_StrLiteral("Foo");
        parser.parsingStruct = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    }
    
    BE_String paramNames[2];
    paramNames[0] = BE_StrLiteral("x");
    paramNames[1] = BE_StrLiteral("y");
    
    {
        BE_StructHeader structHeader;
        BE_ClearStructToZero(structHeader);
        
        structHeader.name = BE_StrLiteral("Bar");
        structHeader.category = BE_StrLiteral("Cat");
        structHeader.categoryNumber = 12;
        AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, BE_ArrayCount(paramNames), paramNames);
    }
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_MemberDeclaration);
    TestAssert(statement.memberDeclaration.type.variant == BE_TypeVariant_Category);
    TestAssert(BE_StringsAreEqual(statement.memberDeclaration.type.categoryVariant.category->name, tokens[0]));
}

Test(Parser_Member_Typedef)
{
    // NOTE(Jens): <<< typedef u(4) U32; ... struct Foo { ... >>> U32 foo;
    
    BE_String tokens[3];
    BE_TokenType tokenTypes[3];
    BE_TokenData tokenData[3];
    
    I(0, "U32");
    I(1, "foo");
    P(2, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    
    {
        BE_StructHeader structHeader;
        BE_ClearStructToZero(structHeader);
        
        structHeader.name = BE_StrLiteral("Foo");
        parser.parsingStruct = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    }
    
    {
        BE_TypedefDeclaration declaration;
        BE_ClearStructToZero(declaration);
        
        declaration.name = BE_StrLiteral("U32");
        // NOTE(Jens): Real implementation would have the other fields populated as well.
        
        BE_AddTypedef(parser.allocator, &parser.typedefs, declaration);
    }
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_MemberDeclaration);
    TestAssert(statement.memberDeclaration.type.variant == BE_TypeVariant_Typedef);
}

Test(Parser_Member_Array)
{
    // NOTE(Jens): <<< struct Foo { ... >>> u(4) foo[8];
    
    BE_String tokens[9];
    BE_TokenType tokenTypes[9];
    BE_TokenData tokenData[9];
    
    I(0, "u");
    P(1, "(");
    N(2, 4);
    P(3, ")");
    I(4, "foo");
    P(5, "[");
    N(6, 8);
    P(7, "]");
    P(8, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    
    BE_StructHeader structHeader;
    BE_ClearStructToZero(structHeader);
    
    structHeader.name = BE_StrLiteral("Foo");
    parser.parsingStruct = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_MemberDeclaration);
    TestAssert(statement.memberDeclaration.type.variant == BE_TypeVariant_Scalar);
    TestAssert(BE_ExpressionIsValid(&statement.memberDeclaration.arrayCount));
}

Test(Parser_Member_Address)
{
    // NOTE(Jens): <<< struct Foo { ... >>> @(16) u(4) foo;
    
    BE_String tokens[10];
    BE_TokenType tokenTypes[10];
    BE_TokenData tokenData[10];
    
    P(0, "@");
    P(1, "(");
    N(2, 16);
    P(3, ")");
    I(4, "u");
    P(5, "(");
    N(6, 4);
    P(7, ")");
    I(8, "foo");
    P(9, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    
    BE_StructHeader structHeader;
    BE_ClearStructToZero(structHeader);
    
    structHeader.name = BE_StrLiteral("Foo");
    parser.parsingStruct = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_MemberDeclaration);
    TestAssert(statement.memberDeclaration.type.variant == BE_TypeVariant_Scalar);
    TestAssert(BE_ExpressionIsValid(&statement.memberDeclaration.address));
    TestAssert(!BE_ExpressionIsValid(&statement.memberDeclaration.arrayCount));
}

Test(Parser_Member_ExternalAddress)
{
    // NOTE(Jens): <<< struct Foo { ... >>> @(external 16) u(4) foo;
    
    BE_String tokens[11];
    BE_TokenType tokenTypes[11];
    BE_TokenData tokenData[11];
    
    P( 0, "@");
    P( 1, "(");
    I( 2, "external");
    N( 3, 16);
    P( 4, ")");
    I( 5, "u");
    P( 6, "(");
    N( 7, 4);
    P( 8, ")");
    I( 9, "foo");
    P(10, ";");
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_ParserState parser = StartParsing();
    
    BE_StructHeader structHeader;
    BE_ClearStructToZero(structHeader);
    
    structHeader.name = BE_StrLiteral("Foo");
    parser.parsingStruct = AddParsedStruct(parser.allocator, &parser.structs, structHeader.name, structHeader.category, structHeader.categoryNumber, 0, 0);
    
    TestAssert(ParseSingleStatement(&parser, &statementType, &statement, BE_ArrayCount(tokens), tokens, tokenTypes, tokenData));
    TestAssert(statementType == BE_StatementType_MemberDeclaration);
    TestAssert(statement.memberDeclaration.type.variant == BE_TypeVariant_Scalar);
    TestAssert(statement.memberDeclaration.isExternal);
    TestAssert(BE_ExpressionIsValid(&statement.memberDeclaration.address));
    TestAssert(!BE_ExpressionIsValid(&statement.memberDeclaration.arrayCount));
}

typedef struct
{
    BE_InstructionAllocator alloc;
    BE_InstructionGenerator generator;
    BE_Expressions expressions;
} GenerateInstructionsTester;

static GenerateInstructionsTester StartGenerateInstructionsTest(void)
{
    GenerateInstructionsTester rVal;
    BE_ClearStructToZero(rVal);
    rVal.generator = BE_StartInstructionGeneration();
    return rVal;
}

static BEbool GenerateInstructionsForStatements(BE_InstructionList* out, GenerateInstructionsTester* tester, BEu32 statementCount, BE_StatementType* types, BE_Statement* statements, BEu32 expectedInstructionsCount, BE_InstructionType* expectedTypes)
{
    BEbool rVal = 1;
    BEu32 instructionIndex = 0;
    
    if (out)
    {
        BE_ClearStructToZero(*out);
    }
    
    for (BEu32 statementIndex = 0; rVal && statementIndex <= statementCount; ++statementIndex)
    {
        BE_Statement* statement;
        BE_StatementType type;
        
        if (statementIndex < statementCount)
        {
            statement = statements + statementIndex;
            type = types[statementIndex];
        }
        else
        {
            statement = 0;
            type = BE_StatementType_End;
        }
        
        BE_ArithmeticError error = BE_ArithmeticError_None;
        BE_InstructionList list = BE_FeedInstructionGenerator(&tester->alloc, &tester->expressions, &tester->generator, &error, type, statement);
        
        if (error)
        {
            rVal = 0;
            break;
        }
        
        for (BE_InstructionListNode* it = list.head; it; it = it->next)
        {
            if (instructionIndex >= expectedInstructionsCount || it->instruction.type != expectedTypes[instructionIndex++])
            {
                rVal = 0;
                break;
            }
        }
        
        if (out && list.head)
        {
            if (out->head)
            {
                out->tail->next = list.head;
                out->tail = list.tail;
            }
            else
            {
                *out = list;
            }
        }
    }
    
    if (instructionIndex != expectedInstructionsCount)
    {
        rVal = 0;
    }
    
    return rVal;
}

static BE_ArithmeticError GetErrorForStatements(GenerateInstructionsTester* tester, BEu32 statementCount, BE_StatementType* types, BE_Statement* statements)
{
    BE_ArithmeticError rVal = BE_ArithmeticError_None;
    
    for (BEu32 statementIndex = 0; !rVal && statementIndex <= statementCount; ++statementIndex)
    {
        BE_Statement* statement;
        BE_StatementType type;
        
        if (statementIndex < statementCount)
        {
            statement = statements + statementIndex;
            type = types[statementIndex];
        }
        else
        {
            statement = 0;
            type = BE_StatementType_End;
        }
        
        BE_FeedInstructionGenerator(&tester->alloc, &tester->expressions, &tester->generator, &rVal, type, statement);
    }
    
    return rVal;
}

static BEbool GenerateInstructionsForSingleStatement(BE_InstructionList* out, GenerateInstructionsTester* tester, BE_StatementType type, BE_Statement* statement, BEu32 expectedInstructionsCount, BE_InstructionType* expectedTypes)
{
    return GenerateInstructionsForStatements(out, tester, 1, &type, statement, expectedInstructionsCount, expectedTypes);
}

static BEu32 PushedStackEntryCount(GenerateInstructionsTester* tester)
{
    BEu32 reservedCount = BE_GlobalSymbol_Count;
    if (tester->generator.stackIndex)
    {
        reservedCount = BE_LocalSymbol_Count;
    }
    
    return tester->generator.stackEntryCount[tester->generator.stackIndex] - reservedCount;
}

#define GetInstruction_0(list) &(list).head->instruction
#define GetInstruction_1(list) &(list).head->next->instruction
#define GetInstruction_2(list) &(list).head->next->next->instruction
#define GetInstruction_3(list) &(list).head->next->next->next->instruction
#define GetInstruction_4(list) &(list).head->next->next->next->next->instruction
#define GetInstruction_5(list) &(list).head->next->next->next->next->next->instruction
#define GetInstruction_6(list) &(list).head->next->next->next->next->next->next->instruction
#define GetInstruction_7(list) &(list).head->next->next->next->next->next->next->next->instruction
#define GetInstruction_8(list) &(list).head->next->next->next->next->next->next->next->next->instruction
#define GetInstruction_9(list) &(list).head->next->next->next->next->next->next->next->next->next->instruction
#define GetInstruction_10(list) &(list).head->next->next->next->next->next->next->next->next->next->next->instruction
#define GetInstruction_11(list) &(list).head->next->next->next->next->next->next->next->next->next->next->next->instruction
#define GetInstruction_12(list) &(list).head->next->next->next->next->next->next->next->next->next->next->next->next->instruction
#define GetInstruction_13(list) &(list).head->next->next->next->next->next->next->next->next->next->next->next->next->next->instruction
#define GetInstruction_14(list) &(list).head->next->next->next->next->next->next->next->next->next->next->next->next->next->next->instruction
#define GetInstruction(list, i) GetInstruction_##i(list)

Test(InstructionGenerator_Print_Msg)
{
    // NOTE(Jens): print("Test");
    // 0: print :: "Test"
    
    BE_Statement statement;
    BE_ClearStructToZero(statement);
    
    statement.print.message = BE_StrLiteral("Test");
    
    BE_InstructionType expectedTypes[] = {
        BE_InstructionType_Print
    };
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForSingleStatement(&list, &tester, BE_StatementType_Print, &statement, BE_ArrayCount(expectedTypes), expectedTypes));
    TestAssert(PushedStackEntryCount(&tester) == 0);
    
    BEInstruction* printInstruction = GetInstruction(list, 0);
    
    BE_String str;
    str.count = printInstruction->print.prefixCharacterCount;
    str.characters = printInstruction->print.prefixCharacters;
    TestAssert(BE_StringsAreEqual(str, statement.print.message));
    TestAssert(printInstruction->print.value.stack == BE_Stack_Invalid);
}

Test(InstructionGenerator_Print_Expr)
{
    // NOTE(Jens): print("Test", 123);
    // 0: set   :: lX, 123
    // 1: print :: "", lX
    
    BE_Statement statement;
    BE_ClearStructToZero(statement);
    
    BE_InstructionType expectedTypes[] = {
        BE_InstructionType_Set,
        BE_InstructionType_Print
    };
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statement.print.expression, 0);
    BE_PushConstantInteger(&statement.print.expression, 123);
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForSingleStatement(&list, &tester, BE_StatementType_Print, &statement, BE_ArrayCount(expectedTypes), expectedTypes));
    TestAssert(PushedStackEntryCount(&tester) == 0);
    
    BEInstruction* setInstruction = GetInstruction(list, 0);
    BEInstruction* printInstruction = GetInstruction(list, 1);
    
    TestAssert(setInstruction->set.value == 123);
    
    BE_String str;
    str.count = printInstruction->print.prefixCharacterCount;
    str.characters = printInstruction->print.prefixCharacters;
    TestAssert(BE_StringsAreEqual(str, statement.print.message));
    TestAssert(printInstruction->print.value.stack != BE_Stack_Invalid);
    TestAssert(BE_StackEntriesAreEqual(printInstruction->print.value, setInstruction->result));
}

Test(InstructionGenerator_Assert)
{
    // NOTE(Jens): assert(123);
    // 0: set    :: lX, 123
    // 1: assert :: lX
    
    BE_Statement statement;
    BE_ClearStructToZero(statement);
    
    BE_InstructionType expectedTypes[] = {
        BE_InstructionType_Set,
        BE_InstructionType_Assert
    };
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statement.assert.expression, 0);
    BE_PushConstantInteger(&statement.assert.expression, 123);
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForSingleStatement(&list, &tester, BE_StatementType_Assert, &statement, BE_ArrayCount(expectedTypes), expectedTypes));
    TestAssert(PushedStackEntryCount(&tester) == 0);
    
    BEInstruction* setInstruction = GetInstruction(list, 0);
    BEInstruction* assertInstruction = GetInstruction(list, 1);
    
    TestAssert(setInstruction->set.value == 123);
    
    TestAssert(assertInstruction->assert.condition.stack != BE_Stack_Invalid);
    TestAssert(BE_StackEntriesAreEqual(assertInstruction->assert.condition, setInstruction->result));
}

Test(InstructionGenerator_VariableDeclaration)
{
    // NOTE(Jens): var i;
    // 0: set    :: lX, 0
    
    BE_Statement statement;
    BE_ClearStructToZero(statement);
    
    BE_InstructionType expectedTypes[] = {
        BE_InstructionType_Set
    };
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    statement.variableDeclaration.name = BE_StrLiteral("i");
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForSingleStatement(&list, &tester, BE_StatementType_VariableDeclaration, &statement, BE_ArrayCount(expectedTypes), expectedTypes));
    TestAssert(PushedStackEntryCount(&tester) == 1);
    
    BEInstruction* setInstruction = GetInstruction(list, 0);
    TestAssert(setInstruction->set.value == 0);
}

Test(InstructionGenerator_VariableDeclaration_Initializer)
{
    // NOTE(Jens): var i = 12;
    // 0: set    :: lX, 12
    
    BE_Statement statement;
    BE_ClearStructToZero(statement);
    
    BE_InstructionType expectedTypes[] = {
        BE_InstructionType_Set
    };
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statement.variableDeclaration.initializationExpression, 0);
    BE_PushConstantInteger(&statement.variableDeclaration.initializationExpression, 12);
    statement.variableDeclaration.name = BE_StrLiteral("i");
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForSingleStatement(&list, &tester, BE_StatementType_VariableDeclaration, &statement, BE_ArrayCount(expectedTypes), expectedTypes));
    TestAssert(PushedStackEntryCount(&tester) == 1);
    
    BEInstruction* setInstruction = GetInstruction(list, 0);
    TestAssert(setInstruction->set.value == 12);
}

Test(InstructionGenerator_Typedef)
{
    // NOTE(Jens): typedef ... U32;
    // <no instructions>
    
    BE_TypedefDeclaration typedefDeclaration;
    BE_ClearStructToZero(typedefDeclaration);
    
    typedefDeclaration.name = BE_StrLiteral("U32");
    typedefDeclaration.type.variant = BE_TypeVariant_Scalar;
    
    BE_Statement statement;
    statement.typedefDeclaration = typedefDeclaration;
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    TestAssert(GenerateInstructionsForSingleStatement(0, &tester, BE_StatementType_EnumDeclaration, &statement, 0, 0));
    TestAssert(PushedStackEntryCount(&tester) == 0);
}

Test(InstructionGenerator_Enum)
{
    // NOTE(Jens): enum Foo { foo = 123 };
    // <no instructions>
    
    BE_EnumMemberList member;
    BE_ClearStructToZero(member);
    
    member.name = BE_StrLiteral("foo");
    member.value = 123;
    
    BE_EnumDeclaration enumDeclaration;
    BE_ClearStructToZero(enumDeclaration);
    
    enumDeclaration.name = BE_StrLiteral("Foo");
    enumDeclaration.memberCount = 1;
    enumDeclaration.members = &member;
    
    BE_Statement statement;
    statement.enumDeclaration = enumDeclaration;
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    TestAssert(GenerateInstructionsForSingleStatement(0, &tester, BE_StatementType_EnumDeclaration, &statement, 0, 0));
    TestAssert(PushedStackEntryCount(&tester) == 0);
}

Test(InstructionGenerator_Import)
{
    // NOTE(Jens): import("foobar.bet");
    // <no instructions>
    
    BE_ImportStatement importStatement;
    BE_ClearStructToZero(importStatement);
    
    importStatement.unescapedFile = BE_StrLiteral("foobar.bet");
    
    BE_Statement statement;
    statement.import = importStatement;
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    TestAssert(GenerateInstructionsForSingleStatement(0, &tester, BE_StatementType_Import, &statement, 0, 0));
    TestAssert(PushedStackEntryCount(&tester) == 0);
}

Test(InstructionGenerator_Default)
{
    // NOTE(Jens): #default(byteorder = little);
    // <no instructions>
    
    BE_DefaultDirective defaultDirective;
    BE_ClearStructToZero(defaultDirective);
    
    BE_Statement statement;
    statement.defaultDirective = defaultDirective;
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    TestAssert(GenerateInstructionsForSingleStatement(0, &tester, BE_StatementType_DefaultDirective, &statement, 0, 0));
    TestAssert(PushedStackEntryCount(&tester) == 0);
}

Test(InstructionGenerator_Layout)
{
    // NOTE(Jens): <<< struct Foo { ... }; >>> layout Foo;
    // 0: push-frame
    // 1: set         :: l0, 3
    // 2: goto        :: 0
    // 3: pop-frame
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_AddStartOfStruct(tester.alloc.allocator, &tester.generator);
    
    BE_Statement statement = { 0 };
    statement.layout.typeIndex = 0;
    
    BE_InstructionType expectedTypes[] = {
        BE_InstructionType_PushFrame,
        BE_InstructionType_Set,
        BE_InstructionType_Jump,
        BE_InstructionType_PopFrame
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForSingleStatement(&list, &tester, BE_StatementType_Layout, &statement, BE_ArrayCount(expectedTypes), expectedTypes));
    TestAssert(PushedStackEntryCount(&tester) == 0);
    
    BEInstruction* setReturnAddressInstruction = GetInstruction(list, 1);
    BEInstruction* gotoInstruction = GetInstruction(list, 2);
    
    TestAssert(setReturnAddressInstruction->set.value == 3);
    TestAssert(gotoInstruction->jump.newInstructionIndex == 0);
}

// NOTE(Jens): 'struct':s don't return instructions until the entire type has been generated.
//             This is due to the skip jump address is not known until the amount of instructions contained in the struct is available.

Test(InstructionGenerator_StructHeader)
{
    // NOTE(Jens): struct Foo {
    // <no instructions>
    
    BE_StructHeader structHeader;
    BE_ClearStructToZero(structHeader);
    
    structHeader.name = BE_StrLiteral("Foo");
    
    BE_Statement statement;
    statement.structHeader = structHeader;
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    tester.generator.instructionIndex = 123;
    
    TestAssert(GenerateInstructionsForSingleStatement(0, &tester, BE_StatementType_StructHeader, &statement, 0, 0));
    TestAssert(PushedStackEntryCount(&tester) == 0);
    TestAssert(tester.generator.structStartAddresses.head);
    TestAssert(tester.generator.structStartAddresses.head->instructionIndex == 124);
}

Test(InstructionGenerator_EmptyStruct)
{
    // NOTE(Jens): struct Foo {};
    // 0: jump         :: 3
    // 1: struct-index :: 0
    // 2: jump         :: r
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[2];
    BE_ClearStructToZero(statements);
    
    statements[0].structHeader.name = BE_StrLiteral("Foo");
    
    BE_StatementType types[2] = {
        BE_StatementType_StructHeader,
        BE_StatementType_EndOf_Struct,
    };
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Jump,
        BE_InstructionType_StructIndex,
        BE_InstructionType_Jump,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, 2, types, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
    TestAssert(PushedStackEntryCount(&tester) == 0);
    
    BEInstruction* jumpOverInstruction = GetInstruction(list, 0);
    BEInstruction* returnInstruction = GetInstruction(list, 2);
    
    TestAssert(jumpOverInstruction->jump.newInstructionIndex == 3);
    TestAssert(BE_StackEntriesAreEqual(returnInstruction->jump.dynamicNewInstructionIndex, BE_ReturnAddress()));
}

Test(InstructionGenerator_EmptyStructWithParameters)
{
    // NOTE(Jens): struct Foo(var param1, var param2, var param3) {};
    // 0: jump         :: 3
    // 1: struct-index :: 0
    // 2: jump         :: r
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[2];
    BE_ClearStructToZero(statements);
    
    statements[0].structHeader.name = BE_StrLiteral("Foo");
    BE_AddMappedName(tester.alloc.allocator, &statements[0].structHeader.parameters, BE_StrLiteral("param1"));
    BE_AddMappedName(tester.alloc.allocator, &statements[0].structHeader.parameters, BE_StrLiteral("param2"));
    BE_AddMappedName(tester.alloc.allocator, &statements[0].structHeader.parameters, BE_StrLiteral("param3"));
    
    BE_StatementType types[2] = {
        BE_StatementType_StructHeader,
        BE_StatementType_EndOf_Struct,
    };
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Jump,
        BE_InstructionType_StructIndex,
        BE_InstructionType_Jump,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, 2, types, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
    TestAssert(PushedStackEntryCount(&tester) == 0);
    
    BEInstruction* jumpOverInstruction = GetInstruction(list, 0);
    BEInstruction* returnInstruction = GetInstruction(list, 2);
    
    TestAssert(jumpOverInstruction->jump.newInstructionIndex == 3);
    TestAssert(BE_StackEntriesAreEqual(returnInstruction->jump.dynamicNewInstructionIndex, BE_ReturnAddress()));
}

Test(InstructionGenerator_Variable_Assignment)
{
    // NOTE(Jens): var i; i = 5;
    // 0: set  :: l0, 0
    // 1: set  :: l0, 5
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[2];
    BE_ClearStructToZero(statements);
    
    BE_StatementType statementTypes[2] = {
        BE_StatementType_VariableDeclaration,
        BE_StatementType_Assignment,
    };
    
    statements[0].variableDeclaration.name = BE_StrLiteral("i");
    
    statements[1].assignment.variableIndex = BE_LocalSymbol_Count + 0;
    statements[1].assignment.op = BE_Punctuation("=");
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[1].assignment.rhs, 0);
    BE_PushConstantInteger(&statements[1].assignment.rhs, 5);
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Set,
        BE_InstructionType_Set,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, 2, statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
    TestAssert(PushedStackEntryCount(&tester) == 1);
    
    BEInstruction* setInstruction = GetInstruction(list, 1);
    TestAssert(setInstruction->set.value == 5);
}

Test(InstructionGenerator_Variable_CompoundAssignment)
{
    // NOTE(Jens): var i; i += 5;
    // 0: set  :: l0, 0
    // 1: set  :: l1, 5
    // 2: add  :: l2, l0, l1
    // 3: copy :: l0, l2
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[2];
    BE_ClearStructToZero(statements);
    
    BE_StatementType statementTypes[2] = {
        BE_StatementType_VariableDeclaration,
        BE_StatementType_Assignment,
    };
    
    statements[0].variableDeclaration.name = BE_StrLiteral("i");
    
    statements[1].assignment.variableIndex = BE_LocalSymbol_Count + 0;
    statements[1].assignment.op = BE_Punctuation("+=");
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[1].assignment.rhs, 0);
    BE_PushConstantInteger(&statements[1].assignment.rhs, 5);
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Set,
        BE_InstructionType_Set,
        BE_InstructionType_Add,
        BE_InstructionType_Copy,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, 2, statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
    TestAssert(PushedStackEntryCount(&tester) == 1);
    
    BEInstruction* addInstruction = GetInstruction(list, 2);
    
    TestAssert(BE_StackEntriesAreEqual(addInstruction->result, BE_Local(2)));
    TestAssert(BE_StackEntriesAreEqual(addInstruction->binary.left, BE_Local(0)));
    TestAssert(BE_StackEntriesAreEqual(addInstruction->binary.right, BE_Local(1)));
}

Test(InstructionGenerator_If)
{
    // NOTE(Jens): if (1) { breakpoint(); }
    // 0: set          :: l0, 1
    // 1: logical-not  :: l0, l0
    // 2: cnd-jump     :: 5, l0
    // 3: breakpoint
    // 4: jump         :: 5
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[3];
    BE_ClearStructToZero(statements);
    
    BE_StatementType statementTypes[3] = {
        BE_StatementType_If,
        BE_StatementType_Breakpoint,
        BE_StatementType_EndOf_If,
    };
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[0].ifStatement.condition, 0);
    BE_PushConstantInteger(&statements[0].ifStatement.condition, 1);
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Set,
        BE_InstructionType_LogicalNot,
        BE_InstructionType_Jump,
        BE_InstructionType_Breakpoint,
        BE_InstructionType_Jump
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, 3, statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
    TestAssert(PushedStackEntryCount(&tester) == 0);
    
    BEInstruction* elseJump = GetInstruction(list, 2);
    TestAssert(BE_StackEntriesAreEqual(elseJump->jump.condition, BE_Local(0)));
    TestAssert(elseJump->jump.newInstructionIndex == 5);
    
    BEInstruction* endOfIfJump = GetInstruction(list, 4);
    TestAssert(endOfIfJump->jump.newInstructionIndex == 5);
}

Test(InstructionGenerator_If_Else)
{
    // NOTE(Jens): if (1) { breakpoint(); } else { breakpoint(); }
    // 0: set          :: l0, 1
    // 1: logical-not  :: l0, l0
    // 2: cnd-jump     :: 5, l0
    // 3: breakpoint
    // 4: jump         :: 6
    // 5: breakpoint
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[6];
    BE_ClearStructToZero(statements);
    
    BE_StatementType statementTypes[6] = {
        BE_StatementType_If,
        BE_StatementType_Breakpoint,
        BE_StatementType_EndOf_If,
        BE_StatementType_Else,
        BE_StatementType_Breakpoint,
        BE_StatementType_EndOf_Else,
    };
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[0].ifStatement.condition, 0);
    BE_PushConstantInteger(&statements[0].ifStatement.condition, 1);
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Set,
        BE_InstructionType_LogicalNot,
        BE_InstructionType_Jump,
        BE_InstructionType_Breakpoint,
        BE_InstructionType_Jump,
        BE_InstructionType_Breakpoint,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, 6, statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
    TestAssert(PushedStackEntryCount(&tester) == 0);
    
    BEInstruction* jumpToElseInstruction = GetInstruction(list, 2);
    TestAssert(BE_StackEntriesAreEqual(jumpToElseInstruction->jump.condition, BE_Local(0)));
    TestAssert(jumpToElseInstruction->jump.newInstructionIndex == 5);
    
    BEInstruction* jumpToAwayFromIfInstruction = GetInstruction(list, 4);
    TestAssert(jumpToAwayFromIfInstruction->jump.newInstructionIndex == 6);
}

Test(InstructionGenerator_If_ElseIf)
{
    // NOTE(Jens): if (1) { breakpoint(); } else if (1) { breakpoint(); }
    // 0: set          :: l0, 1
    // 1: logical-not  :: l0, l0
    // 2: cnd-jump     :: 5, l0
    // 3: breakpoint
    // 4: jump         :: 10
    // 5: set          :: l0, 1
    // 6: logical-not  :: l0, l0
    // 7: cnd-jump     :: 10, l0
    // 8: breakpoint
    // 9: jump         :: 10
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[6];
    BE_ClearStructToZero(statements);
    
    BE_StatementType statementTypes[6] = {
        BE_StatementType_If,
        BE_StatementType_Breakpoint,
        BE_StatementType_EndOf_If,
        BE_StatementType_If,
        BE_StatementType_Breakpoint,
        BE_StatementType_EndOf_If,
    };
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[0].ifStatement.condition, 0);
    BE_PushConstantInteger(&statements[0].ifStatement.condition, 1);
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[3].ifStatement.condition, 0);
    BE_PushConstantInteger(&statements[3].ifStatement.condition, 1);
    statements[3].ifStatement.isElseIf = 1;
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Set,
        BE_InstructionType_LogicalNot,
        BE_InstructionType_Jump,
        BE_InstructionType_Breakpoint,
        BE_InstructionType_Jump,
        BE_InstructionType_Set,
        BE_InstructionType_LogicalNot,
        BE_InstructionType_Jump,
        BE_InstructionType_Breakpoint,
        BE_InstructionType_Jump,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, 6, statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
    TestAssert(PushedStackEntryCount(&tester) == 0);
    
    {
        BEInstruction* jumpToElseInstruction = GetInstruction(list, 2);
        TestAssert(BE_StackEntriesAreEqual(jumpToElseInstruction->jump.condition, BE_Local(0)));
        TestAssert(jumpToElseInstruction->jump.newInstructionIndex == 5);
    }
    
    {
        BEInstruction* jumpToAwayFromIfInstruction = GetInstruction(list, 4);
        TestAssert(jumpToAwayFromIfInstruction->jump.newInstructionIndex == 10);
    }
    
    {
        BEInstruction* jumpToAwayFromIfInstruction = GetInstruction(list, 7);
        TestAssert(jumpToAwayFromIfInstruction->jump.newInstructionIndex == 10);
    }
}

Test(InstructionGenerator_If_ElseIf_Else)
{
    // NOTE(Jens): if (1) { breakpoint(); } else if (1) { breakpoint(); } else { breakpoint(); }
    //  0: set          :: l0, 1
    //  1: logical-not  :: l0, l0
    //  2: cnd-jump     :: 5, l0
    //  3: breakpoint
    //  4: jump         :: 11
    //  5: set          :: l0, 1
    //  6: logical-not  :: l0, l0
    //  7: cnd-jump     :: 10, l0
    //  8: breakpoint
    //  9: jump         :: 11
    // 10: breakpoint
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[6];
    BE_ClearStructToZero(statements);
    
    BE_StatementType statementTypes[9] = {
        BE_StatementType_If,
        BE_StatementType_Breakpoint,
        BE_StatementType_EndOf_If,
        BE_StatementType_If,
        BE_StatementType_Breakpoint,
        BE_StatementType_EndOf_If,
        BE_StatementType_Else,
        BE_StatementType_Breakpoint,
        BE_StatementType_EndOf_Else,
    };
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[0].ifStatement.condition, 0);
    BE_PushConstantInteger(&statements[0].ifStatement.condition, 1);
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[3].ifStatement.condition, 0);
    BE_PushConstantInteger(&statements[3].ifStatement.condition, 1);
    statements[3].ifStatement.isElseIf = 1;
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Set,
        BE_InstructionType_LogicalNot,
        BE_InstructionType_Jump,
        BE_InstructionType_Breakpoint,
        BE_InstructionType_Jump,
        BE_InstructionType_Set,
        BE_InstructionType_LogicalNot,
        BE_InstructionType_Jump,
        BE_InstructionType_Breakpoint,
        BE_InstructionType_Jump,
        BE_InstructionType_Breakpoint,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, 9, statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
    TestAssert(PushedStackEntryCount(&tester) == 0);
    
    {
        BEInstruction* jumpToElseInstruction = GetInstruction(list, 2);
        TestAssert(BE_StackEntriesAreEqual(jumpToElseInstruction->jump.condition, BE_Local(0)));
        TestAssert(jumpToElseInstruction->jump.newInstructionIndex == 5);
    }
    
    {
        BEInstruction* jumpToElseInstruction = GetInstruction(list, 7);
        TestAssert(BE_StackEntriesAreEqual(jumpToElseInstruction->jump.condition, BE_Local(0)));
        TestAssert(jumpToElseInstruction->jump.newInstructionIndex == 10);
    }
    
    {
        BEInstruction* jumpToAwayFromIfInstruction = GetInstruction(list, 4);
        TestAssert(jumpToAwayFromIfInstruction->jump.newInstructionIndex == 11);
    }
    
    {
        BEInstruction* jumpToAwayFromIfInstruction = GetInstruction(list, 9);
        TestAssert(jumpToAwayFromIfInstruction->jump.newInstructionIndex == 11);
    }
}

Test(InstructionGenerator_For)
{
    // NOTE(Jens): for (var i; 1; i = 1) { breakpoint(); }
    //  0: set          :: l0, 0
    //  1: jump         :: 3
    //  2: set          :: l0, 1
    //  3: set          :: l1, 1
    //  4: logical-not  :: l1, l1
    //  5: cnd-jump     :: 8, l1
    //  6: breakpoint
    //  7: jump         :: 2
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[3];
    BE_ClearStructToZero(statements);
    
    BE_StatementType statementTypes[3] = {
        BE_StatementType_For,
        BE_StatementType_Breakpoint,
        BE_StatementType_EndOf_For,
    };
    
    statements[0].forLoop.variable.name = BE_StrLiteral("i");
    statements[0].forLoop.variable.index = BE_LocalSymbol_Count + 0;
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[0].forLoop.endCondition, 0);
    BE_PushConstantInteger(&statements[0].forLoop.endCondition, 1);
    
    statements[0].forLoop.assignment.variableIndex = statements[0].forLoop.variable.index;
    statements[0].forLoop.assignment.op = BE_Punctuation("=");
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[0].forLoop.assignment.rhs, 0);
    BE_PushConstantInteger(&statements[0].forLoop.assignment.rhs, 1);
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Set,
        BE_InstructionType_Jump,
        BE_InstructionType_Set,
        BE_InstructionType_Set,
        BE_InstructionType_LogicalNot,
        BE_InstructionType_Jump,
        BE_InstructionType_Breakpoint,
        BE_InstructionType_Jump,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, 3, statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
    TestAssert(PushedStackEntryCount(&tester) == 0);
    
    {
        BEInstruction* varInit = GetInstruction(list, 0);
        TestAssert(varInit->set.value == 0);
    }
    
    {
        BEInstruction* jumpToStartOfLoop = GetInstruction(list, 1);
        TestAssert(jumpToStartOfLoop->jump.newInstructionIndex == 3);
    }
    
    {
        BEInstruction* jumpAwayFromLoop = GetInstruction(list, 5);
        TestAssert(jumpAwayFromLoop->jump.newInstructionIndex == 8);
    }
    
    {
        BEInstruction* jumpBackToIteration = GetInstruction(list, 7);
        TestAssert(jumpBackToIteration->jump.newInstructionIndex == 2);
    }
}

Test(InstructionGenerator_While)
{
    // NOTE(Jens): while (1) { breakpoint(); }
    //  0: set          :: l0, 1
    //  1: logical-not  :: l0, l0
    //  2: cnd-jump     :: 5, l0
    //  3: breakpoint
    //  4: jump         :: 0
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[3];
    BE_ClearStructToZero(statements);
    
    BE_StatementType statementTypes[3] = {
        BE_StatementType_While,
        BE_StatementType_Breakpoint,
        BE_StatementType_EndOf_While,
    };
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[0].whileLoop.condition, 0);
    BE_PushConstantInteger(&statements[0].whileLoop.condition, 1);
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Set,
        BE_InstructionType_LogicalNot,
        BE_InstructionType_Jump,
        BE_InstructionType_Breakpoint,
        BE_InstructionType_Jump,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, 3, statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
    TestAssert(PushedStackEntryCount(&tester) == 0);
    
    {
        BEInstruction* jumpAwayFromLoop = GetInstruction(list, 2);
        TestAssert(jumpAwayFromLoop->jump.newInstructionIndex == 5);
    }
    
    {
        BEInstruction* jumpBackToIteration = GetInstruction(list, 4);
        TestAssert(jumpBackToIteration->jump.newInstructionIndex == 0);
    }
}

Test(InstructionGenerator_DoWhile)
{
    // NOTE(Jens): do { breakpoint(); } while (1);
    //  0: breakpoint
    //  1: set          :: l0, 1
    //  2: cnd-jump     :: 0, l0
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[3];
    BE_ClearStructToZero(statements);
    
    BE_StatementType statementTypes[3] = {
        BE_StatementType_Do,
        BE_StatementType_Breakpoint,
        BE_StatementType_EndOf_DoWhile,
    };
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[2].endOfDoWhile.condition, 0);
    BE_PushConstantInteger(&statements[2].endOfDoWhile.condition, 1);
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Breakpoint,
        BE_InstructionType_Set,
        BE_InstructionType_Jump,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, 3, statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
    TestAssert(PushedStackEntryCount(&tester) == 0);
    
    {
        BEInstruction* jumpBackToIteration = GetInstruction(list, 2);
        TestAssert(jumpBackToIteration->jump.newInstructionIndex == 0);
    }
}

Test(InstructionGenerator_MemberDeclaration_LittleScalar)
{
    // NOTE(Jens): struct Foo { u(4) member; };
    // 0: jump         :: 4
    // 1: struct-index :: 0
    // 2: set          :: l0, 4
    // 3: scalar-u     :: 0, l0, 0
    // 4: add          :: @, @, l0
    // 5: jump         :: r
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[3];
    BE_ClearStructToZero(statements);
    
    BE_StatementType statementTypes[3] = {
        BE_StatementType_StructHeader,
        BE_StatementType_MemberDeclaration,
        BE_StatementType_EndOf_Struct,
    };
    
    statements[0].structHeader.name = BE_StrLiteral("Foo");
    statements[1].memberDeclaration.type.scalarVariant.byteOrder = BE_ScalarByteOrder_little;
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[1].memberDeclaration.type.scalarVariant.sizeExpression, 0);
    BE_PushConstantInteger(&statements[1].memberDeclaration.type.scalarVariant.sizeExpression, 4);
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Jump,
        BE_InstructionType_StructIndex,
        BE_InstructionType_Set,
        BE_InstructionType_ExportScalar_u,
        BE_InstructionType_Add,
        BE_InstructionType_Jump,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, 3, statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
    TestAssert(PushedStackEntryCount(&tester) == 0);
    
    BEInstruction* setSize = GetInstruction(list, 2);
    BEInstruction* exportScalar = GetInstruction(list, 3);
    
    TestAssert(BE_StackEntriesAreEqual(setSize->result, exportScalar->scalarExport.size));
    TestAssert(exportScalar->scalarExport.arrayCount.stack == BE_Stack_Invalid);
    TestAssert(!exportScalar->scalarExport.isBigEndian);
}

Test(InstructionGenerator_MemberDeclaration_BigScalar)
{
    // NOTE(Jens): struct Foo { u(4) member; };
    // 0: jump         :: 4
    // 1: struct-index :: 0
    // 2: set          :: l0, 4
    // 3: scalar-u     :: 0, l0, 0
    // 4: add          :: @, @, l0
    // 5: jump         :: r
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[3];
    BE_ClearStructToZero(statements);
    
    BE_StatementType statementTypes[3] = {
        BE_StatementType_StructHeader,
        BE_StatementType_MemberDeclaration,
        BE_StatementType_EndOf_Struct,
    };
    
    statements[0].structHeader.name = BE_StrLiteral("Foo");
    statements[1].memberDeclaration.type.scalarVariant.byteOrder = BE_ScalarByteOrder_big;
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[1].memberDeclaration.type.scalarVariant.sizeExpression, 0);
    BE_PushConstantInteger(&statements[1].memberDeclaration.type.scalarVariant.sizeExpression, 4);
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Jump,
        BE_InstructionType_StructIndex,
        BE_InstructionType_Set,
        BE_InstructionType_ExportScalar_u,
        BE_InstructionType_Add,
        BE_InstructionType_Jump,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, 3, statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
    TestAssert(PushedStackEntryCount(&tester) == 0);
    
    BEInstruction* setSize = GetInstruction(list, 2);
    BEInstruction* exportScalar = GetInstruction(list, 3);
    
    TestAssert(BE_StackEntriesAreEqual(setSize->result, exportScalar->scalarExport.size));
    TestAssert(exportScalar->scalarExport.arrayCount.stack == BE_Stack_Invalid);
    TestAssert(exportScalar->scalarExport.isBigEndian);
}

Test(InstructionGenerator_MemberDeclaration_Scalar_Array)
{
    // NOTE(Jens): struct Foo { u(4) member[8]; };
    // 0: jump         :: 4
    // 1: struct-index :: 0
    // 2: set          :: l0, 4
    // 3: set          :: l1, 8
    // 4: scalar-u     :: 0, l0, l1
    // 5: mul          :: l0, l0, l1
    // 6: add          :: @, @, l0
    // 7: jump         :: r
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[3];
    BE_ClearStructToZero(statements);
    
    BE_StatementType statementTypes[3] = {
        BE_StatementType_StructHeader,
        BE_StatementType_MemberDeclaration,
        BE_StatementType_EndOf_Struct,
    };
    
    statements[0].structHeader.name = BE_StrLiteral("Foo");
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[1].memberDeclaration.type.scalarVariant.sizeExpression, 0);
    BE_PushConstantInteger(&statements[1].memberDeclaration.type.scalarVariant.sizeExpression, 4);
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[1].memberDeclaration.arrayCount, 0);
    BE_PushConstantInteger(&statements[1].memberDeclaration.arrayCount, 8);
    statements[1].memberDeclaration.isArray = 1;
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Jump,
        BE_InstructionType_StructIndex,
        BE_InstructionType_Set,
        BE_InstructionType_Set,
        BE_InstructionType_ExportScalar_u,
        BE_InstructionType_Mul,
        BE_InstructionType_Add,
        BE_InstructionType_Jump,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, 3, statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
    TestAssert(PushedStackEntryCount(&tester) == 0);
    
    BEInstruction* setSize = GetInstruction(list, 2);
    BEInstruction* setArrayCount = GetInstruction(list, 3);
    BEInstruction* exportScalar = GetInstruction(list, 4);
    
    TestAssert(BE_StackEntriesAreEqual(setSize->result, exportScalar->scalarExport.size));
    TestAssert(BE_StackEntriesAreEqual(setArrayCount->result, exportScalar->scalarExport.arrayCount));
}

Test(InstructionGenerator_MemberDeclaration_Scalar_Aligned)
{
    // NOTE(Jens): struct Foo { alignas(4) u(4) member; };
    // 0: jump         :: 4
    // 1: struct-index :: 0
    // 2: set          :: l0, 4
    // 3: set          :: l1, 8
    // 4: scalar-u     :: 0, l0, l1
    // 5: mul          :: l0, l0, l1
    // 6: add          :: @, @, l0
    // 7: jump         :: r
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[3];
    BE_ClearStructToZero(statements);
    
    BE_StatementType statementTypes[3] = {
        BE_StatementType_StructHeader,
        BE_StatementType_MemberDeclaration,
        BE_StatementType_EndOf_Struct,
    };
    
    statements[0].structHeader.name = BE_StrLiteral("Foo");
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[1].memberDeclaration.type.scalarVariant.sizeExpression, 0);
    BE_PushConstantInteger(&statements[1].memberDeclaration.type.scalarVariant.sizeExpression, 4);
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[1].memberDeclaration.arrayCount, 0);
    BE_PushConstantInteger(&statements[1].memberDeclaration.arrayCount, 8);
    statements[1].memberDeclaration.isArray = 1;
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Jump,
        BE_InstructionType_StructIndex,
        BE_InstructionType_Set,
        BE_InstructionType_Set,
        BE_InstructionType_ExportScalar_u,
        BE_InstructionType_Mul,
        BE_InstructionType_Add,
        BE_InstructionType_Jump,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, 3, statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
    TestAssert(PushedStackEntryCount(&tester) == 0);
    
    BEInstruction* setSize = GetInstruction(list, 2);
    BEInstruction* setArrayCount = GetInstruction(list, 3);
    BEInstruction* exportScalar = GetInstruction(list, 4);
    
    TestAssert(BE_StackEntriesAreEqual(setSize->result, exportScalar->scalarExport.size));
    TestAssert(BE_StackEntriesAreEqual(setArrayCount->result, exportScalar->scalarExport.arrayCount));
}

Test(InstructionGenerator_MemberDeclaration_Typedef_Scalar)
{
    // NOTE(Jens): typedef u(4) u32; struct Foo { u32 member; };
    // 0: jump         :: 4
    // 1: struct-index :: 0
    // 2: set          :: l0, 4
    // 3: scalar-u     :: 0, l0, 0
    // 4: add          :: @, @, l0
    // 5: jump         :: r
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[3];
    BE_ClearStructToZero(statements);
    
    BE_StatementType statementTypes[3] = {
        BE_StatementType_StructHeader,
        BE_StatementType_MemberDeclaration,
        BE_StatementType_EndOf_Struct,
    };
    
    statements[0].structHeader.name = BE_StrLiteral("Foo");
    
    BE_TypeDeclaration scalarDecl;
    BE_ClearStructToZero(scalarDecl);
    
    scalarDecl.variant = BE_TypeVariant_Scalar;
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &scalarDecl.scalarVariant.sizeExpression, 0);
    BE_PushConstantInteger(&scalarDecl.scalarVariant.sizeExpression, 4);
    scalarDecl.scalarVariant.baseType = BE_ScalarBaseType_u;
    scalarDecl.scalarVariant.displayType = BE_ScalarDisplayType_decimal;
    
    BE_TypeDeclaration u32Decl;
    BE_ClearStructToZero(u32Decl);
    
    BE_ParsedTypedefNode parsedNode;
    BE_ClearStructToZero(parsedNode);
    
    parsedNode.def.name = BE_StrLiteral("u32");
    parsedNode.def.type = scalarDecl;
    
    u32Decl.variant = BE_TypeVariant_Typedef;
    u32Decl.typedefVariant.def = &parsedNode;
    
    statements[1].memberDeclaration.type = u32Decl;
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Jump,
        BE_InstructionType_StructIndex,
        BE_InstructionType_Set,
        BE_InstructionType_ExportScalar_u,
        BE_InstructionType_Add,
        BE_InstructionType_Jump,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, 3, statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
    TestAssert(PushedStackEntryCount(&tester) == 0);
    
    BEInstruction* setSize = GetInstruction(list, 2);
    BEInstruction* exportScalar = GetInstruction(list, 3);
    
    TestAssert(BE_StackEntriesAreEqual(setSize->result, exportScalar->scalarExport.size));
    TestAssert(exportScalar->scalarExport.arrayCount.stack == BE_Stack_Invalid);
}

Test(InstructionGenerator_MemberDeclaration_Struct)
{
    // NOTE(Jens): struct Bar {}; struct Foo { Bar member; };
    
    // struct Bar {};
    //  0: jump         :: 3
    //  1: struct-index :: 0
    //  2: jump         :: r
    
    // struct Foo { Bar member; };
    //  3: jump         :: 13
    //  4: struct-index :: 1
    //  5: push-frame
    //  6: push-struct :: 0, -
    //  7: set         :: r, 8
    //  8: set         :: i, 0
    //  9: jump        :: 1
    // 10: pop-struct
    // 11: pop-frame
    // 12: jump        :: r
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[5];
    BE_ClearStructToZero(statements);
    
    BE_StatementType statementTypes[5] = {
        BE_StatementType_StructHeader,
        BE_StatementType_EndOf_Struct,
        BE_StatementType_StructHeader,
        BE_StatementType_MemberDeclaration,
        BE_StatementType_EndOf_Struct,
    };
    
    statements[0].structHeader.name = BE_StrLiteral("Bar");
    statements[2].structHeader.name = BE_StrLiteral("Foo");
    
    BE_ParsedStructNode structBar;
    BE_ClearStructToZero(structBar);
    structBar.name = statements[0].structHeader.name;
    
    BE_TypeDeclaration memberDecl;
    BE_ClearStructToZero(memberDecl);
    
    memberDecl.variant = BE_TypeVariant_Struct;
    memberDecl.structVariant.type = &structBar;
    
    statements[3].memberDeclaration.type = memberDecl;
    statements[3].memberDeclaration.name = BE_StrLiteral("member");
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Jump,
        BE_InstructionType_StructIndex,
        BE_InstructionType_Jump,
        BE_InstructionType_Jump,
        BE_InstructionType_StructIndex,
        BE_InstructionType_PushFrame,
        BE_InstructionType_PushStruct,
        BE_InstructionType_Set,
        BE_InstructionType_Set,
        BE_InstructionType_Jump,
        BE_InstructionType_PopStruct,
        BE_InstructionType_PopFrame,
        BE_InstructionType_Jump,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, 5, statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
    TestAssert(PushedStackEntryCount(&tester) == 0);
    
    {
        BEInstruction* setArrayIndex = GetInstruction(list, 8);
        TestAssert(BE_StackEntriesAreEqual(setArrayIndex->result, BE_ArrayIndex()));
    }
    
    {
        BEInstruction* setReturnAddress = GetInstruction(list, 7);
        TestAssert(BE_StackEntriesAreEqual(setReturnAddress->result, BE_ReturnAddress()));
    }
}

Test(InstructionGenerator_MemberDeclaration_Struct_Internal)
{
    // NOTE(Jens): struct Bar {}; struct Foo { Bar member; };
    
    // struct Bar {};
    //  0: jump                 :: 3
    //  1: struct-index         :: 0
    //  2: jump                 :: r
    
    // struct Foo { Bar member; };
    //  3: jump                 :: 13
    //  4: struct-index         :: 1
    //  5: push-frame
    //  6: push-struct-internal :: 0, -
    //  7: set                  :: r, 8
    //  8: set                  :: i, 0
    //  9: jump                 :: 1
    // 10: pop-struct
    // 11: pop-frame
    // 12: jump                 :: r
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[5];
    BE_ClearStructToZero(statements);
    
    BE_StatementType statementTypes[5] = {
        BE_StatementType_StructHeader,
        BE_StatementType_EndOf_Struct,
        BE_StatementType_StructHeader,
        BE_StatementType_MemberDeclaration,
        BE_StatementType_EndOf_Struct,
    };
    
    statements[0].structHeader.name = BE_StrLiteral("Bar");
    statements[2].structHeader.name = BE_StrLiteral("Foo");
    
    BE_ParsedStructNode structBar;
    BE_ClearStructToZero(structBar);
    structBar.name = statements[0].structHeader.name;
    
    BE_TypeDeclaration memberDecl;
    BE_ClearStructToZero(memberDecl);
    
    memberDecl.variant = BE_TypeVariant_Struct;
    memberDecl.structVariant.type = &structBar;
    
    statements[3].memberDeclaration.type = memberDecl;
    statements[3].memberDeclaration.name = BE_StrLiteral("member");
    statements[3].memberDeclaration.isInternal = 1;
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Jump,
        BE_InstructionType_StructIndex,
        BE_InstructionType_Jump,
        BE_InstructionType_Jump,
        BE_InstructionType_StructIndex,
        BE_InstructionType_PushFrame,
        BE_InstructionType_PushStructInternal,
        BE_InstructionType_Set,
        BE_InstructionType_Set,
        BE_InstructionType_Jump,
        BE_InstructionType_PopStruct,
        BE_InstructionType_PopFrame,
        BE_InstructionType_Jump,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, 5, statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
    TestAssert(PushedStackEntryCount(&tester) == 0);
    
    {
        BEInstruction* setArrayIndex = GetInstruction(list, 8);
        TestAssert(BE_StackEntriesAreEqual(setArrayIndex->result, BE_ArrayIndex()));
    }
    
    {
        BEInstruction* setReturnAddress = GetInstruction(list, 7);
        TestAssert(BE_StackEntriesAreEqual(setReturnAddress->result, BE_ReturnAddress()));
    }
}

Test(InstructionGenerator_MemberDeclaration_Struct_Array)
{
    // NOTE(Jens): struct Bar {}; struct Foo { Bar member[7]; };
    
    // struct Bar {};
    //  0: jump         :: 2
    //  1: struct-index :: 0
    //  2: jump         :: r
    
    // struct Foo { Bar member[7]; };
    //  3: jump         :: 18
    //  4: struct-index :: 1
    //  5: set          :: l0, 7
    //  6: set          :: l1, 1
    //  7: push-frame
    //  8: push-struct  :: 0, 0, l0
    //  9: set          :: r, 15
    // 10: set          :: i, -1
    // 11: add          :: i, i, p1
    // 12: comp-ge      :: p2, i, p0
    // 13: cnd-jump     :: 16, p2
    // 14: jump         :: 1
    // 15: jump         :: 11
    // 16: pop-struct
    // 17: pop-frame
    // 18: jump         :: r
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[5];
    BE_ClearStructToZero(statements);
    
    BE_StatementType statementTypes[5] = {
        BE_StatementType_StructHeader,
        BE_StatementType_EndOf_Struct,
        BE_StatementType_StructHeader,
        BE_StatementType_MemberDeclaration,
        BE_StatementType_EndOf_Struct,
    };
    
    statements[0].structHeader.name = BE_StrLiteral("Bar");
    statements[2].structHeader.name = BE_StrLiteral("Foo");
    
    BE_ParsedStructNode structBar;
    BE_ClearStructToZero(structBar);
    structBar.name = statements[0].structHeader.name;
    
    BE_TypeDeclaration memberDecl;
    BE_ClearStructToZero(memberDecl);
    
    memberDecl.variant = BE_TypeVariant_Struct;
    memberDecl.structVariant.type = &structBar;
    
    statements[3].memberDeclaration.type = memberDecl;
    statements[3].memberDeclaration.name = BE_StrLiteral("member");
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[3].memberDeclaration.arrayCount, 0);
    BE_PushConstantInteger(&statements[3].memberDeclaration.arrayCount, 7);
    statements[3].memberDeclaration.isArray = 1;
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Jump,
        BE_InstructionType_StructIndex,
        BE_InstructionType_Jump,
        BE_InstructionType_Jump,
        BE_InstructionType_StructIndex,
        BE_InstructionType_Set,
        BE_InstructionType_Set,
        BE_InstructionType_PushFrame,
        BE_InstructionType_PushStruct,
        BE_InstructionType_Set,
        BE_InstructionType_Set,
        BE_InstructionType_Add,
        BE_InstructionType_GreaterOrEqual,
        BE_InstructionType_Jump,
        BE_InstructionType_Jump,
        BE_InstructionType_Jump,
        BE_InstructionType_PopStruct,
        BE_InstructionType_PopFrame,
        BE_InstructionType_Jump,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, 5, statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
    TestAssert(PushedStackEntryCount(&tester) == 0);
    
    {
        BEInstruction* setArrayIndex = GetInstruction(list, 10);
        TestAssert(BE_StackEntriesAreEqual(setArrayIndex->result, BE_ArrayIndex()));
    }
    
    {
        BEInstruction* setArrayCount = GetInstruction(list, 5);
        TestAssert(setArrayCount->set.value == 7);
    }
    
    {
        BEInstruction* setReturnAddress = GetInstruction(list, 9);
        TestAssert(BE_StackEntriesAreEqual(setReturnAddress->result, BE_ReturnAddress()));
    }
    
    {
        BEInstruction* jumpToPopFrame = GetInstruction(list, 13);
        TestAssert(jumpToPopFrame->jump.newInstructionIndex == 16);
    }
}

Test(InstructionGenerator_MemberDeclaration_Category)
{
    // NOTE(Jens): struct(Cat, 123) Bar {}; struct Foo { Cat[123] member; };
    
    // struct Bar {};
    //  0: jump                 :: 2
    //  1: struct-index         :: 0
    //  2: jump                 :: r
    
    // struct Foo { Cat[123] member; };
    //  3: jump                 :: 12
    //  4: struct-index         :: 1
    //  5: set                  :: l1, 123
    //  6: fetch-category-start :: l0, 0, l1
    //  7: push-frame
    //  8: push-struct          :: 0, -
    //  9: set                  :: r, 8
    // 10: set                  :: i, 0
    // 11: jump                 :: p0
    // 12: pop-struct
    // 13: pop-frame
    // 14: jump                 :: r
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[5];
    BE_ClearStructToZero(statements);
    
    BE_StatementType statementTypes[5] = {
        BE_StatementType_StructHeader,
        BE_StatementType_EndOf_Struct,
        BE_StatementType_StructHeader,
        BE_StatementType_MemberDeclaration,
        BE_StatementType_EndOf_Struct,
    };
    
    statements[0].structHeader.name = BE_StrLiteral("Bar");
    statements[2].structHeader.name = BE_StrLiteral("Foo");
    
    BE_ParsedStructNode structBar;
    BE_ClearStructToZero(structBar);
    structBar.name = statements[0].structHeader.name;
    structBar.categoryNumber = 123;
    
    BE_ParsedCategoryNode categoryCat;
    BE_ClearStructToZero(categoryCat);
    categoryCat.types = &structBar;
    categoryCat.name = BE_StrLiteral("Cat");
    
    BE_TypeDeclaration memberDecl;
    BE_ClearStructToZero(memberDecl);
    
    memberDecl.variant = BE_TypeVariant_Category;
    memberDecl.categoryVariant.category = &categoryCat;
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &memberDecl.categoryVariant.categoryIdentifier, 0);
    BE_PushConstantInteger(&memberDecl.categoryVariant.categoryIdentifier, 123);
    
    statements[3].memberDeclaration.type = memberDecl;
    statements[3].memberDeclaration.name = BE_StrLiteral("member");
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Jump,
        BE_InstructionType_StructIndex,
        BE_InstructionType_Jump,
        BE_InstructionType_Jump,
        BE_InstructionType_StructIndex,
        BE_InstructionType_Set,
        BE_InstructionType_FetchStartOfType,
        BE_InstructionType_PushFrame,
        BE_InstructionType_PushStruct,
        BE_InstructionType_Set,
        BE_InstructionType_Set,
        BE_InstructionType_Jump,
        BE_InstructionType_PopStruct,
        BE_InstructionType_PopFrame,
        BE_InstructionType_Jump,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, 5, statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
    TestAssert(PushedStackEntryCount(&tester) == 0);
    
    {
        BEInstruction* setArrayIndex = GetInstruction(list, 10);
        TestAssert(BE_StackEntriesAreEqual(setArrayIndex->result, BE_ArrayIndex()));
    }
    
    {
        BEInstruction* setReturnAddress = GetInstruction(list, 9);
        TestAssert(BE_StackEntriesAreEqual(setReturnAddress->result, BE_ReturnAddress()));
    }
    
    {
        BEInstruction* fetchStartOfType = GetInstruction(list, 6);
        BEInstruction* jumpToType = GetInstruction(list, 11);
        
        TestAssert(BE_StackEntriesAreEqual(BE_FromParentScope(fetchStartOfType->result), jumpToType->jump.dynamicNewInstructionIndex));
    }
}

Test(InstructionGenerator_MemberDeclaration_Enum)
{
    // NOTE(Jens): enum Bar(4) { FOO }; struct Foo { Bar member; };
    // 0: jump         :: 4
    // 1: struct-index :: 0
    // 2: set          :: l0, 4
    // 3: scalar-u     :: 0, l0, 0
    // 4: add          :: @, @, l0
    // 5: jump         :: r
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    BE_Statement statements[4];
    BE_ClearStructToZero(statements);
    
    BE_StatementType statementTypes[4] = {
        BE_StatementType_EnumDeclaration,
        BE_StatementType_StructHeader,
        BE_StatementType_MemberDeclaration,
        BE_StatementType_EndOf_Struct,
    };
    
    
    BE_EnumMemberList enumMembers;
    BE_ClearStructToZero(enumMembers);
    
    enumMembers.name = BE_StrLiteral("FOO");
    
    statements[0].enumDeclaration.name = BE_StrLiteral("Bar");
    statements[0].enumDeclaration.memberCount = 1;
    statements[0].enumDeclaration.members = &enumMembers;
    statements[0].enumDeclaration.size = 4;
    
    statements[1].structHeader.name = BE_StrLiteral("Foo");
    
    BE_ParsedEnumNode dummyNode;
    BE_ClearStructToZero(dummyNode);
    dummyNode.members = &enumMembers;
    dummyNode.index = 0;
    dummyNode.size = statements[0].enumDeclaration.size;
    
    statements[2].memberDeclaration.type.scalarVariant.byteOrder = BE_ScalarByteOrder_little;
    statements[2].memberDeclaration.type.scalarVariant.enumType = &dummyNode;
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Jump,
        BE_InstructionType_StructIndex,
        BE_InstructionType_Set,
        BE_InstructionType_ExportScalar_u,
        BE_InstructionType_Add,
        BE_InstructionType_Jump,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, 4, statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
    TestAssert(PushedStackEntryCount(&tester) == 0);
    
    BEInstruction* setSize = GetInstruction(list, 2);
    BEInstruction* exportScalar = GetInstruction(list, 3);
    
    TestAssert(BE_StackEntriesAreEqual(setSize->result, exportScalar->scalarExport.size));
    TestAssert(exportScalar->scalarExport.arrayCount.stack == BE_Stack_Invalid);
    TestAssert(exportScalar->scalarExport.displayType == -1);
}

Test(InstructionGenerator_Expression_MemberOffset)
{
    // NOTE(Jens): var i = @;
    // 0: copy  :: l0, @
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[1];
    BE_ClearStructToZero(statements);
    
    BE_StatementType statementTypes[1] = {
        BE_StatementType_VariableDeclaration,
    };
    
    statements[0].variableDeclaration.name = BE_StrLiteral("i");
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[0].variableDeclaration.initializationExpression, 0);
    BE_PushMemberOffset(&statements[0].variableDeclaration.initializationExpression);
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Copy,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, 1, statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
    
    BEInstruction* copyInstruction = GetInstruction(list, 0);
    TestAssert(BE_StackEntriesAreEqual(copyInstruction->unaryValue, BE_MemberOffset()));
}

Test(InstructionGenerator_Expression_LocalVariable)
{
    // NOTE(Jens): var i; var j = i;
    // 0: set   :: l0, 0
    // 1: copy  :: l1, l0
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[2];
    BE_ClearStructToZero(statements);
    
    BE_StatementType statementTypes[] = {
        BE_StatementType_VariableDeclaration,
        BE_StatementType_VariableDeclaration,
    };
    
    statements[0].variableDeclaration.name = BE_StrLiteral("i");
    
    statements[1].variableDeclaration.name = BE_StrLiteral("j");
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[1].variableDeclaration.initializationExpression, 0);
    BE_PushLocalVariable(&statements[1].variableDeclaration.initializationExpression, 0);
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Set,
        BE_InstructionType_Copy,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, 2, statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
    
    BEInstruction* copyInstruction = GetInstruction(list, 1);
    TestAssert(BE_StackEntriesAreEqual(copyInstruction->unaryValue, BE_Local(0)));
}

Test(InstructionGenerator_Expression_SizeOfFile)
{
    // NOTE(Jens): var i = size_of_file;
    // 0: fetch-size-of-file :: l1
    // 1: copy               :: l0, l1
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[1];
    BE_ClearStructToZero(statements);
    
    BE_StatementType statementTypes[1] = {
        BE_StatementType_VariableDeclaration,
    };
    
    statements[0].variableDeclaration.name = BE_StrLiteral("i");
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[0].variableDeclaration.initializationExpression, 0);
    BE_PushSizeOfFile(&statements[0].variableDeclaration.initializationExpression);
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_FetchSizeOfFile,
        BE_InstructionType_Copy,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, 1, statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
    
    BEInstruction* copyInstruction = GetInstruction(list, 1);
    TestAssert(BE_StackEntriesAreEqual(copyInstruction->result, BE_Local(0)));
}

Test(InstructionGenerator_Expression_Parameter)
{
    // NOTE(Jens): struct Foo(var p) { var i = p; };
    // 0: jump         :: 3
    // 1: struct-index :: 0
    // 2: copy         :: l1, l0
    // 3: jump         :: r
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[3];
    BE_ClearStructToZero(statements);
    
    BE_StatementType statementTypes[3] = {
        BE_StatementType_StructHeader,
        BE_StatementType_VariableDeclaration,
        BE_StatementType_EndOf_Struct,
    };
    
    BE_NameMapNode param;
    BE_ClearStructToZero(param);
    param.value = BE_StrLiteral("p");
    
    statements[0].structHeader.name = BE_StrLiteral("Foo");
    statements[0].structHeader.parameters.count = 1;
    statements[0].structHeader.parameters.root = &param;
    
    statements[1].variableDeclaration.name = BE_StrLiteral("i");
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[1].variableDeclaration.initializationExpression, 0);
    BE_PushParameter(&statements[1].variableDeclaration.initializationExpression, 0);
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Jump,
        BE_InstructionType_StructIndex,
        BE_InstructionType_Copy,
        BE_InstructionType_Jump,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, 3, statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
    
    BEInstruction* copyInstruction = GetInstruction(list, 2);
    TestAssert(BE_StackEntriesAreEqual(copyInstruction->result, BE_Local(1)));
}

Test(InstructionGenerator_Expression_Load)
{
    // NOTE(Jens): var i = load(u(4));
    // 0: set          :: l2, 4
    // 1: fetch-memory :: l1, l2, (unsigned, little)
    // 2: copy         :: l0, l1
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[1];
    BE_ClearStructToZero(statements);
    
    BE_StatementType statementTypes[1] = {
        BE_StatementType_VariableDeclaration,
    };
    
    statements[0].variableDeclaration.name = BE_StrLiteral("i");
    
    BE_Expression address;
    BE_ClearStructToZero(address);
    
    BE_Expression size;
    BE_ClearStructToZero(size);
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &size, 0);
    BE_PushConstantInteger(&size, 4);
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[0].variableDeclaration.initializationExpression, 0);
    BE_PushLoad(&statements[0].variableDeclaration.initializationExpression, address, size, 0);
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Set,
        BE_InstructionType_FetchMemory,
        BE_InstructionType_Copy,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, 1, statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
    
    {
        BEInstruction* fetchInstruction = GetInstruction(list, 1);
        TestAssert(BE_StackEntriesAreEqual(fetchInstruction->result, BE_Local(1)));
        TestAssert(BE_StackEntriesAreEqual(fetchInstruction->fetchMemory.size, BE_Local(2)));
        TestAssert(fetchInstruction->fetchMemory.flags == 0);
    }
    
    {
        BEInstruction* copyInstruction = GetInstruction(list, 2);
        TestAssert(BE_StackEntriesAreEqual(copyInstruction->result, BE_Local(0)));
    }
}

Test(InstructionGenerator_Expression_Load_Address)
{
    // NOTE(Jens): var i = load(@(16) u(4));
    // 0: copy         :: l2, @
    // 1: set          :: @, 16
    // 2: set          :: l3, 4
    // 3: fetch-memory :: l1, l3, (unsigned, little)
    // 4: copy         :: @, l2
    // 5: copy         :: l0, l1
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[1];
    BE_ClearStructToZero(statements);
    
    BE_StatementType statementTypes[1] = {
        BE_StatementType_VariableDeclaration,
    };
    
    statements[0].variableDeclaration.name = BE_StrLiteral("i");
    
    BE_Expression address;
    BE_ClearStructToZero(address);
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &address, 0);
    BE_PushConstantInteger(&address, 16);
    
    BE_Expression size;
    BE_ClearStructToZero(size);
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &size, 0);
    BE_PushConstantInteger(&size, 4);
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[0].variableDeclaration.initializationExpression, 0);
    BE_PushLoad(&statements[0].variableDeclaration.initializationExpression, address, size, 0);
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Copy,
        BE_InstructionType_Set,
        BE_InstructionType_Set,
        BE_InstructionType_FetchMemory,
        BE_InstructionType_Copy,
        BE_InstructionType_Copy,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, 1, statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
    
    {
        BEInstruction* fetchInstruction = GetInstruction(list, 3);
        TestAssert(BE_StackEntriesAreEqual(fetchInstruction->result, BE_Local(1)));
        TestAssert(BE_StackEntriesAreEqual(fetchInstruction->fetchMemory.size, BE_Local(3)));
        TestAssert(fetchInstruction->fetchMemory.flags == 0);
    }
    
    {
        BEInstruction* copyInstruction = GetInstruction(list, 5);
        TestAssert(BE_StackEntriesAreEqual(copyInstruction->result, BE_Local(0)));
    }
}

Test(InstructionGenerator_Expression_HasType)
{
    // NOTE(Jens): struct (Cat, 123) Foo {}; var i = has_type(Cat, 8);
    // 0: jump              :: 2
    // 1: struct-index      :: 0
    // 2: jump              :: r
    // 3: set               :: l2, 8
    // 4: has-category-type :: l1, 0, l2
    // 5: copy              :: l0, l1
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[3];
    BE_ClearStructToZero(statements);
    
    BE_StatementType statementTypes[3] = {
        BE_StatementType_StructHeader,
        BE_StatementType_EndOf_Struct,
        BE_StatementType_VariableDeclaration,
    };
    
    statements[0].structHeader.name = BE_StrLiteral("Foo");
    statements[0].structHeader.category = BE_StrLiteral("Cat");
    statements[0].structHeader.categoryNumber = 123;
    
    statements[2].variableDeclaration.name = BE_StrLiteral("i");
    
    BE_Expression categoryNumber;
    BE_ClearStructToZero(categoryNumber);
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &categoryNumber, 0);
    BE_PushConstantInteger(&categoryNumber, 8);
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[2].variableDeclaration.initializationExpression, 0);
    BE_PushHasType(&statements[2].variableDeclaration.initializationExpression, 0, categoryNumber);
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Jump,
        BE_InstructionType_StructIndex,
        BE_InstructionType_Jump,
        BE_InstructionType_Set,
        BE_InstructionType_HasType,
        BE_InstructionType_Copy,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, 3, statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
    
    {
        BEInstruction* copyInstruction = GetInstruction(list, 5);
        TestAssert(BE_StackEntriesAreEqual(copyInstruction->result, BE_Local(0)));
    }
}

Test(InstructionGenerator_Expression_SizeOf)
{
    // NOTE(Jens): struct Foo {}; var i = sizeof(Foo);
    //  0: jump          :: 2
    //  1: struct-index  :: 0
    //  2: jump          :: r
    //  3: copy          :: l2, @
    //  4: hide-members
    //  5: push-frame
    //  6: set           :: r, 7
    //  7: jump          :: 1
    //  8: pop-frame
    //  9: show-members
    // 10: sub           :: l1, @, l2
    // 11: copy          :: @, l2
    // 12: copy          :: l0, l1
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[3];
    BE_ClearStructToZero(statements);
    
    BE_StatementType statementTypes[3] = {
        BE_StatementType_StructHeader,
        BE_StatementType_EndOf_Struct,
        BE_StatementType_VariableDeclaration,
    };
    
    statements[0].structHeader.name = BE_StrLiteral("Foo");
    
    BE_Expression address;
    BE_ClearStructToZero(address);
    
    statements[2].variableDeclaration.name = BE_StrLiteral("i");
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[2].variableDeclaration.initializationExpression, 0);
    BE_PushSizeOf(&statements[2].variableDeclaration.initializationExpression, 0, address);
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Jump,
        BE_InstructionType_StructIndex,
        BE_InstructionType_Jump,
        BE_InstructionType_Copy,
        BE_InstructionType_HideMembers,
        BE_InstructionType_PushFrame,
        BE_InstructionType_Set,
        BE_InstructionType_Jump,
        BE_InstructionType_PopFrame,
        BE_InstructionType_ShowMembers,
        BE_InstructionType_Sub,
        BE_InstructionType_Copy,
        BE_InstructionType_Copy,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, 3, statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
    
    {
        BEInstruction* copyInstruction = GetInstruction(list, 12);
        TestAssert(BE_StackEntriesAreEqual(copyInstruction->result, BE_Local(0)));
    }
}

Test(InstructionGenerator_Expression_SizeOf_Address)
{
    // NOTE(Jens): struct Foo {}; var i = sizeof(@(64) Foo);
    //  0: jump          :: 2
    //  1: struct-index  :: 0
    //  2: jump          :: r
    //  3: copy          :: l2, @
    //  4: set           :: @, 64
    //  5: copy          :: l3, @
    //  6: hide-members
    //  7: push-frame
    //  8: set           :: r, 9
    //  9: jump          :: 1
    // 10: pop-frame
    // 11: show-members
    // 12: sub           :: l1, @, l3
    // 13: copy          :: @, l2
    // 14: copy          :: l0, l1
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[3];
    BE_ClearStructToZero(statements);
    
    BE_StatementType statementTypes[3] = {
        BE_StatementType_StructHeader,
        BE_StatementType_EndOf_Struct,
        BE_StatementType_VariableDeclaration,
    };
    
    statements[0].structHeader.name = BE_StrLiteral("Foo");
    
    BE_Expression address;
    BE_ClearStructToZero(address);
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &address, 0);
    BE_PushConstantInteger(&address, 64);
    
    statements[2].variableDeclaration.name = BE_StrLiteral("i");
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[2].variableDeclaration.initializationExpression, 0);
    BE_PushSizeOf(&statements[2].variableDeclaration.initializationExpression, 0, address);
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Jump,
        BE_InstructionType_StructIndex,
        BE_InstructionType_Jump,
        BE_InstructionType_Copy,
        BE_InstructionType_Set,
        BE_InstructionType_Copy,
        BE_InstructionType_HideMembers,
        BE_InstructionType_PushFrame,
        BE_InstructionType_Set,
        BE_InstructionType_Jump,
        BE_InstructionType_PopFrame,
        BE_InstructionType_ShowMembers,
        BE_InstructionType_Sub,
        BE_InstructionType_Copy,
        BE_InstructionType_Copy,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, 3, statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
    
    {
        BEInstruction* copyInstruction = GetInstruction(list, 14);
        TestAssert(BE_StackEntriesAreEqual(copyInstruction->result, BE_Local(0)));
    }
}

Test(InstructionGenerator_Expression_Member)
{
    // NOTE(Jens): struct Foo { u(4) value; var i = value; };
    // 0: jump            :: 7
    // 1: struct-index    :: 0
    // 2: set             :: l0, 4
    // 3: scalar-u        :: 0, l0, 0
    // 4: add             :: @, @, l0
    // 5: fetch-member    :: l1, (0)
    // 6: copy            :: l0, l1
    // 7: jump            :: r
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[4];
    BE_ClearStructToZero(statements);
    
    BE_StatementType statementTypes[4] = {
        BE_StatementType_StructHeader,
        BE_StatementType_MemberDeclaration,
        BE_StatementType_VariableDeclaration,
        BE_StatementType_EndOf_Struct,
    };
    
    statements[0].structHeader.name = BE_StrLiteral("Foo");
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[1].memberDeclaration.type.scalarVariant.sizeExpression, 0);
    BE_PushConstantInteger(&statements[1].memberDeclaration.type.scalarVariant.sizeExpression, 4);
    
    BE_MemberPathNode node;
    BE_ClearStructToZero(node);
    node.memberIndex = 0;
    
    BE_MemberPath path;
    BE_ClearStructToZero(path);
    path.head = path.tail = &node;
    path.count = 1;
    
    statements[2].variableDeclaration.name = BE_StrLiteral("i");
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[2].variableDeclaration.initializationExpression, 0);
    BE_PushMember(&statements[2].variableDeclaration.initializationExpression, path);
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Jump,
        BE_InstructionType_StructIndex,
        BE_InstructionType_Set,
        BE_InstructionType_ExportScalar_u,
        BE_InstructionType_Add,
        BE_InstructionType_FetchMemberValue,
        BE_InstructionType_Copy,
        BE_InstructionType_Jump,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, 4, statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
}

Test(InstructionGenerator_Expression_ArrayCount)
{
    // NOTE(Jens): struct Foo { u(4) values[123]; var i = array_count(values); };
    // 0: jump               :: 7
    // 1: struct-index       :: 0
    // 2: set                :: l0, 4
    // 3: set                :: l1, 123
    // 4: scalar-u           :: 0, l0, l1
    // 5: mul                :: l0, l0, l1
    // 6: add                :: @, @, l0
    // 7: fetch-array-count  :: l1 (0)
    // 8: copy               :: l0, l1
    // 9: jump               :: r
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[4];
    BE_ClearStructToZero(statements);
    
    BE_StatementType statementTypes[4] = {
        BE_StatementType_StructHeader,
        BE_StatementType_MemberDeclaration,
        BE_StatementType_VariableDeclaration,
        BE_StatementType_EndOf_Struct,
    };
    
    statements[0].structHeader.name = BE_StrLiteral("Foo");
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[1].memberDeclaration.type.scalarVariant.sizeExpression, 0);
    BE_PushConstantInteger(&statements[1].memberDeclaration.type.scalarVariant.sizeExpression, 4);
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[1].memberDeclaration.arrayCount, 0);
    BE_PushConstantInteger(&statements[1].memberDeclaration.arrayCount, 123);
    statements[1].memberDeclaration.isArray = 1;
    
    BE_MemberPathNode node = { 0 };
    node.memberIndex = 0;
    
    BE_MemberPath path = { 0 };
    path.head = path.tail = &node;
    path.count = 1;
    
    statements[2].variableDeclaration.name = BE_StrLiteral("i");
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[2].variableDeclaration.initializationExpression, 0);
    BE_PushArrayCount(&statements[2].variableDeclaration.initializationExpression, path);
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Jump,
        BE_InstructionType_StructIndex,
        BE_InstructionType_Set,
        BE_InstructionType_Set,
        BE_InstructionType_ExportScalar_u,
        BE_InstructionType_Mul,
        BE_InstructionType_Add,
        BE_InstructionType_FetchArrayCount,
        BE_InstructionType_Copy,
        BE_InstructionType_Jump,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, 4, statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
}

Test(InstructionGenerator_ConstantExpression_DivZero)
{
    // NOTE(Jens): var i = 1 / 0;
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[1];
    BE_ClearStructToZero(statements);
    
    BE_StatementType statementTypes[1] = {
        BE_StatementType_VariableDeclaration,
    };
    
    statements[0].variableDeclaration.name = BE_StrLiteral("i");
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[0].variableDeclaration.initializationExpression, 0);
    BE_PushConstantInteger(&statements[0].variableDeclaration.initializationExpression, 1);
    BE_PushOperator(tester.alloc.allocator, &tester.expressions, &statements[0].variableDeclaration.initializationExpression, BE_Punctuation("/"));
    BE_PushConstantInteger(&statements[0].variableDeclaration.initializationExpression, 0);
    
    TestAssert(GetErrorForStatements(&tester, 1, statementTypes, statements) == BE_ArithmeticError_DivByZero);
}

Test(InstructionGenerator_ConstantExpression_ModZero)
{
    // NOTE(Jens): var i = 1 % 0;
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[1];
    BE_ClearStructToZero(statements);
    
    BE_StatementType statementTypes[1] = {
        BE_StatementType_VariableDeclaration,
    };
    
    statements[0].variableDeclaration.name = BE_StrLiteral("i");
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[0].variableDeclaration.initializationExpression, 0);
    BE_PushConstantInteger(&statements[0].variableDeclaration.initializationExpression, 1);
    BE_PushOperator(tester.alloc.allocator, &tester.expressions, &statements[0].variableDeclaration.initializationExpression, BE_Punctuation("%"));
    BE_PushConstantInteger(&statements[0].variableDeclaration.initializationExpression, 0);
    
    TestAssert(GetErrorForStatements(&tester, 1, statementTypes, statements) == BE_ArithmeticError_ModByZero);
}

Test(InstructionGenerator_Plot_X)
{
    // NOTE(Jens): plot_x(123, 0.5);
    // 0: set    :: l0, 123
    // 1: set    :: l1, f0.5
    // 2: plot-x :: l0, l1
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[1] = { 0 };
    
    BE_StatementType statementTypes[1] = {
        BE_StatementType_Plot_X
    };
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[0].plot.plotId, 0);
    BE_PushConstantInteger(&statements[0].plot.plotId, 123);
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[0].plot.value_f64, 1);
    BE_PushConstantFloat(&statements[0].plot.value_f64, 0.5);
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Set,
        BE_InstructionType_Set,
        BE_InstructionType_Plot_F8_X,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, BE_ArrayCount(statements), statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
}

Test(InstructionGenerator_Plot_Y)
{
    // NOTE(Jens): plot_y(123, 0.5);
    // 0: set    :: l0, 123
    // 1: set    :: l1, f0.5
    // 2: plot-y :: l0, l1
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[1] = { 0 };
    
    BE_StatementType statementTypes[1] = {
        BE_StatementType_Plot_Y
    };
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[0].plot.plotId, 0);
    BE_PushConstantInteger(&statements[0].plot.plotId, 123);
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[0].plot.value_f64, 1);
    BE_PushConstantFloat(&statements[0].plot.value_f64, 0.5);
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Set,
        BE_InstructionType_Set,
        BE_InstructionType_Plot_F8_Y,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, BE_ArrayCount(statements), statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
}

Test(InstructionGenerator_Plot_XY)
{
    // NOTE(Jens): plot_xy(123, 0.5, 0.25);
    // 0: set    :: l0, 123
    // 1: set    :: l1, f0.5
    // 2: set    :: l2, f0.25
    // 3: plot-x :: l0, l1
    // 4: plot-y :: l0, l2
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[1] = { 0 };
    
    BE_StatementType statementTypes[1] = {
        BE_StatementType_Plot_XY
    };
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[0].plotXY.plotId, 0);
    BE_PushConstantInteger(&statements[0].plotXY.plotId, 123);
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[0].plotXY.x, 1);
    BE_PushConstantFloat(&statements[0].plotXY.x, 0.5);
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[0].plotXY.y, 1);
    BE_PushConstantFloat(&statements[0].plotXY.y, 0.25);
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Set,
        BE_InstructionType_Set,
        BE_InstructionType_Set,
        BE_InstructionType_Plot_F8_X,
        BE_InstructionType_Plot_F8_Y,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, BE_ArrayCount(statements), statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
}

Test(InstructionGenerator_PlotMember_X)
{
    // NOTE(Jens): struct Foo { u(4) value[12]; plot_member_x(123, value); };
    //  0: jump               :: 19
    //  1: struct-index       :: 0
    //  2: set                :: l0, 4
    //  3: set                :: l1, 12
    //  4: scalar-u           :: 0, l0, l1
    //  5: mul                :: l0, l0, l1
    //  6: add                :: @, @, l0
    //  7: set                :: l0, 123
    //  8: fetch-array-count  :: l4, (0)
    //  9: set                :: l2, -1
    // 10: set                :: l3, 1
    // 11: add                :: l2, l3
    // 12: comp-ge            :: l5, l2, l4
    // 13: cnd-jump           :: 17, l8
    // 14: fetch-member-flt   :: l6 (0 [l2])
    // 15: plot-x             :: l0, l6
    // 16: jump               :: 11
    // 17: jump               :: r
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[4] = { 0 };
    
    BE_StatementType statementTypes[4] = {
        BE_StatementType_StructHeader,
        BE_StatementType_MemberDeclaration,
        BE_StatementType_Plot_Member_X,
        BE_StatementType_EndOf_Struct,
    };
    
    statements[0].structHeader.name = BE_StrLiteral("Foo");
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[1].memberDeclaration.type.scalarVariant.sizeExpression, 0);
    BE_PushConstantInteger(&statements[1].memberDeclaration.type.scalarVariant.sizeExpression, 4);
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[1].memberDeclaration.arrayCount, 0);
    BE_PushConstantInteger(&statements[1].memberDeclaration.arrayCount, 12);
    statements[1].memberDeclaration.isArray = 1;
    
    BE_MemberPathNode node = { 0 };
    node.memberIndex = 0;
    
    BE_MemberPath path = { 0 };
    path.head = path.tail = &node;
    path.count = 1;
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[2].plotMember.plotId, 0);
    BE_PushConstantInteger(&statements[2].plotMember.plotId, 123);
    statements[2].plotMember.member = path;
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Jump,
        BE_InstructionType_StructIndex,
        BE_InstructionType_Set,
        BE_InstructionType_Set,
        BE_InstructionType_ExportScalar_u,
        BE_InstructionType_Mul,
        BE_InstructionType_Add,
        BE_InstructionType_Set,
        BE_InstructionType_FetchArrayCount,
        BE_InstructionType_Set,
        BE_InstructionType_Set,
        BE_InstructionType_Add,
        BE_InstructionType_GreaterOrEqual,
        BE_InstructionType_Jump,
        BE_InstructionType_FetchMemberValueFloat,
        BE_InstructionType_Plot_F8_X,
        BE_InstructionType_Jump,
        BE_InstructionType_Jump,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, 4, statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
}

Test(InstructionGenerator_PlotMember_Y)
{
    // NOTE(Jens): struct Foo { u(4) value[12]; plot_member_x(123, value); };
    //  0: jump               :: 19
    //  1: struct-index       :: 0
    //  2: set                :: l0, 4
    //  3: set                :: l1, 12
    //  4: scalar-u           :: 0, l0, l1
    //  5: mul                :: l0, l0, l1
    //  6: add                :: @, @, l0
    //  7: set                :: l0, 123
    //  8: fetch-array-count  :: l4, (0)
    //  9: set                :: l2, -1
    // 10: set                :: l3, 1
    // 11: add                :: l2, l3
    // 12: comp-ge            :: l5, l2, l4
    // 13: cnd-jump           :: 17, l8
    // 14: fetch-member-flt   :: l6 (0 [l2])
    // 15: plot-x             :: l0, l6
    // 16: jump               :: 11
    // 17: jump               :: r
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[4] = { 0 };
    
    BE_StatementType statementTypes[4] = {
        BE_StatementType_StructHeader,
        BE_StatementType_MemberDeclaration,
        BE_StatementType_Plot_Member_Y,
        BE_StatementType_EndOf_Struct,
    };
    
    statements[0].structHeader.name = BE_StrLiteral("Foo");
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[1].memberDeclaration.type.scalarVariant.sizeExpression, 0);
    BE_PushConstantInteger(&statements[1].memberDeclaration.type.scalarVariant.sizeExpression, 4);
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[1].memberDeclaration.arrayCount, 0);
    BE_PushConstantInteger(&statements[1].memberDeclaration.arrayCount, 12);
    statements[1].memberDeclaration.isArray = 1;
    
    BE_MemberPathNode node = { 0 };
    node.memberIndex = 0;
    
    BE_MemberPath path = { 0 };
    path.head = path.tail = &node;
    path.count = 1;
    
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[2].plotMember.plotId, 0);
    BE_PushConstantInteger(&statements[2].plotMember.plotId, 123);
    statements[2].plotMember.member = path;
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_Jump,
        BE_InstructionType_StructIndex,
        BE_InstructionType_Set,
        BE_InstructionType_Set,
        BE_InstructionType_ExportScalar_u,
        BE_InstructionType_Mul,
        BE_InstructionType_Add,
        BE_InstructionType_Set,
        BE_InstructionType_FetchArrayCount,
        BE_InstructionType_Set,
        BE_InstructionType_Set,
        BE_InstructionType_Add,
        BE_InstructionType_GreaterOrEqual,
        BE_InstructionType_Jump,
        BE_InstructionType_FetchMemberValueFloat,
        BE_InstructionType_Plot_F8_Y,
        BE_InstructionType_Jump,
        BE_InstructionType_Jump,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, 4, statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
}

Test(InstructionGenerator_CreatePlot)
{
    // NOTE(Jens): var i = create_plot();
    // 0: create-plot     :: l1
    // 1: copy            :: l0, l1
    
    GenerateInstructionsTester tester = StartGenerateInstructionsTest();
    
    BE_Statement statements[1] = { 0 };
    
    BE_StatementType statementTypes[1] = {
        BE_StatementType_VariableDeclaration,
    };
    
    statements[0].variableDeclaration.name = BE_StrLiteral("i");
    statements[0].variableDeclaration.index = 0;
    BE_InitExpression(tester.alloc.allocator, &tester.expressions, &statements[0].variableDeclaration.initializationExpression, 0);
    BE_PushCreatePlot(&statements[0].variableDeclaration.initializationExpression);
    
    BE_InstructionType expectedInstructions[] = {
        BE_InstructionType_CreatePlot,
        BE_InstructionType_Copy,
    };
    
    BE_InstructionList list;
    TestAssert(GenerateInstructionsForStatements(&list, &tester, BE_ArrayCount(statements), statementTypes, statements, BE_ArrayCount(expectedInstructions), expectedInstructions));
}

static void DebugPrintStackEntry(FILE* out, BERuntime* runtime, BE_StackEntry entry)
{
    switch (entry.stack)
    {
        case BE_Stack_Invalid:
        {
            fprintf(out, "(invalid)");
        } break;
        
        case BE_Stack_Global:
        {
            if (entry.index >= BE_GlobalSymbol_Count)
            {
                fprintf(out, "g(%u)", entry.index - BE_GlobalSymbol_Count);
            }
            else
            {
                if (entry.index == BE_GlobalSymbol_MemberOffset)
                {
                    fprintf(out, "@");
                }
                else
                {
                    fprintf(out, "?");
                }
            }
        } break;
        
        case BE_Stack_Local:
        {
            if (entry.index >= BE_LocalSymbol_Count)
            {
                fprintf(out, "l(%u)", entry.index - BE_GlobalSymbol_Count);
            }
            else
            {
                if (entry.index == BE_LocalSymbol_ReturnAddress)
                {
                    fprintf(out, "r");
                }
                else if (entry.index == BE_LocalSymbol_ArrayIndex)
                {
                    fprintf(out, "i");
                }
                else 
                {
                    fprintf(out, "?");
                }
            } break;
        } break;
        
        case BE_Stack_Parent:
        {
            if (entry.index >= BE_LocalSymbol_Count)
            {
                fprintf(out, "p(%u)", entry.index - BE_LocalSymbol_Count);
            }
            else
            {
                fprintf(out, "?");
            }
        } break;
    }
    
    if (runtime && entry.stack)
    {
        fprintf(out, "=%lli", *BEGetPtr(runtime, entry));
    }
}

static void DebugPrintInstruction(BEInstruction* instruction, BERuntime* runtime)
{
    switch (instruction->type)
    {
        default:
        {
            fprintf(stdout, "\?\?\?");
        } break;
        
        case BE_InstructionType_Set:
        {
            DebugPrintStackEntry(stdout, 0, instruction->result);
            fprintf(stdout, " <- %lli", instruction->set.value);
        } break;
        
        case BE_InstructionType_Negate:
        {
            DebugPrintStackEntry(stdout, 0, instruction->result);
            fprintf(stdout, " <- -");
            DebugPrintStackEntry(stdout, runtime, instruction->unaryValue);
        } break;
        
        case BE_InstructionType_LogicalNot:
        {
            DebugPrintStackEntry(stdout, 0, instruction->result);
            fprintf(stdout, " <- !");
            DebugPrintStackEntry(stdout, runtime, instruction->unaryValue);
        } break;
        
        case BE_InstructionType_BitwiseNot:
        {
            DebugPrintStackEntry(stdout, 0, instruction->result);
            fprintf(stdout, " <- ~");
            DebugPrintStackEntry(stdout, runtime, instruction->unaryValue);
        } break;
        
        case BE_InstructionType_Abs:
        {
            DebugPrintStackEntry(stdout, 0, instruction->result);
            fprintf(stdout, " <- abs(");
            DebugPrintStackEntry(stdout, runtime, instruction->unaryValue);
            fprintf(stdout, ")");
        } break;
        
        case BE_InstructionType_Copy:
        {
            DebugPrintStackEntry(stdout, 0, instruction->result);
            fprintf(stdout, " <- ");
            DebugPrintStackEntry(stdout, runtime, instruction->unaryValue);
        } break;
        
        case BE_InstructionType_Add:
        {
            DebugPrintStackEntry(stdout, 0, instruction->result);
            fprintf(stdout, " <- ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.left);
            fprintf(stdout, " + ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_Sub:
        {
            DebugPrintStackEntry(stdout, 0, instruction->result);
            fprintf(stdout, " <- ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.left);
            fprintf(stdout, " - ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_Mul:
        {
            DebugPrintStackEntry(stdout, 0, instruction->result);
            fprintf(stdout, " <- ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.left);
            fprintf(stdout, " * ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_Div:
        {
            DebugPrintStackEntry(stdout, 0, instruction->result);
            fprintf(stdout, " <- ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.left);
            fprintf(stdout, " / ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_Mod:
        {
            DebugPrintStackEntry(stdout, 0, instruction->result);
            fprintf(stdout, " <- ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.left);
            fprintf(stdout, " %% ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_Less:
        {
            DebugPrintStackEntry(stdout, 0, instruction->result);
            fprintf(stdout, " <- ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.left);
            fprintf(stdout, " < ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_LessOrEqual:
        {
            DebugPrintStackEntry(stdout, 0, instruction->result);
            fprintf(stdout, " <- ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.left);
            fprintf(stdout, " <= ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_Equal:
        {
            DebugPrintStackEntry(stdout, 0, instruction->result);
            fprintf(stdout, " <- ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.left);
            fprintf(stdout, " == ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_Greater:
        {
            DebugPrintStackEntry(stdout, 0, instruction->result);
            fprintf(stdout, " <- ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.left);
            fprintf(stdout, " > ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_GreaterOrEqual:
        {
            DebugPrintStackEntry(stdout, 0, instruction->result);
            fprintf(stdout, " <- ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.left);
            fprintf(stdout, " >= ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_NotEqual:
        {
            DebugPrintStackEntry(stdout, 0, instruction->result);
            fprintf(stdout, " <- ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.left);
            fprintf(stdout, " != ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_BitshiftLeft:
        {
            DebugPrintStackEntry(stdout, 0, instruction->result);
            fprintf(stdout, " <- ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.left);
            fprintf(stdout, " << ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_BitshiftRight:
        {
            DebugPrintStackEntry(stdout, 0, instruction->result);
            fprintf(stdout, " <- ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.left);
            fprintf(stdout, " >> ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_BitwiseAnd:
        {
            DebugPrintStackEntry(stdout, 0, instruction->result);
            fprintf(stdout, " <- ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.left);
            fprintf(stdout, " & ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_BitwiseOr:
        {
            DebugPrintStackEntry(stdout, 0, instruction->result);
            fprintf(stdout, " <- ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.left);
            fprintf(stdout, " | ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_BitwiseXOr:
        {
            DebugPrintStackEntry(stdout, 0, instruction->result);
            fprintf(stdout, " <- ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.left);
            fprintf(stdout, " ^ ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_LogicalAnd:
        {
            DebugPrintStackEntry(stdout, 0, instruction->result);
            fprintf(stdout, " <- ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.left);
            fprintf(stdout, " && ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_LogicalOr:
        {
            DebugPrintStackEntry(stdout, 0, instruction->result);
            fprintf(stdout, " <- ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.left);
            fprintf(stdout, " || ");
            DebugPrintStackEntry(stdout, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_PushFrame:
        {
            fprintf(stdout, "push-frame (%u bytes)", instruction->pushFrame.sizeRequiredInBytes);
        } break;
        
        case BE_InstructionType_PopFrame:
        {
            fprintf(stdout, "pop-frame");
        } break;
        
        case BE_InstructionType_PushStruct:
        {
            fprintf(stdout, "push-struct %u", instruction->beginStruct.memberIndex);
            if (instruction->beginStruct.arrayCount.stack)
            {
                fprintf(stdout, " [");
                DebugPrintStackEntry(stdout, runtime, instruction->beginStruct.arrayCount);
                fprintf(stdout, "]");
            }
        } break;
        
        case BE_InstructionType_PopStruct:
        {
            fprintf(stdout, "pop-struct");
        } break;
        
        case BE_InstructionType_ExportScalar_u:
        {
            fprintf(stdout, "scalar-u %u (%s) ", instruction->scalarExport.memberIndex, instruction->scalarExport.isBigEndian ? "big" : "little");
            DebugPrintStackEntry(stdout, runtime, instruction->scalarExport.size);
            if (instruction->scalarExport.arrayCount.stack)
            {
                fprintf(stdout, " [");
                DebugPrintStackEntry(stdout, runtime, instruction->scalarExport.arrayCount);
                fprintf(stdout, "]");
            }
        } break;
        
        case BE_InstructionType_ExportScalar_s:
        {
            fprintf(stdout, "scalar-s %u (%s) ", instruction->scalarExport.memberIndex, instruction->scalarExport.isBigEndian ? "big" : "little");
            DebugPrintStackEntry(stdout, runtime, instruction->scalarExport.size);
            if (instruction->scalarExport.arrayCount.stack)
            {
                fprintf(stdout, " [");
                DebugPrintStackEntry(stdout, runtime, instruction->scalarExport.arrayCount);
                fprintf(stdout, "]");
            }
        } break;
        
        case BE_InstructionType_ExportScalar_f:
        {
            fprintf(stdout, "scalar-f %u (%s) ", instruction->scalarExport.memberIndex, instruction->scalarExport.isBigEndian ? "big" : "little");
            DebugPrintStackEntry(stdout, runtime, instruction->scalarExport.size);
            if (instruction->scalarExport.arrayCount.stack)
            {
                fprintf(stdout, " [");
                DebugPrintStackEntry(stdout, runtime, instruction->scalarExport.arrayCount);
                fprintf(stdout, "]");
            }
        } break;
        
        case BE_InstructionType_ExportScalar_string:
        {
            fprintf(stdout, "scalar-string %u (%s) ", instruction->scalarExport.memberIndex, instruction->scalarExport.isBigEndian ? "big" : "little");
            DebugPrintStackEntry(stdout, runtime, instruction->scalarExport.size);
            if (instruction->scalarExport.arrayCount.stack)
            {
                fprintf(stdout, " [");
                DebugPrintStackEntry(stdout, runtime, instruction->scalarExport.arrayCount);
                fprintf(stdout, "]");
            }
        } break;
        
        case BE_InstructionType_ExportScalar_raw:
        {
            fprintf(stdout, "scalar-raw %u (%s) ", instruction->scalarExport.memberIndex, instruction->scalarExport.isBigEndian ? "big" : "little");
            DebugPrintStackEntry(stdout, runtime, instruction->scalarExport.size);
            if (instruction->scalarExport.arrayCount.stack)
            {
                fprintf(stdout, " [");
                DebugPrintStackEntry(stdout, runtime, instruction->scalarExport.arrayCount);
                fprintf(stdout, "]");
            }
        } break;
        
        case BE_InstructionType_FetchMemberValue:
        {
            DebugPrintStackEntry(stdout, 0, instruction->result);
            fprintf(stdout, " <- fetch-member (");
            
            for (BEu32 i = 0; i < instruction->fetchMember.pathCount; ++i)
            {
                fprintf(stdout, "%u", instruction->fetchMember.path[i].memberIndex);
                
                if (instruction->fetchMember.path[i].arrayIndex.stack)
                {
                    fprintf(stdout, " [");
                    DebugPrintStackEntry(stdout, runtime, instruction->fetchMember.path[i].arrayIndex);
                    fprintf(stdout, "]");
                }
                
                if (i == instruction->fetchMember.pathCount - 1)
                {
                    fprintf(stdout, ")");
                }
                else
                {
                    fprintf(stdout, ", ");
                }
            }
        } break;
        
        case BE_InstructionType_FetchMemory:
        {
            DebugPrintStackEntry(stdout, 0, instruction->result);
            fprintf(stdout, " <- load ");
            DebugPrintStackEntry(stdout, runtime, instruction->fetchMemory.size);
        } break;
        
        case BE_InstructionType_Jump:
        case BE_InstructionType_JumpDynamic:
        case BE_InstructionType_JumpConditional:
        case BE_InstructionType_JumpDynamicConditional:
        {
            if (instruction->jump.condition.stack)
            {
                fprintf(stdout, "cnd-jump ");
                DebugPrintStackEntry(stdout, runtime, instruction->jump.condition);
                fprintf(stdout, " ");
            }
            else
            {
                fprintf(stdout, "jump ");
            }
            
            if (instruction->jump.dynamicNewInstructionIndex.stack)
            {
                DebugPrintStackEntry(stdout, runtime, instruction->jump.dynamicNewInstructionIndex);
            }
            else
            {
                fprintf(stdout, "%u", instruction->jump.newInstructionIndex);
            }
        } break;
        
        case BE_InstructionType_FetchSizeOfFile:
        {
            DebugPrintStackEntry(stdout, 0, instruction->result);
            fprintf(stdout, " <- size-of-file");
        } break;
        
        case BE_InstructionType_HideMembers:
        {
            fprintf(stdout, "hide-members");
        } break;
        
        case BE_InstructionType_ShowMembers:
        {
            fprintf(stdout, "show-members");
        } break;
        
        case BE_InstructionType_FetchStartOfType:
        {
            DebugPrintStackEntry(stdout, 0, instruction->result);
            fprintf(stdout, " <- start-of-category-type-%u ", instruction->categoryQuery.categoryIndex);
            DebugPrintStackEntry(stdout, runtime, instruction->categoryQuery.typeNumber);
        } break;
        
        case BE_InstructionType_HasType:
        {
            DebugPrintStackEntry(stdout, 0, instruction->result);
            fprintf(stdout, " <- has-category-type-%u ", instruction->categoryQuery.categoryIndex);
            DebugPrintStackEntry(stdout, runtime, instruction->categoryQuery.typeNumber);
        } break;
        
        case BE_InstructionType_Assert:
        {
            fprintf(stdout, "assert ");
            DebugPrintStackEntry(stdout, runtime, instruction->assert.condition);
        } break;
        
        case BE_InstructionType_Print:
        {
            fprintf(stdout, "print %.*s, ", (int)instruction->print.prefixCharacterCount, instruction->print.prefixCharacters);
            DebugPrintStackEntry(stdout, runtime, instruction->print.value);
        } break;
        
        case BE_InstructionType_Breakpoint:
        {
            fprintf(stdout, "breakpoint");
        } break;
        
        case BE_InstructionType_StructIndex:
        {
            fprintf(stdout, "struct-index %u", instruction->structIndex.index);
        } break;
    }
    
    fprintf(stdout, "\n");
}

static BEbool InstructionsAreEqual(BEInstruction* a, BEInstruction* b)
{
    BEbool rVal = (a->type == b->type);
    
    if (rVal)
    {
        if (BE_InstructionType_FirstUnary <= a->type && a->type <= BE_InstructionType_LastUnary)
        {
            rVal = (BE_StackEntriesAreEqual(a->result, b->result)
                    && BE_StackEntriesAreEqual(a->unaryValue, b->unaryValue));
        }
        else if (BE_InstructionType_FirstBinary <= a->type && a->type <= BE_InstructionType_LastBinary)
        {
            rVal = (BE_StackEntriesAreEqual(a->result, b->result)
                    && BE_StackEntriesAreEqual(a->binary.left, b->binary.left)
                    && BE_StackEntriesAreEqual(a->binary.right, b->binary.right));
        }
        else if (a->type == BE_InstructionType_Set)
        {
            rVal = (BE_StackEntriesAreEqual(a->result, b->result)
                    && a->set.value == b->set.value);
        }
        else if (a->type == BE_InstructionType_ExportScalar_u
                 || a->type == BE_InstructionType_ExportScalar_s
                 || a->type == BE_InstructionType_ExportScalar_f
                 || a->type == BE_InstructionType_ExportScalar_string
                 || a->type == BE_InstructionType_ExportScalar_raw)
        {
            rVal = (a->scalarExport.memberIndex == b->scalarExport.memberIndex
                    && BE_StackEntriesAreEqual(a->scalarExport.size, b->scalarExport.size)
                    && BE_StackEntriesAreEqual(a->scalarExport.arrayCount, b->scalarExport.arrayCount));
        }
        else if (a->type == BE_InstructionType_FetchMemberValue)
        {
            rVal = (a->fetchMember.pathCount == b->fetchMember.pathCount);
            if (rVal)
            {
                for (BEu32 i = 0; i < a->fetchMember.pathCount; ++i)
                {
                    if (a->fetchMember.path[i].memberIndex != b->fetchMember.path[i].memberIndex 
                        || !BE_StackEntriesAreEqual(a->fetchMember.path[i].arrayIndex, b->fetchMember.path[i].arrayIndex))
                    {
                        rVal = 0;
                        break;
                    }
                }
            }
        }
        else if (a->type == BE_InstructionType_FetchArrayCount)
        {
            rVal = (a->fetchArrayCount.pathCount == b->fetchArrayCount.pathCount);
            if (rVal)
            {
                for (BEu32 i = 0; i < a->fetchArrayCount.pathCount; ++i)
                {
                    if (a->fetchArrayCount.path[i].memberIndex != b->fetchArrayCount.path[i].memberIndex 
                        || !BE_StackEntriesAreEqual(a->fetchArrayCount.path[i].arrayIndex, b->fetchArrayCount.path[i].arrayIndex))
                    {
                        rVal = 0;
                        break;
                    }
                }
            }
        }
        else if (a->type == BE_InstructionType_FetchMemory)
        {
            rVal = (BE_StackEntriesAreEqual(a->result, b->result)
                    && a->fetchMemory.flags == b->fetchMemory.flags
                    && BE_StackEntriesAreEqual(a->fetchMemory.size, b->fetchMemory.size));
        }
        else if (a->type == BE_InstructionType_Jump)
        {
            rVal = (BE_StackEntriesAreEqual(a->jump.dynamicNewInstructionIndex, b->jump.dynamicNewInstructionIndex)
                    && a->jump.newInstructionIndex == b->jump.newInstructionIndex
                    && BE_StackEntriesAreEqual(a->jump.condition, b->jump.condition));
        }
        else if (a->type == BE_InstructionType_Assert)
        {
            rVal = (BE_StackEntriesAreEqual(a->assert.condition, b->assert.condition));
        }
        else if (a->type == BE_InstructionType_Print)
        {
            BE_String strA;
            strA.count = a->print.prefixCharacterCount;
            strA.characters = a->print.prefixCharacters;
            
            BE_String strB;
            strB.count = b->print.prefixCharacterCount;
            strB.characters = b->print.prefixCharacters;
            
            rVal = (BE_StringsAreEqual(strA, strB)
                    && BE_StackEntriesAreEqual(a->print.value, b->print.value));
        }
        else if (a->type == BE_InstructionType_StructIndex)
        {
            rVal = (a->structIndex.index == b->structIndex.index);
        }
        else if (a->type == BE_InstructionType_Plot_F8_X || a->type == BE_InstructionType_Plot_F8_Y)
        {
            rVal = (BE_StackEntriesAreEqual(a->plot.plotId, b->plot.plotId) 
                    && BE_StackEntriesAreEqual(a->plot.value_f64, b->plot.value_f64));
        }
        else if (a->type == BE_InstructionType_CreatePlot)
        {
            rVal = BE_StackEntriesAreEqual(a->result, b->result);
        }
        else if (a->type == BE_InstructionType_IntToFloat)
        {
            rVal = BE_StackEntriesAreEqual(a->result, b->result) && BE_StackEntriesAreEqual(a->conversion.source, b->conversion.source);
        }
    }
    
    return rVal;
}

static BEbool TestSerializeSingleInstruction(BEInstruction instruction)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BE_InstructionEncoder encoder;
    BE_ClearStructToZero(encoder);
    
    BE_FeedInstructionEncoder(allocator, &encoder, &instruction);
    BE_EncodedInstructions encoded = BE_FlushInstructionBuffer(&encoder);
    
    BEu32 count = encoded.writtenBytes;
    BE_EncodedInstructionChunk* chunk = encoded.head;
    BEAssert(!chunk->next); // TODO(Jens): If triggered, flatten the buffer to a single array.
    
    BEu32 size;
    BEInstruction decodedInstruction = BEDecodeInstruction(&size, chunk->data);
    
    return size == count && InstructionsAreEqual(&decodedInstruction, &instruction);
}

Test(Serializer_Set)
{
    BEInstruction instruction;
    BE_ClearStructToZero(instruction);
    
    instruction.type = BE_InstructionType_Set;
    instruction.set.value = 123;
    instruction.result = BE_Local(4);
    
    TestAssert(TestSerializeSingleInstruction(instruction));
}

Test(Serializer_NegativeValues)
{
    BEInstruction instruction;
    BE_ClearStructToZero(instruction);
    
    instruction.type = BE_InstructionType_Set;
    instruction.set.value = -123;
    instruction.result = BE_Local(4);
    
    TestAssert(TestSerializeSingleInstruction(instruction));
}

Test(Serializer_Unary)
{
    BEInstruction instruction;
    BE_ClearStructToZero(instruction);
    
    instruction.type = BE_InstructionType_Negate;
    instruction.unaryValue = BE_Local(123);
    instruction.result = BE_Local(4);
    
    TestAssert(TestSerializeSingleInstruction(instruction));
}

Test(Serializer_Binary)
{
    BEInstruction instruction;
    BE_ClearStructToZero(instruction);
    
    instruction.type = BE_InstructionType_Negate;
    instruction.binary.left = BE_Local(123);
    instruction.binary.right = BE_Local(321);
    instruction.result = BE_Local(4);
    
    TestAssert(TestSerializeSingleInstruction(instruction));
}

Test(Serializer_PushFrame)
{
    BEInstruction instruction;
    BE_ClearStructToZero(instruction);
    
    instruction.type = BE_InstructionType_PushFrame;
    TestAssert(TestSerializeSingleInstruction(instruction));
}

Test(Serializer_PushStruct)
{
    BEInstruction instruction;
    BE_ClearStructToZero(instruction);
    
    instruction.type = BE_InstructionType_PushStruct;
    instruction.beginStruct.memberIndex = 123;
    instruction.beginStruct.arrayCount = BE_Local(321);
    TestAssert(TestSerializeSingleInstruction(instruction));
}

Test(Serializer_PopFrame)
{
    BEInstruction instruction;
    BE_ClearStructToZero(instruction);
    
    instruction.type = BE_InstructionType_PopFrame;
    TestAssert(TestSerializeSingleInstruction(instruction));
}

Test(Serializer_PopStruct)
{
    BEInstruction instruction;
    BE_ClearStructToZero(instruction);
    
    instruction.type = BE_InstructionType_PopStruct;
    TestAssert(TestSerializeSingleInstruction(instruction));
}

Test(Serializer_HideMembers)
{
    BEInstruction instruction;
    BE_ClearStructToZero(instruction);
    
    instruction.type = BE_InstructionType_HideMembers;
    TestAssert(TestSerializeSingleInstruction(instruction));
}

Test(Serializer_ShowMembers)
{
    BEInstruction instruction;
    BE_ClearStructToZero(instruction);
    
    instruction.type = BE_InstructionType_ShowMembers;
    TestAssert(TestSerializeSingleInstruction(instruction));
}

Test(Serializer_Breakpoint)
{
    BEInstruction instruction;
    BE_ClearStructToZero(instruction);
    
    instruction.type = BE_InstructionType_Breakpoint;
    TestAssert(TestSerializeSingleInstruction(instruction));
}

Test(Serializer_ExportScalar)
{
    BEInstruction instruction;
    BE_ClearStructToZero(instruction);
    
    instruction.type = BE_InstructionType_ExportScalar_u;
    instruction.scalarExport.memberIndex = 8;
    instruction.scalarExport.size = BE_Local(123);
    instruction.scalarExport.arrayCount = BE_Local(321);
    TestAssert(TestSerializeSingleInstruction(instruction));
}

Test(Serializer_FetchMemberValue)
{
    BEInstruction instruction;
    BE_ClearStructToZero(instruction);
    
    instruction.type = BE_InstructionType_FetchMemberValue;
    instruction.fetchMember.pathCount = 2;
    instruction.fetchMember.path[0].memberIndex = 7;
    instruction.fetchMember.path[1].memberIndex = 4;
    instruction.fetchMember.path[1].arrayIndex = BE_Local(6);
    instruction.result = BE_Local(123);
    TestAssert(TestSerializeSingleInstruction(instruction));
}

Test(Serializer_FetchArrayCount)
{
    BEInstruction instruction;
    BE_ClearStructToZero(instruction);
    
    instruction.type = BE_InstructionType_FetchArrayCount;
    instruction.fetchArrayCount.pathCount = 2;
    instruction.fetchArrayCount.path[0].memberIndex = 7;
    instruction.fetchArrayCount.path[1].memberIndex = 4;
    instruction.fetchArrayCount.path[1].arrayIndex = BE_Local(6);
    instruction.result = BE_Local(123);
    TestAssert(TestSerializeSingleInstruction(instruction));
}

Test(Serializer_Jump)
{
    BEInstruction instruction;
    BE_ClearStructToZero(instruction);
    
    instruction.type = BE_InstructionType_Jump;
    instruction.jump.newInstructionIndex = 0;  // NOTE(Jens): Specified here is index, the returned value after decoding is byte offset.
    TestAssert(TestSerializeSingleInstruction(instruction));
}

Test(Serializer_JumpDynamic)
{
    BEInstruction instruction;
    BE_ClearStructToZero(instruction);
    
    instruction.type = BE_InstructionType_JumpDynamic;
    instruction.jump.dynamicNewInstructionIndex = BE_Local(5);
    TestAssert(TestSerializeSingleInstruction(instruction));
}

Test(Serializer_JumpDynamicConditional)
{
    BEInstruction instruction;
    BE_ClearStructToZero(instruction);
    
    instruction.type = BE_InstructionType_JumpDynamicConditional;
    instruction.jump.dynamicNewInstructionIndex = BE_Local(5);
    instruction.jump.condition = BE_Local(3);
    TestAssert(TestSerializeSingleInstruction(instruction));
}

Test(Serializer_JumpConditional)
{
    BEInstruction instruction;
    BE_ClearStructToZero(instruction);
    
    instruction.type = BE_InstructionType_JumpConditional;
    instruction.jump.newInstructionIndex = 0;  // NOTE(Jens): Specified here is index, the returned value after decoding is byte offset.
    instruction.jump.condition = BE_Local(3);
    TestAssert(TestSerializeSingleInstruction(instruction));
}

Test(Serializer_FetchSizeOfFile)
{
    BEInstruction instruction;
    BE_ClearStructToZero(instruction);
    
    instruction.type = BE_InstructionType_FetchSizeOfFile;
    instruction.result = BE_Local(3);
    TestAssert(TestSerializeSingleInstruction(instruction));
}

Test(Serializer_FetchStartOfType)
{
    BEInstruction instruction;
    BE_ClearStructToZero(instruction);
    
    instruction.type = BE_InstructionType_FetchStartOfType;
    instruction.result = BE_Local(3);
    instruction.categoryQuery.categoryIndex = 8;
    instruction.categoryQuery.typeNumber = BE_Local(2);
    TestAssert(TestSerializeSingleInstruction(instruction));
}

Test(Serializer_HasType)
{
    BEInstruction instruction;
    BE_ClearStructToZero(instruction);
    
    instruction.type = BE_InstructionType_HasType;
    instruction.result = BE_Local(3);
    instruction.categoryQuery.categoryIndex = 8;
    instruction.categoryQuery.typeNumber = BE_Local(2);
    TestAssert(TestSerializeSingleInstruction(instruction));
}

Test(Serializer_Print)
{
    BEInstruction instruction;
    BE_ClearStructToZero(instruction);
    
    instruction.type = BE_InstructionType_Print;
    instruction.print.prefixCharacters = "prefix";
    instruction.print.prefixCharacterCount = sizeof("prefix") - 1;
    instruction.print.value = BE_Local(1);
    TestAssert(TestSerializeSingleInstruction(instruction));
}

Test(Serializer_Assert)
{
    BEInstruction instruction;
    BE_ClearStructToZero(instruction);
    
    instruction.type = BE_InstructionType_Assert;
    instruction.assert.condition = BE_Local(7);
    TestAssert(TestSerializeSingleInstruction(instruction));
}

Test(Serializer_Plot_X)
{
    BEInstruction instruction = { 0 };
    
    instruction.type =  BE_InstructionType_Plot_F8_X;
    instruction.plot.plotId = BE_Local(123);
    instruction.plot.value_f64 = BE_Local(321);
    TestAssert(TestSerializeSingleInstruction(instruction));
}

Test(Serializer_Plot_Y)
{
    BEInstruction instruction = { 0 };
    
    instruction.type =  BE_InstructionType_Plot_F8_Y;
    instruction.plot.plotId = BE_Local(123);
    instruction.plot.value_f64 = BE_Local(321);
    TestAssert(TestSerializeSingleInstruction(instruction));
}

Test(Serializer_CreatePlot)
{
    BEInstruction instruction;
    BE_ClearStructToZero(instruction);
    
    instruction.type = BE_InstructionType_CreatePlot;
    TestAssert(TestSerializeSingleInstruction(instruction));
}

Test(Serializer_IntToFloat)
{
    BEInstruction instruction;
    BE_ClearStructToZero(instruction);
    
    instruction.type = BE_InstructionType_IntToFloat;
    TestAssert(TestSerializeSingleInstruction(instruction));
}

static BEu8 g_Stack[1024];

static BEbool RunInstructions(BERuntime* outRuntime, BEu8* dataSection, BEu32 count, BEInstruction* instructions, BEu32 expectedCount, BEInstruction* expectedInstructions, BEu32 binaryFileSize, BEu8* binaryFile, BEu32 eventCount, BE_RuntimeEventType* events)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BE_InstructionEncoder encoder;
    BE_ClearStructToZero(encoder);
    
    for (BEu32 instructionIndex = 0; instructionIndex < count; ++instructionIndex)
    {
        BE_FeedInstructionEncoder(allocator, &encoder, instructions + instructionIndex);
    }
    
    BE_EncodedInstructions encoded = BE_FlushInstructionBuffer(&encoder);
    
    BEu32 byteCount = encoded.writtenBytes;
    BEu32 writtenBytes = 0;
    BEu8* data = BEAllocArray(allocator, BEu8, byteCount);
    
    for (BE_EncodedInstructionChunk* chunk = encoded.head; chunk; chunk = chunk->next)
    {
        BEu32 toCopy = byteCount - writtenBytes;
        if (toCopy > BE_ArrayCount(chunk->data))
        {
            toCopy = BE_ArrayCount(chunk->data);
        }
        
        memcpy(data + writtenBytes, chunk->data, toCopy);
        writtenBytes += toCopy;
    }
    
    BEAssert(writtenBytes == byteCount);
    
    BEbool rVal = 1;
    
    BERuntime runtime = BEStartRuntime(allocator, BE_ArrayCount(g_Stack), g_Stack, dataSection, byteCount, data, binaryFileSize, binaryFile);
    
    BEu32 eventIndex = 0;
    BEu32 instructionIndex = 0;
    while (!BERuntimeHasCompleted(&runtime))
    {
        BERuntimeEvent event = BEStepOne(&runtime);
        
        BEInstruction executedInstruction = event.instruction;
        BEInstruction* expectedInstruction = expectedInstructions + instructionIndex++;
        
        if (instructionIndex > expectedCount || !InstructionsAreEqual(&executedInstruction, expectedInstruction))
        {
            rVal = 0;
            break;
        }
        
        if (event.type)
        {
            BE_RuntimeEventType* expectedType = events + eventIndex++;
            
            if (eventIndex > eventCount || *expectedType != event.type)
            {
                rVal = 0;
                break;
            }
        }
    }
    
    if (instructionIndex != expectedCount || eventIndex != eventCount)
    {
        rVal = 0;
    }
    
    if (outRuntime)
    {
        *outRuntime = runtime;
    }
    
    return rVal;
}

Test(Runtime_Set)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[1] = { 0 };
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(1);
    instructions[0].set.value = -123;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*BEGetPtr(&runtime, instructions[0].result) == instructions[0].set.value);
}

Test(Runtime_Unary_Negate)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[2] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 123;
    
    instructions[1].type = BE_InstructionType_Negate;
    instructions[1].result = BE_Local(0);
    instructions[1].unaryValue = instructions[0].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*BEGetPtr(&runtime, instructions[0].result) == -instructions[0].set.value);
}

Test(Runtime_Unary_LogicalNot)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[2] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 123;
    
    instructions[1].type = BE_InstructionType_LogicalNot;
    instructions[1].result = BE_Local(0);
    instructions[1].unaryValue = instructions[0].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*BEGetPtr(&runtime, instructions[0].result) == !instructions[0].set.value);
}

Test(Runtime_Unary_BitwiseNot)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[2] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 123;
    
    instructions[1].type = BE_InstructionType_BitwiseNot;
    instructions[1].result = BE_Local(0);
    instructions[1].unaryValue = instructions[0].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*BEGetPtr(&runtime, instructions[0].result) == ~instructions[0].set.value);
}

Test(Runtime_Unary_Abs)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[2] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = -123;
    
    instructions[1].type = BE_InstructionType_Abs;
    instructions[1].result = BE_Local(0);
    instructions[1].unaryValue = instructions[0].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*BEGetPtr(&runtime, instructions[0].result) == -instructions[0].set.value);
}

Test(Runtime_Unary_Copy)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[2] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 123;
    
    instructions[1].type = BE_InstructionType_Copy;
    instructions[1].result = BE_Local(1);
    instructions[1].unaryValue = instructions[0].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*BEGetPtr(&runtime, instructions[0].result) == *BEGetPtr(&runtime, instructions[1].result));
}

Test(Runtime_Unary_FNegate)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[2] = { 0 };
    
    BEf64 value = 64.0;
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    memcpy(&instructions[0].set.value, &value, sizeof(value));
    
    instructions[1].type = BE_InstructionType_FNegate;
    instructions[1].result = BE_Local(1);
    instructions[1].unaryValue = instructions[0].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*(BEf64*)BEGetPtr(&runtime, instructions[1].result) == -value);
}

Test(Runtime_Unary_FAbs)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[2] = { 0 };
    
    BEf64 value = -64.0;
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    memcpy(&instructions[0].set.value, &value, sizeof(value));
    
    instructions[1].type = BE_InstructionType_FAbs;
    instructions[1].result = BE_Local(1);
    instructions[1].unaryValue = instructions[0].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*(BEf64*)BEGetPtr(&runtime, instructions[1].result) == -value);
}

Test(Runtime_Binary_Add)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 123;
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(1);
    instructions[1].set.value = 321;
    
    instructions[2].type = BE_InstructionType_Add;
    instructions[2].result = BE_Local(0);
    instructions[2].binary.left = instructions[0].result;
    instructions[2].binary.right = instructions[1].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*BEGetPtr(&runtime, instructions[2].result) == instructions[0].set.value + instructions[1].set.value);
}

Test(Runtime_Binary_Sub)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 123;
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(1);
    instructions[1].set.value = 321;
    
    instructions[2].type = BE_InstructionType_Sub;
    instructions[2].result = BE_Local(0);
    instructions[2].binary.left = instructions[0].result;
    instructions[2].binary.right = instructions[1].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*BEGetPtr(&runtime, instructions[2].result) == instructions[0].set.value - instructions[1].set.value);
}

Test(Runtime_Binary_Mul)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 123;
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(1);
    instructions[1].set.value = 321;
    
    instructions[2].type = BE_InstructionType_Mul;
    instructions[2].result = BE_Local(0);
    instructions[2].binary.left = instructions[0].result;
    instructions[2].binary.right = instructions[1].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*BEGetPtr(&runtime, instructions[2].result) == instructions[0].set.value * instructions[1].set.value);
}

Test(Runtime_Binary_Div)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 123;
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(1);
    instructions[1].set.value = 8;
    
    instructions[2].type = BE_InstructionType_Div;
    instructions[2].result = BE_Local(0);
    instructions[2].binary.left = instructions[0].result;
    instructions[2].binary.right = instructions[1].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*BEGetPtr(&runtime, instructions[2].result) == instructions[0].set.value / instructions[1].set.value);
}

Test(Runtime_Binary_Div_Zero)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 123;
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(1);
    instructions[1].set.value = 0;
    
    instructions[2].type = BE_InstructionType_Div;
    instructions[2].result = BE_Local(0);
    instructions[2].binary.left = instructions[0].result;
    instructions[2].binary.right = instructions[1].result;
    
    BE_RuntimeEventType events[] = {
        BE_RuntimeEventType_Error_DivByZero
    };
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, BE_ArrayCount(events), events));
}

Test(Runtime_Binary_Mod)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 123;
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(1);
    instructions[1].set.value = 8;
    
    instructions[2].type = BE_InstructionType_Mod;
    instructions[2].result = BE_Local(0);
    instructions[2].binary.left = instructions[0].result;
    instructions[2].binary.right = instructions[1].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*BEGetPtr(&runtime, instructions[2].result) == instructions[0].set.value % instructions[1].set.value);
}

Test(Runtime_Binary_Mod_Zero)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 123;
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(1);
    instructions[1].set.value = 0;
    
    instructions[2].type = BE_InstructionType_Mod;
    instructions[2].result = BE_Local(0);
    instructions[2].binary.left = instructions[0].result;
    instructions[2].binary.right = instructions[1].result;
    
    BE_RuntimeEventType events[] = {
        BE_RuntimeEventType_Error_ModByZero
    };
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, BE_ArrayCount(events), events));
}

Test(Runtime_Binary_Less)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 123;
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(1);
    instructions[1].set.value = 321;
    
    instructions[2].type = BE_InstructionType_Less;
    instructions[2].result = BE_Local(0);
    instructions[2].binary.left = instructions[0].result;
    instructions[2].binary.right = instructions[1].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*BEGetPtr(&runtime, instructions[2].result) == (instructions[0].set.value < instructions[1].set.value));
}

Test(Runtime_Binary_LessOrEqual)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 123;
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(1);
    instructions[1].set.value = 321;
    
    instructions[2].type = BE_InstructionType_LessOrEqual;
    instructions[2].result = BE_Local(0);
    instructions[2].binary.left = instructions[0].result;
    instructions[2].binary.right = instructions[1].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*BEGetPtr(&runtime, instructions[2].result) == (instructions[0].set.value <= instructions[1].set.value));
}

Test(Runtime_Binary_Equal)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 123;
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(1);
    instructions[1].set.value = 321;
    
    instructions[2].type = BE_InstructionType_Equal;
    instructions[2].result = BE_Local(0);
    instructions[2].binary.left = instructions[0].result;
    instructions[2].binary.right = instructions[1].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*BEGetPtr(&runtime, instructions[2].result) == (instructions[0].set.value == instructions[1].set.value));
}

Test(Runtime_Binary_Greater)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 123;
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(1);
    instructions[1].set.value = 321;
    
    instructions[2].type = BE_InstructionType_Greater;
    instructions[2].result = BE_Local(0);
    instructions[2].binary.left = instructions[0].result;
    instructions[2].binary.right = instructions[1].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*BEGetPtr(&runtime, instructions[2].result) == (instructions[0].set.value > instructions[1].set.value));
}

Test(Runtime_Binary_GreaterOrEqual)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 123;
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(1);
    instructions[1].set.value = 321;
    
    instructions[2].type = BE_InstructionType_GreaterOrEqual;
    instructions[2].result = BE_Local(0);
    instructions[2].binary.left = instructions[0].result;
    instructions[2].binary.right = instructions[1].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*BEGetPtr(&runtime, instructions[2].result) == (instructions[0].set.value >= instructions[1].set.value));
}

Test(Runtime_Binary_NotEqual)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 123;
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(1);
    instructions[1].set.value = 321;
    
    instructions[2].type = BE_InstructionType_NotEqual;
    instructions[2].result = BE_Local(0);
    instructions[2].binary.left = instructions[0].result;
    instructions[2].binary.right = instructions[1].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*BEGetPtr(&runtime, instructions[2].result) == (instructions[0].set.value != instructions[1].set.value));
}

Test(Runtime_Binary_BitshiftLeft)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 123;
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(1);
    instructions[1].set.value = 3;
    
    instructions[2].type = BE_InstructionType_BitshiftLeft;
    instructions[2].result = BE_Local(0);
    instructions[2].binary.left = instructions[0].result;
    instructions[2].binary.right = instructions[1].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*BEGetPtr(&runtime, instructions[2].result) == (instructions[0].set.value << instructions[1].set.value));
}

Test(Runtime_Binary_BitshiftRight)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 123;
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(1);
    instructions[1].set.value = 3;
    
    instructions[2].type = BE_InstructionType_BitshiftRight;
    instructions[2].result = BE_Local(0);
    instructions[2].binary.left = instructions[0].result;
    instructions[2].binary.right = instructions[1].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*BEGetPtr(&runtime, instructions[2].result) == (instructions[0].set.value >> instructions[1].set.value));
}

Test(Runtime_Binary_BitwiseAnd)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 123;
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(1);
    instructions[1].set.value = 321;
    
    instructions[2].type = BE_InstructionType_BitwiseAnd;
    instructions[2].result = BE_Local(0);
    instructions[2].binary.left = instructions[0].result;
    instructions[2].binary.right = instructions[1].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*BEGetPtr(&runtime, instructions[2].result) == (instructions[0].set.value & instructions[1].set.value));
}

Test(Runtime_Binary_BitwiseOr)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 123;
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(1);
    instructions[1].set.value = 321;
    
    instructions[2].type = BE_InstructionType_BitwiseOr;
    instructions[2].result = BE_Local(0);
    instructions[2].binary.left = instructions[0].result;
    instructions[2].binary.right = instructions[1].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*BEGetPtr(&runtime, instructions[2].result) == (instructions[0].set.value | instructions[1].set.value));
}

Test(Runtime_Binary_BitwiseXOr)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 123;
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(1);
    instructions[1].set.value = 321;
    
    instructions[2].type = BE_InstructionType_BitwiseXOr;
    instructions[2].result = BE_Local(0);
    instructions[2].binary.left = instructions[0].result;
    instructions[2].binary.right = instructions[1].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*BEGetPtr(&runtime, instructions[2].result) == (instructions[0].set.value ^ instructions[1].set.value));
}

Test(Runtime_Binary_LogicalAnd)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 123;
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(1);
    instructions[1].set.value = 321;
    
    instructions[2].type = BE_InstructionType_LogicalAnd;
    instructions[2].result = BE_Local(0);
    instructions[2].binary.left = instructions[0].result;
    instructions[2].binary.right = instructions[1].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*BEGetPtr(&runtime, instructions[2].result) == (instructions[0].set.value && instructions[1].set.value));
}

Test(Runtime_Binary_LogicalOr)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 123;
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(1);
    instructions[1].set.value = 321;
    
    instructions[2].type = BE_InstructionType_LogicalOr;
    instructions[2].result = BE_Local(0);
    instructions[2].binary.left = instructions[0].result;
    instructions[2].binary.right = instructions[1].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*BEGetPtr(&runtime, instructions[2].result) == (instructions[0].set.value || instructions[1].set.value));
}

Test(Runtime_Binary_FAdd)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEf64 valueA = 123.0;
    BEf64 valueB = 321.0;
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    memcpy(&instructions[0].set.value, &valueA, sizeof(valueA));
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(1);
    memcpy(&instructions[1].set.value, &valueB, sizeof(valueB));
    
    instructions[2].type = BE_InstructionType_FAdd;
    instructions[2].result = BE_Local(0);
    instructions[2].binary.left = instructions[0].result;
    instructions[2].binary.right = instructions[1].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*(BEf64*)BEGetPtr(&runtime, instructions[2].result) == valueA + valueB);
}

Test(Runtime_Binary_FSub)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEf64 valueA = 123.0;
    BEf64 valueB = 321.0;
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    memcpy(&instructions[0].set.value, &valueA, sizeof(valueA));
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(1);
    memcpy(&instructions[1].set.value, &valueB, sizeof(valueB));
    
    instructions[2].type = BE_InstructionType_FSub;
    instructions[2].result = BE_Local(0);
    instructions[2].binary.left = instructions[0].result;
    instructions[2].binary.right = instructions[1].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*(BEf64*)BEGetPtr(&runtime, instructions[2].result) == valueA - valueB);
}

Test(Runtime_Binary_FMul)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEf64 valueA = 123.0;
    BEf64 valueB = 321.0;
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    memcpy(&instructions[0].set.value, &valueA, sizeof(valueA));
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(1);
    memcpy(&instructions[1].set.value, &valueB, sizeof(valueB));
    
    instructions[2].type = BE_InstructionType_FMul;
    instructions[2].result = BE_Local(0);
    instructions[2].binary.left = instructions[0].result;
    instructions[2].binary.right = instructions[1].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*(BEf64*)BEGetPtr(&runtime, instructions[2].result) == valueA * valueB);
}

Test(Runtime_Binary_FDiv)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEf64 valueA = 123.0;
    BEf64 valueB = 8.0;
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    memcpy(&instructions[0].set.value, &valueA, sizeof(valueA));
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(1);
    memcpy(&instructions[1].set.value, &valueB, sizeof(valueB));
    
    instructions[2].type = BE_InstructionType_FDiv;
    instructions[2].result = BE_Local(0);
    instructions[2].binary.left = instructions[0].result;
    instructions[2].binary.right = instructions[1].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*(BEf64*)BEGetPtr(&runtime, instructions[2].result) == valueA / valueB);
}

Test(Runtime_Binary_FDiv_Zero)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEf64 valueA = 123.0;
    BEf64 valueB = 0.0;
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    memcpy(&instructions[0].set.value, &valueA, sizeof(valueA));
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(1);
    memcpy(&instructions[1].set.value, &valueB, sizeof(valueB));
    
    instructions[2].type = BE_InstructionType_FDiv;
    instructions[2].result = BE_Local(0);
    instructions[2].binary.left = instructions[0].result;
    instructions[2].binary.right = instructions[1].result;
    
    BE_RuntimeEventType events[] = {
        BE_RuntimeEventType_Error_DivByZero
    };
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, BE_ArrayCount(events), events));
}

Test(Runtime_Binary_FLess)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEf64 valueA = 123.0;
    BEf64 valueB = 123.0;
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    memcpy(&instructions[0].set.value, &valueA, sizeof(valueA));
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(1);
    memcpy(&instructions[1].set.value, &valueB, sizeof(valueB));
    
    instructions[2].type = BE_InstructionType_FLess;
    instructions[2].result = BE_Local(0);
    instructions[2].binary.left = instructions[0].result;
    instructions[2].binary.right = instructions[1].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*(BEf64*)BEGetPtr(&runtime, instructions[2].result) == (valueA < valueB));
}

Test(Runtime_Binary_FLessOrEqual)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEf64 valueA = 123.0;
    BEf64 valueB = 123.0;
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    memcpy(&instructions[0].set.value, &valueA, sizeof(valueA));
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(1);
    memcpy(&instructions[1].set.value, &valueB, sizeof(valueB));
    
    instructions[2].type = BE_InstructionType_FLessOrEqual;
    instructions[2].result = BE_Local(0);
    instructions[2].binary.left = instructions[0].result;
    instructions[2].binary.right = instructions[1].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*(BEf64*)BEGetPtr(&runtime, instructions[2].result) == (valueA <= valueB));
}

Test(Runtime_Binary_FEqual)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEf64 valueA = 123.0;
    BEf64 valueB = 123.0;
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    memcpy(&instructions[0].set.value, &valueA, sizeof(valueA));
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(1);
    memcpy(&instructions[1].set.value, &valueB, sizeof(valueB));
    
    instructions[2].type = BE_InstructionType_FEqual;
    instructions[2].result = BE_Local(0);
    instructions[2].binary.left = instructions[0].result;
    instructions[2].binary.right = instructions[1].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*(BEf64*)BEGetPtr(&runtime, instructions[2].result) == (valueA == valueB));
}

Test(Runtime_Binary_FGreater)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEf64 valueA = 123.0;
    BEf64 valueB = 123.0;
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    memcpy(&instructions[0].set.value, &valueA, sizeof(valueA));
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(1);
    memcpy(&instructions[1].set.value, &valueB, sizeof(valueB));
    
    instructions[2].type = BE_InstructionType_FGreater;
    instructions[2].result = BE_Local(0);
    instructions[2].binary.left = instructions[0].result;
    instructions[2].binary.right = instructions[1].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*(BEf64*)BEGetPtr(&runtime, instructions[2].result) == (valueA > valueB));
}

Test(Runtime_Binary_FGreaterOrEqual)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEf64 valueA = 123.0;
    BEf64 valueB = 123.0;
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    memcpy(&instructions[0].set.value, &valueA, sizeof(valueA));
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(1);
    memcpy(&instructions[1].set.value, &valueB, sizeof(valueB));
    
    instructions[2].type = BE_InstructionType_FGreaterOrEqual;
    instructions[2].result = BE_Local(0);
    instructions[2].binary.left = instructions[0].result;
    instructions[2].binary.right = instructions[1].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*(BEf64*)BEGetPtr(&runtime, instructions[2].result) == (valueA >= valueB));
}

Test(Runtime_Binary_FNotEqual)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEf64 valueA = 123.0;
    BEf64 valueB = 123.0;
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    memcpy(&instructions[0].set.value, &valueA, sizeof(valueA));
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(1);
    memcpy(&instructions[1].set.value, &valueB, sizeof(valueB));
    
    instructions[2].type = BE_InstructionType_FNotEqual;
    instructions[2].result = BE_Local(0);
    instructions[2].binary.left = instructions[0].result;
    instructions[2].binary.right = instructions[1].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*(BEf64*)BEGetPtr(&runtime, instructions[2].result) == (valueA != valueB));
}

Test(Runtime_FramePushPop)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[4] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 123;
    
    instructions[1].type = BE_InstructionType_PushFrame;
    instructions[1].pushFrame.sizeRequiredInBytes = 8 * 16;
    
    instructions[2].type = BE_InstructionType_Set;
    instructions[2].result = BE_Local(0);
    instructions[2].set.value = 321;
    
    instructions[3].type = BE_InstructionType_PopFrame;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*BEGetPtr(&runtime, instructions[0].result) == instructions[0].set.value);
}

static BE_MemberDeclaration* CreateScalarMember(BE_ScalarBaseType baseType)
{
    static BE_MemberDeclaration rVal = { 0 };
    rVal.type.scalarVariant.baseType = baseType;
    rVal.name = BE_StrLiteral("test_member");
    return &rVal;
}

Test(Runtime_ExportScalar_u)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_StructIndex;
    instructions[0].structIndex.index = 0;
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(0);
    instructions[1].set.value = 4;
    
    instructions[2].type = BE_InstructionType_ExportScalar_u;
    instructions[2].scalarExport.memberIndex = 0;
    instructions[2].scalarExport.isBigEndian = 0;
    instructions[2].scalarExport.size = instructions[1].result;
    
    BE_ParsedEnums enums = { 0 };
    BE_ParsedStructs structs = { 0 };
    BE_AddStructMember(allocator, AddStruct(allocator, &structs, BE_StrLiteral("Foo"), 0, 0), CreateScalarMember(BE_ScalarBaseType_u));
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BE_RuntimeEventType events[] = {
        BE_RuntimeEventType_StartStruct,
        BE_RuntimeEventType_ScalarExport
    };
    
    BERuntime runtime;
    
    BEs64 data = 0;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, sizeof(data), (BEu8*)&data, BE_ArrayCount(events), events));
}

Test(Runtime_ExportScalar_s)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_StructIndex;
    instructions[0].structIndex.index = 0;
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(0);
    instructions[1].set.value = 4;
    
    instructions[2].type = BE_InstructionType_ExportScalar_s;
    instructions[2].scalarExport.memberIndex = 0;
    instructions[2].scalarExport.isBigEndian = 0;
    instructions[2].scalarExport.size = instructions[1].result;
    
    BE_ParsedEnums enums = { 0 };
    BE_ParsedStructs structs = { 0 };
    BE_AddStructMember(allocator, AddStruct(allocator, &structs, BE_StrLiteral("Foo"), 0, 0), CreateScalarMember(BE_ScalarBaseType_s));
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BE_RuntimeEventType events[] = {
        BE_RuntimeEventType_StartStruct,
        BE_RuntimeEventType_ScalarExport
    };
    
    BERuntime runtime;
    BEs64 data = 0;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, sizeof(data), (BEu8*)&data, BE_ArrayCount(events), events));
}

Test(Runtime_ExportScalar_f)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_StructIndex;
    instructions[0].structIndex.index = 0;
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(0);
    instructions[1].set.value = 4;
    
    instructions[2].type = BE_InstructionType_ExportScalar_f;
    instructions[2].scalarExport.memberIndex = 0;
    instructions[2].scalarExport.isBigEndian = 0;
    instructions[2].scalarExport.size = instructions[1].result;
    
    BE_ParsedEnums enums = { 0 };
    BE_ParsedStructs structs = { 0 };
    BE_AddStructMember(allocator, AddStruct(allocator, &structs, BE_StrLiteral("Foo"), 0, 0), CreateScalarMember(BE_ScalarBaseType_f));
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BE_RuntimeEventType events[] = {
        BE_RuntimeEventType_StartStruct,
        BE_RuntimeEventType_ScalarExport
    };
    
    BERuntime runtime;
    BEs64 data = 0;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, sizeof(data), (BEu8*)&data, BE_ArrayCount(events), events));
}

Test(Runtime_ExportScalar_string)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[3]  = { 0 };
    
    instructions[0].type = BE_InstructionType_StructIndex;
    instructions[0].structIndex.index = 0;
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(0);
    instructions[1].set.value = 4;
    
    instructions[2].type = BE_InstructionType_ExportScalar_string;
    instructions[2].scalarExport.memberIndex = 0;
    instructions[2].scalarExport.size = instructions[1].result;
    
    BE_ParsedEnums enums = { 0 };
    BE_ParsedStructs structs = { 0 };
    BE_AddStructMember(allocator, AddStruct(allocator, &structs, BE_StrLiteral("Foo"), 0, 0), CreateScalarMember(BE_ScalarBaseType_string));
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BE_RuntimeEventType events[] = {
        BE_RuntimeEventType_StartStruct,
        BE_RuntimeEventType_ScalarExport
    };
    
    BERuntime runtime;
    BEs64 data = 0;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, sizeof(data), (BEu8*)&data, BE_ArrayCount(events), events));
}

Test(Runtime_ExportScalar_raw)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_StructIndex;
    instructions[0].structIndex.index = 0;
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(0);
    instructions[1].set.value = 4;
    
    instructions[2].type = BE_InstructionType_ExportScalar_raw;
    instructions[2].scalarExport.size = instructions[1].result;
    
    BE_ParsedEnums enums = { 0 };
    BE_ParsedStructs structs = { 0 };
    BE_AddStructMember(allocator, AddStruct(allocator, &structs, BE_StrLiteral("Foo"), 0, 0), CreateScalarMember(BE_ScalarBaseType_raw));
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BE_RuntimeEventType events[] = {
        BE_RuntimeEventType_StartStruct,
        BE_RuntimeEventType_ScalarExport
    };
    
    BERuntime runtime;
    BEs64 data = 0;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, sizeof(data), (BEu8*)&data, BE_ArrayCount(events), events));
}

Test(Runtime_ExportScalar_BadAddress)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_StructIndex;
    instructions[0].structIndex.index = 0;
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(0);
    instructions[1].set.value = 4;
    
    instructions[2].type = BE_InstructionType_ExportScalar_raw;
    instructions[2].scalarExport.size = instructions[1].result;
    
    BE_ParsedEnums enums = { 0 };
    BE_ParsedStructs structs = { 0 };
    AddStruct(allocator, &structs, BE_StrLiteral("Foo"), 0, 0);
    
    BE_TreeIterator* treeFreelist = 0;
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, 0, 0, 0).data;
    
    BE_RuntimeEventType events[] = {
        BE_RuntimeEventType_StartStruct,
        BE_RuntimeEventType_Error_MemberInvalidMemoryRange
    };
    
    BERuntime runtime;
    BEu8 data[2];
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, sizeof(data), data, BE_ArrayCount(events), events));
}

Test(Runtime_FetchMemberValue)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[4] = { 0 };
    
    instructions[0].type = BE_InstructionType_StructIndex;
    instructions[0].structIndex.index = 0;
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(0);
    instructions[1].set.value = 4;
    
    instructions[2].type = BE_InstructionType_ExportScalar_u;
    instructions[2].scalarExport.memberIndex = 0;
    instructions[2].scalarExport.isBigEndian = 0;
    instructions[2].scalarExport.size = instructions[1].result;
    
    instructions[3].type = BE_InstructionType_FetchMemberValue;
    instructions[3].result = BE_Local(0);
    instructions[3].fetchMember.pathCount = 1;
    instructions[3].fetchMember.path[0].memberIndex = 0;
    
    BE_ParsedEnums enums = { 0 };
    BE_ParsedStructs structs = { 0 };
    BE_AddStructMember(allocator, AddStruct(allocator, &structs, BE_StrLiteral("Foo"), 0, 0), CreateScalarMember(BE_ScalarBaseType_u));
    
    BE_TreeIterator* treeFreelist = 0;
    BEu32 structStartAddresses[1] = { 0 };
    
    {
        BE_NameMap parameters;
        BE_ClearStructToZero(parameters);
        
        BE_AddStruct(allocator, &structs, BE_StrLiteral("TestStruct"), parameters);
    }
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, structStartAddresses, 0, 0).data;
    
    BE_RuntimeEventType events[] = {
        BE_RuntimeEventType_StartStruct,
        BE_RuntimeEventType_ScalarExport
    };
    
    BERuntime runtime;
    BEu32 data = 123;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, sizeof(data), (BEu8*)&data, BE_ArrayCount(events), events));
    
    TestAssert(*BEGetPtr(&runtime, instructions[3].result) == data);
}

Test(Runtime_FetchMemberValue_ScalarArray)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[6];
    
    instructions[0].type = BE_InstructionType_StructIndex;
    instructions[0].structIndex.index = 0;
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(0);
    instructions[1].set.value = 4;
    
    instructions[2].type = BE_InstructionType_Set;
    instructions[2].result = BE_Local(1);
    instructions[2].set.value = 5;
    
    instructions[3].type = BE_InstructionType_ExportScalar_u;
    instructions[3].scalarExport.memberIndex = 0;
    instructions[3].scalarExport.isBigEndian = 0;
    instructions[3].scalarExport.size = instructions[1].result;
    instructions[3].scalarExport.arrayCount = instructions[2].result;
    
    instructions[4].type = BE_InstructionType_Set;
    instructions[4].result = BE_Local(2);
    instructions[4].set.value = 2;
    
    instructions[5].type = BE_InstructionType_FetchMemberValue;
    instructions[5].result = BE_Local(0);
    instructions[5].fetchMember.pathCount = 1;
    instructions[5].fetchMember.path[0].memberIndex = 0;
    instructions[5].fetchMember.path[0].arrayIndex = instructions[4].result;
    
    BE_ParsedEnums enums = { 0 };
    BE_ParsedStructs structs = { 0 };
    BE_AddStructMember(allocator, AddStruct(allocator, &structs, BE_StrLiteral("Foo"), 0, 0), CreateScalarMember(BE_ScalarBaseType_u));
    
    BE_TreeIterator* treeFreelist = 0;
    BEu32 structStartAddresses[1] = { 0 };
    
    {
        BE_NameMap parameters;
        BE_ClearStructToZero(parameters);
        
        BE_AddStruct(allocator, &structs, BE_StrLiteral("TestStruct"), parameters);
    }
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, structStartAddresses, 0, 0).data;
    
    BE_RuntimeEventType events[] = {
        BE_RuntimeEventType_StartStruct,
        BE_RuntimeEventType_ScalarExport
    };
    
    
    BEu32 data[5] = {
        0x01020304,
        0x05060708,
        0x090a0b0c,
        0x0d0e0f10,
        0x11121314
    };
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, sizeof(data), (BEu8*)data, BE_ArrayCount(events), events));
    
    TestAssert(*BEGetPtr(&runtime, instructions[5].result) == data[2]);
}

Test(Runtime_FetchMemberValue_ScalarArray_BadIndex)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[6];
    
    instructions[0].type = BE_InstructionType_StructIndex;
    instructions[0].structIndex.index = 0;
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(0);
    instructions[1].set.value = 4;
    
    instructions[2].type = BE_InstructionType_Set;
    instructions[2].result = BE_Local(1);
    instructions[2].set.value = 5;
    
    instructions[3].type = BE_InstructionType_ExportScalar_u;
    instructions[3].scalarExport.memberIndex = 0;
    instructions[3].scalarExport.size = instructions[1].result;
    instructions[3].scalarExport.arrayCount = instructions[2].result;
    
    instructions[4].type = BE_InstructionType_Set;
    instructions[4].result = BE_Local(2);
    instructions[4].set.value = -2;
    
    instructions[5].type = BE_InstructionType_FetchMemberValue;
    instructions[5].result = BE_Local(0);
    instructions[5].fetchMember.pathCount = 1;
    instructions[5].fetchMember.path[0].memberIndex = 0;
    instructions[5].fetchMember.path[0].arrayIndex = instructions[4].result;
    
    BE_ParsedEnums enums = { 0 };
    BE_ParsedStructs structs = { 0 };
    BE_AddStructMember(allocator, AddStruct(allocator, &structs, BE_StrLiteral("Foo"), 0, 0), CreateScalarMember(BE_ScalarBaseType_u));
    
    BE_TreeIterator* treeFreelist = 0;
    BEu32 structStartAddresses[1] = { 0 };
    
    {
        BE_NameMap parameters;
        BE_ClearStructToZero(parameters);
        
        BE_AddStruct(allocator, &structs, BE_StrLiteral("TestStruct"), parameters);
    }
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, structStartAddresses, 0, 0).data;
    
    BE_RuntimeEventType events[] = {
        BE_RuntimeEventType_StartStruct,
        BE_RuntimeEventType_ScalarExport,
        BE_RuntimeEventType_Error_FetchMemberBadArrayIndex
    };
    
    BEu32 data[5] = {
        0x01020304,
        0x05060708,
        0x090a0b0c,
        0x0d0e0f10,
        0x11121314
    };
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, sizeof(data), (BEu8*)data, BE_ArrayCount(events), events));
}

Test(Runtime_FetchMemberValue_TooBig)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[4];
    
    instructions[0].type = BE_InstructionType_StructIndex;
    instructions[0].structIndex.index = 0;
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(0);
    instructions[1].set.value = 128;
    
    instructions[2].type = BE_InstructionType_ExportScalar_string;
    instructions[2].scalarExport.memberIndex = 0;
    instructions[2].scalarExport.size = instructions[1].result;
    BE_ClearStructToZero(instructions[2].scalarExport.arrayCount);
    
    instructions[3].type = BE_InstructionType_FetchMemberValue;
    instructions[3].result = BE_Local(0);
    instructions[3].fetchMember.pathCount = 1;
    instructions[3].fetchMember.path[0].memberIndex = 0;
    BE_ClearStructToZero(instructions[3].fetchMember.path[0].arrayIndex);
    
    BE_RuntimeEventType events[] = {
        BE_RuntimeEventType_StartStruct,
        BE_RuntimeEventType_ScalarExport,
        BE_RuntimeEventType_Error_FetchMemberTooBig
    };
    
    BE_ParsedEnums enums = { 0 };
    BE_ParsedStructs structs = { 0 };
    BE_AddStructMember(allocator, AddStruct(allocator, &structs, BE_StrLiteral("Foo"), 0, 0), CreateScalarMember(BE_ScalarBaseType_string));
    
    BE_TreeIterator* treeFreelist = 0;
    BEu32 structStartAddresses[1] = { 0 };
    
    {
        BE_NameMap parameters;
        BE_ClearStructToZero(parameters);
        
        BE_AddStruct(allocator, &structs, BE_StrLiteral("TestStruct"), parameters);
    }
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, structStartAddresses, 0, 0).data;
    
    BEu8 data[128] = { 0 };
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, sizeof(data), data, BE_ArrayCount(events), events));
}

Test(Runtime_FetchMemberValue_ZeroSize)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[4];
    
    instructions[0].type = BE_InstructionType_StructIndex;
    instructions[0].structIndex.index = 0;
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(0);
    instructions[1].set.value = 0;
    
    instructions[2].type = BE_InstructionType_ExportScalar_string;
    instructions[2].scalarExport.memberIndex = 0;
    instructions[2].scalarExport.size = instructions[1].result;
    BE_ClearStructToZero(instructions[2].scalarExport.arrayCount);
    
    instructions[3].type = BE_InstructionType_FetchMemberValue;
    instructions[3].result = BE_Local(0);
    instructions[3].fetchMember.pathCount = 1;
    instructions[3].fetchMember.path[0].memberIndex = 0;
    BE_ClearStructToZero(instructions[3].fetchMember.path[0].arrayIndex);
    
    BE_RuntimeEventType events[] = {
        BE_RuntimeEventType_StartStruct,
        BE_RuntimeEventType_Error_FetchMemberSizeIsZero
    };
    
    BE_ParsedEnums enums = { 0 };
    BE_ParsedStructs structs = { 0 };
    BE_AddStructMember(allocator, AddStruct(allocator, &structs, BE_StrLiteral("Foo"), 0, 0), CreateScalarMember(BE_ScalarBaseType_string));
    
    BE_TreeIterator* treeFreelist = 0;
    BEu32 structStartAddresses[1] = { 0 };
    
    {
        BE_NameMap parameters;
        BE_ClearStructToZero(parameters);
        
        BE_AddStruct(allocator, &structs, BE_StrLiteral("TestStruct"), parameters);
    }
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, structStartAddresses, 0, 0).data;
    
    BEu8 data[128] = { 0 };
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, sizeof(data), data, BE_ArrayCount(events), events));
}

Test(Runtime_FetchArrayCount)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[5];
    
    instructions[0].type = BE_InstructionType_StructIndex;
    instructions[0].structIndex.index = 0;
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(0);
    instructions[1].set.value = 4;
    
    instructions[2].type = BE_InstructionType_Set;
    instructions[2].result = BE_Local(1);
    instructions[2].set.value = 8;
    
    instructions[3].type = BE_InstructionType_ExportScalar_u;
    instructions[3].scalarExport.memberIndex = 0;
    instructions[3].scalarExport.size = instructions[1].result;
    instructions[3].scalarExport.arrayCount = instructions[2].result;
    
    instructions[4].type = BE_InstructionType_FetchArrayCount;
    instructions[4].result = BE_Local(0);
    instructions[4].fetchMember.pathCount = 1;
    instructions[4].fetchMember.path[0].memberIndex = 0;
    BE_ClearStructToZero(instructions[4].fetchMember.path[0].arrayIndex);
    
    BE_RuntimeEventType events[] = {
        BE_RuntimeEventType_StartStruct,
        BE_RuntimeEventType_ScalarExport,
    };
    
    BE_ParsedEnums enums = { 0 };
    BE_ParsedStructs structs = { 0 };
    BE_AddStructMember(allocator, AddStruct(allocator, &structs, BE_StrLiteral("Foo"), 0, 0), CreateScalarMember(BE_ScalarBaseType_u));
    
    BE_TreeIterator* treeFreelist = 0;
    BEu32 structStartAddresses[1] = { 0 };
    
    {
        BE_NameMap parameters;
        BE_ClearStructToZero(parameters);
        
        BE_AddStruct(allocator, &structs, BE_StrLiteral("TestStruct"), parameters);
    }
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, structStartAddresses, 0, 0).data;
    
    BEu8 data[128] = { 0 };
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, sizeof(data), data, BE_ArrayCount(events), events));
}

Test(Runtime_FetchMemory)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[2];
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 4;
    
    instructions[1].type = BE_InstructionType_FetchMemory;
    instructions[1].result = BE_Local(0);
    instructions[1].fetchMemory.size = BE_Local(0);
    instructions[1].fetchMemory.flags = 0;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    BEu32 structStartAddresses[1] = { 0 };
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, structStartAddresses, 0, 0).data;
    
    BEu32 binaryContent = 123;
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, sizeof(binaryContent), (BEu8*)&binaryContent, 0, 0));
    TestAssert(*BEGetPtr(&runtime, instructions[1].result) == binaryContent);
}

Test(Runtime_FetchMemory_Signed)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[2];
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 4;
    
    instructions[1].type = BE_InstructionType_FetchMemory;
    instructions[1].result = BE_Local(0);
    instructions[1].fetchMemory.size = BE_Local(0);
    instructions[1].fetchMemory.flags = BEFetchMemoryFlag_Signed;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    BEu32 structStartAddresses[1] = { 0 };
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, structStartAddresses, 0, 0).data;
    
    BEs32 binaryContent = -123;
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, sizeof(binaryContent), (BEu8*)&binaryContent, 0, 0));
    TestAssert(*BEGetPtr(&runtime, instructions[1].result) == binaryContent);
}

Test(Runtime_FetchMemory_Big)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[2];
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 4;
    
    instructions[1].type = BE_InstructionType_FetchMemory;
    instructions[1].result = BE_Local(0);
    instructions[1].fetchMemory.size = BE_Local(0);
    instructions[1].fetchMemory.flags = BEFetchMemoryFlag_BigEndian;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    BEu32 structStartAddresses[1] = { 0 };
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, structStartAddresses, 0, 0).data;
    
    BEu32 binaryContent = 0xaabbccdd;
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, sizeof(binaryContent), (BEu8*)&binaryContent, 0, 0));
    TestAssert(*BEGetPtr(&runtime, instructions[1].result) == 0xddccbbaa);
}

Test(Runtime_FetchMemory_BadAddress)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[3];
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 4;
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_MemberOffset();
    instructions[1].set.value = -1;
    
    instructions[2].type = BE_InstructionType_FetchMemory;
    instructions[2].result = BE_Local(0);
    instructions[2].fetchMemory.size = BE_Local(0);
    instructions[2].fetchMemory.flags = 0;
    
    BE_RuntimeEventType events[] = {
        BE_RuntimeEventType_Error_FetchMemoryInvalidMemoryRange
    };
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    BEu32 structStartAddresses[1] = { 0 };
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, structStartAddresses, 0, 0).data;
    
    BEu32 binaryContent = 123;
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, sizeof(binaryContent), (BEu8*)&binaryContent, BE_ArrayCount(events), events));
}

Test(Runtime_FetchMemory_TooBig)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[2];
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 32;
    
    instructions[1].type = BE_InstructionType_FetchMemory;
    instructions[1].result = BE_Local(0);
    instructions[1].fetchMemory.size = BE_Local(0);
    instructions[1].fetchMemory.flags = BEFetchMemoryFlag_BigEndian;
    
    BE_RuntimeEventType events[] = {
        BE_RuntimeEventType_Error_FetchMemoryTooBig
    };
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    BEu32 structStartAddresses[1] = { 0 };
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, structStartAddresses, 0, 0).data;
    
    BEu32 binaryContent = 0xaabbccdd;
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, sizeof(binaryContent), (BEu8*)&binaryContent, BE_ArrayCount(events), events));
}

Test(Runtime_Assert_Success)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[2];
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 1;
    
    instructions[1].type = BE_InstructionType_Assert;
    instructions[1].assert.condition = BE_Local(0);
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    BEu32 structStartAddresses[1] = { 0 };
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, structStartAddresses, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
}

Test(Runtime_Assert_Fail)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[2];
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 0;
    
    instructions[1].type = BE_InstructionType_Assert;
    instructions[1].assert.condition = BE_Local(0);
    
    BE_RuntimeEventType events[] = {
        BE_RuntimeEventType_Error_AssertTriggered
    };
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    BEu32 structStartAddresses[1] = { 0 };
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, structStartAddresses, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, BE_ArrayCount(events), events));
}

Test(Runtime_HasType_Existing)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[2];
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 20;
    
    instructions[1].type = BE_InstructionType_HasType;
    instructions[1].result = BE_Local(0);
    instructions[1].categoryQuery.categoryIndex = 0;
    instructions[1].categoryQuery.typeNumber = instructions[0].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    BEu32 structStartAddresses[6] = { 0 };
    
    {
        // NOTE(Jens): Three categories, two with one struct and first with four.
        //
        // struct (Cat_1,  0) ...
        // struct (Cat_1, 10) ...
        // struct (Cat_1, 20) ...
        // struct (Cat_1, 30) ...
        // struct (Cat_2,  5) ...
        // struct (Cat_3,  4) ...
        //
        
        BE_ParsedCategoryNode* category_Cat_1 = BE_AcquireCategory(allocator, &structs, BE_StrLiteral("Cat_1"));
        BE_ParsedCategoryNode* category_Cat_2 = BE_AcquireCategory(allocator, &structs, BE_StrLiteral("Cat_2"));
        BE_ParsedCategoryNode* category_Cat_3 = BE_AcquireCategory(allocator, &structs, BE_StrLiteral("Cat_3"));
        
        BE_ParsedStructNode* struct_Cat_1_0 = AddStruct(allocator, &structs, BE_StrLiteral("struct_Cat_1_0"), 0, 0);
        BE_ParsedStructNode* struct_Cat_1_10 = AddStruct(allocator, &structs, BE_StrLiteral("struct_Cat_1_10"), 0, 0);
        BE_ParsedStructNode* struct_Cat_1_20 = AddStruct(allocator, &structs, BE_StrLiteral("struct_Cat_1_20"), 0, 0);
        BE_ParsedStructNode* struct_Cat_1_30 = AddStruct(allocator, &structs, BE_StrLiteral("struct_Cat_1_30"), 0, 0);
        BE_ParsedStructNode* struct_Cat_2_5 = AddStruct(allocator, &structs, BE_StrLiteral("struct_Cat_2_5"), 0, 0);
        BE_ParsedStructNode* struct_Cat_3_4 = AddStruct(allocator, &structs, BE_StrLiteral("struct_Cat_3_4"), 0, 0);
        
        BE_AddTypeToCategory(struct_Cat_1_0, category_Cat_1, 0);
        BE_AddTypeToCategory(struct_Cat_1_10, category_Cat_1, 10);
        BE_AddTypeToCategory(struct_Cat_1_20, category_Cat_1, 20);
        BE_AddTypeToCategory(struct_Cat_1_30, category_Cat_1, 30);
        BE_AddTypeToCategory(struct_Cat_2_5, category_Cat_2, 5);
        BE_AddTypeToCategory(struct_Cat_3_4, category_Cat_3, 4);
        
        structStartAddresses[0] = 321;
        structStartAddresses[1] = 123;
        structStartAddresses[2] = 87;
        structStartAddresses[3] = 91;
        structStartAddresses[4] = 86;
        structStartAddresses[5] = 111;
    }
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, structStartAddresses, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*BEGetPtr(&runtime, instructions[1].result) == 1);
}

Test(Runtime_HasType_NonExisting)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[2];
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 5;
    
    instructions[1].type = BE_InstructionType_HasType;
    instructions[1].result = BE_Local(0);
    instructions[1].categoryQuery.categoryIndex = 0;
    instructions[1].categoryQuery.typeNumber = instructions[0].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    BEu32 structStartAddresses[6] = { 0 };
    
    {
        // NOTE(Jens): Three categories, two with one struct and first with four.
        //
        // struct (Cat_1,  0) ...
        // struct (Cat_1, 10) ...
        // struct (Cat_1, 20) ...
        // struct (Cat_1, 30) ...
        // struct (Cat_2,  5) ...
        // struct (Cat_3,  4) ...
        //
        
        BE_ParsedCategoryNode* category_Cat_1 = BE_AcquireCategory(allocator, &structs, BE_StrLiteral("Cat_1"));
        BE_ParsedCategoryNode* category_Cat_2 = BE_AcquireCategory(allocator, &structs, BE_StrLiteral("Cat_2"));
        BE_ParsedCategoryNode* category_Cat_3 = BE_AcquireCategory(allocator, &structs, BE_StrLiteral("Cat_3"));
        
        BE_ParsedStructNode* struct_Cat_1_0 = AddStruct(allocator, &structs, BE_StrLiteral("struct_Cat_1_0"), 0, 0);
        BE_ParsedStructNode* struct_Cat_1_10 = AddStruct(allocator, &structs, BE_StrLiteral("struct_Cat_1_10"), 0, 0);
        BE_ParsedStructNode* struct_Cat_1_20 = AddStruct(allocator, &structs, BE_StrLiteral("struct_Cat_1_20"), 0, 0);
        BE_ParsedStructNode* struct_Cat_1_30 = AddStruct(allocator, &structs, BE_StrLiteral("struct_Cat_1_30"), 0, 0);
        BE_ParsedStructNode* struct_Cat_2_5 = AddStruct(allocator, &structs, BE_StrLiteral("struct_Cat_2_5"), 0, 0);
        BE_ParsedStructNode* struct_Cat_3_4 = AddStruct(allocator, &structs, BE_StrLiteral("struct_Cat_3_4"), 0, 0);
        
        BE_AddTypeToCategory(struct_Cat_1_0, category_Cat_1, 0);
        BE_AddTypeToCategory(struct_Cat_1_10, category_Cat_1, 10);
        BE_AddTypeToCategory(struct_Cat_1_20, category_Cat_1, 20);
        BE_AddTypeToCategory(struct_Cat_1_30, category_Cat_1, 30);
        BE_AddTypeToCategory(struct_Cat_2_5, category_Cat_2, 5);
        BE_AddTypeToCategory(struct_Cat_3_4, category_Cat_3, 4);
        
        structStartAddresses[0] = 321;
        structStartAddresses[1] = 123;
        structStartAddresses[2] = 87;
        structStartAddresses[3] = 91;
        structStartAddresses[4] = 86;
        structStartAddresses[5] = 111;
    }
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, structStartAddresses, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*BEGetPtr(&runtime, instructions[1].result) == 0);
}

Test(Runtime_FetchStartOfType_Existing)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[2];
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 20;
    
    instructions[1].type = BE_InstructionType_FetchStartOfType;
    instructions[1].result = BE_Local(0);
    instructions[1].categoryQuery.categoryIndex = 0;
    instructions[1].categoryQuery.typeNumber = instructions[0].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    BEu32 structStartAddresses[6] = { 0 };
    
    {
        // NOTE(Jens): Three categories, two with one struct and first with four.
        //
        // struct (Cat_1,  0) ...
        // struct (Cat_1, 10) ...
        // struct (Cat_1, 20) ...
        // struct (Cat_1, 30) ...
        // struct (Cat_2,  5) ...
        // struct (Cat_3,  4) ...
        //
        
        BE_ParsedCategoryNode* category_Cat_1 = BE_AcquireCategory(allocator, &structs, BE_StrLiteral("Cat_1"));
        BE_ParsedCategoryNode* category_Cat_2 = BE_AcquireCategory(allocator, &structs, BE_StrLiteral("Cat_2"));
        BE_ParsedCategoryNode* category_Cat_3 = BE_AcquireCategory(allocator, &structs, BE_StrLiteral("Cat_3"));
        
        BE_ParsedStructNode* struct_Cat_1_0 = AddStruct(allocator, &structs, BE_StrLiteral("struct_Cat_1_0"), 0, 0);
        BE_ParsedStructNode* struct_Cat_1_10 = AddStruct(allocator, &structs, BE_StrLiteral("struct_Cat_1_10"), 0, 0);
        BE_ParsedStructNode* struct_Cat_1_20 = AddStruct(allocator, &structs, BE_StrLiteral("struct_Cat_1_20"), 0, 0);
        BE_ParsedStructNode* struct_Cat_1_30 = AddStruct(allocator, &structs, BE_StrLiteral("struct_Cat_1_30"), 0, 0);
        BE_ParsedStructNode* struct_Cat_2_5 = AddStruct(allocator, &structs, BE_StrLiteral("struct_Cat_2_5"), 0, 0);
        BE_ParsedStructNode* struct_Cat_3_4 = AddStruct(allocator, &structs, BE_StrLiteral("struct_Cat_3_4"), 0, 0);
        
        BE_AddTypeToCategory(struct_Cat_1_0, category_Cat_1, 0);
        BE_AddTypeToCategory(struct_Cat_1_10, category_Cat_1, 10);
        BE_AddTypeToCategory(struct_Cat_1_20, category_Cat_1, 20);
        BE_AddTypeToCategory(struct_Cat_1_30, category_Cat_1, 30);
        BE_AddTypeToCategory(struct_Cat_2_5, category_Cat_2, 5);
        BE_AddTypeToCategory(struct_Cat_3_4, category_Cat_3, 4);
        
        structStartAddresses[0] = 321;
        structStartAddresses[1] = 123;
        structStartAddresses[2] = 87;
        structStartAddresses[3] = 91;
        structStartAddresses[4] = 86;
        structStartAddresses[5] = 111;
    }
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, structStartAddresses, 0, 0).data;
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, 0, 0));
    TestAssert(*BEGetPtr(&runtime, instructions[1].result) == 87);
}

Test(Runtime_FetchStartOfType_NonExisting)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[2];
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 5;
    
    instructions[1].type = BE_InstructionType_FetchStartOfType;
    instructions[1].result = BE_Local(0);
    instructions[1].categoryQuery.categoryIndex = 0;
    instructions[1].categoryQuery.typeNumber = instructions[0].result;
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    BEu32 structStartAddresses[6] = { 0 };
    
    {
        // NOTE(Jens): Three categories, two with one struct and first with four.
        //
        // struct (Cat_1,  0) ...
        // struct (Cat_1, 10) ...
        // struct (Cat_1, 20) ...
        // struct (Cat_1, 30) ...
        // struct (Cat_2,  5) ...
        // struct (Cat_3,  4) ...
        //
        
        BE_ParsedCategoryNode* category_Cat_1 = BE_AcquireCategory(allocator, &structs, BE_StrLiteral("Cat_1"));
        BE_ParsedCategoryNode* category_Cat_2 = BE_AcquireCategory(allocator, &structs, BE_StrLiteral("Cat_2"));
        BE_ParsedCategoryNode* category_Cat_3 = BE_AcquireCategory(allocator, &structs, BE_StrLiteral("Cat_3"));
        
        BE_ParsedStructNode* struct_Cat_1_0 = AddStruct(allocator, &structs, BE_StrLiteral("struct_Cat_1_0"), 0, 0);
        BE_ParsedStructNode* struct_Cat_1_10 = AddStruct(allocator, &structs, BE_StrLiteral("struct_Cat_1_10"), 0, 0);
        BE_ParsedStructNode* struct_Cat_1_20 = AddStruct(allocator, &structs, BE_StrLiteral("struct_Cat_1_20"), 0, 0);
        BE_ParsedStructNode* struct_Cat_1_30 = AddStruct(allocator, &structs, BE_StrLiteral("struct_Cat_1_30"), 0, 0);
        BE_ParsedStructNode* struct_Cat_2_5 = AddStruct(allocator, &structs, BE_StrLiteral("struct_Cat_2_5"), 0, 0);
        BE_ParsedStructNode* struct_Cat_3_4 = AddStruct(allocator, &structs, BE_StrLiteral("struct_Cat_3_4"), 0, 0);
        
        BE_AddTypeToCategory(struct_Cat_1_0, category_Cat_1, 0);
        BE_AddTypeToCategory(struct_Cat_1_10, category_Cat_1, 10);
        BE_AddTypeToCategory(struct_Cat_1_20, category_Cat_1, 20);
        BE_AddTypeToCategory(struct_Cat_1_30, category_Cat_1, 30);
        BE_AddTypeToCategory(struct_Cat_2_5, category_Cat_2, 5);
        BE_AddTypeToCategory(struct_Cat_3_4, category_Cat_3, 4);
        
        structStartAddresses[0] = 321;
        structStartAddresses[1] = 123;
        structStartAddresses[2] = 87;
        structStartAddresses[3] = 91;
        structStartAddresses[4] = 86;
        structStartAddresses[5] = 111;
    }
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, structStartAddresses, 0, 0).data;
    
    BE_RuntimeEventType events[] = {
        BE_RuntimeEventType_Error_CategoryTypeNotFound
    };
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, 0, 0, BE_ArrayCount(events), events));
}

Test(Runtime_Plot_X)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 123;
    
    BEf64 sample = 1.2;
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(0);
    memcpy(&instructions[1].set.value, &sample, 8);
    
    instructions[2].type = BE_InstructionType_Plot_F8_X;
    instructions[2].plot.plotId = BE_Local(0);
    instructions[2].plot.value_f64 = BE_Local(0);
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    BEu32 structStartAddresses[1] = { 0 };
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, structStartAddresses, 0, 0).data;
    
    BE_RuntimeEventType events[] = {
        BE_RuntimeEventType_PlotX
    };
    
    BEu8 binaryFile[1024] = { 0 };
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, sizeof(binaryFile), binaryFile, BE_ArrayCount(events), events));
}

Test(Runtime_Plot_Y)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[3] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 123;
    
    BEf64 sample = 1.2;
    
    instructions[1].type = BE_InstructionType_Set;
    instructions[1].result = BE_Local(0);
    memcpy(&instructions[1].set.value, &sample, 8);
    
    instructions[2].type = BE_InstructionType_Plot_F8_Y;
    instructions[2].plot.plotId = BE_Local(0);
    instructions[2].plot.value_f64 = BE_Local(0);
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    BEu32 structStartAddresses[1] = { 0 };
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, structStartAddresses, 0, 0).data;
    
    BE_RuntimeEventType events[] = {
        BE_RuntimeEventType_PlotY
    };
    
    BEu8 binaryFile[1024] = { 0 };
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, sizeof(binaryFile), binaryFile, BE_ArrayCount(events), events));
}

Test(Runtime_CreatePlot)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[1] = { 0 };
    
    instructions[0].type = BE_InstructionType_CreatePlot;
    instructions[0].result = BE_Local(0);
    
    BE_ParsedStructs structs;
    BE_ClearStructToZero(structs);
    
    BE_ParsedEnums enums;
    BE_ClearStructToZero(enums);
    
    BE_TreeIterator* treeFreelist = 0;
    BEu32 structStartAddresses[1] = { 0 };
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, structStartAddresses, 0, 0).data;
    
    BEu8 binaryFile[1024] = { 0 };
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, sizeof(binaryFile), binaryFile, 0, 0));
    TestAssert(runtime.createdPlotCount == 1);
}

Test(Runtime_IntToFloat)
{
    BEAllocatorType allocator;
    BE_ClearStructToZero(allocator);
    
    BEInstruction instructions[2] = { 0 };
    
    instructions[0].type = BE_InstructionType_Set;
    instructions[0].result = BE_Local(0);
    instructions[0].set.value = 4;
    
    instructions[1].type = BE_InstructionType_IntToFloat;
    instructions[1].result = BE_Local(0);
    instructions[1].conversion.source = BE_Local(0);
    
    BE_ParsedStructs structs = { 0 };
    BE_ParsedEnums enums = { 0 };
    
    BE_TreeIterator* treeFreelist = 0;
    BEu32 structStartAddresses[1] = { 0 };
    
    BEu8* dataSection = BE_CreateDataSection(allocator, &treeFreelist, 64, BE_ArrayCount(instructions), &structs, &enums, structStartAddresses, 0, 0).data;
    
    BEu8 binaryFile[1024] = { 0 };
    
    BERuntime runtime;
    TestAssert(RunInstructions(&runtime, dataSection, BE_ArrayCount(instructions), instructions, BE_ArrayCount(instructions), instructions, sizeof(binaryFile), binaryFile, 0, 0));
    BEf64 actualValue = *(BEf64*)BEGetPtr(&runtime, BE_Local(0));
    TestAssert(actualValue == 4.0);
}

typedef struct
{
    BEu32 address;
    char* fullName;
    BEs64 value;
} MemberValue;

typedef struct
{
    BEu32 count;
    BE_String* names;
    BEu32* sizes;
    BEu8** files;
} LibraryFiles;

typedef struct
{
    BEu32 mainFileSize;
    BEu8* mainFile;
    
    LibraryFiles* library;
} TestFileLoadedContext;

static BELoadFileResult LoadTestFile(void* context_, BEAllocatorType allocator, BEu32 pathLen, const char* nullTerminatedFilename)
{
    (void)allocator;
    
    TestFileLoadedContext* context = (TestFileLoadedContext*)context_;
    LibraryFiles* libraryFiles = context->library;
    
    BELoadFileResult rVal;
    BE_ClearStructToZero(rVal);
    
    BE_String filename;
    filename.count = pathLen;
    filename.characters = (char*)nullTerminatedFilename;
    
    if (BE_StringsAreEqual(filename, BE_StrLiteral("main.bet")))
    {
        rVal.fileSizeInBytes = context->mainFileSize;
        rVal.fileContent = context->mainFile;
    }
    else if (libraryFiles)
    {
        for (BEu32 i = 0; i < libraryFiles->count; ++i)
        {
            if (BE_StringsAreEqual(filename, libraryFiles->names[i]))
            {
                rVal.fileSizeInBytes = libraryFiles->sizes[i];
                rVal.fileContent = libraryFiles->files[i];
                break;
            }
        }
    }
    
    if (!rVal.fileContent)
    {
        rVal.error = 1;
    }
    
    return rVal;
}

static BEu32 SerializeInstructions(BEAllocatorType allocator, BEu32 layoutFileSize, BEu8* layoutFile, BEu8** instructionsOut, BEu8** dataSectionOut, LibraryFiles* libraryFiles)
{
    BEFileLoader fileLoader;
    BE_ClearStructToZero(fileLoader);
    
    TestFileLoadedContext fileLoaderContext;
    BE_ClearStructToZero(fileLoaderContext);
    
    fileLoaderContext.mainFile = layoutFile;
    fileLoaderContext.mainFileSize = layoutFileSize;
    fileLoaderContext.library = libraryFiles;
    
    fileLoader.context = &fileLoaderContext;
    fileLoader.loadFile = LoadTestFile;
    
    BECompilationContext compilation;
    
    {
        BEFileLoadError err = BEStartCompilation(&compilation, allocator, fileLoader, 0, "main.bet");
        BEAssert(!err);
    }
    
    BEu32 instructionCapacity = 32 * 1024;
    *instructionsOut = BEAllocArray(allocator, BEu8, instructionCapacity);
    
    BECompilationEvent event;
    BEu8* out = *instructionsOut;
    
    do
    {
        BEu32 writtenBytes = instructionCapacity;
        
        event = BEGenerateInstructions(&compilation, &writtenBytes, out);
        
        if (writtenBytes)
        {
            instructionCapacity -= writtenBytes;
            out += writtenBytes;
            
            BEAssert(instructionCapacity > 0);
        }
    }
    while (event.type != BECompilationEventType_Complete);
    
    *dataSectionOut = BEFinalizeCompilation(&compilation, 0).data;
    
    return (BEu32)(out - *instructionsOut);
}

static BEbool RunExample(BEu32 layoutFileSize, BEu8* layoutFile, BEu32 binaryFileSize, BEu8* binaryFile, BEu32 memberCount, MemberValue* members, LibraryFiles* libraryFiles)
{
    BEAllocatorType allocator = { 0 };
    
    BEu8* instructions;
    BEu8* dataSection;
    BEu32 byteCount = SerializeInstructions(allocator, layoutFileSize, layoutFile, &instructions, &dataSection, libraryFiles);
    
    BEu8 stack[1024];
    BERuntime runtime = BEStartRuntime(allocator, BE_ArrayCount(stack), stack, dataSection, byteCount, instructions, binaryFileSize, binaryFile);
    
    BEbool rVal = 1;
    BEu64 memberIndex = 0;
    
    for (BERuntimeEvent event = BE_RunUntilEventOfInterest(&runtime);
         rVal && event.type != BE_RuntimeEventType_End;
         event = BE_RunUntilEventOfInterest(&runtime))
    {
        if (event.type == BE_RuntimeEventType_ScalarExport)
        {
            BEu8* data = binaryFile + event.scalarExport.address;
            
            BE_String structName = BE_GetStructName(runtime.typesSection, event.scalarExport.structIndex);
            BE_String memberName = BE_GetStructMemberName(runtime.typesSection, event.scalarExport.structIndex, event.scalarExport.memberIndex);
            
            if (event.scalarExport.arrayCount)
            {
                for (BEu32 arrayIndex = 0; arrayIndex < event.scalarExport.arrayCount; ++arrayIndex)
                {
                    BEbool nameIsCorrect = 1;
                    BEu32 nameIndex = 0;
                    
                    for (BEu32 i = 0; nameIsCorrect && i < structName.count; ++i)
                    {
                        nameIsCorrect = nameIsCorrect && (members[memberIndex].fullName[nameIndex++] == structName.characters[i]);
                    }
                    
                    nameIsCorrect = nameIsCorrect && (members[memberIndex].fullName[nameIndex++] == '.');
                    
                    for (BEu32 i = 0; nameIsCorrect && i < memberName.count; ++i)
                    {
                        nameIsCorrect = nameIsCorrect && (members[memberIndex].fullName[nameIndex++] == memberName.characters[i]);
                    }
                    
                    nameIsCorrect = nameIsCorrect && (members[memberIndex].fullName[nameIndex++] == '[');
                    
                    BEu32 index = 0;
                    while (members[memberIndex].fullName[nameIndex] != ']')
                    {
                        BEAssert('0' <= members[memberIndex].fullName[nameIndex] && members[memberIndex].fullName[nameIndex] <= '9');
                        
                        index *= 10;
                        index += members[memberIndex].fullName[nameIndex] - '0';
                        ++nameIndex;
                    }
                    
                    BEbool valueIsSame;
                    if (event.scalarExport.size <= 8)
                    {
                        valueIsSame = BE_ValueIs(data + arrayIndex * event.scalarExport.size, event.scalarExport.size, event.scalarExport.isBigEndian, (BEu8*)&members[memberIndex].value);
                    }
                    else
                    {
                        valueIsSame = (members[memberIndex].value == 0);
                    }
                    
                    if (memberIndex >= memberCount 
                        || index != arrayIndex
                        || event.scalarExport.address + arrayIndex * event.scalarExport.size != members[memberIndex].address
                        || !valueIsSame)
                    {
                        rVal = 0;
                        break;
                    }
                    
                    ++memberIndex;
                }
            }
            else
            {
                BEbool nameIsCorrect = 1;
                BEu32 nameIndex = 0;
                
                for (BEu32 i = 0; nameIsCorrect && i < structName.count; ++i)
                {
                    nameIsCorrect = nameIsCorrect && (members[memberIndex].fullName[nameIndex++] == structName.characters[i]);
                }
                
                nameIsCorrect = nameIsCorrect && (members[memberIndex].fullName[nameIndex++] == '.');
                
                for (BEu32 i = 0; nameIsCorrect && i < memberName.count; ++i)
                {
                    nameIsCorrect = nameIsCorrect && (members[memberIndex].fullName[nameIndex++] == memberName.characters[i]);
                }
                
                nameIsCorrect = nameIsCorrect && (members[memberIndex].fullName[nameIndex++] == '\0');
                
                BEbool valueIsSame;
                if (event.scalarExport.size <= 8)
                {
                    valueIsSame = BE_ValueIs(data, event.scalarExport.size, event.scalarExport.isBigEndian, (BEu8*)&members[memberIndex].value);
                }
                else
                {
                    valueIsSame = (members[memberIndex].value == 0);
                }
                
                if (!nameIsCorrect
                    || memberIndex >= memberCount 
                    || event.scalarExport.address != members[memberIndex].address
                    || !valueIsSame
                    || !nameIndex)
                {
                    rVal = 0;
                    break;
                }
                
                ++memberIndex;
            }
        }
        else if (event.type >= BE_RuntimeEventType_Error_First)
        {
            rVal = 0;
            break;
        }
    }
    
    if (memberIndex != memberCount)
    {
        rVal = 0;
    }
    
    return rVal;
}

static BEbool CompileExampleWithExpectedError(BEu32 layoutFileSize, BEu8* layoutFile, BECompilationEventType expectedError, BEu32 lineIndex, BEu32 colIndex)
{
    BEAllocatorType allocator = { 0 };
    
    BEFileLoader fileLoader;
    BE_ClearStructToZero(fileLoader);
    
    TestFileLoadedContext fileLoaderContext;
    BE_ClearStructToZero(fileLoaderContext);
    
    fileLoaderContext.mainFile = layoutFile;
    fileLoaderContext.mainFileSize = layoutFileSize;
    
    fileLoader.context = &fileLoaderContext;
    fileLoader.loadFile = LoadTestFile;
    
    BECompilationContext compilation;
    
    {
        BEFileLoadError err = BEStartCompilation(&compilation, allocator, fileLoader, 0, "main.bet");
        BEAssert(!err);
    }
    
    BEu32 instructionCapacity = 32 * 1024;
    BEu8* instructions = BEAllocArray(allocator, BEu8, instructionCapacity);
    
    BECompilationEvent event;
    BEu8* out = instructions;
    
    do
    {
        BEu32 writtenBytes = instructionCapacity;
        
        event = BEGenerateInstructions(&compilation, &writtenBytes, out);
        
        if (writtenBytes)
        {
            instructionCapacity -= writtenBytes;
            out += writtenBytes;
            
            BEAssert(instructionCapacity > 0);
        }
    }
    while (event.type == BECompilationEventType_Continue);
    
    BEbool rVal = (event.type == expectedError && event.sourceLineIndex == lineIndex && event.sourceColIndex == colIndex);
    return rVal;
}

static BEbool RunExampleWithExpectedError(BEu32 layoutFileSize, BEu8* layoutFile, BEu32 binaryFileSize, BEu8* binaryFile, BE_RuntimeEventType err, BEu32 lineIndex, BEu32 colIndex)
{
    BEAllocatorType allocator = { 0 };
    
    BEu8* instructions;
    BEu8* dataSection;
    BEu32 byteCount = SerializeInstructions(allocator, layoutFileSize, layoutFile, &instructions, &dataSection, 0);
    
    BEu8 stack[1024];
    BERuntime runtime = BEStartRuntime(allocator, BE_ArrayCount(stack), stack, dataSection, byteCount, instructions, binaryFileSize, binaryFile);
    
    BEbool rVal = 0;
    
    for (BERuntimeEvent event = BE_RunUntilEventOfInterest(&runtime);
         event.type != BE_RuntimeEventType_End;
         event = BE_RunUntilEventOfInterest(&runtime))
    {
        if (BE_RuntimeEventType_Error_First <= event.type && event.type <= BE_RuntimeEventType_Error_Last)
        {
            if (event.type == err && event.sourceLineIndex == lineIndex && event.sourceColIndex == colIndex)
            {
                rVal = 1;
            }
            
            break;
        }
    }
    
    return rVal;
}

Test(Example_CompileError_InvalidToken)
{
    char layoutFile[] = "var ¤ = 1;\n";
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidToken, 0, 4));
}

Test(Example_CompileError_FileLoadFailed)
{
    char layoutFile[] = "import('doesnt_exist.bet');\n";
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_FileLoadFailed, 0, 26));
}

Test(Example_CompileError_ConstantExpression_DivZero)
{
    char layoutFile[] = "var i = 1 / 0;\n";
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ConstantExpression_DivZero, 0, 13));
}

Test(Example_CompileError_ConstantExpression_ModZero)
{
    char layoutFile[] = "var i = 1 % (1 - 1);\n";
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ConstantExpression_ModZero, 0, 19));
}

Test(Example_CompileError_InvalidStatement)
{
    char layoutFile[] = "123;\n";
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedStatementStart, 0, 0));
}

Test(Example_CompileError_InvalidStatement_EndOfStatement_ExpectedSemicolon)
{
    char layoutFile[] = 
        "enum {}\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedSemicolon, 0, 7));
}

Test(Example_CompileError_InvalidStatement_Breakpoint_MissingOpenParenthesis)
{
    char layoutFile[] = "breakpoint);\n";
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedOpenParenthesis, 0, 10));
}

Test(Example_CompileError_InvalidStatement_Breakpoint_MissingCloseParenthesis)
{
    char layoutFile[] = "breakpoint(;\n";
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedCloseParenthesis, 0, 11));
}

Test(Example_CompileError_InvalidStatement_Print_MissingOpenParenthesis)
{
    char layoutFile[] = "print);\n";
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedOpenParenthesis, 0, 5));
}

Test(Example_CompileError_InvalidStatement_Print_MissingCloseParenthesis)
{
    char layoutFile[] = 
        "var i = 0;\n"
        "print('i is ', i;\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedCloseParenthesis, 1, 16));
}

Test(Example_CompileError_InvalidStatement_Print_MissingCommaOrCloseParenthesis)
{
    char layoutFile[] = 
        "var i = 0;\n"
        "print('i is ';\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedCommaOrCloseParenthesis, 1, 13));
}

Test(Example_CompileError_InvalidStatement_Assert_MissingOpenParenthesis)
{
    char layoutFile[] = 
        "assert 0;\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedOpenParenthesis, 0, 7));
}

Test(Example_CompileError_InvalidStatement_Assert_MissingCloseParenthesis)
{
    char layoutFile[] = 
        "assert(0;\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedCloseParenthesis, 0, 8));
}

Test(Example_CompileError_InvalidStatement_VariableDeclaration_MissingVarName)
{
    char layoutFile[] = 
        "var 1;\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedIdentifier, 0, 4));
}

Test(Example_CompileError_InvalidStatement_TypedefDeclaration_MissingName)
{
    char layoutFile[] = 
        "typedef u(4) 'foobar';\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedIdentifier, 0, 13));
}

Test(Example_CompileError_InvalidStatement_Layout_MissingType)
{
    char layoutFile[] = 
        "layout DoesntExist;\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedStructName, 0, 7));
}

Test(Example_CompileError_InvalidStatement_Enum_InvalidSize)
{
    char layoutFile[] = 
        "enum Foo(256) { Bar };\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_InvalidEnumSize, 0, 12));
}

Test(Example_CompileError_InvalidStatement_Enum_MissingCloseParenthesis)
{
    char layoutFile[] = 
        "enum Foo(4 { Bar };\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedCloseParenthesis, 0, 11));
}

Test(Example_CompileError_InvalidStatement_Enum_MissingOpenCurlyBracket)
{
    char layoutFile[] = 
        "enum Foo(4) Bar };\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedOpenCurlyBracket, 0, 12));
}

Test(Example_CompileError_InvalidStatement_Enum_MissingCloseCurlyBracket_TrailingComma)
{
    char layoutFile[] = 
        "enum Foo(4) { Bar, ;\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedCloseCurlyBracket, 0, 19));
}

Test(Example_CompileError_InvalidStatement_Enum_MissingCloseCurlyBracket)
{
    char layoutFile[] = 
        "enum Foo(4) { Bar ;\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedCloseCurlyBracket, 0, 18));
}

Test(Example_CompileError_InvalidStatement_Enum_MissingCloseCurlyBracket_TrailingExpression)
{
    char layoutFile[] = 
        "enum Foo(4) { Bar = 123;\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedCommaOrCloseCurlyBracket, 0, 23));
}

Test(Example_CompileError_InvalidStatement_Import_MissingOpenParenthesis)
{
    char layoutFile[] = 
        "import 'foo.bet';";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedOpenParenthesis, 0, 7));
}

Test(Example_CompileError_InvalidStatement_Import_MissingCloseParenthesis)
{
    char layoutFile[] = 
        "import('foo.bet';";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedCloseParenthesis, 0, 16));
}

Test(Example_CompileError_InvalidStatement_StructHeader_MissingName)
{
    char layoutFile[] = 
        "struct { };";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedIdentifierOrOpenParenthesis, 0, 7));
}

Test(Example_CompileError_InvalidStatement_StructHeader_MissingCategoryName)
{
    char layoutFile[] = 
        "struct('foobar') Foo { };";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedIdentifier, 0, 7));
}

Test(Example_CompileError_InvalidStatement_StructHeader_MissingCategoryComma)
{
    char layoutFile[] = 
        "struct(Kek 'foobar') Foo { };";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedComma, 0, 11));
}

Test(Example_CompileError_InvalidStatement_StructHeader_MissingCategoryCloseParenthesis)
{
    char layoutFile[] = 
        "struct(Kek, 'foobar' Foo { };";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedCloseParenthesis, 0, 21));
}

Test(Example_CompileError_InvalidStatement_StructHeader_CategoryDivZero)
{
    char layoutFile[] = 
        "struct(Kek, 1/0) Foo { };\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ConstantExpression_DivZero, 0, 15));
}

Test(Example_CompileError_InvalidStatement_StructHeader_CategoryModZero)
{
    char layoutFile[] = 
        "struct(Kek, 1%0) Foo { };\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ConstantExpression_ModZero, 0, 15));
}

Test(Example_CompileError_InvalidStatement_StructHeader_MissingNameAfterCategory)
{
    char layoutFile[] = 
        "struct(Kek, 123) { };\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedIdentifier, 0, 17));
}

Test(Example_CompileError_InvalidStatement_StructHeader_MissingParamVar)
{
    char layoutFile[] = 
        "struct Foo(i) { };\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedKeyword_var, 0, 11));
}

Test(Example_CompileError_InvalidStatement_StructHeader_MissingParamName)
{
    char layoutFile[] = 
        "struct Foo(var) { };\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedIdentifier, 0, 14));
}

Test(Example_CompileError_InvalidStatement_StructHeader_DuplicateParamName)
{
    char layoutFile[] = 
        "struct Foo(var i, var i) { };\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_DuplicateParameterName, 0, 22));
}

Test(Example_CompileError_InvalidStatement_StructHeader_MissingEndOfParameters)
{
    char layoutFile[] = 
        "struct Foo(var i, var j { };\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedCommaOrCloseParenthesis, 0, 24));
}

Test(Example_CompileError_InvalidStatement_StructHeader_MissingOpenCurlyBracket)
{
    char layoutFile[] = 
        "struct Foo };\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedOpenCurlyBracket, 0, 11));
}

Test(Example_CompileError_InvalidStatement_StructHeader_DuplicateStructName)
{
    char layoutFile[] = 
        "struct Foo {};\n"
        "struct Foo {};\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_DuplicateStructName, 1, 11));
}

Test(Example_CompileError_InvalidStatement_EndOfStruct_MissingSemicolon)
{
    char layoutFile[] = 
        "struct Foo {}\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedSemicolon, 0, 13));
}

Test(Example_CompileError_InvalidStatement_EndOfDoWhile_MissingWhile)
{
    char layoutFile[] = 
        "do {} while_ (0);";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedKeyword_while, 0, 6));
}

Test(Example_CompileError_InvalidStatement_EndOfDoWhile_MissingOpenParenthesis)
{
    char layoutFile[] = 
        "do {} while 0;";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedOpenParenthesis, 0, 12));
}

Test(Example_CompileError_InvalidStatement_EndOfDoWhile_MissingCloseParenthesis)
{
    char layoutFile[] = 
        "do {} while (0;";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedCloseParenthesis, 0, 14));
}

Test(Example_CompileError_InvalidStatement_ConstantIntegerExpression_InvalidLookup)
{
    char layoutFile[] = 
        "enum Foo\n"
        "{\n"
        "    Fool\n"
        "};\n"
        "enum Bar\n"
        "{\n"
        "    Pub = Foo.Food,\n"
        "};\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ConstantLookupFailure, 6, 14));
}

Test(Example_CompileError_InvalidStatement_ConstantIntegerExpression_InvalidSyntax)
{
    char layoutFile[] = 
        "enum Foo\n"
        "{\n"
        "    Fool\n"
        "};\n"
        "enum Bar\n"
        "{\n"
        "    Pub = Foo:Fool\n"
        "};\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ConstantLookupFailure, 6, 13));
}

Test(Example_CompileError_InvalidStatement_ConstantIntegerExpression_InvalidNestedLookup)
{
    char layoutFile[] = 
        "enum Foo\n"
        "{\n"
        "    Fool\n"
        "};\n"
        "enum Bar\n"
        "{\n"
        "    Pub = Foo.Fool.Kek\n"
        "};\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ConstantLookupFailure, 6, 18));
}

Test(Example_CompileError_InvalidStatement_ConstantIntegerExpression_InvalidExpression)
{
    char layoutFile[] = 
        "enum Foo\n"
        "{\n"
        "    Fool\n"
        "};\n"
        "enum Bar\n"
        "{\n"
        "    Pub = 7 + 8 Foo.Fool,\n"
        "};\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_IntegerExpression, 6, 24));
}

Test(Example_CompileError_InvalidStatement_Load_MissingOpenParenthesis)
{
    char layoutFile[] = 
        "var i = load u(4);\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedOpenParenthesis, 0, 13));
}

Test(Example_CompileError_InvalidStatement_Load_MissingAddressOpenParenthesis)
{
    char layoutFile[] = 
        "var i = load(@64 u(4));\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedOpenParenthesis, 0, 14));
}

Test(Example_CompileError_InvalidStatement_Load_MissingAddressCloseParenthesis)
{
    char layoutFile[] = 
        "var i = load(@(64 u(4));\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedCloseParenthesis, 0, 18));
}

Test(Example_CompileError_InvalidStatement_Load_InvalidScalar)
{
    char layoutFile[] = 
        "var i = load(f(4));\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedKeyword_s_or_u, 0, 13));
}

Test(Example_CompileError_InvalidStatement_Load_MissingSizeOpenParenthesis)
{
    char layoutFile[] = 
        "var i = load(u 4));\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedOpenParenthesis, 0, 15));
}

Test(Example_CompileError_InvalidStatement_Load_MissingSizeCloseParenthesis)
{
    char layoutFile[] = 
        "var i = load(u(4]);\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedCloseParenthesis, 0, 16));
}

Test(Example_CompileError_InvalidStatement_Load_MissingCloseParenthesis)
{
    char layoutFile[] = 
        "var i = load(u(4);\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedCloseParenthesis, 0, 17));
}

Test(Example_CompileError_InvalidStatement_HasType_MissingOpenParenthesis)
{
    char layoutFile[] = 
        "struct(Foo, 123) Bar {};\n"
        "var i = has_type Foo, 123);\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedOpenParenthesis, 1, 17));
}

Test(Example_CompileError_InvalidStatement_HasType_BadCategoryName)
{
    char layoutFile[] = 
        "struct(Foo, 123) Bar {};\n"
        "var i = has_type(Fool, 123);\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedCategoryName, 1, 17));
}

Test(Example_CompileError_InvalidStatement_HasType_MissingComma)
{
    char layoutFile[] = 
        "struct(Foo, 123) Bar {};\n"
        "var i = has_type(Foo 123);\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedComma, 1, 21));
}

Test(Example_CompileError_InvalidStatement_HasType_MissingCloseParenthesis)
{
    char layoutFile[] = 
        "struct(Foo, 123) Bar {};\n"
        "var i = has_type(Foo, 123;\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedCloseParenthesis, 1, 25));
}

Test(Example_CompileError_InvalidStatement_SizeOf_MissingOpenParenthesis)
{
    char layoutFile[] = 
        "struct Bar { u(4) mem; };\n"
        "var i = sizeof Bar;\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedOpenParenthesis, 1, 15));
}

Test(Example_CompileError_InvalidStatement_SizeOf_InvalidStructName)
{
    char layoutFile[] = 
        "struct Bar { u(4) mem; };\n"
        "var i = sizeof(Bax);\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedStructName, 1, 15));
}

Test(Example_CompileError_InvalidStatement_SizeOf_InvalidIntegerExpression)
{
    char layoutFile[] = 
        "struct Bar { u(4) mem; };\n"
        "var i = 1 + 5 sizeof(Bar);\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_IntegerExpression, 1, 14));
}

Test(Example_CompileError_InvalidStatement_SizeOf_MissingCloseParenthesis)
{
    char layoutFile[] = 
        "struct Bar { u(4) mem; };\n"
        "var i = sizeof(Bar;\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedCloseParenthesis, 1, 18));
}

Test(Example_CompileError_InvalidStatement_NonExistingNestedMember)
{
    char layoutFile[] = 
        "struct Foo { u(4) mem; };\n"
        "struct Bar\n"
        "{\n"
        "    Foo foo;\n"
        "    var i = foo.men;\n"
        "};\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedMemberName, 4, 16));
}

Test(Example_CompileError_InvalidStatement_BadNestedMemberSyntax)
{
    char layoutFile[] = 
        "struct Foo { u(4) mem; };\n"
        "struct Bar\n"
        "{\n"
        "    Foo foo;\n"
        "    var i = foo,mem;\n"
        "};\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedDot, 4, 15));
}

Test(Example_CompileError_InvalidStatement_BadNestedArraMemberSyntax)
{
    char layoutFile[] = 
        "struct Foo { u(4) mem[8]; };\n"
        "struct Bar\n"
        "{\n"
        "    Foo foo;\n"
        "    var i = foo.mem;\n"
        "};\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedOpenSquareBracket, 4, 19));
}

Test(Example_CompileError_InvalidStatement_NestedArraMemberSyntax_MissingCloseSquareBracket)
{
    char layoutFile[] = 
        "struct Foo { u(4) mem[8]; };\n"
        "struct Bar\n"
        "{\n"
        "    Foo foo;\n"
        "    var i = foo.mem[2;\n"
        "};\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedCloseSquareBracket, 4, 21));
}

Test(Example_CompileError_InvalidStatement_AddressSpecification_MissingOpenParenthesis)
{
    char layoutFile[] = 
        "struct Foo\n"
        "{\n"
        "    @23 u(4) kek;\n"
        "};\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedOpenParenthesisOrAssignmentOperator, 2, 5));
}

Test(Example_CompileError_InvalidStatement_AddressSpecification_MissingCloseParenthesis)
{
    char layoutFile[] = 
        "struct Foo\n"
        "{\n"
        "    @(23 u(4) kek;\n"
        "};\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedCloseParenthesis, 2, 9));
}

Test(Example_CompileError_InvalidStatement_Preprocessor_Unknown)
{
    char layoutFile[] = 
        "#whatevs;\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedKeyword_default, 0, 1));
}

Test(Example_CompileError_InvalidStatement_Preprocessor_Default_MissingOpenParenthesis)
{
    char layoutFile[] = 
        "#default byteorder=big;\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedOpenParenthesis, 0, 9));
}

Test(Example_CompileError_InvalidStatement_Preprocessor_Default_Unknown)
{
    char layoutFile[] = 
        "#default(endian=big);\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedKeyword_byteorder, 0, 9));
}

Test(Example_CompileError_InvalidStatement_Preprocessor_Default_ByteOrder_MissingEquals)
{
    char layoutFile[] = 
        "#default(byteorder big);\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedEquals, 0, 19));
}

Test(Example_CompileError_InvalidStatement_Preprocessor_Default_ByteOrder_UnknownValue)
{
    char layoutFile[] = 
        "#default(byteorder=litle);\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedKeyword_little_or_big, 0, 19));
}

Test(Example_CompileError_InvalidStatement_Preprocessor_Default_ByteOrder_MissingEndParenthesis)
{
    char layoutFile[] = 
        "#default(byteorder=big;\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedCloseParenthesis, 0, 22));
}

Test(Example_CompileError_InvalidStatement_Preprocessor_Default_ByteOrder_MissingSemicolon)
{
    char layoutFile[] = 
        "#default(byteorder=big)\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedSemicolon, 0, 23));
}

Test(Example_CompileError_InvalidStatement_Assignment_BadOperator)
{
    char layoutFile[] = 
        "var i;\n"
        "i ||= 4;\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedAssignmentOperator, 1, 2));
}

Test(Example_CompileError_InvalidStatement_DuplicateEnumMemberNames_Local)
{
    char layoutFile[] = 
        "enum Foo\n"
        "{\n"
        "    Bar,\n"
        "    Bar,\n"
        "};\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_DuplicateEnumMemberNames, 3, 4));
}

Test(Example_CompileError_InvalidStatement_DuplicateEnumMemberNames_Global)
{
    char layoutFile[] = 
        "enum\n"
        "{\n"
        "    Bar,\n"
        "};\n"
        "enum\n"
        "{\n"
        "    Bar,\n"
        "};\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_DuplicateEnumMemberNames, 6, 4));
}

Test(Example_CompileError_InvalidStatement_ExpectedSemicolon_ForVar)
{
    char layoutFile[] = 
        "for (var i = 1) {}\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedSemicolon, 0, 14));
}

Test(Example_CompileError_InvalidStatement_ExpectedSemicolon_ForCondition)
{
    char layoutFile[] = 
        "for (var i = 1; i < 8) {}\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedSemicolon, 0, 21));
}

Test(Example_CompileError_InvalidStatement_ExpectedIdentifier_MemberName)
{
    char layoutFile[] = 
        "struct Foo \n"
        "{\n"
        "    u(4) 123;\n"
        "};\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedIdentifier, 2, 9));
}

Test(Example_CompileError_InvalidStatement_ExpectedIdentifier_ForVar)
{
    char layoutFile[] = 
        "for (var 123 = 1; 123 < 8; 123 += 1) {}\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedIdentifier, 0, 9));
}

Test(Example_CompileError_InvalidStatement_ExpectedVariable_ForAssignment)
{
    char layoutFile[] = 
        "for (var i = 1; i < 8; j += 1) {}\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedVariable, 0, 23));
}

Test(Example_CompileError_InvalidStatement_ExpectedAssignmentOperator_ForAssignment)
{
    // NOTE(Jens): ++ not supported (yet).
    char layoutFile[] = 
        "for (var i = 1; i < 8; i++) {}\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_ExpectedAssignmentOperator, 0, 24));
}

Test(Example_CompileError_InvalidStatement_DuplicateVariableName)
{
    char layoutFile[] = 
        "var i;\n"
        "var i;\n";
    
    TestAssert(CompileExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, BECompilationEventType_Error_InvalidStatement_DuplicateVariableName, 1, 5));
}

Test(Example_RuntimeError_AssertTriggered)
{
    BEu8 binaryFile[] = {
        0x00, 0x01, 0x02, 0x03,  0x04, 0x05, 0x06, 0x07,
    };
    
    char layoutFile[] = 
        "struct Foo\n"
        "{\n"
        "    assert(0);\n"
        "};\n"
        "layout Foo;\n";
    
    TestAssert(RunExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, sizeof(binaryFile), binaryFile, BE_RuntimeEventType_Error_AssertTriggered, 2, 4));
}

Test(Example_RuntimeError_DivByZero)
{
    BEu8 binaryFile[] = {
        0x00, 0x01, 0x02, 0x03,  0x04, 0x05, 0x06, 0x07,
    };
    
    // NOTE(Jens): First byte in binary file is 0x00. 
    //             Note that dividing with zero in an expression that is evaluated at compilation will be reported during compile time.
    
    char layoutFile[] = 
        "struct Foo\n"
        "{\n"
        "    u(1) den;\n"
        "    print(123 / den);\n"
        "};\n"
        "layout Foo;\n";
    
    TestAssert(RunExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, sizeof(binaryFile), binaryFile, BE_RuntimeEventType_Error_DivByZero, 3, 4));
}

Test(Example_RuntimeError_ModByZero)
{
    BEu8 binaryFile[] = {
        0x00, 0x01, 0x02, 0x03,  0x04, 0x05, 0x06, 0x07,
    };
    
    char layoutFile[] = 
        "struct Foo\n"
        "{\n"
        "    u(1) den;\n"
        "    print(123 % den);\n"
        "};\n"
        "layout Foo;\n";
    
    TestAssert(RunExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, sizeof(binaryFile), binaryFile, BE_RuntimeEventType_Error_ModByZero, 3, 4));
}

Test(Example_RuntimeError_FetchMemoryInvalidMemoryRange)
{
    BEu8 binaryFile[] = {
        0x00, 0x01, 0x02, 0x03,  0x04, 0x05, 0x06, 0x07,
    };
    
    char layoutFile[] = 
        "struct Foo\n"
        "{\n"
        "    var bar = load(@(123) u(4));\n"
        "};\n"
        "layout Foo;\n";
    
    TestAssert(RunExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, sizeof(binaryFile), binaryFile, BE_RuntimeEventType_Error_FetchMemoryInvalidMemoryRange, 2, 4));
}

Test(Example_RuntimeError_FetchMemoryTooBig)
{
    BEu8 binaryFile[] = {
        0x09, 0x01, 0x02, 0x03,  0x04, 0x05, 0x06, 0x07,
        0x08, 0x09, 0x0a, 0x0b,  0x0c, 0x0d, 0x0e, 0x0f,
        0x10, 0x11, 0x12, 0x13,  0x14, 0x15, 0x16, 0x17,
        0x18, 0x19, 0x1a, 0x1b,  0x1c, 0x1d, 0x1e, 0x1f,
    };
    
    char layoutFile[] = 
        "struct Foo\n"
        "{\n"
        "    u(1) size;\n" // NOTE(Jens): First byte is 9, but variables are 8 byte integers.
        "    var bar = load(u(size));\n"
        "};\n"
        "layout Foo;\n";
    
    TestAssert(RunExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, sizeof(binaryFile), binaryFile, BE_RuntimeEventType_Error_FetchMemoryTooBig, 3, 4));
}

Test(Example_RuntimeError_CategoryTypeNotFound)
{
    BEu8 binaryFile[] = {
        0x00, 0x01, 0x02, 0x03,  0x04, 0x05, 0x06, 0x07,
    };
    
    char layoutFile[] = 
        "struct(Cat, 123) Bar\n"
        "{\n"
        "    u(1) v;\n"
        "};\n"
        "\n"
        "struct Foo\n"
        "{\n"
        "    u(1) cat;\n"
        "    Cat[cat] value;\n"
        "};\n"
        "layout Foo;\n";
    
    TestAssert(RunExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, sizeof(binaryFile), binaryFile, BE_RuntimeEventType_Error_CategoryTypeNotFound, 8, 4));
}

Test(Example_RuntimeError_MemberInvalidMemoryRange)
{
    BEu8 binaryFile[] = {
        0x00, 0x01, 0x02, 0x03,  0x04, 0x05, 0x06, 0x07,
    };
    
    char layoutFile[] = 
        "struct Foo\n"
        "{\n"
        "    string(256) v;\n"
        "};\n"
        "layout Foo;\n";
    
    TestAssert(RunExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, sizeof(binaryFile), binaryFile, BE_RuntimeEventType_Error_MemberInvalidMemoryRange, 2, 4));
}

Test(Example_RuntimeError_FetchMemberSizeIsZero)
{
    BEu8 binaryFile[] = {
        0x00, 0x01, 0x02, 0x03,  0x04, 0x05, 0x06, 0x07,
    };
    
    char layoutFile[] = 
        "struct Foo\n"
        "{\n"
        "    u(1) size;\n"
        "    u(size) member;\n"
        "    var i = member;\n"
        "};\n"
        "layout Foo;\n";
    
    TestAssert(RunExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, sizeof(binaryFile), binaryFile, BE_RuntimeEventType_Error_FetchMemberSizeIsZero, 4, 4));
}

Test(Example_RuntimeError_FetchMemberTooBig)
{
    BEu8 binaryFile[] = {
        0x09, 0x01, 0x02, 0x03,  0x04, 0x05, 0x06, 0x07,
        0x08, 0x09, 0x0a, 0x0b,  0x0c, 0x0d, 0x0e, 0x0f,
        0x10, 0x11, 0x12, 0x13,  0x14, 0x15, 0x16, 0x17,
        0x18, 0x19, 0x1a, 0x1b,  0x1c, 0x1d, 0x1e, 0x1f,
    };
    
    char layoutFile[] = 
        "struct Foo\n"
        "{\n"
        "    u(1) size;\n"
        "    u(size) member;\n"
        "    var i = member;\n"
        "};\n"
        "layout Foo;\n";
    
    TestAssert(RunExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, sizeof(binaryFile), binaryFile, BE_RuntimeEventType_Error_FetchMemberTooBig, 4, 4));
}

Test(Example_RuntimeError_FetchMemberBadArrayIndex_TooBig)
{
    BEu8 binaryFile[] = {
        0x09, 0x01, 0x02, 0x03,  0x04, 0x05, 0x06, 0x07,
        0x08, 0x09, 0x0a, 0x0b,  0x0c, 0x0d, 0x0e, 0x0f,
        0x10, 0x11, 0x12, 0x13,  0x14, 0x15, 0x16, 0x17,
        0x18, 0x19, 0x1a, 0x1b,  0x1c, 0x1d, 0x1e, 0x1f,
    };
    
    char layoutFile[] = 
        "struct Foo\n"
        "{\n"
        "    u(1) count;\n"
        "    u(1) members[count];\n"
        "    var i = members[10];\n"
        "};\n"
        "layout Foo;\n";
    
    TestAssert(RunExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, sizeof(binaryFile), binaryFile, BE_RuntimeEventType_Error_FetchMemberBadArrayIndex, 4, 4));
}

Test(Example_RuntimeError_FetchMemberBadArrayIndex_Negative)
{
    BEu8 binaryFile[] = {
        0x09, 0xff, 0x02, 0x03,  0x04, 0x05, 0x06, 0x07,
        0x08, 0x09, 0x0a, 0x0b,  0x0c, 0x0d, 0x0e, 0x0f,
        0x10, 0x11, 0x12, 0x13,  0x14, 0x15, 0x16, 0x17,
        0x18, 0x19, 0x1a, 0x1b,  0x1c, 0x1d, 0x1e, 0x1f,
    };
    
    char layoutFile[] = 
        "struct Foo\n"
        "{\n"
        "    u(1) count;\n"
        "    s(1) index;\n"
        "    assert(index == -1);\n"
        "    u(1) members[count];\n"
        "    var i = members[index];\n"
        "};\n"
        "layout Foo;\n";
    
    TestAssert(RunExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, sizeof(binaryFile), binaryFile, BE_RuntimeEventType_Error_FetchMemberBadArrayIndex, 6, 4));
}

Test(Example_RuntimeError_FetchMemberNoSuchMember)
{
    BEu8 binaryFile[] = {
        0x00, 0x01, 0x02, 0x03,  0x04, 0x05, 0x06, 0x07,
    };
    
    char layoutFile[] = 
        "struct Foo\n"
        "{\n"
        "    u(1) cnd;\n"
        "    if (cnd)\n"
        "    {\n"
        "        u(1) member;\n"
        "    }\n"
        "    var i = member;\n"
        "};\n"
        "layout Foo;\n";
    
    TestAssert(RunExampleWithExpectedError(sizeof(layoutFile) - 1, (BEu8*)layoutFile, sizeof(binaryFile), binaryFile, BE_RuntimeEventType_Error_FetchMemberNoSuchMember, 7, 4));
}


#define Magic4CC(a, b, c, d) ((a) | ((b) << 8) | ((c) << 16) | ((d) << 24))

Test(Example_BMP)
{
    // NOTE(Jens): Contents of examples/test.bmp
    BEu8 binaryFile[] = {
        0x42, 0x4D, 0x32, 0x01,  0x00, 0x00, 0x00, 0x00,  0x00, 0x00, 0x36, 0x00,  0x00, 0x00, 0x28, 0x00,  0x00, 0x00, 0x0B, 0x00,
        0x00, 0x00, 0x07, 0x00,  0x00, 0x00, 0x01, 0x00,  0x18, 0x00, 0x00, 0x00,  0x00, 0x00, 0xFC, 0x00,  0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00,  0x00, 0x00, 0x00, 0x00,  0x00, 0x00, 0x00, 0x00,  0x00, 0x00, 0xFF, 0x00,  0xFF, 0xFF, 0xFF, 0xFF,
        0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,
        0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0x00,  0x00, 0x00, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,
        0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,
        0xFF, 0xFF, 0xFF, 0x00,  0x00, 0x00, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,
        0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0x00,
        0x00, 0x00, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,
        0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0x00,  0x00, 0x00, 0xFF, 0xFF,
        0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,
        0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0x00,  0x00, 0x00, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,
        0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,
        0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0x00,  0x00, 0x00, 0x00, 0x00,  0xFF, 0xFF, 0x00, 0x00,  0xFF, 0xFF, 0xFF, 0xFF,
        0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF,
        0x00, 0xFF, 0xFF, 0x00,  0x00, 0x00,
    };
    
    char layoutFile[] = 
        "// bmp.bet - Public domain - no warranty //\n"
        "\n"
        "enum PixelCompression\n"
        "{\n"
        "    BI_RGB,\n"
        "    BI_RLE8,\n"
        "    BI_RLE4,\n"
        "    BI_BITFIELDS,\n"
        "    BI_JPEG,\n"
        "    BI_PNG,\n"
        "    BI_ALPHABITFIELDS,\n"
        "    BI_CMYK = 11,\n"
        "    BI_CMYKRLE8,\n"
        "    BI_CMYKRLE4,\n"
        "};\n"
        "\n"
        "struct FileHeader\n"
        "{\n"
        "    string(2) Signature;\n"
        "    assert(Signature == \"BM\");\n"
        "    \n"
        "    u(4) FileSize;\n"
        "    hidden(2) Reserved[2];\n"
        "    u(4, hex) FileOffsetToPixelArray;\n"
        "};\n"
        "\n"
        "struct InfoHeader\n"
        "{\n"
        "    u(4) Size;\n"
        "    assert(Size == 40);\n"
        "    \n"
        "    s(4) Width;\n"
        "    s(4) Height;\n"
        "    \n"
        "    u(2) PlaneCount; // NOTE(Jens): Should be 1.\n"
        "    u(2) BitsPerPixel;\n"
        "    PixelCompression(4) Compression;\n"
        "    u(4) ImageSize;\n"
        "    u(4) XPixelsPerMeter;\n"
        "    u(4) YPixelsPerMeter;\n"
        "    u(4) ColorsInColorTable;\n"
        "    u(4) ImportantColorCount;\n"
        "};\n"
        "\n"
        "struct PixelRow(var compression, var bitsPerPixel, var width)\n"
        "{\n"
        "    assert(bitsPerPixel % 8 == 0);\n"
        "    var sizeInBytes = 4 * ((bitsPerPixel * width + 31) / 32);\n"
        "    var paddingInBytes = sizeInBytes - bitsPerPixel * width / 8;\n"
        "    \n"
        "    raw(bitsPerPixel / 8) Pixels[width];\n"
        "    \n"
        "    hidden(paddingInBytes) Padding;\n"
        "};\n"
        "\n"
        "struct Bitmap\n"
        "{\n"
        "    FileHeader Header;\n"
        "    InfoHeader Info;\n"
        "    \n"
        "    @(Header.FileOffsetToPixelArray) PixelRow(Info.Compression, Info.BitsPerPixel, Info.Width) Rows[abs Info.Height];\n"
        "};\n"
        "\n"
        "layout Bitmap;\n";
    
    MemberValue expectedMembers[] = {
        { 0x0000, "FileHeader.Signature",Magic4CC('B', 'M', 0, 0) },
        { 0x0002, "FileHeader.FileSize",                      306 },
        { 0x000A, "FileHeader.FileOffsetToPixelArray",       0x36 },
        { 0x000E, "InfoHeader.Size",                           40 },
        { 0x0012, "InfoHeader.Width",                          11 },
        { 0x0016, "InfoHeader.Height",                          7 },
        { 0x001A, "InfoHeader.PlaneCount",                      1 },
        { 0x001C, "InfoHeader.BitsPerPixel",                   24 },
        { 0x001E, "InfoHeader.Compression",                     0 },
        { 0x0022, "InfoHeader.ImageSize",                     252 },
        { 0x0026, "InfoHeader.XPixelsPerMeter",                 0 },
        { 0x002A, "InfoHeader.YPixelsPerMeter",                 0 },
        { 0x002E, "InfoHeader.ColorsInColorTable",              0 },
        { 0x0032, "InfoHeader.ImportantColorCount",             0 },
        
        // NOTE(Jens): Command line displays these in memory order, we need to flip them as we write literals with most significant digit first.
        { 0x0036, "PixelRow.Pixels[0]",                  0xFF00FF },
        { 0x0039, "PixelRow.Pixels[1]",                  0xFFFFFF },
        { 0x003C, "PixelRow.Pixels[2]",                  0xFFFFFF },
        { 0x003F, "PixelRow.Pixels[3]",                  0xFFFFFF },
        { 0x0042, "PixelRow.Pixels[4]",                  0xFFFFFF },
        { 0x0045, "PixelRow.Pixels[5]",                  0xFFFFFF },
        { 0x0048, "PixelRow.Pixels[6]",                  0xFFFFFF },
        { 0x004B, "PixelRow.Pixels[7]",                  0xFFFFFF },
        { 0x004E, "PixelRow.Pixels[8]",                  0xFFFFFF },
        { 0x0051, "PixelRow.Pixels[9]",                  0xFFFFFF },
        { 0x0054, "PixelRow.Pixels[10]",                 0xFFFFFF },
        { 0x005A, "PixelRow.Pixels[0]",                  0xFFFFFF },
        { 0x005D, "PixelRow.Pixels[1]",                  0xFFFFFF },
        { 0x0060, "PixelRow.Pixels[2]",                  0xFFFFFF },
        { 0x0063, "PixelRow.Pixels[3]",                  0xFFFFFF },
        { 0x0066, "PixelRow.Pixels[4]",                  0xFFFFFF },
        { 0x0069, "PixelRow.Pixels[5]",                  0xFFFFFF },
        { 0x006C, "PixelRow.Pixels[6]",                  0xFFFFFF },
        { 0x006F, "PixelRow.Pixels[7]",                  0xFFFFFF },
        { 0x0072, "PixelRow.Pixels[8]",                  0xFFFFFF },
        { 0x0075, "PixelRow.Pixels[9]",                  0xFFFFFF },
        { 0x0078, "PixelRow.Pixels[10]",                 0xFFFFFF },
        { 0x007E, "PixelRow.Pixels[0]",                  0xFFFFFF },
        { 0x0081, "PixelRow.Pixels[1]",                  0xFFFFFF },
        { 0x0084, "PixelRow.Pixels[2]",                  0xFFFFFF },
        { 0x0087, "PixelRow.Pixels[3]",                  0xFFFFFF },
        { 0x008A, "PixelRow.Pixels[4]",                  0xFFFFFF },
        { 0x008D, "PixelRow.Pixels[5]",                  0xFFFFFF },
        { 0x0090, "PixelRow.Pixels[6]",                  0xFFFFFF },
        { 0x0093, "PixelRow.Pixels[7]",                  0xFFFFFF },
        { 0x0096, "PixelRow.Pixels[8]",                  0xFFFFFF },
        { 0x0099, "PixelRow.Pixels[9]",                  0xFFFFFF },
        { 0x009C, "PixelRow.Pixels[10]",                 0xFFFFFF },
        { 0x00A2, "PixelRow.Pixels[0]",                  0xFFFFFF },
        { 0x00A5, "PixelRow.Pixels[1]",                  0xFFFFFF },
        { 0x00A8, "PixelRow.Pixels[2]",                  0xFFFFFF },
        { 0x00AB, "PixelRow.Pixels[3]",                  0xFFFFFF },
        { 0x00AE, "PixelRow.Pixels[4]",                  0xFFFFFF },
        { 0x00B1, "PixelRow.Pixels[5]",                  0xFFFFFF },
        { 0x00B4, "PixelRow.Pixels[6]",                  0xFFFFFF },
        { 0x00B7, "PixelRow.Pixels[7]",                  0xFFFFFF },
        { 0x00BA, "PixelRow.Pixels[8]",                  0xFFFFFF },
        { 0x00BD, "PixelRow.Pixels[9]",                  0xFFFFFF },
        { 0x00C0, "PixelRow.Pixels[10]",                 0xFFFFFF },
        { 0x00C6, "PixelRow.Pixels[0]",                  0xFFFFFF },
        { 0x00C9, "PixelRow.Pixels[1]",                  0xFFFFFF },
        { 0x00CC, "PixelRow.Pixels[2]",                  0xFFFFFF },
        { 0x00CF, "PixelRow.Pixels[3]",                  0xFFFFFF },
        { 0x00D2, "PixelRow.Pixels[4]",                  0xFFFFFF },
        { 0x00D5, "PixelRow.Pixels[5]",                  0xFFFFFF },
        { 0x00D8, "PixelRow.Pixels[6]",                  0xFFFFFF },
        { 0x00DB, "PixelRow.Pixels[7]",                  0xFFFFFF },
        { 0x00DE, "PixelRow.Pixels[8]",                  0xFFFFFF },
        { 0x00E1, "PixelRow.Pixels[9]",                  0xFFFFFF },
        { 0x00E4, "PixelRow.Pixels[10]",                 0xFFFFFF },
        { 0x00EA, "PixelRow.Pixels[0]",                  0xFFFFFF },
        { 0x00ED, "PixelRow.Pixels[1]",                  0xFFFFFF },
        { 0x00F0, "PixelRow.Pixels[2]",                  0xFFFFFF },
        { 0x00F3, "PixelRow.Pixels[3]",                  0xFFFFFF },
        { 0x00F6, "PixelRow.Pixels[4]",                  0xFFFFFF },
        { 0x00F9, "PixelRow.Pixels[5]",                  0xFFFFFF },
        { 0x00FC, "PixelRow.Pixels[6]",                  0xFFFFFF },
        { 0x00FF, "PixelRow.Pixels[7]",                  0xFFFFFF },
        { 0x0102, "PixelRow.Pixels[8]",                  0xFFFFFF },
        { 0x0105, "PixelRow.Pixels[9]",                  0xFFFFFF },
        { 0x0108, "PixelRow.Pixels[10]",                 0xFFFFFF },
        { 0x010E, "PixelRow.Pixels[0]",                  0xFF0000 },
        { 0x0111, "PixelRow.Pixels[1]",                  0x0000FF },
        { 0x0114, "PixelRow.Pixels[2]",                  0xFFFFFF },
        { 0x0117, "PixelRow.Pixels[3]",                  0xFFFFFF },
        { 0x011A, "PixelRow.Pixels[4]",                  0xFFFFFF },
        { 0x011D, "PixelRow.Pixels[5]",                  0xFFFFFF },
        { 0x0120, "PixelRow.Pixels[6]",                  0xFFFFFF },
        { 0x0123, "PixelRow.Pixels[7]",                  0xFFFFFF },
        { 0x0126, "PixelRow.Pixels[8]",                  0xFFFFFF },
        { 0x0129, "PixelRow.Pixels[9]",                  0xFFFFFF },
        { 0x012C, "PixelRow.Pixels[10]",                 0xFFFF00 },
    };
    
    TestAssert(RunExample(sizeof(layoutFile) - 1, (BEu8*)layoutFile, sizeof(binaryFile), binaryFile, BE_ArrayCount(expectedMembers), expectedMembers, 0));
}

Test(Example_PNG)
{
    // NOTE(Jens): Contents of examples/test.png
    BEu8 binaryFile[] = {
        0x89, 0x50, 0x4E, 0x47,  0x0D, 0x0A, 0x1A, 0x0A,  0x00, 0x00, 0x00, 0x0D,  0x49, 0x48, 0x44, 0x52,  0x00, 0x00, 0x00, 0x0B, 
        0x00, 0x00, 0x00, 0x07,  0x08, 0x06, 0x00, 0x00,  0x00, 0xDE, 0x6E, 0xB7,  0x5D, 0x00, 0x00, 0x00,  0x01, 0x73, 0x52, 0x47, 
        0x42, 0x00, 0xAE, 0xCE,  0x1C, 0xE9, 0x00, 0x00,  0x00, 0x04, 0x67, 0x41,  0x4D, 0x41, 0x00, 0x00,  0xB1, 0x8F, 0x0B, 0xFC, 
        0x61, 0x05, 0x00, 0x00,  0x00, 0x09, 0x70, 0x48,  0x59, 0x73, 0x00, 0x00,  0x0E, 0xC3, 0x00, 0x00,  0x0E, 0xC3, 0x01, 0xC7, 
        0x6F, 0xA8, 0x64, 0x00,  0x00, 0x00, 0x19, 0x74,  0x45, 0x58, 0x74, 0x53,  0x6F, 0x66, 0x74, 0x77,  0x61, 0x72, 0x65, 0x00, 
        0x70, 0x61, 0x69, 0x6E,  0x74, 0x2E, 0x6E, 0x65,  0x74, 0x20, 0x34, 0x2E,  0x30, 0x2E, 0x32, 0x31,  0xF1, 0x20, 0x69, 0x95, 
        0x00, 0x00, 0x00, 0x1B,  0x49, 0x44, 0x41, 0x54,  0x28, 0x53, 0x63, 0xF8,  0xCF, 0x00, 0x46, 0x44,  0x00, 0xA0, 0x3A, 0x28, 
        0x8B, 0x28, 0x30, 0x34,  0x15, 0x13, 0xAD, 0xFC,  0xFF, 0x7F, 0x00, 0xBB,  0x7E, 0x2C, 0xE2, 0xB1,  0x69, 0x0F, 0x47, 0x00, 
        0x00, 0x00, 0x00, 0x49,  0x45, 0x4E, 0x44, 0xAE,  0x42, 0x60, 0x82
    };
    
    char layoutFile[] = 
        "#default(byteorder = big);\n"
        "\n"
        "struct Header\n"
        "{\n"
        "    raw(1) transmissionByte;\n"
        "    string(3) pngMagic;\n"
        "    assert(pngMagic == \"PNG\");\n"
        "    \n"
        "    string(2) lineEnding_DOS;\n"
        "    string(1) eofChar;\n"
        "    string(1) lineEnding_UNIX;\n"
        "};\n"
        "\n"
        "enum sRGB_Intent\n"
        "{\n"
        "    Perceptual,\n"
        "    RelativeColorimetric,\n"
        "    Saturation,\n"
        "    AbsoluteColorimetric,\n"
        "};\n"
        "\n"
        "struct(Chunk, \"sRGB\") sRGB(var size)\n"
        "{\n"
        "    assert(size == 1);\n"
        "    sRGB_Intent(1) intent;\n"
        "};\n"
        "\n"
        "enum IHDR_ColorType\n"
        "{\n"
        "    Grayscale,\n"
        "    RGB = 2,\n"
        "    Palette,\n"
        "    GrayscaleWithAlpha,\n"
        "    RGBWithAlpha = 6\n"
        "};\n"
        "\n"
        "enum IHDR_Filter\n"
        "{\n"
        "    None,\n"
        "    Sub,\n"
        "    Up,\n"
        "    Average,\n"
        "    Paeth,\n"
        "};\n"
        "\n"
        "enum IHDR_Interlace\n"
        "{\n"
        "    None,\n"
        "    Adam7,\n"
        "};\n"
        "\n"
        "struct(Chunk, \"IHDR\") IHDR(var size)\n"
        "{\n"
        "    assert(size == 13);\n"
        "    \n"
        "    u(4) width;\n"
        "    u(4) height;\n"
        "    u(1) bitDepth;\n"
        "    IHDR_ColorType(1) colorType;\n"
        "    u(1) compressionMethod;\n"
        "    assert(compressionMethod == 0);\n"
        "    \n"
        "    IHDR_Filter(1) filterMethod;\n"
        "    IHDR_Interlace(1) interlaceMethod;\n"
        "};\n"
        "\n"
        "struct(Chunk, \"gAMA\") gAMA(var size)\n"
        "{\n"
        "    assert(size == 4);\n"
        "    u(4) factor;\n"
        "};\n"
        "\n"
        "enum pHYs_Unit\n"
        "{\n"
        "    Unknown,\n"
        "    Meters,\n"
        "};\n"
        "\n"
        "struct(Chunk, \"pHYs\") pHYs(var size)\n"
        "{\n"
        "    assert(size == 9);\n"
        "    \n"
        "    u(4) ppuX;\n"
        "    u(4) ppuY;\n"
        "    pHYs_Unit(1) unit;\n"
        "};\n"
        "\n"
        "struct(Chunk, \"tEXt\") tEXt(var size)\n"
        "{\n"
        "    string(size) value;\n"
        "};\n"
        "\n"
        "struct(Chunk, \"IDAT\") IDAT(var size)\n"
        "{\n"
        "    hidden(size) data;\n"
        "    print(\"Pixel data parsing left as reader's exercise.\");\n"
        "};\n"
        "\n"
        "struct Chunk\n"
        "{\n"
        "    u(4) size;\n"
        "    string(4) type;\n"
        "    \n"
        "    if (has_type(Chunk, type))\n"
        "    {\n"
        "        Chunk[type](size) data;\n"
        "    }\n"
        "    else\n"
        "    {\n"
        "        if (size)\n"
        "        {\n"
        "            raw(size) data;\n"
        "        }\n"
        "    }\n"
        "    \n"
        "    u(4) crc;\n"
        "};\n"
        "\n"
        "struct PNG\n"
        "{\n"
        "    Header header;\n"
        "    Chunk ihdrChunk;\n"
        "    assert(ihdrChunk.type == \"IHDR\");\n"
        "    \n"
        "    while (@ < size_of_file)\n"
        "    {\n"
        "        Chunk chunk;\n"
        "    }\n"
        "};\n"
        "\n"
        "layout PNG;\n";
    
    
    MemberValue expectedMembers[] = {
        { 0x00, "Header.transmissionByte", 0x89 },
        { 0x01, "Header.pngMagic", Magic4CC('P', 'N', 'G', 0) },
        { 0x04, "Header.lineEnding_DOS", Magic4CC('\r', '\n', 0, 0) },
        { 0x06, "Header.eofChar", '\x1a' },
        { 0x07, "Header.lineEnding_UNIX", '\n' },
        { 0x08, "Chunk.size", 13 },
        { 0x0C, "Chunk.type", Magic4CC('I', 'H', 'D', 'R') },
        { 0x10, "IHDR.width", 11 },
        { 0x14, "IHDR.height", 7 },
        { 0x18, "IHDR.bitDepth", 8 },
        { 0x19, "IHDR.colorType", 6 }, // NOTE(Jens): IHDR_ColorType.RGBWithAlpha
        { 0x1A, "IHDR.compressionMethod", 0 },
        { 0x1B, "IHDR.filterMethod", 0 },
        { 0x1C, "IHDR.interlaceMethod", 0 },
        { 0x1D, "Chunk.crc", 3731797853 },
        { 0x21, "Chunk.size", 1 },
        { 0x25, "Chunk.type", Magic4CC('s', 'R', 'G', 'B') },
        { 0x29, "sRGB.intent", 0 }, // NOTE(Jens): sRGB_Intent.Perceptual
        { 0x2A, "Chunk.crc", 2932743401 },
        { 0x2E, "Chunk.size", 4 },
        { 0x32, "Chunk.type", Magic4CC('g', 'A', 'M', 'A') },
        { 0x36, "gAMA.factor", 45455 },
        { 0x3A, "Chunk.crc", 201089285 },
        { 0x3E, "Chunk.size", 9 },
        { 0x42, "Chunk.type", Magic4CC('p', 'H', 'Y', 's') },
        { 0x46, "pHYs.ppuX", 3779 },
        { 0x4A, "pHYs.ppuY", 3779 },
        { 0x4E, "pHYs.unit", 1 }, // NOTE(Jens): pHYs_Unit.Meters
        { 0x4F, "Chunk.crc", 3345983588 },
        { 0x53, "Chunk.size", 25 },
        { 0x57, "Chunk.type", Magic4CC('t', 'E', 'X', 't') },
        { 0x5B, "tEXt.value" }, // NOTE(Jens): Expected value "Software\0paint.net 4.0.21". Testing only checks max 64 bit data.
        { 0x74, "Chunk.crc", 4045433237 },
        { 0x78, "Chunk.size", 27 },
        { 0x7C, "Chunk.type", Magic4CC('I', 'D', 'A', 'T') },
        { 0x9B, "Chunk.crc", 2976452423 },
        { 0x9F, "Chunk.size", 0 },
        { 0xA3, "Chunk.type", Magic4CC('I', 'E', 'N', 'D') },
        { 0xA7, "Chunk.crc", 2923585666 },
    };
    
    TestAssert(RunExample(sizeof(layoutFile) - 1, (BEu8*)layoutFile, sizeof(binaryFile), binaryFile, BE_ArrayCount(expectedMembers), expectedMembers, 0));
}

Test(Example_Import)
{
    BEu8 binaryFile[] = {
        0x00, 0x01, 0x02, 0x03,  0x04, 0x05, 0x06, 0x07,
    };
    
    char layoutFile[] = 
        "import(\"imported.bet\");\n"
        "\n"
        "struct Type\n"
        "{\n"
        "    ImportedStruct value;\n"
        "};\n"
        "\n"
        "layout Type;\n";
    
    char libraryFile_imported[] = 
        "import(\"types.bet\");\n"
        "struct ImportedStruct\n"
        "{\n"
        "    char chr;\n"
        "    u32 data;\n"
        "};\n"
        "\n"
        "layout ImportedStruct;\n";
    
    char libraryFile_types[] = 
        "typedef string(1) char;\n"
        "typedef alignas(2) u(2) u16;\n"
        "typedef alignas(4) u(4) u32;\n"
        "typedef alignas(8) u(8) u64;\n"
        "typedef alignas(2) s(2) i16;\n"
        "typedef alignas(4) s(4) i32;\n"
        "typedef alignas(8) s(8) i64;\n";
    
    MemberValue expectedMembers[] = {
        { 0x00, "ImportedStruct.chr", 0x00 },
        { 0x04, "ImportedStruct.data", 0x07060504 },
    };
    
    BE_String libraryNames[2];
    BEu32 librarySizes[2];
    BEu8* libraryFiles[2];
    
    libraryNames[0] = BE_StrLiteral("imported.bet");
    librarySizes[0] = sizeof(libraryFile_imported) - 1;
    libraryFiles[0] = (BEu8*)libraryFile_imported;
    
    libraryNames[1] = BE_StrLiteral("types.bet");
    librarySizes[1] = sizeof(libraryFile_types) - 1;
    libraryFiles[1] = (BEu8*)libraryFile_types;
    
    LibraryFiles libraries;
    libraries.count = 2;
    libraries.names = libraryNames;
    libraries.sizes = librarySizes;
    libraries.files = libraryFiles;
    
    TestAssert(RunExample(sizeof(layoutFile) - 1, (BEu8*)layoutFile, sizeof(binaryFile), binaryFile, BE_ArrayCount(expectedMembers), expectedMembers, &libraries));
}

Test(Example_ZeroCountStruct)
{
    BEu8 binaryFile[] = {
        0x00, 0x01, 0x02, 0x03,  0x04, 0x05, 0x06, 0x07,
    };
    
    char layoutFile[] = 
        "\n"
        "struct AnotherType\n"
        "{\n"
        "    u(4) value;\n"
        "};\n"
        "\n"
        "struct Type\n"
        "{\n"
        "    u(1) count;\n"
        "    AnotherType value[count];\n"
        "    u(1) data;\n"
        "};\n"
        "\n"
        "layout Type;\n";
    
    MemberValue expectedMembers[] = {
        { 0x00, "Type.count", 0x00 },
        { 0x01, "Type.data", 0x01 },
    };
    
    TestAssert(RunExample(sizeof(layoutFile) - 1, (BEu8*)layoutFile, sizeof(binaryFile), binaryFile, BE_ArrayCount(expectedMembers), expectedMembers, 0));
}

#define AddTest(name) { #name, name }

static TestData Tests[] = {
    AddTest(Tokenizer_NumberLiterals),
    AddTest(Tokenizer_FloatLiterals),
    AddTest(Tokenizer_Comments),
    AddTest(Tokenizer_Identifier),
    AddTest(Tokenizer_StringLiterals),
    AddTest(Tokenizer_Punctuations),
    AddTest(Tokenizer_Invalid),
    
    AddTest(Parser_Print_Msg),
    AddTest(Parser_Print_Expr),
    AddTest(Parser_Print_Msg_Expr),
    AddTest(Parser_Assert),
    AddTest(Parser_VariableDeclaration),
    AddTest(Parser_VariableDeclaration_Initializer),
    AddTest(Parser_Typedef),
    AddTest(Parser_Layout),
    AddTest(Parser_EnumDeclaration),
    AddTest(Parser_EnumDeclaration_Sized),
    AddTest(Parser_EnumDeclaration_TrailingComma),
    AddTest(Parser_EnumDeclaration_Anonymous),
    AddTest(Parser_EnumDeclaration_Nameless),
    AddTest(Parser_Import),
    AddTest(Parser_DefaultDirective_LittleEndian),
    AddTest(Parser_DefaultDirective_BigEndian),
    AddTest(Parser_Struct),
    AddTest(Parser_Struct_Category),
    AddTest(Parser_Struct_Params),
    AddTest(Parser_Struct_Category_Params),
    AddTest(Parser_MemberOffset_Assignment),
    AddTest(Parser_Variable_Assignment),
    AddTest(Parser_Variable_Assignment_BitshiftLeft),
    AddTest(Parser_Variable_Assignment_BitshiftRight),
    AddTest(Parser_Variable_Assignment_Xor),
    AddTest(Parser_Variable_Assignment_Or),
    AddTest(Parser_Variable_Assignment_And),
    AddTest(Parser_Variable_Assignment_Mod),
    AddTest(Parser_Variable_Assignment_Div),
    AddTest(Parser_Variable_Assignment_Mul),
    AddTest(Parser_Variable_Assignment_Sub),
    AddTest(Parser_Variable_Assignment_Add),
    AddTest(Parser_Variable_Assignment_ArrayCount),
    AddTest(Parser_Variable_Assignment_CreatePlot),
    AddTest(Parser_Plot_X),
    AddTest(Parser_Plot_Y),
    AddTest(Parser_Plot_XY),
    AddTest(Parser_PlotMember_X),
    AddTest(Parser_PlotMember_Y),
    AddTest(Parser_If),
    AddTest(Parser_ElseIf),
    AddTest(Parser_Else),
    AddTest(Parser_For),
    AddTest(Parser_For_NoVar),
    AddTest(Parser_For_NoCondition),
    AddTest(Parser_For_NoAssignment),
    AddTest(Parser_For_NoVar_NoCondition),
    AddTest(Parser_For_NoVar_NoAssignment),
    AddTest(Parser_For_NoAssignment_NoCondition),
    AddTest(Parser_For_NoVar_NoAssignment_NoCondition),
    AddTest(Parser_While),
    AddTest(Parser_Do),
    AddTest(Parser_EndOf_Struct),
    AddTest(Parser_EndOf_For),
    AddTest(Parser_EndOf_While),
    AddTest(Parser_EndOf_DoWhile),
    AddTest(Parser_EndOf_If),
    AddTest(Parser_EndOf_Else),
    AddTest(Parser_EnumRead_Unnamed),
    AddTest(Parser_EnumRead_Anonymous),
    AddTest(Parser_EnumRead_Qualified),
    AddTest(Parser_size_of_file),
    AddTest(Parser_has_type),
    AddTest(Parser_sizeof),
    AddTest(Parser_sizeof_AddressExpression),
    AddTest(Parser_load_Signed),
    AddTest(Parser_load_Unsigned),
    AddTest(Parser_load_AddressExpression),
    AddTest(Parser_load_Nested),
    AddTest(Parser_ParameterAccess),
    AddTest(Parser_MemberAccess),
    AddTest(Parser_MemberAccess_Array),
    AddTest(Parser_MemberAccess_NestedStruct),
    AddTest(Parser_MemberAccess_NestedStruct_Array_Scalar),
    AddTest(Parser_MemberAccess_NestedStruct_Array_Array),
    AddTest(Parser_Member_Scalar_u),
    AddTest(Parser_Member_Scalar_big_u),
    AddTest(Parser_Member_Scalar_default_big_u),
    AddTest(Parser_Member_Scalar_s),
    AddTest(Parser_Member_Scalar_f),
    AddTest(Parser_Member_Scalar_raw),
    AddTest(Parser_Member_Scalar_enum),
    AddTest(Parser_Member_Scalar_enum_NoSize),
    AddTest(Parser_Member_Scalar_enum_OverwriteSize),
    AddTest(Parser_Member_Scalar_string),
    AddTest(Parser_Member_Scalar_hidden),
    AddTest(Parser_Member_Struct),
    AddTest(Parser_Member_Struct_Internal),
    AddTest(Parser_Member_hidden),
    AddTest(Parser_Member_Struct_Arguments),
    AddTest(Parser_Member_Category),
    AddTest(Parser_Member_CategoryOrStruct_Struct),
    AddTest(Parser_Member_CategoryOrStruct_Category),
    AddTest(Parser_Member_Category_Arguments),
    AddTest(Parser_Member_Typedef),
    AddTest(Parser_Member_Array),
    AddTest(Parser_Member_Address),
    AddTest(Parser_Member_ExternalAddress),
    
    AddTest(InstructionGenerator_Print_Msg),
    AddTest(InstructionGenerator_Print_Expr),
    AddTest(InstructionGenerator_Assert),
    AddTest(InstructionGenerator_VariableDeclaration),
    AddTest(InstructionGenerator_VariableDeclaration_Initializer),
    AddTest(InstructionGenerator_Typedef),
    AddTest(InstructionGenerator_Enum),
    AddTest(InstructionGenerator_Layout),
    AddTest(InstructionGenerator_Import),
    AddTest(InstructionGenerator_Default),
    AddTest(InstructionGenerator_StructHeader),
    AddTest(InstructionGenerator_EmptyStruct),
    AddTest(InstructionGenerator_EmptyStructWithParameters),
    AddTest(InstructionGenerator_Variable_Assignment),
    AddTest(InstructionGenerator_Variable_CompoundAssignment),
    AddTest(InstructionGenerator_If),
    AddTest(InstructionGenerator_If_Else),
    AddTest(InstructionGenerator_If_ElseIf),
    AddTest(InstructionGenerator_If_ElseIf_Else),
    AddTest(InstructionGenerator_For),
    AddTest(InstructionGenerator_While),
    AddTest(InstructionGenerator_DoWhile),
    AddTest(InstructionGenerator_MemberDeclaration_LittleScalar),
    AddTest(InstructionGenerator_MemberDeclaration_BigScalar),
    AddTest(InstructionGenerator_MemberDeclaration_Scalar_Array),
    AddTest(InstructionGenerator_MemberDeclaration_Scalar_Aligned),
    AddTest(InstructionGenerator_MemberDeclaration_Typedef_Scalar),
    AddTest(InstructionGenerator_MemberDeclaration_Struct),
    AddTest(InstructionGenerator_MemberDeclaration_Struct_Internal),
    AddTest(InstructionGenerator_MemberDeclaration_Struct_Array),
    AddTest(InstructionGenerator_MemberDeclaration_Category),
    AddTest(InstructionGenerator_MemberDeclaration_Enum),
    AddTest(InstructionGenerator_Expression_MemberOffset),
    AddTest(InstructionGenerator_Expression_LocalVariable),
    AddTest(InstructionGenerator_Expression_SizeOfFile),
    AddTest(InstructionGenerator_Expression_Parameter),
    AddTest(InstructionGenerator_Expression_Load),
    AddTest(InstructionGenerator_Expression_Load_Address),
    AddTest(InstructionGenerator_Expression_HasType),
    AddTest(InstructionGenerator_Expression_SizeOf),
    AddTest(InstructionGenerator_Expression_SizeOf_Address),
    AddTest(InstructionGenerator_Expression_Member),
    AddTest(InstructionGenerator_Expression_ArrayCount),
    AddTest(InstructionGenerator_ConstantExpression_DivZero),
    AddTest(InstructionGenerator_ConstantExpression_ModZero),
    AddTest(InstructionGenerator_Plot_X),
    AddTest(InstructionGenerator_Plot_Y),
    AddTest(InstructionGenerator_Plot_XY),
    AddTest(InstructionGenerator_PlotMember_X),
    AddTest(InstructionGenerator_PlotMember_Y),
    AddTest(InstructionGenerator_CreatePlot),
    
    AddTest(Serializer_Set),
    AddTest(Serializer_NegativeValues),
    AddTest(Serializer_Unary),
    AddTest(Serializer_Binary),
    AddTest(Serializer_PushFrame),
    AddTest(Serializer_PushStruct),
    AddTest(Serializer_PopFrame),
    AddTest(Serializer_PopStruct),
    AddTest(Serializer_HideMembers),
    AddTest(Serializer_ShowMembers),
    AddTest(Serializer_Breakpoint),
    AddTest(Serializer_ExportScalar),
    AddTest(Serializer_FetchMemberValue),
    AddTest(Serializer_FetchArrayCount),
    AddTest(Serializer_Jump),
    AddTest(Serializer_JumpDynamic),
    AddTest(Serializer_JumpDynamicConditional),
    AddTest(Serializer_JumpConditional),
    AddTest(Serializer_FetchSizeOfFile),
    AddTest(Serializer_FetchStartOfType),
    AddTest(Serializer_HasType),
    AddTest(Serializer_Print),
    AddTest(Serializer_Assert),
    AddTest(Serializer_Plot_X),
    AddTest(Serializer_Plot_Y),
    AddTest(Serializer_CreatePlot),
    AddTest(Serializer_IntToFloat),
    
    AddTest(Runtime_Set),
    AddTest(Runtime_Unary_Negate),
    AddTest(Runtime_Unary_LogicalNot),
    AddTest(Runtime_Unary_BitwiseNot),
    AddTest(Runtime_Unary_Abs),
    AddTest(Runtime_Unary_Copy),
    AddTest(Runtime_Unary_FNegate),
    AddTest(Runtime_Unary_FAbs),
    AddTest(Runtime_Binary_Add),
    AddTest(Runtime_Binary_Sub),
    AddTest(Runtime_Binary_Mul),
    AddTest(Runtime_Binary_Div),
    AddTest(Runtime_Binary_Div_Zero),
    AddTest(Runtime_Binary_Mod),
    AddTest(Runtime_Binary_Mod_Zero),
    AddTest(Runtime_Binary_Less),
    AddTest(Runtime_Binary_LessOrEqual),
    AddTest(Runtime_Binary_Equal),
    AddTest(Runtime_Binary_Greater),
    AddTest(Runtime_Binary_GreaterOrEqual),
    AddTest(Runtime_Binary_NotEqual),
    AddTest(Runtime_Binary_BitshiftLeft),
    AddTest(Runtime_Binary_BitshiftRight),
    AddTest(Runtime_Binary_BitwiseAnd),
    AddTest(Runtime_Binary_BitwiseOr),
    AddTest(Runtime_Binary_BitwiseXOr),
    AddTest(Runtime_Binary_LogicalAnd),
    AddTest(Runtime_Binary_LogicalOr),
    AddTest(Runtime_Binary_FAdd),
    AddTest(Runtime_Binary_FSub),
    AddTest(Runtime_Binary_FMul),
    AddTest(Runtime_Binary_FDiv),
    AddTest(Runtime_Binary_FDiv_Zero),
    AddTest(Runtime_Binary_FLess),
    AddTest(Runtime_Binary_FLessOrEqual),
    AddTest(Runtime_Binary_FEqual),
    AddTest(Runtime_Binary_FGreater),
    AddTest(Runtime_Binary_FGreaterOrEqual),
    AddTest(Runtime_Binary_FNotEqual),
    AddTest(Runtime_FramePushPop),
    AddTest(Runtime_ExportScalar_u),
    AddTest(Runtime_ExportScalar_s),
    AddTest(Runtime_ExportScalar_f),
    AddTest(Runtime_ExportScalar_string),
    AddTest(Runtime_ExportScalar_raw),
    AddTest(Runtime_ExportScalar_BadAddress),
    AddTest(Runtime_FetchMemberValue),
    AddTest(Runtime_FetchMemberValue_ScalarArray),
    AddTest(Runtime_FetchMemberValue_ScalarArray_BadIndex),
    AddTest(Runtime_FetchMemberValue_TooBig),
    AddTest(Runtime_FetchMemberValue_ZeroSize),
    AddTest(Runtime_FetchArrayCount),
    AddTest(Runtime_FetchMemory),
    AddTest(Runtime_FetchMemory_Signed),
    AddTest(Runtime_FetchMemory_Big),
    AddTest(Runtime_FetchMemory_BadAddress),
    AddTest(Runtime_FetchMemory_TooBig),
    AddTest(Runtime_Assert_Success),
    AddTest(Runtime_Assert_Fail),
    AddTest(Runtime_HasType_Existing),
    AddTest(Runtime_HasType_NonExisting),
    AddTest(Runtime_FetchStartOfType_Existing),
    AddTest(Runtime_FetchStartOfType_NonExisting),
    AddTest(Runtime_Plot_X),
    AddTest(Runtime_Plot_Y),
    AddTest(Runtime_CreatePlot),
    AddTest(Runtime_IntToFloat),
    
    AddTest(Example_CompileError_InvalidToken),
    AddTest(Example_CompileError_FileLoadFailed),
    AddTest(Example_CompileError_ConstantExpression_DivZero),
    AddTest(Example_CompileError_ConstantExpression_ModZero),
    AddTest(Example_CompileError_InvalidStatement),
    AddTest(Example_CompileError_InvalidStatement_EndOfStatement_ExpectedSemicolon),
    AddTest(Example_CompileError_InvalidStatement_Breakpoint_MissingOpenParenthesis),
    AddTest(Example_CompileError_InvalidStatement_Breakpoint_MissingCloseParenthesis),
    AddTest(Example_CompileError_InvalidStatement_Print_MissingOpenParenthesis),
    AddTest(Example_CompileError_InvalidStatement_Print_MissingCloseParenthesis),
    AddTest(Example_CompileError_InvalidStatement_Print_MissingCommaOrCloseParenthesis),
    AddTest(Example_CompileError_InvalidStatement_Assert_MissingOpenParenthesis),
    AddTest(Example_CompileError_InvalidStatement_Assert_MissingCloseParenthesis),
    AddTest(Example_CompileError_InvalidStatement_VariableDeclaration_MissingVarName),
    AddTest(Example_CompileError_InvalidStatement_TypedefDeclaration_MissingName),
    AddTest(Example_CompileError_InvalidStatement_Layout_MissingType),
    AddTest(Example_CompileError_InvalidStatement_Enum_InvalidSize),
    AddTest(Example_CompileError_InvalidStatement_Enum_MissingCloseParenthesis),
    AddTest(Example_CompileError_InvalidStatement_Enum_MissingOpenCurlyBracket),
    AddTest(Example_CompileError_InvalidStatement_Enum_MissingCloseCurlyBracket_TrailingComma),
    AddTest(Example_CompileError_InvalidStatement_Enum_MissingCloseCurlyBracket),
    AddTest(Example_CompileError_InvalidStatement_Enum_MissingCloseCurlyBracket_TrailingExpression),
    AddTest(Example_CompileError_InvalidStatement_Import_MissingOpenParenthesis),
    AddTest(Example_CompileError_InvalidStatement_Import_MissingCloseParenthesis),
    AddTest(Example_CompileError_InvalidStatement_StructHeader_MissingName),
    AddTest(Example_CompileError_InvalidStatement_StructHeader_MissingCategoryName),
    AddTest(Example_CompileError_InvalidStatement_StructHeader_MissingCategoryComma),
    AddTest(Example_CompileError_InvalidStatement_StructHeader_MissingCategoryCloseParenthesis),
    AddTest(Example_CompileError_InvalidStatement_StructHeader_CategoryDivZero),
    AddTest(Example_CompileError_InvalidStatement_StructHeader_CategoryModZero),
    AddTest(Example_CompileError_InvalidStatement_StructHeader_MissingNameAfterCategory),
    AddTest(Example_CompileError_InvalidStatement_StructHeader_MissingParamVar),
    AddTest(Example_CompileError_InvalidStatement_StructHeader_MissingParamName),
    AddTest(Example_CompileError_InvalidStatement_StructHeader_DuplicateParamName),
    AddTest(Example_CompileError_InvalidStatement_StructHeader_MissingEndOfParameters),
    AddTest(Example_CompileError_InvalidStatement_StructHeader_MissingOpenCurlyBracket),
    AddTest(Example_CompileError_InvalidStatement_StructHeader_DuplicateStructName),
    AddTest(Example_CompileError_InvalidStatement_EndOfStruct_MissingSemicolon),
    AddTest(Example_CompileError_InvalidStatement_EndOfDoWhile_MissingWhile),
    AddTest(Example_CompileError_InvalidStatement_EndOfDoWhile_MissingOpenParenthesis),
    AddTest(Example_CompileError_InvalidStatement_EndOfDoWhile_MissingCloseParenthesis),
    AddTest(Example_CompileError_InvalidStatement_ConstantIntegerExpression_InvalidLookup),
    AddTest(Example_CompileError_InvalidStatement_ConstantIntegerExpression_InvalidSyntax),
    AddTest(Example_CompileError_InvalidStatement_ConstantIntegerExpression_InvalidNestedLookup),
    AddTest(Example_CompileError_InvalidStatement_ConstantIntegerExpression_InvalidExpression),
    AddTest(Example_CompileError_InvalidStatement_Load_MissingOpenParenthesis),
    AddTest(Example_CompileError_InvalidStatement_Load_MissingAddressOpenParenthesis),
    AddTest(Example_CompileError_InvalidStatement_Load_MissingAddressCloseParenthesis),
    AddTest(Example_CompileError_InvalidStatement_Load_InvalidScalar),
    AddTest(Example_CompileError_InvalidStatement_Load_MissingSizeOpenParenthesis),
    AddTest(Example_CompileError_InvalidStatement_Load_MissingSizeCloseParenthesis),
    AddTest(Example_CompileError_InvalidStatement_Load_MissingCloseParenthesis),
    AddTest(Example_CompileError_InvalidStatement_HasType_MissingOpenParenthesis),
    AddTest(Example_CompileError_InvalidStatement_HasType_BadCategoryName),
    AddTest(Example_CompileError_InvalidStatement_HasType_MissingComma),
    AddTest(Example_CompileError_InvalidStatement_HasType_MissingCloseParenthesis),
    AddTest(Example_CompileError_InvalidStatement_SizeOf_MissingOpenParenthesis),
    AddTest(Example_CompileError_InvalidStatement_SizeOf_InvalidStructName),
    AddTest(Example_CompileError_InvalidStatement_SizeOf_InvalidIntegerExpression),
    AddTest(Example_CompileError_InvalidStatement_SizeOf_MissingCloseParenthesis),
    AddTest(Example_CompileError_InvalidStatement_NonExistingNestedMember),
    AddTest(Example_CompileError_InvalidStatement_BadNestedMemberSyntax),
    AddTest(Example_CompileError_InvalidStatement_BadNestedArraMemberSyntax),
    AddTest(Example_CompileError_InvalidStatement_NestedArraMemberSyntax_MissingCloseSquareBracket),
    AddTest(Example_CompileError_InvalidStatement_AddressSpecification_MissingOpenParenthesis),
    AddTest(Example_CompileError_InvalidStatement_AddressSpecification_MissingCloseParenthesis),
    AddTest(Example_CompileError_InvalidStatement_Preprocessor_Unknown),
    AddTest(Example_CompileError_InvalidStatement_Preprocessor_Default_MissingOpenParenthesis),
    AddTest(Example_CompileError_InvalidStatement_Preprocessor_Default_Unknown),
    AddTest(Example_CompileError_InvalidStatement_Preprocessor_Default_ByteOrder_MissingEquals),
    AddTest(Example_CompileError_InvalidStatement_Preprocessor_Default_ByteOrder_UnknownValue),
    AddTest(Example_CompileError_InvalidStatement_Preprocessor_Default_ByteOrder_MissingEndParenthesis),
    AddTest(Example_CompileError_InvalidStatement_Preprocessor_Default_ByteOrder_MissingSemicolon),
    AddTest(Example_CompileError_InvalidStatement_Assignment_BadOperator),
    AddTest(Example_CompileError_InvalidStatement_DuplicateEnumMemberNames_Local),
    AddTest(Example_CompileError_InvalidStatement_DuplicateEnumMemberNames_Global),
    AddTest(Example_CompileError_InvalidStatement_ExpectedSemicolon_ForVar),
    AddTest(Example_CompileError_InvalidStatement_ExpectedSemicolon_ForCondition),
    AddTest(Example_CompileError_InvalidStatement_ExpectedIdentifier_MemberName),
    AddTest(Example_CompileError_InvalidStatement_ExpectedIdentifier_ForVar),
    AddTest(Example_CompileError_InvalidStatement_ExpectedVariable_ForAssignment),
    AddTest(Example_CompileError_InvalidStatement_ExpectedAssignmentOperator_ForAssignment),
    AddTest(Example_CompileError_InvalidStatement_DuplicateVariableName),
    
    AddTest(Example_RuntimeError_AssertTriggered),
    AddTest(Example_RuntimeError_DivByZero),
    AddTest(Example_RuntimeError_ModByZero),
    AddTest(Example_RuntimeError_FetchMemoryInvalidMemoryRange),
    AddTest(Example_RuntimeError_FetchMemoryTooBig),
    AddTest(Example_RuntimeError_CategoryTypeNotFound),
    AddTest(Example_RuntimeError_MemberInvalidMemoryRange),
    AddTest(Example_RuntimeError_FetchMemberSizeIsZero),
    AddTest(Example_RuntimeError_FetchMemberTooBig),
    AddTest(Example_RuntimeError_FetchMemberBadArrayIndex_TooBig),
    AddTest(Example_RuntimeError_FetchMemberBadArrayIndex_Negative),
    AddTest(Example_RuntimeError_FetchMemberNoSuchMember),
    
    AddTest(Example_BMP),
    AddTest(Example_PNG),
    AddTest(Example_Import),
    AddTest(Example_ZeroCountStruct),
};

#include <stdio.h>

int main()
{
    fprintf(stdout, "Running tests...\n");
    
    int failedTestCount = 0;
    
    for (unsigned int testIndex = 0; testIndex < BE_ArrayCount(Tests); ++testIndex)
    {
        TestData* testData = Tests + testIndex;
        
        TestResult testResult = { 0 };
        testData->func(&testResult);
        
        if (testResult.failedCount)
        {
            fprintf(stdout, "***FAILURE*** ");
            ++failedTestCount;
        }
        else
        {
            fprintf(stdout, "[SUCCESS] ");
        }
        
        fprintf(stdout, " %3u / %u == %s\n", testIndex + 1, (unsigned int)BE_ArrayCount(Tests), testData->name);
    }
    
    if (failedTestCount)
    {
        if (failedTestCount == 1)
        {
            fprintf(stdout, "  1 test failed.\n");
        }
        else
        {
            fprintf(stdout, "  %u tests failed.\n", failedTestCount);
        }
    }
    
    return failedTestCount;
}
