
enum WaveFormatID
{
    PCM = 1,
    IEEE_FLOAT = 3,
    ALAW = 6,
    MULAW = 7,
    EXTENSIBLE = 0xFFFE,
};

struct RiffHeader
{
	string(4) chunkId;
	u(4) chunkSize;
	string(4) format;
};

struct ChunkHeader
{
	string(4) chunkId;
	u(4) chunkSize;
};

struct OptionalJunkChunk
{
    var chunkId = load(u(4));
    if (chunkId == "JUNK")
    {
        string(4) chunkId;
        u(4) chunkSize;
        
        @ +=  chunkSize;
    }
};

struct FormatChunk
{
    string(4) chunkId;
    u(4) chunkSize;
    assert(chunkId == "fmt ");
    
	WaveFormatID(2) audioFormat;
	u(2) channelCount;
	u(4) sampleRate;
	u(4) byteRate;
	u(2) blockAlign;
	u(2) bitsPerSample;
    
    if (audioFormat != WaveFormatID.PCM && chunkSize != 16)
    {
        u(2) extraParamSize;
        raw(extraParamSize) extra;
    }
    else
    {
        assert(chunkSize == 16);
    }
};

struct FactChunk
{
    string(4) chunkId;
    assert(chunkId == "fact");
	u(4) chunkSize;
    u(4) sampleLength;
};

struct Sample_PCM(var bitsPerSample, var channelCount)
{
    assert(bitsPerSample % 8 == 0);
    s(bitsPerSample / 8) e[channelCount];
};

struct Sample_IEEE_FLOAT(var channelCount)
{
    f(4) e[channelCount];
};

struct DataChunk(var blockAlign, var bitsPerSample, var channelCount, var format)
{
    string(4) chunkId;
    assert(chunkId == "data");
	u(4) chunkSize;
    
    assert(!(chunkSize % blockAlign));
    var blockCount = chunkSize / blockAlign;
    
    if (format == WaveFormatID.PCM)
    {
        hidden Sample_PCM(bitsPerSample, channelCount) samples_pcm[blockCount];
    }
    else if (format == WaveFormatID.IEEE_FLOAT)
    {
        assert(bitsPerSample == 32);
        hidden Sample_IEEE_FLOAT(channelCount) samples_ieee[blockCount];
    }
    else
    {
        internal hidden(blockAlign) data[blockCount];
    }
};

struct WaveFile
{
    RiffHeader header;
    
    OptionalJunkChunk junk;
    FormatChunk fmt;
    OptionalJunkChunk junk;
    
    if (fmt.audioFormat != WaveFormatID.PCM && load(u(4)) == "fmt ")
    {
        FactChunk fact;
        OptionalJunkChunk junk;
    }
    
    DataChunk(fmt.blockAlign, fmt.bitsPerSample, fmt.channelCount, fmt.audioFormat) data;
    
    var plot = create_plot();
    
    print("Sample count", data.chunkSize / fmt.blockAlign);
    
    if (fmt.audioFormat == WaveFormatID.IEEE_FLOAT || fmt.audioFormat == WaveFormatID.PCM)
    {
        for (var channel = 0; channel < fmt.channelCount; channel += 1)
        {
            for (var i = 0; i < data.chunkSize / fmt.blockAlign; i += 1)
            {
                var id = fmt.channelCount * plot + channel;
                plot_x(id, i / fmt.sampleRate);
            }
        }
    }
    
    if (fmt.audioFormat == WaveFormatID.IEEE_FLOAT)
    {
        for (var channel = 0; channel < fmt.channelCount; channel += 1)
        {
            for (var i = 0; i < data.chunkSize / fmt.blockAlign; i += 1)
            {
                var id = fmt.channelCount * plot + channel;
                plot_y(id, data.samples_ieee[i].e[channel]);
            }
        }
    }
    else if (fmt.audioFormat == WaveFormatID.PCM)
    {
        for (var channel = 0; channel < fmt.channelCount; channel += 1)
        {
            for (var i = 0; i < data.chunkSize / fmt.blockAlign; i += 1)
            {
                var id = fmt.channelCount * plot + channel;
                plot_y(id, data.samples_pcm[i].e[channel]);
            }
        }
    }
};

layout WaveFile;
