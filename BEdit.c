// BEdit.c - Public domain - no warranty //

#include "BEdit.h"

#include <stdarg.h> // For va_start va_end.
#include <stdio.h>  // For vsnprintf and similar.

#ifndef BE_ClearStructToZero
#  include <string.h> // For memset and memcmp.
#  define BE_ClearStructToZero(v) memset(&(v), 0, sizeof(v))
#endif

#ifndef BE_ArrayCount
#  define BE_ArrayCount(arr) (sizeof(arr) / sizeof(*(arr)))
#endif

#ifndef BEPrivateAPI
#  define BEPrivateAPI static
#endif

#ifdef __cplusplus
#  define BE_Punctuation(str) BE_punct{str}
#  define BE_StrLiteral(str) BE_String{ sizeof(str) - 1, (char*)str }
#else
#  define BE_Punctuation(str) (BE_punct){str}
#  define BE_StrLiteral(str) (BE_String){ sizeof(str) - 1, (char*)str }
#endif

#define BE_Punctuation_Invalid BE_Punctuation("")

BEPrivateAPI BEbool BE_IsWhitespace(char c)
{
	BEbool rVal = 0;
	switch (c)
	{
		case ' ':
		case '\t':
		case '\r':
		case '\n':
		case '\v':
		case '\f':
		{
			rVal = 1;
		} break;
	}
	return rVal;
}

BEPrivateAPI BEbool BE_IsDigit(char c)
{
	return '0' <= c && c <= '9';
}

BEPrivateAPI BEbool BE_IsAlpha(char c)
{
	return ('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z');
}

BEPrivateAPI BEbool BE_IsOneOf(char c, const char* str)
{
	BEbool rVal = 0;
	while (*str)
	{
		if (c == *str++)
		{
			rVal = 1;
			break;
		}
	}
	return rVal;
}


//
// NOTE(Jens): Tokenizer
//

typedef enum BE_TokenType BE_TokenType;
enum BE_TokenType
{
	BE_TokenType_Invalid,
	BE_TokenType_Identifier,    // Letter (a-zA-Z) or _ followed by letters, digits and _
	BE_TokenType_QuotedString,  // Quoted string with either ' or "
	BE_TokenType_Punctuation,   // Any of || && != == << >> <= >= = -- ++ -= += *= /= <<= >>= ^= |= &= %= ~ | & ! @ % ? , . { } ( ) [ ] + / * - < > ^ ; $ :
	BE_TokenType_NumberLiteral,
    BE_TokenType_FloatLiteral,
    BE_TokenType_Whitespace,
    BE_TokenType_LineComment,
    BE_TokenType_MultilineComment,
    
	BE_TokenType_Count,
    BE_TokenType_EndOfTokens   // End of token stream.
};

enum
{
    BE_TokenizerMode_None,
    
    BE_TokenizerMode_0,
    BE_TokenizerMode_0b,
    BE_TokenizerMode_0x,
    BE_TokenizerMode_BinaryDigits,
    BE_TokenizerMode_OctalDigits,
    BE_TokenizerMode_HexDigits,
    BE_TokenizerMode_DecimalDigits,
    
    BE_TokenizerMode_FloatFDigits,
    
    BE_TokenizerMode_ForwardSlash,
    BE_TokenizerMode_LineComment,
    BE_TokenizerMode_MultilineComment,
    
    BE_TokenizerMode_Identifier,
    BE_TokenizerMode_QuotedString,
    BE_TokenizerMode_Whitespace,
    
    BE_TokenizerMode_Star,
    BE_TokenizerMode_Minus,
    BE_TokenizerMode_Plus,
    BE_TokenizerMode_Less,
    BE_TokenizerMode_Greater,
    BE_TokenizerMode_Hat,
    BE_TokenizerMode_Ampersand,
    BE_TokenizerMode_VerticalLine,
    BE_TokenizerMode_Exclamation,
    BE_TokenizerMode_Equals,
    BE_TokenizerMode_Percentage,
    
    BE_TokenizerMode_LessLess,
    BE_TokenizerMode_GreaterGreater,
    
    BE_TokenizerMode_Dot,
    
    BE_TokenizerMode_EndOfTokenStart,
    // NOTE(Jens): Any value greater than this ends the token at next input with type '(BETokenType)(mode - BE_TokenizerMode_EndOfTokenStart)'
};
typedef BEu32 BE_TokenizerMode;

typedef struct
{
    BE_TokenizerMode mode;
    char prevC;
    
    union
    {
        struct
        {
            BEu32 nestingCount;
        } multilineComment;
        
        struct
        {
            BEu64 value; // NOTE(Jens): Negative numbers are '-' punctuation token followed by number literal.
        } numberLiteral;
        
        struct 
        {
            BEu64 value;
            BEu64 decimalDigits;
        } floatLiteral;
        
        struct
        {
            BEbool hasPendingBackslash;
            char quoteSign;
        } quotedString;
        
        BE_punct punctuation;
    } evaluation;
    
} BE_TokenizerState;

typedef union
{
    BEu64 literalValue;
    BEf64 literalFloatValue;
    BE_punct punctuation;
} BE_TokenData;

BEPrivateAPI BEu64 BE_Pow10(BEu64 n)
{
    BEu64 rVal = 1;
    for (BEu64 i = 0; i < n; ++i)
    {
        rVal *= 10;
    }
    return rVal;
}

BEPrivateAPI BEbool BE_FeedTokenizer(BE_TokenizerState* state, char c, BE_TokenType* out, BE_TokenData* outData)
{
    BEbool tokenComplete = 0;
    
#define TokenComplete(type) \
BEAssert(!tokenComplete); /* NOTE(Jens): If triggered, two tokens tried to end at the same place - one being empty. */ \
tokenComplete = 1;                                                                                                     \
*out = type;                                                                                                           \
if (type == BE_TokenType_Punctuation) { outData->punctuation = state->evaluation.punctuation; }                         \
else if (type == BE_TokenType_NumberLiteral) { outData->literalValue = state->evaluation.numberLiteral.value; }         \
else if (type == BE_TokenType_FloatLiteral) { outData->literalFloatValue = (BEf64)state->evaluation.floatLiteral.value / (BEf64)BE_Pow10(state->evaluation.floatLiteral.decimalDigits); }         \
state->mode = BE_TokenizerMode_None;                                                                                   \
goto label_StartMode
    
    BE_punct punct;
    BE_ClearStructToZero(punct);
    
    punct.str[0] = c;
    
    switch (state->mode)
    {
        case BE_TokenizerMode_None:
        {
            label_StartMode:
            
            BE_ClearStructToZero(state->evaluation);
            
            switch (c)
            {
                case '\0':
                {
                } break;
                
                case '0':
                {
                    state->mode = BE_TokenizerMode_0;
                } break;
                
                case '/':
                {
                    state->mode = BE_TokenizerMode_ForwardSlash;
                } break;
                
                case '\'':
                case '"':
                {
                    state->mode = BE_TokenizerMode_QuotedString;
                    state->evaluation.quotedString.quoteSign = c;
                } break;
                
                // NOTE(Jens): Currently not used.
                // {
                case ':':
                case '$':
                // }
                case '~':
                case '@':
                case ';':
                case '(':
                case ')':
                case '[':
                case ']':
                case '{':
                case '}':
                case '?':
                case ',':
                case '#':
                {
                    state->evaluation.punctuation = punct;
                    state->mode = BE_TokenizerMode_EndOfTokenStart + BE_TokenType_Punctuation;
                } break;
                
                case '.':
                {
                    state->mode = BE_TokenizerMode_Dot;
                } break;
                
                case '%':
                {
                    state->mode = BE_TokenizerMode_Percentage;
                } break;
                
                case '!':
                {
                    state->mode = BE_TokenizerMode_Exclamation;
                } break;
                
                case '*':
                {
                    state->mode = BE_TokenizerMode_Star;
                } break;
                
                case '-':
                {
                    state->mode = BE_TokenizerMode_Minus;
                } break;
                
                case '+':
                {
                    state->mode = BE_TokenizerMode_Plus;
                } break;
                
                case '<':
                {
                    state->mode = BE_TokenizerMode_Less;
                } break;
                
                case '>':
                {
                    state->mode = BE_TokenizerMode_Greater;
                } break;
                
                case '&':
                {
                    state->mode = BE_TokenizerMode_Ampersand;
                } break;
                
                case '|':
                {
                    state->mode = BE_TokenizerMode_VerticalLine;
                } break;
                
                case '^':
                {
                    state->mode = BE_TokenizerMode_Hat;
                } break;
                
                case '=':
                {
                    state->mode = BE_TokenizerMode_Equals;
                } break;
                
                default:
                {
                    if (BE_IsDigit(c))
                    {
                        state->mode = BE_TokenizerMode_DecimalDigits;
                        goto label_DecimalDigits;
                    }
                    else if (BE_IsAlpha(c) || c == '_')
                    {
                        state->mode = BE_TokenizerMode_Identifier;
                    }
                    else if (BE_IsWhitespace(c))
                    {
                        state->mode = BE_TokenizerMode_Whitespace;
                    }
                    else
                    {
                        state->mode = BE_TokenizerMode_EndOfTokenStart + BE_TokenType_Invalid;
                    }
                } break;
            }
        } break;
        
        case BE_TokenizerMode_0:
        {
            if (c == 'b')
            {
                state->mode = BE_TokenizerMode_0b;
            }
            else if (c == 'x')
            {
                state->mode = BE_TokenizerMode_0x;
            }
            else if (c == '0')
            {
                // NOTE(Jens): '00...' is not a valid token.
                TokenComplete(BE_TokenType_Invalid);
            }
            else if (c == '.')
            {
                state->mode = BE_TokenizerMode_FloatFDigits;
            }
            else
            {
                state->mode = BE_TokenizerMode_OctalDigits;
                goto label_OctalDigits;
            }
        } break;
        
        case BE_TokenizerMode_Dot:
        {
            if (BE_IsOneOf(c, "'0123456789"))
            {
                state->mode = BE_TokenizerMode_FloatFDigits;
                goto label_FloatDecimalDigits;
            }
            else
            {
                state->evaluation.punctuation = BE_Punctuation(".");
                TokenComplete(BE_TokenType_Punctuation);
            }
        } break;
        
        case BE_TokenizerMode_0b:
        {
            if (BE_IsOneOf(c, "'01"))
            {
                state->mode = BE_TokenizerMode_BinaryDigits;
                goto label_BinaryDigits;
            }
            else
            {
                TokenComplete(BE_TokenType_Invalid);
            }
        } break;
        
        case BE_TokenizerMode_0x:
        {
            if (BE_IsOneOf(c, "'0123456789abcdefABCDEF"))
            {
                state->mode = BE_TokenizerMode_HexDigits;
                goto label_HexDigits;
            }
            else
            {
                TokenComplete(BE_TokenType_Invalid);
            }
        } break;
        
        case BE_TokenizerMode_FloatFDigits:
        {
            label_FloatDecimalDigits:
            
            if (c == '\'')
            {
            }
            else if (BE_IsOneOf(c, "0123456789"))
            {
                ++state->evaluation.floatLiteral.decimalDigits;
                state->evaluation.floatLiteral.value *= 10;
                state->evaluation.floatLiteral.value += (c - '0');
            }
            else
            {
                TokenComplete(BE_TokenType_FloatLiteral);
            }
        } break;
        
        case BE_TokenizerMode_BinaryDigits:
        {
            label_BinaryDigits:
            
            if (c == '\'')
            {
            }
            else if (BE_IsOneOf(c, "01"))
            {
                BEu64 oldValue = state->evaluation.numberLiteral.value;
                
                state->evaluation.numberLiteral.value *= 2;
                state->evaluation.numberLiteral.value += c - '0';
                
                if (oldValue > state->evaluation.numberLiteral.value)
                {
                    TokenComplete(BE_TokenType_Invalid);
                }
            }
            else
            {
                TokenComplete(BE_TokenType_NumberLiteral);
            }
        } break;
        
        case BE_TokenizerMode_OctalDigits:
        {
            label_OctalDigits:
            
            if (c == '\'')
            {
            }
            else if ('0' <= c && c <= '7')
            {
                BEu64 oldValue = state->evaluation.numberLiteral.value;
                
                state->evaluation.numberLiteral.value *= 010;
                state->evaluation.numberLiteral.value += c - '0';
                
                if (oldValue > state->evaluation.numberLiteral.value)
                {
                    TokenComplete(BE_TokenType_Invalid);
                }
            }
            else
            {
                TokenComplete(BE_TokenType_NumberLiteral);
            }
        } break;
        
        case BE_TokenizerMode_DecimalDigits:
        {
            label_DecimalDigits:
            
            if (c == '\'')
            {
            }
            else if ('0' <= c && c <= '9')
            {
                BEu64 oldValue = state->evaluation.numberLiteral.value;
                
                state->evaluation.numberLiteral.value *= 10;
                state->evaluation.numberLiteral.value += c - '0';
                
                if (oldValue > state->evaluation.numberLiteral.value)
                {
                    TokenComplete(BE_TokenType_Invalid);
                }
            }
            else if (c == '.')
            {
                state->evaluation.floatLiteral.value = state->evaluation.numberLiteral.value;
                state->mode = BE_TokenizerMode_FloatFDigits;
            }
            else
            {
                TokenComplete(BE_TokenType_NumberLiteral);
            }
        } break;
        
        case BE_TokenizerMode_HexDigits:
        {
            label_HexDigits:
            
            if (c == '\'')
            {
            }
            else if ('0' <= c && c <= '9')
            {
                BEu64 oldValue = state->evaluation.numberLiteral.value;
                
                state->evaluation.numberLiteral.value *= 0x10;
                state->evaluation.numberLiteral.value += c - '0';
                
                if (oldValue > state->evaluation.numberLiteral.value)
                {
                    TokenComplete(BE_TokenType_Invalid);
                }
            }
            else if ('a' <= c && c <= 'f')
            {
                BEu64 oldValue = state->evaluation.numberLiteral.value;
                
                state->evaluation.numberLiteral.value *= 0x10;
                state->evaluation.numberLiteral.value += 0xa + c - 'a';
                
                if (oldValue > state->evaluation.numberLiteral.value)
                {
                    TokenComplete(BE_TokenType_Invalid);
                }
            }
            else if ('A' <= c && c <= 'F')
            {
                BEu64 oldValue = state->evaluation.numberLiteral.value;
                
                state->evaluation.numberLiteral.value *= 0x10;
                state->evaluation.numberLiteral.value += 0xA + c - 'A';
                
                if (oldValue > state->evaluation.numberLiteral.value)
                {
                    TokenComplete(BE_TokenType_Invalid);
                }
            }
            else
            {
                TokenComplete(BE_TokenType_NumberLiteral);
            }
        } break;
        
        case BE_TokenizerMode_ForwardSlash:
        {
            if (c == '/')
            {
                state->mode = BE_TokenizerMode_LineComment;
            }
            else if (c == '*')
            {
                state->mode = BE_TokenizerMode_MultilineComment;
            }
            else if (c == '=')
            {
                state->evaluation.punctuation = BE_Punctuation("/=");
                state->mode = BE_TokenizerMode_EndOfTokenStart + BE_TokenType_Punctuation;
            }
            else
            {
                state->evaluation.punctuation = BE_Punctuation("/");
                TokenComplete(BE_TokenType_Punctuation);
            }
        } break;
        
        case BE_TokenizerMode_Whitespace:
        {
            if (c == 0 || !BE_IsWhitespace(c))
            {
                TokenComplete(BE_TokenType_Whitespace);
            }
        } break;
        
        case BE_TokenizerMode_LineComment:
        {
            if (c == 0)
            {
                TokenComplete(BE_TokenType_LineComment);
            }
            else if (c == '\n') // NOTE(Jens): \r\n also ends with \n, the new line is part of the line comment token.
            {
                state->mode = BE_TokenizerMode_EndOfTokenStart + BE_TokenType_LineComment;
            }
        } break;
        
        case BE_TokenizerMode_MultilineComment:
        {
            if (c == 0)
            {
                // NOTE(Jens): End of input while parsing comment.
                TokenComplete(BE_TokenType_Invalid);
            }
            else
            {
                if (state->prevC == '/' && c == '*')
                {
                    ++state->evaluation.multilineComment.nestingCount;
                }
                else if (state->prevC == '*' && c == '/')
                {
                    if (state->evaluation.multilineComment.nestingCount == 0)
                    {
                        state->mode = BE_TokenizerMode_EndOfTokenStart + BE_TokenType_MultilineComment;
                    }
                    else
                    {
                        --state->evaluation.multilineComment.nestingCount;
                    }
                }
            }
        } break;
        
        case BE_TokenizerMode_Identifier:
        {
            if (BE_IsAlpha(c) || BE_IsDigit(c) || c == '_')
            {
            }
            else
            {
                TokenComplete(BE_TokenType_Identifier);
            }
        } break;
        
        case BE_TokenizerMode_QuotedString:
        {
            if (c == '\\' && !state->evaluation.quotedString.hasPendingBackslash)
            {
                state->evaluation.quotedString.hasPendingBackslash = 1;
            }
            else 
            {
                if (!state->evaluation.quotedString.hasPendingBackslash && c == state->evaluation.quotedString.quoteSign)
                {
                    state->mode = BE_TokenizerMode_EndOfTokenStart + BE_TokenType_QuotedString;
                }
                else if (c == 0)
                {
                    TokenComplete(BE_TokenType_Invalid);
                }
                
                state->evaluation.quotedString.hasPendingBackslash = 0;
            }
        } break;
        
        case BE_TokenizerMode_Star:
        {
            if (c == '=')
            {
                state->evaluation.punctuation = BE_Punctuation("*=");
                state->mode = BE_TokenizerMode_EndOfTokenStart + BE_TokenType_Punctuation;
            }
            else
            {
                state->evaluation.punctuation = BE_Punctuation("*");
                TokenComplete(BE_TokenType_Punctuation);
            }
        } break;
        
        case BE_TokenizerMode_Minus:
        {
            if (c == '=')
            {
                state->evaluation.punctuation = BE_Punctuation("-=");
                state->mode = BE_TokenizerMode_EndOfTokenStart + BE_TokenType_Punctuation;
            }
            else if (c == '-')
            {
                state->evaluation.punctuation = BE_Punctuation("--");
                state->mode = BE_TokenizerMode_EndOfTokenStart + BE_TokenType_Punctuation;
            }
            else
            {
                state->evaluation.punctuation = BE_Punctuation("-");
                TokenComplete(BE_TokenType_Punctuation);
            }
        } break;
        
        case BE_TokenizerMode_Plus:
        {
            if (c == '=')
            {
                state->evaluation.punctuation = BE_Punctuation("+=");
                state->mode = BE_TokenizerMode_EndOfTokenStart + BE_TokenType_Punctuation;
            }
            else if (c == '+')
            {
                state->evaluation.punctuation = BE_Punctuation("++");
                state->mode = BE_TokenizerMode_EndOfTokenStart + BE_TokenType_Punctuation;
            }
            else
            {
                state->evaluation.punctuation = BE_Punctuation("+");
                TokenComplete(BE_TokenType_Punctuation);
            }
        } break;
        
        case BE_TokenizerMode_Less:
        {
            if (c == '<')
            {
                state->mode = BE_TokenizerMode_LessLess;
            }
            else if (c == '=')
            {
                state->evaluation.punctuation = BE_Punctuation("<=");
                state->mode = BE_TokenizerMode_EndOfTokenStart + BE_TokenType_Punctuation;
            }
            else
            {
                state->evaluation.punctuation = BE_Punctuation("<");
                TokenComplete(BE_TokenType_Punctuation);
            }
        } break;
        
        case BE_TokenizerMode_Greater:
        {
            if (c == '>')
            {
                state->mode = BE_TokenizerMode_GreaterGreater;
            }
            else if (c == '=')
            {
                state->evaluation.punctuation = BE_Punctuation(">=");
                state->mode = BE_TokenizerMode_EndOfTokenStart + BE_TokenType_Punctuation;
            }
            else
            {
                state->evaluation.punctuation = BE_Punctuation(">");
                TokenComplete(BE_TokenType_Punctuation);
            }
        } break;
        
        case BE_TokenizerMode_Hat:
        {
            if (c == '=')
            {
                state->evaluation.punctuation = BE_Punctuation("^=");
                state->mode = BE_TokenizerMode_EndOfTokenStart + BE_TokenType_Punctuation;
            }
            else
            {
                state->evaluation.punctuation = BE_Punctuation("^");
                TokenComplete(BE_TokenType_Punctuation);
            }
        } break;
        
        // NOTE(Jens): &&= and ||= are not punctuations... but could be if we wanted boolean compound assignment operators.
        
        case BE_TokenizerMode_Ampersand:
        {
            if (c == '=')
            {
                state->evaluation.punctuation = BE_Punctuation("&=");
                state->mode = BE_TokenizerMode_EndOfTokenStart + BE_TokenType_Punctuation;
            }
            else if (c == '&')
            {
                state->evaluation.punctuation = BE_Punctuation("&&");
                state->mode = BE_TokenizerMode_EndOfTokenStart + BE_TokenType_Punctuation;
            }
            else
            {
                state->evaluation.punctuation = BE_Punctuation("&");
                TokenComplete(BE_TokenType_Punctuation);
            }
        } break;
        
        case BE_TokenizerMode_VerticalLine:
        {
            if (c == '=')
            {
                state->evaluation.punctuation = BE_Punctuation("|=");
                state->mode = BE_TokenizerMode_EndOfTokenStart + BE_TokenType_Punctuation;
            }
            else if (c == '|')
            {
                state->evaluation.punctuation = BE_Punctuation("||");
                state->mode = BE_TokenizerMode_EndOfTokenStart + BE_TokenType_Punctuation;
            }
            else
            {
                state->evaluation.punctuation = BE_Punctuation("|");
                TokenComplete(BE_TokenType_Punctuation);
            }
        } break;
        
        case BE_TokenizerMode_Exclamation:
        {
            if (c == '=')
            {
                state->evaluation.punctuation = BE_Punctuation("!=");
                state->mode = BE_TokenizerMode_EndOfTokenStart + BE_TokenType_Punctuation;
            }
            else
            {
                state->evaluation.punctuation = BE_Punctuation("!");
                TokenComplete(BE_TokenType_Punctuation);
            }
        } break;
        
        case BE_TokenizerMode_Percentage:
        {
            if (c == '=')
            {
                state->evaluation.punctuation = BE_Punctuation("%=");
                state->mode = BE_TokenizerMode_EndOfTokenStart + BE_TokenType_Punctuation;
            }
            else
            {
                state->evaluation.punctuation = BE_Punctuation("%");
                TokenComplete(BE_TokenType_Punctuation);
            }
        } break;
        
        case BE_TokenizerMode_Equals:
        {
            if (c == '=')
            {
                state->evaluation.punctuation = BE_Punctuation("==");
                state->mode = BE_TokenizerMode_EndOfTokenStart + BE_TokenType_Punctuation;
            }
            else
            {
                state->evaluation.punctuation = BE_Punctuation("=");
                TokenComplete(BE_TokenType_Punctuation);
            }
        } break;
        
        case BE_TokenizerMode_LessLess:
        {
            if (c == '=')
            {
                state->evaluation.punctuation = BE_Punctuation("<<=");
                state->mode = BE_TokenizerMode_EndOfTokenStart + BE_TokenType_Punctuation;
            }
            else
            {
                state->evaluation.punctuation = BE_Punctuation("<<");
                TokenComplete(BE_TokenType_Punctuation);
            }
        } break;
        
        case BE_TokenizerMode_GreaterGreater:
        {
            if (c == '=')
            {
                state->evaluation.punctuation = BE_Punctuation(">>=");
                state->mode = BE_TokenizerMode_EndOfTokenStart + BE_TokenType_Punctuation;
            }
            else
            {
                state->evaluation.punctuation = BE_Punctuation(">>");
                TokenComplete(BE_TokenType_Punctuation);
            }
        } break;
        
        default:
        {
            if (state->mode >= BE_TokenizerMode_EndOfTokenStart)
            {
                BE_TokenType type = (BE_TokenType)(state->mode - BE_TokenizerMode_EndOfTokenStart);
                TokenComplete(type);
            }
            else
            {
                BEAssert(!"Bug in tokenizer.");
            }
            
        } break;
    }
    
    if (c == 0)
    {
        // NOTE(Jens): If triggered there's a bug causing the currently parsed token to not output. '\0' indicates end of input characters.
        
        BEAssert(state->mode == BE_TokenizerMode_None);
        BEAssert(tokenComplete);
    }
    
    state->prevC = c;
    
#undef TokenComplete
    
    return tokenComplete;
}


BEPrivateAPI BEbool BE_StringsAreEqual(BE_String a, BE_String b)
{
	BEbool rVal = 0;
	if (a.count == b.count)
	{
		rVal = 1;
		if (a.count && a.characters != b.characters)
		{
            for (BEu64 i = 0; i < a.count; ++i)
            {
                if (a.characters[i] != b.characters[i])
                {
                    rVal = 0;
                    break;
                }
            }
		}
	}
	return rVal;
}

BEPrivateAPI BEbool BE_PunctuationsAreSame(BE_punct a, BE_punct b)
{
    BEbool rVal = (a.str[0] == b.str[0] && a.str[1] == b.str[1] && a.str[2] == b.str[2] && a.str[3] == b.str[3]);
    return rVal;
}

BEPrivateAPI BEbool BE_PunctuationIsValid(BE_punct punct)
{
    return punct.str[0];
}

#define BE_InvalidDefault default: { BEAssert(!"Invalid switch case"); } break

BEPrivateAPI BEbool BE_StackEntriesAreEqual(BE_StackEntry a, BE_StackEntry b)
{
    BEbool rVal = (a.stack == b.stack && a.index == b.index);
    return rVal;
}

BEPrivateAPI BE_StackEntry BE_Local(BEu32 i)
{
    BE_StackEntry rVal;
    BE_ClearStructToZero(rVal);
    rVal.stack = BE_Stack_Local;
    rVal.index = i + BE_LocalSymbol_Count;
    return rVal;
}

BEPrivateAPI BE_StackEntry BE_Global(BEu32 i)
{
    BE_StackEntry rVal;
    BE_ClearStructToZero(rVal);
    rVal.stack = BE_Stack_Global;
    rVal.index = i + BE_GlobalSymbol_Count;
    return rVal;
}

BEPrivateAPI BE_StackEntry BE_Parent(BEu32 i)
{
    BE_StackEntry rVal;
    BE_ClearStructToZero(rVal);
    rVal.stack = BE_Stack_Parent;
    rVal.index = i + BE_LocalSymbol_Count;
    return rVal;
}

BEPrivateAPI BE_StackEntry BE_FromParentScope(BE_StackEntry orig)
{
    BE_StackEntry rVal = orig;
    
    if (rVal.stack == BE_Stack_Local)
    {
        rVal.stack = BE_Stack_Parent;
    }
    
    return rVal;
}

BEPrivateAPI BE_StackEntry BE_MemberOffset(void)
{
    BE_StackEntry rVal;
    BE_ClearStructToZero(rVal);
    rVal.stack = BE_Stack_Global;
    rVal.index = BE_GlobalSymbol_MemberOffset;
    return rVal;
}

BEPrivateAPI BE_StackEntry BE_ReturnAddress(void)
{
    BE_StackEntry rVal;
    BE_ClearStructToZero(rVal);
    rVal.stack = BE_Stack_Local;
    rVal.index = BE_LocalSymbol_ReturnAddress;
    return rVal;
}

BEPrivateAPI BE_StackEntry BE_ArrayIndex(void)
{
    BE_StackEntry rVal;
    BE_ClearStructToZero(rVal);
    rVal.stack = BE_Stack_Local;
    rVal.index = BE_LocalSymbol_ArrayIndex;
    return rVal;
}

typedef struct
{
    BEbool success;
    union
    {
        BEu64 uVal;
        BEs64 sVal;
        double dVal;
        BEu8 bytes[sizeof(BEu64)];
    };
} BE_ParseNumberResult;

#define BE_MaxOperatorPrecedence 11
#define BE_UnaryOperatorPrecedence BE_MaxOperatorPrecedence

BEPrivateAPI BEbool BE_IsUnaryOperator(BE_punct op)
{
    BEbool rVal = 0;
    
    if (BE_PunctuationsAreSame(op, BE_Punctuation("!"))
        || BE_PunctuationsAreSame(op, BE_Punctuation("~"))
        || BE_PunctuationsAreSame(op, BE_Punctuation("-"))
        || BE_PunctuationsAreSame(op, BE_Punctuation("abs")))
    {
        rVal = 1;
    }
    
    return rVal;
}

BEPrivateAPI BEu32 BE_PrecedenceOfBinaryOperator(BE_punct op)
{
    BEu32 rVal = 0;
    
    if (   BE_PunctuationsAreSame(op, BE_Punctuation("*")) || BE_PunctuationsAreSame(op, BE_Punctuation("/"))
        || BE_PunctuationsAreSame(op, BE_Punctuation("%")))
    {
        rVal = 10;
    }
    else if (BE_PunctuationsAreSame(op, BE_Punctuation("-")) || BE_PunctuationsAreSame(op, BE_Punctuation("+")))
    {
        rVal = 9;
    }
    else if (BE_PunctuationsAreSame(op, BE_Punctuation("<<")) || BE_PunctuationsAreSame(op, BE_Punctuation(">>")))
    {
        rVal = 8;
    }
    else if (   BE_PunctuationsAreSame(op, BE_Punctuation("<")) || BE_PunctuationsAreSame(op, BE_Punctuation("<="))
             || BE_PunctuationsAreSame(op, BE_Punctuation(">")) || BE_PunctuationsAreSame(op, BE_Punctuation(">=")))
    {
        rVal = 7;
    }
    else if (BE_PunctuationsAreSame(op, BE_Punctuation("==")) || BE_PunctuationsAreSame(op, BE_Punctuation("!=")))
    {
        rVal = 6;
    }
    else if (BE_PunctuationsAreSame(op, BE_Punctuation("&")))
    {
        rVal = 5;
    }
    else if (BE_PunctuationsAreSame(op, BE_Punctuation("^")))
    {
        rVal = 4;
    }
    else if (BE_PunctuationsAreSame(op, BE_Punctuation("|")))
    {
        rVal = 3;
    }
    else if (BE_PunctuationsAreSame(op, BE_Punctuation("&&")))
    {
        rVal = 2;
    }
    else if (BE_PunctuationsAreSame(op, BE_Punctuation("||")))
    {
        rVal = 1;
    }
    
    return rVal;
}

typedef enum
{
    BE_IdentifierFlag_Default = 0,
    
    BE_IdentifierFlag_Deprecated = (1 << 0),
} BE_IdentifierFlags;

BEPrivateAPI BE_ParseNumberResult BE_QuotedStringAsUnsignedInteger(BEu64 count, char* characters)
{
    BE_ParseNumberResult rVal = { 0 };
    
    BEu32 outIndex = 0;
    BEbool trailingBackslash = 0;
    for (BEu64 i = 1; i < count - 1; ++i)
    {
        if (outIndex >= BE_ArrayCount(rVal.bytes))
        {
            // NOTE(Jens): String cannot be integer, it's too large.
            break;
        }
        
        char c = characters[i];
        if (c == '\\' && !trailingBackslash)
        {
            trailingBackslash = 1;
        }
        else
        {
            if (trailingBackslash)
            {
                if (c == 'n')
                {
                    rVal.bytes[outIndex++] = '\n';
                }
                else if (c == 'r')
                {
                    rVal.bytes[outIndex++] = '\r';
                }
                else if (c == '\\')
                {
                    rVal.bytes[outIndex++] = '\\';
                }
                else if (c == 't')
                {
                    rVal.bytes[outIndex++] = '\t';
                }
                else if (c == '0')
                {
                    rVal.bytes[outIndex++] = '\0';
                }
                else
                {
                    break;
                }
            }
            else
            {
                rVal.bytes[outIndex++] = c;
            }
            
            trailingBackslash = 0;
        }
    }
    
    if (trailingBackslash)
    {
    }
    else
    {
        rVal.success = 1;
    }
    
    return rVal;
}


//
// NOTE(Jens): [unary-operator...] <value> <binary-operator>|<END>
//
//             [unary-operator...] '(' ... ')' <binary-operator>|<END>
//
// where unary operators are evaluated right-to-left.
//

struct BE_BinaryOperator
{
    BE_punct op;
    BEu32 precedence;
};

struct BE_UnaryOperator
{
    BE_UnaryOperator* next;
    
    BE_punct op;
    BEu32 precedence;
};

typedef enum BE_OperatorValueType BE_OperatorValueType;

enum BE_OperatorValueType
{
    BE_OperatorValueType_Invalid,
    
    BE_OperatorValueType_Constant,
    BE_OperatorValueType_MemberOffset,
    BE_OperatorValueType_LocalVariable,
    BE_OperatorValueType_GlobalVariable,
    BE_OperatorValueType_Parameter,
    BE_OperatorValueType_Member,
    BE_OperatorValueType_SizeOfFile,
    BE_OperatorValueType_HasType,
    BE_OperatorValueType_SizeOf,
    BE_OperatorValueType_Load,
    BE_OperatorValueType_CreatePlot,
    BE_OperatorValueType_ArrayCount,
    
    BE_OperatorValueType_StackEntry,
};

typedef struct
{
    BEu32 index;
} BE_OperatorValue_LocalVariable;

typedef struct
{
    BEu32 index;
} BE_OperatorValue_GlobalVariable;

typedef struct
{
    BEu32 index;
} BE_OperatorValue_Parameter;

typedef struct
{
    BEu32 category;
    BE_Expression categoryNumber;
} BE_OperatorValue_HasType;

typedef struct
{
    BEu32 typeIndex;
    BE_Expression address;
} BE_OperatorValue_SizeOf;

typedef struct
{
    BE_Expression address;
    BE_Expression size;
    BEbool isSigned;
    BEbool isBig;
} BE_OperatorValue_Load;

struct BE_MemberPathNode
{
    BE_MemberPathNode* next;
    
    BEu32 memberIndex;
    BE_Expression arrayIndex;
};

typedef struct
{
    BE_MemberPath path;
} BE_OperatorValue_Member;

typedef struct
{
    BE_MemberPath path;
} BE_OperatorValue_ArrayCount;

typedef struct
{
    BE_StackEntry entry;
} BE_OperatorValue_StackEntry;

typedef union
{
    BEs64 i;
    BEf64 f;
} BE_ScalarValue;

typedef struct
{
    BE_OperatorValueType type;
    
    union
    {
        BE_ScalarValue constant;
        BE_OperatorValue_LocalVariable localVariable;
        BE_OperatorValue_GlobalVariable globalVariable;
        BE_OperatorValue_Parameter parameter;
        BE_OperatorValue_HasType hasType;
        BE_OperatorValue_SizeOf sizeOf;
        BE_OperatorValue_Member member;
        BE_OperatorValue_Load load;
        BE_OperatorValue_ArrayCount arrayCount;
        BE_OperatorValue_StackEntry stackEntry;
    };
} BE_OperatorValue;

BEPrivateAPI BE_MemberPathNode* BE_AppendMemberPath(BEAllocatorType allocator, BE_MemberPath* path, BEu32 memberIndex)
{
    BE_MemberPathNode* newNode = BEAlloc(allocator, BE_MemberPathNode);
    BE_ClearStructToZero(*newNode);
    
    newNode->memberIndex = memberIndex;
    
    if (path->head)
    {
        path->tail = path->tail->next = newNode;
    }
    else
    {
        path->head = path->tail = newNode;
    }
    
    ++path->count;
    
    return newNode;
}

typedef struct BE_ExpressionTerm
{
    struct BE_ExpressionTerm* next;
    struct BE_ExpressionTerm* prev;
    
    BE_UnaryOperator* unaryOperators;
    BE_BinaryOperator binaryOperator;
    
    BE_OperatorValue value;
} BE_ExpressionTerm;

BEPrivateAPI BE_ExpressionTerm* BE_AllocTerm(BEAllocatorType allocator, BE_Expressions* expressions)
{
    BE_ExpressionTerm* rVal = expressions->freeTerms;
    if (rVal)
    {
        expressions->freeTerms = expressions->freeTerms->next;
    }
    else
    {
        rVal = BEAlloc(allocator, BE_ExpressionTerm);
    }
    
    BE_ClearStructToZero(*rVal);
    return rVal;
}

BEPrivateAPI BE_UnaryOperator* BE_AllocUnaryOperator(BEAllocatorType allocator, BE_Expressions* expressions)
{
    BE_UnaryOperator* rVal = expressions->freeUnaryOperators;
    if (rVal)
    {
        expressions->freeUnaryOperators = expressions->freeUnaryOperators->next;
    }
    else
    {
        rVal = BEAlloc(allocator, BE_UnaryOperator);
    }
    
    BE_ClearStructToZero(*rVal);
    return rVal;
}

BEPrivateAPI void BE_FreeExpression(BE_Expressions* expressions, BE_Expression* expression)
{
    if (expression->termsSentinel)
    {
        for (BE_ExpressionTerm* term = expression->termsSentinel->next; term && term != expression->termsSentinel; )
        {
            for (BE_UnaryOperator* unaryOp = term->unaryOperators; unaryOp; )
            {
                BE_UnaryOperator* nextUnaryOp = unaryOp->next;
                
                unaryOp->next = expressions->freeUnaryOperators;
                expressions->freeUnaryOperators = unaryOp;
                
                unaryOp = nextUnaryOp;
            }
            
            BE_ExpressionTerm* nextTerm = term->next;
            
            term->next = expressions->freeTerms;
            expressions->freeTerms = term;
            
            term = nextTerm;
        }
    }
    
    BE_ClearStructToZero(*expression);
}

BEPrivateAPI BEbool BE_ExpressionIsValid(BE_Expression* expression)
{
    return expression->termsSentinel != 0 && expression->termsSentinel->next && expression->termsSentinel->next->value.type != BE_OperatorValueType_Invalid;
}

BEPrivateAPI BE_punct BE_PopUnaryOperator(BE_Expressions* expressions, BE_ExpressionTerm* term)
{
    BEAssert(term->unaryOperators);
    
    BE_punct rVal = term->unaryOperators->op;
    BE_UnaryOperator* nextUnaryOp = term->unaryOperators->next;
    
    term->unaryOperators->next = expressions->freeUnaryOperators;
    expressions->freeUnaryOperators = term->unaryOperators;
    
    term->unaryOperators = nextUnaryOp;
    
    return rVal;
}

BEPrivateAPI BE_ExpressionTerm* BE_PushTerm(BEAllocatorType allocator, BE_Expressions* expressions, BE_Expression* expression)
{
    BE_ExpressionTerm* term = BE_AllocTerm(allocator, expressions);
    
    term->prev = expression->termsSentinel->prev;
    term->next = expression->termsSentinel;
    
    term->prev->next = term->next->prev = term;
    
    return term;
}

BEPrivateAPI void BE_InitExpression(BEAllocatorType allocator, BE_Expressions* expressions, BE_Expression* expression, BEbool isFloat)
{
    BE_FreeExpression(expressions, expression);
    
    expression->termsSentinel = BE_AllocTerm(allocator, expressions);
    expression->termsSentinel->prev = expression->termsSentinel->next = expression->termsSentinel;
    expression->isFloat = isFloat;
    
    BE_PushTerm(allocator, expressions, expression);
}

BEPrivateAPI BE_Expression BE_CopyExpression(BEAllocatorType allocator, BE_Expressions* expressions, BE_Expression* expression)
{
    BE_Expression rVal;
    BE_ClearStructToZero(rVal);
    
    if (expression->termsSentinel)
    {
        rVal.termsSentinel = BE_AllocTerm(allocator, expressions);
        rVal.termsSentinel->prev = rVal.termsSentinel->next = rVal.termsSentinel;
        
        for (BE_ExpressionTerm* term = expression->termsSentinel->next; term != expression->termsSentinel; term = term->next)
        {
            BE_ExpressionTerm* newTerm = BE_PushTerm(allocator, expressions, &rVal);
            
            newTerm->binaryOperator = term->binaryOperator;
            newTerm->value = term->value;
            
            BE_UnaryOperator** newUnaryOpSlot = &newTerm->unaryOperators;
            for (BE_UnaryOperator* unaryOp = term->unaryOperators; unaryOp; unaryOp = unaryOp->next)
            {
                BE_UnaryOperator* newUnaryOp = BE_AllocUnaryOperator(allocator, expressions);
                newUnaryOp->op = unaryOp->op;
                newUnaryOp->precedence = unaryOp->precedence;
                
                *newUnaryOpSlot = newUnaryOp;
                newUnaryOpSlot = &(*newUnaryOpSlot)->next;
            }
        }
    }
    
    return rVal;
}

BEPrivateAPI BEbool BE_ExpressionHasPendingEndParenthesis(BE_Expression* expression)
{
    return (expression->currentExpressionPrecedence > 0);
}

BEPrivateAPI BEbool BE_PushOperator(BEAllocatorType allocator, BE_Expressions* expressions, BE_Expression* expression, BE_punct op)
{
    BEbool rVal = 1;
    
    BE_ExpressionTerm* currentExpression = expression->termsSentinel->prev;
    
    if (!currentExpression->value.type)
    {
        if (BE_IsUnaryOperator(op))
        {
            BE_UnaryOperator* unaryOp = BE_AllocUnaryOperator(allocator, expressions);
            unaryOp->op = op;
            unaryOp->precedence = expression->currentExpressionPrecedence + BE_UnaryOperatorPrecedence;
            
            // NOTE(Jens): Unary operators are evaluated right to left, push to beginning and evaluate in order.
            
            unaryOp->next = currentExpression->unaryOperators;
            currentExpression->unaryOperators = unaryOp;
        }
        else
        {
            // NOTE(Jens): Unrecognized unary operator.
            rVal = 0;
        }
    }
    else
    {
        if (!BE_PunctuationIsValid(currentExpression->binaryOperator.op))
        {
            currentExpression->binaryOperator.precedence = BE_PrecedenceOfBinaryOperator(op) + expression->currentExpressionPrecedence;
            
            if (currentExpression->binaryOperator.precedence)
            {
                currentExpression->binaryOperator.op = op;
                
                BE_PushTerm(allocator, expressions, expression);
            }
            else
            {
                // NOTE(Jens): Unrecognized binary operator.
                rVal = 0;
            }
        }
        else
        {
            // NOTE(Jens): Binary operator followed by binary operator.
            rVal = 0;
        }
    }
    
    return rVal;
}

BEPrivateAPI BEbool BE_ExpectsValue(BE_Expression* expression)
{
    BE_ExpressionTerm* currentExpression = expression->termsSentinel->prev;
    return !currentExpression->value.type;
}

BEPrivateAPI BE_OperatorValue* BE_PushValue(BE_Expression* expression)
{
    BE_OperatorValue* rVal = 0;
    
    BE_ExpressionTerm* currentExpression = expression->termsSentinel->prev;
    if (!currentExpression->value.type)
    {
        rVal = &currentExpression->value;
    }
    else
    {
        // NOTE(Jens): Expected binary operator got value.
    }
    
    return rVal;
}

BEPrivateAPI BEbool BE_PushConstantInteger(BE_Expression* expression, BEs64 value)
{
    BEbool rVal = 0;
    
    BE_OperatorValue* op = BE_PushValue(expression);
    if (op)
    {
        rVal = 1;
        op->type = BE_OperatorValueType_Constant;
        
        if (expression->isFloat)
        {
            op->constant.f = (BEf64)value;
        }
        else
        {
            op->constant.i = value;
        }
    }
    
    return rVal;
}

BEPrivateAPI BEbool BE_PushConstantFloat(BE_Expression* expression, BEf64 value)
{
    BEbool rVal = 0;
    
    BE_OperatorValue* op = BE_PushValue(expression);
    if (op)
    {
        rVal = 1;
        op->type = BE_OperatorValueType_Constant;
        
        BEAssert(expression->isFloat);
        op->constant.f = value;
    }
    
    return rVal;
}

BEPrivateAPI BEbool BE_PushMemberOffset(BE_Expression* expression)
{
    BEbool rVal = 0;
    
    BE_OperatorValue* op = BE_PushValue(expression);
    if (op)
    {
        rVal = 1;
        op->type = BE_OperatorValueType_MemberOffset;
    }
    
    return rVal;
}

BEPrivateAPI BEbool BE_PushLocalVariable(BE_Expression* expression, BEu32 index)
{
    BEbool rVal = 0;
    
    BE_OperatorValue* op = BE_PushValue(expression);
    if (op)
    {
        rVal = 1;
        op->type = BE_OperatorValueType_LocalVariable;
        op->localVariable.index = index;
    }
    
    return rVal;
}

BEPrivateAPI BEbool BE_PushParameter(BE_Expression* expression, BEu32 index)
{
    BEbool rVal = 0;
    
    BE_OperatorValue* op = BE_PushValue(expression);
    if (op)
    {
        rVal = 1;
        op->type = BE_OperatorValueType_Parameter;
        op->parameter.index = index;
    }
    
    return rVal;
}

BEPrivateAPI BEbool BE_PushHasType(BE_Expression* expression, BEu32 categoryIndex, BE_Expression categoryNumber)
{
    BEbool rVal = 0;
    
    BE_OperatorValue* op = BE_PushValue(expression);
    if (op)
    {
        rVal = 1;
        op->type = BE_OperatorValueType_HasType;
        op->hasType.category = categoryIndex;
        op->hasType.categoryNumber = categoryNumber;
    }
    
    return rVal;
}

BEPrivateAPI BEbool BE_PushCreatePlot(BE_Expression* expression)
{
    BEbool rVal = 0;
    
    BE_OperatorValue* op = BE_PushValue(expression);
    if (op)
    {
        rVal = 1;
        op->type = BE_OperatorValueType_CreatePlot;
    }
    
    return rVal;
}

BEPrivateAPI BEbool BE_PushSizeOf(BE_Expression* expression, BEu32 typeIndex, BE_Expression address)
{
    BEbool rVal = 0;
    
    BE_OperatorValue* op = BE_PushValue(expression);
    if (op)
    {
        rVal = 1;
        op->type = BE_OperatorValueType_SizeOf;
        op->sizeOf.typeIndex = typeIndex;
        op->sizeOf.address = address;
    }
    
    return rVal;
}

BEPrivateAPI BEbool BE_PushGlobalVariable(BE_Expression* expression, BEu32 index)
{
    BEbool rVal = 0;
    
    BE_OperatorValue* op = BE_PushValue(expression);
    if (op)
    {
        rVal = 1;
        op->type = BE_OperatorValueType_GlobalVariable;
        op->globalVariable.index = index;
    }
    
    return rVal;
}

BEPrivateAPI BEbool BE_PushParamater(BE_Expression* expression, BEu32 index)
{
    BEbool rVal = 0;
    
    BE_OperatorValue* op = BE_PushValue(expression);
    if (op)
    {
        rVal = 1;
        op->type = BE_OperatorValueType_Parameter;
        op->parameter.index = index;
    }
    
    return rVal;
}

BEPrivateAPI BEbool BE_PushMember(BE_Expression* expression, BE_MemberPath path)
{
    BEbool rVal = 0;
    
    BEAssert(path.count);
    
    BE_OperatorValue* op = BE_PushValue(expression);
    if (op)
    {
        rVal = 1;
        op->type = BE_OperatorValueType_Member;
        op->member.path = path;
    }
    
    return rVal;
}

BEPrivateAPI BE_MemberPath* BE_PushMemberPlaceholder(BE_Expression* expression)
{
    BE_MemberPath* rVal = 0;
    
    BE_OperatorValue* op = BE_PushValue(expression);
    if (op)
    {
        op->type = BE_OperatorValueType_Member;
        rVal = &op->member.path;
    }
    
    return rVal;
}

BEPrivateAPI BEbool BE_PushArrayCount(BE_Expression* expression, BE_MemberPath path)
{
    BEbool rVal = 0;
    
    BE_OperatorValue* op = BE_PushValue(expression);
    if (op)
    {
        rVal = 1;
        op->type = BE_OperatorValueType_ArrayCount;
        op->member.path = path;
    }
    
    return rVal;
}

BEPrivateAPI BE_MemberPath* BE_PushArrayCountPlaceholder(BE_Expression* expression)
{
    BE_MemberPath* rVal = 0;
    
    BE_OperatorValue* op = BE_PushValue(expression);
    if (op)
    {
        op->type = BE_OperatorValueType_ArrayCount;
        rVal = &op->member.path;
    }
    
    return rVal;
}

BEPrivateAPI BEbool BE_PushSizeOfFile(BE_Expression* expression)
{
    BEbool rVal = 0;
    
    BE_OperatorValue* op = BE_PushValue(expression);
    if (op)
    {
        rVal = 1;
        op->type = BE_OperatorValueType_SizeOfFile;
    }
    
    return rVal;
}

BEPrivateAPI BEbool BE_PushLoad(BE_Expression* expression, BE_Expression address, BE_Expression size, BEbool isSigned)
{
    BEbool rVal = 0;
    
    BE_OperatorValue* op = BE_PushValue(expression);
    if (op)
    {
        rVal = 1;
        op->type = BE_OperatorValueType_Load;
        op->load.address = address;
        op->load.size = size;
        op->load.isSigned = isSigned;
    }
    
    return rVal;
}

BEPrivateAPI BEbool BE_PushParenthesis(BE_Expression* expression, BEbool open)
{
    BEbool rVal = 1;
    
    if (open)
    {
        expression->currentExpressionPrecedence += BE_MaxOperatorPrecedence;
    }
    else
    {
        if (expression->currentExpressionPrecedence >= BE_MaxOperatorPrecedence)
        {
            expression->currentExpressionPrecedence -= BE_MaxOperatorPrecedence;
        }
        else
        {
            // NOTE(Jens): Unbalanced '(' ')'.
            rVal = 0;
        }
    }
    
    return rVal;
}

BEPrivateAPI BE_ScalarValue BE_PerformConstantUnaryOperator(BEbool isFloat, BE_punct op, BE_ScalarValue value)
{
    BE_ScalarValue rVal = { 0 };
    
    if (isFloat)
    {
        if (BE_PunctuationsAreSame(op, BE_Punctuation("-")))
        {
            rVal.f = -value.f;
        }
        else if (BE_PunctuationsAreSame(op, BE_Punctuation("abs")))
        {
            if (value.f < 0.0)
            {
                rVal.f = -value.f;
            }
            else
            {
                rVal.f = value.f;
            }
        }
        else
        {
            BEAssert(0);
        }
    }
    else
    {
        if (BE_PunctuationsAreSame(op, BE_Punctuation("-")))
        {
            rVal.i = -value.i;
        }
        else if (BE_PunctuationsAreSame(op, BE_Punctuation("!")))
        {
            rVal.i = !value.i;
        }
        else if (BE_PunctuationsAreSame(op, BE_Punctuation("~")))
        {
            rVal.i = ~value.i;
        }
        else if (BE_PunctuationsAreSame(op, BE_Punctuation("abs")))
        {
            if (value.i < 0)
            {
                rVal.i = -value.i;
            }
            else
            {
                rVal.i = value.i;
            }
        }
        else
        {
            BEAssert(0);
        }
    }
    
    return rVal;
}

BEPrivateAPI BE_InstructionType BE_UnaryOperatorToInstruction(BEbool isFloat, BE_punct op)
{
    BE_InstructionType rVal = BE_InstructionType_None;
    
    if (isFloat)
    {
        if (BE_PunctuationsAreSame(op, BE_Punctuation("-")))
        {
            rVal = BE_InstructionType_FNegate;
        }
        else if (BE_PunctuationsAreSame(op, BE_Punctuation("abs")))
        {
            rVal = BE_InstructionType_FAbs;
        }
    }
    else
    {
        if (BE_PunctuationsAreSame(op, BE_Punctuation("-")))
        {
            rVal = BE_InstructionType_Negate;
        }
        else if (BE_PunctuationsAreSame(op, BE_Punctuation("!")))
        {
            rVal = BE_InstructionType_LogicalNot;
        }
        else if (BE_PunctuationsAreSame(op, BE_Punctuation("~")))
        {
            rVal = BE_InstructionType_BitwiseNot;
        }
        else if (BE_PunctuationsAreSame(op, BE_Punctuation("abs")))
        {
            rVal = BE_InstructionType_Abs;
        }
    }
    
    return rVal;
}

typedef enum BE_ArithmeticError
{
    BE_ArithmeticError_None,
    
    BE_ArithmeticError_DivByZero,
    BE_ArithmeticError_ModByZero,
    // TODO(Jens): Do range checks and whatnot... but how without slowing things down too much? Optional checking?
} BE_ArithmeticError;

BEPrivateAPI BE_ScalarValue BE_PerformConstantBinaryOperator(BE_ArithmeticError* error, BEbool isFloat, BE_ScalarValue left, BE_punct op, BE_ScalarValue right)
{
    BE_ScalarValue rVal = { 0 };
    
    if (!*error)
    {
        // NOTE(Jens): Both integer and float have zero memory as zero.
        
        if (BE_PunctuationsAreSame(op, BE_Punctuation("/")) && right.i == 0)
        {
            *error = BE_ArithmeticError_DivByZero;
        }
        if (BE_PunctuationsAreSame(op, BE_Punctuation("%")) && right.i == 0)
        {
            *error = BE_ArithmeticError_ModByZero;
        }
    }
    
    if (!*error)
    {
        if (isFloat)
        {
#define ElseIfDoOp(op, candOp) else if (BE_PunctuationsAreSame(op, BE_Punctuation(#candOp))) rVal.f = left.f candOp right.f
            
            if (0) {}
            ElseIfDoOp(op, *);
            ElseIfDoOp(op, /);
            ElseIfDoOp(op, +);
            ElseIfDoOp(op, -);
            ElseIfDoOp(op, <);
            ElseIfDoOp(op, <=);
            ElseIfDoOp(op, >);
            ElseIfDoOp(op, >=);
            ElseIfDoOp(op, ==);
            ElseIfDoOp(op, !=);
            else
            {
                BEAssert(0);
            }
            
#undef ElseIfDoOp
        }
        else
        {
#define ElseIfDoOp(op, candOp) else if (BE_PunctuationsAreSame(op, BE_Punctuation(#candOp))) rVal.i = left.i candOp right.i
            
            if (0) {}
            ElseIfDoOp(op, *);
            ElseIfDoOp(op, /);
            ElseIfDoOp(op, %);
            ElseIfDoOp(op, +);
            ElseIfDoOp(op, -);
            ElseIfDoOp(op, <<);
            ElseIfDoOp(op, >>);
            ElseIfDoOp(op, <);
            ElseIfDoOp(op, <=);
            ElseIfDoOp(op, >);
            ElseIfDoOp(op, >=);
            ElseIfDoOp(op, ==);
            ElseIfDoOp(op, !=);
            ElseIfDoOp(op, &);
            ElseIfDoOp(op, |);
            ElseIfDoOp(op, ^);
            ElseIfDoOp(op, &&);
            ElseIfDoOp(op, ||);
            else
            {
                BEAssert(0);
            }
            
#undef ElseIfDoOp
        }
    }
    
    return rVal;
}

BEPrivateAPI BE_InstructionType BE_BinaryOperatorToInstruction(BEbool isFloat, BE_punct op)
{
    BE_InstructionType rVal = BE_InstructionType_None;
    
    if (isFloat)
    {
        if (BE_PunctuationsAreSame(op, BE_Punctuation("+")))
        {
            rVal = BE_InstructionType_FAdd;
        }
        else if (BE_PunctuationsAreSame(op, BE_Punctuation("-")))
        {
            rVal = BE_InstructionType_FSub;
        }
        else if (BE_PunctuationsAreSame(op, BE_Punctuation("*")))
        {
            rVal = BE_InstructionType_FMul;
        }
        else if (BE_PunctuationsAreSame(op, BE_Punctuation("/")))
        {
            rVal = BE_InstructionType_FDiv;
        }
        else if (BE_PunctuationsAreSame(op, BE_Punctuation("<")))
        {
            rVal = BE_InstructionType_FLess;
        }
        else if (BE_PunctuationsAreSame(op, BE_Punctuation("<=")))
        {
            rVal = BE_InstructionType_FLessOrEqual;
        }
        else if (BE_PunctuationsAreSame(op, BE_Punctuation("==")))
        {
            rVal = BE_InstructionType_FEqual;
        }
        else if (BE_PunctuationsAreSame(op, BE_Punctuation(">")))
        {
            rVal = BE_InstructionType_FGreater;
        }
        else if (BE_PunctuationsAreSame(op, BE_Punctuation(">=")))
        {
            rVal = BE_InstructionType_FGreaterOrEqual;
        }
        else if (BE_PunctuationsAreSame(op, BE_Punctuation("!=")))
        {
            rVal = BE_InstructionType_FNotEqual;
        }
    }
    else
    {
        if (BE_PunctuationsAreSame(op, BE_Punctuation("+")))
        {
            rVal = BE_InstructionType_Add;
        }
        else if (BE_PunctuationsAreSame(op, BE_Punctuation("-")))
        {
            rVal = BE_InstructionType_Sub;
        }
        else if (BE_PunctuationsAreSame(op, BE_Punctuation("*")))
        {
            rVal = BE_InstructionType_Mul;
        }
        else if (BE_PunctuationsAreSame(op, BE_Punctuation("/")))
        {
            rVal = BE_InstructionType_Div;
        }
        else if (BE_PunctuationsAreSame(op, BE_Punctuation("%")))
        {
            rVal = BE_InstructionType_Mod;
        }
        else if (BE_PunctuationsAreSame(op, BE_Punctuation("<")))
        {
            rVal = BE_InstructionType_Less;
        }
        else if (BE_PunctuationsAreSame(op, BE_Punctuation("<=")))
        {
            rVal = BE_InstructionType_LessOrEqual;
        }
        else if (BE_PunctuationsAreSame(op, BE_Punctuation("==")))
        {
            rVal = BE_InstructionType_Equal;
        }
        else if (BE_PunctuationsAreSame(op, BE_Punctuation(">")))
        {
            rVal = BE_InstructionType_Greater;
        }
        else if (BE_PunctuationsAreSame(op, BE_Punctuation(">=")))
        {
            rVal = BE_InstructionType_GreaterOrEqual;
        }
        else if (BE_PunctuationsAreSame(op, BE_Punctuation("!=")))
        {
            rVal = BE_InstructionType_NotEqual;
        }
        else if (BE_PunctuationsAreSame(op, BE_Punctuation("<<")))
        {
            rVal = BE_InstructionType_BitshiftLeft;
        }
        else if (BE_PunctuationsAreSame(op, BE_Punctuation(">>")))
        {
            rVal = BE_InstructionType_BitshiftRight;
        }
        else if (BE_PunctuationsAreSame(op, BE_Punctuation("&")))
        {
            rVal = BE_InstructionType_BitwiseAnd;
        }
        else if (BE_PunctuationsAreSame(op, BE_Punctuation("|")))
        {
            rVal = BE_InstructionType_BitwiseOr;
        }
        else if (BE_PunctuationsAreSame(op, BE_Punctuation("^")))
        {
            rVal = BE_InstructionType_BitwiseXOr;
        }
        else if (BE_PunctuationsAreSame(op, BE_Punctuation("&&")))
        {
            rVal = BE_InstructionType_LogicalAnd;
        }
        else if (BE_PunctuationsAreSame(op, BE_Punctuation("||")))
        {
            rVal = BE_InstructionType_LogicalOr;
        }
    }
    
    return rVal;
}

BEPrivateAPI BE_ArithmeticError BE_EvaluateConstantExpression(BE_Expressions* expressions, BE_ScalarValue* res, BE_Expression* expression)
{
    BE_ClearStructToZero(*res);
    
    BE_ArithmeticError rVal = BE_ArithmeticError_None;
    
    if (!BE_ExpressionHasPendingEndParenthesis(expression) 
        && expression->termsSentinel->next != expression->termsSentinel 
        && !BE_PunctuationIsValid(expression->termsSentinel->prev->binaryOperator.op))
    {
        for (BE_ExpressionTerm* term = expression->termsSentinel->next; term != expression->termsSentinel; )
        {
            while (term->unaryOperators && term->unaryOperators->precedence >= term->binaryOperator.precedence)
            {
                BEAssert(term->value.type == BE_OperatorValueType_Constant);
                
                BE_punct op = BE_PopUnaryOperator(expressions, term);
                term->value.constant = BE_PerformConstantUnaryOperator(expression->isFloat, op, term->value.constant);
            }
            
            BE_ExpressionTerm* lhs = term->prev;
            
            if (!term->unaryOperators && lhs != expression->termsSentinel)
            {
                // NOTE(Jens): `term` is RHS of expression, any unary operators of LHS is guaranteed to have lower precedence.
                
                if (lhs->binaryOperator.precedence >= term->binaryOperator.precedence)
                {
                    BEAssert(lhs->value.type == BE_OperatorValueType_Constant);
                    BEAssert(term->value.type == BE_OperatorValueType_Constant);
                    
                    lhs->value.type = BE_OperatorValueType_Constant;
                    lhs->value.constant = BE_PerformConstantBinaryOperator(&rVal, expression->isFloat, lhs->value.constant, lhs->binaryOperator.op, term->value.constant);
                    lhs->binaryOperator = term->binaryOperator;
                    
                    BE_ExpressionTerm* toFree = term;
                    
                    term->prev->next = term->next;
                    term->next->prev = term->prev;
                    
                    term = term->prev;
                    
                    toFree->next = expressions->freeTerms;
                    expressions->freeTerms = toFree;
                }
                else
                {
                    term = term->next;
                }
            }
            else
            {
                // NOTE(Jens): Still have unary operators to evaluate, can't evaluate previous term.
                term = term->next;
            }
        }
        
        // NOTE(Jens): Expression should've been reduced to a single entry at this point with no unary or binary operators pending evaluation.
        
        BEAssert(expression->termsSentinel->next != expression->termsSentinel);
        BEAssert(expression->termsSentinel->next->next == expression->termsSentinel);
        
        BE_ExpressionTerm* resultTerm = expression->termsSentinel->next;
        BEAssert(!resultTerm->unaryOperators);
        BEAssert(!BE_PunctuationIsValid(resultTerm->binaryOperator.op));
        
        *res = expression->termsSentinel->next->value.constant;
        
        BE_FreeExpression(expressions, expression);
    }
    
    return rVal;
}

BEPrivateAPI BE_punct BE_NonCompoundVersionOfOperator(BE_punct op)
{
    BE_punct rVal = BE_Punctuation_Invalid;
    
    // TODO(Jens): This could probably be simplified.
    
    if (BE_PunctuationsAreSame(op, BE_Punctuation("<<=")))
    {
        rVal = BE_Punctuation("<<");
    }
    else if (BE_PunctuationsAreSame(op, BE_Punctuation(">>=")))
    {
        rVal = BE_Punctuation(">>");
    }
    else if (BE_PunctuationsAreSame(op, BE_Punctuation("^=")))
    {
        rVal= BE_Punctuation("^");
    }
    else if (BE_PunctuationsAreSame(op, BE_Punctuation("|=")))
    {
        rVal = BE_Punctuation("|");
    }
    else if (BE_PunctuationsAreSame(op, BE_Punctuation("&=")))
    {
        rVal= BE_Punctuation("&");
    }
    else if (BE_PunctuationsAreSame(op, BE_Punctuation("%=")))
    {
        rVal = BE_Punctuation("%");
    }
    else if (BE_PunctuationsAreSame(op, BE_Punctuation("/=")))
    {
        rVal = BE_Punctuation("/");
    }
    else if (BE_PunctuationsAreSame(op, BE_Punctuation("*=")))
    {
        rVal = BE_Punctuation("*");
    }
    else if (BE_PunctuationsAreSame(op, BE_Punctuation("-=")))
    {
        rVal = BE_Punctuation("-");
    }
    else if (BE_PunctuationsAreSame(op, BE_Punctuation("+=")))
    {
        rVal = BE_Punctuation("+");
    }
    
    return rVal;
}

struct BE_NameMapNode
{
    BE_NameMapNode* left;
    BE_NameMapNode* right;
    
    BEu32 index;
    BE_String value;
};

BEPrivateAPI BEs32 BE_CompareStrings(BE_String lhs, BE_String rhs)
{
    BEs32 rVal = 0;
    
    if (lhs.count == rhs.count)
    {
        rVal = memcmp(lhs.characters, rhs.characters, lhs.count);
    }
    else if (lhs.count < rhs.count)
    {
        rVal = -1;
    }
    else
    {
        rVal = 1;
    }
    
    return rVal;
}

BEPrivateAPI BE_NameMapNode** BE_FindNameMapSlotFor(BE_NameMap* map, BE_String str)
{
    BE_NameMapNode** slot = &map->root;
    
    while (*slot)
    {
        BEs32 cmp = BE_CompareStrings(str, (*slot)->value);
        
        if (cmp < 0)
        {
            slot = &(*slot)->left;
        }
        else if (cmp > 0)
        {
            slot = &(*slot)->right;
        }
        else
        {
            break;
        }
    }
    
    return slot;
}

BEPrivateAPI BE_NameMapNode* BE_AcquireMappedName(BEAllocatorType allocator, BE_NameMap* map, BE_String str, BEbool* wasCreated)
{
    BE_NameMapNode** node = BE_FindNameMapSlotFor(map, str);
    
    if (!*node)
    {
        *node = BEAlloc(allocator, BE_NameMapNode);
        BE_ClearStructToZero(**node);
        
        (*node)->index = map->count++;
        (*node)->value = str;
        
        if (wasCreated)
        {
            *wasCreated = 1;
        }
    }
    else if (wasCreated)
    {
        *wasCreated = 0;
    }
    
    return *node;
}

BEPrivateAPI BE_NameMapNode* BE_FindMappedName(BE_NameMap* map, BE_String str)
{
    return *BE_FindNameMapSlotFor(map, str);
}

BEPrivateAPI BEbool BE_AddMappedName(BEAllocatorType allocator, BE_NameMap* map, BE_String str)
{
    BE_NameMapNode** node = BE_FindNameMapSlotFor(map, str);
    
    BEbool rVal = 0;
    
    if (!*node)
    {
        rVal = 1;
        *node = BEAlloc(allocator, BE_NameMapNode);
        BE_ClearStructToZero(**node);
        
        (*node)->index = map->count++;
        (*node)->value = str;
    }
    
    return rVal;
}

typedef struct BE_GlobalConstantNode BE_GlobalConstantNode;
struct BE_GlobalConstantNode
{
    BE_GlobalConstantNode* left;
    BE_GlobalConstantNode* right;
    BE_GlobalConstantNode* children;
    
    BE_String name;
    BEbool hasValue;
    BEs64 value;
};

BEPrivateAPI BE_GlobalConstantNode** BE_FindGlobalConstantSlot(BE_GlobalConstantNode** root, BE_String name)
{
    BE_GlobalConstantNode** slot = root;
    
    while (*slot)
    {
        BEs32 cmp = BE_CompareStrings(name, (*slot)->name);
        
        if (cmp < 0)
        {
            slot = &(*slot)->left;
        }
        else if (cmp > 0)
        {
            slot = &(*slot)->right;
        }
        else
        {
            break;
        }
    }
    
    return slot;
}



typedef enum BE_ParserMode BE_ParserMode;
enum BE_ParserMode
{
    BE_ParserMode_TopLevel,
    
    // NOTE(Jens): <breakpoint-statement> := 
    //               'breakpoint' '(' ')' ';'
    BE_ParserMode_Breakpoint_1,
    BE_ParserMode_Breakpoint_2,
    
    // NOTE(Jens): <print-statement> :=
    //               'print' '(' '<quoted-string>' ')' ';'
    //               'print' '(' '<integer-expression>' ')' ';'
    //               'print' '(' '<quoted-string>' ',' '<integer-expression>' ')' ';'
    BE_ParserMode_Print_1,
    BE_ParserMode_Print_2,
    BE_ParserMode_Print_Msg,
    BE_ParserMode_Print_IntegerExpression_End,
    
    // NOTE(Jens): <assert-statement> :=
    //               'assert' '(' '<integer-expression>' ')' ';'
    BE_ParserMode_Assert_1,
    BE_ParserMode_Assert_End,
    
    // NOTE(Jens): <plot-x|y-statement> :=
    //               'plot_x|y' '(' '<integer-expression>' ',' '<float-expression>' ')' ';'
    BE_ParserMode_Plot_X,
    BE_ParserMode_Plot_X_Comma,
    BE_ParserMode_Plot_X_End,
    BE_ParserMode_Plot_Y,
    BE_ParserMode_Plot_Y_Comma,
    BE_ParserMode_Plot_Y_End,
    
    // NOTE(Jens): <plot-xy-statement> :=
    //               'plot_xy' '(' '<integer-expression>' ',' '<integer-expression>' ',' '<integer-expression>' ')' ';'
    BE_ParserMode_Plot_XY_1,
    BE_ParserMode_Plot_XY_Comma_1,
    BE_ParserMode_Plot_XY_Comma_2,
    BE_ParserMode_Plot_XY_End,
    
    // NOTE(Jens): <plot-member-x|y> :=
    //               'plot_member_x|y' '(' '<integer-expression>' ',' '<member-path>' ')' ';'
    BE_ParserMode_PlotMember_X_1,
    BE_ParserMode_PlotMember_X_2,
    BE_ParserMode_PlotMember_X_End,
    
    BE_ParserMode_PlotMember_Y_1,
    BE_ParserMode_PlotMember_Y_2,
    BE_ParserMode_PlotMember_Y_End,
    
    // NOTE(Jens): <variable-declaration> :=
    //               'var' '<identifier>' ';'
    //               'var' '<identifier>' '=' '<integer-expression>' ';'
    BE_ParserMode_VariableDeclaration_1,
    BE_ParserMode_VariableDeclaration_2,
    
    // NOTE(Jens): '<typedef-declaration>' :=
    //               'typedef' '<type-declaration>' '<identifier>' ';'
    BE_ParserMode_TypedefIdentifier,
    
    // NOTE(Jens): <if-statement-begin> :=
    //               'if' '(' '<integer-expression>' ')' '{'
    //
    //             <if-statement> := '<if-statement-begin>' '<any-statement>'* '}'
    BE_ParserMode_If,
    BE_ParserMode_If_ConditionEnd,
    BE_ParserMode_If_End,
    
    // NOTE(Jens): <else-statement> :=
    //               '<if-statement>' 'else' '{'
    //               '<if-statement>' 'else' '<if-statement-begin>'
    BE_ParserMode_Else,
    BE_ParserMode_ElseIf,
    
    // NOTE(Jens): <for-loop-begin> :=
    //               'for' '(' ['<variable-declaration>'] ';' ['<integer-expression>'] ';' ['<variable-assignment>'] ')' '{'
    BE_ParserMode_For,
    BE_ParserMode_For_VarDecl,
    BE_ParserMode_For_VarDecl_1,
    BE_ParserMode_For_VarDecl_2,
    BE_ParserMode_For_VarDecl_End,
    BE_ParserMode_For_ConditionEnd,
    BE_ParserMode_For_Assignment,
    BE_ParserMode_For_Assignment_1,
    BE_ParserMode_For_Assignment_End,
    BE_ParserMode_For_End,
    
    // NOTE(Jens): <while-loop-begin> :=
    //               'while' '(' '<integer-expression>' ')' '{'
    BE_ParserMode_While,
    BE_ParserMode_While_EndCondition,
    BE_ParserMode_While_End,
    
    // NOTE(Jens): <do-loop-begin> :=
    //               'do' '{'
    BE_ParserMode_Do,
    
    // NOTE(Jens): Preprocessing directive(s)
    // {
    
    // NOTE(Jens): '#' ...
    BE_ParserMode_Preprocessor,
    
    // NOTE(Jens): '#' 'default' '(' 'byteorder' '=' 'little|big' ')' ';'
    BE_ParserMode_Default_1,
    BE_ParserMode_Default_2,
    BE_ParserMode_Default_ByteOrder_1,
    BE_ParserMode_Default_ByteOrder_2,
    BE_ParserMode_Default_ByteOrder_3,
    
    // }
    
    // NOTE(Jens): Can be either assignment (e.g. '@ += 4;') or member declaration (e.g. '@(0xfe) u(4) foo;')
    BE_ParserMode_At,
    
    // NOTE(Jens): '<writeable-symbol>' '<assignment-op>' '<integer-expression>' ';'
    BE_ParserMode_Assignment,
    
    // NOTE(Jens): Top-scope only:
    // {
    
    // NOTE(Jens): 'layout' '<identifier>' ';'
    BE_ParserMode_Layout,
    
    // NOTE(Jens): 'enum' ['anonymous'] ['identifier'] ['(' '<constant-integer-enum-expression>' ')'] '{' '<enum-member-list>'+ '}' ';' 
    BE_ParserMode_Enum, 
    BE_ParserMode_Enum_SizeOrBodyStart,
    BE_ParserMode_Enum_SizeEnd,
    BE_ParserMode_Enum_BodyStart,
    BE_ParserMode_Enum_MemberNameOrEnd,
    BE_ParserMode_Enum_MemberValue,
    BE_ParserMode_Enum_MemberValueExpression,
    BE_ParserMode_Enum_MemberValueExpression_ConstantSymbol,
    BE_ParserMode_Enum_MemberValueExpression_ConstantSymbol_1,
    
    // NOTE(Jens): 'import' '(' '<quoted-string>' ')' ';'
    BE_ParserMode_Import,
    BE_ParserMode_Import_Path,
    BE_ParserMode_Import_End,
    
    // NOTE(Jens): ['<address-specification>'] ['hidden'] '<type-declaration>' 'name' ['[' '<integer-expression>' ']'] ';'
    BE_ParserMode_Member_AddressEnd,
    BE_ParserMode_Member_Hidden,
    BE_ParserMode_Member_Name,
    BE_ParserMode_Member_NameEnd,
    BE_ParserMode_Member_ArrayCountEnd,
    
    // NOTE(Jens): 'struct' ['(' '<identifier>' ',' '<constant-integer-expression>' ')'] '<identifier>' ['(' '<param-list>' ')'] '{'
    BE_ParserMode_StructHeader,
    BE_ParserMode_StructHeader_Category,
    BE_ParserMode_StructHeader_Category_1,
    BE_ParserMode_StructHeader_Category_NumberEnd,
    BE_ParserMode_StructHeader_Name,
    BE_ParserMode_StructHeader_Params,
    BE_ParserMode_StructHeader_ParamName,
    BE_ParserMode_StructHeader_ParamName_1,
    BE_ParserMode_StructHeader_ParamSeperator,
    BE_ParserMode_StructHeader_End,
    
    // }
    
    // NOTE(Jens): End of scope expressions:
    // {
    
    // NOTE(Jens): 'struct' ... '{' ... '}' ';'
    BE_ParserMode_EndOfScope_Struct,
    
    // NOTE(Jens): '['else'] if' ... '{' ... '}'
    BE_ParserMode_EndOfScope_If,
    
    // NOTE(Jens): 'if' ... 'else' '{' ... '}'
    BE_ParserMode_EndOfScope_Else,
    
    // NOTE(Jens): 'for' ... '{' ... '}'
    BE_ParserMode_EndOfScope_For,
    
    // NOTE(Jens): 'do' '{' ... '}' 'while' '(' '<integer-expression>' ')' ';'
    BE_ParserMode_EndOfScope_DoWhile,
    BE_ParserMode_EndOfScope_DoWhile_1,
    BE_ParserMode_EndOfScope_DoWhile_ConditionEnd,
    
    // }
    
    // NOTE(Jens): Sub-expressions:
    // {
    
    
    BE_ParserMode_MemberAccess,
    BE_ParserMode_MemberAccess_Nested,
    BE_ParserMode_MemberAccess_ArrayIndex,
    BE_ParserMode_MemberAccess_ArrayIndex_End,
    
    // NOTE(Jens): 'has_type' '(' '<identifier>', '<integer-expression>' ')'
    BE_ParserMode_IntegerExpression_HasType,
    BE_ParserMode_IntegerExpression_HasType_Category,
    BE_ParserMode_IntegerExpression_HasType_Comma,
    BE_ParserMode_IntegerExpression_HasType_CategoryNumber,
    BE_ParserMode_IntegerExpression_HasType_CategoryNumberEnd,
    
    // NOTE(Jens): 'create_plot' '(' ')'
    BE_ParserMode_IntegerExpression_CreatePlot,
    BE_ParserMode_IntegerExpression_CreatePlot_End,
    
    // NOTE(Jens): 'array_count' '(' '<array-member-expression>' ')'
    BE_ParserMode_IntegerExpression_ArrayCount,
    BE_ParserMode_IntegerExpression_ArrayCount_End,
    
    // NOTE(Jens): 'load' '(' ['@' '(' '<integer-expression>' ')'] 'u|s' '(' '<integer-expression>' ')' ')'
    BE_ParserMode_IntegerExpression_Load,
    BE_ParserMode_IntegerExpression_Load_1,
    BE_ParserMode_IntegerExpression_Load_Address,
    BE_ParserMode_IntegerExpression_Load_Address_End,
    BE_ParserMode_IntegerExpression_Load_Type,
    BE_ParserMode_IntegerExpression_Load_Size,
    BE_ParserMode_IntegerExpression_Load_Size_End,
    BE_ParserMode_IntegerExpression_Load_End,
    
    // NOTE(Jens): 'sizeof' '(' ['<address-specification>'] '<struct-name>' ')'
    BE_ParserMode_IntegerExpression_SizeOf,
    BE_ParserMode_IntegerExpression_SizeOf_1,
    BE_ParserMode_IntegerExpression_SizeOf_Type,
    BE_ParserMode_IntegerExpression_SizeOf_End,
    
    // NOTE(Jens): '@' '(' ['external'] '<integer-expression>' ')'
    BE_ParserMode_AddressSpecification,
    BE_ParserMode_AddressSpecification_1,
    BE_ParserMode_AddressSpecification_2,
    
    // NOTE(Jens): '<array-scalar-member>' '[' '<integer-expression>' ']'
    // NOTE(Jens): '<struct-member>' '.'
    // NOTE(Jens): '<array-struct-member>' '[' '<integer-expression>' ']' '.'
    BE_ParserMode_IntegerExpression_MemberFetch,
    BE_ParserMode_IntegerExpression_MemberFetch_Nested,
    BE_ParserMode_IntegerExpression_MemberFetch_ArrayIndex,
    BE_ParserMode_IntegerExpression_MemberFetch_ArrayIndex_End,
    
    // NOTE(Jens): ('<unary-operator>'+ '<symbol>' '<binary-operator>')+ '<unary-operator>'+ '<symbol>'
    BE_ParserMode_IntegerExpression,
    
    BE_ParserMode_IntegerExpression_ConstantSymbol,
    BE_ParserMode_IntegerExpression_ConstantSymbol_1,
    
    // NOTE(Jens): ['alignas' '(' '<integer-expression>' ')'] 'u|s|f|raw|string|hidden' '(' '<integer-expression>' [',' '<display-type>'] ')' 
    // NOTE(Jens): ['alignas' '(' '<integer-expression>' ')'] '<enum-name>' ['(' '<integer-expression>' [',' '<display-type>'] ')']
    // NOTE(Jens): ['alignas' '(' '<integer-expression>' ')'] '<struct-name>' ['(' '<struct-argument-list>' ')']
    // NOTE(Jens): ['alignas' '(' '<integer-expression>' ')'] '<category-name>' '[' '<integer-expression>' ']' ['(' '<category-argument-list>' ')']
    BE_ParserMode_TypeDeclaration,
    BE_ParserMode_TypeDeclaration_Align,
    BE_ParserMode_TypeDeclaration_AlignEnd,
    BE_ParserMode_TypeDeclaration_Type,
    BE_ParserMode_ScalarDeclaration_Size,
    BE_ParserMode_ScalarDeclaration_SizeEnd,
    BE_ParserMode_ScalarDeclaration_DisplayOrByteOrder,
    BE_ParserMode_ScalarDeclaration_OptionalSize,
    BE_ParserMode_CategoryDeclaration_Start,
    BE_ParserMode_CategoryDeclaration_StartAmbiguous,
    BE_ParserMode_CategoryDeclaration_End,
    BE_ParserMode_TypeDeclaration_ParametersStart,
    BE_ParserMode_TypeDeclaration_ParameterName,
    BE_ParserMode_TypeDeclaration_ParameterSeparator,
    
    // }
    
    BE_ParserMode_StartEndOfStatement
};

typedef enum BE_StatementType BE_StatementType;
enum BE_StatementType
{
    BE_StatementType_Invalid_DuplicateVariableName = -46,
    BE_StatementType_Invalid_ExpectedArrayMember = -45,
    BE_StatementType_Invalid_ExpectedOpenParenthesisOrAssignmentOperator = -44,
    BE_StatementType_Invalid_ConstantExpression_ModZero = -43,
    BE_StatementType_Invalid_ConstantExpression_DivZero = -42,
    BE_StatementType_Invalid_NotEnoughArguments = -41,
    BE_StatementType_Invalid_TooManyArguments = -40,
    BE_StatementType_Invalid_DuplicateByteOrder = -39,
    BE_StatementType_Invalid_DuplicateDisplayKeyword = -38,
    BE_StatementType_Invalid_ExpectedMemberName = -37,
    BE_StatementType_Invalid_ExpectedCategoryName = -36,
    BE_StatementType_Invalid_ConstantLookupFailure = -35,
    BE_StatementType_Invalid_DuplicateStructName = -34,
    BE_StatementType_Invalid_DuplicateParameterName = -33,
    BE_StatementType_Invalid_InvalidEnumSize = -32,
    BE_StatementType_Invalid_ExpectedDisplayKeywordOrByteOrder = -31,
    BE_StatementType_Invalid_ExpectedType = -30,  
    BE_StatementType_Invalid_ExpectedStatementStart = -29,
    BE_StatementType_Invalid_ExpectedKeyword_s_or_u = -28,
    BE_StatementType_Invalid_ExpectedKeyword_var_or_semicolon = -27,
    BE_StatementType_Invalid_ExpectedKeyword_little_or_big = -26,
    BE_StatementType_Invalid_ExpectedKeyword_var = -25,
    BE_StatementType_Invalid_ExpectedKeyword_byteorder = -24,
    BE_StatementType_Invalid_ExpectedKeyword_default = -23,
    BE_StatementType_Invalid_ExpectedKeyword_while = -22,
    BE_StatementType_Invalid_ExpectedIdentifierOrOpenParenthesis = -21,
    BE_StatementType_Invalid_ExpectedStructName = -20,
    BE_StatementType_Invalid_ExpectedQuotedString = -19,
    BE_StatementType_Invalid_ExpectedIdentifier = -18,
    BE_StatementType_Invalid_ExpectedCommaOrCloseCurlyBracket = -17,
    BE_StatementType_Invalid_ExpectedCommaOrCloseParenthesis = -16,
    BE_StatementType_Invalid_ExpectedAssignmentOperator = -15,
    BE_StatementType_Invalid_ExpectedVariable = -14,
    BE_StatementType_Invalid_DuplicateTypedefs = -13,
    BE_StatementType_Invalid_DuplicateEnumMemberNames = -12,
    BE_StatementType_Invalid_IntegerExpression = -11,
    BE_StatementType_Invalid_ExpectedCloseCurlyBracket = -10,
    BE_StatementType_Invalid_ExpectedOpenCurlyBracket = -9,
    BE_StatementType_Invalid_ExpectedCloseParenthesis = -8,
    BE_StatementType_Invalid_ExpectedOpenParenthesis = -7,
    BE_StatementType_Invalid_ExpectedCloseSquareBracket = -6,
    BE_StatementType_Invalid_ExpectedOpenSquareBracket = -5,
    BE_StatementType_Invalid_ExpectedEquals = -4,
    BE_StatementType_Invalid_ExpectedDot = -3,
    BE_StatementType_Invalid_ExpectedComma = -2,
    BE_StatementType_Invalid_ExpectedSemicolon = -1,
    BE_StatementType_Invalid = 0,
    
    BE_StatementType_Breakpoint,
    BE_StatementType_Print,
    BE_StatementType_Assert,
    BE_StatementType_VariableDeclaration,
    BE_StatementType_TypedefDeclaration,
    BE_StatementType_Layout,
    BE_StatementType_EnumDeclaration,
    BE_StatementType_Import,
    BE_StatementType_DefaultDirective,
    BE_StatementType_StructHeader,
    BE_StatementType_Assignment,
    BE_StatementType_If,
    BE_StatementType_Else,
    BE_StatementType_For,
    BE_StatementType_While,
    BE_StatementType_Do,
    BE_StatementType_EndOf_Struct,
    BE_StatementType_EndOf_For,
    BE_StatementType_EndOf_While,
    BE_StatementType_EndOf_DoWhile,
    BE_StatementType_EndOf_If,
    BE_StatementType_EndOf_Else,
    BE_StatementType_MemberDeclaration,
    
    BE_StatementType_Plot_X,
    BE_StatementType_Plot_Y,
    BE_StatementType_Plot_XY,
    BE_StatementType_Plot_Member_X,
    BE_StatementType_Plot_Member_Y,
    
    BE_StatementType_End
};

BEPrivateAPI BE_ScalarBaseType BE_IdentifierToScalarBaseType(BEbool* success, BE_String identifier)
{
    BE_ScalarBaseType rVal;
    BE_ClearStructToZero(rVal); // NOTE(Jens): To keep MSVC C4701: "potentially uninitialized local variable 'rVal' used"-warning away.
    
    *success = 1;
    
    if (BE_StringsAreEqual(identifier, BE_StrLiteral("u")))
    {
        rVal = BE_ScalarBaseType_u;
    }
    else if (BE_StringsAreEqual(identifier, BE_StrLiteral("s")))
    {
        rVal = BE_ScalarBaseType_s;
    }
    else if (BE_StringsAreEqual(identifier, BE_StrLiteral("f")))
    {
        rVal = BE_ScalarBaseType_f;
    }
    else if (BE_StringsAreEqual(identifier, BE_StrLiteral("raw")))
    {
        rVal = BE_ScalarBaseType_raw;
    }
    else if (BE_StringsAreEqual(identifier, BE_StrLiteral("string")))
    {
        rVal = BE_ScalarBaseType_string;
    }
    else if (BE_StringsAreEqual(identifier, BE_StrLiteral("hidden")))
    {
        rVal = BE_ScalarBaseType_hidden;
    }
    else
    {
        *success = 0;
    }
    
    return rVal;
}


BEPrivateAPI BE_ScalarByteOrder BE_IdentifierToScalarByteOrder(BEbool* success, BE_String identifier)
{
    BE_ScalarByteOrder rVal;
    BE_ClearStructToZero(rVal); // NOTE(Jens): To keep MSVC C4701: "potentially uninitialized local variable 'rVal' used"-warning away.
    
    *success = 1;
    
    if (BE_StringsAreEqual(identifier, BE_StrLiteral("little")))
    {
        rVal = BE_ScalarByteOrder_little;
    }
    else if (BE_StringsAreEqual(identifier, BE_StrLiteral("big")))
    {
        rVal = BE_ScalarByteOrder_big;
    }
    else
    {
        *success = 0;
    }
    
    return rVal;
}

struct BE_EnumMemberList
{
    BE_EnumMemberList* prev;
    
    BE_String name;
    BEs64 value;
};

typedef enum
{
    BE_ParserScopeType_Struct,
    BE_ParserScopeType_If,
    BE_ParserScopeType_Else,
    BE_ParserScopeType_While,
    BE_ParserScopeType_For,
    BE_ParserScopeType_Do,
} BE_ParserScopeType;

struct BE_ParserScope
{
    BE_ParserScope* prev;
    BE_ParserScopeType type;
};

BEPrivateAPI BE_ParserScope* BE_PushParsingScope(BEAllocatorType allocator, BE_ParserScopes* scopes)
{
    BE_ParserScope* rVal = scopes->free;
    
    if (rVal)
    {
        scopes->free = scopes->free->prev;
    }
    else
    {
        rVal = BEAlloc(allocator, BE_ParserScope);
    }
    
    BE_ClearStructToZero(*rVal);
    
    rVal->prev = scopes->top;
    scopes->top = rVal;
    
    return rVal;
}

BEPrivateAPI void BE_PopParsingScope(BE_ParserScopes* scopes)
{
    BEAssert(scopes->top);
    
    BE_ParserScope* top = scopes->top;
    scopes->top = scopes->top->prev;
    
    top->prev = scopes->free;
    scopes->free = top;
}

typedef struct BE_ParsedMemberNode
{
    struct BE_ParsedMemberNode* next;
    
    BE_MemberDeclaration declaration;
} BE_ParsedMemberNode;

struct BE_LocalVariableScope
{
    BE_LocalVariableScope* parent;
    
    BEu32 countOffset;
    BE_NameMap names;
};


BEPrivateAPI BEu32 BE_FindLocalVariableNumber(BE_LocalVariables* localVariables, BE_String name)
{
    BEu32 rVal = 0;
    
    for (BE_LocalVariableScope* scope = localVariables->topScope; scope; scope = scope->parent)
    {
        BE_NameMapNode* node = BE_FindMappedName(&scope->names, name);
        if (node)
        {
            rVal = 1 + scope->countOffset + node->index;
            break;
        }
    }
    
    return rVal;
}

BEPrivateAPI BEu32 BE_TryAddLocalVariable(BEAllocatorType allocator, BE_LocalVariables* localVariables, BE_String name, BEbool* success)
{
    BEAssert(name.count);
    
    BEbool wasCreated;
    BE_NameMapNode* node = BE_AcquireMappedName(allocator, &localVariables->topScope->names, name, &wasCreated);
    
    *success = wasCreated;
    
    return localVariables->topScope->countOffset + node->index;
}

BEPrivateAPI BEu32 BE_AddLocalVariable(BEAllocatorType allocator, BE_LocalVariables* localVariables, BE_String name)
{
    BEbool wasCreated;
    BEu32 rVal = BE_TryAddLocalVariable(allocator, localVariables, name, &wasCreated);
    BEAssert(wasCreated);
    
    return rVal;
}

BEPrivateAPI void BE_PushLocalVariableScope(BEAllocatorType allocator, BE_LocalVariableScope** freelist, BE_LocalVariables* localVariables)
{
    BE_LocalVariableScope* newScope = *freelist;
    if (newScope)
    {
        *freelist = (*freelist)->parent;
    }
    else
    {
        newScope =  BEAlloc(allocator, BE_LocalVariableScope);
    }
    BE_ClearStructToZero(*newScope);
    
    if (localVariables->topScope)
    {
        newScope->countOffset = localVariables->topScope->countOffset + localVariables->topScope->names.count;
    }
    newScope->parent = localVariables->topScope;
    localVariables->topScope = newScope;
}

BEPrivateAPI void BE_PopLocalVariableScope(BE_LocalVariableScope** freelist, BE_LocalVariables* localVariables)
{
    BE_LocalVariableScope* toFree = localVariables->topScope;
    localVariables->topScope = localVariables->topScope->parent;
    
    toFree->parent = *freelist;
    *freelist = toFree;
}

struct BE_ParsedStructNode
{
    BE_ParsedStructNode* leftByName;
    BE_ParsedStructNode* rightByName;
    
    BE_ParsedStructNode* leftInCategory;
    BE_ParsedStructNode* rightInCategory;
    
    BE_ParsedStructNode* nextInOrder;
    
    BE_ParsedMemberNode* membersHead;
    BE_ParsedMemberNode* membersTail;
    
    BE_String name;
    BEu32 index;
    BEs64 categoryNumber;
    BEu32 memberCount;
    
    BE_LocalVariables localVariables;
    BE_NameMap parameters;
};

struct BE_ParsedCategoryNode
{
    BE_ParsedCategoryNode* leftByName;
    BE_ParsedCategoryNode* rightByName;
    
    BE_ParsedCategoryNode* nextInOrder;
    
    BE_ParsedStructNode* types;
    
    BE_String name;
    BEu32 index;
    BEu32 structCount;
    BEu32 paramCount;
};

struct BE_ParsedEnumNode
{
    BE_ParsedEnumNode* left;
    BE_ParsedEnumNode* right;
    
    BE_ParsedEnumNode* nextInOrder;
    
    BE_EnumMemberList* members;
    
    BE_String name;
    BEu32 index;
    BEu32 size;
};

BEPrivateAPI BE_ParsedEnumNode** BE_FindEnumSlot(BE_ParsedEnums* enums, BE_String name)
{
    BE_ParsedEnumNode** slot = &enums->root;
    while (*slot)
    {
        BEs32 cmp = BE_CompareStrings(name, (*slot)->name);
        if (cmp < 0)
        {
            slot = &(*slot)->left;
        }
        else if (cmp > 0)
        {
            slot = &(*slot)->right;
        }
        else
        {
            break;
        }
    }
    
    return slot;
}

BEPrivateAPI BE_ParsedEnumNode* BE_AddEnum(BEAllocatorType allocator, BE_ParsedEnums* enums, BE_String name, BEu32 size)
{
    BE_ParsedEnumNode** slot = BE_FindEnumSlot(enums, name);
    
    BE_ParsedEnumNode* rVal = 0;
    
    if (!*slot)
    {
        BE_ParsedEnumNode* newNode = BEAlloc(allocator, BE_ParsedEnumNode);
        BE_ClearStructToZero(*newNode);
        
        newNode->name = name;
        newNode->size = size;
        newNode->index = enums->count++;
        
        *slot = newNode;
        
        if (!enums->byOrderHead)
        {
            enums->byOrderHead = enums->byOrderTail = newNode;
        }
        else
        {
            enums->byOrderTail = enums->byOrderTail->nextInOrder = newNode;
        }
        
        rVal = *slot;
    }
    
    return rVal;
}

BEPrivateAPI BE_ParsedEnumNode* BE_FindEnum(BE_ParsedEnums* enums, BE_String name)
{
    return *BE_FindEnumSlot(enums, name);
}

BEPrivateAPI void BE_AddStructMember(BEAllocatorType allocator, BE_ParsedStructNode* structNode, BE_MemberDeclaration* declaration)
{
    BE_ParsedMemberNode* newNode = BEAlloc(allocator, BE_ParsedMemberNode);
    BE_ClearStructToZero(*newNode);
    
    declaration->memberIndex = structNode->memberCount++;
    newNode->declaration = *declaration;
    
    if (!structNode->membersHead)
    {
        structNode->membersHead = structNode->membersTail = newNode;
    }
    else
    {
        structNode->membersTail = structNode->membersTail->next = newNode;
    }
}

BEPrivateAPI BE_ParsedMemberNode* BE_FindStructMember(BE_ParsedStructNode* structNode, BE_String name)
{
    BE_ParsedMemberNode* rVal = 0;
    
    for (BE_ParsedMemberNode* it = structNode->membersHead; it; it = it->next)
    {
        if (BE_StringsAreEqual(it->declaration.name, name))
        {
            rVal = it;
            break;
        }
    }
    
    return rVal;
}

BEPrivateAPI BE_ParsedStructNode** BE_FindStructSlotByName(BE_ParsedStructs* structs, BE_String name)
{
    BE_ParsedStructNode** slot = &structs->byName;
    while (*slot)
    {
        BEs32 cmp = BE_CompareStrings(name, (*slot)->name);
        if (cmp < 0)
        {
            slot = &(*slot)->leftByName;
        }
        else if (cmp > 0)
        {
            slot = &(*slot)->rightByName;
        }
        else
        {
            break;
        }
    }
    
    return slot;
}

BEPrivateAPI BE_ParsedStructNode* BE_FindStruct(BE_ParsedStructs* structs, BE_String name)
{
    return *BE_FindStructSlotByName(structs, name);
}

BEPrivateAPI BE_ParsedStructNode* BE_AddStruct(BEAllocatorType allocator, BE_ParsedStructs* structs, BE_String name, BE_NameMap parameters)
{
    BE_ParsedStructNode** slotByName = BE_FindStructSlotByName(structs, name);
    
    BE_ParsedStructNode* rVal = 0;
    
    if (!*slotByName)
    {
        BE_ParsedStructNode* newNode = BEAlloc(allocator, BE_ParsedStructNode);
        BE_ClearStructToZero(*newNode);
        
        newNode->name = name;
        newNode->index = structs->structCount++;
        newNode->parameters = parameters;
        
        *slotByName = newNode;
        
        if (!structs->byOrderHead)
        {
            structs->byOrderHead = structs->byOrderTail = newNode;
        }
        else
        {
            structs->byOrderTail = structs->byOrderTail->nextInOrder = newNode;
        }
        
        rVal = newNode;
    }
    
    return rVal;
}

BEPrivateAPI BE_ParsedCategoryNode** BE_FindCategorySlotByName(BE_ParsedStructs* structs, BE_String name)
{
    BE_ParsedCategoryNode** slot = &structs->categoriesByName;
    while (*slot)
    {
        BEs32 cmp = BE_CompareStrings(name, (*slot)->name);
        if (cmp < 0)
        {
            slot = &(*slot)->leftByName;
        }
        else if (cmp > 0)
        {
            slot = &(*slot)->rightByName;
        }
        else
        {
            break;
        }
    }
    
    return slot;
}

BEPrivateAPI BE_ParsedCategoryNode* BE_AcquireCategory(BEAllocatorType allocator, BE_ParsedStructs* structs, BE_String name)
{
    BE_ParsedCategoryNode** slot = BE_FindCategorySlotByName(structs, name);
    
    if (!*slot)
    {
        *slot = BEAlloc(allocator, BE_ParsedCategoryNode);
        BE_ClearStructToZero(**slot);
        
        (*slot)->name = name;
        (*slot)->index = structs->categoryCount++;
        
        if (!structs->categoriesByOrderHead)
        {
            structs->categoriesByOrderHead = structs->categoriesByOrderTail = *slot;
        }
        else
        {
            structs->categoriesByOrderTail = structs->categoriesByOrderTail->nextInOrder = *slot;
        }
    }
    
    return *slot;
}

BEPrivateAPI BE_ParsedCategoryNode* BE_FindCategory(BE_ParsedStructs* structs, BE_String name)
{
    return *BE_FindCategorySlotByName(structs, name);
}

BEPrivateAPI BEbool BE_AddTypeToCategory(BE_ParsedStructNode* type, BE_ParsedCategoryNode* category, BEs64 categoryNumber)
{
    BE_ParsedStructNode** slot = &category->types;
    type->categoryNumber = categoryNumber;
    
    while (*slot)
    {
        if (categoryNumber < (*slot)->categoryNumber)
        {
            slot = &(*slot)->leftInCategory;
        }
        else if (categoryNumber > (*slot)->categoryNumber)
        {
            slot = &(*slot)->rightInCategory;
        }
        else
        {
            break;
        }
    }
    
    BEbool rVal = 0;
    
    if (!*slot)
    {
        rVal = 1;
        *slot = type;
        
        if (type == category->types)
        {
            category->paramCount = type->parameters.count;
        }
        else if (category->paramCount != type->parameters.count)
        {
            rVal = 0; // NOTE(Jens): Conflicting category parameter count.
        }
        
        ++category->structCount;
    }
    else
    {
        // NOTE(Jens): Duplicate category numbers.
    }
    
    return rVal;
}

struct BE_ParsedTypedefNode
{
    BE_ParsedTypedefNode* left;
    BE_ParsedTypedefNode* right;
    
    BEu32 index;
    BE_TypedefDeclaration def;
};

BEPrivateAPI BE_TypeDeclaration* BE_UnwrapTypedefs(BE_TypeDeclaration* type)
{
    BE_TypeDeclaration* it = type;
    
    for (;;)
    {
        if (it->variant == BE_TypeVariant_Typedef)
        {
            it = &it->typedefVariant.def->def.type;
        }
        else
        {
            break;
        }
    }
    
    return it;
}

BEPrivateAPI BE_TypeDeclaration* BE_TryGetScalarType(BE_TypeDeclaration* type)
{
    BE_TypeDeclaration* it = BE_UnwrapTypedefs(type);
    
    if (it->variant != BE_TypeVariant_Scalar)
    {
        it = 0;
    }
    
    return it;
}

BEPrivateAPI BEbool BE_TypeIsScalar(BE_TypeDeclaration type)
{
    return !!BE_TryGetScalarType(&type);
}

BEPrivateAPI BE_ParsedStructNode* BE_GetMemberStructType(BE_TypeDeclaration type)
{
    BE_ParsedStructNode* rVal = 0;
    
    BE_TypeDeclaration* it = &type;
    
    for (;;)
    {
        if (it->variant == BE_TypeVariant_Struct)
        {
            rVal = it->structVariant.type;
            break;
        }
        else if (it->variant == BE_TypeVariant_Typedef)
        {
            it = &it->typedefVariant.def->def.type;
        }
        else
        {
            break;
        }
    }
    
    return rVal;
}

BEPrivateAPI BE_ParsedTypedefNode** BE_FindTypedefSlot(BE_ParsedTypedefs* typedefs, BE_String name)
{
    BE_ParsedTypedefNode** slot = &typedefs->root;
    while (*slot)
    {
        BEs32 cmp = BE_CompareStrings(name, (*slot)->def.name);
        if (cmp < 0)
        {
            slot = &(*slot)->left;
        }
        else if (cmp > 0)
        {
            slot = &(*slot)->right;
        }
        else
        {
            break;
        }
    }
    
    return slot;
}

BEPrivateAPI BE_ParsedTypedefNode* BE_FindTypedef(BE_ParsedTypedefs* typedefs, BE_String name)
{
    return *BE_FindTypedefSlot(typedefs, name);
}

BEPrivateAPI BEbool BE_AddTypedef(BEAllocatorType allocator, BE_ParsedTypedefs* typedefs, BE_TypedefDeclaration def)
{
    BE_ParsedTypedefNode** slot = BE_FindTypedefSlot(typedefs, def.name);
    
    BEbool rVal = 0;
    
    if (!*slot)
    {
        rVal = 1;
        
        *slot = BEAlloc(allocator, BE_ParsedTypedefNode);
        BE_ClearStructToZero(**slot);
        
        (*slot)->def = def;
        (*slot)->index = typedefs->count++;
    }
    
    return rVal;
}

struct BE_ParsingMemberAccess
{
    BE_ParsingMemberAccess* prev;
    
    BE_ParsedMemberNode* activeMember;
    BE_MemberPath* path;
    
    BEbool expectArray;
    BEu32 returnMode;
};

BEPrivateAPI BE_ParsingMemberAccess* BE_PushMemberAccess(BEAllocatorType allocator, BE_ParsingMemberAccesses* memberAccesses, BE_MemberPath* path, BEu32 returnMode, BEbool expectArray)
{
    BE_ParsingMemberAccess* rVal = memberAccesses->free;
    if (rVal)
    {
        memberAccesses->free = memberAccesses->free->prev;
    }
    else
    {
        rVal = BEAlloc(allocator, BE_ParsingMemberAccess);
    }
    
    BE_ClearStructToZero(*rVal);
    rVal->returnMode = returnMode;
    rVal->path = path;
    rVal->prev = memberAccesses->top;
    rVal->expectArray = expectArray;
    memberAccesses->top = rVal;
    
    return rVal;
}

BEPrivateAPI void BE_PopMemberAccess(BE_ParsingMemberAccesses* memberAccesses)
{
    BEAssert(memberAccesses->top);
    
    BE_ParsingMemberAccess* toFree = memberAccesses->top;
    memberAccesses->top = memberAccesses->top->prev;
    
    toFree->prev = memberAccesses->free;
    memberAccesses->free = toFree;
}

struct BE_ParsingExpression
{
    BE_ParsingExpression* prev;
    
    BE_Expression* expression;
    BE_ParsedMemberNode* activeMemberAccess;
    BE_MemberPath activeMemberPath;
    
    BEu32 returnMode;
    BEbool isConstant;
};

BEPrivateAPI BE_ParsingExpression* BE_PushExpression(BEAllocatorType allocator, BE_ParsingExpressions* expressions, BEbool isConstant, BEbool isFloat, BE_Expression* expression, BEu32 returnMode)
{
    BE_ParsingExpression* rVal = expressions->free;
    if (rVal)
    {
        expressions->free = expressions->free->prev;
    }
    else
    {
        rVal = BEAlloc(allocator, BE_ParsingExpression);
    }
    
    BE_ClearStructToZero(*rVal);
    rVal->expression = expression;
    rVal->returnMode = returnMode;
    rVal->isConstant = isConstant;
    rVal->prev = expressions->top;
    expressions->top = rVal;
    
    BE_InitExpression(allocator, &expressions->expressions, expression, isFloat);
    
    return rVal;
}

BEPrivateAPI void BE_PopExpression(BE_ParsingExpressions* expressions)
{
    BEAssert(expressions->top);
    
    BE_ParsingExpression* toFree = expressions->top;
    expressions->top = expressions->top->prev;
    
    toFree->prev = expressions->free;
    expressions->free = toFree;
}

struct BE_ParsingLoadExpression
{
    BE_ParsingLoadExpression* prev;
    
    BE_Expression address;
    BE_Expression size;
    BEbool isSigned;
};

BEPrivateAPI BE_ParsingLoadExpression* BE_PushLoadExpression(BEAllocatorType allocator, BE_ParsingLoadExpressions* expressions)
{
    BE_ParsingLoadExpression* rVal = expressions->free;
    if (rVal)
    {
        expressions->free = expressions->free->prev;
    }
    else
    {
        rVal = BEAlloc(allocator, BE_ParsingLoadExpression);
    }
    
    BE_ClearStructToZero(*rVal);
    
    rVal->prev = expressions->top;
    expressions->top = rVal;
    
    return rVal;
}

BEPrivateAPI void BE_PopLoadExpression(BE_ParsingLoadExpressions* expressions)
{
    BEAssert(expressions->top);
    
    BE_ParsingLoadExpression* toFree = expressions->top;
    expressions->top = expressions->top->prev;
    
    toFree->prev = expressions->free;
    expressions->free = toFree;
}

struct BE_ImportStack
{
    BE_ImportStack* prev;
    BE_NameMapNode* nameNode;
};

BEPrivateAPI void BE_TryPushImport(BEAllocatorType allocator, BE_Imports* imports, BE_String file)
{
    BE_NameMapNode** importSlot = BE_FindNameMapSlotFor(&imports->imported, file);
    if (*importSlot)
    {
        // NOTE(Jens): Requested import has already been imported (each file is only loaded one time).
    }
    else
    {
        ++imports->depth;
        
        *importSlot = BEAlloc(allocator, BE_NameMapNode);
        BE_ClearStructToZero(**importSlot);
        
        (*importSlot)->index = imports->imported.count++;
        (*importSlot)->value = file;
        
        BE_ImportStack* stackEntry = imports->freeStack;
        if (stackEntry)
        {
            imports->freeStack = imports->freeStack->prev;
        }
        else
        {
            stackEntry = BEAlloc(allocator, BE_ImportStack);
        }
        BE_ClearStructToZero(*stackEntry);
        
        stackEntry->nameNode = *importSlot;
        stackEntry->prev = imports->top;
        imports->top = stackEntry;
    }
}

BEPrivateAPI BEbool BE_TryPopImport(BE_Imports* imports)
{
    BEbool rVal = 0;
    
    BE_ImportStack* toFree = imports->top;
    
    if (toFree)
    {
        --imports->depth;
        
        imports->top = imports->top->prev;
        toFree->prev = imports->freeStack;
        imports->freeStack = toFree;
        
        rVal = 1;
    }
    
    return rVal;
}

BEPrivateAPI BEbool BE_IsAssignmentOperator(BE_punct punctuation)
{
    return BE_PunctuationsAreSame(punctuation, BE_Punctuation("<<="))
        || BE_PunctuationsAreSame(punctuation, BE_Punctuation(">>="))
        || BE_PunctuationsAreSame(punctuation, BE_Punctuation("^="))
        || BE_PunctuationsAreSame(punctuation, BE_Punctuation("|="))
        || BE_PunctuationsAreSame(punctuation, BE_Punctuation("&="))
        || BE_PunctuationsAreSame(punctuation, BE_Punctuation("%="))
        || BE_PunctuationsAreSame(punctuation, BE_Punctuation("/="))
        || BE_PunctuationsAreSame(punctuation, BE_Punctuation("*="))
        || BE_PunctuationsAreSame(punctuation, BE_Punctuation("-="))
        || BE_PunctuationsAreSame(punctuation, BE_Punctuation("+="))
        || BE_PunctuationsAreSame(punctuation, BE_Punctuation("="));
}

BEPrivateAPI BEbool BE_IsValidOperator(BEbool isFloat, BE_punct punctuation)
{
    BEbool rVal;
    
    if (isFloat)
    {
        rVal = (   BE_PunctuationsAreSame(punctuation, BE_Punctuation("-"))
                || BE_PunctuationsAreSame(punctuation, BE_Punctuation("*"))
                || BE_PunctuationsAreSame(punctuation, BE_Punctuation("/"))
                || BE_PunctuationsAreSame(punctuation, BE_Punctuation("+"))
                || BE_PunctuationsAreSame(punctuation, BE_Punctuation("<"))
                || BE_PunctuationsAreSame(punctuation, BE_Punctuation("<="))
                || BE_PunctuationsAreSame(punctuation, BE_Punctuation(">"))
                || BE_PunctuationsAreSame(punctuation, BE_Punctuation(">="))
                || BE_PunctuationsAreSame(punctuation, BE_Punctuation("=="))
                || BE_PunctuationsAreSame(punctuation, BE_Punctuation("!=")));
    }
    else
    {
        rVal = (   BE_PunctuationsAreSame(punctuation, BE_Punctuation("!"))
                || BE_PunctuationsAreSame(punctuation, BE_Punctuation("~"))
                || BE_PunctuationsAreSame(punctuation, BE_Punctuation("-"))
                || BE_PunctuationsAreSame(punctuation, BE_Punctuation("*"))
                || BE_PunctuationsAreSame(punctuation, BE_Punctuation("/"))
                || BE_PunctuationsAreSame(punctuation, BE_Punctuation("%"))
                || BE_PunctuationsAreSame(punctuation, BE_Punctuation("+"))
                || BE_PunctuationsAreSame(punctuation, BE_Punctuation("<<"))
                || BE_PunctuationsAreSame(punctuation, BE_Punctuation(">>"))
                || BE_PunctuationsAreSame(punctuation, BE_Punctuation("<"))
                || BE_PunctuationsAreSame(punctuation, BE_Punctuation("<="))
                || BE_PunctuationsAreSame(punctuation, BE_Punctuation(">"))
                || BE_PunctuationsAreSame(punctuation, BE_Punctuation(">="))
                || BE_PunctuationsAreSame(punctuation, BE_Punctuation("=="))
                || BE_PunctuationsAreSame(punctuation, BE_Punctuation("!="))
                || BE_PunctuationsAreSame(punctuation, BE_Punctuation("&"))
                || BE_PunctuationsAreSame(punctuation, BE_Punctuation("^"))
                || BE_PunctuationsAreSame(punctuation, BE_Punctuation("|"))
                || BE_PunctuationsAreSame(punctuation, BE_Punctuation("&&"))
                || BE_PunctuationsAreSame(punctuation, BE_Punctuation("||")));
    }
    
    return rVal;
}

BEPrivateAPI BEbool BE_TryAddLiteralOrOperatorToExpression(BEAllocatorType allocator, BE_Expressions* expressions, BE_Expression* expression, BE_TokenType tokenType, BE_TokenData tokenData, BE_String token)
{
    BEbool rVal = 0;
    
    if (tokenType == BE_TokenType_Punctuation)
    {
        if (BE_IsValidOperator(expression->isFloat, tokenData.punctuation))
        {
            rVal = BE_PushOperator(allocator, expressions, expression, tokenData.punctuation);
        }
        else if (BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation("(")))
        {
            rVal = BE_PushParenthesis(expression, 1);
        }
        else if (BE_ExpressionHasPendingEndParenthesis(expression) && BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation(")")))
        {
            rVal= BE_PushParenthesis(expression, 0);
        }
    }
    else if (tokenType == BE_TokenType_NumberLiteral)
    {
        rVal = BE_PushConstantInteger(expression, tokenData.literalValue);
    }
    else if (expression->isFloat && tokenType == BE_TokenType_FloatLiteral)
    {
        rVal = BE_PushConstantFloat(expression, tokenData.literalFloatValue);
    }
    else if (tokenType == BE_TokenType_QuotedString)
    {
        BE_ParseNumberResult res = BE_QuotedStringAsUnsignedInteger(token.count, token.characters);
        if (res.success)
        {
            rVal = BE_PushConstantInteger(expression, res.uVal);
        }
    }
    else if (tokenType == BE_TokenType_Identifier)
    {
        if (BE_StringsAreEqual(token, BE_StrLiteral("abs")))
        {
            rVal = BE_PushOperator(allocator, expressions, expression, BE_Punctuation("abs"));
        }
    }
    
    return rVal;
}

BEPrivateAPI BE_ParserState BE_CreateParserState(BEAllocatorType allocator)
{
    BE_ParserState rVal;
    BE_ClearStructToZero(rVal);
    
    rVal.allocator = allocator;
    BE_PushLocalVariableScope(allocator, &rVal.freeLocalVariableScopes, &rVal.globalVariables);
    
    return rVal;
}

BEPrivateAPI BEbool BE_FeedParser(BE_ParserState* state, BE_StatementType* statementType, BE_Statement* statement, BE_TokenType tokenType, BE_TokenData tokenData, BEu64 tokenSize, char* tokenValue)
{
    if (tokenType == BE_TokenType_Whitespace || tokenType == BE_TokenType_LineComment || tokenType == BE_TokenType_MultilineComment)
    {
        return 0;
    }
    
    BE_ParsingExpressions* parsingExpressions = &state->parsingExpressions;
    BE_ParsingMemberAccesses* parsingMemberAccesses = &state->parsingMemberAccesses;
    BE_Expressions* expressions = &parsingExpressions->expressions;
    BEAllocatorType allocator = state->allocator;
    
#define InvalidStatement_(type) \
*statementType = BE_StatementType_Invalid_##type; \
state->mode = BE_ParserMode_TopLevel; \
rVal = 1
    
#define ExpectPunctuation_(name, str) \
if (!(tokenType == BE_TokenType_Punctuation && BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation(str)))) \
{ \
InvalidStatement_(Expected##name); \
} \
else
    
#define ExpectPunctuation_Comma()              ExpectPunctuation_(Comma, ",")
#define ExpectPunctuation_Dot()                ExpectPunctuation_(Dot, ".")
#define ExpectPunctuation_Semicolon()          ExpectPunctuation_(Semicolon, ";")
#define ExpectPunctuation_Equals()             ExpectPunctuation_(Equals, "=")
#define ExpectPunctuation_OpenParenthesis()    ExpectPunctuation_(OpenParenthesis, "(")
#define ExpectPunctuation_CloseParenthesis()   ExpectPunctuation_(CloseParenthesis, ")")
#define ExpectPunctuation_OpenCurlyBracket()   ExpectPunctuation_(OpenCurlyBracket, "{")
#define ExpectPunctuation_CloseCurlyBracket()  ExpectPunctuation_(CloseCurlyBracket, "}")
#define ExpectPunctuation_OpenSquareBracket()  ExpectPunctuation_(OpenSquareBracket, "[")
#define ExpectPunctuation_CloseSquareBracket() ExpectPunctuation_(CloseSquareBracket, "]")
    
#define ExpectAnyIdentifier() \
if (tokenType != BE_TokenType_Identifier) \
{ \
*statementType = BE_StatementType_Invalid_ExpectedIdentifier; \
state->mode = BE_ParserMode_TopLevel; \
rVal = 1; \
} \
else
    
#define ExpectKeyword_(keyword) \
if (!(tokenType == BE_TokenType_Identifier && BE_StringsAreEqual(BE_StrLiteral(#keyword), token))) \
{ \
InvalidStatement_(ExpectedKeyword_##keyword); \
} \
else
    
#define ExpectKeyword_var()       ExpectKeyword_(var)
#define ExpectKeyword_while()     ExpectKeyword_(while)
#define ExpectKeyword_default()   ExpectKeyword_(default)
#define ExpectKeyword_byteorder() ExpectKeyword_(byteorder)
    
#define ParseFloatExpression(expr, returnMode) \
BE_PushExpression(allocator, parsingExpressions, 0, 1, &(expr), returnMode); \
state->mode = BE_ParserMode_IntegerExpression
    
#define ParseIntegerExpression(expr, returnMode) \
BE_PushExpression(allocator, parsingExpressions, 0, 0, &(expr), returnMode); \
state->mode = BE_ParserMode_IntegerExpression
    
#define ParseConstantIntegerExpression(expr, returnMode) \
BE_PushExpression(allocator, parsingExpressions, 1, 0, &(expr), returnMode); \
state->mode = BE_ParserMode_IntegerExpression
    
#define ParseMemberAccess(path, returnMode) \
BE_PushMemberAccess(allocator, parsingMemberAccesses, path, returnMode, 0); \
state->mode = BE_ParserMode_MemberAccess;
    
#define ParseArrayMemberAccess(path, returnMode) \
BE_PushMemberAccess(allocator, parsingMemberAccesses, path, returnMode, 1); \
state->mode = BE_ParserMode_MemberAccess;
    
    BE_String token;
    token.count = tokenSize;
    token.characters = tokenValue;
    
    BEbool rVal = 0;
    
    BEbool previousStatementWasEndOfIf = state->previousStatementWasEndOfIf;
    state->previousStatementWasEndOfIf = 0;
    
    label_Restart:
    
    BE_LocalVariables* localVariables = (state->parsingStruct ? &state->parsingStruct->localVariables : &state->globalVariables);
    
    switch (state->mode)
    {
        case BE_ParserMode_TopLevel:
        {
            if (tokenType == BE_TokenType_EndOfTokens)
            {
                // NOTE(Jens): More tokens may come, but this is end of this file.
            }
            else if (tokenType == BE_TokenType_Identifier)
            {
                if (BE_StringsAreEqual(BE_StrLiteral("breakpoint"), token))
                {
                    state->mode = BE_ParserMode_Breakpoint_1;
                }
                else if (BE_StringsAreEqual(BE_StrLiteral("print"), token))
                {
                    state->mode = BE_ParserMode_Print_1;
                }
                else if (BE_StringsAreEqual(BE_StrLiteral("assert"), token))
                {
                    state->mode = BE_ParserMode_Assert_1;
                }
                else if (BE_StringsAreEqual(BE_StrLiteral("var"), token))
                {
                    state->mode = BE_ParserMode_VariableDeclaration_1;
                }
                else if (BE_StringsAreEqual(BE_StrLiteral("typedef"), token))
                {
                    state->mode = BE_ParserMode_TypeDeclaration;
                    state->typeDeclaration = &state->statement.typedefDeclaration.type;
                    state->typeDeclarationReturnMode = BE_ParserMode_TypedefIdentifier;
                }
                else if (BE_StringsAreEqual(BE_StrLiteral("if"), token))
                {
                    state->mode = BE_ParserMode_If;
                }
                else if (previousStatementWasEndOfIf && BE_StringsAreEqual(BE_StrLiteral("else"), token))
                {
                    state->mode = BE_ParserMode_Else;
                }
                else if (BE_StringsAreEqual(BE_StrLiteral("for"), token))
                {
                    state->mode = BE_ParserMode_For;
                }
                else if (BE_StringsAreEqual(BE_StrLiteral("while"), token))
                {
                    state->mode = BE_ParserMode_While;
                }
                else if (BE_StringsAreEqual(BE_StrLiteral("do"), token))
                {
                    state->mode = BE_ParserMode_Do;
                }
                else if (BE_StringsAreEqual(BE_StrLiteral("plot_x"), token))
                {
                    state->mode = BE_ParserMode_Plot_X;
                }
                else if (BE_StringsAreEqual(BE_StrLiteral("plot_y"), token))
                {
                    state->mode = BE_ParserMode_Plot_Y;
                }
                else if (BE_StringsAreEqual(BE_StrLiteral("plot_xy"), token))
                {
                    state->mode = BE_ParserMode_Plot_XY_1;
                }
                else if (!state->scopes.top && BE_StringsAreEqual(BE_StrLiteral("layout"), token))
                {
                    state->mode = BE_ParserMode_Layout;
                }
                else if (!state->scopes.top && BE_StringsAreEqual(BE_StrLiteral("enum"), token))
                {
                    state->mode = BE_ParserMode_Enum;
                }
                else if (!state->scopes.top && BE_StringsAreEqual(BE_StrLiteral("import"), token))
                {
                    state->mode = BE_ParserMode_Import;
                }
                else if (!state->scopes.top && BE_StringsAreEqual(BE_StrLiteral("struct"), token))
                {
                    state->mode = BE_ParserMode_StructHeader;
                }
                else if (state->parsingStruct && BE_StringsAreEqual(BE_StrLiteral("internal"), token))
                {
                    state->mode = BE_ParserMode_TypeDeclaration;
                    state->statement.memberDeclaration.isInternal = 1;
                    state->typeDeclaration = &state->statement.memberDeclaration.type;
                    state->typeDeclarationReturnMode = BE_ParserMode_Member_Name;
                }
                else if (state->parsingStruct && BE_StringsAreEqual(BE_StrLiteral("alignas"), token))
                {
                    state->mode = BE_ParserMode_TypeDeclaration_Align;
                    state->typeDeclaration = &state->statement.memberDeclaration.type;
                    state->typeDeclarationReturnMode = BE_ParserMode_Member_Name;
                }
                else if (state->parsingStruct && BE_StringsAreEqual(BE_StrLiteral("hidden"), token))
                {
                    state->mode = BE_ParserMode_Member_Hidden;
                }
                else if (state->parsingStruct && BE_StringsAreEqual(BE_StrLiteral("plot_array_x"), token))
                {
                    state->mode = BE_ParserMode_PlotMember_X_1;
                }
                else if (state->parsingStruct && BE_StringsAreEqual(BE_StrLiteral("plot_array_y"), token))
                {
                    state->mode = BE_ParserMode_PlotMember_Y_1;
                }
                else
                {
                    BEbool isGlobal = 0;
                    BEu32 variableNumber = (state->parsingStruct ? BE_FindLocalVariableNumber(&state->parsingStruct->localVariables, token) : 0);
                    if (!variableNumber)
                    {
                        isGlobal = 1;
                        variableNumber = BE_FindLocalVariableNumber(&state->globalVariables, token);
                    }
                    
                    if (variableNumber)
                    {
                        state->statement.assignment.isGlobal = isGlobal;
                        state->statement.assignment.variableIndex = BE_GlobalSymbol_Count + variableNumber - 1;
                        state->mode = BE_ParserMode_Assignment;
                    }
                    else if (state->parsingStruct)
                    {
                        // NOTE(Jens): Must be a member declaration if statement is valid.
                        
                        state->mode = BE_ParserMode_TypeDeclaration;
                        state->typeDeclaration = &state->statement.memberDeclaration.type;
                        state->typeDeclarationReturnMode = BE_ParserMode_Member_Name;
                        
                        goto label_TypeDeclaration;
                    }
                    else
                    {
                        InvalidStatement_(ExpectedStatementStart);
                    }
                }
            }
            else if (tokenType == BE_TokenType_Punctuation)
            {
                if (BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation("#")))
                {
                    state->mode = BE_ParserMode_Preprocessor;
                }
                else if (BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation("@")))
                {
                    if (!state->parsingStruct)
                    {
                        state->statement.assignment.isGlobal = 1;
                        state->statement.assignment.variableIndex = BE_GlobalSymbol_MemberOffset;
                        state->mode = BE_ParserMode_Assignment;
                    }
                    else
                    {
                        state->mode = BE_ParserMode_At;
                    }
                }
                else if (state->scopes.top && BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation("}")))
                {
                    BE_ParserScopeType scopeType = state->scopes.top->type;
                    BE_PopParsingScope(&state->scopes);
                    BE_PopLocalVariableScope(&state->freeLocalVariableScopes, localVariables);
                    
                    switch (scopeType)
                    {
                        BE_InvalidDefault;
                        
                        case BE_ParserScopeType_Struct:
                        {
                            state->mode = BE_ParserMode_EndOfScope_Struct;
                        } break;
                        
                        case BE_ParserScopeType_If:
                        {
                            *statementType = BE_StatementType_EndOf_If;
                            *statement = state->statement;
                            state->mode = BE_ParserMode_TopLevel;
                            rVal = 1;
                            
                            state->previousStatementWasEndOfIf = 1;
                        } break;
                        
                        case BE_ParserScopeType_Else:
                        {
                            *statementType = BE_StatementType_EndOf_Else;
                            *statement = state->statement;
                            state->mode = BE_ParserMode_TopLevel;
                            rVal = 1;
                        } break;
                        
                        case BE_ParserScopeType_For:
                        {
                            *statementType = BE_StatementType_EndOf_For;
                            *statement = state->statement;
                            state->mode = BE_ParserMode_TopLevel;
                            rVal = 1;
                        } break;
                        
                        case BE_ParserScopeType_While:
                        {
                            *statementType = BE_StatementType_EndOf_While;
                            *statement = state->statement;
                            state->mode = BE_ParserMode_TopLevel;
                            rVal = 1;
                        } break;
                        
                        case BE_ParserScopeType_Do:
                        {
                            state->mode = BE_ParserMode_EndOfScope_DoWhile;
                        } break;
                    }
                }
                else
                {
                    InvalidStatement_(ExpectedStatementStart);
                }
            }
            else
            {
                InvalidStatement_(ExpectedStatementStart);
            }
        } break;
        
        default:
        {
            BEAssert(state->mode >= BE_ParserMode_StartEndOfStatement);
            
            BE_StatementType type = (BE_StatementType)(state->mode - BE_ParserMode_StartEndOfStatement);
            
            if (tokenType == BE_TokenType_Punctuation && BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation(";")))
            {
                *statementType = type;
            }
            else
            {
                *statementType = BE_StatementType_Invalid_ExpectedSemicolon;
            }
            *statement = state->statement;
            state->mode = BE_ParserMode_TopLevel;
            rVal = 1;
            
            switch (*statementType)
            {
                case BE_StatementType_VariableDeclaration:
                {
                    BEbool wasCreated;
                    statement->variableDeclaration.index = BE_TryAddLocalVariable(allocator, localVariables, statement->variableDeclaration.name, &wasCreated);
                    
                    if (!wasCreated)
                    {
                        InvalidStatement_(DuplicateVariableName);
                    }
                } break;
                
                case BE_StatementType_EnumDeclaration:
                {
                    if (statement->enumDeclaration.name.count)
                    {
                        BE_ParsedEnumNode* enumNode = BE_AddEnum(allocator, &state->enums, statement->enumDeclaration.name, statement->enumDeclaration.size);
                        if (!enumNode)
                        {
                            *statementType = BE_StatementType_Invalid_DuplicateEnumMemberNames;
                        }
                        else
                        {
                            enumNode->members = statement->enumDeclaration.members;
                        }
                    }
                    
                    if (statement->enumDeclaration.members)
                    {
                        if (state->statement.enumDeclaration.isAnonymous || state->statement.enumDeclaration.name.count == 0)
                        {
                            BE_GlobalConstantNode** rootSlot = &state->globalConstants.root;
                            
                            for (BE_EnumMemberList* it = statement->enumDeclaration.members; it; it = it->prev)
                            {
                                BE_GlobalConstantNode** slot = BE_FindGlobalConstantSlot(rootSlot, it->name);
                                
                                BEAssert(*slot);
                                BEAssert(!(*slot)->hasValue);
                                
                                (*slot)->value = it->value;
                                (*slot)->hasValue = 1;
                            }
                        }
                        
                        if (state->statement.enumDeclaration.name.count)
                        {
                            BE_GlobalConstantNode** rootSlot = BE_FindGlobalConstantSlot(&state->globalConstants.root, statement->enumDeclaration.name);
                            BEAssert(*rootSlot);
                            
                            rootSlot = &(*rootSlot)->children;
                            
                            for (BE_EnumMemberList* it = statement->enumDeclaration.members; it; it = it->prev)
                            {
                                BE_GlobalConstantNode** slot = BE_FindGlobalConstantSlot(rootSlot, it->name);
                                
                                BEAssert(*slot); // NOTE(Jens): Duplicate enum member names shoud have been caught by now.
                                BEAssert(!(*slot)->hasValue);
                                
                                (*slot)->value = it->value;
                                (*slot)->hasValue = 1;
                            }
                        }
                    }
                } break;
                
                case BE_StatementType_TypedefDeclaration:
                {
                    if (!BE_AddTypedef(allocator, &state->typedefs, statement->typedefDeclaration))
                    {
                        *statementType = BE_StatementType_Invalid_DuplicateTypedefs;
                    }
                } break;
                
                case BE_StatementType_MemberDeclaration:
                {
                    BEAssert(state->parsingStruct);
                    BE_AddStructMember(allocator, state->parsingStruct, &statement->memberDeclaration);
                } break;
                
                case BE_StatementType_DefaultDirective:
                {
                    if (statement->defaultDirective.bigEndian)
                    {
                        state->defaultByteOrder = BE_ScalarByteOrder_big;
                    }
                    else
                    {
                        state->defaultByteOrder = BE_ScalarByteOrder_little;
                    }
                } break;
                
                case BE_StatementType_Import:
                {
                    BE_String name = statement->import.unescapedFile;
                    BE_TryPushImport(allocator, &state->imports, name);
                } break;
            }
        } break;
        
        case BE_ParserMode_Preprocessor:
        {
            ExpectKeyword_default()
            {
                state->mode = BE_ParserMode_Default_1;
            }
        } break;
        
        case BE_ParserMode_MemberAccess:
        {
            ExpectAnyIdentifier()
            {
                BE_ParsingMemberAccess* memberAccess = parsingMemberAccesses->top;
                BE_ParsedStructNode* structNode = state->parsingStruct;
                
                if (memberAccess->activeMember)
                {
                    structNode = BE_GetMemberStructType(memberAccess->activeMember->declaration.type);
                }
                
                BE_ParsedMemberNode* member = BE_FindStructMember(structNode, token);
                if (member)
                {
                    memberAccess->activeMember = member;
                    BE_AppendMemberPath(allocator, memberAccess->path, member->declaration.memberIndex);
                    
                    BEbool isArray = member->declaration.isArray;
                    BEbool isScalar = BE_TypeIsScalar(member->declaration.type);
                    
                    if (isScalar && (!isArray || memberAccess->expectArray))
                    {
                        if (!isArray && memberAccess->expectArray)
                        {
                            InvalidStatement_(ExpectedArrayMember);
                        }
                        else
                        {
                            state->mode = memberAccess->returnMode;
                            BE_PopMemberAccess(parsingMemberAccesses);
                        }
                    }
                    else if (isArray)
                    {
                        state->mode = BE_ParserMode_MemberAccess_ArrayIndex;
                    }
                    else
                    {
                        state->mode = BE_ParserMode_MemberAccess_Nested;
                    }
                }
                else
                {
                    InvalidStatement_(ExpectedMemberName);
                }
            }
        } break;
        case BE_ParserMode_MemberAccess_Nested:
        {
            ExpectPunctuation_Dot()
            {
                state->mode = BE_ParserMode_MemberAccess;
            }
        } break;
        case BE_ParserMode_MemberAccess_ArrayIndex:
        {
            ExpectPunctuation_OpenSquareBracket()
            {
                BE_ParsingMemberAccess* memberAccess = state->parsingMemberAccesses.top;
                ParseIntegerExpression(memberAccess->path->tail->arrayIndex, BE_ParserMode_MemberAccess_ArrayIndex_End);
            }
        } break;
        case BE_ParserMode_MemberAccess_ArrayIndex_End:
        {
            ExpectPunctuation_CloseSquareBracket()
            {
                BE_ParsingMemberAccess* memberAccess = state->parsingMemberAccesses.top;
                
                if (BE_TypeIsScalar(memberAccess->activeMember->declaration.type))
                {
                    state->mode = memberAccess->returnMode;
                    BE_PopMemberAccess(parsingMemberAccesses);
                }
                else
                {
                    state->mode = BE_ParserMode_MemberAccess_Nested;
                }
            }
        } break;
        
        case BE_ParserMode_At:
        {
            if (tokenType == BE_TokenType_Punctuation && BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation("(")))
            {
                state->addressSpecification.returnMode = BE_ParserMode_Member_AddressEnd;
                state->mode = BE_ParserMode_AddressSpecification_1;
            }
            else if (tokenType == BE_TokenType_Punctuation && BE_IsAssignmentOperator(tokenData.punctuation))
            {
                state->statement.assignment.isGlobal = 1;
                state->statement.assignment.variableIndex = BE_GlobalSymbol_MemberOffset;
                state->mode = BE_ParserMode_Assignment;
                
                state->statement.assignment.op = tokenData.punctuation;
                ParseIntegerExpression(state->statement.assignment.rhs, BE_ParserMode_StartEndOfStatement + BE_StatementType_Assignment);
            }
            else
            {
                InvalidStatement_(ExpectedOpenParenthesisOrAssignmentOperator);
            }
        } break;
        case BE_ParserMode_Member_AddressEnd:
        {
            state->statement.memberDeclaration.address = state->addressSpecification.expression;
            state->statement.memberDeclaration.isExternal = state->addressSpecification.isExternal;
            
            state->mode = BE_ParserMode_TypeDeclaration;
            state->typeDeclaration = &state->statement.memberDeclaration.type;
            state->typeDeclarationReturnMode = BE_ParserMode_Member_Name;
            
            goto label_TypeDeclaration;
        } break;
        case BE_ParserMode_Member_Hidden:
        {
            // NOTE(Jens): 'hidden' can be either 'hidden Type name;' or part of scalar declaration 'hidden(size) name;'
            
            state->statement.memberDeclaration.isHidden = 1;
            state->typeDeclaration = &state->statement.memberDeclaration.type;
            state->typeDeclarationReturnMode = BE_ParserMode_Member_Name;
            
            if (tokenType == BE_TokenType_Punctuation && BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation("(")))
            {
                state->typeDeclaration->variant = BE_TypeVariant_Scalar;
                state->typeDeclaration->scalarVariant.baseType = BE_ScalarBaseType_hidden;
                state->typeDeclaration->scalarVariant.byteOrder = state->defaultByteOrder;
                
                ParseIntegerExpression(state->typeDeclaration->scalarVariant.sizeExpression, BE_ParserMode_ScalarDeclaration_SizeEnd);
            }
            else
            {
                state->mode = BE_ParserMode_TypeDeclaration;
                goto label_TypeDeclaration;
            }
        } break;
        case BE_ParserMode_Member_Name:
        {
            ExpectAnyIdentifier()
            {
                state->statement.memberDeclaration.name = token;
                state->mode = BE_ParserMode_Member_NameEnd;
            }
        } break;
        case BE_ParserMode_Member_NameEnd:
        {
            if (tokenType == BE_TokenType_Punctuation && BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation("[")))
            {
                state->statement.memberDeclaration.isArray = 1;
                ParseIntegerExpression(state->statement.memberDeclaration.arrayCount, BE_ParserMode_Member_ArrayCountEnd);
            }
            else
            {
                state->mode = BE_ParserMode_StartEndOfStatement + BE_StatementType_MemberDeclaration;
                goto label_Restart;
            }
        } break;
        case BE_ParserMode_Member_ArrayCountEnd:
        {
            ExpectPunctuation_CloseSquareBracket()
            {
                state->mode = BE_ParserMode_StartEndOfStatement + BE_StatementType_MemberDeclaration;
            }
        } break;
        
        case BE_ParserMode_Plot_Y:
        case BE_ParserMode_Plot_X:
        {
            ExpectPunctuation_OpenParenthesis()
            {
                ParseIntegerExpression(state->statement.plot.plotId, state->mode + 1);
            }
        } break;
        case BE_ParserMode_Plot_X_Comma:
        case BE_ParserMode_Plot_Y_Comma:
        {
            ExpectPunctuation_Comma()
            {
                ParseFloatExpression(state->statement.plot.value_f64, state->mode + 1);
            }
        } break;
        case BE_ParserMode_Plot_X_End:
        {
            ExpectPunctuation_CloseParenthesis()
            {
                state->mode = BE_ParserMode_StartEndOfStatement + BE_StatementType_Plot_X;
            }
        } break;
        case BE_ParserMode_Plot_Y_End:
        {
            ExpectPunctuation_CloseParenthesis()
            {
                state->mode = BE_ParserMode_StartEndOfStatement + BE_StatementType_Plot_Y;
            }
        } break;
        
        case BE_ParserMode_Plot_XY_1:
        {
            ExpectPunctuation_OpenParenthesis()
            {
                ParseIntegerExpression(state->statement.plotXY.plotId, BE_ParserMode_Plot_XY_Comma_1);
            }
        } break;
        case BE_ParserMode_Plot_XY_Comma_1:
        {
            ExpectPunctuation_Comma()
            {
                ParseFloatExpression(state->statement.plotXY.x, BE_ParserMode_Plot_XY_Comma_2);
            }
        } break;
        case BE_ParserMode_Plot_XY_Comma_2:
        {
            ExpectPunctuation_Comma()
            {
                ParseFloatExpression(state->statement.plotXY.y, BE_ParserMode_Plot_XY_End);
            }
        } break;
        case BE_ParserMode_Plot_XY_End:
        {
            ExpectPunctuation_CloseParenthesis()
            {
                state->mode = BE_ParserMode_StartEndOfStatement + BE_StatementType_Plot_XY;
            }
        } break;
        
        case BE_ParserMode_PlotMember_X_1:
        case BE_ParserMode_PlotMember_Y_1:
        {
            ExpectPunctuation_OpenParenthesis()
            {
                ParseIntegerExpression(state->statement.plotMember.plotId, state->mode + 1);
            }
        } break;
        
        case BE_ParserMode_PlotMember_X_2:
        case BE_ParserMode_PlotMember_Y_2:
        {
#define Offset_End (BE_ParserMode_PlotMember_X_End - BE_ParserMode_PlotMember_X_2)
            
            ExpectPunctuation_Comma()
            {
                ParseArrayMemberAccess(&state->statement.plotMember.member, state->mode + Offset_End);
            }
            
#undef Offset_End
        } break;
        
        case BE_ParserMode_PlotMember_X_End:
        {
            ExpectPunctuation_CloseParenthesis()
            {
                state->mode = BE_ParserMode_StartEndOfStatement + BE_StatementType_Plot_Member_X;
            }
        } break;
        case BE_ParserMode_PlotMember_Y_End:
        {
            ExpectPunctuation_CloseParenthesis()
            {
                state->mode = BE_ParserMode_StartEndOfStatement + BE_StatementType_Plot_Member_Y;
            }
        } break;
        
        case BE_ParserMode_If:
        {
            ExpectPunctuation_OpenParenthesis()
            {
                ParseIntegerExpression(state->statement.ifStatement.condition, BE_ParserMode_If_ConditionEnd);
            }
        } break;
        case BE_ParserMode_If_ConditionEnd:
        {
            ExpectPunctuation_CloseParenthesis()
            {
                state->mode = BE_ParserMode_If_End;
            }
        } break;
        case BE_ParserMode_If_End:
        {
            ExpectPunctuation_OpenCurlyBracket()
            {
                *statementType = BE_StatementType_If;
                *statement = state->statement;
                state->mode = BE_ParserMode_TopLevel;
                rVal = 1;
                
                BE_PushParsingScope(allocator, &state->scopes)->type = BE_ParserScopeType_If;
                BE_PushLocalVariableScope(allocator, &state->freeLocalVariableScopes, localVariables);
            }
        } break;
        
        case BE_ParserMode_Else:
        {
            if (tokenType == BE_TokenType_Identifier && BE_StringsAreEqual(token, BE_StrLiteral("if")))
            {
                state->mode = BE_ParserMode_If;
                state->statement.ifStatement.isElseIf = 1;
            }
            else ExpectPunctuation_OpenCurlyBracket()
            {
                *statementType = BE_StatementType_Else;
                *statement = state->statement;
                state->mode = BE_ParserMode_TopLevel;
                rVal = 1;
                
                BE_PushParsingScope(allocator, &state->scopes)->type = BE_ParserScopeType_Else;
                BE_PushLocalVariableScope(allocator, &state->freeLocalVariableScopes, localVariables);
            }
        } break;
        
        // NOTE(Jens): 'for'
        case BE_ParserMode_For: 
        {
            ExpectPunctuation_OpenParenthesis()
            {
                BE_PushLocalVariableScope(allocator, &state->freeLocalVariableScopes, localVariables);
                state->mode = BE_ParserMode_For_VarDecl;
            }
        } break;
        // NOTE(Jens): 'for' '('
        case BE_ParserMode_For_VarDecl: 
        {
            if (tokenType == BE_TokenType_Identifier && BE_StringsAreEqual(BE_StrLiteral("var"), token))
            {
                state->mode = BE_ParserMode_For_VarDecl_1;
            }
            else if (tokenType == BE_TokenType_Punctuation && BE_PunctuationsAreSame(BE_Punctuation(";"), tokenData.punctuation))
            {
                ParseIntegerExpression(state->statement.forLoop.endCondition, BE_ParserMode_For_ConditionEnd);
            }
            else
            {
                InvalidStatement_(ExpectedKeyword_var_or_semicolon);
            }
        } break;
        // NOTE(Jens): 'for' (' 'var'
        case BE_ParserMode_For_VarDecl_1: 
        {
            ExpectAnyIdentifier()
            {
                state->statement.forLoop.variable.name = token;
                state->mode = BE_ParserMode_For_VarDecl_2;
            }
        } break;
        // NOTE(Jens): 'for' (' 'var' '<identifier>'
        case BE_ParserMode_For_VarDecl_2: 
        {
            if (tokenType == BE_TokenType_Punctuation && BE_PunctuationsAreSame(BE_Punctuation("="), tokenData.punctuation))
            {
                ParseIntegerExpression(state->statement.forLoop.variable.initializationExpression, BE_ParserMode_For_VarDecl_End);
            }
            else
            {
                state->mode = BE_ParserMode_For_VarDecl_End;
                goto label_ForVarDecl_End;
            }
        } break;
        // NOTE(Jens): 'for' (' 'var' '<identifier>' ['=' '<integer-expression>']
        case BE_ParserMode_For_VarDecl_End: 
        {
            label_ForVarDecl_End:
            
            ExpectPunctuation_Semicolon()
            {
                state->statement.forLoop.variable.index = BE_AddLocalVariable(allocator, localVariables, state->statement.forLoop.variable.name);
                ParseIntegerExpression(state->statement.forLoop.endCondition, BE_ParserMode_For_ConditionEnd);
            }
        } break;
        // NOTE(Jens): 'for' (' 'var' '<identifier>' ['=' '<integer-expression>'] ';' '<integer-expression>'
        case BE_ParserMode_For_ConditionEnd: 
        {
            ExpectPunctuation_Semicolon()
            {
                state->mode = BE_ParserMode_For_Assignment;
            }
        } break;
        // NOTE(Jens): 'for' (' 'var' '<identifier>' ['=' '<integer-expression>'] ';' '<integer-expression>' ';'
        case BE_ParserMode_For_Assignment:
        {
            if (tokenType == BE_TokenType_Punctuation && BE_PunctuationsAreSame(BE_Punctuation(")"), tokenData.punctuation))
            {
                state->mode = BE_ParserMode_For_End;
            }
            else
            {
                if (tokenType == BE_TokenType_Punctuation && BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation("@")))
                {
                    state->statement.forLoop.assignment.isGlobal = 1;
                    state->statement.forLoop.assignment.variableIndex = BE_GlobalSymbol_MemberOffset;
                    state->mode = BE_ParserMode_For_Assignment_1;
                }
                else if (tokenType == BE_TokenType_Identifier)
                {
                    BEbool isGlobal = 0;
                    BEu32 variableNumber = (state->parsingStruct ? BE_FindLocalVariableNumber(&state->parsingStruct->localVariables, token) : 0);
                    if (!variableNumber)
                    {
                        isGlobal = 1;
                        variableNumber = BE_FindLocalVariableNumber(&state->globalVariables, token);
                    }
                    
                    if (variableNumber)
                    {
                        state->statement.forLoop.assignment.isGlobal = isGlobal;
                        state->statement.forLoop.assignment.variableIndex = BE_GlobalSymbol_Count + variableNumber - 1;
                        state->mode = BE_ParserMode_For_Assignment_1;
                    }
                    else
                    {
                        InvalidStatement_(ExpectedVariable);
                    }
                }
            }
        } break;
        case BE_ParserMode_For_Assignment_1:
        {
            if (tokenType == BE_TokenType_Punctuation &&
                (BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation("<<="))
                 || BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation(">>="))
                 || BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation("^="))
                 || BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation("|="))
                 || BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation("&="))
                 || BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation("%="))
                 || BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation("/="))
                 || BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation("*="))
                 || BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation("-="))
                 || BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation("+="))
                 || BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation("="))))
            {
                state->statement.forLoop.assignment.op = tokenData.punctuation;
                ParseIntegerExpression(state->statement.forLoop.assignment.rhs, BE_ParserMode_For_Assignment_End);
            }
            else
            {
                InvalidStatement_(ExpectedAssignmentOperator);
            }
        } break;
        case BE_ParserMode_For_Assignment_End:
        {
            ExpectPunctuation_CloseParenthesis()
            {
                state->mode = BE_ParserMode_For_End;
            }
        } break;
        case BE_ParserMode_For_End:
        {
            *statementType = BE_StatementType_For;
            *statement = state->statement;
            state->mode = BE_ParserMode_TopLevel;
            rVal = 1;
            
            BE_PushParsingScope(allocator, &state->scopes)->type = BE_ParserScopeType_For;
        } break;
        
        case BE_ParserMode_While:
        {
            ExpectPunctuation_OpenParenthesis()
            {
                ParseIntegerExpression(state->statement.whileLoop.condition, BE_ParserMode_While_EndCondition);
            }
        } break;
        case BE_ParserMode_While_EndCondition:
        {
            ExpectPunctuation_CloseParenthesis()
            {
                state->mode = BE_ParserMode_While_End;
            }
        } break;
        case BE_ParserMode_While_End:
        {
            ExpectPunctuation_OpenCurlyBracket()
            {
                *statementType = BE_StatementType_While;
                *statement = state->statement;
                state->mode = BE_ParserMode_TopLevel;
                rVal = 1;
                
                BE_PushParsingScope(allocator, &state->scopes)->type = BE_ParserScopeType_While;
                BE_PushLocalVariableScope(allocator, &state->freeLocalVariableScopes, localVariables);
            }
        } break;
        
        case BE_ParserMode_Do:
        {
            ExpectPunctuation_OpenCurlyBracket()
            {
                *statementType = BE_StatementType_Do;
                *statement = state->statement;
                state->mode = BE_ParserMode_TopLevel;
                rVal = 1;
                
                BE_PushParsingScope(allocator, &state->scopes)->type = BE_ParserScopeType_Do;
                BE_PushLocalVariableScope(allocator, &state->freeLocalVariableScopes, localVariables);
            }
        } break;
        
        case BE_ParserMode_Default_1:
        {
            ExpectPunctuation_OpenParenthesis()
            {
                state->mode = BE_ParserMode_Default_2;
            }
        } break;
        case BE_ParserMode_Default_2:
        {
            ExpectKeyword_byteorder()
            {
                state->mode = BE_ParserMode_Default_ByteOrder_1;
            }
        } break;
        case BE_ParserMode_Default_ByteOrder_1:
        {
            ExpectPunctuation_Equals()
            {
                state->mode = BE_ParserMode_Default_ByteOrder_2;
            }
        } break;
        case BE_ParserMode_Default_ByteOrder_2:
        {
            if (tokenType == BE_TokenType_Identifier && BE_StringsAreEqual(BE_StrLiteral("little"),  token))
            {
                state->statement.defaultDirective.bigEndian = 0;
                state->mode = BE_ParserMode_Default_ByteOrder_3;
            }
            else if (tokenType == BE_TokenType_Identifier && BE_StringsAreEqual(BE_StrLiteral("big"),  token))
            {
                state->statement.defaultDirective.bigEndian = 1;
                state->mode = BE_ParserMode_Default_ByteOrder_3;
            }
            else
            {
                InvalidStatement_(ExpectedKeyword_little_or_big);
            }
        } break;
        case BE_ParserMode_Default_ByteOrder_3:
        {
            ExpectPunctuation_CloseParenthesis()
            {
                state->mode = BE_ParserMode_StartEndOfStatement + BE_StatementType_DefaultDirective;
            }
        } break;
        
        case BE_ParserMode_Breakpoint_1:
        {
            ExpectPunctuation_OpenParenthesis()
            {
                state->mode = BE_ParserMode_Breakpoint_2;
            }
        } break;
        case BE_ParserMode_Breakpoint_2:
        {
            ExpectPunctuation_CloseParenthesis()
            {
                state->mode = BE_ParserMode_StartEndOfStatement + BE_StatementType_Breakpoint;
            }
        } break;
        
        case BE_ParserMode_Print_1:
        {
            ExpectPunctuation_OpenParenthesis()
            {
                state->mode = BE_ParserMode_Print_2;
            }
        } break;
        case BE_ParserMode_Print_2:
        {
            if (tokenType == BE_TokenType_QuotedString)
            {
                state->mode = BE_ParserMode_Print_Msg;
                state->statement.print.message = token;
            }
            else
            {
                ParseIntegerExpression(state->statement.print.expression, BE_ParserMode_Print_IntegerExpression_End);
                goto label_IntegerExpression;
            }
        } break;
        case BE_ParserMode_Print_Msg:
        {
            if (tokenType == BE_TokenType_Punctuation && BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation(",")))
            {
                ParseIntegerExpression(state->statement.print.expression, BE_ParserMode_Print_IntegerExpression_End);
            }
            else if (tokenType == BE_TokenType_Punctuation && BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation(")")))
            {
                state->mode = BE_ParserMode_StartEndOfStatement + BE_StatementType_Print;
            }
            else
            {
                InvalidStatement_(ExpectedCommaOrCloseParenthesis);
            }
        } break;
        case BE_ParserMode_Print_IntegerExpression_End:
        {
            ExpectPunctuation_CloseParenthesis()
            {
                state->mode = BE_ParserMode_StartEndOfStatement + BE_StatementType_Print;
            }
        } break;
        
        case BE_ParserMode_Assert_1:
        {
            ExpectPunctuation_OpenParenthesis()
            {
                ParseIntegerExpression(state->statement.assert.expression, BE_ParserMode_Assert_End);
            }
        } break;
        case BE_ParserMode_Assert_End:
        {
            ExpectPunctuation_CloseParenthesis()
            {
                state->mode = BE_ParserMode_StartEndOfStatement + BE_StatementType_Assert;
            }
        } break;
        
        case BE_ParserMode_VariableDeclaration_1:
        {
            ExpectAnyIdentifier()
            {
                state->statement.variableDeclaration.name = token;
                state->mode = BE_ParserMode_VariableDeclaration_2;
            }
        } break;
        case BE_ParserMode_VariableDeclaration_2:
        {
            if (tokenType == BE_TokenType_Punctuation && BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation("=")))
            {
                ParseIntegerExpression(state->statement.variableDeclaration.initializationExpression, BE_ParserMode_StartEndOfStatement + BE_StatementType_VariableDeclaration);
            }
            else
            {
                state->mode = BE_ParserMode_StartEndOfStatement + BE_StatementType_VariableDeclaration;
                goto label_Restart;
            }
        } break;
        
        case BE_ParserMode_TypedefIdentifier:
        {
            ExpectAnyIdentifier()
            {
                state->statement.typedefDeclaration.name = token;
                state->mode = BE_ParserMode_StartEndOfStatement + BE_StatementType_TypedefDeclaration;
            }
        } break;
        
        case BE_ParserMode_Layout:
        {
            ExpectAnyIdentifier()
            {
                BE_ParsedStructNode* structNode = BE_FindStruct(&state->structs, token);
                if (structNode)
                {
                    state->statement.layout.typeIndex = structNode->index;
                    state->statement.layout.isInImportedFile = !!state->imports.top;
                    state->mode = BE_ParserMode_StartEndOfStatement + BE_StatementType_Layout;
                }
                else
                {
                    InvalidStatement_(ExpectedStructName);
                }
            }
        } break;
        
        case BE_ParserMode_Assignment:
        {
            if (tokenType == BE_TokenType_Punctuation && BE_IsAssignmentOperator(tokenData.punctuation))
            {
                state->statement.assignment.op = tokenData.punctuation;
                ParseIntegerExpression(state->statement.assignment.rhs, BE_ParserMode_StartEndOfStatement + BE_StatementType_Assignment);
            }
            else
            {
                InvalidStatement_(ExpectedAssignmentOperator);
            }
        } break;
        
        case BE_ParserMode_Enum:
        {
            if (tokenType == BE_TokenType_Identifier)
            {
                if (!state->statement.enumDeclaration.isAnonymous && BE_StringsAreEqual(BE_StrLiteral("anonymous"), token))
                {
                    state->statement.enumDeclaration.isAnonymous = 1;
                }
                else
                {
                    state->statement.enumDeclaration.name = token;
                    state->mode = BE_ParserMode_Enum_SizeOrBodyStart;
                }
            }
            else
            {
                goto label_Enum_BodyStart;
            }
        } break;
        case BE_ParserMode_Enum_SizeOrBodyStart:
        {
            if (tokenType == BE_TokenType_Punctuation && BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation("(")))
            {
                ParseConstantIntegerExpression(state->tmpExpression, BE_ParserMode_Enum_SizeEnd);
            }
            else
            {
                goto label_Enum_BodyStart;
            }
        } break;
        case BE_ParserMode_Enum_SizeEnd:
        {
            ExpectPunctuation_CloseParenthesis()
            {
                BE_ScalarValue res;
                BE_ArithmeticError err = BE_EvaluateConstantExpression(expressions, &res, &state->tmpExpression);
                if (err || res.i <= 0 || res.i > 8)
                {
                    InvalidStatement_(InvalidEnumSize);
                }
                else
                {
                    state->statement.enumDeclaration.size = (BEu32)res.i;
                    state->mode = BE_ParserMode_Enum_BodyStart;
                }
            }
        } break;
        case BE_ParserMode_Enum_BodyStart:
        {
            label_Enum_BodyStart:
            
            ExpectPunctuation_OpenCurlyBracket()
            {
                state->mode = BE_ParserMode_Enum_MemberNameOrEnd;
            }
        } break;
        case BE_ParserMode_Enum_MemberNameOrEnd:
        {
            if (tokenType == BE_TokenType_Punctuation && BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation("}")))
            {
                state->mode = BE_ParserMode_StartEndOfStatement + BE_StatementType_EnumDeclaration;
            }
            else if (tokenType == BE_TokenType_Identifier)
            {
                BE_EnumMemberList* newMember = BEAlloc(allocator, BE_EnumMemberList);
                BE_ClearStructToZero(*newMember);
                
                newMember->name = token;
                if (state->statement.enumDeclaration.members)
                {
                    newMember->value = state->statement.enumDeclaration.members->value + 1;
                }
                
                state->mode = BE_ParserMode_Enum_MemberValue;
                
                for (BE_EnumMemberList* it = state->statement.enumDeclaration.members; it; it = it->prev)
                {
                    if (BE_StringsAreEqual(it->name, newMember->name))
                    {
                        // NOTE(Jens): This enum has a member with same name.
                        InvalidStatement_(DuplicateEnumMemberNames);
                    }
                }
                
                newMember->prev = state->statement.enumDeclaration.members;
                state->statement.enumDeclaration.members = newMember;
                ++state->statement.enumDeclaration.memberCount;
                
                if (!state->globalConstants.root)
                {
                    state->globalConstants.root = BEAlloc(allocator, BE_GlobalConstantNode);
                    BE_ClearStructToZero(*state->globalConstants.root);
                }
                
                if (state->statement.enumDeclaration.isAnonymous || state->statement.enumDeclaration.name.count == 0)
                {
                    BE_GlobalConstantNode** rootSlot = &state->globalConstants.root;
                    BE_GlobalConstantNode** slot = BE_FindGlobalConstantSlot(rootSlot, newMember->name);
                    if (*slot)
                    {
                        // NOTE(Jens): Another enum has a member with same name. Maybe allow if value is same?
                        InvalidStatement_(DuplicateEnumMemberNames);
                    }
                    else
                    {
                        *slot = BEAlloc(allocator, BE_GlobalConstantNode);
                        BE_ClearStructToZero(**slot);
                        
                        (*slot)->name = newMember->name;
                    }
                }
                
                if (state->statement.enumDeclaration.name.count)
                {
                    BE_GlobalConstantNode** rootSlot = BE_FindGlobalConstantSlot(&state->globalConstants.root, state->statement.enumDeclaration.name);
                    
                    if (!*rootSlot)
                    {
                        BEAssert(state->statement.enumDeclaration.memberCount == 1);
                        *rootSlot = BEAlloc(allocator, BE_GlobalConstantNode);
                        BE_ClearStructToZero(**rootSlot);
                        
                        (*rootSlot)->name = state->statement.enumDeclaration.name;
                    }
                    
                    rootSlot = &(*rootSlot)->children;
                    BE_GlobalConstantNode** slot = BE_FindGlobalConstantSlot(rootSlot, newMember->name);
                    if (*slot)
                    {
                        InvalidStatement_(DuplicateEnumMemberNames);
                    }
                    else
                    {
                        *slot = BEAlloc(allocator, BE_GlobalConstantNode);
                        BE_ClearStructToZero(**slot);
                        
                        (*slot)->name = newMember->name;
                    }
                }
                
            }
            else
            {
                InvalidStatement_(ExpectedCloseCurlyBracket);
            }
        } break;
        case BE_ParserMode_Enum_MemberValue:
        {
            if (tokenType == BE_TokenType_Punctuation && BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation(",")))
            {
                state->mode = BE_ParserMode_Enum_MemberNameOrEnd;
            }
            else if (tokenType == BE_TokenType_Punctuation && BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation("=")))
            {
                state->mode = BE_ParserMode_Enum_MemberValueExpression;
                BE_InitExpression(allocator, &state->parsingExpressions.expressions, &state->tmpExpression, 0);
            }
            else if (tokenType == BE_TokenType_Punctuation && BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation("}")))
            {
                state->mode = BE_ParserMode_StartEndOfStatement + BE_StatementType_EnumDeclaration;
            }
            else
            {
                InvalidStatement_(ExpectedCloseCurlyBracket);
            }
        } break;
        case BE_ParserMode_Enum_MemberValueExpression:
        {
            BE_Expression* expression = &state->tmpExpression;
            
            BEbool appendedExpression = BE_TryAddLiteralOrOperatorToExpression(allocator, expressions, expression, tokenType, tokenData, token);
            if (!appendedExpression)
            {
                for (BE_EnumMemberList* it = state->statement.enumDeclaration.members; it; it = it->prev)
                {
                    if (BE_StringsAreEqual(it->name, token))
                    {
                        appendedExpression = BE_PushConstantInteger(expression, it->value);
                        break;
                    }
                }
            }
            
            if (!appendedExpression)
            {
                BE_GlobalConstantNode** slot = BE_FindGlobalConstantSlot(&state->globalConstants.root, token);
                if (*slot)
                {
                    state->constantLookup = *slot;
                    state->mode = BE_ParserMode_Enum_MemberValueExpression_ConstantSymbol;
                    
                    appendedExpression = 1;
                }
            }
            
            if (!appendedExpression)
            {
                BE_ScalarValue value;
                BE_ArithmeticError err = BE_EvaluateConstantExpression(&state->parsingExpressions.expressions, &value, expression);
                
                if (err)
                {
                    if (err == BE_ArithmeticError_DivByZero)
                    {
                        InvalidStatement_(ConstantExpression_DivZero);
                    }
                    else
                    {
                        BEAssert(err == BE_ArithmeticError_ModByZero);
                        InvalidStatement_(ConstantExpression_ModZero);
                    }
                }
                else
                {
                    BE_EnumMemberList* lastMember = state->statement.enumDeclaration.members;
                    lastMember->value = value.i;
                    
                    if (tokenType == BE_TokenType_Punctuation && BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation(",")))
                    {
                        state->mode = BE_ParserMode_Enum_MemberNameOrEnd;
                    }
                    else if (tokenType == BE_TokenType_Punctuation && BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation("}")))
                    {
                        state->mode = BE_ParserMode_StartEndOfStatement + BE_StatementType_EnumDeclaration;
                    }
                    else
                    {
                        InvalidStatement_(ExpectedCommaOrCloseCurlyBracket);
                    }
                }
            }
        } break;
        
        case BE_ParserMode_Import:
        {
            ExpectPunctuation_OpenParenthesis()
            {
                state->mode = BE_ParserMode_Import_Path;
            }
        } break;
        case BE_ParserMode_Import_Path:
        {
            if (tokenType == BE_TokenType_QuotedString)
            {
                state->statement.import.unescapedFile = token;
                ++state->statement.import.unescapedFile.characters;
                state->statement.import.unescapedFile.count -= 2;
                
                state->mode = BE_ParserMode_Import_End;
            }
            else
            {
                InvalidStatement_(ExpectedQuotedString);
            }
        } break;
        case BE_ParserMode_Import_End:
        {
            ExpectPunctuation_CloseParenthesis()
            {
                state->mode = BE_ParserMode_StartEndOfStatement + BE_StatementType_Import;
            }
        } break;
        
        case BE_ParserMode_StructHeader:
        {
            if (tokenType == BE_TokenType_Punctuation && BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation("(")))
            {
                state->mode = BE_ParserMode_StructHeader_Category;
            }
            else if (tokenType == BE_TokenType_Identifier)
            {
                state->statement.structHeader.name = token;
                state->mode = BE_ParserMode_StructHeader_Params;
            }
            else
            {
                InvalidStatement_(ExpectedIdentifierOrOpenParenthesis);
            }
        } break;
        case BE_ParserMode_StructHeader_Category:
        {
            ExpectAnyIdentifier()
            {
                state->statement.structHeader.category = token;
                state->mode = BE_ParserMode_StructHeader_Category_1;
            }
        } break;
        case BE_ParserMode_StructHeader_Category_1:
        {
            ExpectPunctuation_Comma()
            {
                ParseConstantIntegerExpression(state->tmpExpression, BE_ParserMode_StructHeader_Category_NumberEnd);
            }
        } break;
        case BE_ParserMode_StructHeader_Category_NumberEnd:
        {
            ExpectPunctuation_CloseParenthesis()
            {
                BE_ScalarValue categoryNumber;
                BE_ArithmeticError err = BE_EvaluateConstantExpression(expressions, &categoryNumber, &state->tmpExpression);
                
                state->statement.structHeader.categoryNumber = categoryNumber.i;
                
                if (err)
                {
                    if (err == BE_ArithmeticError_DivByZero)
                    {
                        InvalidStatement_(ConstantExpression_DivZero);
                    }
                    else
                    {
                        BEAssert(err == BE_ArithmeticError_ModByZero);
                        InvalidStatement_(ConstantExpression_ModZero);
                    }
                }
                else
                {
                    state->mode = BE_ParserMode_StructHeader_Name;
                }
            }
        } break;
        case BE_ParserMode_StructHeader_Name:
        {
            ExpectAnyIdentifier()
            {
                state->statement.structHeader.name = token;
                state->mode = BE_ParserMode_StructHeader_Params;
            }
        } break;
        case BE_ParserMode_StructHeader_Params:
        {
            if (tokenType == BE_TokenType_Punctuation && BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation("(")))
            {
                state->mode = BE_ParserMode_StructHeader_ParamName;
            }
            else
            {
                goto label_StructHeader_End;
            }
        } break;
        case BE_ParserMode_StructHeader_ParamName:
        {
            ExpectKeyword_var()
            {
                state->mode = BE_ParserMode_StructHeader_ParamName_1;
            }
        } break;
        case BE_ParserMode_StructHeader_ParamName_1:
        {
            ExpectAnyIdentifier()
            {
                if (BE_AddMappedName(allocator, &state->statement.structHeader.parameters, token))
                {
                    state->mode = BE_ParserMode_StructHeader_ParamSeperator;
                }
                else
                {
                    InvalidStatement_(DuplicateParameterName);
                }
            }
        } break;
        case BE_ParserMode_StructHeader_ParamSeperator:
        {
            if (tokenType == BE_TokenType_Punctuation && BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation(")")))
            {
                BEAssert(state->statement.structHeader.parameters.count != 0);
                
                state->mode = BE_ParserMode_StructHeader_End;
            }
            else if (tokenType == BE_TokenType_Punctuation && BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation(",")))
            {
                state->mode = BE_ParserMode_StructHeader_ParamName;
            }
            else
            {
                InvalidStatement_(ExpectedCommaOrCloseParenthesis);
            }
        } break;
        case BE_ParserMode_StructHeader_End:
        {
            label_StructHeader_End:
            
            ExpectPunctuation_OpenCurlyBracket()
            {
                BE_ParsedStructNode* parsingStruct = BE_AddStruct(allocator, &state->structs, state->statement.structHeader.name, state->statement.structHeader.parameters);
                
                rVal = 1;
                
                if (parsingStruct)
                {
                    if (state->statement.structHeader.category.count)
                    {
                        BE_ParsedCategoryNode* category = BE_AcquireCategory(allocator, &state->structs, state->statement.structHeader.category);
                        BE_AddTypeToCategory(parsingStruct, category, state->statement.structHeader.categoryNumber);
                    }
                    
                    BE_PushLocalVariableScope(allocator, &state->freeLocalVariableScopes, &parsingStruct->localVariables);
                    
                    state->statement.structHeader.structIndex = parsingStruct->index;
                    *statementType = BE_StatementType_StructHeader;
                    *statement = state->statement;
                }
                else
                {
                    *statementType = BE_StatementType_Invalid_DuplicateStructName;
                }
                
                state->parsingStruct = parsingStruct;
                state->mode = BE_ParserMode_TopLevel;
                BE_PushParsingScope(allocator, &state->scopes)->type = BE_ParserScopeType_Struct;
            }
        } break;
        
        case BE_ParserMode_EndOfScope_Struct:
        {
            ExpectPunctuation_Semicolon()
            {
                *statementType = BE_StatementType_EndOf_Struct;
                *statement = state->statement;
                state->mode = BE_ParserMode_TopLevel;
                
                rVal = 1;
                
                state->parsingStruct = 0;
                
            }
        } break;
        
        case BE_ParserMode_EndOfScope_DoWhile:
        {
            ExpectKeyword_while()
            {
                state->mode = BE_ParserMode_EndOfScope_DoWhile_1;
            }
        } break;
        case BE_ParserMode_EndOfScope_DoWhile_1:
        {
            ExpectPunctuation_OpenParenthesis()
            {
                ParseIntegerExpression(state->statement.endOfDoWhile.condition, BE_ParserMode_EndOfScope_DoWhile_ConditionEnd);
            }
        } break;
        case BE_ParserMode_EndOfScope_DoWhile_ConditionEnd:
        {
            ExpectPunctuation_CloseParenthesis()
            {
                state->mode = BE_ParserMode_StartEndOfStatement + BE_StatementType_EndOf_DoWhile;
            }
        } break;
        
        case BE_ParserMode_Enum_MemberValueExpression_ConstantSymbol:
        case BE_ParserMode_IntegerExpression_ConstantSymbol:
        {
            BEAssert(state->constantLookup);
            
            if (tokenType == BE_TokenType_Punctuation && BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation(".")))
            {
                if (state->constantLookup->children)
                {
                    if (state->mode == BE_ParserMode_IntegerExpression_ConstantSymbol)
                    {
                        state->mode = BE_ParserMode_IntegerExpression_ConstantSymbol_1;
                    }
                    else
                    {
                        state->mode = BE_ParserMode_Enum_MemberValueExpression_ConstantSymbol_1;
                    }
                }
                else
                {
                    InvalidStatement_(ConstantLookupFailure);
                }
            }
            else if (state->constantLookup->hasValue)
            {
                BE_Expression* expression = (state->mode == BE_ParserMode_IntegerExpression_ConstantSymbol 
                                             ? parsingExpressions->top->expression : &state->tmpExpression);
                
                BEbool appendedExpression = BE_PushConstantInteger(expression, state->constantLookup->value);
                
                if (!appendedExpression)
                {
                    InvalidStatement_(IntegerExpression);
                }
                else
                {
                    if (state->mode == BE_ParserMode_IntegerExpression_ConstantSymbol)
                    {
                        state->mode = BE_ParserMode_IntegerExpression;
                    }
                    else
                    {
                        state->mode = BE_ParserMode_Enum_MemberValueExpression;
                    }
                    
                    goto label_Restart;
                }
            }
            else
            {
                InvalidStatement_(ConstantLookupFailure);
            }
        } break;
        
        case BE_ParserMode_Enum_MemberValueExpression_ConstantSymbol_1:
        case BE_ParserMode_IntegerExpression_ConstantSymbol_1:
        {
            BE_GlobalConstantNode** slot = BE_FindGlobalConstantSlot(&state->constantLookup->children, token);
            if (*slot)
            {
                state->constantLookup = *slot;
                if (state->mode == BE_ParserMode_IntegerExpression_ConstantSymbol_1)
                {
                    state->mode = BE_ParserMode_IntegerExpression_ConstantSymbol;
                }
                else
                {
                    state->mode = BE_ParserMode_Enum_MemberValueExpression_ConstantSymbol;
                }
            }
            else
            {
                InvalidStatement_(ConstantLookupFailure);
            }
        } break;
        
        case BE_ParserMode_IntegerExpression_Load:
        {
            ExpectPunctuation_OpenParenthesis()
            {
                BE_PushLoadExpression(allocator, &state->loadExpressions);
                state->mode = BE_ParserMode_IntegerExpression_Load_1;
            }
        } break;
        case BE_ParserMode_IntegerExpression_Load_1:
        {
            if (tokenType == BE_TokenType_Punctuation && BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation("@")))
            {
                state->mode = BE_ParserMode_IntegerExpression_Load_Address;
            }
            else
            {
                state->mode = BE_ParserMode_IntegerExpression_Load_Type;
                goto label_IntegerExpression_Load_Type;
            }
        } break;
        case BE_ParserMode_IntegerExpression_Load_Address:
        {
            ExpectPunctuation_OpenParenthesis()
            {
                ParseIntegerExpression(state->loadExpressions.top->address, BE_ParserMode_IntegerExpression_Load_Address_End);
            }
        } break;
        case BE_ParserMode_IntegerExpression_Load_Address_End:
        {
            ExpectPunctuation_CloseParenthesis()
            {
                state->mode = BE_ParserMode_IntegerExpression_Load_Type;
            }
        } break;
        case BE_ParserMode_IntegerExpression_Load_Type:
        {
            label_IntegerExpression_Load_Type:
            
            BEbool success = 0;
            
            if (tokenType == BE_TokenType_Identifier)
            {
                success = 1;
                
                if (BE_StringsAreEqual(token, BE_StrLiteral("s")))
                {
                    state->loadExpressions.top->isSigned = 1;
                }
                else if (BE_StringsAreEqual(token, BE_StrLiteral("u")))
                {
                    state->loadExpressions.top->isSigned = 0;
                }
                else
                {
                    success = 0;
                }
            }
            
            if (success)
            {
                state->mode = BE_ParserMode_IntegerExpression_Load_Size;
            }
            else
            {
                InvalidStatement_(ExpectedKeyword_s_or_u);
            }
        } break;
        case BE_ParserMode_IntegerExpression_Load_Size:
        {
            ExpectPunctuation_OpenParenthesis()
            {
                ParseIntegerExpression(state->loadExpressions.top->size, BE_ParserMode_IntegerExpression_Load_Size_End);
            }
        } break;
        case BE_ParserMode_IntegerExpression_Load_Size_End:
        {
            ExpectPunctuation_CloseParenthesis()
            {
                state->mode = BE_ParserMode_IntegerExpression_Load_End;
            }
        } break;
        case BE_ParserMode_IntegerExpression_Load_End:
        {
            ExpectPunctuation_CloseParenthesis()
            {
                BE_Expression* expression = parsingExpressions->top->expression;
                if (BE_PushLoad(expression, state->loadExpressions.top->address, state->loadExpressions.top->size, state->loadExpressions.top->isSigned))
                {
                    state->mode = BE_ParserMode_IntegerExpression;
                    BE_PopLoadExpression(&state->loadExpressions);
                }
                else
                {
                    InvalidStatement_(IntegerExpression);
                }
            }
        } break;
        
        case BE_ParserMode_IntegerExpression_HasType:
        {
            ExpectPunctuation_OpenParenthesis()
            {
                state->mode = BE_ParserMode_IntegerExpression_HasType_Category;
            }
        } break;
        case BE_ParserMode_IntegerExpression_HasType_Category:
        {
            ExpectAnyIdentifier()
            {
                state->category = BE_FindCategory(&state->structs, token);
                
                if (state->category)
                {
                    state->mode = BE_ParserMode_IntegerExpression_HasType_Comma;
                }
                else
                {
                    InvalidStatement_(ExpectedCategoryName);
                }
            }
        } break;
        case BE_ParserMode_IntegerExpression_HasType_Comma:
        {
            ExpectPunctuation_Comma()
            {
                ParseIntegerExpression(state->categoryNumber, BE_ParserMode_IntegerExpression_HasType_CategoryNumberEnd);
            }
        } break;
        case BE_ParserMode_IntegerExpression_HasType_CategoryNumberEnd:
        {
            ExpectPunctuation_CloseParenthesis()
            {
                state->mode = BE_ParserMode_IntegerExpression;
                if (!BE_PushHasType(parsingExpressions->top->expression, state->category->index, state->categoryNumber))
                {
                    InvalidStatement_(IntegerExpression);
                }
            }
        } break;
        
        case BE_ParserMode_IntegerExpression_CreatePlot:
        {
            ExpectPunctuation_OpenParenthesis()
            {
                state->mode = BE_ParserMode_IntegerExpression_CreatePlot_End;
            }
        } break;
        case BE_ParserMode_IntegerExpression_CreatePlot_End:
        {
            ExpectPunctuation_CloseParenthesis()
            {
                state->mode = BE_ParserMode_IntegerExpression;
                if (!BE_PushCreatePlot(parsingExpressions->top->expression))
                {
                    InvalidStatement_(IntegerExpression);
                }
            }
        } break;
        
        case BE_ParserMode_IntegerExpression_SizeOf:
        {
            ExpectPunctuation_OpenParenthesis()
            {
                state->mode = BE_ParserMode_IntegerExpression_SizeOf_1;
            }
        } break;
        case BE_ParserMode_IntegerExpression_SizeOf_1:
        {
            if (tokenType == BE_TokenType_Punctuation && BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation("@")))
            {
                state->addressSpecification.returnMode = BE_ParserMode_IntegerExpression_SizeOf_Type;
                state->mode = BE_ParserMode_AddressSpecification;
            }
            else
            {
                goto label_IntegerExpression_SizeOf_Type;
            }
        } break;
        case BE_ParserMode_IntegerExpression_SizeOf_Type:
        {
            label_IntegerExpression_SizeOf_Type:
            
            ExpectAnyIdentifier()
            {
                BE_ParsedStructNode* type = BE_FindStruct(&state->structs, token);
                if (type)
                {
                    state->mode = BE_ParserMode_IntegerExpression_SizeOf_End;
                    
                    if (!BE_PushSizeOf(parsingExpressions->top->expression, type->index, state->addressSpecification.expression))
                    {
                        InvalidStatement_(IntegerExpression);
                    }
                }
                else
                {
                    InvalidStatement_(ExpectedStructName);
                }
            }
        } break;
        case BE_ParserMode_IntegerExpression_SizeOf_End:
        {
            ExpectPunctuation_CloseParenthesis()
            {
                state->mode = BE_ParserMode_IntegerExpression;
            }
        } break;
        
        case BE_ParserMode_IntegerExpression_ArrayCount:
        {
            ExpectPunctuation_OpenParenthesis()
            {
                BE_Expression* expression = parsingExpressions->top->expression;
                BE_MemberPath* outPath = BE_PushArrayCountPlaceholder(expression);
                
                BEAssert(outPath); // NOTE(Jens): Fail-fast should've taken care of this case.
                
                ParseArrayMemberAccess(outPath, BE_ParserMode_IntegerExpression_ArrayCount_End);
            }
        } break;
        case BE_ParserMode_IntegerExpression_ArrayCount_End:
        {
            ExpectPunctuation_CloseParenthesis()
            {
                state->mode = BE_ParserMode_IntegerExpression;
            }
        } break;
        
        case BE_ParserMode_IntegerExpression:
        {
            label_IntegerExpression:
            
            BEAssert(parsingExpressions->top);
            
            BE_Expression* expression = parsingExpressions->top->expression;
            BEbool integerExpressionIsConstant = parsingExpressions->top->isConstant;
            
            BEbool appendedExpression = BE_TryAddLiteralOrOperatorToExpression(allocator, expressions, expression, tokenType, tokenData, token);
            
            if (!integerExpressionIsConstant && !appendedExpression)
            {
                if (tokenType == BE_TokenType_Identifier)
                {
                    // NOTE(Jens): Variables:
                    
                    BEbool isGlobal = 0;
                    
                    BEu32 variableNumber = (state->parsingStruct ? BE_FindLocalVariableNumber(&state->parsingStruct->localVariables, token) : 0);
                    if (!variableNumber)
                    {
                        isGlobal = 1;
                        variableNumber = BE_FindLocalVariableNumber(&state->globalVariables, token);
                    }
                    
                    if (variableNumber)
                    {
                        if (isGlobal)
                        {
                            appendedExpression = BE_PushGlobalVariable(expression, variableNumber - 1);
                        }
                        else
                        {
                            appendedExpression = BE_PushLocalVariable(expression, variableNumber - 1);
                        }
                    }
                    else
                    {
                        if (state->parsingStruct)
                        {
                            BE_NameMapNode* node = BE_FindMappedName(&state->parsingStruct->parameters, token);
                            if (node)
                            {
                                variableNumber = 1 + node->index;
                            }
                        }
                        
                        if (variableNumber)
                        {
                            appendedExpression = BE_PushParameter(expression, variableNumber - 1);
                        }
                        else if (BE_StringsAreEqual(token, BE_StrLiteral("size_of_file")))
                        {
                            appendedExpression = BE_PushSizeOfFile(expression);
                        }
                        else if (state->parsingStruct && BE_StringsAreEqual(token, BE_StrLiteral("array_count")))
                        {
                            if (BE_ExpectsValue(expression))
                            {
                                state->mode = BE_ParserMode_IntegerExpression_ArrayCount;
                            }
                            else
                            {
                                // NOTE(Jens): Fail-fast to get more accurate error location.
                                InvalidStatement_(IntegerExpression);
                            }
                            
                            appendedExpression = 1;
                        }
                        else if (BE_StringsAreEqual(token, BE_StrLiteral("has_type")))
                        {
                            if (BE_ExpectsValue(expression))
                            {
                                state->mode = BE_ParserMode_IntegerExpression_HasType;
                            }
                            else
                            {
                                // NOTE(Jens): Fail-fast to get more accurate error location.
                                InvalidStatement_(IntegerExpression);
                            }
                            
                            appendedExpression = 1;
                        }
                        else if (BE_StringsAreEqual(token, BE_StrLiteral("create_plot")))
                        {
                            if (BE_ExpectsValue(expression))
                            {
                                state->mode = BE_ParserMode_IntegerExpression_CreatePlot;
                            }
                            else
                            {
                                // NOTE(Jens): Fail-fast to get more accurate error location.
                                InvalidStatement_(IntegerExpression);
                            }
                            
                            appendedExpression = 1;
                        }
                        else if (BE_StringsAreEqual(token, BE_StrLiteral("sizeof")))
                        {
                            if (BE_ExpectsValue(expression))
                            {
                                state->mode = BE_ParserMode_IntegerExpression_SizeOf;
                            }
                            else
                            {
                                // NOTE(Jens): Fail-fast to get more accurate error location.
                                InvalidStatement_(IntegerExpression);
                            }
                            
                            appendedExpression = 1;
                        }
                        else if (BE_StringsAreEqual(token, BE_StrLiteral("load")))
                        {
                            if (BE_ExpectsValue(expression))
                            {
                                state->mode = BE_ParserMode_IntegerExpression_Load;
                            }
                            else
                            {
                                // NOTE(Jens): Fail-fast to get more accurate error location.
                                InvalidStatement_(IntegerExpression);
                            }
                            
                            appendedExpression = 1;
                        }
                        else
                        {
                            if (state->parsingStruct)
                            {
                                BE_ParsedMemberNode* member = BE_FindStructMember(state->parsingStruct, token);
                                
                                if (member)
                                {
                                    BE_MemberPath* outPath = BE_PushMemberPlaceholder(expression);
                                    if (outPath)
                                    {
                                        appendedExpression = 1;
                                        
                                        BE_ParsingMemberAccess* memberAccess = BE_PushMemberAccess(allocator, parsingMemberAccesses, outPath, BE_ParserMode_IntegerExpression, 0);
                                        
                                        memberAccess->activeMember = member;
                                        BE_AppendMemberPath(allocator, memberAccess->path, member->declaration.memberIndex);
                                        
                                        if (member->declaration.isArray)
                                        {
                                            state->mode = BE_ParserMode_MemberAccess_ArrayIndex;
                                        }
                                        else if (BE_TypeIsScalar(member->declaration.type))
                                        {
                                            state->mode = memberAccess->returnMode;
                                            BE_PopMemberAccess(parsingMemberAccesses);
                                        }
                                        else
                                        {
                                            state->mode = BE_ParserMode_MemberAccess_Nested;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if (tokenType == BE_TokenType_Punctuation && BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation("@")))
                {
                    appendedExpression = BE_PushMemberOffset(expression);
                }
            }
            
            if (!appendedExpression)
            {
                BE_GlobalConstantNode** slot = BE_FindGlobalConstantSlot(&state->globalConstants.root, token);
                if (*slot)
                {
                    state->constantLookup = *slot;
                    state->mode = BE_ParserMode_IntegerExpression_ConstantSymbol;
                    
                    appendedExpression = 1;
                }
            }
            
            if (!appendedExpression)
            {
                state->mode = parsingExpressions->top->returnMode;
                BE_PopExpression(parsingExpressions);
                
                goto label_Restart;
            }
        } break;
        
        case BE_ParserMode_IntegerExpression_MemberFetch:
        {
            BE_Expression* expression = parsingExpressions->top->expression;
            ExpectAnyIdentifier()
            {
                BE_ParsedStructNode* structNode = BE_GetMemberStructType(parsingExpressions->top->activeMemberAccess->declaration.type);
                BEAssert(structNode); // NOTE(Jens): Scalar types should not ever reach this state.
                
                BE_ParsedMemberNode* member = BE_FindStructMember(structNode, token);
                if (member)
                {
                    parsingExpressions->top->activeMemberAccess = member;
                    
                    BE_AppendMemberPath(allocator, &parsingExpressions->top->activeMemberPath, member->declaration.memberIndex);
                    
                    if (BE_TypeIsScalar(member->declaration.type))
                    {
                        if (member->declaration.isArray)
                        {
                            state->mode = BE_ParserMode_IntegerExpression_MemberFetch_ArrayIndex;
                        }
                        else
                        {
                            state->mode = BE_ParserMode_IntegerExpression;
                            
                            if (!BE_PushMember(expression, parsingExpressions->top->activeMemberPath))
                            {
                                InvalidStatement_(IntegerExpression);
                            }
                            
                            BE_ClearStructToZero(parsingExpressions->top->activeMemberPath);
                        }
                    }
                    else
                    {
                        state->mode = BE_ParserMode_IntegerExpression_MemberFetch_Nested;
                    }
                }
                else
                {
                    InvalidStatement_(ExpectedMemberName);
                }
            }
        } break;
        case BE_ParserMode_IntegerExpression_MemberFetch_Nested:
        {
            ExpectPunctuation_Dot()
            {
                state->mode = BE_ParserMode_IntegerExpression_MemberFetch;
            }
        } break;
        case BE_ParserMode_IntegerExpression_MemberFetch_ArrayIndex:
        {
            ExpectPunctuation_OpenSquareBracket()
            {
                ParseIntegerExpression(parsingExpressions->top->activeMemberPath.tail->arrayIndex, BE_ParserMode_IntegerExpression_MemberFetch_ArrayIndex_End);
            }
        } break;
        case BE_ParserMode_IntegerExpression_MemberFetch_ArrayIndex_End:
        {
            BE_Expression* expression = parsingExpressions->top->expression;
            
            ExpectPunctuation_CloseSquareBracket()
            {
                if (BE_TypeIsScalar(parsingExpressions->top->activeMemberAccess->declaration.type))
                {
                    if (BE_PushMember(expression, parsingExpressions->top->activeMemberPath))
                    {
                        state->mode = BE_ParserMode_IntegerExpression;
                    }
                    else
                    {
                        InvalidStatement_(IntegerExpression);
                    }
                    
                    BE_ClearStructToZero(parsingExpressions->top->activeMemberPath);
                    parsingExpressions->top->activeMemberAccess = 0;
                }
                else
                {
                    state->mode = BE_ParserMode_IntegerExpression_MemberFetch_Nested;
                }
            }
        } break;
        
        case BE_ParserMode_AddressSpecification:
        {
            ExpectPunctuation_OpenParenthesis()
            {
                state->mode = BE_ParserMode_AddressSpecification_1;
            }
        } break;
        case BE_ParserMode_AddressSpecification_1:
        {
            if (tokenType == BE_TokenType_Identifier && !state->addressSpecification.isExternal && BE_StringsAreEqual(token, BE_StrLiteral("external")))
            {
                state->addressSpecification.isExternal = 1;
            }
            else
            {
                ParseIntegerExpression(state->addressSpecification.expression, BE_ParserMode_AddressSpecification_2);
                goto label_IntegerExpression;
            }
        } break;
        case BE_ParserMode_AddressSpecification_2:
        {
            ExpectPunctuation_CloseParenthesis()
            {
                state->mode = state->addressSpecification.returnMode;
            }
        } break;
        
        case BE_ParserMode_TypeDeclaration:
        {
            label_TypeDeclaration:
            
            if (tokenType == BE_TokenType_Identifier && BE_StringsAreEqual(token, BE_StrLiteral("alignas")))
            {
                state->mode = BE_ParserMode_TypeDeclaration_Align;
            }
            else
            {
                state->mode = BE_ParserMode_TypeDeclaration_Type;
                goto label_ScalarDeclaration_Type;
            }
        } break;
        case BE_ParserMode_TypeDeclaration_Align:
        {
            ExpectPunctuation_OpenParenthesis()
            {
                ParseIntegerExpression(state->typeDeclaration->alignmentExpression, BE_ParserMode_TypeDeclaration_AlignEnd);
            }
        } break;
        case BE_ParserMode_TypeDeclaration_AlignEnd:
        {
            ExpectPunctuation_CloseParenthesis()
            {
                state->mode = BE_ParserMode_TypeDeclaration_Type;
            }
        } break;
        case BE_ParserMode_TypeDeclaration_Type:
        {
            label_ScalarDeclaration_Type:
            
            BEbool success = 0;
            if (tokenType == BE_TokenType_Identifier)
            {
                if (!success)
                {
                    BE_ScalarBaseType baseType = BE_IdentifierToScalarBaseType(&success, token);
                    
                    if (success)
                    {
                        state->typeDeclaration->variant = BE_TypeVariant_Scalar;
                        state->typeDeclaration->scalarVariant.baseType = baseType;
                        state->mode = BE_ParserMode_ScalarDeclaration_Size;
                        
                        if (baseType == BE_ScalarBaseType_u || baseType == BE_ScalarBaseType_s || baseType == BE_ScalarBaseType_hidden || baseType == BE_ScalarBaseType_f)
                        {
                            state->typeDeclaration->scalarVariant.byteOrder = state->defaultByteOrder;
                        }
                        else
                        {
                            // NOTE(Jens): 'raw' and 'string' are by default written in byte order (i.e. little endian).
                            state->typeDeclaration->scalarVariant.byteOrder = BE_ScalarByteOrder_little;
                            
                            if (baseType == BE_ScalarBaseType_raw)
                            {
                                state->typeDeclaration->scalarVariant.displayType = BE_ScalarDisplayType_hex;
                            }
                        }
                    }
                }
                
                if (!success)
                {
                    BE_ParsedCategoryNode* categoryNode = BE_FindCategory(&state->structs, token);
                    BE_ParsedStructNode* structNode = BE_FindStruct(&state->structs, token);
                    BE_ParsedTypedefNode* typedefNode = BE_FindTypedef(&state->typedefs, token);
                    BE_ParsedEnumNode* enumNode = BE_FindEnum(&state->enums, token);
                    
                    BEu32 nonCategoryCandidateCount = !!structNode + !!typedefNode + !!enumNode;
                    
                    if (nonCategoryCandidateCount == 1)
                    {
                        success = 1;
                        
                        if (categoryNode)
                        {
                            // NOTE(Jens): Let's only solve ambiguity for category declarations as those are always different.
                            
                            state->typeDeclaration->variant = BE_TypeVariant_Category;
                            state->typeDeclaration->categoryVariant.category = categoryNode;
                            
                            state->mode = BE_ParserMode_CategoryDeclaration_StartAmbiguous;
                            state->typeDeclaration->argumentCount = categoryNode->paramCount;
                        }
                        else
                        {
                            if (structNode)
                            {
                                state->typeDeclaration->variant = BE_TypeVariant_Struct;
                                state->typeDeclaration->structVariant.type = structNode;
                                state->typeDeclaration->argumentCount = structNode->parameters.count;
                                
                                if (structNode->parameters.count)
                                {
                                    state->typeDeclaration->arguments = BEAllocArray(allocator, BE_Expression, state->typeDeclaration->argumentCount);
                                    state->mode = BE_ParserMode_TypeDeclaration_ParametersStart;
                                }
                                else
                                {
                                    state->mode = state->typeDeclarationReturnMode;
                                }
                            }
                            else if (typedefNode)
                            {
                                state->typeDeclaration->variant = BE_TypeVariant_Typedef;
                                state->typeDeclaration->typedefVariant.def = typedefNode;
                                
                                state->mode = state->typeDeclarationReturnMode;
                            }
                            else
                            {
                                BEAssert(enumNode);
                                
                                state->typeDeclaration->variant = BE_TypeVariant_Scalar;
                                state->typeDeclaration->scalarVariant.enumType = enumNode;
                                state->typeDeclaration->scalarVariant.baseType = BE_ScalarBaseType_s;
                                state->typeDeclaration->scalarVariant.byteOrder = state->defaultByteOrder;
                                
                                if (enumNode->size)
                                {
                                    state->mode = BE_ParserMode_ScalarDeclaration_OptionalSize;
                                }
                                else
                                {
                                    state->mode = BE_ParserMode_ScalarDeclaration_Size;
                                }
                            }
                        }
                    }
                    else if (nonCategoryCandidateCount == 0)
                    {
                        if (categoryNode)
                        {
                            success = 1;
                            
                            state->typeDeclaration->variant = BE_TypeVariant_Category;
                            state->typeDeclaration->categoryVariant.category = categoryNode;
                            
                            state->mode = BE_ParserMode_CategoryDeclaration_Start;
                            
                            if (categoryNode->paramCount)
                            {
                                state->typeDeclaration->argumentCount = categoryNode->paramCount;
                                state->typeDeclaration->arguments = BEAllocArray(allocator, BE_Expression, state->typeDeclaration->argumentCount);
                            }
                        }
                    }
                }
            }
            
            if (!success)
            {
                InvalidStatement_(ExpectedType);
            }
        } break;
        case BE_ParserMode_ScalarDeclaration_OptionalSize:
        {
            if (tokenType == BE_TokenType_Punctuation && BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation("(")))
            {
                ParseIntegerExpression(state->typeDeclaration->scalarVariant.sizeExpression, BE_ParserMode_ScalarDeclaration_SizeEnd);
            }
            else
            {
                state->mode = state->typeDeclarationReturnMode;
                goto label_Restart;
            }
        } break;
        case BE_ParserMode_ScalarDeclaration_Size:
        {
            ExpectPunctuation_OpenParenthesis()
            {
                ParseIntegerExpression(state->typeDeclaration->scalarVariant.sizeExpression, BE_ParserMode_ScalarDeclaration_SizeEnd);
            }
        } break;
        case BE_ParserMode_ScalarDeclaration_SizeEnd:
        {
            if (tokenType == BE_TokenType_Punctuation && BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation(",")))
            {
                state->mode = BE_ParserMode_ScalarDeclaration_DisplayOrByteOrder;
            }
            else if (tokenType == BE_TokenType_Punctuation && BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation(")")))
            {
                state->mode = state->typeDeclarationReturnMode;
            }
            else
            {
                InvalidStatement_(ExpectedCommaOrCloseParenthesis);
            }
        } break;
        case BE_ParserMode_ScalarDeclaration_DisplayOrByteOrder:
        {
            BEbool success = 1;
            
            BEbool isRaw = (state->typeDeclaration->scalarVariant.baseType == BE_ScalarBaseType_raw);
            
            BE_ScalarDisplayType oldDisplayType = state->typeDeclaration->scalarVariant.displayType;
            BE_ScalarByteOrder oldByteOrder = state->typeDeclaration->scalarVariant.byteOrder;
            
            if (tokenType == BE_TokenType_Identifier && BE_StringsAreEqual(token, BE_StrLiteral("binary")))
            {
                state->typeDeclaration->scalarVariant.displayType = BE_ScalarDisplayType_binary;
            }
            else if (tokenType == BE_TokenType_Identifier && BE_StringsAreEqual(token, BE_StrLiteral("octal")))
            {
                state->typeDeclaration->scalarVariant.displayType = BE_ScalarDisplayType_octal;
            }
            else if (tokenType == BE_TokenType_Identifier && BE_StringsAreEqual(token, BE_StrLiteral("hex")))
            {
                state->typeDeclaration->scalarVariant.displayType = BE_ScalarDisplayType_hex;
            }
            else if (tokenType == BE_TokenType_Identifier && BE_StringsAreEqual(token, BE_StrLiteral("little")))
            {
                state->typeDeclaration->scalarVariant.byteOrder = BE_ScalarByteOrder_little;
            }
            else if (tokenType == BE_TokenType_Identifier && BE_StringsAreEqual(token, BE_StrLiteral("big")))
            {
                state->typeDeclaration->scalarVariant.byteOrder = BE_ScalarByteOrder_big;
            }
            else
            {
                success = 0;
                InvalidStatement_(ExpectedDisplayKeywordOrByteOrder);
            }
            
            if (oldDisplayType != state->typeDeclaration->scalarVariant.displayType)
            {
                if (!isRaw && oldDisplayType || isRaw && (oldDisplayType != BE_ScalarDisplayType_hex))
                {
                    InvalidStatement_(DuplicateDisplayKeyword);
                    success = 0;
                }
            }
            else if (oldByteOrder != state->typeDeclaration->scalarVariant.byteOrder)
            {
                if (oldByteOrder)
                {
                    InvalidStatement_(DuplicateByteOrder);
                    success = 0;
                }
            }
            
            if (success)
            {
                state->mode = BE_ParserMode_ScalarDeclaration_SizeEnd;
            }
        } break;
        case BE_ParserMode_TypeDeclaration_ParametersStart:
        {
            ExpectPunctuation_OpenParenthesis()
            {
                state->mode = BE_ParserMode_TypeDeclaration_ParameterName;
            }
        } break;
        case BE_ParserMode_TypeDeclaration_ParameterName:
        {
            BE_Expression* outExpression = state->typeDeclaration->arguments + state->typeDeclaration->argumentReadCount++;
            BE_ClearStructToZero(*outExpression);
            
            if (state->typeDeclaration->argumentReadCount > state->typeDeclaration->argumentCount)
            {
                InvalidStatement_(TooManyArguments);
            }
            else
            {
                ParseIntegerExpression(*outExpression, BE_ParserMode_TypeDeclaration_ParameterSeparator);
                goto label_IntegerExpression;
            }
        } break;
        case BE_ParserMode_TypeDeclaration_ParameterSeparator:
        {
            if (tokenType == BE_TokenType_Punctuation && BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation(",")))
            {
                state->mode = BE_ParserMode_TypeDeclaration_ParameterName;
            }
            else ExpectPunctuation_CloseParenthesis()
            {
                if (state->typeDeclaration->argumentReadCount != state->typeDeclaration->argumentCount)
                {
                    // NOTE(Jens): Too many arguments should be caught in BE_ParserMode_TypeDeclaration_ParameterName.
                    BEAssert(state->typeDeclaration->argumentReadCount < state->typeDeclaration->argumentCount);
                    
                    InvalidStatement_(NotEnoughArguments);
                }
                else
                {
                    state->mode = state->typeDeclarationReturnMode;
                }
            }
        } break;
        case BE_ParserMode_CategoryDeclaration_StartAmbiguous:
        {
            BE_String typeName = state->typeDeclaration->categoryVariant.category->name;
            
            BE_ParsedStructNode* structNode = BE_FindStruct(&state->structs, typeName);
            BE_ParsedTypedefNode* typedefNode = BE_FindTypedef(&state->typedefs, typeName);
            BE_ParsedEnumNode* enumNode = BE_FindEnum(&state->enums, typeName);
            
            if (tokenType == BE_TokenType_Punctuation && BE_PunctuationsAreSame(tokenData.punctuation, BE_Punctuation("[")))
            {
                state->typeDeclaration->arguments = BEAllocArray(allocator, BE_Expression, state->typeDeclaration->argumentCount);
                ParseIntegerExpression(state->typeDeclaration->categoryVariant.categoryIdentifier, BE_ParserMode_CategoryDeclaration_End);
            }
            else if (structNode)
            {
                state->typeDeclaration->variant = BE_TypeVariant_Struct;
                state->typeDeclaration->structVariant.type = structNode;
                state->typeDeclaration->argumentCount = structNode->parameters.count;
                
                if (structNode->parameters.count)
                {
                    state->typeDeclaration->arguments = BEAllocArray(allocator, BE_Expression, state->typeDeclaration->argumentCount);
                    state->mode = BE_ParserMode_TypeDeclaration_ParametersStart;
                }
                else
                {
                    state->mode = state->typeDeclarationReturnMode;
                }
                
                goto label_Restart;
            }
            else if (typedefNode)
            {
                state->typeDeclaration->variant = BE_TypeVariant_Typedef;
                state->typeDeclaration->typedefVariant.def = typedefNode;
                
                state->mode = state->typeDeclarationReturnMode;
                
                goto label_Restart;
            }
            else if (enumNode)
            {
                state->typeDeclaration->variant = BE_TypeVariant_Scalar;
                state->typeDeclaration->scalarVariant.enumType = enumNode;
                state->typeDeclaration->scalarVariant.baseType = BE_ScalarBaseType_s;
                state->typeDeclaration->scalarVariant.byteOrder = state->defaultByteOrder;
                
                if (enumNode->size)
                {
                    state->mode = BE_ParserMode_ScalarDeclaration_OptionalSize;
                }
                else
                {
                    state->mode = BE_ParserMode_ScalarDeclaration_Size;
                }
                
                goto label_Restart;
            }
            else
            {
                BEAssert(0);
            }
            
        } break;
        case BE_ParserMode_CategoryDeclaration_Start:
        {
            ExpectPunctuation_OpenSquareBracket()
            {
                ParseIntegerExpression(state->typeDeclaration->categoryVariant.categoryIdentifier, BE_ParserMode_CategoryDeclaration_End);
            }
        } break;
        case BE_ParserMode_CategoryDeclaration_End:
        {
            ExpectPunctuation_CloseSquareBracket()
            {
                if (state->typeDeclaration->argumentCount)
                {
                    state->mode = BE_ParserMode_TypeDeclaration_ParametersStart;
                }
                else
                {
                    state->mode = state->typeDeclarationReturnMode;
                }
            }
        } break;
    }
    
    if (rVal)
    {
        BE_ClearStructToZero(state->statement);
    }
    
#undef InvalidStatement
#undef ExpectPunctuation
#undef ExpectIdentifier
#undef ParseIntegerExpression
#undef ExpectAnyIdentifier
    
    return rVal;
}

struct BE_StructStartAddressNode
{
    BE_StructStartAddressNode* next;
    
    BEu32 instructionIndex;
    BEu32 highestStackEntryUsed;
};

struct BE_InstructionListNode
{
    BE_InstructionListNode* next;
    
    BEInstruction instruction;
};

struct BE_InstructionRefStack
{
    BE_InstructionRefStack* prev;
    BEInstruction* ref;
};

struct BE_InstructionIfChain
{
    BE_InstructionIfChain* prev;
    BE_InstructionRefStack* jumpToEndInstructions;
};

struct BE_StackU32
{
    BE_StackU32* prev;
    BEu32 value;
};

BEPrivateAPI void BE_PushScopeStackReset(BEAllocatorType allocator, BE_InstructionGenerator* generator)
{
    BE_StackU32* node = generator->freeStackU32;
    if (node)
    {
        generator->freeStackU32 = generator->freeStackU32->prev;
    }
    else
    {
        node = BEAlloc(allocator, BE_StackU32);
    }
    BE_ClearStructToZero(*node);
    
    node->value = generator->stackEntryCount[generator->stackIndex];
    node->prev = generator->startOfScopeEntrySize;
    generator->startOfScopeEntrySize = node;
}

BEPrivateAPI void BE_PopScopeStackReset(BE_InstructionGenerator* generator)
{
    BE_StackU32* node = generator->startOfScopeEntrySize;
    generator->startOfScopeEntrySize = generator->startOfScopeEntrySize->prev;
    
    node->prev = generator->freeStackU32;
    generator->freeStackU32 = node;
    
    generator->stackEntryCount[generator->stackIndex] = node->value;
}

BEPrivateAPI void BE_PushStartOfIteration(BEAllocatorType allocator, BE_InstructionGenerator* generator, BEu32 instructionIndex)
{
    BE_StackU32* node = generator->freeStackU32;
    if (node)
    {
        generator->freeStackU32 = generator->freeStackU32->prev;
    }
    else
    {
        node = BEAlloc(allocator, BE_StackU32);
    }
    BE_ClearStructToZero(*node);
    
    node->value = instructionIndex;
    node->prev = generator->startOfIterationStack;
    generator->startOfIterationStack = node;
}

BEPrivateAPI BEu32 BE_PopStartOfIteration(BE_InstructionGenerator* generator)
{
    BE_StackU32* node = generator->startOfIterationStack;
    generator->startOfIterationStack = generator->startOfIterationStack->prev;
    
    node->prev = generator->freeStackU32;
    generator->freeStackU32 = node;
    
    return node->value;
}

BEPrivateAPI void BE_AddStartOfStruct(BEAllocatorType allocator, BE_InstructionGenerator* generator)
{
    BE_StructStartAddressNode* newNode = BEAlloc(allocator, BE_StructStartAddressNode);
    BE_ClearStructToZero(*newNode);
    
    newNode->instructionIndex = generator->instructionIndex;
    
    if (generator->structStartAddresses.head)
    {
        generator->structStartAddresses.tail = generator->structStartAddresses.tail->next = newNode;
    }
    else
    {
        generator->structStartAddresses.tail = generator->structStartAddresses.head = newNode;
    }
}

BEPrivateAPI BE_StructStartAddressNode* BE_FindStartAddressOfStruct(BE_StructStartAddresses* addresses, BEu32 typeIndex)
{
    BE_StructStartAddressNode* it = addresses->head;
    for (BEu32 i = 0; i < typeIndex; ++i)
    {
        BEAssert(it);
        it = it->next;
    }
    BEAssert(it);
    
    return it;
}

typedef struct
{
    BEu32 stackIndex;
    BEu32 count;
} BE_StackReset;

BEPrivateAPI BE_StackReset BE_StackResetPoint(BE_InstructionGenerator* generator)
{
    BE_StackReset rVal;
    rVal.stackIndex = generator->stackIndex;
    rVal.count = generator->stackEntryCount[generator->stackIndex];
    return rVal;
}

BEPrivateAPI void BE_ResetStack(BE_InstructionGenerator* generator, BE_StackReset reset)
{
    generator->stackEntryCount[reset.stackIndex] = reset.count;
}

BEPrivateAPI void BE_InitLocalStack(BE_InstructionGenerator* generator)
{
    generator->maxStackEntryCount[generator->stackIndex] = 
        generator->stackEntryCount[generator->stackIndex] = BE_LocalSymbol_Count;
}

BEPrivateAPI void BE_InitGlobalStack(BE_InstructionGenerator* generator)
{
    generator->maxStackEntryCount[0] = 
        generator->stackEntryCount[0] = BE_GlobalSymbol_Count;
}

BEPrivateAPI BE_StackEntry BE_PushLocalStackEntry(BE_InstructionGenerator* generator)
{
    BE_StackEntry rVal;
    BE_ClearStructToZero(rVal);
    
    rVal.stack = BE_Stack_Local;
    rVal.index = generator->stackEntryCount[generator->stackIndex]++;
    
    if (generator->stackEntryCount[generator->stackIndex] > generator->maxStackEntryCount[generator->stackIndex])
    {
        generator->maxStackEntryCount[generator->stackIndex] = generator->stackEntryCount[generator->stackIndex];
    }
    
    return rVal;
}

typedef struct
{
    BE_InstructionAllocator* allocator;
    BE_InstructionGenerator* generator;
    BE_Expressions* expressions;
    BE_InstructionList* instructions;
} BE_InstructionGeneratorContext;

BEPrivateAPI BEInstruction* BE_AddInstruction(BE_InstructionGeneratorContext context)
{
    BE_InstructionListNode* node = context.allocator->free;
    if (node)
    {
        context.allocator->free = context.allocator->free->next;
    }
    else
    {
        node = BEAlloc(context.allocator->allocator, BE_InstructionListNode);
    }
    
    BE_ClearStructToZero(*node);
    
    BE_InstructionList* list = context.instructions;
    if (context.generator->pendingInstructionStackCount)
    {
        list = &context.generator->pendingInstructions;
    }
    
    if (list->head)
    {
        list->tail = list->tail->next = node;
    }
    else
    {
        list->head = list->tail = node;
    }
    
    ++list->count;
    ++context.generator->instructionIndex;
    
    return &node->instruction;
}

BEPrivateAPI void BE_AddInstruction_Set(BE_InstructionGeneratorContext context, BE_StackEntry res, BEs64 value)
{
    BEAssert(res.stack);
    
    BEInstruction* instruction = BE_AddInstruction(context);
    instruction->type = BE_InstructionType_Set;
    instruction->result = res;
    instruction->set.value = value;
}

BEPrivateAPI void BE_AddInstruction_UnaryOperator(BE_InstructionGeneratorContext context, BE_InstructionType type, BE_StackEntry res, BE_StackEntry value)
{
    BEAssert(res.stack && value.stack);
    
    BEInstruction* instruction = BE_AddInstruction(context);
    instruction->type = type;
    instruction->result = res;
    instruction->unaryValue = value;
}

BEPrivateAPI void BE_AddInstruction_BinaryOperator(BE_InstructionGeneratorContext context, BE_InstructionType type, BE_StackEntry res, BE_StackEntry lhs, BE_StackEntry rhs)
{
    BEAssert(res.stack && lhs.stack && rhs.stack);
    
    BEInstruction* instruction = BE_AddInstruction(context);
    instruction->type = type;
    instruction->result = res;
    instruction->binary.left = lhs;
    instruction->binary.right = rhs;
}

BEPrivateAPI void BE_EvaluateAndFreeExpression(BE_InstructionGeneratorContext context, BE_ArithmeticError* outError, BE_Expression* expression, BE_StackEntry res);
BEPrivateAPI void BE_AddInstruction_StartCall(BE_InstructionGeneratorContext context, BEu32 stackEntriesUsed);
BEPrivateAPI void BE_AddInstruction_EndCall(BE_InstructionGeneratorContext context);

BEPrivateAPI void BE_ConvertIntToFloat(BE_InstructionGeneratorContext context, BE_StackEntry* entry)
{
    BE_StackEntry tmp = BE_PushLocalStackEntry(context.generator);
    
    BEInstruction* intToFloat = BE_AddInstruction(context);
    intToFloat->type = BE_InstructionType_IntToFloat;
    intToFloat->conversion.source = *entry;
    intToFloat->result = tmp;
    
    *entry = tmp;
}

BEPrivateAPI BE_StackEntry BE_OperatorValueToStackEntry(BE_InstructionGeneratorContext context, BE_ArithmeticError* outError, BEbool isFloat, BE_OperatorValue value)
{
    BE_StackEntry rVal;
    BE_ClearStructToZero(rVal);
    
    if (!*outError)
    {
        switch (value.type)
        {
            BE_InvalidDefault;
            
            case BE_OperatorValueType_Constant:
            {
                rVal = BE_PushLocalStackEntry(context.generator);
                BEInstruction* instruction = BE_AddInstruction(context);
                instruction->type = BE_InstructionType_Set;
                instruction->result = rVal;
                instruction->set.value = value.constant.i;
            } break;
            
            case BE_OperatorValueType_MemberOffset:
            {
                rVal = BE_MemberOffset();
                
                if (isFloat)
                {
                    BE_ConvertIntToFloat(context, &rVal);
                }
            } break;
            
            case BE_OperatorValueType_LocalVariable:
            {
                rVal = BE_Local(context.generator->parameterCount + value.localVariable.index);
                
                if (isFloat)
                {
                    BE_ConvertIntToFloat(context, &rVal);
                }
            } break;
            
            case BE_OperatorValueType_GlobalVariable:
            {
                rVal = BE_Global(value.globalVariable.index);
                
                if (isFloat)
                {
                    BE_ConvertIntToFloat(context, &rVal);
                }
            } break;
            
            case BE_OperatorValueType_Parameter:
            {
                rVal = BE_Local(value.parameter.index);
                
                if (isFloat)
                {
                    BE_ConvertIntToFloat(context, &rVal);
                }
            } break;
            
            case BE_OperatorValueType_Member:
            {
                rVal = BE_PushLocalStackEntry(context.generator);
                
                BEInstruction instruction;
                BE_ClearStructToZero(instruction);
                
                if (isFloat)
                {
                    instruction.type = BE_InstructionType_FetchMemberValueFloat;
                }
                else
                {
                    instruction.type = BE_InstructionType_FetchMemberValue;
                }
                
                BE_StackReset reset = BE_StackResetPoint(context.generator);
                for (BE_MemberPathNode* it = value.member.path.head; it; it = it->next)
                {
                    BE_StackEntry arrayIndex;
                    BE_ClearStructToZero(arrayIndex);
                    
                    if (BE_ExpressionIsValid(&it->arrayIndex))
                    {
                        arrayIndex = BE_PushLocalStackEntry(context.generator);
                        BE_EvaluateAndFreeExpression(context, outError, &it->arrayIndex, arrayIndex);
                    }
                    
                    instruction.fetchMember.path[instruction.fetchMember.pathCount].memberIndex = it->memberIndex;
                    instruction.fetchMember.path[instruction.fetchMember.pathCount].arrayIndex = arrayIndex;
                    ++instruction.fetchMember.pathCount;
                }
                
                instruction.result = rVal;
                
                *BE_AddInstruction(context) = instruction;
                
                BE_ResetStack(context.generator, reset);
            } break;
            
            case BE_OperatorValueType_ArrayCount:
            {
                rVal = BE_PushLocalStackEntry(context.generator);
                
                BEInstruction* instruction = BE_AddInstruction(context);
                instruction->type = BE_InstructionType_FetchArrayCount;
                
                BE_StackReset reset = BE_StackResetPoint(context.generator);
                for (BE_MemberPathNode* it = value.arrayCount.path.head; it; it = it->next)
                {
                    BE_StackEntry arrayIndex;
                    BE_ClearStructToZero(arrayIndex);
                    
                    if (BE_ExpressionIsValid(&it->arrayIndex))
                    {
                        arrayIndex = BE_PushLocalStackEntry(context.generator);
                        BE_EvaluateAndFreeExpression(context, outError, &it->arrayIndex, arrayIndex);
                    }
                    
                    instruction->fetchArrayCount.path[instruction->fetchArrayCount.pathCount].memberIndex = it->memberIndex;
                    instruction->fetchArrayCount.path[instruction->fetchArrayCount.pathCount].arrayIndex = arrayIndex;
                    ++instruction->fetchArrayCount.pathCount;
                }
                
                instruction->result = rVal;
                
                if (isFloat)
                {
                    BE_ConvertIntToFloat(context, &rVal);
                }
                
                BE_ResetStack(context.generator, reset);
            } break;
            
            case BE_OperatorValueType_SizeOfFile:
            {
                rVal = BE_PushLocalStackEntry(context.generator);
                
                BEInstruction* instruction = BE_AddInstruction(context);
                instruction->type = BE_InstructionType_FetchSizeOfFile;
                instruction->result = rVal;
                
                if (isFloat)
                {
                    BE_ConvertIntToFloat(context, &rVal);
                }
            } break;
            
            case BE_OperatorValueType_HasType:
            {
                rVal = BE_PushLocalStackEntry(context.generator);
                
                BE_StackReset reset = BE_StackResetPoint(context.generator);
                
                BE_StackEntry categoryNumber = BE_PushLocalStackEntry(context.generator);
                BE_EvaluateAndFreeExpression(context, outError, &value.hasType.categoryNumber, categoryNumber);
                
                BEInstruction* hasTypeInstruction = BE_AddInstruction(context);
                hasTypeInstruction->type = BE_InstructionType_HasType;
                hasTypeInstruction->categoryQuery.categoryIndex = value.hasType.category;
                hasTypeInstruction->categoryQuery.typeNumber = categoryNumber;
                hasTypeInstruction->result = rVal;
                
                if (isFloat)
                {
                    BE_ConvertIntToFloat(context, &rVal);
                }
                
                BE_ResetStack(context.generator, reset);
            } break;
            
            case BE_OperatorValueType_SizeOf:
            {
                rVal = BE_PushLocalStackEntry(context.generator);
                
                BE_StackReset reset = BE_StackResetPoint(context.generator);
                
                BE_StackEntry addressReset = BE_PushLocalStackEntry(context.generator);
                BE_StackEntry rootAddress = addressReset;
                BE_AddInstruction_UnaryOperator(context, BE_InstructionType_Copy, addressReset, BE_MemberOffset());
                
                if (BE_ExpressionIsValid(&value.sizeOf.address))
                {
                    rootAddress = BE_PushLocalStackEntry(context.generator);
                    
                    BE_EvaluateAndFreeExpression(context, outError, &value.sizeOf.address, BE_MemberOffset());
                    BE_AddInstruction_UnaryOperator(context, BE_InstructionType_Copy, rootAddress, BE_MemberOffset());
                }
                
                {
                    BEInstruction* hideInstruction = BE_AddInstruction(context);
                    hideInstruction->type = BE_InstructionType_HideMembers;
                }
                
                BE_StructStartAddressNode* typeStartInstruction = BE_FindStartAddressOfStruct(&context.generator->structStartAddresses, value.sizeOf.typeIndex);
                
                BE_AddInstruction_StartCall(context, typeStartInstruction->highestStackEntryUsed);
                {
                    BEInstruction* setReturnAddress = BE_AddInstruction(context);
                    setReturnAddress->type = BE_InstructionType_Set;
                    setReturnAddress->result = BE_ReturnAddress();
                    
                    BEInstruction* jump = BE_AddInstruction(context);
                    jump->type = BE_InstructionType_Jump;
                    jump->jump.newInstructionIndex = typeStartInstruction->instructionIndex;
                    
                    setReturnAddress->set.value = context.generator->instructionIndex;
                }
                BE_AddInstruction_EndCall(context);
                
                {
                    BEInstruction* hideInstruction = BE_AddInstruction(context);
                    hideInstruction->type = BE_InstructionType_ShowMembers;
                }
                
                BE_AddInstruction_BinaryOperator(context, BE_InstructionType_Sub, rVal, BE_MemberOffset(), rootAddress);
                BE_AddInstruction_UnaryOperator(context, BE_InstructionType_Copy, BE_MemberOffset(), addressReset);
                
                if (isFloat)
                {
                    BE_ConvertIntToFloat(context, &rVal);
                }
                
                BE_ResetStack(context.generator, reset);
            } break;
            
            case BE_OperatorValueType_Load:
            {
                // TODO(Jens): Create float load instruction.
                
                rVal = BE_PushLocalStackEntry(context.generator);
                
                BE_StackReset reset = BE_StackResetPoint(context.generator);
                
                BE_StackEntry addressReset;
                BE_ClearStructToZero(addressReset);
                
                if (BE_ExpressionIsValid(&value.load.address))
                {
                    addressReset = BE_PushLocalStackEntry(context.generator);
                    BE_AddInstruction_UnaryOperator(context, BE_InstructionType_Copy, addressReset, BE_MemberOffset());
                    BE_EvaluateAndFreeExpression(context, outError, &value.load.address, BE_MemberOffset());
                }
                
                BE_StackEntry size = BE_PushLocalStackEntry(context.generator);
                BE_EvaluateAndFreeExpression(context, outError, &value.load.size, size);
                
                BEInstruction* instruction = BE_AddInstruction(context);
                instruction->type = BE_InstructionType_FetchMemory;
                instruction->fetchMemory.size = size;
                instruction->result = rVal;
                
                if (value.load.isBig)
                {
                    instruction->fetchMemory.flags |= BEFetchMemoryFlag_BigEndian;
                }
                if (value.load.isSigned)
                {
                    instruction->fetchMemory.flags |= BEFetchMemoryFlag_Signed;
                }
                
                if (addressReset.stack != BE_Stack_Invalid)
                {
                    BE_AddInstruction_UnaryOperator(context, BE_InstructionType_Copy, BE_MemberOffset(), addressReset);
                }
                
                if (isFloat)
                {
                    BE_ConvertIntToFloat(context, &rVal);
                }
                
                BE_ResetStack(context.generator, reset);
            } break;
            
            case BE_OperatorValueType_CreatePlot:
            {
                rVal = BE_PushLocalStackEntry(context.generator);
                
                BEInstruction* instruction = BE_AddInstruction(context);
                instruction->type = BE_InstructionType_CreatePlot;
                instruction->result = rVal;
                
                if (isFloat)
                {
                    BE_ConvertIntToFloat(context, &rVal);
                }
            } break;
            
            case BE_OperatorValueType_StackEntry:
            {
                rVal = value.stackEntry.entry;
            } break;
        }
    }
    
    return rVal;
}

BEPrivateAPI BE_OperatorValue BE_PerformBinaryOperator(BE_InstructionGeneratorContext context, BE_ArithmeticError* outError, BEbool isFloat, BE_OperatorValue left, BE_punct op, BE_OperatorValue right)
{
    BE_OperatorValue rVal = { BE_OperatorValueType_Invalid };
    
    if (!*outError)
    {
        if (left.type == BE_OperatorValueType_Constant && right.type == BE_OperatorValueType_Constant)
        {
            rVal.type = BE_OperatorValueType_Constant;
            rVal.constant = BE_PerformConstantBinaryOperator(outError, isFloat, left.constant, op, right.constant);
        }
        else
        {
            rVal.type = BE_OperatorValueType_StackEntry;
            rVal.stackEntry.entry = BE_PushLocalStackEntry(context.generator);
            
            BE_StackEntry leftEntry = BE_OperatorValueToStackEntry(context, outError, isFloat, left);
            BE_StackEntry rightEntry  = BE_OperatorValueToStackEntry(context, outError, isFloat, right);
            
            BE_InstructionType instructionType = BE_BinaryOperatorToInstruction(isFloat, op);
            BEAssert(instructionType);
            
            BE_AddInstruction_BinaryOperator(context, instructionType, rVal.stackEntry.entry, leftEntry, rightEntry);
        }
    }
    
    return rVal;
}

BEPrivateAPI BE_OperatorValue BE_PerformUnaryOperator(BE_InstructionGeneratorContext context, BE_ArithmeticError* outError, BEbool isFloat, BE_punct op, BE_OperatorValue value)
{
    BE_OperatorValue rVal = { BE_OperatorValueType_Invalid };
    
    if (!*outError)
    {
        if (value.type == BE_OperatorValueType_Constant)
        {
            rVal.type = BE_OperatorValueType_Constant;
            rVal.constant = BE_PerformConstantUnaryOperator(isFloat, op, value.constant);
        }
        else
        {
            rVal.type = BE_OperatorValueType_StackEntry;
            rVal.stackEntry.entry = BE_PushLocalStackEntry(context.generator);
            
            BE_StackEntry valueEntry = BE_OperatorValueToStackEntry(context, outError, isFloat, value);
            
            BE_InstructionType instructionType = BE_UnaryOperatorToInstruction(isFloat, op);
            BEAssert(instructionType);
            
            BE_AddInstruction_UnaryOperator(context, instructionType, rVal.stackEntry.entry, valueEntry);
        }
    }
    
    return rVal;
}

BEPrivateAPI void BE_EvaluateAndFreeExpression(BE_InstructionGeneratorContext context, BE_ArithmeticError* outError, BE_Expression* expression, BE_StackEntry res)
{
    BE_OperatorValue expressionValue = { BE_OperatorValueType_Invalid };
    
    BEbool isFloat = expression->isFloat;
    
    if (!BE_ExpressionHasPendingEndParenthesis(expression) 
        && expression->termsSentinel->next != expression->termsSentinel 
        && !BE_PunctuationIsValid(expression->termsSentinel->prev->binaryOperator.op))
    {
        for (BE_ExpressionTerm* term = expression->termsSentinel->next; term != expression->termsSentinel; )
        {
            while (term->unaryOperators && term->unaryOperators->precedence >= term->binaryOperator.precedence)
            {
                BE_punct op = BE_PopUnaryOperator(context.expressions, term);
                term->value = BE_PerformUnaryOperator(context, outError, expression->isFloat, op, term->value);
            }
            
            BE_ExpressionTerm* lhs = term->prev;
            
            if (!term->unaryOperators && lhs != expression->termsSentinel)
            {
                // NOTE(Jens): `term` is RHS of expression, any unary operators of LHS is guaranteed to have lower precedence.
                
                if (lhs->binaryOperator.precedence >= term->binaryOperator.precedence)
                {
                    lhs->value = BE_PerformBinaryOperator(context, outError, expression->isFloat, lhs->value, lhs->binaryOperator.op, term->value);
                    lhs->binaryOperator = term->binaryOperator;
                    
                    BE_ExpressionTerm* toFree = term;
                    
                    term->prev->next = term->next;
                    term->next->prev = term->prev;
                    
                    term = term->prev;
                    
                    toFree->next = context.expressions->freeTerms;
                    context.expressions->freeTerms = toFree;
                }
                else
                {
                    term = term->next;
                }
            }
            else
            {
                // NOTE(Jens): Still have unary operators to evaluate, can't evaluate previous term.
                term = term->next;
            }
        }
        
        // NOTE(Jens): Expression should've been reduced to a single entry at this point with no unary or binary operators pending evaluation.
        
        BEAssert(expression->termsSentinel->next != expression->termsSentinel);
        BEAssert(expression->termsSentinel->next->next == expression->termsSentinel);
        
        BE_ExpressionTerm* resultTerm = expression->termsSentinel->next;
        BEAssert(!resultTerm->unaryOperators);
        BEAssert(!BE_PunctuationIsValid(resultTerm->binaryOperator.op));
        
        expressionValue = expression->termsSentinel->next->value;
        
        BE_FreeExpression(context.expressions, expression);
    }
    
    if (expressionValue.type == BE_OperatorValueType_Constant)
    {
        BEInstruction* instruction = BE_AddInstruction(context);
        instruction->type = BE_InstructionType_Set;
        instruction->result = res;
        instruction->set.value = expressionValue.constant.i;
    }
    else
    {
        // TODO(Jens): Don't copy, but if result is non-constant just push the result to 'res'.
        
        BE_StackEntry entry = BE_OperatorValueToStackEntry(context, outError, isFloat, expressionValue);
        BE_AddInstruction_UnaryOperator(context, BE_InstructionType_Copy, res, entry);
    }
}

BEPrivateAPI void BE_AddInstruction_Breakpoint(BE_InstructionGeneratorContext context)
{
    BEInstruction* instruction = BE_AddInstruction(context);
    instruction->type = BE_InstructionType_Breakpoint;
}

BEPrivateAPI void BE_AddInstruction_Print(BE_InstructionGeneratorContext context, BE_ArithmeticError* outError, BE_PrintStatement* printStatement)
{
    BE_StackReset reset = BE_StackResetPoint(context.generator);
    
    BE_StackEntry prefix;
    BE_ClearStructToZero(prefix);
    
    if (BE_ExpressionIsValid(&printStatement->expression))
    {
        prefix = BE_PushLocalStackEntry(context.generator);
        BE_EvaluateAndFreeExpression(context, outError, &printStatement->expression, prefix);
    }
    
    BEInstruction* instruction = BE_AddInstruction(context);
    instruction->type = BE_InstructionType_Print;
    instruction->print.value = prefix;
    instruction->print.prefixCharacterCount = printStatement->message.count;
    instruction->print.prefixCharacters = printStatement->message.characters;
    
    BE_ResetStack(context.generator, reset);
}

BEPrivateAPI void BE_AddInstruction_Assert(BE_InstructionGeneratorContext context, BE_ArithmeticError* outError, BE_AssertStatement* assertStatement)
{
    BE_StackReset reset = BE_StackResetPoint(context.generator);
    
    BE_StackEntry condition = BE_PushLocalStackEntry(context.generator);
    BE_EvaluateAndFreeExpression(context, outError, &assertStatement->expression, condition);
    
    BEInstruction* instruction = BE_AddInstruction(context);
    instruction->type = BE_InstructionType_Assert;
    instruction->assert.condition = condition;
    
    BE_ResetStack(context.generator, reset);
}

BEPrivateAPI void BE_AddInstruction_LocalVariable(BE_InstructionGeneratorContext context, BE_ArithmeticError* outError, BE_VariableDeclaration* variableDeclaration)
{
    BE_StackEntry entry = BE_PushLocalStackEntry(context.generator);
    
    if (BE_ExpressionIsValid(&variableDeclaration->initializationExpression))
    {
        BE_StackReset reset = BE_StackResetPoint(context.generator);
        BE_EvaluateAndFreeExpression(context, outError, &variableDeclaration->initializationExpression, entry);
        BE_ResetStack(context.generator, reset);
    }
    else
    {
        BEInstruction* instruction = BE_AddInstruction(context);
        instruction->type = BE_InstructionType_Set;
        instruction->result = entry;
        instruction->set.value = 0;
    }
}

BEPrivateAPI void BE_AddInstruction_StartCall(BE_InstructionGeneratorContext context, BEu32 stackEntriesUsed)
{
    BEInstruction* instruction = BE_AddInstruction(context);
    instruction->type = BE_InstructionType_PushFrame;
    instruction->pushFrame.sizeRequiredInBytes = 8 * (BE_LocalSymbol_Count + stackEntriesUsed);
    
    ++context.generator->stackIndex;
    BEAssert(context.generator->stackIndex < BE_ArrayCount(context.generator->stackEntryCount));
    
    BE_InitLocalStack(context.generator);
}

BEPrivateAPI void BE_AddInstruction_EndCall(BE_InstructionGeneratorContext context)
{
    BEInstruction* instruction = BE_AddInstruction(context);
    instruction->type = BE_InstructionType_PopFrame;
    
    BEAssert(context.generator->stackIndex != 0);
    --context.generator->stackIndex;
}

BEPrivateAPI void BE_AddInstruction_Layout(BE_InstructionGeneratorContext context, BE_LayoutStatement* layout)
{
    BE_StructStartAddressNode* startAddress = BE_FindStartAddressOfStruct(&context.generator->structStartAddresses, layout->typeIndex);
    
    BE_AddInstruction_StartCall(context, startAddress->highestStackEntryUsed);
    
    BEInstruction* setReturnAddress = BE_AddInstruction(context);
    setReturnAddress->type = BE_InstructionType_Set;
    setReturnAddress->result = BE_ReturnAddress();
    setReturnAddress->set.value = context.generator->instructionIndex + 1;
    
    BEInstruction* jumpInstruction = BE_AddInstruction(context);
    jumpInstruction->type = BE_InstructionType_Jump;
    jumpInstruction->jump.newInstructionIndex = startAddress->instructionIndex;
    
    BE_AddInstruction_EndCall(context);
}

BEPrivateAPI void BE_AddInstruction_Assignment(BE_InstructionGeneratorContext context, BE_ArithmeticError* outError, BE_Assignment* assignment)
{
    BE_StackEntry lhs;
    BE_ClearStructToZero(lhs);
    
    if (assignment->isGlobal)
    {
        if (assignment->variableIndex == BE_GlobalSymbol_MemberOffset)
        {
            lhs = BE_MemberOffset();
        }
        else
        {
            BEAssert(assignment->variableIndex >= BE_GlobalSymbol_Count);
            lhs = BE_Global(assignment->variableIndex - BE_GlobalSymbol_Count);
        }
    }
    else
    {
        // NOTE(Jens): Although technically it would be possible, we don't allow modifying the return address.
        
        BEAssert(assignment->variableIndex >= BE_LocalSymbol_Count);
        lhs = BE_Local(assignment->variableIndex - BE_LocalSymbol_Count);
    }
    
    BE_StackReset reset = BE_StackResetPoint(context.generator);
    
    if (BE_PunctuationsAreSame(assignment->op, BE_Punctuation("=")))
    {
        BE_EvaluateAndFreeExpression(context, outError, &assignment->rhs, lhs);
    }
    else
    {
        BE_StackEntry tmp = BE_PushLocalStackEntry(context.generator);
        BE_EvaluateAndFreeExpression(context, outError, &assignment->rhs, tmp);
        
        BE_Expression tmpExpression;
        BE_ClearStructToZero(tmpExpression);
        BE_InitExpression(context.allocator->allocator, context.expressions, &tmpExpression, 0);
        
        {
            BE_OperatorValue* value = BE_PushValue(&tmpExpression);
            value->type = BE_OperatorValueType_StackEntry;
            value->stackEntry.entry = lhs;
        }
        
        BE_PushOperator(context.allocator->allocator, context.expressions, &tmpExpression, BE_NonCompoundVersionOfOperator(assignment->op));
        
        
        {
            BE_OperatorValue* value = BE_PushValue(&tmpExpression);
            value->type = BE_OperatorValueType_StackEntry;
            value->stackEntry.entry = tmp;
        }
        
        BE_EvaluateAndFreeExpression(context, outError, &tmpExpression, lhs);
    }
    
    BE_ResetStack(context.generator, reset);
}

BEPrivateAPI BE_InstructionGenerator BE_StartInstructionGeneration(void)
{
    BE_InstructionGenerator rVal;
    BE_ClearStructToZero(rVal),
    
    BE_InitGlobalStack(&rVal);
    
    return rVal;
}

BEPrivateAPI void BE_PushInstructionRef(BEAllocatorType allocator, BE_InstructionRefs* refs, BEInstruction* ref)
{
    BE_InstructionRefStack* node = refs->freeRefs;
    if (node)
    {
        refs->freeRefs = refs->freeRefs->prev;
    }
    else
    {
        node = BEAlloc(allocator, BE_InstructionRefStack);
    }
    
    BE_ClearStructToZero(*node);
    
    node->ref = ref;
    
    node->prev = refs->stack;
    refs->stack = node;
}

BEPrivateAPI BEInstruction* BE_PopInstructionRef(BE_InstructionRefs* refs)
{
    BE_InstructionRefStack* node = refs->stack;
    refs->stack = refs->stack->prev;
    
    node->prev = refs->freeRefs;
    refs->freeRefs = node;
    
    return node->ref;
}

BEPrivateAPI void BE_PushIfChain(BEAllocatorType allocator, BE_InstructionRefs* refs)
{
    BE_InstructionIfChain* ifChain = refs->freeIfChains;
    if (ifChain)
    {
        refs->freeIfChains = refs->freeIfChains->prev;
    }
    else
    {
        ifChain = BEAlloc(allocator, BE_InstructionIfChain);
    }
    
    BE_ClearStructToZero(*ifChain);
    
    ifChain->prev = refs->ifChains;
    refs->ifChains = ifChain;
}

BEPrivateAPI void BE_AddEndOfIfScope(BE_InstructionGeneratorContext context)
{
    BE_InstructionRefs* refs = &context.generator->instructionRefs;
    
    BE_InstructionIfChain* ifChain = refs->ifChains;
    BE_InstructionRefStack* endOfIf = refs->freeRefs;
    
    if (endOfIf)
    {
        refs->freeRefs = refs->freeRefs->prev;
    }
    else
    {
        endOfIf = BEAlloc(context.allocator->allocator, BE_InstructionRefStack);
    }
    
    BE_ClearStructToZero(*endOfIf);
    
    endOfIf->ref = BE_AddInstruction(context);
    endOfIf->ref->type = BE_InstructionType_None;
    
    endOfIf->prev = ifChain->jumpToEndInstructions;
    ifChain->jumpToEndInstructions = endOfIf;
}

BEPrivateAPI void BE_PopIfChain(BE_InstructionRefs* refs, BEu32 endOfChainInstructionIndex)
{
    BEAssert(refs->ifChains);
    BEAssert(refs->ifChains->jumpToEndInstructions);
    
    BE_InstructionIfChain* ifChain = refs->ifChains;
    refs->ifChains = refs->ifChains->prev;
    
    while (ifChain->jumpToEndInstructions)
    {
        BE_InstructionRefStack* jumpToEndInstruction = ifChain->jumpToEndInstructions;
        ifChain->jumpToEndInstructions = ifChain->jumpToEndInstructions->prev;
        
        BEAssert(jumpToEndInstruction->ref->type == BE_InstructionType_None);
        jumpToEndInstruction->ref->type = BE_InstructionType_Jump;
        jumpToEndInstruction->ref->jump.newInstructionIndex = endOfChainInstructionIndex;
        
        jumpToEndInstruction->prev = refs->freeRefs;
        refs->freeRefs = jumpToEndInstruction;
    }
    
    ifChain->prev = refs->freeIfChains;
    refs->freeIfChains = ifChain;
}

BEPrivateAPI BE_InstructionList BE_FeedInstructionGenerator(BE_InstructionAllocator* allocator, BE_Expressions* expressions, BE_InstructionGenerator* generator, BE_ArithmeticError* outError, BE_StatementType statementType, BE_Statement* statement)
{
#define PushInstructionStack() \
do { \
++generator->pendingInstructionStackCount; \
BE_PushScopeStackReset(allocator->allocator, generator); \
} while (0)
    
#define PopInstructionStack() \
do { \
if (--generator->pendingInstructionStackCount == 0) \
{ \
rVal = generator->pendingInstructions; \
BE_ClearStructToZero(generator->pendingInstructions); \
} \
BE_PopScopeStackReset(generator); \
} while (0)
    
    BE_InstructionList rVal;
    BE_ClearStructToZero(rVal);
    
    BE_InstructionGeneratorContext context;
    context.allocator = allocator;
    context.generator = generator;
    context.expressions = expressions;
    context.instructions = &rVal;
    
    BEbool endIfChain = generator->previousStatementWasEndOfIf;
    
    if (statementType == BE_StatementType_Else || statementType == BE_StatementType_If && statement->ifStatement.isElseIf)
    {
        endIfChain = 0;
    }
    
    if (endIfChain)
    {
        BE_PopIfChain(&generator->instructionRefs, generator->instructionIndex);
    }
    
    generator->previousStatementWasEndOfIf = 0;
    
    switch (statementType)
    {
        BE_InvalidDefault;
        
        // NOTE(Jens): Statements that don't generate instructions or change generator state.
        case BE_StatementType_End:
        case BE_StatementType_EnumDeclaration:
        case BE_StatementType_TypedefDeclaration:
        case BE_StatementType_Import:
        case BE_StatementType_DefaultDirective:
        case BE_StatementType_Else:
        {
        } break;
        
        case BE_StatementType_Breakpoint:
        {
            BE_AddInstruction_Breakpoint(context);
        } break;
        
        case BE_StatementType_Print:
        {
            BE_AddInstruction_Print(context, outError, &statement->print);
        } break;
        
        case BE_StatementType_Assert:
        {
            BE_AddInstruction_Assert(context, outError, &statement->assert);
        } break;
        
        case BE_StatementType_VariableDeclaration:
        {
            BE_AddInstruction_LocalVariable(context, outError, &statement->variableDeclaration);
        } break;
        
        case BE_StatementType_Layout:
        {
            if (!statement->layout.isInImportedFile)
            {
                BE_AddInstruction_Layout(context, &statement->layout);
            }
        } break;
        
        case BE_StatementType_StructHeader:
        {
            BEAssert(!generator->pendingInstructionStackCount);
            BEAssert(!generator->pendingInstructions.head);
            
            PushInstructionStack();
            
            BEInstruction* jumpOverStructInstruction = BE_AddInstruction(context);
            jumpOverStructInstruction->type = BE_InstructionType_Jump;
            BE_PushInstructionRef(allocator->allocator, &generator->instructionRefs, jumpOverStructInstruction);
            // NOTE(Jens): Address to jump to is determined after struct has been evaluated.
            
            BE_AddStartOfStruct(allocator->allocator, generator);
            
            BEInstruction* typeIndexInstruction = BE_AddInstruction(context);
            typeIndexInstruction->type = BE_InstructionType_StructIndex;
            typeIndexInstruction->structIndex.index = statement->structHeader.structIndex;
            
            BEAssert(generator->stackIndex == 0);
            ++generator->stackIndex;
            BE_InitLocalStack(generator);
            
            generator->parameterCount = statement->structHeader.parameters.count;
            for (BEu32 paramIndex = 0; paramIndex < statement->structHeader.parameters.count; ++paramIndex)
            {
                BE_PushLocalStackEntry(generator); // NOTE(Jens): One stack entry for each parameter.
            }
        } break;
        
        case BE_StatementType_EndOf_Struct:
        {
            BEAssert(generator->pendingInstructionStackCount);
            BEAssert(generator->stackIndex == 1);
            
            generator->structStartAddresses.tail->highestStackEntryUsed = generator->maxStackEntryCount[1];
            if (generator->structStartAddresses.globalHighestStackEntryUsed < generator->structStartAddresses.tail->highestStackEntryUsed)
            {
                generator->structStartAddresses.globalHighestStackEntryUsed = generator->structStartAddresses.tail->highestStackEntryUsed;
            }
            
            BEInstruction* returnJump = BE_AddInstruction(context);
            returnJump->type = BE_InstructionType_Jump;
            returnJump->jump.dynamicNewInstructionIndex = BE_ReturnAddress();
            
            BEInstruction* pendingJump = BE_PopInstructionRef(&generator->instructionRefs);
            BEAssert(pendingJump->type == BE_InstructionType_Jump);
            pendingJump->jump.newInstructionIndex = generator->instructionIndex;
            
            PopInstructionStack();
            
            --generator->stackIndex;
            generator->parameterCount = 0;
        } break;
        
        case BE_StatementType_Assignment:
        {
            BE_AddInstruction_Assignment(context, outError, &statement->assignment);
        } break;
        
        case BE_StatementType_If:
        {
            if (statement->ifStatement.isElseIf)
            {
            }
            else
            {
                BE_PushIfChain(allocator->allocator, &generator->instructionRefs);
                PushInstructionStack();
            }
            
            BE_StackReset reset = BE_StackResetPoint(generator);
            
            BE_StackEntry condition = BE_PushLocalStackEntry(generator);
            
            BE_EvaluateAndFreeExpression(context, outError, &statement->ifStatement.condition, condition);
            BE_AddInstruction_UnaryOperator(context, BE_InstructionType_LogicalNot, condition, condition);
            
            BEInstruction* jumpToElseInstruction = BE_AddInstruction(context);
            jumpToElseInstruction->type = BE_InstructionType_Jump;
            jumpToElseInstruction->jump.condition = condition;
            BE_PushInstructionRef(allocator->allocator, &generator->instructionRefs, jumpToElseInstruction);
            
            BE_ResetStack(generator, reset);
        } break;
        
        case BE_StatementType_EndOf_If:
        {
            generator->previousStatementWasEndOfIf = 1;
            
            BE_AddEndOfIfScope(context);
            
            BEInstruction* jumpToElseInstruction = BE_PopInstructionRef(&generator->instructionRefs);
            BEAssert(jumpToElseInstruction->type == BE_InstructionType_Jump);
            jumpToElseInstruction->jump.newInstructionIndex = generator->instructionIndex;
        } break;
        
        case BE_StatementType_EndOf_Else:
        {
            BE_PopIfChain(&generator->instructionRefs, generator->instructionIndex);
            PopInstructionStack();
        } break;
        
        case BE_StatementType_For:
        {
            PushInstructionStack();
            
            if (statement->forLoop.variable.name.count)
            {
                BE_AddInstruction_LocalVariable(context, outError, &statement->forLoop.variable);
            }
            
            BEInstruction* jumpToConditionCheck = BE_AddInstruction(context);
            jumpToConditionCheck->type = BE_InstructionType_Jump;
            
            BE_PushStartOfIteration(allocator->allocator, generator, generator->instructionIndex);
            
            if (BE_PunctuationIsValid(statement->forLoop.assignment.op))
            {
                BE_AddInstruction_Assignment(context, outError, &statement->forLoop.assignment);
            }
            
            jumpToConditionCheck->jump.newInstructionIndex = generator->instructionIndex;
            
            if (BE_ExpressionIsValid(&statement->forLoop.endCondition))
            {
                BE_StackReset reset = BE_StackResetPoint(generator);
                
                BE_StackEntry condition = BE_PushLocalStackEntry(generator);
                
                BE_EvaluateAndFreeExpression(context, outError, &statement->forLoop.endCondition, condition);
                BE_AddInstruction_UnaryOperator(context, BE_InstructionType_LogicalNot, condition, condition);
                
                BEInstruction* jumpAwayFromLoop = BE_AddInstruction(context);
                jumpAwayFromLoop->type = BE_InstructionType_Jump;
                jumpAwayFromLoop->jump.condition = condition;
                BE_PushInstructionRef(allocator->allocator, &generator->instructionRefs, jumpAwayFromLoop);
                
                BE_ResetStack(generator, reset);
            }
            else
            {
                BE_PushInstructionRef(allocator->allocator, &generator->instructionRefs, 0);
            }
            
        } break;
        
        case BE_StatementType_EndOf_For:
        {
            BEu32 instructionIndex = BE_PopStartOfIteration(generator);
            
            BEInstruction* jumpToStartOfIteration = BE_AddInstruction(context);
            jumpToStartOfIteration->type = BE_InstructionType_Jump;
            jumpToStartOfIteration->jump.newInstructionIndex = instructionIndex;
            
            BEInstruction* jumpAwayFromLoop = BE_PopInstructionRef(&generator->instructionRefs);
            if (jumpAwayFromLoop)
            {
                jumpAwayFromLoop->jump.newInstructionIndex = generator->instructionIndex;
            }
            
            PopInstructionStack();
        } break;
        
        case BE_StatementType_While:
        {
            PushInstructionStack();
            
            BE_PushStartOfIteration(allocator->allocator, generator, generator->instructionIndex);
            
            BE_StackReset reset = BE_StackResetPoint(generator);
            
            BE_StackEntry condition = BE_PushLocalStackEntry(generator);
            BE_EvaluateAndFreeExpression(context, outError, &statement->whileLoop.condition, condition);
            BE_AddInstruction_UnaryOperator(context, BE_InstructionType_LogicalNot, condition, condition);
            
            BEInstruction* jumpAwayFromLoop = BE_AddInstruction(context);
            jumpAwayFromLoop->type = BE_InstructionType_Jump;
            jumpAwayFromLoop->jump.condition = condition;
            BE_PushInstructionRef(allocator->allocator, &generator->instructionRefs, jumpAwayFromLoop);
            
            BE_ResetStack(generator, reset);
        } break;
        
        case BE_StatementType_EndOf_While:
        {
            BEu32 instructionIndex = BE_PopStartOfIteration(generator);
            
            BEInstruction* jumpToStartOfIteration = BE_AddInstruction(context);
            jumpToStartOfIteration->type = BE_InstructionType_Jump;
            jumpToStartOfIteration->jump.newInstructionIndex = instructionIndex;
            
            BEInstruction* jumpAwayFromLoop = BE_PopInstructionRef(&generator->instructionRefs);
            jumpAwayFromLoop->jump.newInstructionIndex = generator->instructionIndex;
            
            PopInstructionStack();
        } break;
        
        case BE_StatementType_Do:
        {
            BE_PushStartOfIteration(allocator->allocator, generator, generator->instructionIndex);
        } break;
        
        case BE_StatementType_EndOf_DoWhile:
        {
            BE_StackReset reset = BE_StackResetPoint(generator);
            
            BE_StackEntry condition = BE_PushLocalStackEntry(generator);
            BE_EvaluateAndFreeExpression(context, outError, &statement->whileLoop.condition, condition);
            
            BEInstruction* jumpAwayFromLoop = BE_AddInstruction(context);
            jumpAwayFromLoop->type = BE_InstructionType_Jump;
            jumpAwayFromLoop->jump.condition = condition;
            jumpAwayFromLoop->jump.newInstructionIndex = BE_PopStartOfIteration(generator);
            
            BE_ResetStack(generator, reset);
        } break;
        
        case BE_StatementType_Plot_X:
        case BE_StatementType_Plot_Y:
        {
            BE_StackReset reset = BE_StackResetPoint(generator);
            
            BE_StackEntry plotId = BE_PushLocalStackEntry(generator);
            BE_StackEntry value = BE_PushLocalStackEntry(generator);
            
            BEAssert(!statement->plot.plotId.isFloat);
            BEAssert(statement->plot.value_f64.isFloat);
            
            BE_EvaluateAndFreeExpression(context, outError, &statement->plot.plotId, plotId);
            BE_EvaluateAndFreeExpression(context, outError, &statement->plot.value_f64, value);
            
            BEInstruction* plotInstruction = BE_AddInstruction(context);
            if (statementType == BE_StatementType_Plot_X)
            {
                plotInstruction->type = BE_InstructionType_Plot_F8_X;
            }
            else
            {
                plotInstruction->type = BE_InstructionType_Plot_F8_Y;
            }
            
            plotInstruction->plot.plotId = plotId;
            plotInstruction->plot.value_f64 = value;
            
            BE_ResetStack(generator, reset);
        } break;
        
        case BE_StatementType_Plot_XY:
        {
            BE_StackReset reset = BE_StackResetPoint(generator);
            
            BE_StackEntry plotId = BE_PushLocalStackEntry(generator);
            BE_StackEntry valueX = BE_PushLocalStackEntry(generator);
            BE_StackEntry valueY = BE_PushLocalStackEntry(generator);
            
            BEAssert(!statement->plotXY.plotId.isFloat);
            BEAssert(statement->plotXY.x.isFloat);
            BEAssert(statement->plotXY.y.isFloat);
            
            BE_EvaluateAndFreeExpression(context, outError, &statement->plotXY.plotId, plotId);
            BE_EvaluateAndFreeExpression(context, outError, &statement->plotXY.x, valueX);
            BE_EvaluateAndFreeExpression(context, outError, &statement->plotXY.y, valueY);
            
            BEInstruction* plotInstructionX = BE_AddInstruction(context);
            BEInstruction* plotInstructionY = BE_AddInstruction(context);
            
            plotInstructionX->type = BE_InstructionType_Plot_F8_X;
            plotInstructionY->type = BE_InstructionType_Plot_F8_Y;
            
            plotInstructionX->plot.plotId = plotInstructionY->plot.plotId = plotId;
            
            plotInstructionX->plot.value_f64 = valueX;
            plotInstructionY->plot.value_f64 = valueY;
            
            BE_ResetStack(generator, reset);
        } break;
        
        case BE_StatementType_Plot_Member_X:
        case BE_StatementType_Plot_Member_Y:
        {
            BE_StackReset reset = BE_StackResetPoint(generator);
            
            BE_StackEntry plotId = BE_PushLocalStackEntry(generator);
            BE_EvaluateAndFreeExpression(context, outError, &statement->plotMember.plotId, plotId);
            
            BE_StackEntry arrayCount = BE_PushLocalStackEntry(generator);
            
            {
                BE_StackReset reset2 = BE_StackResetPoint(generator);
                
                BEInstruction* fetchArrayCountInstruction = BE_AddInstruction(context);
                fetchArrayCountInstruction->type = BE_InstructionType_FetchArrayCount;
                fetchArrayCountInstruction->result = arrayCount;
                
                for (BE_MemberPathNode* it = statement->plotMember.member.head; it; it = it->next)
                {
                    BE_StackEntry arrayIndex;
                    BE_ClearStructToZero(arrayIndex);
                    
                    if (BE_ExpressionIsValid(&it->arrayIndex))
                    {
                        arrayIndex = BE_PushLocalStackEntry(generator);
                        BE_Expression tmp = BE_CopyExpression(allocator->allocator, expressions, &it->arrayIndex);
                        BE_EvaluateAndFreeExpression(context, outError, &tmp, arrayIndex);
                    }
                    
                    fetchArrayCountInstruction->fetchArrayCount.path[fetchArrayCountInstruction->fetchArrayCount.pathCount].memberIndex = it->memberIndex;
                    fetchArrayCountInstruction->fetchArrayCount.path[fetchArrayCountInstruction->fetchArrayCount.pathCount].arrayIndex = arrayIndex;
                    ++fetchArrayCountInstruction->fetchArrayCount.pathCount;
                }
                
                BE_ResetStack(generator, reset2);
            }
            
            BE_StackEntry index = BE_PushLocalStackEntry(generator);
            
            BE_StackEntry increment = BE_PushLocalStackEntry(generator);
            BE_StackEntry loopHasEnded = BE_PushLocalStackEntry(generator);
            BE_StackEntry memberValue = BE_PushLocalStackEntry(generator);
            
            BE_AddInstruction_Set(context, index, -1);
            BE_AddInstruction_Set(context, increment, 1);
            
            BEu32 incrementInstructionIndex = generator->instructionIndex; 
            BE_AddInstruction_BinaryOperator(context, BE_InstructionType_Add, index, index, increment);
            
            BE_AddInstruction_BinaryOperator(context, BE_InstructionType_GreaterOrEqual, loopHasEnded, index, arrayCount);
            BEInstruction* jumpOut = BE_AddInstruction(context);
            jumpOut->type = BE_InstructionType_Jump;
            jumpOut->jump.condition = loopHasEnded;
            // NOTE(Jens): 'jumpOut' new instruction index is set after other instructions have been pushed.
            
            {
                BE_StackReset reset2 = BE_StackResetPoint(generator);
                
                BEInstruction* fetchMemberValue = BE_AddInstruction(context);
                fetchMemberValue->type = BE_InstructionType_FetchMemberValueFloat;
                fetchMemberValue->result = memberValue;
                
                for (BE_MemberPathNode* it = statement->plotMember.member.head; it; it = it->next)
                {
                    BE_StackEntry arrayIndex;
                    BE_ClearStructToZero(arrayIndex);
                    
                    if (BE_ExpressionIsValid(&it->arrayIndex))
                    {
                        arrayIndex = BE_PushLocalStackEntry(generator);
                        BE_EvaluateAndFreeExpression(context, outError, &it->arrayIndex, arrayIndex);
                    }
                    
                    fetchMemberValue->fetchMember.path[fetchMemberValue->fetchMember.pathCount].memberIndex = it->memberIndex;
                    fetchMemberValue->fetchMember.path[fetchMemberValue->fetchMember.pathCount].arrayIndex = arrayIndex;
                    ++fetchMemberValue->fetchMember.pathCount;
                }
                
                fetchMemberValue->fetchMember.path[fetchMemberValue->fetchMember.pathCount - 1].arrayIndex = index;
                
                BE_ResetStack(generator, reset2);
            }
            
            BEInstruction* plotInstruction = BE_AddInstruction(context);
            if (statementType == BE_StatementType_Plot_Member_X)
            {
                plotInstruction->type = BE_InstructionType_Plot_F8_X;
            }
            else
            {
                plotInstruction->type = BE_InstructionType_Plot_F8_Y;
            }
            
            plotInstruction->plot.plotId = plotId;
            plotInstruction->plot.value_f64 = memberValue;
            
            BEInstruction* jumpBackInstruction = BE_AddInstruction(context);
            jumpBackInstruction->type = BE_InstructionType_Jump;
            jumpBackInstruction->jump.newInstructionIndex = incrementInstructionIndex;
            
            jumpOut->jump.newInstructionIndex = generator->instructionIndex;
            
            BE_ResetStack(generator, reset);
        } break;
        
        case BE_StatementType_MemberDeclaration:
        {
            BE_StackReset reset = BE_StackResetPoint(generator);
            
            BE_StackEntry addressReset;
            BE_ClearStructToZero(addressReset);
            
            if (statement->memberDeclaration.isExternal)
            {
                addressReset = BE_PushLocalStackEntry(generator);
                BE_AddInstruction_UnaryOperator(context, BE_InstructionType_Copy, addressReset, BE_MemberOffset());
            }
            
            if (BE_ExpressionIsValid(&statement->memberDeclaration.address))
            {
                BE_EvaluateAndFreeExpression(context, outError, &statement->memberDeclaration.address, BE_MemberOffset());
            }
            
            BE_Expression* alignmentExpression;
            
            {
                BE_TypeDeclaration* memberType = &statement->memberDeclaration.type;
                
                do
                {
                    alignmentExpression = &memberType->alignmentExpression;
                    
                    if (memberType->variant == BE_TypeVariant_Typedef)
                    {
                        memberType = &memberType->typedefVariant.def->def.type;
                    }
                    else
                    {
                        break;
                    }
                }
                while (!BE_ExpressionIsValid(alignmentExpression));
            }
            
            if (BE_ExpressionIsValid(alignmentExpression))
            {
                // NOTE(Jens): Calculation is:
                // `@ = @ + ((alignment - (@ % alignment)) % alignment)`
                //
                // padding <- @ % alignment
                // padding <- alignment - padding
                // padding <- padding % alignment
                // @ <- @ + padding
                //
                // This could be optimized if we check for constant expression power-of-two alignment.
                //
                
                // TODO(Jens): Optimize.
                
                BE_StackReset alignmentReset = BE_StackResetPoint(generator);
                
                BE_StackEntry alignment = BE_PushLocalStackEntry(generator);
                BE_StackEntry padding = BE_PushLocalStackEntry(generator);
                
                BE_Expression tmpExpression = BE_CopyExpression(allocator->allocator, expressions, alignmentExpression);
                BE_EvaluateAndFreeExpression(context, outError, &tmpExpression, alignment);
                
                BE_AddInstruction_BinaryOperator(context, BE_InstructionType_Mod, padding, BE_MemberOffset(), alignment);
                BE_AddInstruction_BinaryOperator(context, BE_InstructionType_Sub, padding, alignment, padding);
                BE_AddInstruction_BinaryOperator(context, BE_InstructionType_Mod, padding, padding, alignment);
                BE_AddInstruction_BinaryOperator(context, BE_InstructionType_Add, BE_MemberOffset(), BE_MemberOffset(), padding);
                
                BE_ResetStack(generator, alignmentReset);
            }
            
            BE_TypeDeclaration* type = BE_UnwrapTypedefs(&statement->memberDeclaration.type);
            
            BEbool isHidden = statement->memberDeclaration.isHidden || (type->variant == BE_TypeVariant_Scalar 
                                                                        && type->scalarVariant.baseType == BE_ScalarBaseType_hidden);
            
            if (isHidden)
            {
                BEInstruction* hideInstruction = BE_AddInstruction(context);
                hideInstruction->type = BE_InstructionType_HideMembers;
            }
            
            BEbool isArray = statement->memberDeclaration.isArray;
            
            if (type->variant == BE_TypeVariant_Scalar)
            {
                BE_StackReset sizeReset = BE_StackResetPoint(generator);
                
                BE_StackEntry size = BE_PushLocalStackEntry(generator);
                if (BE_ExpressionIsValid(&type->scalarVariant.sizeExpression))
                {
                    BE_Expression tmpExpression = BE_CopyExpression(allocator->allocator, expressions, &type->scalarVariant.sizeExpression);
                    BE_EvaluateAndFreeExpression(context, outError, &tmpExpression, size);
                }
                else
                {
                    // NOTE(Jens): All non-enum scalars must have size expression. Should've be caught at parsing stage.
                    
                    BEAssert(type->scalarVariant.enumType);
                    BEAssert(type->scalarVariant.enumType->size);
                    
                    BEInstruction* setInstruction = BE_AddInstruction(context);
                    setInstruction->type = BE_InstructionType_Set;
                    setInstruction->result = size;
                    setInstruction->set.value = type->scalarVariant.enumType->size;
                }
                
                BE_StackEntry arrayCount;
                BE_ClearStructToZero(arrayCount);
                
                if (isArray)
                {
                    arrayCount = BE_PushLocalStackEntry(generator);
                    BE_EvaluateAndFreeExpression(context, outError, &statement->memberDeclaration.arrayCount, arrayCount);
                }
                
                BEInstruction* exportInstruction = BE_AddInstruction(context);
                exportInstruction->scalarExport.memberIndex = statement->memberDeclaration.memberIndex;
                exportInstruction->scalarExport.size = size;
                exportInstruction->scalarExport.arrayCount = arrayCount;
                exportInstruction->scalarExport.isBigEndian = !!(type->scalarVariant.byteOrder & BE_ScalarByteOrder_big);
                exportInstruction->scalarExport.displayType = type->scalarVariant.displayType;
                
                switch (type->scalarVariant.baseType)
                {
                    BE_InvalidDefault;
                    
                    case BE_ScalarBaseType_u:
                    {
                        exportInstruction->type = BE_InstructionType_ExportScalar_u;
                    } break;
                    
                    case BE_ScalarBaseType_s:
                    {
                        exportInstruction->type = BE_InstructionType_ExportScalar_s;
                    } break;
                    
                    case BE_ScalarBaseType_f:
                    {
                        exportInstruction->type = BE_InstructionType_ExportScalar_f;
                    } break;
                    
                    case BE_ScalarBaseType_hidden:
                    case BE_ScalarBaseType_raw:
                    {
                        exportInstruction->type = BE_InstructionType_ExportScalar_raw;
                    } break;
                    
                    case BE_ScalarBaseType_string:
                    {
                        exportInstruction->type = BE_InstructionType_ExportScalar_string;
                    } break;
                }
                
                if (type->scalarVariant.enumType)
                {
                    exportInstruction->scalarExport.displayType = -1 - (BEs32)type->scalarVariant.enumType->index;
                }
                
                if (isArray)
                {
                    BE_AddInstruction_BinaryOperator(context, BE_InstructionType_Mul, size, size, arrayCount);
                }
                
                BE_AddInstruction_BinaryOperator(context, BE_InstructionType_Add, BE_MemberOffset(), BE_MemberOffset(), size);
                
                BE_ResetStack(generator, sizeReset);
            }
            else
            {
                
                BE_StackEntry firstArgument;
                BE_ClearStructToZero(firstArgument);
                
                for (BEu32 i = 0; i < type->argumentCount; ++i)
                {
                    BE_StackEntry argument = BE_PushLocalStackEntry(generator);
                    
                    BE_StackReset argumentReset = BE_StackResetPoint(generator);
                    
                    {
                        BE_Expression tmpExpression = BE_CopyExpression(allocator->allocator, expressions, &type->arguments[i]);
                        BE_EvaluateAndFreeExpression(context, outError, &tmpExpression, argument);
                    }
                    
                    if (i == 0)
                    {
                        firstArgument = argument;
                    }
                    
                    BE_ResetStack(generator, argumentReset);
                }
                
                BEInstruction* jumpToPopFrame = 0;
                
                BE_StackEntry arrayCount;
                BE_ClearStructToZero(arrayCount);
                
                BE_StackEntry indexIncrement;
                BE_ClearStructToZero(indexIncrement);
                
                BE_StackEntry arrayIndexCompare;
                BE_ClearStructToZero(arrayIndexCompare);
                
                if (isArray)
                {
                    arrayCount = BE_PushLocalStackEntry(generator);
                    BE_EvaluateAndFreeExpression(context, outError, &statement->memberDeclaration.arrayCount, arrayCount);
                    
                    indexIncrement = BE_PushLocalStackEntry(generator);
                    
                    BEInstruction* setInstruction = BE_AddInstruction(context);
                    setInstruction->type = BE_InstructionType_Set;
                    setInstruction->result = indexIncrement;
                    setInstruction->set.value = 1;
                    
                    arrayIndexCompare = BE_PushLocalStackEntry(generator);
                }
                
                BE_StackEntry categoryStartAddress;
                BE_ClearStructToZero(categoryStartAddress);
                
                if (type->variant == BE_TypeVariant_Category)
                {
                    categoryStartAddress = BE_PushLocalStackEntry(generator);
                    
                    BE_StackReset categoryReset = BE_StackResetPoint(generator);
                    
                    BE_StackEntry categoryNumber = BE_PushLocalStackEntry(generator);
                    
                    {
                        BE_Expression tmpExpression = BE_CopyExpression(allocator->allocator, expressions, &type->categoryVariant.categoryIdentifier);
                        BE_EvaluateAndFreeExpression(context, outError, &tmpExpression, BE_FromParentScope(categoryNumber));
                    }
                    
                    BEInstruction* fetchStartAddress = BE_AddInstruction(context);
                    fetchStartAddress->type = BE_InstructionType_FetchStartOfType;
                    fetchStartAddress->result = categoryStartAddress;
                    fetchStartAddress->categoryQuery.categoryIndex = type->categoryVariant.category->index;
                    fetchStartAddress->categoryQuery.typeNumber = BE_FromParentScope(categoryNumber);
                    
                    BE_ResetStack(generator, categoryReset);
                }
                
                BE_StructStartAddressNode* startAddress = 0;
                if (type->variant == BE_TypeVariant_Struct)
                {
                    startAddress = BE_FindStartAddressOfStruct(&context.generator->structStartAddresses, type->structVariant.type->index);
                    BE_AddInstruction_StartCall(context, startAddress->highestStackEntryUsed);
                }
                else
                {
                    // TODO(Jens): Could fetch size of the frame instead of over-allocating.
                    BE_AddInstruction_StartCall(context, generator->structStartAddresses.globalHighestStackEntryUsed);
                }
                
                for (BEu32 i = 0; i < type->argumentCount; ++i)
                {
                    BE_StackEntry argument = firstArgument;
                    argument.index += i;
                    
                    BE_StackEntry param = BE_PushLocalStackEntry(generator);
                    BEAssert(param.index == BE_LocalSymbol_Count + i);
                    
                    BE_AddInstruction_UnaryOperator(context, BE_InstructionType_Copy, param, BE_FromParentScope(argument));
                }
                
                BEInstruction* pushStruct = BE_AddInstruction(context);
                pushStruct->type = BE_InstructionType_PushStruct;
                pushStruct->beginStruct.memberIndex = statement->memberDeclaration.memberIndex;
                pushStruct->beginStruct.arrayCount = BE_FromParentScope(arrayCount);
                
                BEInstruction* setReturnAddress = BE_AddInstruction(context);
                setReturnAddress->type = BE_InstructionType_Set;
                setReturnAddress->result = BE_ReturnAddress();
                
                BEInstruction* setArrayIndex = BE_AddInstruction(context);
                setArrayIndex->type = BE_InstructionType_Set;
                setArrayIndex->result = BE_ArrayIndex();
                
                BEu32 arrayIncrementInstruction = 0;
                
                if (statement->memberDeclaration.isInternal)
                {
                    pushStruct->type = BE_InstructionType_PushStructInternal;
                }
                
                if (isArray)
                {
                    setArrayIndex->set.value = -1;
                    arrayIncrementInstruction = generator->instructionIndex;
                    BE_AddInstruction_BinaryOperator(context, BE_InstructionType_Add, BE_ArrayIndex(), BE_ArrayIndex(), BE_FromParentScope(indexIncrement));
                    BE_AddInstruction_BinaryOperator(context, BE_InstructionType_GreaterOrEqual, BE_FromParentScope(arrayIndexCompare), BE_ArrayIndex(), BE_FromParentScope(arrayCount));
                    jumpToPopFrame = BE_AddInstruction(context);
                    jumpToPopFrame->type = BE_InstructionType_Jump;
                    jumpToPopFrame->jump.condition = BE_FromParentScope(arrayIndexCompare);
                }
                else
                {
                    setArrayIndex->set.value = 0;
                }
                
                setReturnAddress->set.value = context.generator->instructionIndex + 1;
                
                if (type->variant == BE_TypeVariant_Struct)
                {
                    BEInstruction* jumpInstruction = BE_AddInstruction(context);
                    jumpInstruction->type = BE_InstructionType_Jump;
                    jumpInstruction->jump.newInstructionIndex = startAddress->instructionIndex;
                }
                else
                {
                    BEAssert(type->variant == BE_TypeVariant_Category);
                    
                    BEInstruction* jumpInstruction = BE_AddInstruction(context);
                    jumpInstruction->type = BE_InstructionType_Jump;
                    jumpInstruction->jump.dynamicNewInstructionIndex = BE_FromParentScope(categoryStartAddress);
                }
                
                if (isArray)
                {
                    BEInstruction* jumpToIncrement = BE_AddInstruction(context);
                    jumpToIncrement->type = BE_InstructionType_Jump;
                    jumpToIncrement->jump.newInstructionIndex = arrayIncrementInstruction;
                }
                
                if (jumpToPopFrame)
                {
                    jumpToPopFrame->jump.newInstructionIndex = generator->instructionIndex;
                }
                
                BEInstruction* popStruct = BE_AddInstruction(context);
                popStruct->type = BE_InstructionType_PopStruct;
                
                BE_AddInstruction_EndCall(context);
            }
            
            if (isHidden)
            {
                BEInstruction* showInstruction = BE_AddInstruction(context);
                showInstruction->type = BE_InstructionType_ShowMembers;
            }
            
            if (statement->memberDeclaration.isExternal)
            {
                BE_AddInstruction_UnaryOperator(context, BE_InstructionType_Copy, BE_MemberOffset(), addressReset);
            }
            
            BE_ResetStack(generator, reset);
        } break;
        
    }
    
    if (endIfChain)
    {
        PopInstructionStack();
    }
    
#undef PushInstructionStack
#undef PopInstructionStack
    
    return rVal;
}

struct BE_PendingJump
{
    struct BE_PendingJump* prev;
    
    BEu32 address;
    BEu32 instructionIndex;
};

typedef struct BE_InstructionLocationChunk
{
    struct BE_InstructionLocationChunk* next;
    BEu32 addresses[256];
} BE_InstructionLocationChunk;


struct BE_EncodedInstructionChunk
{
    BE_EncodedInstructionChunk* next;
    BEu8 data[256];
};


BEPrivateAPI BE_PendingJump* BE_PopPendingJump(BE_InstructionEncoder* encoder)
{
    BE_PendingJump* rVal = encoder->pendingJumps;
    encoder->pendingJumps = encoder->pendingJumps->prev;
    
    rVal->prev = encoder->freePendingJumps;
    encoder->freePendingJumps = rVal;
    
    return rVal;
}

BEPrivateAPI void BE_PushPendingJump(BEAllocatorType allocator, BE_InstructionEncoder* encoder, BEu32 address, BEu32 instructionIndex)
{
    BE_PendingJump* newJump = encoder->freePendingJumps;
    
    if (newJump)
    {
        encoder->freePendingJumps = encoder->freePendingJumps->prev;
    }
    else
    {
        newJump = BEAlloc(allocator, BE_PendingJump);
    }
    
    BE_ClearStructToZero(*newJump);
    
    newJump->address = address;
    newJump->instructionIndex = instructionIndex;
    
    newJump->prev = encoder->pendingJumps;
    encoder->pendingJumps = newJump;
}

BEPrivateAPI void BE_AddAddress(BEAllocatorType allocator, BE_InstructionLocations* locations, BEu32 address)
{
    BEu32 localIndex = locations->count % BE_ArrayCount(locations->head->addresses);
    
    if (localIndex == 0)
    {
        BE_InstructionLocationChunk* newChunk = BEAlloc(allocator, BE_InstructionLocationChunk);
        newChunk->next = 0;
        
        if (locations->head)
        {
            locations->tail = locations->tail->next = newChunk;
        }
        else
        {
            locations->tail = locations->head = newChunk;
        }
    }
    
    locations->tail->addresses[localIndex] = address;
    ++locations->count;
}

BEPrivateAPI BEu32 BE_FindInstructionAddress(BE_InstructionEncoder* encoder, BEu32 instructionIndex)
{
    BE_InstructionLocations* locations = &encoder->instructionAddresses;
    
    BEu32 rVal;
    if (instructionIndex == locations->count)
    {
        rVal = encoder->totalWrittenBytes;
    }
    else
    {
        BEu32 index = instructionIndex;
        BE_InstructionLocationChunk* chunk = locations->head;
        
        while (index >= BE_ArrayCount(chunk->addresses))
        {
            index -= BE_ArrayCount(chunk->addresses);
            chunk = chunk->next;
        }
        
        rVal = chunk->addresses[index];
    }
    
    return rVal;
}

BEPrivateAPI void BE_WriteByte(BEAllocatorType allocator, BE_InstructionEncoder* encoder, BEu8 value)
{
    BEu32 localIndex = encoder->written.writtenBytes % BE_ArrayCount(encoder->written.head->data);
    
    if (localIndex == 0)
    {
        BE_EncodedInstructionChunk* newChunk = encoder->freeEncodedInstructions.head;
        
        if (newChunk)
        {
            encoder->freeEncodedInstructions.head = encoder->freeEncodedInstructions.head->next;
            if (!encoder->freeEncodedInstructions.head)
            {
                encoder->freeEncodedInstructions.tail = 0;
            }
        }
        else
        {
            newChunk = BEAlloc(allocator, BE_EncodedInstructionChunk);
        }
        
        newChunk->next = 0;
        
        if (encoder->written.head)
        {
            encoder->written.tail = encoder->written.tail->next = newChunk;
        }
        else
        {
            encoder->written.tail = encoder->written.head = newChunk;
        }
    }
    
    BEu8* out = encoder->written.tail->data + localIndex;
    *out = value;
    
    ++encoder->written.writtenBytes;
    ++encoder->totalWrittenBytes;
}

BEPrivateAPI void BE_OverwriteBytes(BE_InstructionEncoder* encoder, BEu32 address, BEu32 count, BEu8* values)
{
    BEAssert(address >= encoder->totalWrittenBytesLastFlush);
    
    BEu32 index = address - encoder->totalWrittenBytesLastFlush;
    BE_EncodedInstructionChunk* chunk = encoder->written.head;
    
    while (index >= BE_ArrayCount(encoder->written.head->data))
    {
        index -= BE_ArrayCount(encoder->written.head->data);
        chunk = chunk->next;
    }
    
    for (BEu32 i = 0; i < count; ++i)
    {
        chunk->data[index] = values[i];
        
        if (++index == BE_ArrayCount(encoder->written.head->data))
        {
            index = 0;
            chunk = chunk->next;
        }
    }
}

BEPrivateAPI void BE_OverwriteConstant(BE_InstructionEncoder* encoder, BEu32 address, BEs64 value)
{
    BEu8 values[8];
    
    values[0] = (BEu8)((value & 0x00000000000000ffULL) >> (0 * 8));
    values[1] = (BEu8)((value & 0x000000000000ff00ULL) >> (1 * 8));
    values[2] = (BEu8)((value & 0x0000000000ff0000ULL) >> (2 * 8));
    values[3] = (BEu8)((value & 0x00000000ff000000ULL) >> (3 * 8));
    values[4] = (BEu8)((value & 0x000000ff00000000ULL) >> (4 * 8));
    values[5] = (BEu8)((value & 0x0000ff0000000000ULL) >> (5 * 8));
    values[6] = (BEu8)((value & 0x00ff000000000000ULL) >> (6 * 8));
    values[7] = (BEu8)((value & 0xff00000000000000ULL) >> (7 * 8));
    
    BE_OverwriteBytes(encoder, address, 8, values);
}

BEPrivateAPI void BE_WriteConstant(BEAllocatorType allocator, BE_InstructionEncoder* encoder, BEs64 value)
{
    BE_WriteByte(allocator, encoder, (BEu8)((value & 0x00000000000000ffULL) >> (0 * 8)));
    BE_WriteByte(allocator, encoder, (BEu8)((value & 0x000000000000ff00ULL) >> (1 * 8)));
    BE_WriteByte(allocator, encoder, (BEu8)((value & 0x0000000000ff0000ULL) >> (2 * 8)));
    BE_WriteByte(allocator, encoder, (BEu8)((value & 0x00000000ff000000ULL) >> (3 * 8)));
    BE_WriteByte(allocator, encoder, (BEu8)((value & 0x000000ff00000000ULL) >> (4 * 8)));
    BE_WriteByte(allocator, encoder, (BEu8)((value & 0x0000ff0000000000ULL) >> (5 * 8)));
    BE_WriteByte(allocator, encoder, (BEu8)((value & 0x00ff000000000000ULL) >> (6 * 8)));
    BE_WriteByte(allocator, encoder, (BEu8)((value & 0xff00000000000000ULL) >> (7 * 8)));
}

BEPrivateAPI void BE_WriteStackEntry(BEAllocatorType allocator, BE_InstructionEncoder* encoder, BE_StackEntry entry)
{
    BEu32 value = 0;
    BEAssert(entry.stack < 4);
    BEAssert(!(entry.index & 0xc0000000));
    
    value |= ((BEu32)entry.stack << 30);
    value |= entry.index;
    
    BE_WriteByte(allocator, encoder, (BEu8)((value & 0x000000ff) >> (0 * 8)));
    BE_WriteByte(allocator, encoder, (BEu8)((value & 0x0000ff00) >> (1 * 8)));
    BE_WriteByte(allocator, encoder, (BEu8)((value & 0x00ff0000) >> (2 * 8)));
    BE_WriteByte(allocator, encoder, (BEu8)((value & 0xff000000) >> (3 * 8)));
}

BEPrivateAPI void BE_WriteText(BEAllocatorType allocator, BE_InstructionEncoder* encoder, BE_String text)
{
    BE_WriteConstant(allocator, encoder, text.count);
    
    for (BEu32 i = 0; i < text.count; ++i)
    {
        BE_WriteByte(allocator, encoder, text.characters[i]);
    }
    
    BE_WriteByte(allocator, encoder, 0);
}

BEPrivateAPI void BE_EncodeMemberPath(BEAllocatorType allocator, BE_InstructionEncoder* encoder, BEu32 pathCount, BEFetchMemberLocation* path)
{
    BE_WriteConstant(allocator, encoder, pathCount);
    
    for (BEu32 i = 0; i < pathCount; ++i)
    {
        BE_WriteConstant(allocator, encoder, path[i].memberIndex);
    }
    
    for (BEu32 i = 0; i < pathCount; ++i)
    {
        BE_WriteStackEntry(allocator, encoder, path[i].arrayIndex);
    }
}

BEPrivateAPI void BE_FeedInstructionEncoder(BEAllocatorType allocator, BE_InstructionEncoder* encoder, BEInstruction* instruction)
{
    BE_AddAddress(allocator, &encoder->instructionAddresses, encoder->totalWrittenBytes);
    
    BE_InstructionType type = instruction->type;
    
    if (type == BE_InstructionType_Jump)
    {
        BEbool hasCondition = !!instruction->jump.condition.stack;
        BEbool hasDynamicAddress = !!instruction->jump.dynamicNewInstructionIndex.stack;
        
        if (hasCondition && hasDynamicAddress)
        {
            type = BE_InstructionType_JumpDynamicConditional;
        }
        else if (!hasCondition && hasDynamicAddress)
        {
            type = BE_InstructionType_JumpDynamic;
        }
        else if (hasCondition && !hasDynamicAddress)
        {
            type = BE_InstructionType_JumpConditional;
        }
    }
    
    BE_WriteByte(allocator, encoder, (BEu8)type);
    
    switch (type)
    {
        BE_InvalidDefault;
        
        case BE_InstructionType_Set:
        {
            BEAssert(instruction->result.stack);
            
            BE_WriteStackEntry(allocator, encoder, instruction->result);
            
            BEu32 writeLocation = encoder->totalWrittenBytes;
            BE_WriteConstant(allocator, encoder, instruction->set.value);
            
            if (BE_StackEntriesAreEqual(instruction->result, BE_ReturnAddress()))
            {
                BE_PushPendingJump(allocator, encoder, writeLocation, (BEu32)instruction->set.value);
            }
        } break;
        
        case BE_InstructionType_StructIndex:
        {
            BE_WriteConstant(allocator, encoder, instruction->structIndex.index);
        } break;
        
        case BE_InstructionType_Negate:
        case BE_InstructionType_LogicalNot:
        case BE_InstructionType_BitwiseNot:
        case BE_InstructionType_Abs:
        case BE_InstructionType_Copy:
        case BE_InstructionType_FAbs:
        case BE_InstructionType_FNegate:
        {
            BEAssert(instruction->result.stack && instruction->unaryValue.stack);
            
            BE_WriteStackEntry(allocator, encoder, instruction->result);
            BE_WriteStackEntry(allocator, encoder, instruction->unaryValue);
        } break;
        
        case BE_InstructionType_Add:
        case BE_InstructionType_Sub:
        case BE_InstructionType_Mul:
        case BE_InstructionType_Div:
        case BE_InstructionType_Mod:
        case BE_InstructionType_Less:
        case BE_InstructionType_LessOrEqual:
        case BE_InstructionType_Equal:
        case BE_InstructionType_Greater:
        case BE_InstructionType_GreaterOrEqual:
        case BE_InstructionType_NotEqual:
        case BE_InstructionType_BitshiftLeft:
        case BE_InstructionType_BitshiftRight:
        case BE_InstructionType_BitwiseAnd:
        case BE_InstructionType_BitwiseOr:
        case BE_InstructionType_BitwiseXOr:
        case BE_InstructionType_LogicalAnd:
        case BE_InstructionType_LogicalOr:
        case BE_InstructionType_FAdd:
        case BE_InstructionType_FSub:
        case BE_InstructionType_FMul:
        case BE_InstructionType_FDiv:
        case BE_InstructionType_FLess:
        case BE_InstructionType_FLessOrEqual:
        case BE_InstructionType_FEqual:
        case BE_InstructionType_FGreater:
        case BE_InstructionType_FGreaterOrEqual:
        case BE_InstructionType_FNotEqual:
        {
            BEAssert(instruction->result.stack && instruction->binary.left.stack && instruction->binary.right.stack);
            
            BE_WriteStackEntry(allocator, encoder, instruction->result);
            BE_WriteStackEntry(allocator, encoder, instruction->binary.left);
            BE_WriteStackEntry(allocator, encoder, instruction->binary.right);
        } break;
        
        case BE_InstructionType_PopFrame:
        case BE_InstructionType_PopStruct:
        case BE_InstructionType_HideMembers:
        case BE_InstructionType_ShowMembers:
        case BE_InstructionType_Breakpoint:
        {
            // NOTE(Jens): No-op.
        } break;
        
        case BE_InstructionType_PushFrame:
        {
            BE_WriteConstant(allocator, encoder, instruction->pushFrame.sizeRequiredInBytes);
        } break;
        
        case BE_InstructionType_PushStructInternal:
        case BE_InstructionType_PushStruct:
        {
            BE_WriteConstant(allocator, encoder, instruction->beginStruct.memberIndex);
            BE_WriteStackEntry(allocator, encoder, instruction->beginStruct.arrayCount);
        } break;
        
        case BE_InstructionType_ExportScalar_u:
        case BE_InstructionType_ExportScalar_s:
        case BE_InstructionType_ExportScalar_f:
        case BE_InstructionType_ExportScalar_string:
        case BE_InstructionType_ExportScalar_raw:
        {
            BEAssert(instruction->scalarExport.size.stack);
            
            BE_WriteConstant(allocator, encoder, instruction->scalarExport.displayType);
            BE_WriteConstant(allocator, encoder, instruction->scalarExport.isBigEndian);
            BE_WriteConstant(allocator, encoder, instruction->scalarExport.memberIndex);
            BE_WriteStackEntry(allocator, encoder, instruction->scalarExport.size);
            BE_WriteStackEntry(allocator, encoder, instruction->scalarExport.arrayCount);
        } break;
        
        case BE_InstructionType_FetchMemberValueFloat:
        case BE_InstructionType_FetchMemberValue:
        {
            BEAssert(instruction->result.stack);
            BE_WriteStackEntry(allocator, encoder, instruction->result);
            BE_EncodeMemberPath(allocator, encoder, instruction->fetchMember.pathCount, instruction->fetchMember.path);
        } break;
        
        case BE_InstructionType_FetchArrayCount:
        {
            BEAssert(instruction->result.stack);
            BE_WriteStackEntry(allocator, encoder, instruction->result);
            BE_EncodeMemberPath(allocator, encoder, instruction->fetchArrayCount.pathCount, instruction->fetchArrayCount.path);
        } break;
        
        case BE_InstructionType_FetchMemory:
        {
            BEAssert(instruction->result.stack && instruction->fetchMemory.size.stack);
            
            BE_WriteStackEntry(allocator, encoder, instruction->result);
            BE_WriteConstant(allocator, encoder, instruction->fetchMemory.flags);
            BE_WriteStackEntry(allocator, encoder, instruction->fetchMemory.size);
        } break;
        
        case BE_InstructionType_Jump:
        case BE_InstructionType_JumpDynamic:
        case BE_InstructionType_JumpConditional:
        case BE_InstructionType_JumpDynamicConditional:
        {
            if (instruction->jump.condition.stack)
            {
                BE_WriteStackEntry(allocator, encoder, instruction->jump.condition);
            }
            
            if (instruction->jump.dynamicNewInstructionIndex.stack)
            {
                BE_WriteStackEntry(allocator, encoder, instruction->jump.dynamicNewInstructionIndex);
            }
            else
            {
                BEu32 writeLocation = encoder->totalWrittenBytes;
                BE_WriteConstant(allocator, encoder, 0x0123456789abcdef);
                BE_PushPendingJump(allocator, encoder, writeLocation, instruction->jump.newInstructionIndex);
            }
        } break;
        
        case BE_InstructionType_FetchSizeOfFile:
        {
            BEAssert(instruction->result.stack);
            
            BE_WriteStackEntry(allocator, encoder, instruction->result);
        } break;
        
        case BE_InstructionType_FetchStartOfType:
        case BE_InstructionType_HasType:
        {
            BEAssert(instruction->result.stack && instruction->categoryQuery.typeNumber.stack);
            
            BE_WriteStackEntry(allocator, encoder, instruction->result);
            BE_WriteConstant(allocator, encoder, instruction->categoryQuery.categoryIndex);
            BE_WriteStackEntry(allocator, encoder, instruction->categoryQuery.typeNumber);
        } break;
        
        case BE_InstructionType_Assert:
        {
            BEAssert(instruction->assert.condition.stack);
            
            BE_WriteStackEntry(allocator, encoder, instruction->assert.condition);
        } break;
        
        case BE_InstructionType_Print:
        {
            BE_WriteStackEntry(allocator, encoder, instruction->print.value);
            
            BE_String str;
            str.count = instruction->print.prefixCharacterCount;
            str.characters = instruction->print.prefixCharacters;
            BE_WriteText(allocator, encoder, str);
        } break;
        
        case BE_InstructionType_Plot_F8_X:
        case BE_InstructionType_Plot_F8_Y:
        {
            BE_WriteStackEntry(allocator, encoder, instruction->plot.plotId);
            BE_WriteStackEntry(allocator, encoder, instruction->plot.value_f64);
        }break;
        
        case BE_InstructionType_CreatePlot:
        {
            BE_WriteStackEntry(allocator, encoder, instruction->result);
        } break;
        
        case BE_InstructionType_IntToFloat:
        {
            BE_WriteStackEntry(allocator, encoder, instruction->result);
            BE_WriteStackEntry(allocator, encoder, instruction->conversion.source);
        } break;
    }
}

BEPrivateAPI BE_EncodedInstructions BE_FlushInstructionBuffer(BE_InstructionEncoder* encoder)
{
    while (encoder->pendingJumps)
    {
        BE_PendingJump* jump = BE_PopPendingJump(encoder);
        
        BEu32 address = BE_FindInstructionAddress(encoder, jump->instructionIndex);
        BE_OverwriteConstant(encoder, jump->address, address);
    }
    
    BE_EncodedInstructions rVal = encoder->written;
    
    if (encoder->written.head)
    {
        if (!encoder->freeEncodedInstructions.head)
        {
            encoder->freeEncodedInstructions = encoder->written;
        }
        else
        {
            encoder->freeEncodedInstructions.tail->next = encoder->written.head;
            encoder->freeEncodedInstructions.tail = encoder->written.tail;
        }
        
        BE_ClearStructToZero(encoder->written);
    }
    
    encoder->totalWrittenBytesLastFlush = encoder->totalWrittenBytes;
    
    return rVal;
}

BEPrivateAPI BE_StackEntry BE_ReadStackEntry(BEu8** bytes)
{
    BE_StackEntry rVal;
    BE_ClearStructToZero(rVal);
    
    rVal.index |= ((*bytes)[0] << 0);
    rVal.index |= ((*bytes)[1] << 8);
    rVal.index |= ((*bytes)[2] << 16);
    
    BEu8 stack = ((*bytes)[3] & 0xc0) >> 6;
    
    rVal.stack = (BE_Stack)stack;
    rVal.index |= ((*bytes)[3] & ~0xc0) << 24;
    
    *bytes += 4;
    return rVal;
}

BEPrivateAPI BEs64 BE_ReadConstant(BEu8** bytes)
{
    BEu64 shift = 0;
    
    BEs64 rVal = 0;
    
    for (BEu32 i = 0; i < 8; ++i)
    {
        rVal |= ((BEs64)(*bytes)[i]) << shift;
        shift += 8;
    }
    
    *bytes += 8;
    
    return rVal;
}

BEPrivateAPI BE_String BE_ReadText(BEu8** bytes)
{
    BE_String rVal;
    BE_ClearStructToZero(rVal);
    
    rVal.count = (BEu32)BE_ReadConstant(bytes);
    rVal.characters = (char*)*bytes;
    
    *bytes += rVal.count + 1;
    
    return rVal;
}

BEPrivateAPI void BE_DecodeMemberPath(BEu8** bytes, BEu32* pathCount, BEFetchMemberLocation* path)
{
    *pathCount = (BEu32)BE_ReadConstant(bytes);
    
    for (BEu32 i = 0; i < *pathCount; ++i)
    {
        path[i].memberIndex = (BEu32)BE_ReadConstant(bytes);
    }
    
    for (BEu32 i = 0; i < *pathCount; ++i)
    {
        path[i].arrayIndex = BE_ReadStackEntry(bytes);
    }
}

BEPublicAPI BEInstruction BEDecodeInstruction(BEu32* outSize, BEu8* bytesStart)
{
    BEInstruction rVal;
    BE_ClearStructToZero(rVal);
    
    BEu8* bytes = bytesStart;
    
    rVal.type = (BE_InstructionType)*bytes++;
    
    switch (rVal.type)
    {
        BE_InvalidDefault;
        
        case BE_InstructionType_Set:
        {
            rVal.result = BE_ReadStackEntry(&bytes);
            rVal.set.value = BE_ReadConstant(&bytes);
            
            BEAssert(rVal.result.stack);
        } break;
        
        case BE_InstructionType_StructIndex:
        {
            rVal.structIndex.index = (BEu32)BE_ReadConstant(&bytes);
        } break;
        
        case BE_InstructionType_Negate:
        case BE_InstructionType_LogicalNot:
        case BE_InstructionType_BitwiseNot:
        case BE_InstructionType_Abs:
        case BE_InstructionType_Copy:
        case BE_InstructionType_FAbs:
        case BE_InstructionType_FNegate:
        {
            rVal.result = BE_ReadStackEntry(&bytes);
            rVal.unaryValue = BE_ReadStackEntry(&bytes);
            
            BEAssert(rVal.result.stack);
            BEAssert(rVal.unaryValue.stack);
        } break;
        
        case BE_InstructionType_Add:
        case BE_InstructionType_Sub:
        case BE_InstructionType_Mul:
        case BE_InstructionType_Div:
        case BE_InstructionType_Mod:
        case BE_InstructionType_Less:
        case BE_InstructionType_LessOrEqual:
        case BE_InstructionType_Equal:
        case BE_InstructionType_Greater:
        case BE_InstructionType_GreaterOrEqual:
        case BE_InstructionType_NotEqual:
        case BE_InstructionType_BitshiftLeft:
        case BE_InstructionType_BitshiftRight:
        case BE_InstructionType_BitwiseAnd:
        case BE_InstructionType_BitwiseOr:
        case BE_InstructionType_BitwiseXOr:
        case BE_InstructionType_LogicalAnd:
        case BE_InstructionType_LogicalOr:
        case BE_InstructionType_FAdd:
        case BE_InstructionType_FSub:
        case BE_InstructionType_FMul:
        case BE_InstructionType_FDiv:
        case BE_InstructionType_FLess:
        case BE_InstructionType_FLessOrEqual:
        case BE_InstructionType_FEqual:
        case BE_InstructionType_FGreater:
        case BE_InstructionType_FGreaterOrEqual:
        case BE_InstructionType_FNotEqual:
        {
            rVal.result = BE_ReadStackEntry(&bytes);
            rVal.binary.left = BE_ReadStackEntry(&bytes);
            rVal.binary.right = BE_ReadStackEntry(&bytes);
            
            BEAssert(rVal.result.stack);
            BEAssert(rVal.binary.left.stack);
            BEAssert(rVal.binary.right.stack);
        } break;
        
        case BE_InstructionType_PopFrame:
        case BE_InstructionType_PopStruct:
        case BE_InstructionType_HideMembers:
        case BE_InstructionType_ShowMembers:
        case BE_InstructionType_Breakpoint:
        {
            // NOTE(Jens): No-op.
        } break;
        
        case BE_InstructionType_PushFrame:
        {
            rVal.pushFrame.sizeRequiredInBytes = (BEu32)BE_ReadConstant(&bytes);
        } break;
        
        case BE_InstructionType_PushStructInternal:
        case BE_InstructionType_PushStruct:
        {
            rVal.beginStruct.memberIndex = (BEu32)BE_ReadConstant(&bytes);
            rVal.beginStruct.arrayCount = BE_ReadStackEntry(&bytes);
        } break;
        
        case BE_InstructionType_ExportScalar_u:
        case BE_InstructionType_ExportScalar_s:
        case BE_InstructionType_ExportScalar_f:
        case BE_InstructionType_ExportScalar_string:
        case BE_InstructionType_ExportScalar_raw:
        {
            rVal.scalarExport.displayType = (BEs32)BE_ReadConstant(&bytes);
            rVal.scalarExport.isBigEndian = (BEbool)BE_ReadConstant(&bytes);
            rVal.scalarExport.memberIndex = (BEu32)BE_ReadConstant(&bytes);
            rVal.scalarExport.size = BE_ReadStackEntry(&bytes);
            rVal.scalarExport.arrayCount = BE_ReadStackEntry(&bytes);
            
            BEAssert(rVal.scalarExport.size.stack);
        } break;
        
        case BE_InstructionType_FetchMemberValueFloat:
        case BE_InstructionType_FetchMemberValue:
        {
            rVal.result = BE_ReadStackEntry(&bytes);
            
            BE_DecodeMemberPath(&bytes, &rVal.fetchMember.pathCount, rVal.fetchMember.path);
            BEAssert(rVal.result.stack);
        } break;
        
        case BE_InstructionType_FetchArrayCount:
        {
            rVal.result = BE_ReadStackEntry(&bytes);
            
            BE_DecodeMemberPath(&bytes, &rVal.fetchArrayCount.pathCount, rVal.fetchArrayCount.path);
            BEAssert(rVal.result.stack);
        } break;
        
        case BE_InstructionType_FetchMemory:
        {
            rVal.result = BE_ReadStackEntry(&bytes);
            rVal.fetchMemory.flags = (BEFetchMemoryFlags)BE_ReadConstant(&bytes);
            rVal.fetchMemory.size = BE_ReadStackEntry(&bytes);
            
            BEAssert(rVal.result.stack);
            BEAssert(rVal.fetchMemory.size.stack);
        } break;
        
        case BE_InstructionType_Jump:
        {
            rVal.jump.newInstructionIndex = (BEu32)BE_ReadConstant(&bytes);
        } break;
        
        case BE_InstructionType_JumpDynamic:
        {
            rVal.jump.dynamicNewInstructionIndex = BE_ReadStackEntry(&bytes);
        } break;
        
        case BE_InstructionType_JumpConditional:
        {
            rVal.jump.condition = BE_ReadStackEntry(&bytes);
            rVal.jump.newInstructionIndex = (BEu32)BE_ReadConstant(&bytes);
        } break;
        
        case BE_InstructionType_JumpDynamicConditional:
        {
            rVal.jump.condition = BE_ReadStackEntry(&bytes);
            rVal.jump.dynamicNewInstructionIndex = BE_ReadStackEntry(&bytes);
        } break;
        
        case BE_InstructionType_FetchSizeOfFile:
        {
            rVal.result = BE_ReadStackEntry(&bytes);
            
            BEAssert(rVal.result.stack);
        } break;
        
        case BE_InstructionType_FetchStartOfType:
        case BE_InstructionType_HasType:
        {
            rVal.result = BE_ReadStackEntry(&bytes);
            rVal.categoryQuery.categoryIndex = (BEu32)BE_ReadConstant(&bytes);
            rVal.categoryQuery.typeNumber = BE_ReadStackEntry(&bytes);
            
            BEAssert(rVal.result.stack);
            BEAssert(rVal.categoryQuery.typeNumber.stack);
        } break;
        
        case BE_InstructionType_Assert:
        {
            rVal.assert.condition = BE_ReadStackEntry(&bytes);
            
            BEAssert(rVal.assert.condition.stack);
        } break;
        
        case BE_InstructionType_Print:
        {
            rVal.print.value = BE_ReadStackEntry(&bytes);
            BE_String str = BE_ReadText(&bytes);
            rVal.print.prefixCharacterCount = str.count;
            rVal.print.prefixCharacters = str.characters;
        } break;
        
        case BE_InstructionType_Plot_F8_X:
        case BE_InstructionType_Plot_F8_Y:
        {
            rVal.plot.plotId = BE_ReadStackEntry(&bytes);
            rVal.plot.value_f64 = BE_ReadStackEntry(&bytes);
        }break;
        
        case BE_InstructionType_CreatePlot:
        {
            rVal.result = BE_ReadStackEntry(&bytes);
        } break;
        
        case BE_InstructionType_IntToFloat:
        {
            rVal.result = BE_ReadStackEntry(&bytes);
            rVal.conversion.source = BE_ReadStackEntry(&bytes);
        } break;
    }
    
    *outSize = (BEu32)(bytes - bytesStart);
    
    return rVal;
}

typedef struct BE_TreeIterator
{
    struct BE_TreeIterator* prev;
    void* value;
} BE_TreeIterator;

BEPrivateAPI BE_TreeIterator* BE_IterateTree(BEAllocatorType allocator, BE_TreeIterator** freelist, void* value)
{
    BE_TreeIterator* rVal = 0;
    
    if (value)
    {
        rVal = *freelist;
        
        if (rVal)
        {
            *freelist = (*freelist)->prev;
        }
        else
        {
            rVal = BEAlloc(allocator, BE_TreeIterator);
        }
        
        rVal->prev = 0;
        rVal->value = value;
    }
    
    return rVal;
}

BEPrivateAPI void* BE_PopTreeIteration(BE_TreeIterator** freelist, BE_TreeIterator** it)
{
    void* rVal = (*it)->value;
    
    BE_TreeIterator* itValue = *it;
    *it = (*it)->prev;
    
    itValue->prev = *freelist;
    *freelist = itValue;
    
    return rVal;
}

BEPrivateAPI void BE_PushTreeIteration(BEAllocatorType allocator, BE_TreeIterator** freelist, BE_TreeIterator** it, void* value)
{
    if (value)
    {
        BE_TreeIterator* newIt = *freelist;
        if (newIt)
        {
            *freelist = (*freelist)->prev;
        }
        else
        {
            newIt = BEAlloc(allocator, BE_TreeIterator);
        }
        
        newIt->prev = *it;
        newIt->value = value;
        
        *it = newIt;
    }
}

struct BE_ImportedFilesChunk
{
    BE_ImportedFilesChunk* next;
    BEu64 lastWriteFileTimestamp[16];
    BE_String e[16];
    BE_String content[16];
};

struct BE_SourceLocationChunk
{
    BE_SourceLocationChunk* next;
    
    BEu32 startInstructionIndices[256];
    BESourceLocation locations[256];
};

//
// NOTE(Jens): Runtime overview
//
//
// Encoded instructions:
//
//   instruction-1
//   instruction-2
//   instruction-3
//       ...
//   instruction-N
//   (end-of-data)
//
//
// Stack:
//
//   s64 stack-entry-1
//   s64 stack-entry-2
//           ...
//   s64 stack-entry-N
//           ...
//   u32 end-of-frame-N
//   u32 end-of-frame-(N-1)
//           ...
//   u32 end-of-frame-1
//       (end-of-data)
//
//
// Data:
//
//   struct Categories
//   {
//       u(4) categoryCount;
//       u(4) firstStructIndexOfCategories[categoryCount + 1];
//
//       var totalAmountOfCategorizedStructs = firstStructIndexOfCategories[categoryCount];
//
//       u(4) startAddress[totalAmountOfCategorizedStructs];
//       alignas(8) s(8) categoryNumbers[totalAmountOfCategorizedStructs]; // NOTE(Jens): Sorted (ascending) within each category range.
//   };
//
//   struct Types
//   {
//       u(4) structCount;
//       u(4) firstMemberIndexOfStructs[structCount + 1];
//
//       var totalAmountOfMembers = firstMemberIndexOfStructs[structCount];
//
//       u(4) firstCharIndexOfStructs[structCount + 1];
//       u(4) firstCharIndexOfMembers[totalAmountOfMembers + 1];
//
//       var totalAmountOfCharacters = firstCharIndexOfMembers[totalAmountOfMembers];
//       string(totalAmountOfCharacters) nullTerminatedNames; // NOTE(Jens): Stores struct names first, then member names - all nullterminated.
//   };
//
//   struct Enums
//   {
//       u(4) enumCount;
//       u(4) firstMemberIndexOfEnums[enumCount + 1];
//
//       var totalAmountOfMembers = firstMemberIndexOfEnums[enumCount];
//
//       u(4) firstCharIndexOfEnums[enumCount + 1];
//       u(4) firstCharIndexOfMembers[totalAmountOfMembers + 1];
//       alignas(8) s(8) memberValues[totalAmountOfMembers];
//
//       var totalAmountOfCharacters = firstCharIndexOfMembers[totalAmountOfMembers];
//       string(totalAmountOfCharacters) nullTerminatedNames; // NOTE(Jens): Stores struct names first, then member names - all nullterminated.
//   };
//
//   struct SourceLocation
//   {
//       u(4) fileIndex;
//       u(4) sizeInBytes;
//       u(4) byteIndex;
//       u(4) lineIndex;
//       u(4) bytesSinceStartOfLine;
//   };
//
//   struct Sources
//   {
//       u(4) fileCount;
//       u(4) instructionCount;
//       u(4) firstCharIndexOfFilePath[fileCount + 1];
//       alignas(8) u(8) lastWriteFileTimestamp[fileCount];
//       u(4) instructionIndices[instructionCount];
//       SourceLocation locations[instructionCount];
//
//       var totalAmountOfCharacters = firstCharIndexOfFilePath[fileCount];
//       string(totalAmountOfCharacters) nullTerminatedPaths;
//   };
//
//   struct DataSection
//   {
//       u(8, hex) categoriesLocation;
//       u(8, hex) typesLocation;
//       u(8, hex) enumsLocation;
//       u(8, hex) sourcesLocation;
//       u(4) globalFrameSize;
//       u(4) instructionCount;
//
//       @(categoriesLocation) Categories categories;
//       @(typesLocation) Types types;
//       @(enumsLocation) Enums enums;
//
//       if (sourceLocation)
//       {
//           @(sourceLocation) Sources sources;
//       }
//   };
//

typedef struct
{
    BEu32 totalSizeInBytes;
    
    BEu32 loc_DataSection_categoriesLocation;
    BEu32 loc_DataSection_typesLocation;
    BEu32 loc_DataSection_enumsLocation;
    BEu32 loc_DataSection_sourcesLocation;
    BEu32 loc_DataSection_globalFrameSize;
    BEu32 loc_DataSection_instructionCount;
    
    BEu32 Categories_totalAmountOfCategorizedStructs;
    BEu32 loc_Categories;
    BEu32 loc_Categories_categoryCount;
    BEu32 loc_Categories_firstStructIndexOfCategories;
    BEu32 loc_Categories_startAddress;
    BEu32 loc_Categories_categoryNumbers;
    
    BEu32 Types_totalMemberCount;
    BEu32 Types_totalCharacterCount;
    BEu32 loc_Types;
    BEu32 loc_Types_structCount;
    BEu32 loc_Types_firstMemberIndexOfStructs;
    BEu32 loc_Types_firstCharIndexOfStructs;
    BEu32 loc_Types_firstCharIndexOfMembers;
    BEu32 loc_Types_nullTerminatedNames;
    
    BEu32 Enums_totalMemberCount;
    BEu32 Enums_totalCharacterCount;
    BEu32 loc_Enums;
    BEu32 loc_Enums_enumCount;
    BEu32 loc_Enums_firstMemberIndexOfEnums;
    BEu32 loc_Enums_firstCharIndexOfEnums;
    BEu32 loc_Enums_firstCharIndexOfMembers;
    BEu32 loc_Enums_memberValues;
    BEu32 loc_Enums_nullTerminatedNames;
    
    BEu32 Sources_totalAmountOfCharacters;
    BEu32 loc_Sources;
    BEu32 loc_Sources_fileCount;
    BEu32 loc_Sources_instructionCount;
    BEu32 loc_Sources_firstCharIndexOfFilePath;
    BEu32 loc_Sources_lastWriteFileTimestamp;
    BEu32 loc_Sources_instructionIndices;
    BEu32 loc_Sources_locations;
    BEu32 loc_Sources_nullTerminatedPaths;
} BE_DataSectionLocations;

BEPrivateAPI BE_DataSectionLocations BE_CalculateDataSectionLayout(BE_ParsedStructs* structs, BE_ParsedEnums* enums, BE_SourceLocations* sources, BE_ImportedFiles* imported)
{
    BE_DataSectionLocations rVal;
    BE_ClearStructToZero(rVal);
    
    for (BE_ParsedCategoryNode* it = structs->categoriesByOrderHead; it; it = it->nextInOrder)
    {
        rVal.Categories_totalAmountOfCategorizedStructs += it->structCount;
    }
    
    for (BE_ParsedStructNode* it = structs->byOrderHead; it; it = it->nextInOrder)
    {
        rVal.Types_totalMemberCount += it->memberCount;
        rVal.Types_totalCharacterCount += (BEu32)it->name.count + 1;
        
        for (BE_ParsedMemberNode* memberIt = it->membersHead; memberIt; memberIt = memberIt->next)
        {
            rVal.Types_totalCharacterCount += (BEu32)memberIt->declaration.name.count + 1;
        }
    }
    
    for (BE_ParsedEnumNode* it = enums->byOrderHead; it; it = it->nextInOrder)
    {
        rVal.Enums_totalCharacterCount += (BEu32)it->name.count + 1;
        
        for (BE_EnumMemberList* memberIt = it->members; memberIt; memberIt = memberIt->prev)
        {
            rVal.Enums_totalCharacterCount += (BEu32)memberIt->name.count + 1;
            ++rVal.Enums_totalMemberCount;
        }
    }
    
    if (sources)
    {
        BE_ImportedFilesChunk* chunk = imported->head;
        BEu32 localIndex = 0;
        for (BEu32 fileIndex = 0; fileIndex < imported->count; ++fileIndex)
        {
            rVal.Sources_totalAmountOfCharacters += (BEu32)(chunk->e[localIndex].count + 1);
            
            if (++localIndex == BE_ArrayCount(chunk->e))
            {
                localIndex = 0;
                chunk = chunk->next;
            }
        }
    }
    
    // NOTE(Jens): DataSection
    
    rVal.loc_DataSection_categoriesLocation = rVal.totalSizeInBytes;
    rVal.totalSizeInBytes += 8;
    
    rVal.loc_DataSection_typesLocation = rVal.totalSizeInBytes;
    rVal.totalSizeInBytes += 8;
    
    rVal.loc_DataSection_enumsLocation = rVal.totalSizeInBytes;
    rVal.totalSizeInBytes += 8;
    
    rVal.loc_DataSection_sourcesLocation = rVal.totalSizeInBytes;
    rVal.totalSizeInBytes += 8;
    
    rVal.loc_DataSection_globalFrameSize = rVal.totalSizeInBytes;
    rVal.totalSizeInBytes += 4;
    
    rVal.loc_DataSection_instructionCount = rVal.totalSizeInBytes;
    rVal.totalSizeInBytes += 4;
    
    
    // NOTE(Jens): Categories
    
    rVal.loc_Categories = rVal.totalSizeInBytes;
    
    rVal.loc_Categories_categoryCount = rVal.totalSizeInBytes;
    rVal.totalSizeInBytes += 4;
    
    rVal.loc_Categories_firstStructIndexOfCategories = rVal.totalSizeInBytes;
    rVal.totalSizeInBytes += 4 * (structs->categoryCount + 1);
    
    rVal.loc_Categories_startAddress = rVal.totalSizeInBytes;
    rVal.totalSizeInBytes += 4 * rVal.Categories_totalAmountOfCategorizedStructs;
    
    // NOTE(Jens): 'alignas(8)'
    rVal.totalSizeInBytes = (rVal.totalSizeInBytes + 0x7) & ~0x7;
    BEAssert(!(rVal.totalSizeInBytes & 0x7));
    
    rVal.loc_Categories_categoryNumbers = rVal.totalSizeInBytes;
    rVal.totalSizeInBytes += 8 * rVal.Categories_totalAmountOfCategorizedStructs;
    
    
    // NOTE(Jens): Types
    
    rVal.loc_Types = rVal.totalSizeInBytes;
    
    rVal.loc_Types_structCount = rVal.totalSizeInBytes;
    rVal.totalSizeInBytes += 4;
    
    rVal.loc_Types_firstMemberIndexOfStructs = rVal.totalSizeInBytes;
    rVal.totalSizeInBytes += 4 * (structs->structCount + 1);
    
    rVal.loc_Types_firstCharIndexOfStructs = rVal.totalSizeInBytes;
    rVal.totalSizeInBytes += 4 * (structs->structCount + 1);
    
    rVal.loc_Types_firstCharIndexOfMembers = rVal.totalSizeInBytes;
    rVal.totalSizeInBytes += 4 * (rVal.Types_totalMemberCount + 1);
    
    rVal.loc_Types_nullTerminatedNames = rVal.totalSizeInBytes;
    rVal.totalSizeInBytes += rVal.Types_totalCharacterCount;
    
    
    // NOTE(Jens): Enums
    
    // NOTE(Jens): Align 'Enums' to 4-bytes
    rVal.totalSizeInBytes = (rVal.totalSizeInBytes + 0x3) & ~0x3;
    BEAssert(!(rVal.totalSizeInBytes & 0x3));
    
    rVal.loc_Enums = rVal.totalSizeInBytes;
    
    rVal.loc_Enums_enumCount = rVal.totalSizeInBytes;
    rVal.totalSizeInBytes += 4;
    
    rVal.loc_Enums_firstMemberIndexOfEnums = rVal.totalSizeInBytes;
    rVal.totalSizeInBytes += 4 * (enums->count + 1);
    
    rVal.loc_Enums_firstCharIndexOfEnums = rVal.totalSizeInBytes;
    rVal.totalSizeInBytes += 4 * (enums->count + 1);
    
    rVal.loc_Enums_firstCharIndexOfMembers = rVal.totalSizeInBytes;
    rVal.totalSizeInBytes += 4 * (rVal.Enums_totalMemberCount + 1);
    
    rVal.totalSizeInBytes = (rVal.totalSizeInBytes + 0x7) & ~0x7;
    
    rVal.loc_Enums_memberValues = rVal.totalSizeInBytes;
    rVal.totalSizeInBytes += 8 * rVal.Enums_totalMemberCount;
    
    rVal.loc_Enums_nullTerminatedNames = rVal.totalSizeInBytes;
    rVal.totalSizeInBytes += rVal.Enums_totalCharacterCount;
    
    
    // NOTE(Jens): Sources
    
    if (sources)
    {
        // NOTE(Jens): Align 'Sources' to 4-bytes
        rVal.totalSizeInBytes = (rVal.totalSizeInBytes + 0x3) & ~0x3;
        BEAssert(!(rVal.totalSizeInBytes & 0x3));
        
        rVal.loc_Sources = rVal.totalSizeInBytes;
        
        rVal.loc_Sources_fileCount = rVal.totalSizeInBytes;
        rVal.totalSizeInBytes += 4;
        
        rVal.loc_Sources_instructionCount = rVal.totalSizeInBytes;
        rVal.totalSizeInBytes += 4;
        
        rVal.loc_Sources_firstCharIndexOfFilePath = rVal.totalSizeInBytes;
        rVal.totalSizeInBytes += 4 * (imported->count + 1);
        
        // NOTE(Jens): 'alignas(8)'
        rVal.totalSizeInBytes = (rVal.totalSizeInBytes + 0x7) & ~0x7;
        
        rVal.loc_Sources_lastWriteFileTimestamp = rVal.totalSizeInBytes;
        rVal.totalSizeInBytes += 8 * imported->count;
        
        rVal.loc_Sources_instructionIndices = rVal.totalSizeInBytes;
        rVal.totalSizeInBytes += 4 * sources->count;
        
        rVal.loc_Sources_locations = rVal.totalSizeInBytes;
        rVal.totalSizeInBytes += 4 * 5 * sources->count;
        
        rVal.loc_Sources_nullTerminatedPaths = rVal.totalSizeInBytes;
        rVal.totalSizeInBytes += rVal.Sources_totalAmountOfCharacters;
    }
    
    return rVal;
}

#define PushUntilLeftmostDescendent(type, it, left) \
do { \
if (it) \
{ \
while (((type*)it->value)->left) \
{ \
BE_PushTreeIteration(allocator, treeFreelist, &it, ((type*)it->value)->left); \
} \
} \
} \
while (0)

BEPrivateAPI void BE_WriteCategoriesSection(BEu8* data, BEAllocatorType allocator, BE_TreeIterator** treeFreelist, BE_DataSectionLocations* layout, BE_ParsedStructs* structs, BEu32* structStartAddresses)
{
    
    *(BEu32*)(data + layout->loc_Categories_categoryCount) = structs->categoryCount;
    
    // NOTE(Jens): Ordered iteration of trees: Start with leftmost descendent, then go to parent, then leftmost descendent of right child.
    
    BEu32 categorizedStructIndex = 0;
    BEu32 categoryIndex = 0;
    
    for (BE_ParsedCategoryNode* categoryIt = structs->categoriesByOrderHead; categoryIt; categoryIt = categoryIt->nextInOrder)
    {
        BEAssert(categoryIt->index == categoryIndex);
        
        ((BEu32*)(data + layout->loc_Categories_firstStructIndexOfCategories))[categoryIndex] = categorizedStructIndex;
        
        BE_TreeIterator* structIt = BE_IterateTree(allocator, treeFreelist, categoryIt->types);
        PushUntilLeftmostDescendent(BE_ParsedStructNode, structIt, leftInCategory);
        
        while (structIt)
        {
            BE_ParsedStructNode* structNode = (BE_ParsedStructNode*)BE_PopTreeIteration(treeFreelist, &structIt);
            
            ((BEs64*)(data + layout->loc_Categories_categoryNumbers))[categorizedStructIndex] = structNode->categoryNumber;
            ((BEu32*)(data + layout->loc_Categories_startAddress))[categorizedStructIndex] = structStartAddresses[structNode->index];
            
            ++categorizedStructIndex;
            
            if (structNode->rightInCategory)
            {
                BE_PushTreeIteration(allocator, treeFreelist, &structIt, structNode->rightInCategory);
                PushUntilLeftmostDescendent(BE_ParsedStructNode, structIt, leftInCategory);
            }
        }
        
        ++categoryIndex;
    }
    
    BEAssert(categorizedStructIndex == layout->Categories_totalAmountOfCategorizedStructs);
    BEAssert(categoryIndex == structs->categoryCount);
    
    ((BEu32*)(data + layout->loc_Categories_firstStructIndexOfCategories))[structs->categoryCount] = layout->Categories_totalAmountOfCategorizedStructs;
}

#undef PushUntilLeftmostDescendent

BEPrivateAPI void BE_WriteTypesSection(BEu8* data, BE_DataSectionLocations* layout, BE_ParsedStructs* structs)
{
    *(BEu32*)(data + layout->loc_Types_structCount) = structs->structCount;
    
    BEu32 globalCharIndex = 0;
    BEu32 globalMemberIndex = 0;
    
    {
        BEu32 structIndex = 0;
        
        for (BE_ParsedStructNode* structIt = structs->byOrderHead; structIt; structIt = structIt->nextInOrder)
        {
            BEAssert(structIt->index == structIndex);
            
            ((BEu32*)(data + layout->loc_Types_firstMemberIndexOfStructs))[structIt->index] = globalMemberIndex;
            ((BEu32*)(data + layout->loc_Types_firstCharIndexOfStructs))[structIt->index] = globalCharIndex;
            ++structIndex;
            
            for (BEu32 i = 0; i < structIt->name.count; ++i)
            {
                (data + layout->loc_Types_nullTerminatedNames)[globalCharIndex++] = (BEu8)structIt->name.characters[i];
            }
            (data + layout->loc_Types_nullTerminatedNames)[globalCharIndex++] = 0;
            
            globalMemberIndex += structIt->memberCount;
        }
        
        BEAssert(structIndex == structs->structCount);
        
        ((BEu32*)(data + layout->loc_Types_firstMemberIndexOfStructs))[structs->structCount] = globalMemberIndex;
        ((BEu32*)(data + layout->loc_Types_firstCharIndexOfStructs))[structs->structCount] = globalCharIndex;
    }
    
    for (BE_ParsedStructNode* structIt = structs->byOrderHead; structIt; structIt = structIt->nextInOrder)
    {
        BEu32 baseMemberIndex = ((BEu32*)(data + layout->loc_Types_firstMemberIndexOfStructs))[structIt->index];
        
        BEu32 localMemberIndex = 0;
        for (BE_ParsedMemberNode* memberIt = structIt->membersHead; memberIt; memberIt = memberIt->next)
        {
            ((BEu32*)(data + layout->loc_Types_firstCharIndexOfMembers))[baseMemberIndex + localMemberIndex++] = globalCharIndex;
            
            for (BEu32 i = 0; i < memberIt->declaration.name.count; ++i)
            {
                (data + layout->loc_Types_nullTerminatedNames)[globalCharIndex++] = (BEu8)memberIt->declaration.name.characters[i];
            }
            (data + layout->loc_Types_nullTerminatedNames)[globalCharIndex++] = 0;
        }
    }
    
    ((BEu32*)(data + layout->loc_Types_firstCharIndexOfMembers))[globalMemberIndex] = globalCharIndex;
}

BEPrivateAPI void BE_WriteEnumsSection(BEu8* data, BE_DataSectionLocations* layout, BE_ParsedEnums* enums)
{
    *(BEu32*)(data + layout->loc_Enums_enumCount) = enums->count;
    
    BEu32 globalCharIndex = 0;
    
    {
        BEu32 globalMemberIndex = 0;
        BEu32 enumIndex = 0;
        
        for (BE_ParsedEnumNode* enumIt = enums->byOrderHead; enumIt; enumIt = enumIt->nextInOrder)
        {
            BEAssert(enumIt->index == enumIndex);
            
            ((BEu32*)(data + layout->loc_Enums_firstMemberIndexOfEnums))[enumIt->index] = globalMemberIndex;
            ((BEu32*)(data + layout->loc_Enums_firstCharIndexOfEnums))[enumIt->index] = globalCharIndex;
            ++enumIndex;
            
            for (BEu32 i = 0; i < enumIt->name.count; ++i)
            {
                (data + layout->loc_Enums_nullTerminatedNames)[globalCharIndex++] = (BEu8)enumIt->name.characters[i];
            }
            (data + layout->loc_Enums_nullTerminatedNames)[globalCharIndex++] = 0;
            
            for (BE_EnumMemberList* memberIt = enumIt->members; memberIt; memberIt = memberIt->prev)
            {
                ++globalMemberIndex;
            }
        }
        
        BEAssert(enumIndex == enums->count);
        
        ((BEu32*)(data + layout->loc_Enums_firstMemberIndexOfEnums))[enums->count] = globalMemberIndex;
        ((BEu32*)(data + layout->loc_Enums_firstCharIndexOfEnums))[enums->count] = globalCharIndex;
    }
    
    for (BE_ParsedEnumNode* enumIt = enums->byOrderHead; enumIt; enumIt = enumIt->nextInOrder)
    {
        BEu32 baseMemberIndex = ((BEu32*)(data + layout->loc_Enums_firstMemberIndexOfEnums))[enumIt->index];
        
        BEu32 localMemberIndex = 0;
        for (BE_EnumMemberList* memberIt = enumIt->members; memberIt; memberIt = memberIt->prev)
        {
            BEu32 globalMemberIndex = baseMemberIndex + localMemberIndex++;
            ((BEu32*)(data + layout->loc_Enums_firstCharIndexOfMembers))[globalMemberIndex] = globalCharIndex;
            
            for (BEu32 i = 0; i < memberIt->name.count; ++i)
            {
                (data + layout->loc_Enums_nullTerminatedNames)[globalCharIndex++] = (BEu8)memberIt->name.characters[i];
            }
            (data + layout->loc_Enums_nullTerminatedNames)[globalCharIndex++] = 0;
            
            ((BEs64*)(data + layout->loc_Enums_memberValues))[globalMemberIndex] = memberIt->value;
        }
    }
    
    ((BEu32*)(data + layout->loc_Enums_firstCharIndexOfMembers))[layout->Enums_totalMemberCount] = globalCharIndex;
}

BEPrivateAPI void BE_WriteSourcesSection(BEu8* data, BE_DataSectionLocations* layout, BE_SourceLocations* sources, BE_ImportedFiles* imported)
{
    *(BEu32*)(data + layout->loc_Sources_fileCount) = imported->count;
    *(BEu32*)(data + layout->loc_Sources_instructionCount) = sources->count;
    
    {
        BE_ImportedFilesChunk* chunk = imported->head;
        BEu32 charIndex = 0;
        BEu32 localIndex = 0;
        for (BEu32 fileIndex = 0; fileIndex < imported->count; ++fileIndex)
        {
            ((BEu32*)(data + layout->loc_Sources_firstCharIndexOfFilePath))[fileIndex] = charIndex;
            
            for (BEu32 i = 0; i < (BEu32)chunk->e[localIndex].count; ++i)
            {
                (data + layout->loc_Sources_nullTerminatedPaths)[charIndex++] = chunk->e[localIndex].characters[i];
            }
            (data + layout->loc_Sources_nullTerminatedPaths)[charIndex++] = '\0';
            
            ((BEu64*)(data + layout->loc_Sources_lastWriteFileTimestamp))[fileIndex] = chunk->lastWriteFileTimestamp[localIndex];
            
            
            if (++localIndex == BE_ArrayCount(chunk->e))
            {
                localIndex = 0;
                chunk = chunk->next;
            }
        }
        
        ((BEu32*)(data + layout->loc_Sources_firstCharIndexOfFilePath))[imported->count] = charIndex;
    }
    
    {
        BE_SourceLocationChunk* chunk = sources->head;
        BEu32 localIndex = 0;
        for (BEu32 sourceIndex = 0; sourceIndex < sources->count; ++sourceIndex)
        {
            ((BEu32*)(data + layout->loc_Sources_instructionIndices))[sourceIndex] = chunk->startInstructionIndices[localIndex];
            ((BESourceLocation*)(data + layout->loc_Sources_locations))[sourceIndex] = chunk->locations[localIndex];
            
            
            if (++localIndex == BE_ArrayCount(chunk->locations))
            {
                localIndex = 0;
                chunk = chunk->next;
            }
        }
    }
}

BEPrivateAPI BEDataSection BE_CreateDataSection(BEAllocatorType allocator, BE_TreeIterator** treeFreelist, BEu32 globalFrameSize, BEu32 instructionCount, BE_ParsedStructs* structs, BE_ParsedEnums* enums, BEu32* structStartAddresses, BE_SourceLocations* sources, BE_ImportedFiles* imported)
{
    BE_DataSectionLocations layout = BE_CalculateDataSectionLayout(structs, enums, sources, imported);
    
    BEu32 size = 8 * ((layout.totalSizeInBytes + 7) / 8);
    
    // NOTE(Jens): Allocate as 64-bit to get 8 byte alignment.
    BEu8* data = (BEu8*)BEAllocArray(allocator, BEu64, size / 8);
    
    *(BEu64*)(data + layout.loc_DataSection_categoriesLocation) = layout.loc_Categories;
    *(BEu64*)(data + layout.loc_DataSection_typesLocation) = layout.loc_Types;
    *(BEu64*)(data + layout.loc_DataSection_enumsLocation) = layout.loc_Enums;
    *(BEu64*)(data + layout.loc_DataSection_sourcesLocation) = layout.loc_Sources;
    *(BEu32*)(data + layout.loc_DataSection_globalFrameSize) = globalFrameSize;
    *(BEu32*)(data + layout.loc_DataSection_instructionCount) = instructionCount;
    
    BE_WriteCategoriesSection(data, allocator, treeFreelist, &layout, structs, structStartAddresses);
    BE_WriteTypesSection(data, &layout, structs);
    BE_WriteEnumsSection(data, &layout, enums);
    
    if (sources)
    {
        BE_WriteSourcesSection(data, &layout, sources, imported);
    }
    
    BEDataSection rVal;
    rVal.size = size;
    rVal.data = data;
    
    return rVal;
}


BEPrivateAPI BEu32 BE_GetStructIndex(BE_MemberNode* members_)
{
    BEu32 rVal;
    
    BE_MemberNode* members = members_;
    
    if (members->type == BE_MemberNodeType_Struct)
    {
        members = members->parent;
    }
    
    if (members->type == BE_MemberNodeType_StructArray)
    {
        rVal = members->value_StructArray.structIndex;
    }
    else
    {
        BEAssert(members->type == BE_MemberNodeType_Root);
        
        rVal = members->value_Root.structIndex;
    }
    
    return rVal;
}

BEPrivateAPI BEu32 BE_GetStructMemberIndex(BE_MemberNode* member)
{
    BEAssert(member->type == BE_MemberNodeType_Struct);
    
    BE_MemberNode* grandParent = member->parent->parent;
    
    BEu32 rVal;
    BEu32 memberCount;
    BE_MemberNode* members;
    
    if (grandParent->parent)
    {
        BEAssert(grandParent->type == BE_MemberNodeType_Struct);
        
        members = grandParent->value_Struct.members;
        memberCount = grandParent->parent->value_StructArray.memberCount;
    }
    else
    {
        BEAssert(grandParent->type == BE_MemberNodeType_Root);
        
        members = grandParent->value_Root.members;
        memberCount = grandParent->value_Root.memberCount;
    }
    
    rVal = (BEu32)(member->parent - members);
    BEAssert(rVal < memberCount);
    
    return rVal;
}

BEPrivateAPI BEu64 BE_GetStructArrayCount(BE_MemberNode* member)
{
    BEAssert(member->type == BE_MemberNodeType_Struct);
    BEAssert(member->parent->type == BE_MemberNodeType_StructArray);
    
    BEu64 rVal = member->parent->value_StructArray.arrayCount;
    
    return rVal;
}

BEPrivateAPI BEbool BE_GetStructIsInternal(BE_MemberNode* member)
{
    BEAssert(member->type == BE_MemberNodeType_Struct);
    BEAssert(member->parent->type == BE_MemberNodeType_StructArray);
    
    BEbool rVal = member->parent->value_StructArray.isInternal;
    
    return rVal;
}

BEPrivateAPI BEu64 BE_GetStructAppearIndex(BE_MemberNode* member)
{
    BEAssert(member->type == BE_MemberNodeType_Struct);
    BEAssert(member->parent->type == BE_MemberNodeType_StructArray);
    
    BEu64 rVal = member->parent->appearCount;
    
    return rVal - 1;
}

BEPrivateAPI BE_MemberNode* BE_GetMemberNode(BE_MemberNode* members, BEu32 memberIndex)
{
    BE_MemberNode* rVal;
    
    if (members->parent)
    {
        BEAssert(members->type == BE_MemberNodeType_Struct);
        BEAssert(members->parent->type == BE_MemberNodeType_StructArray);
        BEAssert(memberIndex < members->parent->value_StructArray.memberCount);
        
        rVal = members->value_Struct.members + memberIndex;
    }
    else
    {
        BEAssert(memberIndex < members->value_Root.memberCount);
        
        rVal = members->value_Root.members + memberIndex;
    }
    
    return rVal;
}

BEPrivateAPI BEu64 BE_ObserveScalarMember(BE_MemberNode* members, BEu32 memberIndex, BE_MemberNodeScalar scalar)
{
    BE_MemberNode* member = BE_GetMemberNode(members, memberIndex);
    ++member->appearCount;
    
    member->type = BE_MemberNodeType_Scalar;
    member->value_Scalar = scalar;
    
    return member->appearCount - 1;
}

BEPrivateAPI BEu64 BE_ObserveStructMember(BEAllocatorType allocator, BE_MemberNode** currentMembers, BEu32 memberIndex, BEu64 arrayCount, BEbool isInternal)
{
    BE_MemberNode* arrayMember = BE_GetMemberNode(*currentMembers, memberIndex);
    ++arrayMember->appearCount;
    
    BEu64 elementCount = arrayCount;
    if (!elementCount)
    {
        elementCount = 1;
    }
    
    BE_MemberNodeStructArray* array = &arrayMember->value_StructArray;
    
    if (arrayMember->type)
    {
        // NOTE(Jens): This member has been observed before (probably due to loop). However, array count might have changed as well as the type.
        
        if (arrayMember->type != BE_MemberNodeType_StructArray)
        {
            BE_ClearStructToZero(*array);
        }
    }
    
    arrayMember->type = BE_MemberNodeType_StructArray;
    
    array->arrayCount = arrayCount;
    array->isInternal = isInternal;
    
    if (array->arrayCapacity < elementCount)
    {
        // FIXME(Jens): Will leak previously allocated array if present.
        
        array->arrayCapacity = elementCount;
        array->array = BEAllocArray(allocator, BE_MemberNode, elementCount);
    }
    
    for (BEu64 i = 0; i < elementCount; ++i)
    {
        if (array->array[i].type != BE_MemberNodeType_Struct)
        {
            BE_ClearStructToZero(array->array[i].value_Struct);
        }
        
        array->array[i].type = BE_MemberNodeType_Struct;
        array->array[i].parent = arrayMember;
    }
    
    *currentMembers = arrayMember;
    
    return arrayMember->appearCount - 1;
}

BEPrivateAPI void BE_ObserveStructMemberTypeInfo(BEAllocatorType allocator, BE_MemberNode** currentMembers, BEu32 structIndex, BEu64 arrayIndex, BEu32 memberCount)
{
    if (!*currentMembers)
    {
        BE_MemberNode* root = BEAlloc(allocator, BE_MemberNode);
        BE_ClearStructToZero(*root);
        
        root->type = BE_MemberNodeType_Root;
        
        *currentMembers = root;
    }
    
    if ((*currentMembers)->type == BE_MemberNodeType_Root)
    {
        // NOTE(Jens): Root only comes from layout-statement, which does not support arrays at this time.
        //             'arrayIndex' is most likely nonsensical at this point.
        
        BEu32 memberCapacity = (*currentMembers)->value_Root.memberCapacity;
        
        if (memberCapacity < memberCount)
        {
            (*currentMembers)->value_Root.memberCapacity = memberCount;
            (*currentMembers)->value_Root.members = BEAllocArray(allocator, BE_MemberNode, memberCount);
            
            for (BEu32 i = 0; i < memberCount; ++i)
            {
                BE_ClearStructToZero((*currentMembers)->value_Root.members[i]);
                (*currentMembers)->value_Root.members[i].parent = *currentMembers;
            }
        }
        
        for (BEu32 i = 0; i < memberCount; ++i)
        {
            (*currentMembers)->value_Root.members[i].appearCount = 0;
        }
        
        (*currentMembers)->value_Root.memberCount = memberCount;
        (*currentMembers)->value_Root.structIndex = structIndex;
    }
    else
    {
        if ((*currentMembers)->type == BE_MemberNodeType_StructArray)
        {
            // NOTE(Jens): First time observing the array.
            
            BEAssert(arrayIndex == 0);
            
            (*currentMembers)->value_StructArray.structIndex = structIndex;
            (*currentMembers)->value_StructArray.memberCount = memberCount;
        }
        else
        {
            BEAssert((*currentMembers)->type == BE_MemberNodeType_Struct);
            BEAssert((*currentMembers)->parent->type == BE_MemberNodeType_StructArray);
            
            *currentMembers = (*currentMembers)->parent;
        }
        
        BE_MemberNode* member = (*currentMembers)->value_StructArray.array + arrayIndex;
        
        BEu32 memberCapacity = (*currentMembers)->value_StructArray.memberCapacity;
        BEbool isLastArrayElement = (!(*currentMembers)->value_StructArray.arrayCount || arrayIndex == (*currentMembers)->value_StructArray.arrayCount - 1);
        
        if (memberCapacity < memberCount)
        {
            member->value_Struct.members = BEAllocArray(allocator, BE_MemberNode, memberCount);
            
            for (BEu32 i = 0; i < memberCount; ++i)
            {
                BE_ClearStructToZero(member->value_Struct.members[i]);
                member->value_Struct.members[i].parent = member;
            }
        }
        
        for (BEu32 i = 0; i < memberCount; ++i)
        {
            member->value_Struct.members[i].appearCount = 0;
        }
        
        if (isLastArrayElement && (*currentMembers)->value_StructArray.memberCapacity < memberCount)
        {
            (*currentMembers)->value_StructArray.memberCapacity = memberCount;
        }
        
        *currentMembers = member;
    }
}

BEPrivateAPI BE_MemberNode* BE_GetNestedMember(BE_MemberNode* members, BE_RuntimeEventType* error, BEu32 memberIndex, BEs64 arrayIndex)
{
    BE_MemberNode* arrayMember = BE_GetMemberNode(members, memberIndex);
    
    BE_MemberNode* rVal = 0;
    
    if (!arrayMember->appearCount)
    {
        *error = BE_RuntimeEventType_Error_FetchMemberNoSuchMember;
    }
    else if (!arrayMember->value_StructArray.arrayCount)
    {
        BEAssert(!arrayIndex);
        rVal = arrayMember->value_StructArray.array;
    }
    else if (0 <= arrayIndex && (BEu64)arrayIndex < arrayMember->value_StructArray.arrayCount)
    {
        // NOTE(Jens): No need to check 'appearCount' for the array elements. If array has appeared, so has the elements.
        rVal = arrayMember->value_StructArray.array + arrayIndex;
    }
    else
    {
        *error = BE_RuntimeEventType_Error_FetchMemberBadArrayIndex;
    }
    
    return rVal;
}

BEPrivateAPI BE_MemberNode* BE_GetScalarMember(BE_MemberNode* members, BE_RuntimeEventType* error, BEu32 memberIndex, BEs64 arrayIndex)
{
    BE_MemberNode* rVal = 0;
    
    BE_MemberNode* member = BE_GetMemberNode(members, memberIndex);
    
    if (!member->appearCount)
    {
        *error = BE_RuntimeEventType_Error_FetchMemberNoSuchMember;
    }
    else
    {
        // NOTE(Jens): If member has not appeared, we don't know which type it is.
        BEAssert(member->type == BE_MemberNodeType_Scalar);
        
        if (!member->value_Scalar.arrayCount)
        {
            BEAssert(!arrayIndex);
            rVal = member;
        }
        else if (0 <= arrayIndex && (BEu64)arrayIndex < member->value_Scalar.arrayCount)
        {
            rVal = member;
        }
        else
        {
            *error = BE_RuntimeEventType_Error_FetchMemberBadArrayIndex;
        }
    }
    
    return rVal;
}

BEPrivateAPI BEu32 BE_BinarySearch(BEu32 count, BEs64* values, BEs64 value)
{
    BEu32 leftIndex = 0;
    
    if (count > 0)
    {
        BEu32 rightIndex = count - 1;
        
        while (leftIndex != rightIndex)
        {
            BEu32 midIndex = (leftIndex + rightIndex + 1) / 2;
            if (values[midIndex] > value)
            {
                rightIndex = midIndex - 1;
            }
            else
            {
                leftIndex = midIndex;
            }
        }
    }
    
    if (values[leftIndex] == value)
    {
        ++leftIndex;
    }
    else
    {
        leftIndex = 0;
    }
    
    return leftIndex;
}

BEPrivateAPI BEu32 BE_FindStartAddress(BEu8* categoriesSection, BEu32 categoryIndex, BEs64 categoryNumber)
{
    BEu32 rVal = 0;
    
    BEu32 categoryCount = *(BEu32*)(categoriesSection + 0);
    BEAssert(categoryIndex < categoryCount);
    
    BEu32* firstStructIndexOfCategories = (BEu32*)(categoriesSection + 4);
    BEu32 totalAmountOfCategorizedStructs = firstStructIndexOfCategories[categoryCount];
    
    BEu32 categoryNumbersOffset = 4 * (1 + (categoryCount + 1) + totalAmountOfCategorizedStructs);
    
    {
        BEu32 alignment = 8;
        BEu32 alignmentMask = alignment - 1;
        if (categoryNumbersOffset & alignmentMask)
        {
            categoryNumbersOffset += alignment - (categoryNumbersOffset & alignmentMask);
        }
    }
    
    BEs64* categoryNumbers = (BEs64*)(categoriesSection + categoryNumbersOffset);
    BEu32* startAddresses = (BEu32*)(categoriesSection + 4 * (1 + (categoryCount + 1)));
    
    BEu32* firstStructIndexOfCategory = firstStructIndexOfCategories + categoryIndex;
    BEu32* endStructIndexOfCategory = firstStructIndexOfCategories + categoryIndex + 1;
    
    BEu32 structCountOfCategory = *endStructIndexOfCategory - *firstStructIndexOfCategory;
    BEs64* categoryNumbersOfCategory = categoryNumbers + *firstStructIndexOfCategory;
    
    BEu32 categorizedStructIndexPlusOne = BE_BinarySearch(structCountOfCategory, categoryNumbersOfCategory, categoryNumber);
    if (categorizedStructIndexPlusOne)
    {
        rVal = 1 + startAddresses[*firstStructIndexOfCategory + (categorizedStructIndexPlusOne - 1)];
    }
    
    return rVal;
}

BEPrivateAPI BESourceLocation BE_FindSourceAtAddress(BEu8* sourcesSection, BEu32 address)
{
    BEu32 fileCount = *(BEu32*)(sourcesSection + 0);
    BEu32 instructionCount = *(BEu32*)(sourcesSection + 4);
    BEu32* addresses = (BEu32*)(sourcesSection + 4 + 4 + 4 * (fileCount + 1) + 8 * fileCount);
    
    // NOTE(Jens): Alignment offset.
    if ((BEu64)addresses & 0x7)
    {
        ++addresses;
    }
    
    BESourceLocation* sourceLocations = (BESourceLocation*)(addresses + instructionCount);
    
    BEu32 count = instructionCount;
    BEu32 left = 0;
    
    // NOTE(Jens): Binary search, addresses are assumed to be in order.
    
    while (count)
    {
        BEu32 step = count / 2;
        BEu32 it = left + step;
        
        if (addresses[it] <= address)
        {
            left = it + 1;
            count -= step + 1;
        }
        else
        {
            count = step;
        }
    }
    
    BESourceLocation rVal;
    if (left)
    {
        rVal = sourceLocations[left - 1];
    }
    else
    {
        // NOTE(Jens): Requested address is before any recorded statement... Seems very suspicious.
        BEAssert(0);
        rVal = sourceLocations[0];
    }
    return rVal;
}

BEPrivateAPI char* BE_FindSourceFilename(BEu8* sourcesSection, BEu32* size, BEu32 fileIndex)
{
    BEu32 fileCount = *(BEu32*)(sourcesSection + 0);
    BEAssert(fileIndex < fileCount);
    
    BEu32 instructionCount = *(BEu32*)(sourcesSection + 4);
    BEu32* firstCharIndexOfFilePath = (BEu32*)(sourcesSection + 4 + 4);
    
    BEu32* addresses = (BEu32*)(sourcesSection + 4 + 4 + 4 * (fileCount + 1) + 8 * fileCount);
    
    // NOTE(Jens): Alignment offset.
    if ((BEu64)addresses & 0x7)
    {
        ++addresses;
    }
    
    BESourceLocation* sourceLocations = (BESourceLocation*)(addresses + instructionCount);
    
    char* nullTerminatedPaths = (char*)(sourceLocations + instructionCount);
    
    *size = firstCharIndexOfFilePath[fileIndex + 1] - firstCharIndexOfFilePath[fileIndex] - 1;
    char* rVal = nullTerminatedPaths + firstCharIndexOfFilePath[fileIndex];
    
    return rVal;
}

BEPrivateAPI BE_String BE_GetStructName(BEu8* typesSection, BEu32 structIndex)
{
    BEu32 structCount = *(BEu32*)(typesSection + 0);
    BEAssert(structIndex < structCount);
    
    BEu32* firstMemberIndexOfStructs = (BEu32*)(typesSection + 4);
    BEu32 totalMemberCount = firstMemberIndexOfStructs[structCount];
    
    BEu32* firstCharIndexOfStructs = firstMemberIndexOfStructs + (structCount + 1);
    BEu32* firstCharIndexOfMembers = firstCharIndexOfStructs + (structCount + 1);
    
    BEu8* nullTerminatedNames = (BEu8*)(firstCharIndexOfMembers + totalMemberCount + 1);
    
    BE_String rVal;
    rVal.count = firstCharIndexOfStructs[structIndex + 1] - firstCharIndexOfStructs[structIndex] - 1;
    rVal.characters = (char*)nullTerminatedNames + firstCharIndexOfStructs[structIndex];
    
    return rVal;
}

BEPrivateAPI BEu32 BE_GetStructMemberCount(BEu8* typesSection, BEu32 structIndex)
{
    BEu32 structCount = *(BEu32*)(typesSection + 0);
    BEAssert(structIndex < structCount);
    
    BEu32* firstMemberIndexOfStructs = (BEu32*)(typesSection + 4);
    
    BEu32 rVal = firstMemberIndexOfStructs[structIndex + 1] - firstMemberIndexOfStructs[structIndex];
    return rVal;
}

BEPrivateAPI BE_String BE_GetStructMemberName(BEu8* typesSection, BEu32 structIndex, BEu32 memberIndex)
{
    BEu32 structCount = *(BEu32*)(typesSection + 0);
    BEAssert(structIndex < structCount);
    
    BEu32* firstMemberIndexOfStructs = (BEu32*)(typesSection + 4);
    BEu32 totalMemberCount = firstMemberIndexOfStructs[structCount];
    
    BEu32* firstCharIndexOfStructs = firstMemberIndexOfStructs + (structCount + 1);
    BEu32* firstCharIndexOfMembers = firstCharIndexOfStructs + (structCount + 1);
    
    BEu8* nullTerminatedNames = (BEu8*)(firstCharIndexOfMembers + totalMemberCount + 1);
    
    BEu32 globalMemberIndex = firstMemberIndexOfStructs[structIndex] + memberIndex;
    
    BE_String rVal;
    rVal.count = firstCharIndexOfMembers[globalMemberIndex + 1] - firstCharIndexOfMembers[globalMemberIndex] - 1;
    rVal.characters = (char*)nullTerminatedNames + firstCharIndexOfMembers[globalMemberIndex];
    
    return rVal;
}

BEPrivateAPI BE_String BE_GetEnumName(BEu8* enumsSection, BEu32 enumIndex)
{
    BEu32 enumCount = *(BEu32*)(enumsSection + 0);
    BEAssert(enumIndex < enumCount);
    
    BEu32* firstMemberIndexOfEnums = (BEu32*)(enumsSection + 4);
    BEu32 totalMemberCount = firstMemberIndexOfEnums[enumCount];
    
    BEu32* firstCharIndexOfEnums = firstMemberIndexOfEnums + (enumCount + 1);
    BEu32* firstCharIndexOfMembers = firstCharIndexOfEnums + (enumCount + 1);
    
    BEu32* endOfCharIndexOfMembers = firstCharIndexOfMembers + totalMemberCount + 1;
    
    BEs64* memberValues;
    if ((BEu64)endOfCharIndexOfMembers % (BEu64)0x8)
    {
        memberValues = (BEs64*)(endOfCharIndexOfMembers + 1);
    }
    else
    {
        memberValues = (BEs64*)endOfCharIndexOfMembers;
    }
    
    BEu8* nullTerminatedNames = (BEu8*)(memberValues + totalMemberCount);
    
    BE_String rVal;
    rVal.count = firstCharIndexOfEnums[enumIndex + 1] - firstCharIndexOfEnums[enumIndex] - 1;
    rVal.characters = (char*)nullTerminatedNames + firstCharIndexOfEnums[enumIndex];
    
    return rVal;
}

BEPrivateAPI BE_String BE_GetEnumMemberName(BEu8* enumsSection, BEu32 enumIndex, BEu32 memberIndex)
{
    BEu32 enumCount = *(BEu32*)(enumsSection + 0);
    BEAssert(enumIndex < enumCount);
    
    BEu32* firstMemberIndexOfEnums = (BEu32*)(enumsSection + 4);
    BEu32 totalMemberCount = firstMemberIndexOfEnums[enumCount];
    
    BEu32* firstCharIndexOfEnums = firstMemberIndexOfEnums + (enumCount + 1);
    BEu32* firstCharIndexOfMembers = firstCharIndexOfEnums + (enumCount + 1);
    
    BEu32* endOfCharIndexOfMembers = firstCharIndexOfMembers + totalMemberCount + 1;
    
    BEs64* memberValues;
    if ((BEu64)endOfCharIndexOfMembers % (BEu64)0x8)
    {
        memberValues = (BEs64*)(endOfCharIndexOfMembers + 1);
    }
    else
    {
        memberValues = (BEs64*)endOfCharIndexOfMembers;
    }
    
    BEu8* nullTerminatedNames = (BEu8*)(memberValues + totalMemberCount);
    
    BEu32 globalMemberIndex = firstMemberIndexOfEnums[enumIndex] + memberIndex;
    
    BE_String rVal;
    rVal.count = firstCharIndexOfMembers[globalMemberIndex + 1] - firstCharIndexOfMembers[globalMemberIndex] - 1;
    rVal.characters = (char*)nullTerminatedNames + firstCharIndexOfMembers[globalMemberIndex];
    
    return rVal;
}

BEPrivateAPI BEs64* BE_GetEnumValues(BEu8* enumsSection, BEu32 enumIndex)
{
    BEu32 enumCount = *(BEu32*)(enumsSection + 0);
    BEAssert(enumIndex < enumCount);
    
    BEu32* firstMemberIndexOfEnums = (BEu32*)(enumsSection + 4);
    BEu32 totalMemberCount = firstMemberIndexOfEnums[enumCount];
    
    BEu32* firstCharIndexOfEnums = firstMemberIndexOfEnums + (enumCount + 1);
    BEu32* firstCharIndexOfMembers = firstCharIndexOfEnums + (enumCount + 1);
    
    BEu32* endOfCharIndexOfMembers = firstCharIndexOfMembers + totalMemberCount + 1;
    
    BEs64* memberValues;
    if ((BEu64)endOfCharIndexOfMembers % (BEu64)0x8)
    {
        memberValues = (BEs64*)(endOfCharIndexOfMembers + 1);
    }
    else
    {
        memberValues = (BEs64*)endOfCharIndexOfMembers;
    }
    
    return memberValues + firstMemberIndexOfEnums[enumIndex];
}

BEPrivateAPI BEu32 BE_GetEnumMemberCount(BEu8* enumsSection, BEu32 enumIndex)
{
    BEu32 enumCount = *(BEu32*)(enumsSection + 0);
    BEAssert(enumIndex < enumCount);
    
    BEu32* firstMemberIndexOfEnums = (BEu32*)(enumsSection + 4);
    return firstMemberIndexOfEnums[enumIndex + 1] - firstMemberIndexOfEnums[enumIndex];
}

BEPrivateAPI BEu32* BE_GetEndOfFrame(BEu8* endOfStack, BEu32 frameIndex)
{
    return (BEu32*)(endOfStack - 4 * (frameIndex + 1));
}

BEPrivateAPI BEbool BE_PushFrame(BERuntime* runtime, BEu32 size)
{
    runtime->startOfFrame = runtime->stack;
    
    BEu32 endOfFrameAddress = size;
    
    if (runtime->frameCount)
    {
        BEu32* previousEndOfFrame = BE_GetEndOfFrame(runtime->stack + runtime->stackCapacity, runtime->frameCount - 1);
        endOfFrameAddress += *previousEndOfFrame;
        
        runtime->startOfFrame = runtime->stack + *previousEndOfFrame;
    }
    
    *BE_GetEndOfFrame(runtime->stack + runtime->stackCapacity, runtime->frameCount) = endOfFrameAddress;
    ++runtime->frameCount;
    
    BEbool stackOverflow = (endOfFrameAddress + runtime->frameCount * 4 > runtime->stackCapacity);
    return !stackOverflow;
}

BEPrivateAPI void BE_PopFrame(BERuntime* runtime)
{
    runtime->startOfFrame = runtime->stack;
    --runtime->frameCount;
    
    BEAssert(runtime->frameCount > 0);
    
    if (runtime->frameCount > 1)
    {
        BEu32* startOfFrame = BE_GetEndOfFrame(runtime->stack + runtime->stackCapacity, runtime->frameCount - 2);
        runtime->startOfFrame = runtime->stack + *startOfFrame;
    }
}

BEPrivateAPI BEs64* BE_GetLocalPtr(BERuntime* runtime, BEu32 index)
{
    return (BEs64*)(runtime->startOfFrame + 8 * index);
}

BEPrivateAPI BEs64* BE_GetGlobalPtr(BERuntime* runtime, BEu32 index)
{
    return (BEs64*)(runtime->stack + 8 * index);
}

BEPrivateAPI BEs64* BE_GetParentPtr(BERuntime* runtime, BEu32 index)
{
    BEu8* parentFrame = runtime->stack;
    
    BEAssert(runtime->frameCount >= 1);
    if (runtime->frameCount > 2)
    {
        BEu32* startOfFrame = BE_GetEndOfFrame(runtime->stack + runtime->stackCapacity, runtime->frameCount - 3);
        parentFrame = runtime->stack + *startOfFrame;
    }
    
    return (BEs64*)(parentFrame + 8 * index);
}

BEPrivateAPI void BE_SwapByteOrder(BEu64 size, BEu8* value)
{
    for (BEu64 i = 0; i < size / 2; ++i)
    {
        BEu8 tmp = value[i];
        value[i] = value[size - 1 - i];
        value[size - 1 - i] = tmp;
    }
}

BEPrivateAPI BEbool BE_ValueIs(BEu8* data, BEu64 size, BEbool dataIsBigEndian, BEu8* expectedValue)
{
    BEu8* firstByteOfMember = data;
    BEu8* endByteOfMember = firstByteOfMember + size;
    BEu8* valueByte = expectedValue;
    
    BEbool rVal = 1;
    
    if (!dataIsBigEndian)
    {
        for (BEu8* it = firstByteOfMember; rVal && it < endByteOfMember; )
        {
            rVal = rVal && (*it++ == *valueByte++);
        }
    }
    else
    {
        for (BEu8* it = endByteOfMember; rVal && it < firstByteOfMember; )
        {
            rVal = rVal && (*--it == *valueByte++);
        }
    }
    
    return rVal;
}

BEPrivateAPI BERuntimeEvent BE_RunUntilEventOfInterest(BERuntime* runtime)
{
    BERuntimeEvent rVal;
    BE_ClearStructToZero(rVal);
    
    rVal.type = BE_RuntimeEventType_End;
    
    while (runtime->currentAddress < runtime->instructionsSizeInBytes)
    {
        BERuntimeEvent event = BEStepOne(runtime);
        if (event.type)
        {
            rVal = event;
            break;
        }
    }
    
    return rVal;
}

struct BE_FileParsing
{
    BE_FileParsing* prev;
    
    BEu32 fileIndex;
    BE_TokenizerState tokenizer;
    BEu32 size;
    BEu32 charIndex;
    BEu8* data;
    BEu8* tokenStart;
    BEu8* statementStart;
    
    BEu32 lineCount;
    BEu32 bytesSinceStartOfLine;
    
    BEu32 lineCountAtTokenStart;
    BEu32 bytesSinceStartOfLineAtTokenStart;
    
    BEu32 lineCountAtStatementStart;
    BEu32 bytesSinceStartOfLineAtStatementStart;
};

BEPrivateAPI BE_String BE_NullterminateString(BEAllocatorType allocator, BE_String str)
{
    BE_String rVal;
    rVal.count = str.count;
    rVal.characters = BEAllocArray(allocator, char, str.count + 1);
    memcpy(rVal.characters, str.characters, str.count);
    rVal.characters[rVal.count] = 0;
    
    return rVal;
}

BEPrivateAPI BEu32 BE_PushImportedFile(BEAllocatorType allocator, BE_ImportedFiles* imported, BEu64 lastFileWrite, BE_String filename, BE_String content)
{
    BEu32 localIndex = imported->count % BE_ArrayCount(imported->head->e);
    
    if (localIndex == 0)
    {
        BE_ImportedFilesChunk* newChunk = BEAlloc(allocator, BE_ImportedFilesChunk);
        newChunk->next = 0;
        
        if (imported->head)
        {
            imported->tail->next = imported->tail = newChunk;
        }
        else
        {
            imported->head = imported->tail = newChunk;
        }
    }
    
    imported->tail->lastWriteFileTimestamp[localIndex] = lastFileWrite;
    imported->tail->e[localIndex] = BE_NullterminateString(allocator, filename);
    imported->tail->content[localIndex] = content;
    return imported->count++;
}

BEPrivateAPI void BE_PushFileParsing(BEAllocatorType allocator, BE_FileParsings* parsings, BE_String filename, BEu64 lastFileWrite, BEu32 size, BEu8* data)
{
    BE_FileParsing* newEntry = parsings->free;
    
    if (newEntry)
    {
        parsings->free = parsings->free->prev;
    }
    else
    {
        newEntry = BEAlloc(allocator, BE_FileParsing);
    }
    BE_ClearStructToZero(*newEntry);
    
    newEntry->size = size;
    newEntry->tokenStart = newEntry->data = data;
    
    newEntry->prev = parsings->top;
    parsings->top = newEntry;
    
    ++parsings->depth;
    
    BE_String content;
    content.count = size;
    content.characters = (char*)data;
    
    newEntry->fileIndex = BE_PushImportedFile(allocator, &parsings->imported, lastFileWrite, filename, content);
}

BEPrivateAPI void BE_PopFileParsing(BE_FileParsings* parsings)
{
    BE_FileParsing* oldEntry = parsings->top;
    
    parsings->top = parsings->top->prev;
    
    oldEntry->prev = parsings->free;
    parsings->free = oldEntry;
    
    --parsings->depth;
}

BEPrivateAPI void BE_PushSourceLocation(BEAllocatorType allocator, BE_SourceLocations* sourceLocations, BEu32 firstInstructionIndex, BESourceLocation location)
{
    BEu32 localIndex = sourceLocations->count % BE_ArrayCount(sourceLocations->head->locations);
    
    if (localIndex == 0)
    {
        BE_SourceLocationChunk* newChunk = BEAlloc(allocator, BE_SourceLocationChunk);
        newChunk->next = 0;
        
        if (sourceLocations->head)
        {
            sourceLocations->tail->next = sourceLocations->tail = newChunk;
        }
        else
        {
            sourceLocations->head = sourceLocations->tail = newChunk;
        }
    }
    
    sourceLocations->tail->startInstructionIndices[localIndex] = firstInstructionIndex;
    sourceLocations->tail->locations[localIndex] = location;
    ++sourceLocations->count;
}

struct BE_TokenEvaluation
{
    BECompilationEvent event;
    BE_StatementType evaluatedStatement;
    BEu32 instructionIndexStart;
    BEu32 instructionIndexEnd;
};
typedef struct BE_TokenEvaluation BE_TokenEvaluation;

BEPrivateAPI BE_TokenEvaluation BE_EvaluateToken(BECompilationContext* context, BE_TokenType tokenType, BE_TokenData tokenData, BEu8* tokenStart, BEu8* tokenEnd)
{
    BE_TokenEvaluation rVal;
    BE_ClearStructToZero(rVal);
    
    BE_StatementType statementType;
    BE_Statement statement;
    
    BE_Expressions* expressions = &context->expressions;
    BE_ParserState* parser = &context->parser;
    BE_InstructionAllocator* instructionAlloc = &context->instructionAlloc;
    BEAllocatorType allocator = context->instructionAlloc.allocator;
    BE_InstructionGenerator* instructionGenerator = &context->instructionGenerator;
    
    if (BE_FeedParser(parser, &statementType, &statement, tokenType, tokenData, (BEu32)(tokenEnd - tokenStart), (char*)tokenStart))
    {
        if (statementType <= 0)
        {
            rVal.event.type = (BECompilationEventType)(BECompilationEventType_Error_InvalidStatement + -(BEs32)statementType);
        }
        else
        {
            BE_ArithmeticError error = BE_ArithmeticError_None;
            
            rVal.evaluatedStatement = statementType;
            
            rVal.instructionIndexStart = instructionGenerator->instructionIndex;
            BE_InstructionList list = BE_FeedInstructionGenerator(instructionAlloc, expressions, instructionGenerator, &error, statementType, &statement);
            rVal.instructionIndexEnd = instructionGenerator->instructionIndex;
            
            if (error)
            {
                switch (error)
                {
                    BE_InvalidDefault;
                    
                    case BE_ArithmeticError_None: 
                    {
                    } break;
                    case BE_ArithmeticError_ModByZero:
                    {
                        rVal.event.type = BECompilationEventType_Error_InvalidStatement_ConstantExpression_ModZero;
                    } break;
                    case BE_ArithmeticError_DivByZero:
                    {
                        rVal.event.type = BECompilationEventType_Error_InvalidStatement_ConstantExpression_DivZero;
                    } break;
                }
            }
            else if (list.head)
            {
                BE_InstructionEncoder* encoder = &context->encoder;
                
                for (BE_InstructionListNode* it = list.head; it; it = it->next)
                {
                    BE_FeedInstructionEncoder(instructionAlloc->allocator, encoder, &it->instruction);
                }
                
                context->pendingResult = BE_FlushInstructionBuffer(encoder);
                context->readBytes = 0;
            }
            
            if (context->parsings.depth - 1 != parser->imports.depth)
            {
                // NOTE(Jens): The only way depth changes is if an import statement was parsed.
                
                BEAssert(context->parsings.depth == parser->imports.depth);
                
                BEFileLoader* fileLoader = &context->fileLoader;
                BE_String filename = parser->imports.top->nameNode->value;
                
                BELoadFileResult fileLoadRes = fileLoader->loadFile(fileLoader->context, allocator, (BEu32)filename.count, filename.characters);
                
                if (!fileLoadRes.error)
                {
                    BE_PushFileParsing(allocator, &context->parsings, filename, fileLoadRes.lastFileWrite, fileLoadRes.fileSizeInBytes, (BEu8*)fileLoadRes.fileContent);
                }
                else
                {
                    rVal.event.type = BECompilationEventType_Error_FileLoadFailed;
                    rVal.event.fileLoadError = fileLoadRes.error;
                }
            }
        }
    }
    
    return rVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// NOTE(Jens): Public API starts here.


BEPublicAPI char* BEGetCompilationErrorDescription(BECompilationEventType event)
{
    char* desc[] = {
        "",                                                    // BECompilationEventType_Continue
        "",                                                    // BECompilationEventType_Complete,
        "",                                                    // BECompilationEventType_FirstError,
        "failed to load file",                                 // BECompilationEventType_Error_FileLoadFailed,
        "invalid token",                                       // BECompilationEventType_Error_InvalidToken,
        "invalid statement",                                   // BECompilationEventType_Error_InvalidStatement,
        "expected ';'",                                        // BECompilationEventType_Error_InvalidStatement_ExpectedSemicolon,
        "expected ','",                                        // BECompilationEventType_Error_InvalidStatement_ExpectedComma,
        "expected '.'",                                        // BECompilationEventType_Error_InvalidStatement_ExpectedDot,
        "expected '='",                                        // BECompilationEventType_Error_InvalidStatement_ExpectedEquals,
        "expected '['",                                        // BECompilationEventType_Error_InvalidStatement_ExpectedOpenSquareBracket,
        "expected ']'",                                        // BECompilationEventType_Error_InvalidStatement_ExpectedCloseSquareBracket,
        "expected '('",                                        // BECompilationEventType_Error_InvalidStatement_ExpectedOpenParenthesis,
        "expected ')'",                                        // BECompilationEventType_Error_InvalidStatement_ExpectedCloseParenthesis,
        "expected '{'",                                        // BECompilationEventType_Error_InvalidStatement_ExpectedOpenCurlyBracket,
        "expected '}'",                                        // BECompilationEventType_Error_InvalidStatement_ExpectedCloseCurlyBracket,
        "invalid integer expression",                          // BECompilationEventType_Error_InvalidStatement_IntegerExpression,
        "duplicate enum member",                               // BECompilationEventType_Error_InvalidStatement_DuplicateEnumMemberNames,
        "duplicate typedef",                                   // BECompilationEventType_Error_InvalidStatement_DuplicateTypedefs,
        "expected variable name",                              // BECompilationEventType_Error_InvalidStatement_ExpectedVariable,
        "expected assignment or compound assignment operator", // BECompilationEventType_Error_InvalidStatement_ExpectedAssignmentOperator,
        "expected ',' or ')'",                                 // BECompilationEventType_Error_InvalidStatement_ExpectedCommaOrCloseParenthesis,
        "expected ',' or '}'",                                 // BECompilationEventType_Error_InvalidStatement_ExpectedCommaOrCloseCurlyBracket,
        "expected identifier",                                 // BECompilationEventType_Error_InvalidStatement_ExpectedIdentifier,
        "expected \"\" or '' quoted string",                   // BECompilationEventType_Error_InvalidStatement_ExpectedQuotedString,
        "expected struct name",                                // BECompilationEventType_Error_InvalidStatement_ExpectedStructName,
        "expected identifier or '('",                          // BECompilationEventType_Error_InvalidStatement_ExpectedIdentifierOrOpenParenthesis,
        "expected 'while'",                                    // BECompilationEventType_Error_InvalidStatement_ExpectedKeyword_while,
        "expected 'default'",                                  // BECompilationEventType_Error_InvalidStatement_ExpectedKeyword_default,
        "expected 'byteorder'",                                // BECompilationEventType_Error_InvalidStatement_ExpectedKeyword_byteorder,
        "expected 'var'",                                      // BECompilationEventType_Error_InvalidStatement_ExpectedKeyword_var,
        "expected 'little' or 'big'",                          // BECompilationEventType_Error_InvalidStatement_ExpectedKeyword_little_or_big,
        "expected 'var' or ';'",                               // BECompilationEventType_Error_InvalidStatement_ExpectedKeyword_var_or_semicolon,
        "expected 's' or 'u'",                                 // BECompilationEventType_Error_InvalidStatement_ExpectedKeyword_s_or_u,
        "expected start of statement",                         // BECompilationEventType_Error_InvalidStatement_ExpectedStatementStart,
        "expected type name",                                  // BECompilationEventType_Error_InvalidStatement_ExpectedType,
        "expected display or byte order specifier",            // BECompilationEventType_Error_InvalidStatement_ExpectedDisplayKeywordOrByteOrder,
        "invalid enum size, must be <= 8",                     // BECompilationEventType_Error_InvalidStatement_InvalidEnumSize,
        "duplicate parameter name",                            // BECompilationEventType_Error_InvalidStatement_DuplicateParameterName,
        "duplicate struct name",                               // BECompilationEventType_Error_InvalidStatement_DuplicateStructName,
        "invalid constant lookup",                             // BECompilationEventType_Error_InvalidStatement_ConstantLookupFailure,
        "expected category name",                              // BECompilationEventType_Error_InvalidStatement_ExpectedCategoryName,
        "expected member name",                                // BECompilationEventType_Error_InvalidStatement_ExpectedMemberName,
        "duplicate display specifier",                         // BECompilationEventType_Error_InvalidStatement_DuplicateDisplayKeyword,
        "duplicate byte order specifier",                      // BECompilationEventType_Error_InvalidStatement_DuplicateByteOrder,
        "too many arguments",                                  // BECompilationEventType_Error_InvalidStatement_TooManyArguments,
        "not enough arguments",                                // BECompilationEventType_Error_InvalidStatement_NotEnoughArguments,
        "division with zero",                                  // BECompilationEventType_Error_InvalidStatement_ConstantExpression_DivZero,
        "modulo with zero",                                    // BECompilationEventType_Error_InvalidStatement_ConstantExpression_ModZero,
        "expected '(', assignment or compound assignment",     // BECompilationEventType_Error_InvalidStatement_ExpectedOpenParenthesisOrAssignmentOperator,
        "expected array member",                               // BECompilationEventType_Error_InvalidStatement_ExpectedArrayMember,
        "variable name already in use",                        // BECompilationEventType_Error_InvalidStatement_DuplicateVariableName,
    };
    
    return desc[event];
}

BEPublicAPI BEFileLoadError BEStartCompilation(BECompilationContext* out, BEAllocatorType allocator, BEFileLoader fileLoader, BEu32 pathLen_, const char* nullTerminatedName)
{
    BEu32 pathLen = pathLen_;
    if (!pathLen)
    {
        while (nullTerminatedName[pathLen])
        {
            ++pathLen;
        }
    }
    
    BELoadFileResult loadRes = fileLoader.loadFile(fileLoader.context, allocator, pathLen, nullTerminatedName);
    
    if (!loadRes.error)
    {
        BECompilationContext context;
        BE_ClearStructToZero(context);
        
        context.parser = BE_CreateParserState(allocator);
        context.instructionAlloc.allocator = allocator;
        context.instructionGenerator = BE_StartInstructionGeneration();
        context.fileLoader = fileLoader;
        
        BE_String filename;
        filename.count = pathLen;
        filename.characters = (char*)nullTerminatedName;
        
        BE_PushFileParsing(allocator, &context.parsings, filename, loadRes.lastFileWrite, loadRes.fileSizeInBytes, (BEu8*)loadRes.fileContent);
        
        *out = context;
    }
    
    return loadRes.error;
}

BEPublicAPI BECompilationEvent BEGenerateInstructions(BECompilationContext* context, BEu32* capacityIn_sizeOut, BEu8* encodedInstructionsOut)
{
    BECompilationEvent rVal;
    BE_ClearStructToZero(rVal);
    
    BEAllocatorType allocator = context->instructionAlloc.allocator;
    
    // IMPORTANT(Jens): While there is 'pendingResult' don't feed instruction generator. 
    //                  The instructions are on a freelist and would get overwritten.
    
    if (!context->pendingResult.writtenBytes && context->parsings.top)
    {
        BE_FileParsing* parsing = context->parsings.top;
        BE_ParserState* parser = &context->parser;
        
        BEu8 chr = (parsing->charIndex == parsing->size) ? 0 : parsing->data[parsing->charIndex];
        
        BE_TokenType tokenType;
        BE_TokenData tokenData;
        
        BEu32 charIndex = parsing->charIndex++;
        
        BEu8* parsingStart = parsing->data;
        BEu8* tokenStart = parsing->tokenStart;
        BEu32 fileIndex = parsing->fileIndex;
        BEu32 bytesSinceStartOfLine = parsing->bytesSinceStartOfLine;
        BEu32 lineCount = parsing->lineCount;
        
        BEu32 bytesSinceStartOfLineAtTokenStart = parsing->bytesSinceStartOfLineAtTokenStart;
        BEu32 lineCountAtTokenStart = parsing->lineCountAtTokenStart;
        
        if (chr == '\n')
        {
            ++parsing->lineCount;
            parsing->bytesSinceStartOfLine = 0;
        }
        else
        {
            ++parsing->bytesSinceStartOfLine;
        }
        
        if (BE_FeedTokenizer(&parsing->tokenizer, chr, &tokenType, &tokenData))
        {
            BEu8* tokenEnd = parsing->data + charIndex;
            parsing->tokenStart = tokenEnd;
            
            if (!parsing->statementStart && tokenType != BE_TokenType_Whitespace && tokenType != BE_TokenType_MultilineComment && tokenType != BE_TokenType_LineComment)
            {
                parsing->statementStart = tokenStart;
                parsing->lineCountAtStatementStart = parsing->lineCountAtTokenStart;
                parsing->bytesSinceStartOfLineAtStatementStart = parsing->bytesSinceStartOfLineAtTokenStart;
            }
            
            parsing->lineCountAtTokenStart = lineCount;
            parsing->bytesSinceStartOfLineAtTokenStart = bytesSinceStartOfLine;
            
            if (tokenType != BE_TokenType_Invalid)
            {
                BE_TokenEvaluation tokenEvaluation = BE_EvaluateToken(context, tokenType, tokenData, tokenStart, tokenEnd);
                rVal = tokenEvaluation.event;
                
                if (tokenEvaluation.evaluatedStatement)
                {
                    BEAssert(parsing->statementStart); // NOTE(Jens): A statement must have at least one non-whitespace / comment token.
                    
                    if (tokenEvaluation.instructionIndexStart != tokenEvaluation.instructionIndexEnd)
                    {
                        BESourceLocation location;
                        location.fileIndex = parsing->fileIndex;
                        location.sizeInBytes = (BEu32)(tokenEnd - parsing->statementStart);
                        location.byteIndex = (BEu32)(parsing->statementStart - parsing->data);
                        location.lineIndex = parsing->lineCountAtStatementStart;
                        location.bytesSinceStartOfLine = parsing->bytesSinceStartOfLineAtStatementStart;
                        BE_PushSourceLocation(allocator, &context->sourceLocations, tokenEvaluation.instructionIndexStart, location);
                    }
                    
                    parsing->statementStart = 0;
                }
            }
            else
            {
                rVal.type = BECompilationEventType_Error_InvalidToken;
            }
        }
        
        while (context->parsings.top && context->parsings.top->charIndex == context->parsings.top->size)
        {
            if (rVal.type < BECompilationEventType_FirstError)
            {
                // NOTE(Jens): Allow parser to evaluate end of file to catch incomplete statements. All statements must be finished before end of file.
                
                BE_TokenData invalidTokenData;
                BE_ClearStructToZero(invalidTokenData); // NOTE(Jens): Just to supress warnings regarding uninitialized variable.
                
                BEu8* endOfData = context->parsings.top->tokenStart;
                
                BE_TokenEvaluation endOfFileCompletion = BE_EvaluateToken(context, BE_TokenType_EndOfTokens, invalidTokenData, endOfData, endOfData);
                if (endOfFileCompletion.event.type >= BECompilationEventType_FirstError)
                {
                    rVal = endOfFileCompletion.event;
                    
                    fileIndex = parsing->fileIndex;
                    bytesSinceStartOfLine = parsing->bytesSinceStartOfLine;
                    lineCount = parsing->lineCount;
                    bytesSinceStartOfLineAtTokenStart = parsing->bytesSinceStartOfLineAtTokenStart;
                    lineCountAtTokenStart = parsing->lineCountAtTokenStart;
                }
                
                if (endOfFileCompletion.evaluatedStatement)
                {
                    if (endOfFileCompletion.instructionIndexStart != endOfFileCompletion.instructionIndexEnd)
                    {
                        BESourceLocation location;
                        location.fileIndex = parsing->fileIndex;
                        location.sizeInBytes = (BEu32)(endOfData - parsing->statementStart);
                        location.byteIndex = (BEu32)(parsing->statementStart - parsing->data);
                        location.lineIndex = parsing->lineCountAtStatementStart;
                        location.bytesSinceStartOfLine = parsing->bytesSinceStartOfLineAtStatementStart;
                        BE_PushSourceLocation(allocator, &context->sourceLocations, endOfFileCompletion.instructionIndexStart, location);
                    }
                    
                    parsing->statementStart = endOfData;
                    parsingStart = parsing->data;
                    tokenStart = parsing->tokenStart;
                }
            }
            
            BE_TryPopImport(&parser->imports);
            BE_PopFileParsing(&context->parsings);
            
            parsing = context->parsings.top;
        }
        
        if (rVal.type >= BECompilationEventType_FirstError)
        {
            rVal.sourceFileIndex = fileIndex;
            rVal.sourceLineIndex = lineCountAtTokenStart;
            rVal.sourceColIndex = bytesSinceStartOfLineAtTokenStart;
            rVal.sourceByteIndex = (BEu32)(tokenStart - parsingStart);
        }
    }
    
    if (rVal.type < BECompilationEventType_FirstError && !context->parsings.top && context->readBytes >= context->pendingResult.writtenBytes)
    {
        rVal.type = BECompilationEventType_Complete;
    }
    
    if (context->readBytes < context->pendingResult.writtenBytes)
    {
        BEu32 bytesToRead = context->pendingResult.writtenBytes - context->readBytes;
        if (bytesToRead > *capacityIn_sizeOut)
        {
            bytesToRead = *capacityIn_sizeOut;
        }
        
        *capacityIn_sizeOut = bytesToRead;
        
        BE_EncodedInstructionChunk* chunk = context->pendingResult.head;
        BEu32 readStartInChunk = context->readBytes;
        
        while (readStartInChunk >= BE_ArrayCount(chunk->data))
        {
            readStartInChunk -= BE_ArrayCount(chunk->data);
            chunk = chunk->next;
        }
        
        BEu8* in = chunk->data + readStartInChunk;
        BEu8* out = encodedInstructionsOut;
        
        for (BEu32 i = 0; i < bytesToRead; ++i)
        {
            if (in == chunk->data + BE_ArrayCount(chunk->data))
            {
                chunk = chunk->next;
                in = chunk->data;
            }
            
            *out++ = *in++;
        }
        
        context->readBytes += bytesToRead;
        
        if (context->readBytes == context->pendingResult.writtenBytes)
        {
            context->readBytes = 0;
            BE_ClearStructToZero(context->pendingResult);
        }
    }
    else
    {
        *capacityIn_sizeOut = 0;
    }
    
    return rVal;
}

BEPublicAPI BEu32 BEGetFileCount(BECompilationContext* context)
{
    return context->parsings.imported.count;
}

BEPublicAPI char* BEGetFileContent(BECompilationContext* context, BEu32 fileIndex, BEu32* length)
{
    char* rVal = 0;
    
    if (fileIndex < context->parsings.imported.count)
    {
        BEu32 index = fileIndex;
        BE_ImportedFilesChunk* importedChunk = context->parsings.imported.head;
        
        while (index >= BE_ArrayCount(importedChunk->content))
        {
            index -= BE_ArrayCount(importedChunk->content);
            importedChunk = importedChunk->next;
        }
        
        rVal = importedChunk->content[index].characters;
        
        if (length)
        {
            *length = (BEu32)importedChunk->content[index].count;
        }
    }
    
    return rVal;
}

BEPublicAPI char* BEGetFilename(BECompilationContext* context, BEu32 fileIndex)
{
    char* rVal = 0;
    
    if (fileIndex < context->parsings.imported.count)
    {
        BEu32 index = fileIndex;
        BE_ImportedFilesChunk* importedChunk = context->parsings.imported.head;
        
        while (index >= BE_ArrayCount(importedChunk->e))
        {
            index -= BE_ArrayCount(importedChunk->e);
            importedChunk = importedChunk->next;
        }
        
        rVal = importedChunk->e[index].characters;
    }
    
    return rVal;
}

BEPublicAPI BEDataSection BEFinalizeCompilation(BECompilationContext* context, BEAllocatorType outputAllocator)
{
    BEAllocatorType allocator = context->instructionAlloc.allocator;
    
    BE_TreeIterator* treeFreelist = 0;
    BEu32* structStartAddresses = 0;
    
    if (context->parser.structs.structCount)
    {
        structStartAddresses = BEAllocArray(allocator, BEu32, context->parser.structs.structCount);
    }
    
    for (BE_ParsedStructNode* it = context->parser.structs.byOrderHead; it; it = it->nextInOrder)
    {
        BEu32 startInstructionIndex = BE_FindStartAddressOfStruct(&context->instructionGenerator.structStartAddresses, it->index)->instructionIndex;
        BEu32 address = BE_FindInstructionAddress(&context->encoder, startInstructionIndex);
        
        structStartAddresses[it->index] = address;
    }
    
    {
        BEu32 localIndex = 0;
        BE_SourceLocationChunk* chunk = context->sourceLocations.head;
        
        for (BEu32 index = 0; index < context->sourceLocations.count; ++index)
        {
            chunk->startInstructionIndices[localIndex] = BE_FindInstructionAddress(&context->encoder, chunk->startInstructionIndices[localIndex]);
            
            if (++localIndex == BE_ArrayCount(chunk->startInstructionIndices))
            {
                localIndex = 0;
                chunk = chunk->next;
            }
        }
    }
    
    BEDataSection rVal = BE_CreateDataSection(outputAllocator, &treeFreelist, 8 * context->instructionGenerator.maxStackEntryCount[0], context->instructionGenerator.instructionIndex, &context->parser.structs, &context->parser.enums, structStartAddresses, &context->sourceLocations, &context->parsings.imported);
    return rVal;
}


BEPublicAPI char* BEGetRuntimeErrorDescription(BE_RuntimeEventType event)
{
    char* desc[] = {
        "", // BE_RuntimeEventType_Generic,
        "", // BE_RuntimeEventType_Breakpoint,
        "", // BE_RuntimeEventType_Print,
        "", // BE_RuntimeEventType_ScalarExport,
        "", // BE_RuntimeEventType_StartStruct,
        "", // BE_RuntimeEventType_EndStruct,
        "", // BE_RuntimeEventType_PlotX,
        "", // BE_RuntimeEventType_PlotY,
        
        "assertion triggered",                                         // BE_RuntimeEventType_Error_AssertTriggered,
        "division by zero",                                            // BE_RuntimeEventType_Error_DivByZero,
        "modulo by zero",                                              // BE_RuntimeEventType_Error_ModByZero,
        "invalid load range",                                          // BE_RuntimeEventType_Error_FetchMemoryInvalidMemoryRange,
        "load size is too large (> 64-bit)",                           // BE_RuntimeEventType_Error_FetchMemoryTooBig,
        "category type not found",                                     // BE_RuntimeEventType_Error_CategoryTypeNotFound,
        "invalid memory range for member",                             // BE_RuntimeEventType_Error_MemberInvalidMemoryRange,
        "fetch zero sized member",                                     // BE_RuntimeEventType_Error_FetchMemberSizeIsZero,
        "fetching too large member (> 64-bit)",                        // BE_RuntimeEventType_Error_FetchMemberTooBig,
        "invalid array index",                                         // BE_RuntimeEventType_Error_FetchMemberBadArrayIndex,
        "fetching float of invalid size (only 4 and 8 supported)",     // BE_RuntimeEventType_Error_FetchMemberBadFloatSize,
        "cannot fetch member (member was excluded by branch or jump)", // BE_RuntimeEventType_Error_FetchMemberNoSuchMember,
        "array count is negative",                                     // BE_RuntimeEventType_Error_MemberInvalidArrayCount,
    };
    
    return desc[event];
}

BEPublicAPI BERuntime BEStartRuntime(BEAllocatorType allocator, BEu32 stackCapacity, BEu8* stack, BEu8* dataSection, BEu32 instructionsSizeInBytes, BEu8* encodedInstructions, BEs64 sizeOfFile, BEu8* binaryFile)
{
    BERuntime rVal;
    BE_ClearStructToZero(rVal);
    
    rVal.allocator = allocator;
    
    rVal.stackCapacity = stackCapacity;
    rVal.stack = stack;
    
    rVal.instructionsSizeInBytes = instructionsSizeInBytes;
    rVal.encodedInstructions = encodedInstructions;
    
    rVal.sizeOfFile = sizeOfFile;
    rVal.binaryFile = binaryFile;
    
    BEu64 categoriesLocation = *(BEu64*)(dataSection + 0);
    rVal.categoriesSection = dataSection + categoriesLocation;
    
    BEu64 typesLocation = *(BEu64*)(dataSection + 8);
    rVal.typesSection = dataSection + typesLocation;
    
    BEu64 enumsLocation = *(BEu64*)(dataSection + 16);
    rVal.enumsSection = dataSection + enumsLocation;
    
    BEu64 sourcesLocation = *(BEu64*)(dataSection + 24);
    if (sourcesLocation)
    {
        rVal.sourcesSection = dataSection + sourcesLocation;
    }
    
    BEu32 globalFrameSize = *(BEu32*)(dataSection + 32);
    BEu32 instructionCount = *(BEu32*)(dataSection + 36);
    
    rVal.instructionCount = instructionCount;
    
    BE_PushFrame(&rVal, globalFrameSize);
    
    // NOTE(Jens): This clears @ to zero (and the reserved entries). Local symbols are manually set to zero by 'set' instructions.
    memset(rVal.stack, 0, 8 * BE_GlobalSymbol_Count);
    
    return rVal;
}

BEPublicAPI BERuntimeEvent BEStepOne(BERuntime* runtime)
{
    BERuntimeEvent rVal;
    BE_ClearStructToZero(rVal);
    
    BEu32 instructionAddress = runtime->currentAddress;
    
    if (instructionAddress < runtime->instructionsSizeInBytes)
    {
        BEu32 instructionSize;
        rVal.instruction = BEDecodeInstruction(&instructionSize, runtime->encodedInstructions + runtime->currentAddress);
        
        runtime->currentAddress += instructionSize;
        
        switch (rVal.instruction.type)
        {
            BE_InvalidDefault;
            
            case BE_InstructionType_Set:
            {
                BEs64* result = BEGetPtr(runtime, rVal.instruction.result);
                *result = rVal.instruction.set.value;
            } break;
            
            case BE_InstructionType_StructIndex:
            {
                BEu32 memberCount = BE_GetStructMemberCount(runtime->typesSection, rVal.instruction.structIndex.index);
                BEu64 arrayIndex = (BEu64)*BE_GetLocalPtr(runtime, BE_LocalSymbol_ArrayIndex);
                BE_ObserveStructMemberTypeInfo(runtime->allocator, &runtime->currentMembers, rVal.instruction.structIndex.index, arrayIndex, memberCount);
                
                rVal.type = BE_RuntimeEventType_StartStruct;
                rVal.startStruct.structIndex = rVal.instruction.structIndex.index;
                rVal.startStruct.arrayIndex = (BEu32)*BE_GetLocalPtr(runtime, BE_LocalSymbol_ArrayIndex);
                rVal.startStruct.parentStructIndex = BEInvalidParentStructIndex;
                rVal.startStruct.arrayCount = 0;
                if (runtime->currentMembers->parent)
                {
                    rVal.startStruct.parentStructIndex = BE_GetStructIndex(runtime->currentMembers->parent->parent);
                    rVal.startStruct.memberIndex = BE_GetStructMemberIndex(runtime->currentMembers);
                    rVal.startStruct.arrayCount = (BEu32)BE_GetStructArrayCount(runtime->currentMembers);
                    rVal.startStruct.isInternal = BE_GetStructIsInternal(runtime->currentMembers);
                    rVal.startStruct.appearIndex = BE_GetStructAppearIndex(runtime->currentMembers);
                }
            } break;
            
            case BE_InstructionType_Negate:
            {
                BEs64* result = BEGetPtr(runtime, rVal.instruction.result);
                BEs64 unaryValue = *BEGetPtr(runtime, rVal.instruction.unaryValue);
                
                *result = -unaryValue;
            } break;
            case BE_InstructionType_FNegate:
            {
                BEf64* result = (BEf64*)BEGetPtr(runtime, rVal.instruction.result);
                BEf64 unaryValue = *(BEf64*)BEGetPtr(runtime, rVal.instruction.unaryValue);
                
                *result = -unaryValue;
            } break;
            
            case BE_InstructionType_LogicalNot:
            {
                BEs64* result = BEGetPtr(runtime, rVal.instruction.result);
                BEs64 unaryValue = *BEGetPtr(runtime, rVal.instruction.unaryValue);
                
                *result = !unaryValue;
            } break;
            case BE_InstructionType_BitwiseNot:
            {
                BEs64* result = BEGetPtr(runtime, rVal.instruction.result);
                BEs64 unaryValue = *BEGetPtr(runtime, rVal.instruction.unaryValue);
                
                *result = ~unaryValue;
            } break;
            
            case BE_InstructionType_Abs:
            {
                BEs64* result = BEGetPtr(runtime, rVal.instruction.result);
                BEs64 unaryValue = *BEGetPtr(runtime, rVal.instruction.unaryValue);
                
                *result = (unaryValue < 0 ? -unaryValue : unaryValue);
            } break;
            case BE_InstructionType_FAbs:
            {
                BEf64* result = (BEf64*)BEGetPtr(runtime, rVal.instruction.result);
                BEf64 unaryValue = *(BEf64*)BEGetPtr(runtime, rVal.instruction.unaryValue);
                
                *result = (unaryValue < 0.0 ? -unaryValue : unaryValue);
            } break;
            
            case BE_InstructionType_Copy:
            {
                BEs64* result = BEGetPtr(runtime, rVal.instruction.result);
                BEs64 unaryValue = *BEGetPtr(runtime, rVal.instruction.unaryValue);
                
                *result = unaryValue;
            } break;
            
            case BE_InstructionType_Add:
            {
                BEs64* result = BEGetPtr(runtime, rVal.instruction.result);
                BEs64 left = *BEGetPtr(runtime, rVal.instruction.binary.left);
                BEs64 right = *BEGetPtr(runtime, rVal.instruction.binary.right);
                
                *result = (left + right);
            } break;
            case BE_InstructionType_FAdd:
            {
                BEf64* result = (BEf64*)BEGetPtr(runtime, rVal.instruction.result);
                BEf64 left = *(BEf64*)BEGetPtr(runtime, rVal.instruction.binary.left);
                BEf64 right = *(BEf64*)BEGetPtr(runtime, rVal.instruction.binary.right);
                
                *result = (left + right);
            } break;
            
            case BE_InstructionType_Sub:
            {
                BEs64* result = BEGetPtr(runtime, rVal.instruction.result);
                BEs64 left = *BEGetPtr(runtime, rVal.instruction.binary.left);
                BEs64 right = *BEGetPtr(runtime, rVal.instruction.binary.right);
                
                *result = (left - right);
            } break;
            case BE_InstructionType_FSub:
            {
                BEf64* result = (BEf64*)BEGetPtr(runtime, rVal.instruction.result);
                BEf64 left = *(BEf64*)BEGetPtr(runtime, rVal.instruction.binary.left);
                BEf64 right = *(BEf64*)BEGetPtr(runtime, rVal.instruction.binary.right);
                
                *result = (left - right);
            } break;
            
            case BE_InstructionType_Mul:
            {
                BEs64* result = BEGetPtr(runtime, rVal.instruction.result);
                BEs64 left = *BEGetPtr(runtime, rVal.instruction.binary.left);
                BEs64 right = *BEGetPtr(runtime, rVal.instruction.binary.right);
                
                *result = (left * right);
            } break;
            case BE_InstructionType_FMul:
            {
                BEf64* result = (BEf64*)BEGetPtr(runtime, rVal.instruction.result);
                BEf64 left = *(BEf64*)BEGetPtr(runtime, rVal.instruction.binary.left);
                BEf64 right = *(BEf64*)BEGetPtr(runtime, rVal.instruction.binary.right);
                
                *result = (left * right);
            } break;
            
            case BE_InstructionType_Div:
            {
                BEs64* result = BEGetPtr(runtime, rVal.instruction.result);
                BEs64 left = *BEGetPtr(runtime, rVal.instruction.binary.left);
                BEs64 right = *BEGetPtr(runtime, rVal.instruction.binary.right);
                
                if (right == 0)
                {
                    rVal.type = BE_RuntimeEventType_Error_DivByZero;
                }
                else
                {
                    *result = (left / right);
                }
                
            } break;
            case BE_InstructionType_FDiv:
            {
                BEf64* result = (BEf64*)BEGetPtr(runtime, rVal.instruction.result);
                BEf64 left = *(BEf64*)BEGetPtr(runtime, rVal.instruction.binary.left);
                BEf64 right = *(BEf64*)BEGetPtr(runtime, rVal.instruction.binary.right);
                
                if (right == 0.0)
                {
                    rVal.type = BE_RuntimeEventType_Error_DivByZero;
                }
                else
                {
                    *result = (left / right);
                }
                
            } break;
            
            case BE_InstructionType_Mod:
            {
                BEs64* result = BEGetPtr(runtime, rVal.instruction.result);
                BEs64 left = *BEGetPtr(runtime, rVal.instruction.binary.left);
                BEs64 right = *BEGetPtr(runtime, rVal.instruction.binary.right);
                
                
                if (right == 0)
                {
                    rVal.type = BE_RuntimeEventType_Error_ModByZero;
                }
                else
                {
                    *result = (left % right);
                }
            } break;
            
            case BE_InstructionType_Less:
            {
                BEs64* result = BEGetPtr(runtime, rVal.instruction.result);
                BEs64 left = *BEGetPtr(runtime, rVal.instruction.binary.left);
                BEs64 right = *BEGetPtr(runtime, rVal.instruction.binary.right);
                
                *result = (left < right);
            } break;
            case BE_InstructionType_FLess:
            {
                BEf64* result = (BEf64*)BEGetPtr(runtime, rVal.instruction.result);
                BEf64 left = *(BEf64*)BEGetPtr(runtime, rVal.instruction.binary.left);
                BEf64 right = *(BEf64*)BEGetPtr(runtime, rVal.instruction.binary.right);
                
                *result = (left < right);
            } break;
            
            case BE_InstructionType_LessOrEqual:
            {
                BEs64* result = BEGetPtr(runtime, rVal.instruction.result);
                BEs64 left = *BEGetPtr(runtime, rVal.instruction.binary.left);
                BEs64 right = *BEGetPtr(runtime, rVal.instruction.binary.right);
                
                *result = (left <= right);
            } break;
            case BE_InstructionType_FLessOrEqual:
            {
                BEf64* result = (BEf64*)BEGetPtr(runtime, rVal.instruction.result);
                BEf64 left = *(BEf64*)BEGetPtr(runtime, rVal.instruction.binary.left);
                BEf64 right = *(BEf64*)BEGetPtr(runtime, rVal.instruction.binary.right);
                
                *result = (left <= right);
            } break;
            
            case BE_InstructionType_Equal:
            {
                BEs64* result = BEGetPtr(runtime, rVal.instruction.result);
                BEs64 left = *BEGetPtr(runtime, rVal.instruction.binary.left);
                BEs64 right = *BEGetPtr(runtime, rVal.instruction.binary.right);
                
                *result = (left == right);
            } break;
            case BE_InstructionType_FEqual:
            {
                BEf64* result = (BEf64*)BEGetPtr(runtime, rVal.instruction.result);
                BEf64 left = *(BEf64*)BEGetPtr(runtime, rVal.instruction.binary.left);
                BEf64 right = *(BEf64*)BEGetPtr(runtime, rVal.instruction.binary.right);
                
                *result = (left == right);
            } break;
            
            case BE_InstructionType_Greater:
            {
                BEs64* result = BEGetPtr(runtime, rVal.instruction.result);
                BEs64 left = *BEGetPtr(runtime, rVal.instruction.binary.left);
                BEs64 right = *BEGetPtr(runtime, rVal.instruction.binary.right);
                
                *result = (left > right);
            } break;
            case BE_InstructionType_FGreater:
            {
                BEf64* result = (BEf64*)BEGetPtr(runtime, rVal.instruction.result);
                BEf64 left = *(BEf64*)BEGetPtr(runtime, rVal.instruction.binary.left);
                BEf64 right = *(BEf64*)BEGetPtr(runtime, rVal.instruction.binary.right);
                
                *result = (left > right);
            } break;
            
            case BE_InstructionType_GreaterOrEqual:
            {
                BEs64* result = BEGetPtr(runtime, rVal.instruction.result);
                BEs64 left = *BEGetPtr(runtime, rVal.instruction.binary.left);
                BEs64 right = *BEGetPtr(runtime, rVal.instruction.binary.right);
                
                *result = (left >= right);
            } break;
            case BE_InstructionType_FGreaterOrEqual:
            {
                BEf64* result = (BEf64*)BEGetPtr(runtime, rVal.instruction.result);
                BEf64 left = *(BEf64*)BEGetPtr(runtime, rVal.instruction.binary.left);
                BEf64 right = *(BEf64*)BEGetPtr(runtime, rVal.instruction.binary.right);
                
                *result = (left >= right);
            } break;
            
            case BE_InstructionType_NotEqual:
            {
                BEs64* result = BEGetPtr(runtime, rVal.instruction.result);
                BEs64 left = *BEGetPtr(runtime, rVal.instruction.binary.left);
                BEs64 right = *BEGetPtr(runtime, rVal.instruction.binary.right);
                
                *result = (left != right);
            } break;
            case BE_InstructionType_FNotEqual:
            {
                BEf64* result = (BEf64*)BEGetPtr(runtime, rVal.instruction.result);
                BEf64 left = *(BEf64*)BEGetPtr(runtime, rVal.instruction.binary.left);
                BEf64 right = *(BEf64*)BEGetPtr(runtime, rVal.instruction.binary.right);
                
                *result = (left != right);
            } break;
            
            case BE_InstructionType_BitshiftLeft:
            {
                BEs64* result = BEGetPtr(runtime, rVal.instruction.result);
                BEs64 left = *BEGetPtr(runtime, rVal.instruction.binary.left);
                BEs64 right = *BEGetPtr(runtime, rVal.instruction.binary.right);
                
                *result = (left << right);
            } break;
            case BE_InstructionType_BitshiftRight:
            {
                BEs64* result = BEGetPtr(runtime, rVal.instruction.result);
                BEs64 left = *BEGetPtr(runtime, rVal.instruction.binary.left);
                BEs64 right = *BEGetPtr(runtime, rVal.instruction.binary.right);
                
                *result = (left >> right);
            } break;
            case BE_InstructionType_BitwiseAnd:
            {
                BEs64* result = BEGetPtr(runtime, rVal.instruction.result);
                BEs64 left = *BEGetPtr(runtime, rVal.instruction.binary.left);
                BEs64 right = *BEGetPtr(runtime, rVal.instruction.binary.right);
                
                *result = (left & right);
            } break;
            case BE_InstructionType_BitwiseOr:
            {
                BEs64* result = BEGetPtr(runtime, rVal.instruction.result);
                BEs64 left = *BEGetPtr(runtime, rVal.instruction.binary.left);
                BEs64 right = *BEGetPtr(runtime, rVal.instruction.binary.right);
                
                *result = (left | right);
            } break;
            case BE_InstructionType_BitwiseXOr:
            {
                BEs64* result = BEGetPtr(runtime, rVal.instruction.result);
                BEs64 left = *BEGetPtr(runtime, rVal.instruction.binary.left);
                BEs64 right = *BEGetPtr(runtime, rVal.instruction.binary.right);
                
                *result = (left ^ right);
            } break;
            case BE_InstructionType_LogicalAnd:
            {
                BEs64* result = BEGetPtr(runtime, rVal.instruction.result);
                BEs64 left = *BEGetPtr(runtime, rVal.instruction.binary.left);
                BEs64 right = *BEGetPtr(runtime, rVal.instruction.binary.right);
                
                *result = (left && right);
            } break;
            case BE_InstructionType_LogicalOr:
            {
                BEs64* result = BEGetPtr(runtime, rVal.instruction.result);
                BEs64 left = *BEGetPtr(runtime, rVal.instruction.binary.left);
                BEs64 right = *BEGetPtr(runtime, rVal.instruction.binary.right);
                
                *result = (left || right);
            } break;
            
            case BE_InstructionType_PushFrame:
            {
                BE_PushFrame(runtime, rVal.instruction.pushFrame.sizeRequiredInBytes);
            } break;
            
            case BE_InstructionType_PopFrame:
            {
                BE_PopFrame(runtime);
            } break;
            
            case BE_InstructionType_HideMembers:
            {
                ++runtime->hideMembersCounter;
            } break;
            
            case BE_InstructionType_ShowMembers:
            {
                BEAssert(runtime->hideMembersCounter);
                --runtime->hideMembersCounter;
            } break;
            
            case BE_InstructionType_Breakpoint:
            {
                rVal.type = BE_RuntimeEventType_Breakpoint;
            } break;
            
            case BE_InstructionType_PushStructInternal:
            case BE_InstructionType_PushStruct:
            {
                BEs64 arrayCount = 0;
                if (rVal.instruction.beginStruct.arrayCount.stack)
                {
                    arrayCount = *BEGetPtr(runtime, rVal.instruction.beginStruct.arrayCount);
                }
                
                if (arrayCount < 0)
                {
                    rVal.type = BE_RuntimeEventType_Error_MemberInvalidArrayCount;
                }
                else
                {
                    BEu32 memberIndex = rVal.instruction.beginStruct.memberIndex;
                    BEbool isInternal = (rVal.instruction.type == BE_InstructionType_PushStructInternal);
                    
                    BE_ObserveStructMember(runtime->allocator, &runtime->currentMembers, memberIndex, (BEu64)arrayCount, isInternal);
                }
                
            } break;
            
            case BE_InstructionType_PopStruct:
            {
                runtime->currentMembers = runtime->currentMembers->parent;
                if (runtime->currentMembers && runtime->currentMembers->type == BE_MemberNodeType_StructArray)
                {
                    // NOTE(Jens): Double-pop for non-root members, once for Struct type and once for Array type.
                    //             However, a BE_MemberNodeStruct is only added to the tree if array count is non-zero.
                    runtime->currentMembers = runtime->currentMembers->parent;
                }
                
                rVal.type = BE_RuntimeEventType_EndStruct;
            } break;
            
            case BE_InstructionType_ExportScalar_u:
            case BE_InstructionType_ExportScalar_s:
            case BE_InstructionType_ExportScalar_f:
            case BE_InstructionType_ExportScalar_string:
            case BE_InstructionType_ExportScalar_raw:
            {
                if (!runtime->hideMembersCounter)
                {
                    rVal.type = BE_RuntimeEventType_ScalarExport;
                }
                
                BEu32 memberIndex = rVal.instruction.scalarExport.memberIndex;
                
                BEs64 address = *BEGetPtr(runtime, BE_MemberOffset());
                BEs64 size = *BEGetPtr(runtime, rVal.instruction.scalarExport.size);
                
                BEs64 arrayCount = 0;
                BEs64 count = 1;
                if (rVal.instruction.scalarExport.arrayCount.stack)
                {
                    count = arrayCount = *BEGetPtr(runtime, rVal.instruction.scalarExport.arrayCount);
                }
                
                if (0 <= address && 0 <= count && 0 <= size && address + count * size <= runtime->sizeOfFile)
                {
                    BE_MemberNodeScalar member;
                    BE_ClearStructToZero(member);
                    
                    BE_ScalarBaseType baseType = (BE_ScalarBaseType)((BEu32)rVal.instruction.type - (BEu32)BE_InstructionType_ExportScalar_u);
                    
                    member.address = address;
                    member.size = size;
                    member.baseType = baseType;
                    member.arrayCount = (BEu64)arrayCount;
                    
                    if (rVal.instruction.scalarExport.isBigEndian)
                    {
                        member.flags |= BEFetchMemoryFlag_BigEndian;
                    }
                    
                    if (rVal.instruction.type == BE_InstructionType_ExportScalar_s)
                    {
                        member.flags |= BEFetchMemoryFlag_Signed;
                    }
                    
                    if (size != 0)
                    {
                        if (rVal.instruction.scalarExport.displayType >= 0)
                        {
                            rVal.scalarExport.displayType = (BE_ScalarDisplayType)rVal.instruction.scalarExport.displayType;
                            rVal.scalarExport.enumIndex = BEInvalidEnumIndex;
                        }
                        else
                        {
                            rVal.scalarExport.displayType = BE_ScalarDisplayType_decimal;
                            rVal.scalarExport.enumIndex = -1 - rVal.instruction.scalarExport.displayType;
                        }
                        
                        rVal.scalarExport.baseType = baseType;
                        rVal.scalarExport.structIndex = BE_GetStructIndex(runtime->currentMembers);
                        rVal.scalarExport.memberIndex = memberIndex;
                        rVal.scalarExport.size = (BEu64)size;
                        rVal.scalarExport.address = (BEu64)address;
                        rVal.scalarExport.isBigEndian = rVal.instruction.scalarExport.isBigEndian;
                        rVal.scalarExport.arrayCount = (BEu64)arrayCount;
                        rVal.scalarExport.appearIndex = BE_ObserveScalarMember(runtime->currentMembers, memberIndex, member);
                    }
                    else
                    {
                        BE_ObserveScalarMember(runtime->currentMembers, memberIndex, member);
                        rVal.type = BE_RuntimeEventType_Generic;
                    }
                }
                else
                {
                    rVal.type = BE_RuntimeEventType_Error_MemberInvalidMemoryRange;
                }
            } break;
            
            case BE_InstructionType_FetchArrayCount:
            {
                BEs64* result = BEGetPtr(runtime, rVal.instruction.result);
                
                BE_MemberNode* memberNode = runtime->currentMembers;
                
                BEAssert(rVal.instruction.fetchMember.pathCount > 0);
                
                for (BEu32 i = 0; i < rVal.instruction.fetchMember.pathCount - 1; ++i)
                {
                    BEu32 memberIndex = rVal.instruction.fetchMember.path[i].memberIndex;
                    
                    BEs64 arrayIndex = 0;
                    if (rVal.instruction.fetchMember.path[i].arrayIndex.stack)
                    {
                        arrayIndex = *BEGetPtr(runtime, rVal.instruction.fetchMember.path[i].arrayIndex);
                    }
                    
                    memberNode = BE_GetNestedMember(memberNode, &rVal.type, memberIndex, arrayIndex);
                }
                
                memberNode = BE_GetMemberNode(memberNode, rVal.instruction.fetchMember.path[rVal.instruction.fetchMember.pathCount - 1].memberIndex);
                
                if (memberNode->type == BE_MemberNodeType_Scalar)
                {
                    *result = memberNode->value_Scalar.arrayCount;
                }
                else
                {
                    BEAssert(memberNode->type == BE_MemberNodeType_Struct);
                    *result = memberNode->parent->value_StructArray.arrayCount;
                }
            } break;
            
            case BE_InstructionType_FetchMemberValueFloat:
            case BE_InstructionType_FetchMemberValue:
            {
                BEs64* result = BEGetPtr(runtime, rVal.instruction.result);
                
                BE_MemberNode* memberNode = runtime->currentMembers;
                
                BEAssert(rVal.instruction.fetchMember.pathCount > 0);
                
                for (BEu32 i = 0; i < rVal.instruction.fetchMember.pathCount - 1; ++i)
                {
                    BEu32 memberIndex = rVal.instruction.fetchMember.path[i].memberIndex;
                    
                    BEs64 arrayIndex = 0;
                    if (rVal.instruction.fetchMember.path[i].arrayIndex.stack)
                    {
                        arrayIndex = *BEGetPtr(runtime, rVal.instruction.fetchMember.path[i].arrayIndex);
                    }
                    
                    memberNode = BE_GetNestedMember(memberNode, &rVal.type, memberIndex, arrayIndex);
                }
                
                BE_MemberNodeScalar* scalarMember = 0;
                
                BEs64 arrayIndex = 0;
                if (rVal.instruction.fetchMember.path[rVal.instruction.fetchMember.pathCount - 1].arrayIndex.stack)
                {
                    arrayIndex = *BEGetPtr(runtime, rVal.instruction.fetchMember.path[rVal.instruction.fetchMember.pathCount - 1].arrayIndex);
                }
                
                if (memberNode)
                {
                    BEu32 memberIndex = rVal.instruction.fetchMember.path[rVal.instruction.fetchMember.pathCount - 1].memberIndex;
                    BE_MemberNode* scalarNode = BE_GetScalarMember(memberNode, &rVal.type, memberIndex, arrayIndex);
                    if (scalarNode)
                    {
                        scalarMember = &scalarNode->value_Scalar;
                    }
                }
                
                if (scalarMember)
                {
                    BEu64 size = scalarMember->size;
                    BEu64 address = scalarMember->address + arrayIndex * size;
                    
                    BEAssert(0 <= size && address + size <= (BEu64)runtime->sizeOfFile);
                    
                    if (0 < size && size <= 8)
                    {
                        BEu8* data = runtime->binaryFile + address;
                        
                        BEs64 value = 0;
                        memcpy(&value, data, size);
                        
                        if (size != 8 && (value & (1ULL << (8ULL * size - 1ULL))))
                        {
                            // NOTE(Jens): 64-bit `value` is larger than data read and value is negative, two's complement must carry to higher bytes.
                            
                            for (BEu32 i = (BEu32)size; i < sizeof(value); ++i)
                            {
                                BEu64 mask = (0xffULL << ((BEu64)i * 8ULL));
                                value |= mask;
                            }
                        }
                        
                        if (scalarMember->flags & BEFetchMemoryFlag_BigEndian)
                        {
                            // NOTE(Jens): Assumes host is little endian.
                            BE_SwapByteOrder(size, (BEu8*)&value);
                        }
                        
                        if (scalarMember->baseType == BE_ScalarBaseType_f)
                        {
                            BEf64 value_dbl = 0.0f;
                            if (size == 8)
                            {
                                memcpy(&value_dbl, &value, 8);
                            }
                            else if (size == 4)
                            {
                                BEf32 value_flt;
                                memcpy(&value_flt, &value, 4);
                                
                                value_dbl = value_flt;
                            }
                            else
                            {
                                rVal.type = BE_RuntimeEventType_Error_FetchMemberBadFloatSize;
                            }
                            
                            if (rVal.instruction.type == BE_InstructionType_FetchMemberValue)
                            {
                                value = (BEs64)value_dbl;
                            }
                            else
                            {
                                memcpy(&value, &value_dbl, 8);
                            }
                        }
                        else
                        {
                            if (size != 8 && (scalarMember->flags & BEFetchMemoryFlag_Signed) && (value & (1ULL << (8ULL * size - 1ULL))))
                            {
                                // NOTE(Jens): 64-bit `value` is larger than data read and value is negative, two's complement must carry to higher bytes.
                                
                                for (BEu32 i = (BEu32)size; i < sizeof(value); ++i)
                                {
                                    BEu64 mask = (0xffULL << ((BEu64)i * 8ULL));
                                    value |= mask;
                                }
                            }
                            
                            if (rVal.instruction.type == BE_InstructionType_FetchMemberValueFloat)
                            {
                                BEf64 valueFloat = (BEf64)value;
                                memcpy(&value, &valueFloat, 8);
                            }
                        }
                        
                        // NOTE(Jens): Although result type is BEs64, it will store a BEf64 if instruction type is BE_InstructionType_FetchMemberValueFloat.
                        *result = value;
                    }
                    else
                    {
                        if (size > 8)
                        {
                            rVal.type = BE_RuntimeEventType_Error_FetchMemberTooBig;
                        }
                        else
                        {
                            rVal.type = BE_RuntimeEventType_Error_FetchMemberSizeIsZero;
                        }
                    }
                }
            } break;
            
            case BE_InstructionType_FetchMemory:
            {
                BEs64* result = BEGetPtr(runtime, rVal.instruction.result);
                
                BEFetchMemoryFlags flags = rVal.instruction.fetchMemory.flags;
                
                BEs64 address = *BEGetPtr(runtime, BE_MemberOffset());
                BEs64 size = *BEGetPtr(runtime, rVal.instruction.fetchMemory.size);
                
                if (0 <= address && 0 < size && size <= 8 && address + size <= runtime->sizeOfFile)
                {
                    BEu8* data = runtime->binaryFile + address;
                    
                    BEs64 value = 0;
                    memcpy(&value, data, size);
                    
                    if (flags & BEFetchMemoryFlag_BigEndian)
                    {
                        // NOTE(Jens): Assumes host is little endian.
                        BE_SwapByteOrder(size, (BEu8*)&value);
                    }
                    
                    if (size != 8 && (flags & BEFetchMemoryFlag_Signed) && (value & (1ULL << (8ULL * size - 1ULL))))
                    {
                        // NOTE(Jens): 64-bit `value` is larger than data read and value is negative, two's complement must carry to higher bytes.
                        
                        for (BEu32 i = (BEu32)size; i < sizeof(value); ++i)
                        {
                            BEu64 mask = (0xffULL << ((BEu64)i * 8ULL));
                            value |= mask;
                        }
                    }
                    
                    *result = value;
                }
                else
                {
                    if (size > 8)
                    {
                        rVal.type = BE_RuntimeEventType_Error_FetchMemoryTooBig;
                    }
                    else
                    {
                        rVal.type = BE_RuntimeEventType_Error_FetchMemoryInvalidMemoryRange;
                    }
                }
            } break;
            
            case BE_InstructionType_Jump:
            {
                runtime->currentAddress = rVal.instruction.jump.newInstructionIndex;
                BEAssert(runtime->currentAddress <= runtime->instructionsSizeInBytes);
            } break;
            
            case BE_InstructionType_JumpDynamic:
            {
                BEs64 newAddress = *BEGetPtr(runtime, rVal.instruction.jump.dynamicNewInstructionIndex);
                BEAssert(0 <= newAddress && newAddress <= runtime->instructionsSizeInBytes);
                
                runtime->currentAddress = (BEu32)newAddress;
            } break;
            
            case BE_InstructionType_JumpConditional:
            {
                if (*BEGetPtr(runtime, rVal.instruction.jump.condition))
                {
                    runtime->currentAddress = rVal.instruction.jump.newInstructionIndex;
                    BEAssert(runtime->currentAddress <= runtime->instructionsSizeInBytes);
                }
            } break;
            
            case BE_InstructionType_JumpDynamicConditional:
            {
                if (*BEGetPtr(runtime, rVal.instruction.jump.condition))
                {
                    BEs64 newAddress = *BEGetPtr(runtime, rVal.instruction.jump.dynamicNewInstructionIndex);
                    BEAssert(0 <= newAddress && newAddress <= runtime->instructionsSizeInBytes);
                    
                    runtime->currentAddress = (BEu32)newAddress;
                }
            } break;
            
            case BE_InstructionType_FetchSizeOfFile:
            {
                BEs64* result = BEGetPtr(runtime, rVal.instruction.result);
                *result = runtime->sizeOfFile;
            } break;
            
            case BE_InstructionType_FetchStartOfType:
            {
                BEs64* result = BEGetPtr(runtime, rVal.instruction.result);
                
                BEs64 typeNumber = *BEGetPtr(runtime, rVal.instruction.categoryQuery.typeNumber);
                BEu32 startAddressPlusOne = BE_FindStartAddress(runtime->categoriesSection, rVal.instruction.categoryQuery.categoryIndex, typeNumber);
                
                if (startAddressPlusOne)
                {
                    *result = startAddressPlusOne - 1;
                }
                else
                {
                    rVal.type = BE_RuntimeEventType_Error_CategoryTypeNotFound;
                }
            } break;
            
            case BE_InstructionType_HasType:
            {
                BEs64* result = BEGetPtr(runtime, rVal.instruction.result);
                
                BEs64 typeNumber = *BEGetPtr(runtime, rVal.instruction.categoryQuery.typeNumber);
                BEu32 startAddressPlusOne = BE_FindStartAddress(runtime->categoriesSection, rVal.instruction.categoryQuery.categoryIndex, typeNumber);
                
                if (startAddressPlusOne)
                {
                    *result = 1;
                }
                else
                {
                    *result = 0;
                }
            } break;
            
            case BE_InstructionType_CreatePlot:
            {
                BEs64* result = BEGetPtr(runtime, rVal.instruction.result);
                *result = ++runtime->createdPlotCount;
            } break;
            
            case BE_InstructionType_Assert:
            {
                BEs64 success = *BEGetPtr(runtime, rVal.instruction.assert.condition);
                
                if (!success)
                {
                    rVal.type = BE_RuntimeEventType_Error_AssertTriggered;
                }
            } break;
            
            case BE_InstructionType_Print:
            {
                rVal.type = BE_RuntimeEventType_Print;
                rVal.print.msg = rVal.instruction.print.prefixCharacters;
                rVal.print.msgSize = (BEu32)rVal.instruction.print.prefixCharacterCount;
                
                if (rVal.instruction.print.value.stack)
                {
                    rVal.print.hasValue = 1;
                    rVal.print.value = *BEGetPtr(runtime, rVal.instruction.print.value);
                }
            } break;
            
            case BE_InstructionType_Plot_F8_X:
            case BE_InstructionType_Plot_F8_Y:
            {
                if (rVal.instruction.type == BE_InstructionType_Plot_F8_X)
                {
                    rVal.type = BE_RuntimeEventType_PlotX;
                }
                else
                {
                    rVal.type = BE_RuntimeEventType_PlotY;
                }
                
                rVal.plotValue.plotId = *BEGetPtr(runtime, rVal.instruction.plot.plotId);
                rVal.plotValue.value = *(BEf64*)BEGetPtr(runtime, rVal.instruction.plot.value_f64);
            } break;
            
            case BE_InstructionType_IntToFloat:
            {
                BEs64* result = BEGetPtr(runtime, rVal.instruction.result);
                BEf64 v = (BEf64)*BEGetPtr(runtime, rVal.instruction.conversion.source);
                
                memcpy(result, &v, sizeof(v));
            } break;
        }
        
        if (rVal.type && runtime->sourcesSection)
        {
            BESourceLocation location = BE_FindSourceAtAddress(runtime->sourcesSection, instructionAddress);
            
            rVal.sourceFileIndex = location.fileIndex;
            rVal.sourceLineIndex = location.lineIndex;
            rVal.sourceColIndex = location.bytesSinceStartOfLine;
            rVal.sourceByteIndex = location.byteIndex;
        }
    }
    else
    {
        rVal.type = BE_RuntimeEventType_End;
    }
    
    return rVal;
}

BEPublicAPI BEbool BERuntimeHasCompleted(BERuntime* runtime)
{
    return runtime->currentAddress >= runtime->instructionsSizeInBytes;
}


BEPublicAPI char* BEGetStructName(BERuntime* runtime, BEu32* size, BEu32 structIndex)
{
    BE_String structName = BE_GetStructName(runtime->typesSection, structIndex);
    if (size)
    {
        *size = (BEu32)structName.count;
    }
    return structName.characters;
}

BEPublicAPI char* BEGetStructMemberName(BERuntime* runtime, BEu32* size, BEu32 structIndex, BEu32 memberIndex)
{
    BE_String memberName = BE_GetStructMemberName(runtime->typesSection, structIndex, memberIndex);
    if (size)
    {
        *size = (BEu32)memberName.count;
    }
    return memberName.characters;
}

BEPublicAPI char* BEGetEnumName(BERuntime* runtime, BEu32* size, BEu32 enumIndex)
{
    BE_String enumName = BE_GetEnumName(runtime->enumsSection, enumIndex);
    if (size)
    {
        *size = (BEu32)enumName.count;
    }
    return enumName.characters;
}

BEPublicAPI char* BEGetEnumMemberName(BERuntime* runtime, BEu32* size, BEu32 enumIndex, BEu32 memberIndex)
{
    BE_String memberName = BE_GetEnumMemberName(runtime->enumsSection, enumIndex, memberIndex);
    if (size)
    {
        *size = (BEu32)memberName.count;
    }
    return memberName.characters;
}

BEPublicAPI BEu32 BEGetEnumMemberCount(BERuntime* runtime, BEu32 enumIndex)
{
    return BE_GetEnumMemberCount(runtime->enumsSection, enumIndex);
}

BEPublicAPI BEs64* BEGetEnumMemberValues(BERuntime* runtime, BEu32 enumIndex)
{
    return BE_GetEnumValues(runtime->enumsSection, enumIndex);
}

BEPublicAPI BESourceLocation BEGetSourceLocationForInstructionAddress(BERuntime* runtime, BEu32 address)
{
    BESourceLocation rVal;
    BE_ClearStructToZero(rVal);
    
    if (runtime->sourcesSection)
    {
        rVal = BE_FindSourceAtAddress(runtime->sourcesSection, address);
    }
    
    return rVal;
}

BEPublicAPI char* BEGetSourceFilenameForFileIndex(BERuntime* runtime, BEu32* size, BEu32 fileIndex)
{
    char* rVal = 0;
    
    if (runtime->sourcesSection)
    {
        BEu32 size_;
        rVal = BE_FindSourceFilename(runtime->sourcesSection, &size_, fileIndex);
        
        if (size)
        {
            *size = size_;
        }
    }
    
    return rVal;
}

BEPublicAPI BEs64* BEGetPtr(BERuntime* runtime, BE_StackEntry entry)
{
    BEs64* rVal = 0;
    
    switch (entry.stack)
    {
        BE_InvalidDefault;
        
        case BE_Stack_Local:
        {
            rVal = BE_GetLocalPtr(runtime, entry.index);
        } break;
        
        case BE_Stack_Parent:
        {
            rVal = BE_GetParentPtr(runtime, entry.index);
        } break;
        
        case BE_Stack_Global:
        {
            rVal = BE_GetGlobalPtr(runtime, entry.index);
        } break;
    }
    
    return rVal;
}
