// BEdit_CommandLine.c - Public domain - no warranty //

// NOTE(Jens): To prevent silly warnings from MSVC.
#define _CRT_SECURE_NO_WARNINGS


#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include <inttypes.h>
#include <assert.h>
#include <math.h>

// NOTE(Jens): Super simple unoptimized allocator.

typedef struct CommandLineAllocation CommandLineAllocation;
struct CommandLineAllocation
{
    CommandLineAllocation* prev;
    void* data;
};

typedef struct CommandLineAllocator CommandLineAllocator;
struct CommandLineAllocator
{
    CommandLineAllocation* rootAllocation;
};

static void* Alloc(CommandLineAllocator* allocator, size_t size)
{
    assert(size);
    
    CommandLineAllocation* allocation = malloc(sizeof(CommandLineAllocation));
    allocation->prev = allocator->rootAllocation;
    allocator->rootAllocation = allocation;
    
    allocation->data = malloc(size);
    
    return allocation->data;
}

static void DeallocAll(CommandLineAllocator* allocator)
{
    while (allocator->rootAllocation)
    {
        CommandLineAllocation* allocation = allocator->rootAllocation;
        allocator->rootAllocation = allocator->rootAllocation->prev;
        
        free(allocation->data);
        free(allocation);
    }
}

#define BEAlloc(allocator, type) Alloc(allocator, sizeof(type))
#define BEAllocArray(allocator, type, count) Alloc(allocator, sizeof(type) * (count))
#define BEAllocatorType CommandLineAllocator*



#include "BEdit.h"

static BEbool UseColoredOutput = 0;

#ifdef _WIN32

// NOTE(Jens): To prevent including Windows.h
// {

#define WINAPI __stdcall
typedef void* HANDLE;
typedef unsigned long DWORD;
typedef int BOOL;

#define STD_OUTPUT_HANDLE ((DWORD)-11)
#define ENABLE_VIRTUAL_TERMINAL_PROCESSING 0x0004
#define DISABLE_NEWLINE_AUTO_RETURN        0x0008

// NOTE(Jens): Part of Kernel32.dll
extern HANDLE WINAPI GetStdHandle(DWORD nStdHandle);
extern BOOL WINAPI GetConsoleMode(HANDLE hConsoleHandle, DWORD* lpMode);
extern BOOL WINAPI SetConsoleMode(HANDLE hConsoleHandle, DWORD dwMode);

// }

static DWORD WIN32OriginalConsoleMode;

static void EnableTerminalColoredOutput(void)
{
    BEAssert(!UseColoredOutput);
    
    HANDLE stdoutHandle = GetStdHandle(STD_OUTPUT_HANDLE);
    if (GetConsoleMode(stdoutHandle, &WIN32OriginalConsoleMode) && SetConsoleMode(stdoutHandle, WIN32OriginalConsoleMode | ENABLE_VIRTUAL_TERMINAL_PROCESSING | DISABLE_NEWLINE_AUTO_RETURN))
    {
        UseColoredOutput = 1;
    }
}

static void ResetTerminalSettings(void)
{
    if (UseColoredOutput)
    {
        HANDLE stdoutHandle = GetStdHandle(STD_OUTPUT_HANDLE);
        SetConsoleMode(stdoutHandle, WIN32OriginalConsoleMode);
        
        UseColoredOutput = 0;
    }
}

#else

static void EnableTerminalColoredOutput(void)
{
    UseColoredOutput = 1;
}

static void ResetTerminalSettings(void)
{
    UseColoredOutput = 0;
}

#endif

typedef enum Color
{
    // NOTE(Jens): At least Linux supports full RGB coloring, this can be extended.
    
    Color_Default,
    
    Color_Black,
    Color_Red,
    Color_Green,
    Color_Yellow,
    Color_Blue,
    Color_Magenta,
    Color_Cyan,
    Color_White,
    
    Color_Bright = (1 << 31),
    
    Color_LayoutAddress = Color_Black | Color_Bright,
    Color_LayoutPrint = Color_Green | Color_Bright,
    
    Color_InstructionStackValue = Color_Black | Color_Bright,
    Color_GlobalStackEntry      = Color_Yellow | Color_Bright,
    Color_OtherStackEntry       = Color_Cyan | Color_Bright,
    Color_GotoPeek              = Color_Black | Color_Bright,
    
    Color_BDBSourceLineMarker         = Color_Black | Color_Bright,
    Color_BDBSourceCode               = Color_Green,
    Color_BDBSourceCodeInactive       = Color_Black | Color_Bright,
    Color_BDBCurrentInstructionNumber = Color_White | Color_Bright,
    Color_BDBOtherInstructionNumber   = Color_Black | Color_Bright,
    Color_BDBCurrentInstructionArrow  = Color_Blue | Color_Bright,
    Color_BDBHelpTitle                = Color_Yellow,
    Color_BDBHelpInstruction          = Color_Cyan,
    Color_BDBScalarExport             = Color_Cyan,
} Color;

static void SetTerminalTextColor(FILE* out, Color color)
{
    if (UseColoredOutput)
    {
        BEu32 isBright = (color & Color_Bright);
        
        color &= ~Color_Bright;
        
        unsigned int code = 0;
        if (color)
        {
            code = 30 + color - 1;
        }
        
        if (isBright)
        {
            fprintf(out, "\033[%u;1m", code);
        }
        else
        {
            fprintf(out, "\033[%um", code);
        }
    }
}

static void PrintStackEntryFloat(FILE* out, BERuntime* runtime, BE_StackEntry entry)
{
    switch (entry.stack)
    {
        case BE_Stack_Invalid:
        {
            fprintf(out, "(invalid)");
        } break;
        
        case BE_Stack_Global:
        {
            SetTerminalTextColor(out, Color_GlobalStackEntry);
            
            if (entry.index >= BE_GlobalSymbol_Count)
            {
                fprintf(out, "g(%u)", entry.index - BE_GlobalSymbol_Count);
            }
            else
            {
                if (entry.index == BE_GlobalSymbol_MemberOffset)
                {
                    fprintf(out, "@");
                }
                else
                {
                    fprintf(out, "?");
                }
            }
            
            SetTerminalTextColor(out, Color_Default);
        } break;
        
        case BE_Stack_Local:
        {
            SetTerminalTextColor(out, Color_OtherStackEntry);
            
            if (entry.index >= BE_LocalSymbol_Count)
            {
                fprintf(out, "l(%u)", entry.index - BE_GlobalSymbol_Count);
            }
            else
            {
                if (entry.index == BE_LocalSymbol_ReturnAddress)
                {
                    fprintf(out, "r");
                }
                else if (entry.index == BE_LocalSymbol_ArrayIndex)
                {
                    fprintf(out, "i");
                }
                else 
                {
                    fprintf(out, "?");
                }
            }
            
            SetTerminalTextColor(out, Color_Default);
        } break;
        
        case BE_Stack_Parent:
        {
            SetTerminalTextColor(out, Color_OtherStackEntry);
            
            if (entry.index >= BE_LocalSymbol_Count)
            {
                fprintf(out, "p(%u)", entry.index - BE_LocalSymbol_Count);
            }
            else
            {
                fprintf(out, "?");
            }
            
            SetTerminalTextColor(out, Color_Default);
        } break;
    }
    
    if (runtime && entry.stack)
    {
        SetTerminalTextColor(out, Color_InstructionStackValue);
        fprintf(out, "=%f", *(BEf64*)BEGetPtr(runtime, entry));
        SetTerminalTextColor(out, Color_Default);
    }
}

static void PrintStackEntryHex(FILE* out, BERuntime* runtime, BE_StackEntry entry)
{
    switch (entry.stack)
    {
        case BE_Stack_Invalid:
        {
            fprintf(out, "(invalid)");
        } break;
        
        case BE_Stack_Global:
        {
            SetTerminalTextColor(out, Color_GlobalStackEntry);
            
            if (entry.index >= BE_GlobalSymbol_Count)
            {
                fprintf(out, "g(%u)", entry.index - BE_GlobalSymbol_Count);
            }
            else
            {
                if (entry.index == BE_GlobalSymbol_MemberOffset)
                {
                    fprintf(out, "@");
                }
                else
                {
                    fprintf(out, "?");
                }
            }
            
            SetTerminalTextColor(out, Color_Default);
        } break;
        
        case BE_Stack_Local:
        {
            SetTerminalTextColor(out, Color_OtherStackEntry);
            
            if (entry.index >= BE_LocalSymbol_Count)
            {
                fprintf(out, "l(%u)", entry.index - BE_GlobalSymbol_Count);
            }
            else
            {
                if (entry.index == BE_LocalSymbol_ReturnAddress)
                {
                    fprintf(out, "r");
                }
                else if (entry.index == BE_LocalSymbol_ArrayIndex)
                {
                    fprintf(out, "i");
                }
                else 
                {
                    fprintf(out, "?");
                }
            }
            
            SetTerminalTextColor(out, Color_Default);
        } break;
        
        case BE_Stack_Parent:
        {
            SetTerminalTextColor(out, Color_OtherStackEntry);
            
            if (entry.index >= BE_LocalSymbol_Count)
            {
                fprintf(out, "p(%u)", entry.index - BE_LocalSymbol_Count);
            }
            else
            {
                fprintf(out, "?");
            }
            
            SetTerminalTextColor(out, Color_Default);
        } break;
    }
    
    if (runtime && entry.stack)
    {
        SetTerminalTextColor(out, Color_InstructionStackValue);
        fprintf(out, "=%llXh", *BEGetPtr(runtime, entry));
        SetTerminalTextColor(out, Color_Default);
    }
}

static void PrintStackEntry(FILE* out, BERuntime* runtime, BE_StackEntry entry)
{
    if (entry.stack == BE_Stack_Local && entry.index == BE_LocalSymbol_ReturnAddress)
    {
        PrintStackEntryHex(out, runtime, entry);
    }
    else
    {
        switch (entry.stack)
        {
            case BE_Stack_Invalid:
            {
                fprintf(out, "(invalid)");
            } break;
            
            case BE_Stack_Global:
            {
                SetTerminalTextColor(out, Color_GlobalStackEntry);
                
                if (entry.index >= BE_GlobalSymbol_Count)
                {
                    fprintf(out, "g(%u)", entry.index - BE_GlobalSymbol_Count);
                }
                else
                {
                    if (entry.index == BE_GlobalSymbol_MemberOffset)
                    {
                        fprintf(out, "@");
                    }
                    else
                    {
                        fprintf(out, "?");
                    }
                }
                
                SetTerminalTextColor(out, Color_Default);
            } break;
            
            case BE_Stack_Local:
            {
                SetTerminalTextColor(out, Color_OtherStackEntry);
                
                if (entry.index >= BE_LocalSymbol_Count)
                {
                    fprintf(out, "l(%u)", entry.index - BE_GlobalSymbol_Count);
                }
                else
                {
                    if (entry.index == BE_LocalSymbol_ReturnAddress)
                    {
                        fprintf(out, "r");
                    }
                    else if (entry.index == BE_LocalSymbol_ArrayIndex)
                    {
                        fprintf(out, "i");
                    }
                    else 
                    {
                        fprintf(out, "?");
                    }
                }
                
                SetTerminalTextColor(out, Color_Default);
            } break;
            
            case BE_Stack_Parent:
            {
                SetTerminalTextColor(out, Color_OtherStackEntry);
                
                if (entry.index >= BE_LocalSymbol_Count)
                {
                    fprintf(out, "p(%u)", entry.index - BE_LocalSymbol_Count);
                }
                else
                {
                    fprintf(out, "?");
                }
                
                SetTerminalTextColor(out, Color_Default);
            } break;
        }
        
        if (runtime && entry.stack)
        {
            SetTerminalTextColor(out, Color_InstructionStackValue);
            fprintf(out, "=%lli", *BEGetPtr(runtime, entry));
            SetTerminalTextColor(out, Color_Default);
        }
    }
}

static void PrintDisplayType(FILE* out, BE_ScalarDisplayType type)
{
    switch (type)
    {
        case BE_ScalarDisplayType_decimal:
        {
            fputs("decimal", out);
        } break;
        
        case BE_ScalarDisplayType_binary:
        {
            fputs("binary", out);
        } break;
        
        case BE_ScalarDisplayType_octal:
        {
            fputs("octal", out);
        } break;
        
        case BE_ScalarDisplayType_hex:
        {
            fputs("hex", out);
        } break;
        
        default:
        {
            if (type < 0)
            {
                fprintf(out, "enum-%u", -1 - type);
            }
        } break;
    }
}

static void PrintInstruction(FILE* out, BEInstruction* instruction, BEu32 indentation, BERuntime* runtime)
{
	for (BEu32 i = 0; i < indentation; ++i)
	{
		fputs("    ", out);
	}
    
    switch (instruction->type)
    {
        default:
        {
            fprintf(out, "\?\?\?");
        } break;
        
        case BE_InstructionType_Set:
        {
            PrintStackEntry(out, 0, instruction->result);
            
            if (instruction->result.stack == BE_Stack_Local && instruction->result.index == BE_LocalSymbol_ReturnAddress)
            {
                fprintf(out, " <- %llXh", instruction->set.value);
            }
            else
            {
                fprintf(out, " <- %lli", instruction->set.value);
            }
        } break;
        
        case BE_InstructionType_Negate:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- -");
            PrintStackEntry(out, runtime, instruction->unaryValue);
        } break;
        
        case BE_InstructionType_FNegate:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- f-");
            PrintStackEntryFloat(out, runtime, instruction->unaryValue);
        } break;
        
        case BE_InstructionType_LogicalNot:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- !");
            PrintStackEntry(out, runtime, instruction->unaryValue);
        } break;
        
        case BE_InstructionType_BitwiseNot:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- ~");
            PrintStackEntry(out, runtime, instruction->unaryValue);
        } break;
        
        case BE_InstructionType_Abs:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- abs(");
            PrintStackEntry(out, runtime, instruction->unaryValue);
            fprintf(out, ")");
        } break;
        
        case BE_InstructionType_FAbs:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- fabs(");
            PrintStackEntryFloat(out, runtime, instruction->unaryValue);
            fprintf(out, ")");
        } break;
        
        case BE_InstructionType_Copy:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- ");
            PrintStackEntry(out, runtime, instruction->unaryValue);
        } break;
        
        case BE_InstructionType_Add:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- ");
            PrintStackEntry(out, runtime, instruction->binary.left);
            fprintf(out, " + ");
            PrintStackEntry(out, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_FAdd:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- ");
            PrintStackEntryFloat(out, runtime, instruction->binary.left);
            fprintf(out, " f+ ");
            PrintStackEntryFloat(out, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_Sub:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- ");
            PrintStackEntry(out, runtime, instruction->binary.left);
            fprintf(out, " - ");
            PrintStackEntry(out, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_FSub:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- ");
            PrintStackEntryFloat(out, runtime, instruction->binary.left);
            fprintf(out, " f- ");
            PrintStackEntryFloat(out, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_Mul:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- ");
            PrintStackEntry(out, runtime, instruction->binary.left);
            fprintf(out, " * ");
            PrintStackEntry(out, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_FMul:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- ");
            PrintStackEntryFloat(out, runtime, instruction->binary.left);
            fprintf(out, " f* ");
            PrintStackEntryFloat(out, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_Div:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- ");
            PrintStackEntry(out, runtime, instruction->binary.left);
            fprintf(out, " / ");
            PrintStackEntry(out, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_FDiv:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- ");
            PrintStackEntryFloat(out, runtime, instruction->binary.left);
            fprintf(out, " f/ ");
            PrintStackEntryFloat(out, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_Mod:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- ");
            PrintStackEntry(out, runtime, instruction->binary.left);
            fprintf(out, " %% ");
            PrintStackEntry(out, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_Less:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- ");
            PrintStackEntry(out, runtime, instruction->binary.left);
            fprintf(out, " < ");
            PrintStackEntry(out, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_FLess:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- ");
            PrintStackEntryFloat(out, runtime, instruction->binary.left);
            fprintf(out, " f< ");
            PrintStackEntryFloat(out, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_LessOrEqual:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- ");
            PrintStackEntry(out, runtime, instruction->binary.left);
            fprintf(out, " <= ");
            PrintStackEntry(out, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_FLessOrEqual:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- ");
            PrintStackEntryFloat(out, runtime, instruction->binary.left);
            fprintf(out, " f<= ");
            PrintStackEntryFloat(out, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_Equal:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- ");
            PrintStackEntry(out, runtime, instruction->binary.left);
            fprintf(out, " == ");
            PrintStackEntry(out, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_FEqual:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- ");
            PrintStackEntryFloat(out, runtime, instruction->binary.left);
            fprintf(out, " f== ");
            PrintStackEntryFloat(out, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_Greater:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- ");
            PrintStackEntry(out, runtime, instruction->binary.left);
            fprintf(out, " > ");
            PrintStackEntry(out, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_FGreater:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- ");
            PrintStackEntryFloat(out, runtime, instruction->binary.left);
            fprintf(out, " f> ");
            PrintStackEntryFloat(out, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_GreaterOrEqual:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- ");
            PrintStackEntry(out, runtime, instruction->binary.left);
            fprintf(out, " >= ");
            PrintStackEntry(out, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_FGreaterOrEqual:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- ");
            PrintStackEntryFloat(out, runtime, instruction->binary.left);
            fprintf(out, " f>= ");
            PrintStackEntryFloat(out, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_NotEqual:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- ");
            PrintStackEntry(out, runtime, instruction->binary.left);
            fprintf(out, " != ");
            PrintStackEntry(out, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_FNotEqual:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- ");
            PrintStackEntryFloat(out, runtime, instruction->binary.left);
            fprintf(out, " f!= ");
            PrintStackEntryFloat(out, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_BitshiftLeft:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- ");
            PrintStackEntry(out, runtime, instruction->binary.left);
            fprintf(out, " << ");
            PrintStackEntry(out, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_BitshiftRight:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- ");
            PrintStackEntry(out, runtime, instruction->binary.left);
            fprintf(out, " >> ");
            PrintStackEntry(out, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_BitwiseAnd:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- ");
            PrintStackEntry(out, runtime, instruction->binary.left);
            fprintf(out, " & ");
            PrintStackEntry(out, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_BitwiseOr:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- ");
            PrintStackEntry(out, runtime, instruction->binary.left);
            fprintf(out, " | ");
            PrintStackEntry(out, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_BitwiseXOr:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- ");
            PrintStackEntry(out, runtime, instruction->binary.left);
            fprintf(out, " ^ ");
            PrintStackEntry(out, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_LogicalAnd:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- ");
            PrintStackEntry(out, runtime, instruction->binary.left);
            fprintf(out, " && ");
            PrintStackEntry(out, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_LogicalOr:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- ");
            PrintStackEntry(out, runtime, instruction->binary.left);
            fprintf(out, " || ");
            PrintStackEntry(out, runtime, instruction->binary.right);
        } break;
        
        case BE_InstructionType_CreatePlot:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- create-plot");
        } break;
        
        case BE_InstructionType_Plot_F8_X:
        {
            fprintf(out, "plot-x ");
            PrintStackEntry(out, runtime, instruction->plot.plotId);
            fprintf(out, ", ");
            PrintStackEntryFloat(out, runtime, instruction->plot.value_f64);
        } break;
        
        case BE_InstructionType_Plot_F8_Y:
        {
            fprintf(out, "plot-y ");
            PrintStackEntry(out, runtime, instruction->plot.plotId);
            fprintf(out, ", ");
            PrintStackEntryFloat(out, runtime, instruction->plot.value_f64);
        } break;
        
        case BE_InstructionType_IntToFloat:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- int-to-float ");
            PrintStackEntry(out, runtime, instruction->conversion.source);
        } break;
        
        case BE_InstructionType_PushFrame:
        {
            fprintf(out, "push-frame (%u bytes)", instruction->pushFrame.sizeRequiredInBytes);
        } break;
        
        case BE_InstructionType_PopFrame:
        {
            fprintf(out, "pop-frame");
        } break;
        
        case BE_InstructionType_PushStruct:
        {
            fprintf(out, "push-struct %u", instruction->beginStruct.memberIndex);
            if (instruction->beginStruct.arrayCount.stack)
            {
                fprintf(out, " [");
                PrintStackEntry(out, runtime, instruction->beginStruct.arrayCount);
                fprintf(out, "]");
            }
        } break;
        
        case BE_InstructionType_PushStructInternal:
        {
            fprintf(out, "push-struct-internal %u", instruction->beginStruct.memberIndex);
            if (instruction->beginStruct.arrayCount.stack)
            {
                fprintf(out, " [");
                PrintStackEntry(out, runtime, instruction->beginStruct.arrayCount);
                fprintf(out, "]");
            }
        } break;
        
        case BE_InstructionType_PopStruct:
        {
            fprintf(out, "pop-struct");
        } break;
        
        case BE_InstructionType_ExportScalar_u:
        {
            fprintf(out, "scalar-u %u (%s ", instruction->scalarExport.memberIndex, instruction->scalarExport.isBigEndian ? "big" : "little");
            PrintDisplayType(out, instruction->scalarExport.displayType);
            fprintf(out, ") ");
            PrintStackEntry(out, runtime, instruction->scalarExport.size);
            if (instruction->scalarExport.arrayCount.stack)
            {
                fprintf(out, " [");
                PrintStackEntry(out, runtime, instruction->scalarExport.arrayCount);
                fprintf(out, "]");
            }
        } break;
        
        case BE_InstructionType_ExportScalar_s:
        {
            fprintf(out, "scalar-s %u (%s ", instruction->scalarExport.memberIndex, instruction->scalarExport.isBigEndian ? "big" : "little");
            PrintDisplayType(out, instruction->scalarExport.displayType);
            fprintf(out, ") ");
            PrintStackEntry(out, runtime, instruction->scalarExport.size);
            if (instruction->scalarExport.arrayCount.stack)
            {
                fprintf(out, " [");
                PrintStackEntry(out, runtime, instruction->scalarExport.arrayCount);
                fprintf(out, "]");
            }
        } break;
        
        case BE_InstructionType_ExportScalar_f:
        {
            fprintf(out, "scalar-f %u (%s ", instruction->scalarExport.memberIndex, instruction->scalarExport.isBigEndian ? "big" : "little");
            PrintDisplayType(out, instruction->scalarExport.displayType);
            fprintf(out, ") ");
            PrintStackEntry(out, runtime, instruction->scalarExport.size);
            if (instruction->scalarExport.arrayCount.stack)
            {
                fprintf(out, " [");
                PrintStackEntry(out, runtime, instruction->scalarExport.arrayCount);
                fprintf(out, "]");
            }
        } break;
        
        case BE_InstructionType_ExportScalar_string:
        {
            fprintf(out, "scalar-string %u ", instruction->scalarExport.memberIndex);
            PrintStackEntry(out, runtime, instruction->scalarExport.size);
            if (instruction->scalarExport.arrayCount.stack)
            {
                fprintf(out, " [");
                PrintStackEntry(out, runtime, instruction->scalarExport.arrayCount);
                fprintf(out, "]");
            }
        } break;
        
        case BE_InstructionType_ExportScalar_raw:
        {
            fprintf(out, "scalar-raw %u (", instruction->scalarExport.memberIndex);
            PrintDisplayType(out, instruction->scalarExport.displayType);
            fprintf(out, ") ");
            PrintStackEntry(out, runtime, instruction->scalarExport.size);
            if (instruction->scalarExport.arrayCount.stack)
            {
                fprintf(out, " [");
                PrintStackEntry(out, runtime, instruction->scalarExport.arrayCount);
                fprintf(out, "]");
            }
        } break;
        
        case BE_InstructionType_FetchMemberValue:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- fetch-member (");
            
            for (BEu32 i = 0; i < instruction->fetchMember.pathCount; ++i)
            {
                fprintf(out, "%u", instruction->fetchMember.path[i].memberIndex);
                
                if (instruction->fetchMember.path[i].arrayIndex.stack)
                {
                    fprintf(out, " [");
                    PrintStackEntry(out, runtime, instruction->fetchMember.path[i].arrayIndex);
                    fprintf(out, "]");
                }
                
                if (i == instruction->fetchMember.pathCount - 1)
                {
                    fprintf(out, ")");
                }
                else
                {
                    fprintf(out, ", ");
                }
            }
        } break;
        
        case BE_InstructionType_FetchMemberValueFloat:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- fetch-member-f (");
            
            for (BEu32 i = 0; i < instruction->fetchMember.pathCount; ++i)
            {
                fprintf(out, "%u", instruction->fetchMember.path[i].memberIndex);
                
                if (instruction->fetchMember.path[i].arrayIndex.stack)
                {
                    fprintf(out, " [");
                    PrintStackEntry(out, runtime, instruction->fetchMember.path[i].arrayIndex);
                    fprintf(out, "]");
                }
                
                if (i == instruction->fetchMember.pathCount - 1)
                {
                    fprintf(out, ")");
                }
                else
                {
                    fprintf(out, ", ");
                }
            }
        } break;
        
        case BE_InstructionType_FetchMemory:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- load ");
            PrintStackEntry(out, runtime, instruction->fetchMemory.size);
        } break;
        
        case BE_InstructionType_Jump:
        case BE_InstructionType_JumpDynamic:
        case BE_InstructionType_JumpConditional:
        case BE_InstructionType_JumpDynamicConditional:
        {
            if (instruction->jump.condition.stack)
            {
                fprintf(out, "cnd-jump ");
                PrintStackEntry(out, runtime, instruction->jump.condition);
                fprintf(out, " ");
            }
            else
            {
                fprintf(out, "jump ");
            }
            
            if (instruction->jump.dynamicNewInstructionIndex.stack)
            {
                PrintStackEntryHex(out, runtime, instruction->jump.dynamicNewInstructionIndex);
            }
            else
            {
                fprintf(out, "%Xh", instruction->jump.newInstructionIndex);
            }
        } break;
        
        case BE_InstructionType_FetchSizeOfFile:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- size-of-file");
        } break;
        
        case BE_InstructionType_HideMembers:
        {
            fprintf(out, "hide-members");
        } break;
        
        case BE_InstructionType_ShowMembers:
        {
            fprintf(out, "show-members");
        } break;
        
        case BE_InstructionType_FetchStartOfType:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- start-of-category-type-%u ", instruction->categoryQuery.categoryIndex);
            PrintStackEntry(out, runtime, instruction->categoryQuery.typeNumber);
        } break;
        
        case BE_InstructionType_HasType:
        {
            PrintStackEntry(out, 0, instruction->result);
            fprintf(out, " <- has-category-type-%u ", instruction->categoryQuery.categoryIndex);
            PrintStackEntry(out, runtime, instruction->categoryQuery.typeNumber);
        } break;
        
        case BE_InstructionType_Assert:
        {
            fprintf(out, "assert ");
            PrintStackEntry(out, runtime, instruction->assert.condition);
        } break;
        
        case BE_InstructionType_Print:
        {
            fprintf(out, "print %.*s, ", (int)instruction->print.prefixCharacterCount, instruction->print.prefixCharacters);
            PrintStackEntry(out, runtime, instruction->print.value);
        } break;
        
        case BE_InstructionType_Breakpoint:
        {
            fprintf(out, "breakpoint");
        } break;
        
        case BE_InstructionType_StructIndex:
        {
            fprintf(out, "struct-index %u", instruction->structIndex.index);
        } break;
    }
    
    fputs("\n", out);
}

static BEu32 CountDecimalDigits(BEu64 value)
{
    BEu32 rVal = 1;
    
    for (BEu64 test = 10ULL; test <= value; test *= 10ULL)
    {
        ++rVal;
        if (test == 10000000000000000000ULL)
        {
            break;
        }
    }
    
    return rVal;
}

static BEu32 CountHexDigits(BEu64 value)
{
    BEu32 digitCount = 1;
    for (BEu64 test = 0x10ULL; test <= value; test *= 0x10ULL)
    {
        ++digitCount;
        if (test == 0x1000000000000000ULL) // NOTE(Jens): Highest 64-bit unsigned 0x10^n value.
        {
            break;
        }
    }
    
    return digitCount;
}

#define MaxBreakpointCount 32

typedef struct Breakpoints Breakpoints;
struct Breakpoints
{
    BEu32 count;
    BEu32 id;
    
    BEu32 addresses[MaxBreakpointCount];
    BEu32 ids[MaxBreakpointCount];
};

static void PrintInstructions(FILE* out, BEu32 sizeInBytes, BEu8* data)
{
    BEu8* encoded = data;
    BEu32 indentation = 0;
    
    BEu32 decimalCount = CountHexDigits(sizeInBytes);
    
    while (encoded < data + sizeInBytes)
    {
        BEu32 size;
        BEInstruction instruction = BEDecodeInstruction(&size, encoded);
        
        fprintf(out, "%*llXh ", (int)decimalCount, (unsigned long long)(encoded - data));
        
        if (instruction.type == BE_InstructionType_PopFrame)
        {
            --indentation;
        }
        
        PrintInstruction(out, &instruction, indentation, 0);
        
        if (instruction.type == BE_InstructionType_PushFrame)
        {
            ++indentation;
        }
        
        encoded += size;
    }
}

static char* ReadDecimalInteger(char* in, BEu32* out)
{
    BEu32 parsed = 0;
    
    // TODO(Jens): Check for overflow.
    while ('0' <= *in && *in <= '9')
    {
        parsed *= 10;
        parsed += *in - '0';
        ++in;
    }
    
    *out = parsed;
    
    return in;
}

static BEbool IsHexDigit(char chr)
{
    return ('0' <= chr && chr <= '9' || 'a' <= chr && chr <= 'f' || 'A' <= chr && chr <= 'F');
}

static char* ReadHexInteger(char* in, BEu32* out)
{
    BEu32 parsed = 0;
    
    while (IsHexDigit(*in))
    {
        parsed *= 0x10;
        if ('0' <= *in && *in <= '9')
        {
            parsed += *in - '0';
        }
        else if ('a' <= *in && *in <= 'f')
        {
            parsed += 10 + (*in - 'a');
        }
        else
        {
            parsed += 10 + (*in - 'A');
        }
        
        ++in;
    }
    
    *out = parsed;
    
    return in;
}

static char* SkipSpaces(char* in)
{
    while (*in == ' ' || *in == '\t')
    {
        ++in;
    }
    
    return in;
}

static char* SkipSpacesAndNewLines(char* in)
{
    while (*in == ' ' || *in == '\t' || *in == '\n' || *in == '\r')
    {
        ++in;
    }
    
    return in;
}

static BEbool ParseUnsignedDecimalIntegerParam(char* param, BEu32* out)
{
    param = SkipSpaces(param);
    
    BEbool rVal = 1;
    
    BEu32 parsed = 0;
    
    if ('0' <= *param && *param <= '9')
    {
        param = ReadDecimalInteger(param, &parsed);
        param = SkipSpacesAndNewLines(param);
        
        rVal = (*param == '\0');
    }
    else
    {
        rVal = 0;
    }
    
    if (rVal)
    {
        *out = parsed;
    }
    
    return rVal;
}

static BEbool ParseUnsignedHexIntegerParam(char* param, BEu32* out)
{
    param = SkipSpaces(param);
    
    BEbool rVal = 1;
    
    BEu32 parsed = 0;
    
    if (IsHexDigit(*param))
    {
        param = ReadHexInteger(param, &parsed);
        if (param[0] == 'h')
        {
            ++param;
        }
        
        param = SkipSpacesAndNewLines(param);
        
        rVal = (*param == '\0');
    }
    else
    {
        rVal = 0;
    }
    
    if (rVal)
    {
        *out = parsed;
    }
    
    return rVal;
}

static BEbool ParamIsString(char* param, char* candidate)
{
    param = SkipSpaces(param);
    
    while (*param && *param == *candidate)
    {
        ++param;
        ++candidate;
    }
    
    param = SkipSpacesAndNewLines(param);
    
    return (*param == '\0') && (*candidate == '\0');
}

typedef struct Debugger Debugger;
struct Debugger
{
    BEu32 prevListedFileIndex;
    BEu32 decimalDigitCountForIndex;
    BEu32 hexDigitCountForAddress;
    BEu32 instructionCount;
    BEu32* instructionAddresses;
    BEu32 exportIndentation;
    char** sourceFiles;
    BEu8* binaryFile;
};

static BEu32 FindInstructionIndexAt(BEu32 instructionCount, BEu32* instructionAddresses, BEu32 address, BEbool* found)
{
    BEu32 leftIndex = 0;
    
    {
        BEu32 rightIndex = instructionCount;
        
        while (leftIndex != rightIndex)
        {
            BEu32 midIndex = (leftIndex + rightIndex + 1) / 2;
            if (instructionAddresses[midIndex] > address)
            {
                rightIndex = midIndex - 1;
            }
            else
            {
                leftIndex = midIndex;
            }
        }
    }
    
    if (found)
    {
        *found = (instructionAddresses[leftIndex] == address);
    }
    else
    {
        BEAssert(instructionAddresses[leftIndex] == address);
    }
    
    return leftIndex;
}

static void BDB_break(Breakpoints* breakpoints, char* param, BEu32 addressCount, BEu32 instructionCount, BEu32* instructionAddresses)
{
    if (breakpoints->count < MaxBreakpointCount)
    {
        BEu32 instructionAddress = 0;
        if (ParseUnsignedHexIntegerParam(param, &instructionAddress))
        {
            if (instructionAddress < addressCount)
            {
                BEbool found;
                FindInstructionIndexAt(instructionCount, instructionAddresses, instructionAddress, &found);
                
                if (found)
                {
                    breakpoints->addresses[breakpoints->count] = instructionAddress;
                    breakpoints->ids[breakpoints->count] = ++breakpoints->id;
                    
                    ++breakpoints->count;
                    
                    fprintf(stdout, "Added breakpoint (id=%u).\n", breakpoints->id);
                }
                else
                {
                    fprintf(stdout, "No instruction at %Xh.\n", instructionAddress);
                }
            }
            else
            {
                fprintf(stdout, "Breakpoint at %Xh is out of range, max instruction address is %Xh.\n", instructionAddress, addressCount - 1);
            }
        }
        else
        {
            fputs("Instruction index required after break command.\n", stdout);
        }
    }
    else
    {
        fputs("Could not add breakpoint, max breakpoints reached.\n", stdout);
    }
}

static void BDB_delbreak(Breakpoints* breakpoints, char* param)
{
    BEu32 idToDelete = 0;
    
    if (ParamIsString(param, "*"))
    {
        if (breakpoints->count)
        {
            for (BEu32 i = 0; i < breakpoints->count; ++i)
            {
                fprintf(stdout, "Deleted breakpoint at %Xh (id=%u)\n", breakpoints->addresses[i], breakpoints->ids[i]);
            }
        }
        else
        {
            fputs("No breakpoints set, nothing to delete.\n", stdout);
        }
        
        breakpoints->count = 0;
    }
    else if (ParseUnsignedDecimalIntegerParam(param, &idToDelete))
    {
        BEu32 indexToRemove;
        for (indexToRemove = 0; indexToRemove < breakpoints->count; ++indexToRemove)
        {
            if (breakpoints->ids[indexToRemove] == idToDelete)
            {
                break;
            }
        }
        
        if (indexToRemove < breakpoints->count)
        {
            fprintf(stdout, "Deleted breakpoint at %Xh (id=%u)\n", breakpoints->addresses[indexToRemove], breakpoints->ids[indexToRemove]);
            
            if (indexToRemove < --breakpoints->count)
            {
                memmove(breakpoints->addresses + indexToRemove, 
                        breakpoints->addresses + indexToRemove + 1,
                        sizeof(*breakpoints->addresses) * (breakpoints->count - indexToRemove));
                memmove(breakpoints->ids + indexToRemove, 
                        breakpoints->ids + indexToRemove + 1,
                        sizeof(*breakpoints->ids) * (breakpoints->count - indexToRemove));
            }
        }
        else
        {
            fprintf(stdout, "Breakpoint with id=%u doesn't exist.\n", idToDelete);
        }
    }
    else
    {
        fputs("Breakpoint identifier, or * required after delbreak command.\n", stdout);
    }
}

static void BDB_showbreak(Breakpoints* breakpoints)
{
    fputs("Breakpoints:\n", stdout);
    
    if (breakpoints->count)
    {
        for (BEu32 i = 0; i < breakpoints->count; ++i)
        {
            fprintf(stdout, "  At %Xh (id=%u)\n", breakpoints->addresses[i], breakpoints->ids[i]);
        }
    }
    else
    {
        fputs("  None\n", stdout);
    }
}

static void WriteEnumValue(BEu32* size, BEu32 capacity, char* out, BERuntime* runtime, BEbool isBigEndian, BEu32 enumIndex, BEu64 valueSize, BEu8* value_);
static void WriteValue(BEu32* size, BEu32 capacity, char* out, BEbool isBigEndian, BE_ScalarBaseType baseType, BE_ScalarDisplayType displayType, BEu64 valueSize, BEu8* value);
static void ShortenStringIfNeeded(BEu32* size, BEu32 capacity, char* str);

static BERuntimeEvent DebuggerStepOne(BERuntime* runtime, Debugger* debugger)
{
    BERuntimeEvent event = BEStepOne(runtime);
    
    if (BE_RuntimeEventType_Error_First <= event.type && event.type <= BE_RuntimeEventType_Error_Last)
    {
        char* filename = BEGetSourceFilenameForFileIndex(runtime, 0, event.sourceFileIndex);
        char* err = BEGetRuntimeErrorDescription(event.type);
        
        if (filename)
        {
            fprintf(stdout, "%s(%u,%u): Error: %s.\n", filename, event.sourceLineIndex + 1, event.sourceColIndex + 1, err);
        }
        else
        {
            fprintf(stdout, "Error: %s.\n", err);
        }
    }
    else if (event.type == BE_RuntimeEventType_StartStruct)
    {
        if (!runtime->hideMembersCounter)
        {
            SetTerminalTextColor(stdout, Color_BDBScalarExport);
            
            if (event.startStruct.arrayIndex != 0)
            {
                --debugger->exportIndentation;
            }
            
            for (BEu32 counter = 0; counter < debugger->exportIndentation; ++counter)
            {
                fputs("    ", stdout);
            }
            
            if (event.startStruct.parentStructIndex != BEInvalidParentStructIndex)
            {
                char* memberName = BEGetStructMemberName(runtime, 0, event.startStruct.parentStructIndex, event.startStruct.memberIndex);
                fputs(memberName, stdout);
            }
            else
            {
                char* structName = BEGetStructName(runtime, 0, event.startStruct.structIndex);
                fputs(structName, stdout);
            }
            
            if (event.startStruct.arrayCount > 0)
            {
                fprintf(stdout, "[%u]", event.startStruct.arrayIndex);
            }
            
            fputc('\n', stdout);
            SetTerminalTextColor(stdout, Color_Default);
        }
        
        ++debugger->exportIndentation;
    }
    else if (event.type == BE_RuntimeEventType_EndStruct)
    {
        assert(debugger->exportIndentation > 0);
        --debugger->exportIndentation;
    }
    else if (event.type == BE_RuntimeEventType_ScalarExport)
    {
        SetTerminalTextColor(stdout, Color_BDBScalarExport);
        
        for (BEu32 counter = 0; counter < debugger->exportIndentation; ++counter)
        {
            fputs("    ", stdout);
        }
        
        {
            char* memberName = BEGetStructMemberName(runtime, 0, event.scalarExport.structIndex, event.scalarExport.memberIndex);
            fputs(memberName, stdout);
        }
        
        if (event.scalarExport.arrayCount)
        {
            fprintf(stdout, "[%llu]\n", event.scalarExport.arrayCount);
            
            for (BEu64 i = 0; i < event.scalarExport.arrayCount; ++i)
            {
                for (BEu32 counter = 0; counter < debugger->exportIndentation; ++counter)
                {
                    fputs("    ", stdout);
                }
                
                SetTerminalTextColor(stdout, Color_BDBScalarExport);
                
                fprintf(stdout, "  [%llu]: ", i);
                
                SetTerminalTextColor(stdout, Color_Default);
                
                BEu32 valueSize = 0;
                char value[128];
                
                if (event.scalarExport.enumIndex != BEInvalidEnumIndex)
                {
                    WriteEnumValue(&valueSize, sizeof(value) - 1, value, runtime, event.scalarExport.isBigEndian, event.scalarExport.enumIndex, event.scalarExport.size, debugger->binaryFile + event.scalarExport.address + event.scalarExport.size * i);
                }
                else
                {
                    WriteValue(&valueSize, sizeof(value) - 1, value, event.scalarExport.isBigEndian, event.scalarExport.baseType, event.scalarExport.displayType, event.scalarExport.size, debugger->binaryFile + event.scalarExport.address + event.scalarExport.size * i);
                }
                
                ShortenStringIfNeeded(&valueSize, sizeof(value) - 1, value);
                value[valueSize] = 0;
                
                fputs(value, stdout);
                fputc('\n', stdout);
            }
        }
        else
        {
            fputs(": ", stdout);
            
            SetTerminalTextColor(stdout, Color_Default);
            
            BEu32 valueSize = 0;
            char value[128];
            
            if (event.scalarExport.enumIndex != BEInvalidEnumIndex)
            {
                WriteEnumValue(&valueSize, sizeof(value) - 1, value, runtime, event.scalarExport.isBigEndian, event.scalarExport.enumIndex, event.scalarExport.size, debugger->binaryFile + event.scalarExport.address);
            }
            else
            {
                WriteValue(&valueSize, sizeof(value) - 1, value, event.scalarExport.isBigEndian, event.scalarExport.baseType, event.scalarExport.displayType, event.scalarExport.size, debugger->binaryFile + event.scalarExport.address);
            }
            
            ShortenStringIfNeeded(&valueSize, sizeof(value) - 1, value);
            value[valueSize] = 0;
            
            fputs(value, stdout);
            fputc('\n', stdout);
        }
    }
    
    return event;
}

static BEbool AboutToHitBreakpoint(BERuntime* runtime, Breakpoints* breakpoints)
{
    BEbool rVal = 0;
    
    for (BEu32 i = 0; i < breakpoints->count; ++i)
    {
        if (runtime->currentAddress == breakpoints->addresses[i])
        {
            rVal = 1;
            break;
        }
    }
    
    return rVal;
}

#define BDB_DefaultInstructionCountForPrinting 7
#define BDB_DefaultInstructionCountForListing 20

static void DebuggerPrintSourceLocationIfRequired(BERuntime* runtime, Debugger* debugger, BEu32 instructionAddress, BESourceLocation* prevSourceLocation, BEu32* prevListedFileIndex)
{
    BESourceLocation sourceLocation = BEGetSourceLocationForInstructionAddress(runtime, instructionAddress);
    
    if (sourceLocation.byteIndex != prevSourceLocation->byteIndex || sourceLocation.fileIndex != *prevListedFileIndex)
    {
        fputc('\n', stdout);
        
        char* sourceFile = debugger->sourceFiles[sourceLocation.fileIndex];
        char* sourceStart = sourceFile + sourceLocation.byteIndex;
        char* lineStart = sourceStart - sourceLocation.bytesSinceStartOfLine;
        char* endOfLine = sourceStart + sourceLocation.sizeInBytes;
        while (endOfLine[0] && endOfLine[0] != '\n' && endOfLine[0] != '\r')
        {
            ++endOfLine;
        }
        
        if (sourceLocation.fileIndex != *prevListedFileIndex)
        {
            char* filename = BEGetSourceFilenameForFileIndex(runtime, 0, sourceLocation.fileIndex);
            
            SetTerminalTextColor(stdout, Color_BDBSourceLineMarker);
            fprintf(stdout, "%s\n", filename);
            
            *prevListedFileIndex = sourceLocation.fileIndex;
        }
        
        BEu32 lineIndex = sourceLocation.lineIndex;
        
        SetTerminalTextColor(stdout, Color_BDBSourceLineMarker);
        fprintf(stdout, "#%u ", lineIndex + 1);
        
        if (lineStart != sourceStart)
        {
            SetTerminalTextColor(stdout, Color_BDBSourceCodeInactive);
            fprintf(stdout, "%.*s", (int)(sourceStart - lineStart), lineStart);
        }
        
        SetTerminalTextColor(stdout, Color_BDBSourceCode);
        for (char* it = sourceStart; it < endOfLine; ++it)
        {
            if (it == sourceStart + sourceLocation.sizeInBytes)
            {
                SetTerminalTextColor(stdout, Color_BDBSourceCodeInactive);
            }
            
            if (*it != '\r')
            {
                fputc(*it, stdout);
                
                if (*it == '\n')
                {
                    ++lineIndex;
                    
                    SetTerminalTextColor(stdout, Color_BDBSourceLineMarker);
                    fprintf(stdout, "#%u ", lineIndex + 1);
                    
                    if (it < sourceStart + sourceLocation.sizeInBytes)
                    {
                        SetTerminalTextColor(stdout, Color_BDBSourceCode);
                    }
                    else
                    {
                        SetTerminalTextColor(stdout, Color_BDBSourceCodeInactive);
                    }
                }
            }
        }
        
        SetTerminalTextColor(stdout, Color_Default);
        
        fputc('\n', stdout);
        
        *prevSourceLocation = sourceLocation;
    }
}

static void DebuggerPrintAllInstructions(BERuntime* runtime, Debugger* debugger, BEbool showSource)
{
    BESourceLocation prevSourceLocation = { 0 };
    prevSourceLocation.byteIndex = (BEu32)-1;
    
    BEu32 prevListedFileIndex = (BEu32)-1;
    
    if (debugger->instructionCount)
    {
        BEu8* encodedInstruction = runtime->encodedInstructions;
        
        for (BEu32 i = 0; 1; ++i)
        {
            if (i < debugger->instructionCount)
            {
                BEInstruction instruction;
                BEu32 instructionAddress = debugger->instructionAddresses[i];
                
                {
                    BEu32 size;
                    instruction = BEDecodeInstruction(&size, encodedInstruction);
                    encodedInstruction += size;
                }
                
                if (showSource)
                {
                    DebuggerPrintSourceLocationIfRequired(runtime, debugger, instructionAddress, &prevSourceLocation, &prevListedFileIndex);
                }
                
                if (instructionAddress == runtime->currentAddress)
                {
                    SetTerminalTextColor(stdout, Color_BDBCurrentInstructionNumber);
                    fprintf(stdout, "%*Xh ", debugger->hexDigitCountForAddress, instructionAddress);
                    SetTerminalTextColor(stdout, Color_BDBCurrentInstructionArrow);
                    fputs("-> ", stdout);
                }
                else
                {
                    SetTerminalTextColor(stdout, Color_BDBOtherInstructionNumber);
                    fprintf(stdout, "%*Xh    ", debugger->hexDigitCountForAddress, instructionAddress);
                }
                SetTerminalTextColor(stdout, Color_Default);
                
                PrintInstruction(stdout, &instruction, 0, instructionAddress == runtime->currentAddress ? runtime : 0);
            }
            else
            {
                fputs("<end of instructions>\n", stdout);
                break;
            }
        }
    }
    else
    {
        fputs("<file has no instructions>\n", stdout);
    }
}

static void DebuggerPrintClosebyInstructions(BERuntime* runtime, Debugger* debugger, BEbool showSource, BEu32 centerAddress, BEs32 indexOffset, BEu32 count)
{
    BESourceLocation prevSourceLocation = { 0 };
    prevSourceLocation.byteIndex = (BEu32)-1;
    
    if (debugger->instructionCount)
    {
        BEu32 centerIndex = FindInstructionIndexAt(debugger->instructionCount, debugger->instructionAddresses, centerAddress, 0);
        
        BEs32 minIndex = (BEs32)centerIndex - (BEs32)count / 2 + indexOffset;
        if (minIndex < 0)
        {
            minIndex = 0;
        }
        
        BEu8* encodedInstruction = runtime->encodedInstructions + debugger->instructionAddresses[minIndex];
        
        for (BEu32 i = minIndex; i < minIndex + count; ++i)
        {
            if (i < debugger->instructionCount)
            {
                BEInstruction instruction;
                BEu32 instructionAddress = debugger->instructionAddresses[i];
                
                {
                    BEu32 size;
                    instruction = BEDecodeInstruction(&size, encodedInstruction);
                    encodedInstruction += size;
                }
                
                if (showSource)
                {
                    DebuggerPrintSourceLocationIfRequired(runtime, debugger, instructionAddress, &prevSourceLocation, &debugger->prevListedFileIndex);
                }
                
                if (instructionAddress == runtime->currentAddress)
                {
                    SetTerminalTextColor(stdout, Color_BDBCurrentInstructionNumber);
                    fprintf(stdout, "%*Xh ", debugger->hexDigitCountForAddress, instructionAddress);
                    SetTerminalTextColor(stdout, Color_BDBCurrentInstructionArrow);
                    fputs("-> ", stdout);
                }
                else
                {
                    SetTerminalTextColor(stdout, Color_BDBOtherInstructionNumber);
                    fprintf(stdout, "%*Xh    ", debugger->hexDigitCountForAddress, instructionAddress);
                }
                SetTerminalTextColor(stdout, Color_Default);
                
                PrintInstruction(stdout, &instruction, 0, instructionAddress == runtime->currentAddress ? runtime : 0);
                // fputs("\n", stdout);
            }
            else
            {
                fputs("<end of instructions>\n", stdout);
                break;
            }
        }
    }
    else
    {
        fputs("<file has no instructions>\n", stdout);
    }
}

static void BDB_until(BERuntime* runtime, Debugger* debugger, BEbool showSource, Breakpoints* breakpoints, BERuntimeEvent* lastEvent)
{
    BEu32 nextAddress = runtime->currentAddress;
    
    {
        BEu32 size;
        BEDecodeInstruction(&size, runtime->encodedInstructions + runtime->currentAddress);
        
        nextAddress += size;
    }
    
    BEs32 stackCounter = 0;
    do
    {
        *lastEvent = DebuggerStepOne(runtime, debugger);
        
        if (lastEvent->instruction.type == BE_InstructionType_PushFrame)
        {
            ++stackCounter;
        }
        else if (lastEvent->instruction.type == BE_InstructionType_PopFrame)
        {
            if (--stackCounter < 0)
            {
                break;
            }
        }
        else if (lastEvent->instruction.type == BE_InstructionType_Breakpoint || AboutToHitBreakpoint(runtime, breakpoints))
        {
            break;
        }
        
        if (stackCounter == 0 && runtime->currentAddress >= nextAddress)
        {
            break;
        }
    }
    while (!(BE_RuntimeEventType_Error_First <= lastEvent->type && lastEvent->type <= BE_RuntimeEventType_Error_Last) && !BERuntimeHasCompleted(runtime));
    
    DebuggerPrintClosebyInstructions(runtime, debugger, showSource, runtime->currentAddress, 0, BDB_DefaultInstructionCountForPrinting);
}

static void BDB_finish(BERuntime* runtime, Debugger* debugger, BEbool showSource, Breakpoints* breakpoints, BERuntimeEvent* lastEvent)
{
    BEs32 stackCounter = 0;
    do
    {
        *lastEvent = DebuggerStepOne(runtime, debugger);
        
        if (lastEvent->instruction.type == BE_InstructionType_PushFrame)
        {
            ++stackCounter;
        }
        else if (lastEvent->instruction.type == BE_InstructionType_PopFrame)
        {
            --stackCounter;
        }
        else if (lastEvent->instruction.type == BE_InstructionType_Breakpoint || AboutToHitBreakpoint(runtime, breakpoints))
        {
            break;
        }
    }
    while (stackCounter >= 0 && !(BE_RuntimeEventType_Error_First <= lastEvent->type && lastEvent->type <= BE_RuntimeEventType_Error_Last) && !BERuntimeHasCompleted(runtime));
    
    DebuggerPrintClosebyInstructions(runtime, debugger, showSource, runtime->currentAddress, 0, BDB_DefaultInstructionCountForPrinting);
}

static void BDB_continue(BERuntime* runtime, Debugger* debugger, BEbool showSource, Breakpoints* breakpoints, BERuntimeEvent* lastEvent)
{
    do
    {
        *lastEvent = DebuggerStepOne(runtime, debugger);
        if (lastEvent->instruction.type == BE_InstructionType_Breakpoint || AboutToHitBreakpoint(runtime, breakpoints))
        {
            break;
        }
    }
    while (!(BE_RuntimeEventType_Error_First <= lastEvent->type && lastEvent->type <= BE_RuntimeEventType_Error_Last) && !BERuntimeHasCompleted(runtime));
    
    DebuggerPrintClosebyInstructions(runtime, debugger, showSource, runtime->currentAddress, 0, BDB_DefaultInstructionCountForPrinting);
}

static void CharToEscaped(char c, BEu32* outLen, char outBuf[4])
{
    switch (c)
    {
        case '\0':
        {
            outBuf[(*outLen)++] = '\\';
            outBuf[(*outLen)++] = '0';
        } break;
        
        case '\a':
        {
            outBuf[(*outLen)++] = '\\';
            outBuf[(*outLen)++] = 'a';
        } break;
        
        case '\b':
        {
            outBuf[(*outLen)++] = '\\';
            outBuf[(*outLen)++] = 'b';
        } break;
        
        case 0x1b:
        {
            outBuf[(*outLen)++] = '\\';
            outBuf[(*outLen)++] = 'e';
        } break;
        
        case '\f':
        {
            outBuf[(*outLen)++] = '\\';
            outBuf[(*outLen)++] = 'f';
        } break;
        
        case '\n':
        {
            outBuf[(*outLen)++] = '\\';
            outBuf[(*outLen)++] = 'n';
        } break;
        
        case '\r':
        {
            outBuf[(*outLen)++] = '\\';
            outBuf[(*outLen)++] = 'r';
        } break;
        
        case '\t':
        {
            outBuf[(*outLen)++] = '\\';
            outBuf[(*outLen)++] = 't';
        } break;
        
        case '\v':
        {
            outBuf[(*outLen)++] = '\\';
            outBuf[(*outLen)++] = 'v';
        } break;
        
        case '\\':
        {
            outBuf[(*outLen)++] = '\\';
            outBuf[(*outLen)++] = '\\';
        } break;
        
        case '"':
        {
            outBuf[(*outLen)++] = '\\';
            outBuf[(*outLen)++] = '"';
        } break;
        
        default:
        {
            if (c <= 0x1f || c == 0x7f)
            {
                outBuf[(*outLen)++] = '\\';
                outBuf[(*outLen)++] = 'x';
                
                BEu8 high = (c & 0xf0) >> 4;
                BEu8 low = c & 0x0f;
                
                if (high < 10)
                {
                    outBuf[(*outLen)++] = '0' + high;
                }
                else
                {
                    outBuf[(*outLen)++] = 'a' + high - 10;
                }
                
                if (low < 10)
                {
                    outBuf[(*outLen)++] = '0' + low;
                }
                else
                {
                    outBuf[(*outLen)++] = 'a' + low - 10;
                }
            }
            else
            {
                outBuf[(*outLen)++] = c;
            }
        } break;
    }
}

static void BDBOutputValue(BEs64 value, char format)
{
    if (value < 0)
    {
        fputs("-", stdout);
        value = -value;
    }
    
    if (value == 0)
    {
        fputs("0", stdout);
    }
    else
    {
        switch (format)
        {
            case 'b':
            {
                BEu32 digitCount = 0;
                
                char buf[10 * 64] = { 0 };
                char* out = buf + sizeof(buf) - 1;
                
                while (value)
                {
                    if (digitCount && digitCount % 4 == 0)
                    {
                        *--out = ' ';
                    }
                    if (digitCount && digitCount % 8 == 0)
                    {
                        *--out = ' ';
                    }
                    
                    char digit = (char)(value & 1);
                    *--out = '0' + digit;
                    
                    ++digitCount;
                    value /= 2;
                    
                    if (out == buf)
                    {
                        break;
                    }
                }
                
                fputs(out, stdout);
            } break;
            
            case 'o':
            {
                BEu32 digitCount = 0;
                
                char buf[5 * 64] = { 0 };
                char* out = buf + sizeof(buf) - 1;
                
                while (value)
                {
                    if (digitCount && digitCount % 3 == 0)
                    {
                        *--out = ' ';
                    }
                    
                    char digit = (char)(value % 010);
                    *--out = '0' + digit;
                    
                    ++digitCount;
                    value /= 010;
                    
                    if (out == buf)
                    {
                        break;
                    }
                }
                
                fputs(out, stdout);
            } break;
            
            case '\0':
            {
                BEu32 digitCount = 0;
                
                char buf[64] = { 0 };
                char* out = buf + sizeof(buf) - 1;
                
                while (value)
                {
                    if (digitCount && digitCount % 3 == 0)
                    {
                        *--out = ' ';
                    }
                    
                    char digit = (char)(value % 10);
                    *--out = '0' + digit;
                    
                    ++digitCount;
                    value /= 10;
                    
                    if (out == buf)
                    {
                        break;
                    }
                }
                
                fputs(out, stdout);
            } break;
            
            case 'h':
            {
                BEu32 digitCount = 0;
                
                char buf[3 * 8] = { 0 };
                char* out = buf + sizeof(buf) - 1;
                
                while (value)
                {
                    if (digitCount && digitCount % 4 == 0)
                    {
                        *--out = ' ';
                    }
                    
                    char digit = (char)(value & 0xf);
                    if (digit < 10)
                    {
                        *--out = '0' + digit;
                    }
                    else
                    {
                        *--out = 'A' + digit - 10;
                    }
                    
                    ++digitCount;
                    value /= 0x10;
                    
                    if (out == buf)
                    {
                        break;
                    }
                }
                
                fputs(out, stdout);
            } break;
            
            case 's':
            {
                BEu32 strLen = 0;
                
                for (BEu32 i = 0; i < sizeof(BEu64); ++i)
                {
                    if ( ((BEu64)value >> (8 * i)) & 0xff )
                    {
                        ++strLen;
                    }
                }
                
                for (BEu32 i = 0; i < strLen; ++i)
                {
                    BEu8 byte = ((BEu64)value >> (8 * i)) & 0xff;
                    
                    char str[5] = { 0 };
                    BEu32 len = 0;
                    CharToEscaped((char)byte, &len, str);
                    
                    fputs(str, stdout);
                }
            } break;
        }
    }
}

static void BDB_print(BERuntime* runtime, char* param_)
{
    param_ = SkipSpaces(param_);
    
    char* param = param_;
    
    BE_StackEntry entryToFetch = { 0 };
    
    BEbool parsedFirstPartCorrectly = 1;
    BEbool entryIsSet = 0;
    BEbool showAsHex = 0;
    
    switch (*param)
    {
        default:
        {
            parsedFirstPartCorrectly = 0;
        } break;
        
        case 'l':
        {
            entryToFetch.stack = BE_Stack_Local;
            ++param;
        } break;
        
        case 'g':
        {
            entryToFetch.stack = BE_Stack_Global;
            ++param;
        } break;
        
        case 'p':
        {
            entryToFetch.stack = BE_Stack_Parent;
            ++param;
        } break;
        
        case '@':
        {
            entryToFetch.stack = BE_Stack_Global;
            entryToFetch.index = BE_GlobalSymbol_MemberOffset;
            entryIsSet = 1;
            ++param;
        } break;
        
        case 'r':
        {
            entryToFetch.stack = BE_Stack_Local;
            entryToFetch.index = BE_LocalSymbol_ReturnAddress;
            entryIsSet = 1;
            showAsHex = 1;
            ++param;
        } break;
        
        case 'i':
        {
            entryToFetch.stack = BE_Stack_Local;
            entryToFetch.index = BE_LocalSymbol_ArrayIndex;
            entryIsSet = 1;
            ++param;
        } break;
    }
    
    param = SkipSpaces(param);
    
    if (parsedFirstPartCorrectly && !entryIsSet)
    {
        parsedFirstPartCorrectly = parsedFirstPartCorrectly && *param++ == '(';
        param = SkipSpaces(param);
        
        char* next = ReadDecimalInteger(param, &entryToFetch.index);
        entryToFetch.index += BE_ReservedSymbolCount;
        parsedFirstPartCorrectly = (next > param);
        param = SkipSpaces(next);
        parsedFirstPartCorrectly = parsedFirstPartCorrectly && *param++ == ')';
    }
    
    if (parsedFirstPartCorrectly)
    {
        param = SkipSpacesAndNewLines(param);
        
        char format = '\0';
        
        if (showAsHex)
        {
            format = 'h';
        }
        
        if (*param == ',')
        {
            ++param;
            param = SkipSpaces(param);
            
            switch (*param)
            {
                case 'b':
                case 'o':
                case 'h':
                case 's':
                {
                    format = *param++;
                } break;
            }
            
            param = SkipSpacesAndNewLines(param);
        }
        
        if (*param == '\0')
        {
            BEs64 value = *BEGetPtr(runtime, entryToFetch);
            BDBOutputValue(value, format);
            fputs("\n", stdout);
        }
        else
        {
            fprintf(stdout, "Expected format specifier (b, o, h) after stack entry, got %s", param);
        }
    }
    else
    {
        fprintf(stdout, "Expected l(<integer>), g(<integer>), p(<integer>), @, r or i, got %s", param_);
    }
}

static void BDB_backtrace(BERuntime* runtime)
{
    if (runtime->currentMembers)
    {
        
#if 0        
        for (BE_MemberNode* it = runtime->currentMembers; it; it = it->parent)
        {
            if (it->prev)
            {
                fputs(BEGetStructName(runtime, 0, it->structIndex), stdout);
                fputs(" (.", stdout);
                fputs(BEGetStructMemberName(runtime, 0, it->prev->structIndex, it->memberIndex), stdout);
                fputs(")", stdout);
            }
            else
            {
                fputs(BEGetStructName(runtime, 0, it->structIndex), stdout);
            }
            fputs("\n", stdout);
        }
#endif
    }
    else
    {
        fputs("Not inside a layout call.\n", stdout);
    }
}

static void BDB_print_hidden(BERuntime* runtime)
{
    if (runtime->hideMembersCounter)
    {
        fprintf(stdout, "Yes (+%u)\n", runtime->hideMembersCounter);
    }
    else
    {
        fputs("No\n", stdout);
    }
}


static void BDB_help(void)
{
    fputs("Available commands - an empty command will repeat previous command (for most commands):\n"
          "\n", stdout);
    
    SetTerminalTextColor(stdout, Color_BDBHelpTitle);
    fputs("Misc\n", stdout);
    SetTerminalTextColor(stdout, Color_Default);
    
    fputs("  help|h                 - shows this screen.\n"
          "  helpinstructions|hi    - shows instruction help screen.\n"
          "  quit|q                 - stops debugging and quits.\n"
          "  restart|r              - restarts the debugging session from beginning.\n"
          "  togglesource|ts        - toggles layout source code display when listing.\n"
          "\n", stdout);
    
    SetTerminalTextColor(stdout, Color_BDBHelpTitle);
    fputs("Stepping\n", stdout);
    SetTerminalTextColor(stdout, Color_Default);
    
    fputs("  next|n                 - continues until next instruction in current frame (skips push-frame).\n"
          "  until|u                - continues until instruction address is greater in current frame (skips jumps).\n"
          "  finish|f               - continues until previous frame is entered.\n"
          "  step|s                 - steps one instruction.\n"
          "  continue|c             - continues until breakpoint is hit.\n"
          "\n", stdout);
    
    SetTerminalTextColor(stdout, Color_BDBHelpTitle);
    fputs("Listing\n", stdout);
    SetTerminalTextColor(stdout, Color_Default);
    
    fputs("  list|l                 - lists instructions around current debugging location.\n"
          "  list+|l+               - lists next instructions.\n"
          "  list-|l-               - lists previous instructions.\n"
          "  list*|l*               - lists all instructions.\n"
          "\n", stdout);
    
    SetTerminalTextColor(stdout, Color_BDBHelpTitle);
    fputs("Breakpoints\n", stdout);
    SetTerminalTextColor(stdout, Color_Default);
    
    fputs("  break|b <instr>        - adds breakpoint at instruction address.\n"
          "  delbreak|db <id>       - deletes breakpoint with numerical id, or all if *.\n"
          "  showbreak|sb           - shows set breakpoints.\n"
          "\n", stdout);
    
    SetTerminalTextColor(stdout, Color_BDBHelpTitle);
    fputs("Inspecting\n", stdout);
    SetTerminalTextColor(stdout, Color_Default);
    
    fputs("  print|p <id> [, fmt]   - prints value of stack entry with optional format.\n"
          "                           When <id> is in format '<stack>' '(' '<integer>' ')' where stack is one of:\n"
          "                             g    - bottom stack (i.e. global stack),\n"
          "                             l    - top stack (i.e. local stack),\n"
          "                             p    - parent stack of local stack.\n"
          "                           the stack entry is printed. <id> may also be one of the special stack entries:\n"
          "                             @    - address of next member,\n"
          "                             r    - return address to goto when struct ends,\n"
          "                             i    - array index of current struct array.\n"
          "\n"
          "                           fmt, if not present will show the value as decimal (except for 'r'), else is one of:\n"
          "                             b  - binary,\n"
          "                             o  - octal,\n"
          "                             h  - hexadecimal.\n"
          "                             s  - ASCII string.\n"
          "  print|p hidden         - prints if members are hidden or not, and the hide counter if is.\n"
          "  backtrace|bt           - shows current member hierarchy from top to bottom.\n"
          , stdout);
}

static void BDB_helpinstructions(void)
{
    SetTerminalTextColor(stdout, Color_BDBHelpTitle);
    fputs("Instruction Help\n"
          "\n"
          "Stack Entries\n"
          "\n", stdout);
    SetTerminalTextColor(stdout, Color_Default);
    
    fputs("Most instructions are in format 'stack-entry <- instruction' where stack-entry is the stack and index\n"
          "or one of the special stack entries ('@', 'r' and 'i').\n"
          "\n"
          "Each time a 'push-frame' instruction is evaluated a new stack is created and set as top. 'pop-frame' pops\n"
          "the top stack and resets the previous one as top. The top stack has the short-hand identifier 'l', \n"
          "the local stack. The bottom-most stack is 'g', the global stack. The stack 'p' refers to the parent stack\n"
          "of the top-most one.\n"
          "\n"
          "The stacks store a number of signed 64-bit values used by instructions. They serve simular purpose as CPU\n"
          "registers but are evaluated by BEdit and not directly by the CPU.\n"
          "\n"
          "Global stack entries:\n", stdout);
    
    SetTerminalTextColor(stdout, Color_BDBHelpInstruction);
    fputs("  @          - current address in binary file.\n"
          "  g(<index>) - general purpose global entry <index>.\n"
          "\n", stdout);
    SetTerminalTextColor(stdout, Color_Default);
    
    fputs("Local stack entries:\n", stdout);
    SetTerminalTextColor(stdout, Color_BDBHelpInstruction);
    
    fputs("  r          - return address.\n"
          "  i          - current array index of struct member.\n"
          "  l(<index>) - general purpose local entry <index>.\n"
          "\n", stdout);
    
    SetTerminalTextColor(stdout, Color_Default);
    fputs("For struct types that take parameters, the arguments are stored in the first local stack entries.\n\n", stdout);
    
    SetTerminalTextColor(stdout, Color_BDBHelpTitle);
    fputs("\nFlow Control\n"
          "\n", stdout);
    SetTerminalTextColor(stdout, Color_Default);
    
    fputs("BEdit runtime will evaluate one instruction at a time starting from first instruction, and then increment the\n"
          "instruction pointer to next instruction, unless the instruction is a jump instruction.\n"
          "\n", stdout);
    
    SetTerminalTextColor(stdout, Color_BDBHelpInstruction);
    fputs("jump <address>|<address-entry>\n"
          "cnd-jump <condition-entry> <address>|<address-entry>\n"
          "\n", stdout);
    SetTerminalTextColor(stdout, Color_Default);
    
    fputs("  Sets the instruction pointer to absolute address indicated by <address> or <address-entry>.\n"
          "  For 'cnd-jump', the jump only occurs if <condition-entry> is non-zero.\n"
          "\n", stdout);
    
    SetTerminalTextColor(stdout, Color_BDBHelpTitle);
    fputs("\nUnary Operators\n"
          "\n", stdout);
    SetTerminalTextColor(stdout, Color_Default);
    
    SetTerminalTextColor(stdout, Color_BDBHelpInstruction);
    fputs("<res-entry> <- <op> <entry>\n"
          "\n", stdout);
    SetTerminalTextColor(stdout, Color_Default);
    
    fputs("  Performs operator <op> on <entry> and stores the result in <res-entry>, the content of <entry>\n"
          "  is unmodified.\n"
          "\n"
          "  <op> is one of:\n"
          "    (nothing)  - copies <entry>\n"
          "    abs        - absolute value\n"
          "    !          - logical not\n"
          "    ~          - bitwise not\n"
          "    -          - negate\n"
          "\n", stdout);
    
    
    SetTerminalTextColor(stdout, Color_BDBHelpTitle);
    fputs("\nBinary Operators\n"
          "\n", stdout);
    SetTerminalTextColor(stdout, Color_Default);
    
    SetTerminalTextColor(stdout, Color_BDBHelpInstruction);
    fputs("<res-entry> <- <lhs-entry> <op> <rhs-entry>\n"
          "\n", stdout);
    SetTerminalTextColor(stdout, Color_Default);
    
    fputs("  Performs operator <op> on <lhs-entry> and <rhs-entry>, and stores the result in <res-entry>.\n"
          "\n"
          "  <op> is one of:\n"
          "    +          - add\n"
          "    -          - subtract\n"
          "    *          - multiply\n"
          "    /          - divide\n"
          "    %          - modulus\n"
          "    <          - less than\n"
          "    <=         - less than or equal\n"
          "    ==         - equal\n"
          "    >          - greater than\n"
          "    >=         - greater than or equal\n"
          "    !=         - not equal\n"
          "    <<         - bit-shift left\n"
          "    >>         - bit-shift right\n"
          "    &          - bitwise add\n"
          "    |          - bitwise or\n"
          "    ^          - bitwise exclusive or\n"
          "    &&         - logical and\n"
          "    ||         - logical or\n"
          "\n", stdout);
    
    
    SetTerminalTextColor(stdout, Color_BDBHelpTitle);
    fputs("\nFrame Manipulation\n"
          "\n", stdout);
    SetTerminalTextColor(stdout, Color_Default);
    
    SetTerminalTextColor(stdout, Color_BDBHelpInstruction);
    fputs("push-frame\n"
          "\n", stdout);
    SetTerminalTextColor(stdout, Color_Default);
    
    fputs("  Creates a new frame, pushes it to top.\n"
          "  After this instruction any stack that was local previously is accessed with 'p(<index>)'\n"
          "\n", stdout);
    
    SetTerminalTextColor(stdout, Color_BDBHelpInstruction);
    fputs("pop-frame\n"
          "\n", stdout);
    SetTerminalTextColor(stdout, Color_Default);
    
    fputs("  Removes top frame and makes previous one top.\n"
          "\n", stdout);
    
    
    SetTerminalTextColor(stdout, Color_BDBHelpTitle);
    fputs("\nStruct Types\n"
          "\n", stdout);
    SetTerminalTextColor(stdout, Color_Default);
    
    SetTerminalTextColor(stdout, Color_BDBHelpInstruction);
    fputs("push-struct(-internal) <member-index>\n"
          "\n", stdout);
    SetTerminalTextColor(stdout, Color_Default);
    
    fputs("  Starts struct member or internal struct member.\n"
          "\n", stdout);
    
    SetTerminalTextColor(stdout, Color_BDBHelpInstruction);
    fputs("pop-struct\n"
          "\n", stdout);
    SetTerminalTextColor(stdout, Color_Default);
    
    fputs("  End of struct member or internal struct member.\n"
          "\n", stdout);
    
    SetTerminalTextColor(stdout, Color_BDBHelpInstruction);
    fputs("struct-index <index>\n"
          "\n", stdout);
    SetTerminalTextColor(stdout, Color_Default);
    
    fputs("  Sets struct index of current struct member. Structs ordering is retained from source code.\n"
          "\n", stdout);
    
    SetTerminalTextColor(stdout, Color_BDBHelpInstruction);
    fputs("scalar-member-u <size-entry> [<array-count>]\n"
          "scalar-member-s <size-entry> [<array-count>]\n"
          "scalar-member-f <size-entry> [<array-count>]\n"
          "scalar-member-string <size-entry> [<array-count>]\n"
          "scalar-member-raw <size-entry> [<array-count>]\n"
          "\n", stdout);
    SetTerminalTextColor(stdout, Color_Default);
    
    fputs("  Defines a scalar member of the current struct at the address '@' and size <size-entry>.\n"
          "\n", stdout);
    
    
    SetTerminalTextColor(stdout, Color_BDBHelpInstruction);
    fputs("<res-entry> <- fetch-member (<member-number-0> [<array-index-0>], ..., <member-number-N> [<array-index-N>])\n"
          "\n", stdout);
    SetTerminalTextColor(stdout, Color_Default);
    
    fputs("  Loads the data of member to <res-entry>, performing any conversions required to load as signed 64-bit.\n"
          "  As the member may be nested the full hierarchy is required as input, for example:\n"
          "\n"
          "    struct Bar { u(4) values[2]; };\n"
          "    struct Baz { u(4) count; Bar bar[count]; };\n"
          "    struct Foo\n"
          "    {\n"
          "        Baz(4) baz[4];\n"
          "        var value = baz[2].bar[8].values[0];\n"
          "        ...\n"
          "\n"
          "  Will request the member (0 [2], 1 [8], 0 [0]).\n"
          "\n", stdout);
    
    SetTerminalTextColor(stdout, Color_BDBHelpInstruction);
    fputs("hide-members\n"
          "show-members\n"
          "\n", stdout);
    SetTerminalTextColor(stdout, Color_Default);
    
    fputs("  Decrements / increments show counter, if this counter is negative members are not shown.\n"
          "  This only affects how the members are displayed.\n"
          "  While debugging with -dbd, use 'print hidden' to show hide counter.\n"
          "\n", stdout);
    
    SetTerminalTextColor(stdout, Color_BDBHelpTitle);
    fputs("\nCategories / Reflection\n"
          "\n", stdout);
    SetTerminalTextColor(stdout, Color_Default);
    
    SetTerminalTextColor(stdout, Color_BDBHelpInstruction);
    fputs("<res-entry> <- start-of-category-type-<category-index> <type-number-entry>\n"
          "\n", stdout);
    SetTerminalTextColor(stdout, Color_Default);
    
    fputs("  Gets start instruction address of struct with category index <category-index> and type number <type-number-entry>.\n"
          "\n", stdout);
    
    SetTerminalTextColor(stdout, Color_BDBHelpInstruction);
    fputs("<res-entry> <- has-category-type-<category-index> <type-number-entry>\n"
          "\n", stdout);
    SetTerminalTextColor(stdout, Color_Default);
    
    fputs("  Checks if the type that category <category-index> with type number <type-number-entry> refers to exists.\n"
          "\n", stdout);
    
    SetTerminalTextColor(stdout, Color_BDBHelpTitle);
    fputs("\nMisc\n"
          "\n", stdout);
    SetTerminalTextColor(stdout, Color_Default);
    
    SetTerminalTextColor(stdout, Color_BDBHelpInstruction);
    fputs("<res-entry> <- size-of-file\n"
          "\n", stdout);
    SetTerminalTextColor(stdout, Color_Default);
    
    fputs("  Gets the size of the binary file being evaluated, in bytes.\n"
          "\n", stdout);
    
    SetTerminalTextColor(stdout, Color_BDBHelpInstruction);
    fputs("<res-entry> <- load <size-entry>\n"
          "\n", stdout);
    SetTerminalTextColor(stdout, Color_Default);
    
    fputs("  Loads data of size <size-entry> directly from binary file at current address (i.e. '@').\n"
          "\n", stdout);
    
    SetTerminalTextColor(stdout, Color_BDBHelpInstruction);
    fputs("print <message> <entry>\n"
          "\n", stdout);
    SetTerminalTextColor(stdout, Color_Default);
    
    fputs("  Prints the message <message> and value of <entry> or (invalid) if wasn't specified.\n"
          "\n", stdout);
    
    SetTerminalTextColor(stdout, Color_BDBHelpInstruction);
    fputs("assert <condition>\n"
          "\n", stdout);
    SetTerminalTextColor(stdout, Color_Default);
    
    fputs("  If <condition> evaluates to zero, stops executing instructions.\n"
          "\n", stdout);
    
    SetTerminalTextColor(stdout, Color_BDBHelpInstruction);
    fputs("breakpoint\n"
          "\n", stdout);
    SetTerminalTextColor(stdout, Color_Default);
    
    fputs("  Halts the debugger. This instruction is only emitted by the code 'breakpoint();' and is ignored\n"
          "  when not running with -bdb.\n"
          "\n", stdout);
}

static void BDB_step(BERuntime* runtime, Debugger* debugger, BEbool showSource, BERuntimeEvent* lastEvent)
{
    *lastEvent = DebuggerStepOne(runtime, debugger);
    DebuggerPrintClosebyInstructions(runtime, debugger, showSource, runtime->currentAddress, 0, BDB_DefaultInstructionCountForPrinting);
}

static void BDB_next(BERuntime* runtime, Debugger* debugger, BEbool showSource, Breakpoints* breakpoints, BERuntimeEvent* lastEvent)
{
    BEs32 stackCounter = 0;
    do
    {
        *lastEvent = DebuggerStepOne(runtime, debugger);
        
        if (lastEvent->instruction.type == BE_InstructionType_PushFrame)
        {
            ++stackCounter;
        }
        else if (lastEvent->instruction.type == BE_InstructionType_PopFrame)
        {
            --stackCounter;
        }
        else if (lastEvent->instruction.type == BE_InstructionType_Breakpoint || AboutToHitBreakpoint(runtime, breakpoints))
        {
            break;
        }
    }
    while (stackCounter > 0 
           && !(BE_RuntimeEventType_Error_First <= lastEvent->type && lastEvent->type <= BE_RuntimeEventType_Error_Last) 
           && !BERuntimeHasCompleted(runtime));
    
    DebuggerPrintClosebyInstructions(runtime, debugger, showSource, runtime->currentAddress, 0, BDB_DefaultInstructionCountForPrinting);
}

static BEbool StringIsCmdWithParameter(char* str, char* cmd, char** paramStart)
{
    BEbool rVal = 1;
    
    for (char* c = cmd; *c; ++c)
    {
        if (*str++ != *c)
        {
            rVal = 0;
            break;
        }
    }
    
    if (rVal)
    {
        rVal = (*str == ' ' || *str == '\t' || *str == '\n' || *str == '\r' || *str == '\0');
        
        *paramStart = str + 1;
    }
    
    return rVal;
}

#ifdef _MSC_VER
#  pragma warning(push)
#  pragma warning(disable:4706) // NOTE(Jens): "C4706: Assignment within conditional expression"
#endif

static void DebugData(BEu8* dataSection, BEu32 instructionsSizeInBytes, BEu8* instructions, BEu32 sizeOfFile, BEu8* binaryFile, char** sourceFiles)
{
    fputs("bdb - Interactive CLI debugger for BEdit layout instructions. Write 'help' for more information.\n", stdout);
    
    label_Restart:
    
    CommandLineAllocator runtimeAllocator = { 0 };
    
    BEu8 stack[64 * 1024]; // TODO(Jens): Maybe restart if stack overflow and increase this number, or pass the size in.
    BERuntime runtime = BEStartRuntime(&runtimeAllocator, sizeof(stack), stack, dataSection, instructionsSizeInBytes, instructions, sizeOfFile, binaryFile);
    
    Debugger debugger = { 0 };
    debugger.prevListedFileIndex = (BEu32)-1;
    debugger.instructionAddresses = BEAllocArray(&runtimeAllocator, BEu32, runtime.instructionCount + 1);
    debugger.sourceFiles = sourceFiles;
    debugger.binaryFile = binaryFile;
    
    {
        BEu32 address = 0;
        
        while (address < instructionsSizeInBytes)
        {
            BEu32 size;
            BEDecodeInstruction(&size, instructions + address);
            
            debugger.instructionAddresses[debugger.instructionCount++] = address;
            address += size;
        }
        
        debugger.instructionAddresses[debugger.instructionCount] = address;
    }
    
    debugger.decimalDigitCountForIndex = CountDecimalDigits(debugger.instructionCount);
    debugger.hexDigitCountForAddress = CountHexDigits(instructionsSizeInBytes);
    
    BEbool showSourceInListing = 1;
    
#define ElseIfCmd(cmd, alias) else if ((strcmp(buf, alias "\n") == 0 || strcmp(buf, cmd "\n") == 0) && (prevCmd = cmd "\n"))
#define ElseIfCmdWithParam(cmd, alias) else if ((StringIsCmdWithParameter(buf, alias, &param) || StringIsCmdWithParameter(buf, cmd, &param)))
    
    DebuggerPrintClosebyInstructions(&runtime, &debugger, showSourceInListing, runtime.currentAddress, 0, BDB_DefaultInstructionCountForPrinting);
    
    Breakpoints breakpoints = { 0 };
    
    BEs32 listingOffset = 0;
    
    char* prevCmd = "";
    char buf_[128];
    
    BERuntimeEvent lastEvent = { 0 };
    
    for (;;)
    {
        fputs("\n$ ", stdout);
        
        if (fgets(buf_, sizeof(buf_), stdin))
        {
            char* buf = buf_;
            
            if (strcmp(buf, "\n") == 0)
            {
                buf = prevCmd;
            }
            
            BEbool showListing = 0;
            BEbool unrecognizedCmd = 0;
            
            char* param = 0;
            
            if (0) 
            {
            }
            ElseIfCmd("restart", "r")
            {
                DeallocAll(&runtimeAllocator);
                goto label_Restart;
            }
            ElseIfCmd("help", "h")
            {
                BDB_help();
            }
            ElseIfCmd("helpinstructions", "hi")
            {
                BDB_helpinstructions();
            }
            ElseIfCmdWithParam("togglesource", "ts")
            {
                showSourceInListing = !showSourceInListing;
            }
            ElseIfCmdWithParam("break", "b")
            {
                BDB_break(&breakpoints, param, runtime.instructionsSizeInBytes, debugger.instructionCount, debugger.instructionAddresses);
            }
            ElseIfCmdWithParam("delbreak", "db")
            {
                BDB_delbreak(&breakpoints, param);
            }
            ElseIfCmd("showbreak", "sb")
            {
                BDB_showbreak(&breakpoints);
            }
            ElseIfCmd("quit", "q")
            {
                break;
            }
            ElseIfCmd("until", "u")
            {
                BDB_until(&runtime, &debugger, showSourceInListing, &breakpoints, &lastEvent);
            }
            ElseIfCmd("finish", "f")
            {
                BDB_finish(&runtime, &debugger, showSourceInListing, &breakpoints, &lastEvent);
            }
            ElseIfCmd("next", "n")
            {
                BDB_next(&runtime, &debugger, showSourceInListing, &breakpoints, &lastEvent);
            }
            ElseIfCmd("step", "s")
            {
                BDB_step(&runtime, &debugger, showSourceInListing, &lastEvent);
            }
            ElseIfCmd("continue", "c")
            {
                BDB_continue(&runtime, &debugger, showSourceInListing, &breakpoints, &lastEvent);
            }
            ElseIfCmd("list", "l")
            {
                listingOffset = 0;
                showListing = 1;
            }
            ElseIfCmd("list+", "l+")
            {
                listingOffset += BDB_DefaultInstructionCountForListing;
                showListing = 1;
                
                BEu32 instructionIndex = FindInstructionIndexAt(debugger.instructionCount, debugger.instructionAddresses, runtime.currentAddress, 0);
                if (instructionIndex + listingOffset >= debugger.instructionCount)
                {
                    listingOffset = debugger.instructionCount - instructionIndex - 1;
                }
                
            }
            ElseIfCmd("list-", "l-")
            {
                listingOffset -= BDB_DefaultInstructionCountForListing;
                showListing = 1;
                
                BEu32 instructionIndex = FindInstructionIndexAt(debugger.instructionCount, debugger.instructionAddresses, runtime.currentAddress, 0);
                if (instructionIndex + listingOffset < 0)
                {
                    listingOffset = -(BEs32)instructionIndex;
                }
            }
            ElseIfCmd("list*", "l*")
            {
                DebuggerPrintAllInstructions(&runtime, &debugger, showSourceInListing);
            }
            ElseIfCmdWithParam("print hidden", "p hidden")
            {
                BDB_print_hidden(&runtime);
            }
            ElseIfCmdWithParam("print", "p")
            {
                BDB_print(&runtime, param);
            }
            ElseIfCmdWithParam("backtrace", "bt")
            {
                BDB_backtrace(&runtime);
            }
            else
            {
                BEu32 len = (BEu32)strlen(buf);
                fprintf(stdout, "Unrecognized command '%.*s'\n", len - 1, buf);
                unrecognizedCmd = 1;
            }
            
            if (showListing)
            {
                DebuggerPrintClosebyInstructions(&runtime, &debugger, showSourceInListing, runtime.currentAddress, listingOffset, BDB_DefaultInstructionCountForListing);
            }
            else
            {
                if (!unrecognizedCmd)
                {
                    listingOffset = 0;
                }
            }
        }
        else
        {
            break;
        }
        
        if (BERuntimeHasCompleted(&runtime))
        {
            fputs("Execution has finished.\n", stdout);
        }
    }
    
#undef ElseIfCmdWithParam
#undef ElseIfCmd
}

#ifdef _MSC_VER
#  pragma warning(pop)
#endif

#define SamplesPerPlotData 1024

typedef struct PlotData PlotData;
struct PlotData
{
    PlotData* next;
    BEf64 values[SamplesPerPlotData];
};

typedef struct PlotSamples PlotSamples;
struct PlotSamples
{
    BEu32 count;
    PlotData* head;
    PlotData* tail;
    BEf64 prevValue;
    BEf64 minValue;
    BEf64 maxValue;
    BEf64 absoluteSumOfDifferences;
};

typedef struct PlotLines PlotLines;
struct PlotLines
{
    BEbool xIsNotMonotonicallyIncreasing;
    
    union
    {
        struct
        {
            PlotSamples xs;
            PlotSamples ys;
        };
        
        PlotSamples samples[2];
    };
};

typedef struct Plot Plot;
struct Plot
{
    Plot* next;
    
    BEs64 id;
    PlotLines lines;
};

typedef struct Plots Plots;
struct Plots
{
    Plot* list;
};

static PlotLines* AcquirePlotLines(BEAllocatorType allocator, Plots* plots, BEs64 id)
{
    Plot* plot = plots->list;
    while (plot && plot->id != id)
    {
        plot = plot->next;
    }
    
    PlotLines* rVal;
    
    if (plot)
    {
        rVal = &plot->lines;
    }
    else
    {
        Plot* newPlot = BEAlloc(allocator, Plot);
        memset(newPlot, 0, sizeof(*newPlot));
        newPlot->id = id;
        
        newPlot->next = plots->list;
        plots->list = newPlot;
        
        rVal = &newPlot->lines;
    }
    
    return rVal;
}

static BEbool HostIsLittleEndian(void)
{
	union
	{
		BEu32 i;
		char c[4];
	} e = { 0x01000000 };
	return e.c[3];
}

static void AddPlotSample(BEAllocatorType allocator, Plots* plots, BEs64 plotId, BEf64 value, BEbool isX)
{
    PlotLines* lines = AcquirePlotLines(allocator, plots, plotId);
    PlotSamples* samples = lines->samples + !isX;
    
    if (isX && samples->count && !lines->xIsNotMonotonicallyIncreasing)
    {
        lines->xIsNotMonotonicallyIncreasing = (samples->prevValue > value);
    }
    
    BEu32 localIndex = samples->count % SamplesPerPlotData;
    
    if (!samples->count || value < samples->minValue)
    {
        samples->minValue = value;
    }
    if (!samples->count || value > samples->maxValue)
    {
        samples->maxValue = value;
    }
    
    if (samples->count)
    {
        BEf64 diff = value - samples->prevValue;
        samples->absoluteSumOfDifferences += (diff > 0.0 ? diff : -diff);
    }
    
    samples->prevValue = value;
    
    if (!localIndex)
    {
        PlotData* data = BEAlloc(allocator, PlotData);
        data->next = 0;
        
        if (samples->tail)
        {
            samples->tail = samples->tail->next = data;
        }
        else
        {
            samples->tail = samples->head = data;
        }
    }
    
    samples->tail->values[localIndex] = value;
    
    ++samples->count;
}

typedef struct FileLoader FileLoader;
struct FileLoader
{
    BEbool retainSources;
};

static BELoadFileResult loadFile(void* userdata, BEAllocatorType allocator, BEu32 pathLen, const char* nullTerminatedPath)
{
    BELoadFileResult rVal = { 0 };
    
    FILE* file = fopen(nullTerminatedPath, "rb");
    if (file)
    {
        fseek(file, 0, SEEK_END);
        long size = ftell(file);
        fseek(file, 0, SEEK_SET);
        
        void* data;
        if (userdata && ((FileLoader*)userdata)->retainSources)
        {
            data = malloc(size + 1);
        }
        else
        {
            data = BEAllocArray(allocator, BEu8, size + 1);
        }
        
        fread(data, size, 1, file);
        fclose(file);
        
        ((char*)data)[size] = '\0'; // NOTE(Jens): Nulltermination is not required by compiler, but is assumed by debugger.
        
        rVal.error = ferror(file);
        rVal.fileSizeInBytes = size;
        rVal.fileContent = data;
    }
    else
    {
        rVal.error = -1;
    }
    
    return rVal;
}

typedef struct CompiledData CompiledData;
struct CompiledData
{
    BEu32 instructionsSizeInBytes;
    BEu8* instructions;
    BEDataSection dataSection;
    char** sourceFiles;
};

static BEbool CompiledDataIsValid(CompiledData* compiled)
{
    return compiled->instructions && compiled->dataSection.data;
}

typedef struct CompiledInstructionChunk CompiledInstructionChunk;
struct CompiledInstructionChunk
{
    CompiledInstructionChunk* next;
    BEu32 size;
    BEu8 data[1024];
};

typedef struct CompiledInstructions CompiledInstructions;
struct CompiledInstructions
{
    CompiledInstructionChunk head;
    CompiledInstructionChunk* tail;
    
    BEu32 totalSize;
};

static void PrintTokenLine(FILE* out, char* fileContent, BEu32 colIndex, BEu32 byteIndex)
{
    char* startOfLine = fileContent + (byteIndex - colIndex);
    
    int lineSize = 0;
    while (startOfLine[lineSize] != 0 && startOfLine[lineSize] != '\n')
    {
        ++lineSize;
    }
    
    fprintf(out, "%.*s\n", lineSize, startOfLine);
    for (BEu32 i = 0; i < colIndex; ++i)
    {
        if (startOfLine[i] == '\t')
        {
            putc('\t', out);
        }
        else
        {
            putc(' ', out);
        }
    }
    putc('^', out);
    putc('\n', out);
}

static CompiledData CompileLayout(CommandLineAllocator* outputAllocator, FILE* errOut, char* layoutFile, BEbool retainSources)
{
    CommandLineAllocator compilationAllocator = { 0 };
    CompiledData compiled = { 0 };
    
    FileLoader loader;
    loader.retainSources = retainSources;
    
    BEFileLoader fileLoader;
    fileLoader.context = &loader;
    fileLoader.loadFile = loadFile;
    
    BECompilationContext compilation;
    BEFileLoadError err = BEStartCompilation(&compilation, &compilationAllocator, fileLoader, 0, layoutFile);
    if (!err)
    {
        CompiledInstructions instructions;
        instructions.head.next = 0;
        instructions.head.size = 0;
        instructions.tail = &instructions.head;
        instructions.totalSize = 0;
        
        BECompilationEvent event;
        do
        {
            BEu32 outIndex = instructions.tail->size;
            BEu32 size = sizeof(instructions.tail->data) - outIndex;
            
            event = BEGenerateInstructions(&compilation, &size, instructions.tail->data + outIndex);
            instructions.totalSize += size;
            instructions.tail->size += size;
            
            if (outIndex + size == sizeof(instructions.tail->data))
            {
                instructions.tail = instructions.tail->next = BEAlloc(&compilationAllocator, CompiledInstructionChunk);
                instructions.tail->next = 0;
                instructions.tail->size = 0;
            }
        }
        while (event.type == BECompilationEventType_Continue);
        
        if (event.type == BECompilationEventType_Complete)
        {
            compiled.instructionsSizeInBytes = instructions.totalSize;
            compiled.instructions = BEAllocArray(outputAllocator, BEu8, instructions.totalSize);
            BEu8* out = compiled.instructions;
            
            for (CompiledInstructionChunk* chunk = &instructions.head; chunk; chunk = chunk->next)
            {
                memcpy(out, chunk->data, chunk->size);
                out += chunk->size;
            }
            
            compiled.dataSection = BEFinalizeCompilation(&compilation, outputAllocator);
        }
        else
        {
            fprintf(errOut, "%s(%u,%u): %s\n", BEGetFilename(&compilation, event.sourceFileIndex), event.sourceLineIndex + 1, event.sourceColIndex + 1, BEGetCompilationErrorDescription(event.type));
            
            char* fileContent = BEGetFileContent(&compilation, event.sourceFileIndex, 0);
            PrintTokenLine(errOut, fileContent, event.sourceColIndex, event.sourceByteIndex);
        }
    }
    else
    {
        fprintf(errOut, "Failed to load layout file %s\n", layoutFile);
    }
    
    if (retainSources)
    {
        BEu32 fileCount = BEGetFileCount(&compilation);
        compiled.sourceFiles = malloc(fileCount * sizeof(char*));
        
        for (BEu32 fileIndex = 0; fileIndex < fileCount; ++fileIndex)
        {
            // IMPORTANT(Jens): When retainSources flag is set for file loader, the file content is allocated from the CRT heap instead of the allocator.
            compiled.sourceFiles[fileIndex] = BEGetFileContent(&compilation, fileIndex, 0);
        }
    }
    
    DeallocAll(&compilationAllocator);
    
    return compiled;
}

typedef enum LineType LineType;
enum LineType
{
    LineType_Member,
    LineType_StructStart,
    LineType_Print,
};

typedef struct LineListNode LineListNode;
struct LineListNode
{
    LineListNode* next;
    LineType type;
    
    BEu64 arrayIndex;
    BEu64 arrayCount;
    
    union
    {
        struct
        {
            BEu64 address;
            BEu32 nameSize;
            BEu32 valueSize;
            
            char name[128];
            char value[128];
        } member;
        
        struct
        {
            BEu32 memberNameSize;
            BEu32 typeNameSize;
            char memberName[128];
            char typeName[128];
        } structType;
        
        struct
        {
            BEu32 messageSize;
            BEu32 valueSize;
            char message[128];
            char value[128];
        } print;
    };
};

typedef struct NamePrefix NamePrefix;
struct NamePrefix
{
    NamePrefix* prev;
    
    BEu32 size;
    char* str;
    BEu32 arrayIndexPlusOne;
};

typedef struct ColumnLayout ColumnLayout;
struct ColumnLayout
{
    CommandLineAllocator allocator;
    
    LineListNode* head;
    LineListNode* tail;
    
    BEu32 maxNameSize;
    BEu32 maxValueSize;
    
    NamePrefix* freePrefix;
    NamePrefix* prefixes;
};

static void PushMemberNamePrefix(ColumnLayout* layout, BEu32 size, char* str)
{
    NamePrefix* newPrefix = layout->freePrefix;
    if (newPrefix)
    {
        layout->freePrefix = layout->freePrefix->prev;
    }
    else
    {
        newPrefix = BEAlloc(&layout->allocator, NamePrefix);
    }
    
    newPrefix->size = size;
    newPrefix->str = str;
    newPrefix->prev = layout->prefixes;
    newPrefix->arrayIndexPlusOne = 0;
    layout->prefixes = newPrefix;
}

static void PushMemberNamePrefixWithArrayIndex(ColumnLayout* layout, BEu32 size, char* str, BEu32 arrayIndex)
{
    NamePrefix* newPrefix = layout->freePrefix;
    if (newPrefix)
    {
        layout->freePrefix = layout->freePrefix->prev;
    }
    else
    {
        newPrefix = BEAlloc(&layout->allocator, NamePrefix);
    }
    
    newPrefix->size = size;
    newPrefix->str = str;
    newPrefix->prev = layout->prefixes;
    newPrefix->arrayIndexPlusOne = 1 + arrayIndex;
    layout->prefixes = newPrefix;
}

static void PushMemberNamePrefixBarrier(ColumnLayout* layout)
{
    NamePrefix* newPrefix = layout->freePrefix;
    if (newPrefix)
    {
        layout->freePrefix = layout->freePrefix->prev;
    }
    else
    {
        newPrefix = BEAlloc(&layout->allocator, NamePrefix);
    }
    
    newPrefix->size = 0;
    newPrefix->str = 0;
    newPrefix->prev = layout->prefixes;
    layout->prefixes = newPrefix;
}

static void PopMemberNamePrefix(ColumnLayout* layout)
{
    NamePrefix* oldPrefix = layout->prefixes;
    layout->prefixes = layout->prefixes->prev;
    oldPrefix->prev = layout->freePrefix;
    layout->freePrefix = oldPrefix;
}

static void ShortenStringIfNeeded(BEu32* size, BEu32 capacity, char* str)
{
    BEAssert(capacity >= 3);
    if (*size > capacity)
    {
        *size = capacity;
        str[capacity - 1] = str[capacity - 2] = str[capacity - 3] = '.';
    }
}

static BEu32 StringToString(BEu32 bufLen, char* buf, BEu64 valueSize, BEu8* value)
{
	BEu32 writtenCount = 0;
	char* out = buf;
	
#define TryWrite(c) if ((c) && writtenCount++ < bufLen) { *out++ = (c); }
	
	TryWrite('"');
	
    for (BEu32 i = 0; i < valueSize; ++i)
    {
        BEu8 in = value[i];
        
        BEu32 len = 0;
        char escapeBuf[4];
        
        CharToEscaped((char)in, &len, escapeBuf);
        for (BEu32 bufI = 0; bufI < len; ++bufI)
        {
            TryWrite(escapeBuf[bufI]);
        }
    }
    
	TryWrite('"');
	
#undef TryWrite
    
	return writtenCount;
}

typedef enum
{ 
	DisplayFlag_InDisplayOrder = (1 << 0),   // NOTE(Jens): For little-endian systems, this will swap the byte display.
	DisplayFlag_LowerCase = (1 << 1),
	DisplayFlag_AddLeadingZeros = (1 << 2),
	DisplayFlag_AddSuffix = (1 << 3),
	DisplayFlag_AddPrefix = (1 << 4),
	
	DisplayFlag_Hex_hSuffix = (1 << 3),
	DisplayFlag_Hex_0xPrefix = (1 << 4),
	
	DisplayFlag_Binary_bSuffix = (1 << 3),
	DisplayFlag_Binary_0bPrefix = (1 << 4),
} DisplayFlag;

typedef BEu32 DisplayFlags;

static BEu32 BinaryToString(BEu32 bufLen, char* buf, BEbool swapByteOrder, BEu64 valueSize, BEu8* value, DisplayFlags flags, char padding)
{
	BEu32 writtenCount = 0;
	char* out = buf;
	
#define TryWrite(c) if ((c) != 0 && writtenCount++ < bufLen) { *out ++= (c); }
#define TryWriteDigit(c) if ((c) != 0 && writtenCount++ < bufLen) { *out++ = (c); }
	
	if (flags & DisplayFlag_Binary_0bPrefix)
	{
		TryWrite('0');
		TryWrite('b');
	}
	
	if ((flags & DisplayFlag_InDisplayOrder) && HostIsLittleEndian())
	{
		swapByteOrder = !swapByteOrder;
	}
	
	for (BEu32 readIndex = 0; readIndex < valueSize; ++readIndex)
	{
		BEu64 byteIndex = readIndex;
		if (swapByteOrder)
		{
			byteIndex = valueSize - byteIndex - 1;
		}
		BEu8 byteValue = value[byteIndex];
		
		for (BEu32 bitIndex = 0; bitIndex < 8; ++bitIndex)
		{
			BEu8 bitValue = (byteValue & (1 << (7 - bitIndex)));
			
			if (bitIndex && bitIndex % 4 == 0)
			{
				TryWrite(padding);
			}
			
			if (bitIndex && bitIndex % 8 == 0)
			{
				TryWrite(padding);
			}
			
			TryWriteDigit(bitValue ? '1' : '0');
		}
	}
	
	if (!(flags & DisplayFlag_AddLeadingZeros))
	{
		// TODO
	}
    
	if (!writtenCount)
	{
		TryWriteDigit('0');
	}
	
	if (flags & DisplayFlag_Binary_bSuffix)
	{
		TryWrite('b');
	}
	
#undef TryWrite
#undef TryWriteDigit
	
	return writtenCount;
}

static BEu32 HexToString(BEu32 bufLen, char* buf, BEbool swapByteOrder, BEu64 valueSize, BEu8* value, DisplayFlags flags, char padding)
{
	BEu32 writtenCount = 0;
	char* out = buf;
	
#define TryWrite(c) if ((c) && writtenCount++ < bufLen) { *out++ = (c); }
#define TryWriteDigit(c) if ((c) && writtenCount++ < bufLen) { *out++ = (c); }
	
	if (flags & DisplayFlag_Hex_0xPrefix)
	{
		TryWrite('0');
		TryWrite('x');
	}
	
	if ((flags & DisplayFlag_InDisplayOrder) && HostIsLittleEndian())
	{
		swapByteOrder = !swapByteOrder;
	}
	
    BEbool mayWriteZeros = (flags & DisplayFlag_AddLeadingZeros);
	
	for (BEu64 readIndex = 0; readIndex < valueSize; ++readIndex)
	{
		BEu64 byteIndex = (BEu32)readIndex;
		if (swapByteOrder)
		{
			byteIndex = valueSize - byteIndex - 1;
		}
		
		BEu8 byteValue = value[byteIndex];
		
		BEu8 low = (byteValue & 0x0f);
		BEu8 high = (byteValue & 0xf0) >> 4;
		
		if (high || mayWriteZeros)
		{
			mayWriteZeros = 1;
			if (high < 10)
			{
				TryWriteDigit('0' + high);
			}
			else
			{
				TryWriteDigit(((flags & DisplayFlag_LowerCase) ? 'a' : 'A') + (high - 10));
			}
		}
		
		if (low || mayWriteZeros)
		{
			mayWriteZeros = 1;
			if (low < 10)
			{
				TryWriteDigit('0' + low);
			}
			else
			{
				TryWriteDigit(((flags & DisplayFlag_LowerCase) ? 'a' : 'A') + (low - 10));
			}
		}
		
		if (mayWriteZeros)
		{
			if (readIndex + 1 < valueSize)
			{
				TryWrite(padding);
			}
		}
	}
    
	if (!writtenCount)
	{
		TryWriteDigit('0');
	}
	
	if (flags & DisplayFlag_Hex_hSuffix)
	{
		TryWrite('h');
	}
	
#undef TryWrite
#undef TryWriteDigit
	
	return writtenCount;
}

static BEu32 DecimalToString(BEu32 bufLen, char* buf, BEbool swapByteOrder, BEu64 valueSize, BEu8* value_, char padding, BEbool valueIsSigned)
{
	BEu32 writtenCount = 0;
	
    if (valueSize <= sizeof(BEu64))
    {
        BEu8 v[8];
        
        if (!swapByteOrder)
        {
            for (BEu32 i = 0; i < valueSize; ++i)
            {
                v[i] = value_[i];
            }
        }
        else
        {
            for (BEu32 i = 0; i < valueSize; ++i)
            {
                v[i] = value_[valueSize - 1 - i];
            }
        }
        
        BEu64 value = 0;
        memcpy(&value, v, valueSize);
        
        BEu32 characterCount = 0;
        BEbool valueIsNegative = 0;
        
        if (valueIsSigned)
        {
            valueIsNegative = !!(value & (1ULL << (8ULL * valueSize - 1)));
            
            if (valueIsNegative)
            {
                BEu64 mask = 0;
                for (BEu64 i = 0; i < valueSize; ++i)
                {
                    mask <<= 8;
                    mask |= 0xff;
                }
                
                value ^= mask;
                ++value;
            }
        }
        
        BEu32 totalDigitCount = CountDecimalDigits(value);
        
        characterCount += totalDigitCount;
        if (valueIsNegative)
        {
            ++characterCount;
        }
        
        if (padding)
        {
            characterCount += (totalDigitCount - 1) / 3;
        }
        
        char* out = buf + characterCount;
        
#define TryWrite(c) if ((c) && writtenCount++ < bufLen) { *--out = (c); }
#define TryWriteDigit(c) if ((c) && writtenCount++ < bufLen) { *--out = (c); }
        
        if (value)
        {
            BEu32 digitCount = 0;
            while (value)
            {
                if (digitCount && digitCount % 3 == 0)
                {
                    TryWrite(padding);
                }
                
                char digit = '0' + (char)(value % 10);
                value /= 10;
                TryWriteDigit(digit);
                
                ++digitCount;
            }
            
            if (valueIsNegative)
            {
                TryWrite('-');
            }
            
            assert(digitCount == totalDigitCount);
        }
        else
        {
            TryWriteDigit('0');
        }
        
#undef TryWrite
#undef TryWriteDigit
        
        assert(characterCount == writtenCount);
    }
    else
    {
        // TODO Support?
    }
    
	return writtenCount;
}

static BEu32 FloatToString(BEu32 bufLen, char* buf, BEbool swapByteOrder, BEu64 valueSize, BEu8* value_)
{
	// TODO(Jens): Better float printing.
    
	BEu32 size = 0;
	if (valueSize == 4)
	{
		float value = 0.0f;
        for (BEu32 i = 0; i < sizeof(value); ++i)
        {
            if (swapByteOrder)
            {
                ((BEu8*)&value)[i] = value_[sizeof(value) - 1 - i];
            }
            else
            {
                ((BEu8*)&value)[i] = value_[i];
            }
        }
        
        size += snprintf(buf, bufLen, "%f", value);
	}
	else if (valueSize == 8)
	{
		double value = 0.0f;
		for (BEu32 i = 0; i < sizeof(value); ++i)
        {
            if (swapByteOrder)
            {
                ((BEu8*)&value)[i] = value_[sizeof(value) - 1 - i];
            }
            else
            {
                ((BEu8*)&value)[i] = value_[i];
            }
        }
        
        size += snprintf(buf, bufLen, "%f", value);
	}
	else
	{
		// TODO(Jens): Half floats? And what about 'long double'?
	}
    
	return size;
}

static void WriteEnumValue(BEu32* size, BEu32 capacity, char* out, BERuntime* runtime, BEbool isBigEndian, BEu32 enumIndex, BEu64 valueSize, BEu8* value_)
{
    BEbool swapByteOrder = (isBigEndian == HostIsLittleEndian());
    
    BEbool hasHighBytesSet = 0;
    BEs64 value = 0;
    for (BEu64 i = 0; i < valueSize; ++i)
    {
        if (i < sizeof(value))
        {
            if (swapByteOrder)
            {
                value |= (BEu64)value_[i] << (8ULL * (valueSize - 1 - i));
            }
            else
            {
                value |= (BEu64)value_[i] << (8ULL * i);
            }
        }
        else if (value_[i])
        {
            hasHighBytesSet = 1;
            break;
        }
    }
    
    BEs64* enumValues = BEGetEnumMemberValues(runtime, enumIndex);
    BEu32 memberCount = BEGetEnumMemberCount(runtime, enumIndex);
    
    BEbool didWriteToOut = 0;
    
    if (!hasHighBytesSet)
    {
        BEbool exactMatchNumber = 0;
        
        for (BEu32 memberIndex = 0; memberIndex < memberCount; ++memberIndex)
        {
            if (enumValues[memberIndex] == value)
            {
                exactMatchNumber = memberIndex + 1;
                break;
            }
        }
        
        if (exactMatchNumber)
        {
            char* exactMatchName = BEGetEnumMemberName(runtime, size, enumIndex, exactMatchNumber - 1);
            if (*size <= capacity)
            {
                memcpy(out, exactMatchName, *size);
            }
            else
            {
                memcpy(out, exactMatchName, capacity);
            }
            
            didWriteToOut = 1;
        }
    }
    
#define TryWrite(chr) if (*size < capacity) out[(*size)++] = (chr)
    
    if (!didWriteToOut)
    {
        // NOTE(Jens): Interpret the enum as a bitset, print with | separators.
        
        if (value == 0 && !hasHighBytesSet)
        {
            TryWrite('0');
        }
        else
        {
            for (BEu32 memberIndex = 0; memberIndex < memberCount && *size < capacity; ++memberIndex)
            {
                if (enumValues[memberIndex] && (value & enumValues[memberIndex]) == enumValues[memberIndex])
                {
                    value &= ~enumValues[memberIndex];
                    BEu32 nameSize;
                    char* memberName = BEGetEnumMemberName(runtime, &nameSize, enumIndex, memberIndex);
                    for (BEu32 i = 0; i < nameSize && *size < capacity; ++i)
                    {
                        TryWrite(memberName[i]);
                    }
                    
                    if (value != 0 || hasHighBytesSet)
                    {
                        TryWrite('|');
                    }
                    
                    if (value == 0)
                    {
                        break;
                    }
                }
            }
            
            BEbool mayWriteZeros = 0;
            
            if (hasHighBytesSet)
            {
                for (BEu64 i = sizeof(value); i < valueSize && *size < capacity; ++i)
                {
                    BEu64 byteIndex = i;
                    if (!swapByteOrder)
                    {
                        byteIndex = valueSize - 1 - i;
                    }
                    
                    BEu8 byteValue = value_[byteIndex];
                    
                    BEu8 low = (byteValue & 0x0f);
                    BEu8 high = (byteValue & 0xf0) >> 4;
                    
                    if (high || mayWriteZeros)
                    {
                        mayWriteZeros = 1;
                        if (high < 10)
                        {
                            TryWrite('0' + high);
                        }
                        else
                        {
                            TryWrite('A' + (high - 10));
                        }
                    }
                    
                    if (low || mayWriteZeros)
                    {
                        mayWriteZeros = 1;
                        if (low < 10)
                        {
                            TryWrite('0' + low);
                        }
                        else
                        {
                            TryWrite('A' + (low - 10));
                        }
                    }
                }
            }
            
            if (value)
            {
                for (BEu64 i = 0; i < sizeof(value); ++i)
                {
                    BEu64 byteIndex = sizeof(value) - 1 - i;
                    BEu64 byteValue = (value >> (8ULL * byteIndex)) & 0xff;
                    
                    BEu8 low = (byteValue & 0x0f);
                    BEu8 high = (byteValue & 0xf0) >> 4;
                    
                    if (high || mayWriteZeros)
                    {
                        mayWriteZeros = 1;
                        if (high < 10)
                        {
                            TryWrite('0' + high);
                        }
                        else
                        {
                            TryWrite('A' + (high - 10));
                        }
                    }
                    
                    if (low || mayWriteZeros)
                    {
                        mayWriteZeros = 1;
                        if (low < 10)
                        {
                            TryWrite('0' + low);
                        }
                        else
                        {
                            TryWrite('A' + (low - 10));
                        }
                    }
                }
            }
            
            if (value || hasHighBytesSet)
            {
                TryWrite('h');
            }
        }
    }
    
#undef TryWrite
    
    ShortenStringIfNeeded(size, capacity, out);
}

static void WriteValue(BEu32* size, BEu32 capacity, char* out, BEbool isBigEndian, BE_ScalarBaseType baseType, BE_ScalarDisplayType displayType, BEu64 valueSize, BEu8* value)
{
    BEbool swapByteOrder = (isBigEndian == HostIsLittleEndian());
    
    if (baseType == BE_ScalarBaseType_string)
    {
        *size = StringToString(capacity, out, valueSize, value);
    }
    else if (baseType == BE_ScalarBaseType_f)
    {
        *size = FloatToString(capacity, out, swapByteOrder, valueSize, value);
    }
    else
    {
        char padding = ' ';
        DisplayFlags flags = 0;
        
        switch (baseType)
        {
            case BE_ScalarBaseType_u:
            case BE_ScalarBaseType_s:
            {
                flags |= DisplayFlag_InDisplayOrder;
                flags |= DisplayFlag_AddSuffix;
                flags |= DisplayFlag_LowerCase;
            } break;
            
            case BE_ScalarBaseType_raw:
            {
                padding = 0;
                flags |= DisplayFlag_AddPrefix;
                flags |= DisplayFlag_AddLeadingZeros;
            } break;
        }
        
        switch (displayType)
        {
            case BE_ScalarDisplayType_decimal:
            {
                *size = DecimalToString(capacity, out, swapByteOrder, valueSize, value, padding, (baseType == BE_ScalarBaseType_s));
            } break;
            
            case BE_ScalarDisplayType_binary:
            {
                *size = BinaryToString(capacity, out, swapByteOrder, valueSize, value, flags, padding);
            } break;
            
            case BE_ScalarDisplayType_octal:
            {
                // TODO
            } break;
            
            case BE_ScalarDisplayType_hex:
            {
                *size = HexToString(capacity, out, swapByteOrder, valueSize, value, flags, padding);
            } break;
        }
    }
    
    ShortenStringIfNeeded(size, capacity, out);
}

static void PrependNamePrefixes(NamePrefix* prefixes, BEu32* size, BEu32 capacity, char* str)
{
    BEu32 bytesLeft = capacity - *size;
    BEu32 bytesRequired = 0;
    
    for (NamePrefix* it = prefixes; it && it->size; it = it->prev)
    {
        bytesRequired += it->size + 1;
        
        if (it->arrayIndexPlusOne)
        {
            bytesRequired += 2;
            bytesRequired += CountDecimalDigits(it->arrayIndexPlusOne - 1);
        }
    }
    
    if (bytesRequired <= bytesLeft)
    {
        memmove(str + bytesRequired, str, *size);
        char* out = str + bytesRequired;
        
        for (NamePrefix* it = prefixes; it && it->size; it = it->prev)
        {
            *--out = '.';
            if (it->arrayIndexPlusOne)
            {
                *--out = ']';
                BEu32 arrayIndex = it->arrayIndexPlusOne - 1;
                
                if (!arrayIndex)
                {
                    *--out = '0';
                }
                else
                {
                    while (arrayIndex)
                    {
                        char c = '0' + (arrayIndex % 10);
                        *--out = c;
                        arrayIndex /= 10;
                    }
                }
                
                *--out = '[';
            }
            
            memcpy(out - it->size, it->str, it->size);
            out -= it->size;
        }
        
        assert(out == str);
    }
    else
    {
        memmove(str + bytesLeft, str, *size);
        char* out = str + bytesLeft;
        
        for (NamePrefix* it = prefixes; it && it->size && bytesLeft; it = it->prev)
        {
            *--out = '.';
            --bytesLeft;
            
            if (it->arrayIndexPlusOne)
            {
                if (bytesLeft)
                {
                    *--out = ']';
                    --bytesLeft;
                }
                
                BEu32 arrayIndex = it->arrayIndexPlusOne - 1;
                
                if (!arrayIndex)
                {
                    if (bytesLeft)
                    {
                        *--out = '0';
                        --bytesLeft;
                    }
                }
                else
                {
                    while (arrayIndex && bytesLeft)
                    {
                        char c = '0' + (arrayIndex % 10);
                        *--out = c;
                        --bytesLeft;
                        arrayIndex /= 10;
                    }
                }
                
                if (bytesLeft)
                {
                    *--out = '[';
                    --bytesLeft;
                }
            }
            
            BEu32 sizeToCopy = it->size;
            if (sizeToCopy > bytesLeft)
            {
                sizeToCopy = bytesLeft;
            }
            
            if (sizeToCopy)
            {
                bytesLeft -= sizeToCopy;
                memcpy(out - sizeToCopy, it->str + it->size - sizeToCopy, sizeToCopy);
                out -= sizeToCopy;
            }
        }
        
        assert(out == str);
        
        str[0] = str[1] = str[2] = '.';
    }
    
    *size += bytesRequired;
}

static void AddMember(ColumnLayout* layout, BERuntime* runtime, BEu8* binaryFile, BEu32 structIndex, BEu32 memberIndex, BEu64 address, BEu64 size, BEu64 arrayCount, BEbool isBigEndian, BEu32 enumIndex, BE_ScalarBaseType baseType, BE_ScalarDisplayType displayType, BEu64 appearIndex)
{
    if (arrayCount)
    {
        BEu32 extraSpaceRequired = 2 + CountDecimalDigits(arrayCount - 1);
        
        BEu32 nameSize;
        char name[sizeof(((LineListNode*)0)->member.name)];
        
        char* rawName = BEGetStructMemberName(runtime, &nameSize, structIndex, memberIndex);
        if (nameSize <= sizeof(name) - extraSpaceRequired)
        {
            memcpy(name, rawName, nameSize);
        }
        else
        {
            memcpy(name, rawName, sizeof(name) - extraSpaceRequired);
        }
        
        if (nameSize < sizeof(name) - 1 && appearIndex)
        {
            name[nameSize++] = '#';
            nameSize += DecimalToString(nameSize - sizeof(name), name + nameSize, 0, sizeof(appearIndex), (BEu8*)&appearIndex, ' ', 0);
        }
        
        ShortenStringIfNeeded(&nameSize, sizeof(name) - extraSpaceRequired, name);
        PrependNamePrefixes(layout->prefixes, &nameSize, sizeof(name) - extraSpaceRequired, name);
        
        if (nameSize + extraSpaceRequired > layout->maxNameSize)
        {
            layout->maxNameSize = nameSize + extraSpaceRequired;
        }
        
        for (BEu64 i = 0; i < arrayCount; ++i)
        {
            LineListNode* newNode = BEAlloc(&layout->allocator, LineListNode);
            if (layout->head)
            {
                layout->tail = layout->tail->next = newNode;
            }
            else
            {
                layout->tail = layout->head = newNode;
            }
            
            newNode->next = 0;
            newNode->type = LineType_Member;
            newNode->member.address = address + i * size;
            newNode->member.nameSize = nameSize;
            newNode->member.valueSize = 0;
            newNode->arrayCount = arrayCount;
            newNode->arrayIndex = i;
            memcpy(newNode->member.name, name, nameSize);
            
            if (enumIndex != BEInvalidEnumIndex)
            {
                WriteEnumValue(&newNode->member.valueSize, sizeof(newNode->member.value), newNode->member.value, runtime, isBigEndian, enumIndex, size, binaryFile + address + size * i);
            }
            else
            {
                WriteValue(&newNode->member.valueSize, sizeof(newNode->member.value), newNode->member.value, isBigEndian, baseType, displayType, size, binaryFile + address + size * i);
            }
            
            ShortenStringIfNeeded(&newNode->member.valueSize, sizeof(newNode->member.value), newNode->member.value);
            
            if (newNode->member.valueSize > layout->maxValueSize)
            {
                layout->maxValueSize = newNode->member.valueSize;
            }
        }
    }
    else
    {
        LineListNode* newNode = BEAlloc(&layout->allocator, LineListNode);
        if (layout->head)
        {
            layout->tail = layout->tail->next = newNode;
        }
        else
        {
            layout->tail = layout->head = newNode;
        }
        
        newNode->next = 0;
        newNode->arrayCount = 0;
        newNode->arrayIndex = 0;
        newNode->type = LineType_Member;
        newNode->member.address = address;
        newNode->member.valueSize = 0;
        
        char* rawName = BEGetStructMemberName(runtime, &newNode->member.nameSize, structIndex, memberIndex);
        if (newNode->member.nameSize <= sizeof(newNode->member.name))
        {
            memcpy(newNode->member.name, rawName, newNode->member.nameSize);
        }
        else
        {
            memcpy(newNode->member.name, rawName, sizeof(newNode->member.name));
        }
        
        if (newNode->member.nameSize < sizeof(newNode->member.name) - 1 && appearIndex)
        {
            newNode->member.name[newNode->member.nameSize++] = '#';
            newNode->member.nameSize += DecimalToString(newNode->member.nameSize - sizeof(newNode->member.name), newNode->member.name + newNode->member.nameSize, 0, sizeof(appearIndex), (BEu8*)&appearIndex, ' ', 0);
        }
        
        ShortenStringIfNeeded(&newNode->member.nameSize, sizeof(newNode->member.name), newNode->member.name);
        PrependNamePrefixes(layout->prefixes, &newNode->member.nameSize, sizeof(newNode->member.name), newNode->member.name);
        
        if (newNode->member.nameSize > layout->maxNameSize)
        {
            layout->maxNameSize = newNode->member.nameSize;
        }
        
        if (enumIndex != BEInvalidEnumIndex)
        {
            WriteEnumValue(&newNode->member.valueSize, sizeof(newNode->member.value), newNode->member.value, runtime, isBigEndian, enumIndex, size, binaryFile + address);
        }
        else
        {
            WriteValue(&newNode->member.valueSize, sizeof(newNode->member.value), newNode->member.value, isBigEndian, baseType, displayType, size, binaryFile + address);
        }
        
        ShortenStringIfNeeded(&newNode->member.valueSize, sizeof(newNode->member.value), newNode->member.value);
        
        if (newNode->member.valueSize > layout->maxValueSize)
        {
            layout->maxValueSize = newNode->member.valueSize;
        }
    }
}

static void AddNestedMember(ColumnLayout* layout, BERuntime* runtime, BEu32 structIndex, BEu32 arrayIndex, BEu32 parentStructIndex, BEu32 memberIndex, BEu32 arrayCount, BEu64 appearIndex)
{
    LineListNode* node = BEAlloc(&layout->allocator, LineListNode);
    if (layout->head)
    {
        layout->tail = layout->tail->next = node;
    }
    else
    {
        layout->tail = layout->head = node;
    }
    
    node->next = 0;
    node->type = LineType_StructStart;
    node->structType.typeNameSize = 0;
    node->structType.memberNameSize = 0;
    node->arrayCount = arrayCount;
    node->arrayIndex = arrayIndex;
    
    {
        char* rawName = BEGetStructName(runtime, &node->structType.typeNameSize, structIndex);
        if (node->structType.typeNameSize <= sizeof(node->structType.typeName))
        {
            memcpy(node->structType.typeName, rawName, node->structType.typeNameSize);
        }
        else
        {
            memcpy(node->structType.typeName, rawName, sizeof(node->structType.typeName));
        }
        ShortenStringIfNeeded(&node->structType.typeNameSize, sizeof(node->structType.typeName), node->structType.typeName);
    }
    
    if (parentStructIndex != BEInvalidParentStructIndex)
    {
        BEu32 extraSpaceRequired = 0;
        if (arrayCount)
        {
            // NOTE(Jens): Reserve space for array index '[   XX]'
            extraSpaceRequired = 2 + CountDecimalDigits(arrayCount - 1);
        }
        
        char* rawName = BEGetStructMemberName(runtime, &node->structType.memberNameSize, parentStructIndex, memberIndex);
        if (node->structType.memberNameSize <= sizeof(node->structType.memberName) - extraSpaceRequired)
        {
            memcpy(node->structType.memberName, rawName, node->structType.memberNameSize);
        }
        else
        {
            memcpy(node->structType.memberName, rawName, sizeof(node->structType.memberName) - extraSpaceRequired);
        }
        
        if (node->structType.memberNameSize < sizeof(node->structType.memberName) - 1 && appearIndex)
        {
            node->structType.memberName[node->structType.memberNameSize++] = '#';
            node->structType.memberNameSize += DecimalToString(node->structType.memberNameSize - sizeof(node->structType.memberName), node->structType.memberName + node->structType.memberNameSize, 0, sizeof(appearIndex), (BEu8*)&appearIndex, ' ', 0);
        }
        
        ShortenStringIfNeeded(&node->structType.memberNameSize, sizeof(node->structType.memberName) - extraSpaceRequired, node->structType.memberName);
    }
}

static LineListNode* AddPrintWithoutValue(ColumnLayout* layout, BEu32 msgSize, char* msg)
{
    LineListNode* node = BEAlloc(&layout->allocator, LineListNode);
    if (layout->head)
    {
        layout->tail = layout->tail->next = node;
    }
    else
    {
        layout->tail = layout->head = node;
    }
    
    node->next = 0;
    node->type = LineType_Print;
    node->print.valueSize = 0;
    node->print.messageSize = msgSize;
    
    if (msgSize < sizeof(node->print.message))
    {
        memcpy(node->print.message, msg, msgSize);
    }
    else
    {
        memcpy(node->print.message, msg, sizeof(node->print.message));
    }
    
    ShortenStringIfNeeded(&node->print.messageSize, sizeof(node->print.message), node->print.message);
    
    return node;
}

static void AddPrintWithValue(ColumnLayout* layout, BEu32 msgSize, char* msg, BEs64 value)
{
    LineListNode* node = AddPrintWithoutValue(layout, msgSize, msg);
    
    node->print.valueSize = DecimalToString(sizeof(node->print.value), node->print.value, 0, sizeof(value), (BEu8*)&value, ' ', 1);
    ShortenStringIfNeeded(&node->print.valueSize, sizeof(node->print.value), node->print.value);
}

static void FlushLayout(FILE* out, ColumnLayout* layout, BEu32 addressHexDecimalCount, char* linePrefix)
{
    BEu32 addressBufCapacity = addressHexDecimalCount + (addressHexDecimalCount - 1) / 2 + 2;
    char* addressBuf = BEAllocArray(&layout->allocator, char, addressBufCapacity);
    
    LineListNode* prevLine = 0;
    for (LineListNode* line = layout->head; line; line = line->next)
    {
        switch (line->type)
        {
            case LineType_Member:
            {
                if (linePrefix)
                {
                    fputs(linePrefix, out);
                }
                
                BEu32 addressLen = HexToString(addressBufCapacity, addressBuf, 0, (addressHexDecimalCount + 1) / 2, (BEu8*)&line->member.address, DisplayFlag_InDisplayOrder | DisplayFlag_AddLeadingZeros | DisplayFlag_Hex_hSuffix, ' ');
                assert(addressLen <= addressBufCapacity);
                
                for (BEu32 i = 0; i < addressBufCapacity - addressLen; ++i)
                {
                    fputc(' ', out);
                }
                SetTerminalTextColor(stdout, Color_Black | Color_Bright);
                fprintf(out, "%.*s ", (int)addressLen, addressBuf);
                SetTerminalTextColor(stdout, Color_Default);
                
                if (line->arrayCount)
                {
                    BEu32 arrayIndexDigitCount = CountDecimalDigits(line->arrayCount - 1);
                    fprintf(out, "%*.*s[%*llu] ", 
                            (int)(layout->maxNameSize - (2 + arrayIndexDigitCount)), (int)line->member.nameSize, line->member.name,
                            (int)arrayIndexDigitCount, line->arrayIndex);
                }
                else
                {
                    fprintf(out, "%*.*s ", (int)layout->maxNameSize, (int)line->member.nameSize, line->member.name);
                }
                
                fprintf(out, "%*.*s\n", (int)layout->maxValueSize, (int)line->member.valueSize, line->member.value);
            } break;
            
            case LineType_Print:
            {
                if (linePrefix)
                {
                    fputs(linePrefix, out);
                }
                
                for (BEu32 i = 0; i < addressBufCapacity; ++i)
                {
                    fputc(' ', out);
                }
                
                SetTerminalTextColor(stdout, Color_Green);
                
                fprintf(out, " %.*s ", (int)line->print.messageSize, line->print.message);
                if (line->print.valueSize)
                {
                    fprintf(out, "%.*s\n", (int)line->print.valueSize, line->print.value);
                }
                else
                {
                    fputc('\n', out);
                }
                
                SetTerminalTextColor(stdout, Color_Default);
            } break;
            
            case LineType_StructStart:
            {
                if (prevLine && prevLine->type != LineType_StructStart)
                {
                    fputc('\n', out);
                }
                
                if (line->structType.memberNameSize)
                {
                    if (line->arrayCount)
                    {
                        fprintf(out, "%.*s (.%.*s[%*llu])\n", 
                                (int)line->structType.typeNameSize, line->structType.typeName,
                                (int)line->structType.memberNameSize, line->structType.memberName,
                                (int)CountDecimalDigits(line->arrayCount), line->arrayIndex);
                    }
                    else
                    {
                        fprintf(out, "%.*s (.%.*s)\n", 
                                (int)line->structType.typeNameSize, line->structType.typeName,
                                (int)line->structType.memberNameSize, line->structType.memberName);
                    }
                }
                else
                {
                    // NOTE(Jens): Starting struct without member name, must be layout call.
                    SetTerminalTextColor(stdout, Color_Green);
                    fprintf(out, "%.*s\n", (int)line->structType.typeNameSize, line->structType.typeName);
                    SetTerminalTextColor(stdout, Color_Default);
                }
            } break;
        }
        
        prevLine = line;
    }
    
    DeallocAll(&layout->allocator);
    
    memset(layout, 0, sizeof(*layout));
}

// NOTE(Jens): Not used atm.
static void WriteOBJ(FILE* out, Plot* plot)
{
    PlotData* xs = plot->lines.xs.head;
    PlotData* ys = plot->lines.ys.head;
    
    fprintf(out, "# Vertices:\n");
    
    BEu32 localIndex = 0;
    for (BEu32 i = 0; i < plot->lines.ys.count; ++i)
    {
        BEf64 x;
        BEf64 y;
        
        if (!plot->lines.xs.count)
        {
            x = (BEf64)i;
        }
        else
        {
            x = xs->values[localIndex];
        }
        
        y = ys->values[localIndex];
        
        fprintf(out, "v %f %f\n", x, y);
        
        if (++localIndex == SamplesPerPlotData)
        {
            localIndex = 0;
            
            if (xs)
            {
                xs = xs->next;
            }
            
            ys = ys->next;
        }
    }
    
    fprintf(out, "\nl");
    for (BEu32 i = 0; i < plot->lines.ys.count; ++i)
    {
        fprintf(out, " %i", i);
    }
    fprintf(out, "\n");
}

static void WriteCommaSeparatedSamples(FILE* out, PlotSamples* samples, char* indent)
{
    PlotData* data = samples->head;
    
    BEu32 columnCount = 8;
    
    BEu32 localIndex = 0;
    for (BEu32 i = 0; i < samples->count; ++i)
    {
        BEf64 value = data->values[localIndex];
        
        if (i && i % columnCount == 0)
        {
            fputs("\n", out);
            fputs(indent, out);
        }
        
        if (i < samples->count - 1)
        {
            fprintf(out, "%e, ", value);
        }
        else
        {
            fprintf(out, "%e", value);
        }
        
        if (++localIndex == SamplesPerPlotData)
        {
            localIndex = 0;
            data = data->next;
        }
    }
}

static char* const HTML_Start = 
"<!DOCTYPE html>\n"
"<html style='height:100%;margin:0;'>\n"
"<body style='height:100%;margin:0;'>\n"
"<canvas id='plot-overview' style='position:absolute;left:0;top:0;z-index:0;width:100%;height:10%;display:block;'></canvas>\n"
"<div style='position:relative;height:90%;top:10%;margin:0;'>\n"
"  <canvas id='plot-canvas' style='position:absolute;left:0;top:0;z-index:0;width:100%;height:100%;display:block;'></canvas>\n"
"  <canvas id='plot-overlay'style='position:absolute;left:0;top:0;z-index:1;background-color:transparent;pointer-events:none;width:100%;height:100%;display:block;'></canvas>\n"
"</div>\n"
"<script>\n"
"\n"
"'use strict';\n"
"\n";

static char* const HTML_End =
"</script>\n"
"</html>\n"
"\n";

static char* const HTML_CanvasSetup =
"const canvas = document.getElementById('plot-canvas');\n"
"const ctx = canvas.getContext('2d');\n"
"\n"
"const overlayCanvas = document.getElementById('plot-overlay');\n"
"const overlayCtx = overlayCanvas.getContext('2d');\n"
"\n"
"const overviewCanvas = document.getElementById('plot-overview');\n"
"const overviewCtx = overviewCanvas.getContext('2d');\n"
"\n"
"const plotResizeObserver = new ResizeObserver(() => {\n"
"    canvas.width = Math.round(canvas.clientWidth * devicePixelRatio);\n"
"    canvas.height = Math.round(canvas.clientHeight * devicePixelRatio);\n"
"\n"
"    drawCanvas();\n"
"});\n"
"plotResizeObserver.observe(canvas);\n"
"\n"
"const overlayResizeObserver = new ResizeObserver(() => {\n"
"    overlayCanvas.width = Math.round(overlayCanvas.clientWidth * devicePixelRatio);\n"
"    overlayCanvas.height = Math.round(overlayCanvas.clientHeight * devicePixelRatio);\n"
"\n"
"    drawOverlay();\n"
"});\n"
"overlayResizeObserver.observe(overlayCanvas);\n"
"\n"
"const overviewResizeObserver = new ResizeObserver(() => {\n"
"    overviewCanvas.width = Math.round(overviewCanvas.clientWidth * devicePixelRatio);\n"
"    overviewCanvas.height = Math.round(overviewCanvas.clientHeight * devicePixelRatio);\n"
"\n"
"    drawOverview();\n"
"});\n"
"overviewResizeObserver.observe(overviewCanvas);\n"
"\n";

static char* const HTML_GlobalState =
"const knotRadius = 4;\n"
"const paddingH = 10;\n"
"const paddingV = 5;\n"
"const numberFormat = new Intl.NumberFormat('en-US')\n"
"\n"
"const panning = {\n"
"    active: false,\n"
"    prevX: 0,\n"
"    prevY: 0\n"
"};\n"
"\n"
"const overview = {\n"
"    bucketCount: 0,\n"
"    maxValues01: null,\n"
"    minValues01: null,\n"
"    hoveredCanvasX: null,\n"
"    hoveredCanvasY: null,\n"
"    isPanning: false\n"
"};\n"
"\n"
"const theme = {\n"
"    backgroundColor: 'white',\n"
"    gridColor: '#1E1E1E1E',\n"
"    curveColor: 'black',\n"
"    textColor: 'black',\n"
"    textBackgroundColor: '#FFFFFFDC',\n"
"    highlightedColor: '#ACAC07FF',\n"
"    overviewCurveColorR: 0x11,\n"
"    overviewCurveColorG: 0x11,\n"
"    overviewCurveColorB: 0x11,\n"
"    overviewBackgroundColor: '#EEEEEEFF',\n"
"    overviewAreaColor: '#62627C77'\n"
"};\n"
"\n"
"if (window.matchMedia)\n"
"{\n"
"    if (window.matchMedia('(prefers-color-scheme: dark)').matches)\n"
"    {\n"
"        theme.backgroundColor = 'black';\n"
"        theme.gridColor = '#FEFEFE1E';\n"
"        theme.curveColor = '#DEDEDEFF';\n"
"        theme.textColor = 'white';\n"
"        theme.textBackgroundColor = '#000000DC';\n"
"        theme.highlightedColor = '#CCCC07FF';\n"
"        theme.overviewCurveColorR = 0xCE;\n"
"        theme.overviewCurveColorG = 0xCE;\n"
"        theme.overviewCurveColorB = 0xCE;\n"
"        theme.overviewBackgroundColor = '#313131FF';\n"
"        theme.overviewAreaColor = '#42427A77';\n"
"    }\n"
"}\n"
"\n"
"theme.overviewCurveColor = 'rgb(' + theme.overviewCurveColorR + ', ' + theme.overviewCurveColorG + ', ' + theme.overviewCurveColorB + ', 100%)';\n"
"\n"
"canvas.style.backgroundColor = theme.backgroundColor;\n"
"overviewCanvas.style.backgroundColor = theme.overviewBackgroundColor;\n"
"\n"
"let renderedPlot = null;\n"
"\n";

static char* const HTML_CanvasEventHandlers_YofX =
"canvas.onmousedown = function(event)\n"
"{\n"
"    panning.active = true;\n"
"    panning.prevX = event.screenX;\n"
"    panning.prevY = event.screenY;\n"
"};\n"
"\n"
"canvas.onmousemove = function(event)\n"
"{\n"
"    if (panning.active)\n"
"    {\n"
"        const canvasToViewX = view.dim.width / canvas.width;\n"
"        const dx = canvasToViewX * (event.screenX - panning.prevX);\n"
"        view.bottomLeft.x -= dx;\n"
"        \n"
"        panning.prevX = event.screenX;\n"
"        panning.prevY = event.screenY;\n"
"        \n"
"        drawCanvas();\n"
"        drawOverview();\n"
"    }\n"
"    \n"
"    {\n"
"        const canvasToViewX = view.dim.width / canvas.width;\n"
"        const canvasToViewY = view.dim.height / canvas.height;\n"
"        \n"
"        const x = canvasToViewX * event.offsetX + view.bottomLeft.x;\n"
"        const y = canvasToViewY * (canvas.height - event.offsetY) + view.bottomLeft.y;\n"
"        \n"
"        view.highlightedPos = { x: x, y: y };\n"
"    }\n"
"    \n"
"    drawOverlay();\n"
"};\n"
"\n"
"canvas.onmouseup = function(event)\n"
"{\n"
"    panning.active = false;\n"
"};\n"
"\n"
"canvas.onmouseleave = function(event)\n"
"{\n"
"    panning.active = false;\n"
"    view.highlightedPos = null;\n"
"    drawOverlay();\n"
"};\n"
"\n"
"overviewCanvas.onmousedown = function(event)\n"
"{\n"
"    overview.isPanning = true;\n"
"    let centerX = getBucketX(event.offsetX / overviewCanvas.width);\n"
"    view.bottomLeft.x = centerX - 0.5 * view.dim.width;\n"
"    drawCanvas();\n"
"};\n"
"\n"
"overviewCanvas.onmouseup = function(event)\n"
"{\n"
"    overview.isPanning = false;\n"
"};\n"
"\n"
"overviewCanvas.onmousemove = function(event)\n"
"{\n"
"    overview.hoveredCanvasX = event.offsetX;\n"
"    \n"
"    if (overview.isPanning)\n"
"    {\n"
"        let centerX = getBucketX(event.offsetX / overviewCanvas.width);\n"
"        view.bottomLeft.x = centerX - 0.5 * view.dim.width;\n"
"        drawCanvas();\n"
"    }\n"
"    \n"
"    drawOverview();\n"
"};\n"
"\n"
"overviewCanvas.onmouseleave = function(event)\n"
"{\n"
"    overview.hoveredCanvasX = null;\n"
"    overview.isPanning = false;\n"
"    drawOverview();\n"
"};\n"
"\n"
"window.addEventListener('wheel', function(event)\n"
"{\n"
"    view.dim.width += scrollSensitivity * event.deltaY;\n"
"    if (view.dim.width < minViewWidth)\n"
"    {\n"
"        view.dim.width = minViewWidth;\n"
"    }\n"
"    else if (view.dim.width > plot.ys.length - 1)\n"
"    {\n"
"        view.dim.width = plot.ys.length - 1;\n"
"    }\n"
"    \n"
"    view.highlightedPos = null;\n"
"    \n"
"    drawCanvas();\n"
"    drawOverview();\n"
"    drawOverlay();\n"
"}, { passive: true });\n"
"\n";

static char* const HTML_CanvasEventHandlers_Curve =
"canvas.onmousedown = function(event)\n"
"{\n"
"    panning.active = true;\n"
"    panning.prevX = event.screenX;\n"
"    panning.prevY = event.screenY;\n"
"};\n"
"\n"
"canvas.onmousemove = function(event)\n"
"{\n"
"    if (panning.active)\n"
"    {\n"
"        const canvasToViewX = view.dim.width / canvas.width;\n"
"        const canvasToViewY = view.dim.height / canvas.height;\n"
"        \n"
"        const dx = canvasToViewX * (event.screenX - panning.prevX);\n"
"        const dy = -canvasToViewY * (event.screenY - panning.prevY);\n"
"        \n"
"        view.bottomLeft.x -= dx;\n"
"        view.bottomLeft.y -= dy;\n"
"        \n"
"        panning.prevX = event.screenX;\n"
"        panning.prevY = event.screenY;\n"
"        \n"
"        drawCanvas();\n"
"        drawOverview();\n"
"    }\n"
"    \n"
"    {\n"
"        const canvasToViewX = view.dim.width / canvas.width;\n"
"        const canvasToViewY = view.dim.height / canvas.height;\n"
"        \n"
"        const x = canvasToViewX * event.offsetX + view.bottomLeft.x;\n"
"        const y = canvasToViewY * (canvas.height - event.offsetY) + view.bottomLeft.y;\n"
"        \n"
"        view.highlightedPos = { x: x, y: y };\n"
"    }\n"
"    \n"
"    drawOverlay();\n"
"};\n"
"\n"
"canvas.onmouseup = function(event)\n"
"{\n"
"    panning.active = false;\n"
"};\n"
"\n"
"canvas.onmouseleave = function(event)\n"
"{\n"
"    panning.active = false;\n"
"    view.highlightedPos = null;\n"
"    drawOverlay();\n"
"};\n"
"\n"
"overviewCanvas.onmousedown = function(event)\n"
"{\n"
"    overview.isPanning = true;\n"
"    let centerX = plot.minX + (plot.maxX - plot.minX) * (event.offsetX / overviewCanvas.width);\n"
"    let centerY = plot.minY + (plot.maxY - plot.minY) * (1 - event.offsetY / overviewCanvas.height);\n"
"    view.bottomLeft.x = centerX - 0.5 * view.dim.width;\n"
"    view.bottomLeft.y = centerY - 0.5 * view.dim.height;\n"
"    drawCanvas();\n"
"};\n"
"\n"
"overviewCanvas.onmouseup = function(event)\n"
"{\n"
"    overview.isPanning = false;\n"
"};\n"
"\n"
"overviewCanvas.onmousemove = function(event)\n"
"{\n"
"    overview.hoveredCanvasX = event.offsetX;\n"
"    overview.hoveredCanvasY = event.offsetY;\n"
"    \n"
"    if (overview.isPanning)\n"
"    {\n"
"        let centerX = plot.minX + (plot.maxX - plot.minX) * (event.offsetX / overviewCanvas.width);\n"
"        let centerY = plot.minY + (plot.maxY - plot.minY) * (1 - event.offsetY / overviewCanvas.height);\n"
"        view.bottomLeft.x = centerX - 0.5 * view.dim.width;\n"
"        view.bottomLeft.y = centerY - 0.5 * view.dim.height;\n"
"        drawCanvas();\n"
"    }\n"
"    \n"
"    drawOverview();\n"
"};\n"
"\n"
"overviewCanvas.onmouseleave = function(event)\n"
"{\n"
"    overview.hoveredCanvasX = null;\n"
"    overview.hoveredCanvasY = null;\n"
"    overview.isPanning = false;\n"
"    drawOverview();\n"
"};\n"
"\n"
"window.addEventListener('wheel', function(event)\n"
"{\n"
"    const centerX = view.bottomLeft.x + 0.5 * view.dim.width;\n"
"    const centerY = view.bottomLeft.y + 0.5 * view.dim.height;\n"
"    \n"
"    view.dim.width += scrollSensitivityH * event.deltaY;\n"
"    view.dim.height += scrollSensitivityV * event.deltaY;\n"
"    if (view.dim.width < minViewWidth)\n"
"    {\n"
"        view.dim.width = minViewWidth;\n"
"    }\n"
"    if (view.dim.height < minViewHeight)\n"
"    {\n"
"        view.dim.height = minViewHeight;\n"
"    }\n"
"    \n"
"    view.bottomLeft.x = centerX - 0.5 * view.dim.width;\n"
"    view.bottomLeft.y = centerY - 0.5 * view.dim.height;\n"
"    \n"
"    view.highlightedPos = null;\n"
"    \n"
"    drawCanvas();\n"
"    drawOverview();\n"
"    drawOverlay();\n"
"}, { passive: true });\n"
"\n";

static char* const HTML_Utilities =
"function roundToGoodLookingGridNumber(v)\n"
"{\n"
"    let rVal = 0;\n"
"\n"
"    if (v > 1)\n"
"    {\n"
"        let candidate = 1;\n"
"\n"
"        while (10 * candidate < v)\n"
"        {\n"
"            candidate *= 10;\n"
"        }\n"
"        \n"
"        // NOTE(Jens): candidate <= v <= 10 * candidate\n"
"        \n"
"        if (v >= 8 * candidate)\n"
"        {\n"
"            candidate *= 8;\n"
"        }\n"
"        else if (v >= 6 * candidate)\n"
"        {\n"
"            candidate *= 6;\n"
"        }\n"
"        else if (v >= 4 * candidate)\n"
"        {\n"
"            candidate *= 4;\n"
"        }\n"
"        else if (v >= 2 * candidate)\n"
"        {\n"
"            candidate *= 2;\n"
"        }\n"
"        \n"
"        rVal = candidate;\n"
"    }\n"
"    else if (v > 0)\n"
"    {\n"
"        let candidate = 0.1;\n"
"        \n"
"        while (0.1 * candidate > v)\n"
"        {\n"
"            candidate *= 0.1;\n"
"        }\n"
"        \n"
"        // NOTE(Jens): 0.1 * candidate <= v <= candidate\n"
"        \n"
"        if (v <= candidate / 8)\n"
"        {\n"
"            candidate /= 8;\n"
"        }\n"
"        else if (v <= candidate / 6)\n"
"        {\n"
"            candidate /= 6;\n"
"        }\n"
"        else if (v <= candidate / 4)\n"
"        {\n"
"            candidate /= 4;\n"
"        }\n"
"        else if (v <= 2 * candidate)\n"
"        {\n"
"            candidate /= 2;\n"
"        }\n"
"        \n"
"        if (v < 0.5 * candidate)\n"
"        {\n"
"            candidate *= 0.5;\n"
"        }\n"
"        \n"
"        rVal = candidate;\n"
"    }\n"
"    \n"
"    return rVal;\n"
"}\n"
"\n"
"function drawTextBackground(ctx, str, x, y)\n"
"{\n"
"    const metrics = ctx.measureText(str);\n"
"    ctx.fillRect(\n"
"        x - metrics.actualBoundingBoxLeft,\n"
"        y - metrics.fontBoundingBoxAscent,\n"
"        metrics.actualBoundingBoxRight + metrics.actualBoundingBoxLeft,\n"
"        metrics.fontBoundingBoxDescent + metrics.fontBoundingBoxAscent\n"
"    );\n"
"}\n"
"\n";

static char* const HTML_DrawOverlay_YofX =
"function drawOverlay()\n"
"{\n"
"    overlayCtx.reset();\n"
"    \n"
"    overlayCtx.font = '18px mono';\n"
"    overlayCtx.textAlign = 'center';\n"
"    overlayCtx.textBaseline = 'bottom';\n"
"    \n"
"    if (renderedPlot && view.highlightedPos != null)\n"
"    {\n"
"        const [globalI, xValue] = findClosestSample(view.highlightedPos.x, view.highlightedPos.y);\n"
"        if (renderedPlot.startI <= globalI && globalI < renderedPlot.endI)\n"
"        {\n"
"            overlayCtx.fillStyle = theme.highlightedColor;\n"
"            \n"
"            const i = globalI - renderedPlot.startI;\n"
"            overlayCtx.beginPath();\n"
"            overlayCtx.arc(\n"
"                renderedPlot.points[i].x, \n"
"                renderedPlot.points[i].y, \n"
"                2 * knotRadius, \n"
"                0, \n"
"                Math.PI * 2\n"
"            );\n"
"            \n"
"            overlayCtx.stroke();\n"
"            overlayCtx.fill();\n"
"            \n"
"            overlayCtx.fillStyle = theme.textBackgroundColor;\n"
"            \n"
"            const strX = numberFormat.format(xValue);\n"
"            const strValue = numberFormat.format(plot.ys[globalI]);\n"
"            \n"
"            drawTextBackground(\n"
"                overlayCtx,\n"
"                strX,\n"
"                renderedPlot.points[i].x,\n"
"                overlayCanvas.height - 2 * paddingV\n"
"            );\n"
"            drawTextBackground(\n"
"                overlayCtx,\n"
"                strValue,\n"
"                renderedPlot.points[i].x,\n"
"                renderedPlot.points[i].y - 2.5 * knotRadius\n"
"            );\n"
"            \n"
"            overlayCtx.fillStyle = theme.highlightedColor;\n"
"            overlayCtx.fillText(\n"
"                strX,\n"
"                renderedPlot.points[i].x, \n"
"                overlayCanvas.height - 2 * paddingV\n"
"            );\n"
"            overlayCtx.fillText(\n"
"                strValue,\n"
"                renderedPlot.points[i].x, \n"
"                renderedPlot.points[i].y - 2.5 * knotRadius\n"
"            );\n"
"        }\n"
"    }\n"
"}\n"
"\n";

static char* const HTML_DrawOverlay_Curve =
"function drawOverlay()\n"
"{\n"
"    overlayCtx.reset();\n"
"    \n"
"    overlayCtx.font = '18px mono';\n"
"    \n"
"    if (renderedPlot && view.highlightedPos != null)\n"
"    {\n"
"        const viewToCanvasX = canvas.width / view.dim.width;\n"
"        const viewToCanvasY = canvas.height / view.dim.height;\n"
"        const i = findClosestSample(view.prevHighlight, viewToCanvasX, viewToCanvasY, view.highlightedPos.x, view.highlightedPos.y);\n"
"        let localIndex = null;\n"
"        let localOffset = 0;\n"
"        let x = 0;\n"
"        let y = 0;\n"
"        if (i != null)\n"
"        {\n"
"            localIndex = binarySearch(renderedPlot.indices, i);\n"
"            x = plot.xs[i];\n"
"            y = plot.ys[i];\n"
"            if (localIndex == null)\n"
"            {\n"
"                localIndex = binarySearch(renderedPlot.indices, i - 1);\n"
"                localOffset = 1;\n"
"            }\n"
"        }\n"
"        if (localIndex != null)\n"
"        {\n"
"            const canvasX = renderedPlot.points[2 * localIndex + localOffset].x;\n"
"            const canvasY = renderedPlot.points[2 * localIndex + localOffset].y;\n"
"            overlayCtx.fillStyle = theme.highlightedColor;\n"
"            \n"
"            overlayCtx.beginPath();\n"
"            overlayCtx.arc(\n"
"                canvasX, \n"
"                canvasY, \n"
"                2 * knotRadius, \n"
"                0, \n"
"                Math.PI * 2\n"
"            );\n"
"            \n"
"            overlayCtx.stroke();\n"
"            overlayCtx.fill();\n"
"            \n"
"            overlayCtx.fillStyle = theme.textBackgroundColor;\n"
"            \n"
"            const strX = numberFormat.format(x);\n"
"            const strY = numberFormat.format(y);\n"
"            const strI = numberFormat.format(i);\n"
"            \n"
"            overlayCtx.textAlign = 'center';\n"
"            overlayCtx.textBaseline = 'bottom';\n"
"            drawTextBackground(\n"
"                overlayCtx,\n"
"                strX,\n"
"                canvasX,\n"
"                overlayCanvas.height - 2 * paddingV\n"
"            );\n"
"            \n"
"            overlayCtx.textAlign = 'left';\n"
"            overlayCtx.textBaseline = 'middle';\n"
"            drawTextBackground(\n"
"                overlayCtx,\n"
"                strY,\n"
"                2 * paddingH,\n"
"                canvasY\n"
"            );\n"
"            \n"
"            overlayCtx.textAlign = 'center';\n"
"            overlayCtx.textBaseline = 'top';\n"
"            drawTextBackground(\n"
"                overlayCtx,\n"
"                strI,\n"
"                canvasX,\n"
"                canvasY + 3 * knotRadius\n"
"            );\n"
"            \n"
"            overlayCtx.fillStyle = theme.highlightedColor;\n"
"            \n"
"            overlayCtx.textAlign = 'center';\n"
"            overlayCtx.textBaseline = 'bottom';\n"
"            overlayCtx.fillText(\n"
"                strX,\n"
"                canvasX,\n"
"                overlayCanvas.height - 2 * paddingV\n"
"            );\n"
"            \n"
"            overlayCtx.textAlign = 'left';\n"
"            overlayCtx.textBaseline = 'middle';\n"
"            overlayCtx.fillText(\n"
"                strY,\n"
"                2 * paddingH,\n"
"                canvasY\n"
"            );\n"
"            \n"
"            overlayCtx.textAlign = 'center';\n"
"            overlayCtx.textBaseline = 'top';\n"
"            overlayCtx.fillText(\n"
"                strI,\n"
"                canvasX,\n"
"                canvasY + 3 * knotRadius\n"
"            );\n"
"        }\n"
"    }\n"
"}\n"
"\n";

static char* const HTML_DrawOverview_YofX =
"function drawOverview()\n"
"{\n"
"    overviewCtx.reset();\n"
"    \n"
"    overviewCtx.fillStyle = theme.overviewCurveColor;\n"
"    \n"
"    let targetBucketCount = Math.ceil(0.5 * overviewCanvas.width);\n"
"    targetBucketCount = 10 * Math.floor(targetBucketCount / 10);\n"
"    targetBucketCount = Math.max(10, targetBucketCount);\n"
"    \n"
"    if (targetBucketCount != overview.bucketCount)\n"
"    {\n"
"        overview.bucketCount = targetBucketCount;\n"
"        [overview.minValues01, overview.maxValues01] = bucketPoints(targetBucketCount);\n"
"    }\n"
"    \n"
"    {\n"
"        const bucketWidth = overviewCanvas.width / overview.bucketCount;\n"
"        \n"
"        {\n"
"            let x = 0;\n"
"            overviewCtx.beginPath();\n"
"            overviewCtx.moveTo(x, overviewCanvas.height * overview.minValues01[0]);\n"
"            x += bucketWidth;\n"
"            \n"
"            for (let i = 1; i < overview.bucketCount; ++i)\n"
"            {\n"
"                overviewCtx.lineTo(x, overviewCanvas.height * overview.minValues01[i]);\n"
"                x += bucketWidth;\n"
"            }\n"
"            \n"
"            for (let i = 0; i < overview.bucketCount; ++i)\n"
"            {\n"
"                x -= bucketWidth;\n"
"                overviewCtx.lineTo(x, overviewCanvas.height * overview.maxValues01[overview.bucketCount - 1 - i]);\n"
"            }\n"
"            \n"
"            overviewCtx.fill();\n"
"        }\n"
"    }\n"
"    \n"
"    const minAreaWidth = 5;\n"
"    \n"
"    overviewCtx.fillStyle = theme.overviewAreaColor;\n"
"    \n"
"    let left = overviewCanvas.width * toBucketX(view.bottomLeft.x);\n"
"    let right = overviewCanvas.width * toBucketX(view.bottomLeft.x + view.dim.width);\n"
"    let w = right - left;\n"
"    \n"
"    if (w < minAreaWidth)\n"
"    {\n"
"        w = minAreaWidth;\n"
"        const center = 0.5 * (left + right);\n"
"        left = center - 0.5 * w;\n"
"        right = center + 0.5 * w;\n"
"    }\n"
"    \n"
"    overviewCtx.fillRect(left, 0, w, overviewCanvas.height);\n"
"    \n"
"    if (overview.hoveredCanvasX != null)\n"
"    {\n"
"        overviewCtx.fillRect(overview.hoveredCanvasX - 0.5 * w, 0, w, overviewCanvas.height);\n"
"        \n"
"        overviewCtx.font = '18px mono';\n"
"        overviewCtx.textAlign = 'center';\n"
"        overviewCtx.textBaseline = 'bottom';\n"
"        \n"
"        const x = getBucketX(overview.hoveredCanvasX / overviewCanvas.width);\n"
"        const strX = numberFormat.format(x);\n"
"        \n"
"        overviewCtx.fillStyle = theme.textBackgroundColor;\n"
"        drawTextBackground(\n"
"            overviewCtx,\n"
"            strX,\n"
"            overview.hoveredCanvasX,\n"
"            overviewCanvas.height - 2 * paddingV\n"
"        );\n"
"        \n"
"        overviewCtx.fillStyle = theme.highlightedColor;\n"
"        overviewCtx.fillText(strX, overview.hoveredCanvasX, overviewCanvas.height - 2 * paddingV);\n"
"    }\n"
"}\n"
"\n";

static char* const HTML_DrawOverview_Curve =
"function hasSampleInRegion(quadtree, minX, minY, maxX, maxY)\n"
"{\n"
"    const nodes = [quadtree];\n"
"    while (nodes.length > 0)\n"
"    {\n"
"        const node = nodes.pop();\n"
"        if (node.children === undefined)\n"
"        {\n"
"            for (const index of node.indices)\n"
"            {\n"
"                if (minX <= plot.xs[index] && plot.xs[index] <= maxX && minY <= plot.ys[index] && plot.ys[index] < maxY)\n"
"                {\n"
"                    return true;\n"
"                }\n"
"            }\n"
"        }\n"
"        else\n"
"        {\n"
"            for (const child of node.children)\n"
"            {\n"
"                if (child.minX <= maxX && minX <= child.maxX && child.minY <= maxY && minY <= child.maxY)\n"
"                {\n"
"                    nodes.push(child);\n"
"                }\n"
"            }\n"
"        }\n"
"    }\n"
"    \n"
"    return false;\n"
"}\n"
"\n"
"function drawOverview()\n"
"{\n"
"    overviewCtx.reset();\n"
"    \n"
"    overviewCtx.fillStyle = theme.overviewCurveColor;\n"
"    \n"
"    let bucketCountX = overviewCanvas.width;\n"
"    let bucketCountY = overviewCanvas.height;\n"
"    let cellWidth = (plot.maxX - plot.minX) / bucketCountX;\n"
"    let cellHeight = (plot.maxY - plot.minY) / bucketCountY;\n"
"    \n"
"    if (overview.width === undefined || overviewCanvas.width != overview.width || overviewCanvas.height != overview.height)\n"
"    {\n"
"        overview.cellWidth = cellWidth;\n"
"        const imageArr = new Uint8ClampedArray(4 * overviewCanvas.width * overviewCanvas.height);\n"
"        for (let bucketY = 0; bucketY < bucketCountY; ++bucketY)\n"
"        {\n"
"            const minY = plot.minY + cellHeight * bucketY;\n"
"            const maxY = minY + cellHeight;\n"
"            for (let bucketX = 0; bucketX < bucketCountX; ++bucketX)\n"
"            {\n"
"                const minX = plot.minX + cellWidth * bucketX;\n"
"                const maxX = minX + cellWidth;\n"
"                \n"
"                let sampleCount = 0;\n"
"                if (hasSampleInRegion(quadtree, minX, minY, minX + 0.5 * cellWidth, minY + 0.5 * cellHeight))\n"
"                {\n"
"                    ++sampleCount;\n"
"                }\n"
"                if (hasSampleInRegion(quadtree, minX + 0.5 * cellWidth, minY, maxX, minY + 0.5 * cellHeight))\n"
"                {\n"
"                    ++sampleCount;\n"
"                }\n"
"                if (hasSampleInRegion(quadtree, minX, minY + 0.5 * cellHeight, minX + 0.5 * cellWidth, maxY))\n"
"                {\n"
"                    ++sampleCount;\n"
"                }\n"
"                if (hasSampleInRegion(quadtree, minX + 0.5 * cellWidth, minY + minX + 0.5 * cellHeight, maxX, maxY))\n"
"                {\n"
"                    ++sampleCount;\n"
"                }\n"
"                \n"
"                if (sampleCount > 0)\n"
"                {\n"
"                    imageArr[4 * (overviewCanvas.width * bucketY + bucketX) + 0] = theme.overviewCurveColorR;\n"
"                    imageArr[4 * (overviewCanvas.width * bucketY + bucketX) + 1] = theme.overviewCurveColorG;\n"
"                    imageArr[4 * (overviewCanvas.width * bucketY + bucketX) + 2] = theme.overviewCurveColorB;\n"
"                    imageArr[4 * (overviewCanvas.width * bucketY + bucketX) + 3] = 255 * sampleCount / 4;\n"
"                }\n"
"            }\n"
"        }\n"
"        overview.width = overviewCanvas.width;\n"
"        overview.height = overviewCanvas.height;\n"
"        overview.imageData = new ImageData(imageArr, overviewCanvas.width, overviewCanvas.height);\n"
"    }\n"
"    \n"
"    overviewCtx.putImageData(overview.imageData, 0, 0);\n"
"    \n"
"    const minAreaDim = 5;\n"
"    \n"
"    overviewCtx.fillStyle = theme.overviewAreaColor;\n"
"    \n"
"    let left = overviewCanvas.width * (view.bottomLeft.x - plot.minX) / (plot.maxX - plot.minX);\n"
"    let right = overviewCanvas.width * (view.bottomLeft.x + view.dim.width - plot.minX) / (plot.maxX - plot.minX);\n"
"    let width = right - left;\n"
"    let bottom = overviewCanvas.height * (1 - (view.bottomLeft.y - plot.minY) / (plot.maxY - plot.minY));\n"
"    let top = overviewCanvas.height * (1 - (view.bottomLeft.y + view.dim.height - plot.minY) / (plot.maxY - plot.minY));\n"
"    let height = bottom - top;\n"
"    \n"
"    if (width < minAreaDim)\n"
"    {\n"
"        const center = left + 0.5 * width;\n"
"        width = minAreaDim;\n"
"        left = center - 0.5 * width;\n"
"    }\n"
"    \n"
"    if (height < minAreaDim)\n"
"    {\n"
"        const center = bottom + 0.5 * height;\n"
"        height = minAreaDim;\n"
"        bottom = center - 0.5 * height;\n"
"    }\n"
"    \n"
"    overviewCtx.fillRect(left, top, width, height);\n"
"    \n"
"    if (overview.hoveredCanvasX != null)\n"
"    {\n"
"        overviewCtx.fillRect(overview.hoveredCanvasX - 0.5 * width, overview.hoveredCanvasY - 0.5 * height, width, height);\n"
"        \n"
"        overviewCtx.font = '18px mono';\n"
"        overviewCtx.textAlign = 'center';\n"
"        overviewCtx.textBaseline = 'bottom';\n"
"        \n"
"        const x = plot.minX + (plot.maxX - plot.minX) * overview.hoveredCanvasX / overviewCanvas.width;\n"
"        const y = plot.minY + (plot.maxY - plot.minY) * (1 - overview.hoveredCanvasY / overviewCanvas.height);\n"
"        const strX = numberFormat.format(x);\n"
"        const strY = numberFormat.format(y);\n"
"        const str = strX + ', ' + strY;\n"
"        \n"
"        overviewCtx.fillStyle = theme.textBackgroundColor;\n"
"        drawTextBackground(\n"
"            overviewCtx,\n"
"            str,\n"
"            overview.hoveredCanvasX,\n"
"            overview.hoveredCanvasY\n"
"        );\n"
"        \n"
"        overviewCtx.fillStyle = theme.highlightedColor;\n"
"        overviewCtx.fillText(str, overview.hoveredCanvasX, overview.hoveredCanvasY);\n"
"    }\n"
"}\n"
"\n";

static char* const HTML_DrawGatheredPoints_YofX =
"function drawGatheredPoints(context, gathered)\n"
"{\n"
"    const points = gathered.points;\n"
"    \n"
"    context.strokeStyle = theme.curveColor;\n"
"    context.fillStyle = theme.curveColor;\n"
"    \n"
"    context.beginPath();\n"
"    context.moveTo(points[0].x, points[0].y);\n"
"    for (let i = 1; i < points.length; ++i)\n"
"    {\n"
"        context.lineTo(points[i].x, points[i].y);\n"
"    }\n"
"    context.stroke();\n"
"    \n"
"    for (let i = 0; i < points.length; ++i)\n"
"    {\n"
"        context.beginPath();\n"
"        context.arc(points[i].x, points[i].y, knotRadius, 0, Math.PI * 2);\n"
"        context.stroke();\n"
"        context.fill();\n"
"    }\n"
"    \n"
"}\n"
"\n";

static char* const HTML_DrawCanvas =
"function drawCanvas()\n"
"{\n"
"    ctx.reset();\n"
"    \n"
"    const gathered = gatherVisiblePoints();\n"
"    const viewToCanvasX = gathered.viewToCanvasX;\n"
"    const viewToCanvasY = gathered.viewToCanvasY;\n"
"    const points = gathered.points;\n"
"    const startI = gathered.startI;\n"
"    const endI = gathered.endI;\n"
"    \n"
"    drawGatheredPoints(ctx, gathered);\n"
"    \n"
"    {\n"
"        const canvasToViewX = 1 / viewToCanvasX;\n"
"        const canvasToViewY = 1 / viewToCanvasY;\n"
"        \n"
"        const left = view.bottomLeft.x;\n"
"        const right = view.bottomLeft.x + view.dim.width;\n"
"        \n"
"        const bottom = view.bottomLeft.y;\n"
"        const top = view.bottomLeft.y + view.dim.height;\n"
"        \n"
"        const roughGridPixelSpacing = 200.0;\n"
"        \n"
"        const gridSpacingX = roundToGoodLookingGridNumber(canvasToViewX * roughGridPixelSpacing);\n"
"        const gridSpacingY = roundToGoodLookingGridNumber(canvasToViewY * roughGridPixelSpacing);\n"
"        \n"
"        ctx.strokeStyle = theme.gridColor;\n"
"        \n"
"        ctx.font = '18px mono';\n"
"        \n"
"        ctx.textAlign = 'center';\n"
"        ctx.textBaseline = 'bottom';\n"
"        \n"
"        const startX = left - (left % gridSpacingX);\n"
"        for (let x = startX; x < right; x += gridSpacingX)\n"
"        {\n"
"            const str = numberFormat.format(x);\n"
"            const canvasX = viewToCanvasX * (x - view.bottomLeft.x);\n"
"            \n"
"            ctx.fillStyle = theme.textBackgroundColor;\n"
"            drawTextBackground(\n"
"                ctx,\n"
"                str,\n"
"                canvasX,\n"
"                canvas.height - paddingV\n"
"            );\n"
"            \n"
"            ctx.fillStyle = theme.textColor;\n"
"            ctx.fillText(str, canvasX, canvas.height - paddingV);\n"
"            \n"
"            ctx.beginPath();\n"
"            ctx.moveTo(canvasX, 0);\n"
"            ctx.lineTo(canvasX, canvas.height);\n"
"            ctx.stroke();\n"
"        }\n"
"        \n"
"        ctx.textAlign = 'left';\n"
"        ctx.textBaseline = 'middle';\n"
"        \n"
"        const startY = bottom - (bottom % gridSpacingY);\n"
"        for (let y = startY; y < top; y += gridSpacingY)\n"
"        {\n"
"            let str;\n"
"            if (-0.5 * gridSpacingY < y && y < 0.5 * gridSpacingY)\n"
"            {\n"
"                str = '0';\n"
"            }\n"
"            else\n"
"            {\n"
"                str = numberFormat.format(y);\n"
"            }\n"
"            \n"
"            let canvasY = canvas.height - viewToCanvasY * (y - view.bottomLeft.y);\n"
"            \n"
"            ctx.fillStyle = theme.textBackgroundColor;\n"
"            drawTextBackground(\n"
"                ctx,\n"
"                str,\n"
"                paddingH,\n"
"                canvasY\n"
"            );\n"
"            \n"
"            ctx.fillStyle = theme.textColor;\n"
"            ctx.fillText(str, paddingH, canvasY);\n"
"            \n"
"            ctx.beginPath();\n"
"            ctx.moveTo(0, canvasY);\n"
"            ctx.lineTo(canvas.width, canvasY);\n"
"            ctx.stroke();\n"
"        }\n"
"    }\n"
"    \n"
"    renderedPlot = {\n"
"        points: points,\n"
"        startI: startI,\n"
"        endI: endI,\n"
"        indices: gathered.indices\n"
"    };\n"
"}\n"
"\n";


static void PlotHTML_YofX_NoXSamples(FILE* out, Plot* plot)
{
    BEf64 minX;
    BEf64 maxX;
    
    if (plot->lines.xs.count == 0)
    {
        minX = 0.0;
        maxX = (BEf64)(plot->lines.ys.count - 1);
    }
    else
    {
        minX = plot->lines.xs.minValue;
        maxX = plot->lines.xs.maxValue;
    }
    
    char* views =
        "const viewPadding = 0.1 * (plot.maxY - plot.minY);\n"
        "\n"
        "const view = {\n"
        "    bottomLeft: { x: 0.0, y: plot.minY - 0.5 * viewPadding },\n"
        "    dim: { width: Math.min(200, plot.ys.length - 1), height: viewPadding + plot.maxY - plot.minY },\n"
        "    highlightedPos: null\n"
        "};\n"
        "\n"
        "const scrollSensitivity = 0.1;\n"
        "const minViewWidth = 2;\n"
        "\n";
    
    char* gatherVisiblePoints = 
        "function gatherVisiblePoints()\n"
        "{\n"
        "    const startI = Math.max(0, Math.floor(view.bottomLeft.x));\n"
        "    const endI = Math.min(plot.ys.length, Math.ceil(startI + view.dim.width + 2));\n"
        "    \n"
        "    const viewToCanvasX = canvas.width / view.dim.width;\n"
        "    const viewToCanvasY = canvas.height / view.dim.height;\n"
        "    \n"
        "    const points = new Array(endI - startI);\n"
        "    for (let i = startI; i < endI; ++i)\n"
        "    {\n"
        "        points[i - startI] = {\n"
        "            x: viewToCanvasX * (i - view.bottomLeft.x),\n"
        "            y: canvas.height - viewToCanvasY * (plot.ys[i] - view.bottomLeft.y)\n"
        "        };\n"
        "    }\n"
        "    \n"
        "    return { startI:startI, endI:endI, viewToCanvasX:viewToCanvasX, viewToCanvasY:viewToCanvasY, points:points };\n"
        "}\n"
        "\n"
        "function bucketPoints(bucketCount)\n"
        "{\n"
        "    const maxValues01 = new Array(bucketCount);\n"
        "    const minValues01 = new Array(bucketCount);\n"
        "    \n"
        "    const margin = 0.1;\n"
        "    \n"
        "    for (let i = 0; i < plot.ys.length; ++i)\n"
        "    {\n"
        "        let bucketIndex = i / plot.ys.length;\n"
        "        bucketIndex *= bucketCount;\n"
        "        bucketIndex = Math.floor(bucketIndex);\n"
        "        \n"
        "        let v = 1 - (plot.ys[i] - plot.minY) / (plot.maxY - plot.minY);\n"
        "        v = 0.5 * margin + (1 - margin) * v;\n"
        "        \n"
        "        if (minValues01[bucketIndex] === undefined)\n"
        "        {\n"
        "            minValues01[bucketIndex] = maxValues01[bucketIndex] = v;\n"
        "        }\n"
        "        else\n"
        "        {\n"
        "            if (minValues01[bucketIndex] > v)\n"
        "            {\n"
        "                minValues01[bucketIndex] = v;\n"
        "            }\n"
        "            if (maxValues01[bucketIndex] < v)\n"
        "            {\n"
        "                maxValues01[bucketIndex] = v;\n"
        "            }\n"
        "        }\n"
        "    }\n"
        "    \n"
        "    return [minValues01, maxValues01];\n"
        "}\n"
        "\n"
        "function getBucketX(v01)\n"
        "{\n"
        "    return Math.round(plot.ys.length * v01);\n"
        "}\n"
        "\n"
        "function toBucketX(x)\n"
        "{\n"
        "    return x / plot.ys.length;\n"
        "}\n"
        "\n"
        "function findClosestSample(x, y)\n"
        "{\n"
        "    const v = Math.round(x);\n"
        "    return [v, v];\n"
        "}\n"
        "\n";
    
    fprintf(out, 
            "const plot = {\n"
            "    minY:%f,\n"
            "    maxY:%f,\n"
            "    ys:[",
            plot->lines.ys.minValue, plot->lines.ys.maxValue);
    
    WriteCommaSeparatedSamples(out,  &plot->lines.ys, "        ");
    fputs("\n"
          "    ]\n"
          "};\n"
          "\n",
          out);
    
    fputs(HTML_CanvasSetup, out);
    fputs(HTML_GlobalState, out);
    fputs(views, out);
    fputs(HTML_Utilities, out);
    fputs(gatherVisiblePoints, out);
    fputs(HTML_CanvasEventHandlers_YofX, out);
    fputs(HTML_DrawOverlay_YofX, out);
    fputs(HTML_DrawOverview_YofX, out);
    fputs(HTML_DrawGatheredPoints_YofX, out);
    fputs(HTML_DrawCanvas, out);
}

static void PlotHTML_YofX(FILE* out, Plot* plot)
{
    char* views =
        "const viewPadding = 0.1 * (plot.maxY - plot.minY);\n"
        "\n"
        "const view = {\n"
        "    bottomLeft: { x: plot.minX, y: plot.minY - 0.5 * viewPadding },\n"
        "    dim: { width: plot.maxX - plot.minX, height: viewPadding + plot.maxY - plot.minY },\n"
        "    highlightedPos: null\n"
        "};\n"
        "\n"
        "if (plot.xs.length >= 200)\n"
        "{\n"
        "    view.dim.width = plot.xs[200] - plot.minX;\n"
        "}\n"
        "const minViewWidth = 2 * (plot.maxX - plot.minX) / plot.xs.length;\n"
        "const scrollSensitivity = 0.05 * minViewWidth;\n"
        "\n";
    
    char* gatherVisiblePoints =
        "function findLowestIndexGreaterThanValue(values, value)\n"
        "{\n"
        "    let count = values.length;\n"
        "    let left = 0;\n"
        "    \n"
        "    while (count > 0)\n"
        "    {\n"
        "        const step = Math.floor(count / 2);\n"
        "        const it = left + step;\n"
        "        \n"
        "        if (values[it] < value)\n"
        "        {\n"
        "            left = it + 1;\n"
        "        }\n"
        "        else\n"
        "        {\n"
        "            count = step;\n"
        "        }\n"
        "    }\n"
        "    \n"
        "    return left;\n"
        "}\n"
        "\n"
        "function findVisibleXIndices(startX, endX)\n"
        "{\n"
        "    let startI = findLowestIndexGreaterThanValue(plot.xs, startX);\n"
        "    let endI = findLowestIndexGreaterThanValue(plot.xs, endX) + 1;\n"
        "    if (startI > 0)\n"
        "    {\n"
        "        --startI;\n"
        "    }\n"
        "    \n"
        "    return [startI, endI];\n"
        "}\n"
        "function gatherVisiblePoints()\n"
        "{\n"
        "    const [startI, endI] = findVisibleXIndices(view.bottomLeft.x, view.bottomLeft.x + view.dim.width);\n"
        "    \n"
        "    const viewToCanvasX = canvas.width / view.dim.width;\n"
        "    const viewToCanvasY = canvas.height / view.dim.height;\n"
        "    \n"
        "    const points = new Array(endI - startI);\n"
        "    for (let i = startI; i < endI; ++i)\n"
        "    {\n"
        "        points[i - startI] = {\n"
        "            x: viewToCanvasX * (plot.xs[i] - view.bottomLeft.x),\n"
        "            y: canvas.height - viewToCanvasY * (plot.ys[i] - view.bottomLeft.y)\n"
        "        };\n"
        "    }\n"
        "    \n"
        "    return { startI:startI, endI:endI, viewToCanvasX:viewToCanvasX, viewToCanvasY:viewToCanvasY, points:points };\n"
        "}\n"
        "\n"
        "function bucketPoints(bucketCount)\n"
        "{\n"
        "    const maxValues01 = new Array(bucketCount);\n"
        "    const minValues01 = new Array(bucketCount);\n"
        "    \n"
        "    const margin = 0.1;\n"
        "    \n"
        "    for (let i = 0; i < plot.xs.length; ++i)\n"
        "    {\n"
        "        let bucketIndex = (plot.xs[i] - plot.minX) / (plot.maxX - plot.minX);\n"
        "        bucketIndex *= bucketCount;\n"
        "        bucketIndex = Math.floor(bucketIndex);\n"
        "        \n"
        "        let v = 1 - (plot.ys[i] - plot.minY) / (plot.maxY - plot.minY);\n"
        "        v = 0.5 * margin + (1 - margin) * v;\n"
        "        \n"
        "        if (minValues01[bucketIndex] === undefined)\n"
        "        {\n"
        "            minValues01[bucketIndex] = maxValues01[bucketIndex] = v;\n"
        "        }\n"
        "        else\n"
        "        {\n"
        "            if (minValues01[bucketIndex] > v)\n"
        "            {\n"
        "                minValues01[bucketIndex] = v;\n"
        "            }\n"
        "            if (maxValues01[bucketIndex] < v)\n"
        "            {\n"
        "                maxValues01[bucketIndex] = v;\n"
        "            }\n"
        "        }\n"
        "    }\n"
        "    \n"
        "    return [minValues01, maxValues01];\n"
        "}\n"
        "\n"
        "function getBucketX(v01)\n"
        "{\n"
        "     return plot.minX + v01 * (plot.maxX - plot.minX);\n"
        "}\n"
        "\n"
        "function toBucketX(x)\n"
        "{\n"
        "    return (x - plot.minX) / (plot.maxX - plot.minX);\n"
        "}\n"
        "\n"
        "function findClosestSample(x, y)\n"
        "{\n"
        "    let i = findLowestIndexGreaterThanValue(plot.xs, x);\n"
        "    if (i > 0)\n"
        "    {\n"
        "        --i;\n"
        "    }\n"
        "    return [i, plot.xs[i]];\n"
        "}\n"
        "\n";
    
    fprintf(out, 
            "const plot = {\n"
            "    minX:%f,\n"
            "    maxX:%f,\n"
            "    minY:%f,\n"
            "    maxY:%f,\n",
            plot->lines.xs.minValue, plot->lines.xs.maxValue,
            plot->lines.ys.minValue, plot->lines.ys.maxValue);
    
    fputs("    xs:[", out);
    WriteCommaSeparatedSamples(out, &plot->lines.xs, "        ");
    fputs("\n"
          "    ],\n",
          out);
    
    fputs("    ys:[", out);
    WriteCommaSeparatedSamples(out, &plot->lines.ys, "        ");
    fputs("\n"
          "    ]\n"
          "};\n",
          out);
    
    fputs(HTML_CanvasSetup, out);
    fputs(HTML_GlobalState, out);
    fputs(views, out);
    fputs(HTML_Utilities, out);
    fputs(gatherVisiblePoints, out);
    fputs(HTML_CanvasEventHandlers_YofX, out);
    fputs(HTML_DrawOverlay_YofX, out);
    fputs(HTML_DrawOverview_YofX, out);
    fputs(HTML_DrawGatheredPoints_YofX, out);
    fputs(HTML_DrawCanvas, out);
}

#define SamplesPerQuadtreeNode 64

typedef struct Rect Rect;
struct Rect
{
    BEf64 minX;
    BEf64 maxX;
    BEf64 minY;
    BEf64 maxY;
};

typedef struct QuadtreeNode QuadtreeNode;
struct QuadtreeNode
{
    QuadtreeNode* parent;
    QuadtreeNode* children[4];
    
    BEu32 count;
    BEu32 indices[SamplesPerQuadtreeNode];
    Rect rects[SamplesPerQuadtreeNode];
    
    BEf64 centerX;
    BEf64 centerY;
    BEf64 halfWidth;
    BEf64 halfHeight;
};

static void SplitNode(BEAllocatorType allocator, QuadtreeNode* rootNode)
{
    QuadtreeNode* node = rootNode;
    
    do
    {
        while (node->count)
        {
            BEu32 splitCount = 0;
            BEu32 splitIndices[4];
            
            BEf64 childHalfWidth = 0.5 * node->halfWidth;
            BEf64 childHalfHeight = 0.5 * node->halfHeight;
            
            BEf64 minX = node->rects[node->count - 1].minX;
            BEf64 maxX = node->rects[node->count - 1].maxX;
            BEf64 minY = node->rects[node->count - 1].minY;
            BEf64 maxY = node->rects[node->count - 1].maxY;
            
            {
                // NOTE(Jens): Terminology uses "x increases to right" and "y increases to up".
                
                BEbool overlapsLeft = (minX < node->centerX);
                BEbool overlapsRight = (maxX >= node->centerX);
                BEbool overlapsDown = (minY < node->centerY);
                BEbool overlapsUp = (maxY >= node->centerY);
                
                if (overlapsLeft && overlapsDown)
                {
                    splitIndices[splitCount++] = 0;
                }
                if (overlapsRight && overlapsDown)
                {
                    splitIndices[splitCount++] = 1;
                }
                if (overlapsLeft && overlapsUp)
                {
                    splitIndices[splitCount++] = 2;
                }
                if (overlapsRight && overlapsUp)
                {
                    splitIndices[splitCount++] = 3;
                }
            }
            
            QuadtreeNode* fullChild = 0;
            for (BEu32 i = 0; i < splitCount; ++i)
            {
                QuadtreeNode* child = node->children[splitIndices[i]];
                if (child && child->count == SamplesPerQuadtreeNode)
                {
                    fullChild = child;
                    break;
                }
            }
            
            if (fullChild)
            {
                node = fullChild;
            }
            else
            {
                BEAssert(splitCount > 0);
                for (BEu32 i = 0; i < splitCount; ++i)
                {
                    BEu32 childIndex = splitIndices[i];
                    
                    QuadtreeNode** childSlot = &node->children[childIndex];
                    
                    if (!*childSlot)
                    {
                        *childSlot = BEAlloc(allocator, QuadtreeNode);
                        (*childSlot)->parent = node;
                        (*childSlot)->children[0] = (*childSlot)->children[1] = (*childSlot)->children[2] = (*childSlot)->children[3] = 0;
                        (*childSlot)->count = 0;
                        
                        (*childSlot)->halfWidth = childHalfWidth;
                        (*childSlot)->halfHeight = childHalfHeight;
                        
                        (*childSlot)->centerX = node->centerX - childHalfWidth  + node->halfWidth  * (BEf64)(childIndex & 1);
                        (*childSlot)->centerY = node->centerY - childHalfHeight + node->halfHeight * (BEf64)(childIndex >> 1);
                    }
                    
                    (*childSlot)->rects[(*childSlot)->count] = node->rects[node->count - 1];
                    (*childSlot)->indices[(*childSlot)->count] = node->indices[node->count - 1];
                    ++(*childSlot)->count;
                }
                
                --node->count;
            }
        }
        
        node = node->parent;
    }
    while (node != rootNode->parent);
}

static QuadtreeNode* NextDepthFirstNode(QuadtreeNode* root, QuadtreeNode* node, BEs32* depthDiff)
{
    QuadtreeNode* rVal = node;
    
    if (depthDiff)
    {
        *depthDiff = 0;
    }
    
    QuadtreeNode* childNodeToEnter = 0;
    
    for (BEu32 i = 0; i < 4; ++i)
    {
        if (rVal->children[i])
        {
            childNodeToEnter = rVal->children[i];
            break;
        }
    }
    
    if (childNodeToEnter)
    {
        if (depthDiff)
        {
            ++(*depthDiff);
        }
        
        rVal = childNodeToEnter;
    }
    else
    {
        label_FindSibling:
        
        if (rVal != root)
        {
            QuadtreeNode* siblingToEnter = 0;
            QuadtreeNode* parent = rVal->parent;
            
            BEu32 childIndex = 0;
            while (parent->children[childIndex] != rVal)
            {
                ++childIndex;
                BEAssert(childIndex < 4);
            }
            
            for (BEu32 siblingIndex = childIndex + 1; siblingIndex < 4; ++siblingIndex)
            {
                if (parent->children[siblingIndex])
                {
                    siblingToEnter = parent->children[siblingIndex];
                    break;
                }
            }
            
            if (siblingToEnter)
            {
                rVal = siblingToEnter;
            }
            else
            {
                if (depthDiff)
                {
                    --(*depthDiff);
                }
                
                rVal = parent;
                goto label_FindSibling;
            }
        }
        else
        {
            rVal = 0;
        }
    }
    
    return rVal;
}

static void SplitNodesLargerThan(BEAllocatorType allocator, QuadtreeNode* root, BEf64 width, BEf64 height, BEu32 maxDepth)
{
    BEf64 halfWidth = 0.5 * width;
    BEf64 halfHeight = 0.5 * height;
    
    // NOTE(Jens): Depth-first traversal with lowest child index first.
    
    BEs32 depth = 0;
    
    QuadtreeNode* node = root;
    while (node)
    {
        if (depth < (BEs32)maxDepth && (node->halfWidth > halfWidth || node->halfHeight > halfHeight))
        {
            SplitNode(allocator, node);
        }
        
        BEs32 depthDiff;
        node = NextDepthFirstNode(root, node, &depthDiff);
        
        depth += depthDiff;
    }
}

static void WriteIndent(FILE* out, BEs32 indent)
{
    for (BEs32 i = 0; i < indent; ++i)
    {
        fputs("  ", out);
    }
}

static BEu32 FindLowestIndexGreaterThanOrEqualToValue(BEu32 count_, BEu32* values, BEu32 value)
{
    BEu32 count = count_;
    BEu32 left = 0;
    
    while (count)
    {
        BEu32 step = count / 2;
        BEu32 it = left + step;
        
        if (values[it] < value)
        {
            left = ++it;
            count -= step + 1;
        }
        else
        {
            count = step;
        }
    }
    
    return left;
}

static void OrderedArrayInsertIgnoreDuplicate(BEu32* count, BEu32* values, BEu32 value)
{
    if (*count == 0)
    {
        values[(*count)++] = value;
    }
    else
    {
        BEu32 index = FindLowestIndexGreaterThanOrEqualToValue(*count, values, value);
        if (values[index] != value)
        {
            if (index == *count)
            {
                values[(*count)++] = value;
            }
            else
            {
                memmove(values + (index + 1), values + index, sizeof(BEu32) * (*count - index));
                values[index] = value;
                ++(*count);
            }
        }
    }
}

static QuadtreeNode* WriteQuadtreeIndices(FILE* out, QuadtreeNode* rootNode, BEs32* deltaDepth)
{
    BEu32 maxCount = 0;
    
    QuadtreeNode* lastNode;
    
    {
        QuadtreeNode* node = rootNode;
        BEs32 depth = 0;
        
        do
        {
            maxCount += node->count;
            
            lastNode = node;
            
            BEs32 depthDiff;
            *deltaDepth = depth;
            node = NextDepthFirstNode(rootNode, node, &depthDiff);
            
            depth += depthDiff;
        }
        while (depth > 0);
    }
    
    CommandLineAllocator allocator = { 0 };
    
    BEu32 indexCount = 0;
    BEu32* indices = BEAllocArray(&allocator, BEu32, maxCount);
    
    {
        QuadtreeNode* node = rootNode;
        BEs32 depth = 0;
        
        do
        {
            maxCount += node->count;
            
            for (BEu32 i = 0; i < node->count; ++i)
            {
                OrderedArrayInsertIgnoreDuplicate(&indexCount, indices, node->indices[i]);
            }
            
            BEs32 depthDiff;
            node = NextDepthFirstNode(rootNode, node, &depthDiff);
            
            depth += depthDiff;
        }
        while (depth > 0);
    }
    
    for (BEu32 i = 0; i < indexCount; ++i)
    {
        if (i != indexCount - 1)
        {
            fprintf(out, "%u, ", indices[i]);
        }
        else
        {
            fprintf(out, "%u", indices[i]);
        }
    }
    
    DeallocAll(&allocator);
    
    return lastNode;
}

static void WriteQuadtree(FILE* out, QuadtreeNode* rootNode)
{
    QuadtreeNode* node = rootNode;
    
    BEs32 indent = 0;
    while (node)
    {
        WriteIndent(out, indent);
        fputs("{\n", out);
        
        WriteIndent(out, indent + 1);
        fprintf(out, 
                "minX:%e, maxX:%e, minY:%e, maxY:%e, \n", 
                node->centerX - node->halfWidth,
                node->centerX + node->halfWidth,
                node->centerY - node->halfHeight,
                node->centerY + node->halfHeight);
        
        WriteIndent(out, indent + 1);
        
        BEs32 depthDiff = 0;
        
        if (node->count)
        {
            fputs("indices:[", out);
            
            BEs32 deltaDepth;
            
            QuadtreeNode* lastDescendentLeafNode = WriteQuadtreeIndices(out, node, &deltaDepth);
            fputs("]\n", out);
            
            WriteIndent(out, indent);
            fputs("},\n", out);
            
            BEAssert(deltaDepth >= 0);
            depthDiff += deltaDepth;
            
            node = lastDescendentLeafNode;
        }
        else
        {
            fputs("children:[\n", out);
            ++indent;
        }
        
        BEs32 deltaDepth;
        QuadtreeNode* nextNode = NextDepthFirstNode(rootNode, node, &deltaDepth);
        
        depthDiff += deltaDepth;
        
        if (depthDiff > 0)
        {
            indent += depthDiff;
        }
        else if (depthDiff == 0)
        {
        }
        else
        {
            for (BEs32 i = 0; i < -depthDiff; ++i)
            {
                --indent;
                WriteIndent(out, indent);
                fputs("]\n", out);
                --indent;
                WriteIndent(out, indent);
                
                if (indent > 0)
                {
                    fputs("},", out);
                }
                else
                {
                    fputs("}", out);
                }
                
                fputs("\n", out);
            }
        }
        
        node = nextNode;
    }
}

static void AddRect(BEAllocatorType allocator, QuadtreeNode* node, BEu32 index, BEf64 x0, BEf64 y0, BEf64 x1, BEf64 y1)
{
    if (node->count == SamplesPerQuadtreeNode)
    {
        SplitNode(allocator, node);
    }
    
    Rect rect;
    
    if (x0 < x1)
    {
        rect.minX = x0;
        rect.maxX = x1;
    }
    else
    {
        rect.minX = x1;
        rect.maxX = x0;
    }
    
    if (y0 < y1)
    {
        rect.minY = y0;
        rect.maxY = y1;
    }
    else
    {
        rect.minY = y1;
        rect.maxY = y0;
    }
    
    node->rects[node->count] = rect;
    node->indices[node->count] = index;
    ++node->count;
}

// NOTE(Jens): This is potentially a pretty bad spatial partitioning scheme for lines, could be improved.
static void PartitionSamplesByLineArea(QuadtreeNode* out, BEAllocatorType allocator, Plot* plot, BEf64 gridW, BEf64 gridH, BEu32 maxDepth)
{
    BEf64 plotWidth = (plot->lines.xs.maxValue - plot->lines.xs.minValue);
    BEf64 plotHeight = (plot->lines.ys.maxValue - plot->lines.ys.minValue);
    
    BEf64 cellCountX = ceil(plotWidth / gridW);
    BEf64 cellCountY = ceil(plotHeight / gridH);
    
    BEf64 cellCount = (cellCountX > cellCountY ? cellCountX : cellCountY);
    
    out->halfWidth = 0.5 * cellCount * gridW;
    out->halfHeight = 0.5 * cellCount * gridH;
    out->centerX = 0.5 * (plot->lines.xs.maxValue + plot->lines.xs.minValue);
    out->centerY = 0.5 * (plot->lines.ys.maxValue + plot->lines.ys.minValue);
    
    BEu32 localIndex = 1;
    BEu32 sampleCount = plot->lines.xs.count;
    
    PlotData* xs = plot->lines.xs.head;
    PlotData* ys = plot->lines.ys.head;
    
    BEf64 prevX = xs->values[0];
    BEf64 prevY = ys->values[0];
    for (BEu32 i = 1; i < sampleCount; ++i)
    {
        BEf64 x = xs->values[localIndex];
        BEf64 y = ys->values[localIndex];
        
        AddRect(allocator, out, i - 1, prevX, prevY, x, y);
        prevX = x;
        prevY = y;
        
        if (++localIndex == SamplesPerPlotData)
        {
            localIndex = 0;
            xs = xs->next;
            ys = ys->next;
        }
    }
    
    SplitNodesLargerThan(allocator, out, gridW, gridH, maxDepth);
}

static void PlotHTML_Curve(FILE* out, Plot* plot)
{
    char* views =
        "const view = {\n"
        "    bottomLeft: { x:0, y:0 },\n"
        "    dim: { width:0, height:0 },\n"
        "    prevHighlight: 0,\n"
        "    highlightedPos: null\n"
        "};\n"
        "\n"
        "const minViewWidth = 4 * plot.averageDX;\n"
        "const minViewHeight = 4 * plot.averageDY;\n"
        "\n"
        "{\n"
        "    const i = Math.min(200, plot.xs.length - 1);\n"
        "    view.bottomLeft.x = Math.min(plot.xs[0], plot.xs[i]);\n"
        "    view.bottomLeft.y = Math.min(plot.ys[0], plot.ys[i]);\n"
        "    view.dim.width = Math.abs(plot.xs[0] - plot.xs[i]);\n"
        "    view.dim.height = Math.abs(plot.ys[0] - plot.ys[i]);\n"
        "}\n"
        "view.dim.width = Math.max(view.dim.width, minViewWidth);\n"
        "view.dim.height = Math.max(view.dim.height, minViewHeight);\n"
        "const scrollSensitivityH = 0.04 * plot.averageDX;\n"
        "const scrollSensitivityV = 0.04 * plot.averageDY;\n"
        "\n";
    
    char* gatherVisiblePoints =
        "function gatherVisiblePoints()\n"
        "{\n"
        "    const indicesSet = new Set();\n"
        "    \n"
        "    const minX = view.bottomLeft.x;\n"
        "    const maxX = minX + view.dim.width;\n"
        "    const minY = view.bottomLeft.y;\n"
        "    const maxY = minY + view.dim.height;\n"
        "    \n"
        "    const viewToCanvasX = canvas.width / view.dim.width;\n"
        "    const viewToCanvasY = canvas.height / view.dim.height;\n"
        "    \n"
        "    const nodesToConsider = [quadtree];\n"
        "    while (nodesToConsider.length > 0)\n"
        "    {\n"
        "        const node = nodesToConsider.pop();\n"
        "        if (node.minX <= maxX && minX <= node.maxX && node.minY <= maxY && minY <= node.maxY)\n"
        "        {\n"
        "            if (node.children === undefined)\n"
        "            {\n"
        "                for (const index of node.indices)\n"
        "                {\n"
        "                    indicesSet.add(index);\n"
        "                }\n"
        "            }\n"
        "            else\n"
        "            {\n"
        "                for (const child of node.children)\n"
        "                {\n"
        "                    nodesToConsider.push(child);\n"
        "                }\n"
        "            }\n"
        "        }\n"
        "    }\n"
        "    \n"
        "    \n"
        "    const points = new Array(2 * indicesSet.size);\n"
        "    const indices = new Array(indicesSet.size);\n"
        "    \n"
        "    {\n"
        "        let pIndex = 0;\n"
        "        for (const index of indicesSet)\n"
        "        {\n"
        "            indices[pIndex] = index;\n"
        "            ++pIndex;\n"
        "        }\n"
        "        indices.sort((a, b) => a - b);\n"
        "    }\n"
        "    \n"
        "    for (let i = 0; i < indices.length; ++i)\n"
        "    {\n"
        "        points[2 * i + 0] = {\n"
        "            x: viewToCanvasX * (plot.xs[indices[i]] - view.bottomLeft.x),\n"
        "            y: canvas.height - viewToCanvasY * (plot.ys[indices[i]] - view.bottomLeft.y)\n"
        "        };\n"
        "        points[2 * i + 1] = {\n"
        "            x: viewToCanvasX * (plot.xs[indices[i] + 1] - view.bottomLeft.x),\n"
        "            y: canvas.height - viewToCanvasY * (plot.ys[indices[i] + 1] - view.bottomLeft.y)\n"
        "        };\n"
        "    }\n"
        "    \n"
        "    return { viewToCanvasX:viewToCanvasX, viewToCanvasY:viewToCanvasY, points:points, indices:indices };\n"
        "}\n"
        "\n"
        "function drawGatheredPoints(context, gathered)\n"
        "{\n"
        "    const points = gathered.points;\n"
        "    const indices = gathered.indices;\n"
        "    \n"
        "    context.strokeStyle = theme.curveColor;\n"
        "    context.fillStyle = theme.curveColor;\n"
        "    \n"
        "    context.beginPath();\n"
        "    {\n"
        "        let prevEndOfLine = null;\n"
        "        for (let i = 0; i < indices.length; ++i)\n"
        "        {\n"
        "            const index = indices[i];\n"
        "            if (prevEndOfLine != index)\n"
        "            {\n"
        "                context.moveTo(points[2 * i + 0].x, points[2 * i + 0].y);\n"
        "            }\n"
        "            context.lineTo(points[2 * i + 1].x, points[2 * i + 1].y);\n"
        "            prevEndOfLine = index + 1;\n"
        "        }\n"
        "        context.stroke();\n"
        "    }\n"
        "    \n"
        "    for (let i = 0; i < points.length; ++i)\n"
        "    {\n"
        "        context.beginPath();\n"
        "        context.arc(points[i].x, points[i].y, knotRadius, 0, Math.PI * 2);\n"
        "        context.stroke();\n"
        "        context.fill();\n"
        "    }\n"
        "    \n"
        "}\n"
        "\n"
        "function binarySearch(values, value)\n"
        "{\n"
        "    let leftIndex = 0;\n"
        "    \n"
        "    if (values.length > 0)\n"
        "    {\n"
        "        let rightIndex = values.length - 1;\n"
        "        \n"
        "        while (leftIndex != rightIndex)\n"
        "        {\n"
        "            let midIndex = Math.ceil((leftIndex + rightIndex) / 2);\n"
        "            if (values[midIndex] > value)\n"
        "            {\n"
        "                rightIndex = midIndex - 1;\n"
        "            }\n"
        "            else\n"
        "            {\n"
        "                leftIndex = midIndex;\n"
        "            }\n"
        "        }\n"
        "        if (values[leftIndex] != value)\n"
        "        {\n"
        "            leftIndex = null;\n"
        "        }\n"
        "    }\n"
        "    else\n"
        "    {\n"
        "        leftIndex = null;\n"
        "    }\n"
        "    return leftIndex;\n"
        "}\n"
        "\n"
        "function sq(v) { return v*v; }\n"
        "function clamp(min, v, max) { return Math.max(min, Math.min(v, max)); }\n"
        "\n"
        "function findClosestSample(indexGuess, viewToCanvasX, viewToCanvasY, x, y)\n"
        "{\n"
        "    let minIndex = indexGuess;\n"
        "    let minDistSq = sq(viewToCanvasX * (plot.xs[indexGuess] - x)) + sq(viewToCanvasY * (plot.ys[indexGuess] - y));\n"
        "    \n"
        "    const nodesToConsider = [{node:quadtree, distanceSq:0}];\n"
        "    while (nodesToConsider.length > 0)\n"
        "    {\n"
        "        const toConsider = nodesToConsider.pop();\n"
        "        if (toConsider.distanceSq > minDistSq)\n"
        "        {\n"
        "            continue;\n"
        "        }\n"
        "        \n"
        "        if (toConsider.node.children === undefined)\n"
        "        {\n"
        "            for (const index of toConsider.node.indices)\n"
        "            {\n"
        "                const x0 = plot.xs[index];\n"
        "                const y0 = plot.ys[index];\n"
        "                const x1 = plot.xs[index + 1];\n"
        "                const y1 = plot.ys[index + 1];\n"
        "                \n"
        "                const distSq0 = sq(viewToCanvasX * (x0 - x)) + sq(viewToCanvasY * (y0 - y));\n"
        "                const distSq1 = sq(viewToCanvasX * (x1 - x)) + sq(viewToCanvasY * (y1 - y));\n"
        "                \n"
        "                if (distSq0 < minDistSq)\n"
        "                {\n"
        "                    minDistSq = distSq0;\n"
        "                    minIndex = index;\n"
        "                }\n"
        "                \n"
        "                if (distSq1 < minDistSq)\n"
        "                {\n"
        "                    minDistSq = distSq1;\n"
        "                    minIndex = index + 1;\n"
        "                }\n"
        "            }\n"
        "        }\n"
        "        else\n"
        "        {\n"
        "            const nodesToAdd = [];\n"
        "            for (const child of toConsider.node.children)\n"
        "            {\n"
        "                let distanceToNodeSq = 0;\n"
        "                if (!(child.minX < x && x < child.maxX && child.minY < y && y < child.maxY))\n"
        "                {\n"
        "                    const nearX = clamp(child.minX, x, child.maxX);\n"
        "                    const nearY = clamp(child.minY, y, child.maxY);\n"
        "                    \n"
        "                    distanceToNodeSq = sq(viewToCanvasX * (nearX - x)) + sq(viewToCanvasY * (nearY - y));\n"
        "                }\n"
        "                \n"
        "                if (distanceToNodeSq < minDistSq)\n"
        "                {\n"
        "                    nodesToAdd.push({node:child, distanceSq:distanceToNodeSq});\n"
        "                }\n"
        "            }\n"
        "            \n"
        "            if (nodesToAdd.length > 0)\n"
        "            {\n"
        "                nodesToAdd.sort((a, b) => b.distanceSq - a.distanceSq);\n"
        "            }\n"
        "            \n"
        "            for (const n of nodesToAdd)\n"
        "            {\n"
        "                nodesToConsider.push(n);\n"
        "            }\n"
        "        }\n"
        "    }\n"
        "    \n"
        "    return minIndex;\n"
        "}\n"
        "\n";
    
    BEf64 averageXDiff = plot->lines.xs.absoluteSumOfDifferences / (BEf64)plot->lines.xs.count;
    BEf64 averageYDiff = plot->lines.ys.absoluteSumOfDifferences / (BEf64)plot->lines.ys.count;
    
    BEf64 minX = plot->lines.xs.minValue;
    BEf64 maxX = plot->lines.xs.maxValue;
    BEf64 minY = plot->lines.ys.minValue;
    BEf64 maxY = plot->lines.ys.maxValue;
    
    if (minX == maxX)
    {
        minX = -1.0f;
        maxX = 1.0f;
    }
    
    if (minY == maxY)
    {
        minY = -1.0f;
        maxY = 1.0f;
    }
    
    fprintf(out, 
            "const plot = {\n"
            "    averageDX:%e,\n"
            "    averageDY:%e,\n"
            "    minX:%e,\n"
            "    maxX:%e,\n"
            "    minY:%e,\n"
            "    maxY:%e,\n",
            averageXDiff, averageYDiff,
            minX, maxX,
            minY, maxY);
    
    fputs("    xs:[", out);
    WriteCommaSeparatedSamples(out, &plot->lines.xs, "        ");
    fputs("\n"
          "    ],\n",
          out);
    
    fputs("    ys:[", out);
    WriteCommaSeparatedSamples(out, &plot->lines.ys, "        ");
    fputs("\n"
          "    ]\n"
          "};\n",
          out);
    
    BEf64 gridW = 10.0 * averageXDiff;
    BEf64 gridH = 10.0 * averageYDiff;
    BEu32 maxDepth = 16;
    
    CommandLineAllocator allocator = { 0 };
    QuadtreeNode rootNode = { 0 };
    PartitionSamplesByLineArea(&rootNode, &allocator, plot, gridW, gridH, maxDepth);
    
    fputs("const quadtree = ", out);
    WriteQuadtree(out, &rootNode);
    fputs(";\n", out);
    
    fputs(HTML_CanvasSetup, out);
    fputs(HTML_GlobalState, out);
    fputs(views, out);
    fputs(HTML_Utilities, out);
    fputs(gatherVisiblePoints, out);
    fputs(HTML_CanvasEventHandlers_Curve, out);
    fputs(HTML_DrawOverlay_Curve, out);
    fputs(HTML_DrawOverview_Curve, out);
    fputs(HTML_DrawCanvas, out);
    
    DeallocAll(&allocator);
}

static void WriteHTML(FILE* out, Plot* plot)
{
    fputs(HTML_Start, out);
    
    if (plot->lines.xs.count == 0)
    {
        PlotHTML_YofX_NoXSamples(out, plot);
    }
    else if (!plot->lines.xIsNotMonotonicallyIncreasing)
    {
        PlotHTML_YofX(out, plot);
    }
    else
    {
        PlotHTML_Curve(out, plot);
    }
    
    fputs(HTML_End, out);
}

static void ExportPlots(FILE* printOut, Plots* plots)
{
    char filenameBuf[256];
    
    for (Plot* plot = plots->list; plot; plot = plot->next)
    {
        if (plot->lines.xs.count == 0 && plot->lines.ys.count == 0)
        {
            fprintf(printOut, "Skipping empty plot with id %lli\n", plot->id);
        }
        else
        {
            BEbool plotIsValid = (plot->lines.xs.count == 0 || plot->lines.xs.count == plot->lines.ys.count);
            
            if (!plotIsValid)
            {
                fprintf(printOut, "Plot with id %lli is not valid, inconsistent x (%u) and y (%u) sample count.\n", plot->id, plot->lines.xs.count, plot->lines.ys.count);
            }
            else if (plot->lines.ys.count == 0)
            {
                fprintf(printOut, "Ignoring empty plot with id %lli\n", plot->id);
            }
            else
            {
                snprintf(filenameBuf, sizeof(filenameBuf), "plot_%llu.html", (unsigned long long)plot->id);
                
                fprintf(printOut, "Exporting plot '%s'... ", filenameBuf);
                FILE* out = fopen(filenameBuf, "w");
                if (out)
                {
                    WriteHTML(out, plot);
                    fprintf(printOut, "complete.\n");
                    
                    fclose(out);
                }
                else
                {
                    fprintf(printOut, "Failed to create file for writing.\n");
                }
            }
        }
    }
}

static void LayoutData(FILE* out, BEu8* dataSection, BEu32 instructionsSizeInBytes, BEu8* instructions, BEu32 sizeOfFile, BEu8* binaryFile,  BEbool flushEachGrid, FILE* instructionPrintOut)
{
    CommandLineAllocator runtimeAllocator = { 0 };
    ColumnLayout layout = { 0 };
    Plots plots = { 0 };
    
    BEu8 stack[64 * 1024]; // TODO(Jens): Maybe restart if stack overflow and increase this number, or pass the size in.
    BERuntime runtime = BEStartRuntime(&runtimeAllocator, sizeof(stack), stack, dataSection, instructionsSizeInBytes, instructions, sizeOfFile, binaryFile);
    
    BEu32 addressDigitCount = CountHexDigits(sizeOfFile);
    BEu32 instructionDigitCount = CountHexDigits(instructionsSizeInBytes);
    BEu32 indentation = 0;
    
    BEbool hasHitBreakpoint = 0;
    
    if (instructionPrintOut)
    {
        fputs("Evaluated instructions...\n", instructionPrintOut);
    }
    
    BEbool prevWasStructStart = 1;
    
    BERuntimeEvent event;
    while (!BERuntimeHasCompleted(&runtime))
    {
        if (instructionPrintOut)
        {
            // NOTE(Jens): Although 'BEStepOne' returns the instruction, we should print it out before it's evaluated.
            //             If not, then the side-effects of the instruction will already have taken place and the output
            //             would be unintuitive.
            
            BEu32 instructionAddress = runtime.currentAddress;
            
            fprintf(instructionPrintOut, "%*Xh ", (int)instructionDigitCount, instructionAddress);
            
            BEu32 instructionSize;
            BEInstruction instruction = BEDecodeInstruction(&instructionSize, instructions + instructionAddress);
            
            if (instruction.type == BE_InstructionType_PopFrame)
            {
                --indentation;
            }
            
            PrintInstruction(out, &instruction, indentation, &runtime);
            
            if (instruction.type == BE_InstructionType_PushFrame)
            {
                ++indentation;
            }
        }
        
        event = BEStepOne(&runtime);
        
        switch (event.type)
        {
            case BE_RuntimeEventType_Breakpoint:
            {
                if (!hasHitBreakpoint)
                {
                    fputs("User breakpoint triggered; launch with '-bdb' option to pause execution. Continuing...\n", out);
                    hasHitBreakpoint = 1;
                }
            } break;
            
            case BE_RuntimeEventType_Print:
            {
                if (event.print.hasValue)
                {
                    AddPrintWithValue(&layout, event.print.msgSize, event.print.msg, event.print.value);
                }
                else
                {
                    AddPrintWithoutValue(&layout, event.print.msgSize, event.print.msg);
                }
                
            } break;
            
            case BE_RuntimeEventType_EndStruct:
            {
                if (!runtime.hideMembersCounter)
                {
                    PopMemberNamePrefix(&layout);
                }
            } break;
            
            case BE_RuntimeEventType_StartStruct:
            {
                if (!runtime.hideMembersCounter)
                {
                    // TODO(Jens): Maybe not return StartStruct / EndStruct if members are hidden.
                    
                    if (event.startStruct.isInternal)
                    {
                        if (event.startStruct.arrayCount == 0)
                        {
                            BEu32 nameSize = 0;
                            char* name = BEGetStructMemberName(&runtime, &nameSize, event.startStruct.parentStructIndex, event.startStruct.memberIndex);
                            
                            PushMemberNamePrefix(&layout, nameSize, name);
                        }
                        else
                        {
                            if (event.startStruct.arrayIndex != 0)
                            {
                                PopMemberNamePrefix(&layout);
                            }
                            
                            BEu32 nameSize = 0;
                            char* name = BEGetStructMemberName(&runtime, &nameSize, event.startStruct.parentStructIndex, event.startStruct.memberIndex);
                            PushMemberNamePrefixWithArrayIndex(&layout, nameSize, name, event.startStruct.arrayIndex);
                        }
                    }
                    else
                    {
                        if (flushEachGrid)
                        {
                            if (!prevWasStructStart)
                            {
                                fputc('\n', out);
                            }
                            
                            if (layout.tail)
                            {
                                prevWasStructStart = (layout.tail->type == LineType_StructStart);
                            }
                            FlushLayout(out, &layout, addressDigitCount, " ");
                        }
                        
                        PushMemberNamePrefixBarrier(&layout);
                        
                        AddNestedMember(&layout, &runtime, event.startStruct.structIndex, event.startStruct.arrayIndex, event.startStruct.parentStructIndex, event.startStruct.memberIndex, event.startStruct.arrayCount, event.startStruct.appearIndex);
                    }
                }
            } break;
            
            case BE_RuntimeEventType_ScalarExport:
            {
                AddMember(&layout, &runtime, binaryFile, event.scalarExport.structIndex, event.scalarExport.memberIndex, event.scalarExport.address, event.scalarExport.size, event.scalarExport.arrayCount, event.scalarExport.isBigEndian, event.scalarExport.enumIndex, event.scalarExport.baseType, event.scalarExport.displayType, event.scalarExport.appearIndex);
            } break;
            
            case BE_RuntimeEventType_PlotX:
            case BE_RuntimeEventType_PlotY:
            {
                AddPlotSample(&runtimeAllocator, &plots, event.plotValue.plotId, event.plotValue.value, event.type == BE_RuntimeEventType_PlotX);
            } break;
        }
        
        if (event.type >= BE_RuntimeEventType_Error_First)
        {
            char* filename = BEGetSourceFilenameForFileIndex(&runtime, 0, event.sourceFileIndex);
            char* err = BEGetRuntimeErrorDescription(event.type);
            
            if (filename)
            {
                fprintf(stdout, "%s(%u,%u): Error: %s.\n", filename, event.sourceLineIndex + 1, event.sourceColIndex + 1, err);
            }
            else
            {
                fprintf(stdout, "Error: %s.\n", err);
            }
            
            break;
        }
    }
    
    if (instructionPrintOut)
    {
        fputs("\n", instructionPrintOut);
    }
    
    if (flushEachGrid)
    {
        if (!prevWasStructStart)
        {
            fputc('\n', out);
        }
    }
    
    FlushLayout(out, &layout, addressDigitCount, " ");
    fputs("\n", out);
    ExportPlots(out, &plots);
}

enum
{
    BEditArg_Error          = (1 << 1),
    BEditArg_Verbose        = (1 << 2),
    BEditArg_ShowVersion    = (1 << 3),
    BEditArg_GenTestFile    = (1 << 4),
    BEditArg_ShowHelp       = (1 << 5),
    BEditArg_DebuggerMode   = (1 << 6),
    BEditArg_NoColorOutput  = (1 << 7),
    BEditArg_Instructions   = (1 << 8),
    BEditArg_FlushGrid      = (1 << 9),
};

typedef struct
{
    BEu32 flags;
    
    union
    {
        struct
        {
            char* dataPath;
            char* layoutPath;
        };
        
        char* testGenFile;
    };
    
} BEditArgs;

static void GenTestFile(char* outPath)
{
    BEu8 testData[256];
    for (BEu32 byteIndex = 0; byteIndex < 256; ++byteIndex)
    {
        testData[byteIndex] = (BEu8)byteIndex;
    }
    
    FILE* testFile = fopen(outPath, "wb");
    fwrite(testData, 1, sizeof(testData), testFile);
    fclose(testFile);
}

static BEditArgs ParseArgs(char* argv[])
{
    BEditArgs rVal = { 0 };
    
    ++argv;
    if (*argv)
    {
        if (strcmp("-gen", *argv) == 0)
        {
            rVal.flags |= BEditArg_GenTestFile;
            
            rVal.testGenFile = "test.bin";
            
            ++argv;
            if (*argv)
            {
                rVal.testGenFile = *argv++;
                
                if (*argv)
                {
                    fprintf(stderr, "Expected end of argument after -gen <file>, got %s\n", *argv);
                    rVal.flags |= BEditArg_Error;
                }
            }
        }
        else if (strcmp("-version", *argv) == 0)
        {
            rVal.flags |= BEditArg_ShowVersion;
        }
        else
        {
            for (char** arg = argv; *arg; ++arg)
            {
                if (strcmp("-layout", *arg) == 0)
                {
                    ++arg;
                    if (*arg)
                    {
                        if (!rVal.layoutPath)
                        {
                            rVal.layoutPath = *arg;
                        }
                        else
                        {
                            fprintf(stderr, "-layout already specified.\n");
                            rVal.flags |= BEditArg_Error;
                        }
                    }
                    else
                    {
                        fprintf(stderr, "Expected file after -layout, got end of parameters.\n");
                        rVal.flags |= BEditArg_Error;
                    }
                }
                else if (strcmp("-data", *arg) == 0)
                {
                    ++arg;
                    if (*arg)
                    {
                        if (!rVal.dataPath)
                        {
                            rVal.dataPath = *arg;
                        }
                        else
                        {
                            fprintf(stderr, "-data already specified.\n");
                            rVal.flags |= BEditArg_Error;
                        }
                    }
                    else
                    {
                        fprintf(stderr, "Expected file after -data, got end of parameters.\n");
                        rVal.flags |= BEditArg_Error;
                    }
                }
                else if (strcmp("-bdb", *arg) == 0)
                {
                    rVal.flags |= BEditArg_DebuggerMode;
                }
                else if (strcmp("-nocolor", *arg) == 0)
                {
                    rVal.flags |= BEditArg_NoColorOutput;
                }
                else if (strcmp("-instructions", *arg) == 0)
                {
                    rVal.flags |= BEditArg_Instructions;
                }
                else if (strcmp("-flush=grid", *arg) == 0)
                {
                    rVal.flags |= BEditArg_FlushGrid;
                }
                else
                {
                    fprintf(stderr, "Unknown argument \"%s\".\n", *arg);
                    rVal.flags |= BEditArg_Error;
                }
                
                if (rVal.flags & BEditArg_Error)
                {
                    break;
                }
            }
            
            if (!rVal.dataPath)
            {
                fprintf(stderr, "-data <file> not specified.\n");
                rVal.flags |= BEditArg_Error;
            }
            
            if (!rVal.layoutPath)
            {
                fprintf(stderr, "-layout <file> not specified.\n");
                rVal.flags |= BEditArg_Error;
            }
        }
    }
    else
    {
        rVal.flags |= BEditArg_ShowHelp;
    }
    
    return rVal;
}

enum
{
	AppError_None = 0,
	AppError_IOError = -1,
	AppError_BadParameters = -2,
	AppError_LayoutError = -3,
};

int main(int argc, char* argv[])
{
    int rVal = AppError_None;
    
	BEditArgs args = ParseArgs(argv);
    
    if (!(args.flags & BEditArg_NoColorOutput))
    {
        EnableTerminalColoredOutput();
    }
    
    if (args.flags & BEditArg_ShowHelp)
    {
        fprintf(stderr,
                "\n"
                "=== BEdit - Structured Binary Editor ===\n"
                "\n"
                "Usage:\n"
                "\n"
                "  %s -data <binary-file> -layout <layout-file> [-flush=grid] [-instructions] [-nocolor] [-bdb]  Displays file content with specified layout.\n"
                "\n"
                "    -layout <layout-file>  The binary layout file (.bet-file). See examples and README for format.\n"
                "    -data <data-file>      The binary content file.\n"
                "    -nocolor               Doesn't add color codes for text.\n"
                "    -flush=grid            Flushes prints every struct member instead of at end of execution.\n"
                "    -instructions          Displays instructions as they are compiled and executed.\n"
                "    -bdb                   Launches the process in instruction debugging mode, like gdb or lldb but for BEdit layout.\n"
                "\n"
                "\n"
                "  %s -gen [binary-file]  Creates binary file containing 0,1,..,255 in binary form - nice file for testing.\n"
                "\n"
                "    binary-file  Output file to create, defaults to \"test.bin\".\n"
                "\n"
                "\n"
                "  %s -version  Displays current version.\n",
                argv[0],
                argv[0],
                argv[0]);
    }
    else if (args.flags & BEditArg_ShowVersion)
    {
        fprintf(stdout, "%s\n", BEdit_VersionString);
    }
    else if (args.flags & BEditArg_Error)
    {
        rVal = AppError_BadParameters;
    }
    else if (args.flags & BEditArg_GenTestFile)
    {
        GenTestFile(args.testGenFile);
    }
    else 
    {
        CommandLineAllocator allocator = { 0 };
        BELoadFileResult binaryFile = loadFile(0, &allocator, 0, args.dataPath);
        
        if (!binaryFile.error)
        {
            CompiledData compiled = CompileLayout(&allocator, stderr, args.layoutPath, (args.flags & BEditArg_DebuggerMode));
            
            FILE* instructionPrintOut = 0;
            if (args.flags & BEditArg_Instructions)
            {
                instructionPrintOut = stdout;
                
                fputs("Compiled instructions...\n", instructionPrintOut);
                PrintInstructions(instructionPrintOut, compiled.instructionsSizeInBytes, compiled.instructions);
                fputs("\n", instructionPrintOut);
            }
            
            if (CompiledDataIsValid(&compiled))
            {
                if (args.flags & BEditArg_DebuggerMode)
                {
                    DebugData(compiled.dataSection.data, compiled.instructionsSizeInBytes, compiled.instructions, binaryFile.fileSizeInBytes, binaryFile.fileContent, compiled.sourceFiles);
                }
                else
                {
                    LayoutData(stdout, compiled.dataSection.data, compiled.instructionsSizeInBytes, compiled.instructions, binaryFile.fileSizeInBytes, binaryFile.fileContent, args.flags & BEditArg_FlushGrid, instructionPrintOut);
                }
            }
            else
            {
                rVal = AppError_LayoutError;
            }
        }
        else
        {
            fprintf(stderr, "Failed to load binary file %s\n", args.dataPath);
        }
    }
    
    // TODO(Jens): This is probably best to call in case of crash as well, maybe add a signal handler.
    ResetTerminalSettings();
    
    return rVal;
}

// NOTE(Jens): Implementation in bottom to avoid accidentally using non-public API.
#include "BEdit.c"
