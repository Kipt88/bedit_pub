@echo off

REM Assumes you are running from developer command prompt for Visual Studio

pushd %~dp0

if not exist vs_build mkdir vs_build

pushd vs_build

set WarningFlags=/wd4530 /wd4577 /wd4100 /wd4201 /wd4505 /wd4611 /wd4127 /wd4068 /wd4324 /W4

if "%~1"=="debug" (
	echo Building debug...
	cl ..\BEdit_CommandLine.c %WarningFlags% /nologo /Od /MTd /Z7 /FC /DBETabSize=8 /link /PDB:bedit.pdb /OUT:bedit.exe /INCREMENTAL:NO /OPT:REF
) else (
	echo Building release...
	cl ..\BEdit_CommandLine.c %WarningFlags% /nologo /O2 /MT /DBETabSize=8 /link /OUT:bedit.exe /INCREMENTAL:NO /OPT:REF
)

popd

popd