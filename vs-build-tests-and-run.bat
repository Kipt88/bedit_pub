@echo off

REM Assumes you are running from developer command prompt for Visual Studio

if not exist vs_build mkdir vs_build

pushd vs_build

set WarningFlags=/wd4530 /wd4577 /wd4100 /wd4201 /wd4505 /wd4611 /wd4127 /wd4068 /wd4324 /W4

echo Building tests debug...
cl ..\BEdit_tests.c %WarningFlags% /nologo /Od /MTd /Z7 /FC /DBETabSize=8 /link /PDB:bedit_tests.pdb /OUT:bedit_tests.exe /INCREMENTAL:NO /OPT:REF
if not %errorlevel% == 0 exit \b

bedit_tests.exe

popd
