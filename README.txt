BEdit - a structured binary editor

Version 0.2.3-wip (built 3rd of June 2024) https://bitbucket.org/Kipt88/bedit_pub/src/master/

This is the public part of BEdit. It contains examples and source code used for parsing structured types as well as evaluating data. There is no *editor* in this repository, only a viewer.

At this point in the development, expect this repository to change rapidly and without prior notification.


HOW TO BUILD

Windows: Open Visual Studio command prompt of your choice (developed using VS2017 x64), run "vs-build.bat debug". For release builds omit the debug parameter.
Linux: BEdit is tested with GCC on Linux. Run "./gcc-build debug" for a debug build. For release builds omit the debug parameter.


HOW TO RUN

When built successfully there should be bedit.exe (for Windows) or bedit (for Linux) inside the vs_build or gcc_build folder. Run it from command line with parameters -layout <your-layout-file> -data <your-binary-file>, it will print out formatted data to the standard output streams.

Try "<build-folder>/bedit -layout examples\bmp.bet -data examples\test.bmp" to test the structured art bitmap file that is in the repo.


LAYOUT FILE

The viewer takes two parameter, one is the data file you want to view and the other is the layout file. In the layout file you specify the expected structure of the binary data in a C-like language (the "layout language"). Unlike C source code the layout file does not get compiled by a compiler, but translated to commands interpreted by the executable. In other words, you don't need to recompile the tool every time you want to add a new layout file.

The layout language is being developed and don't expect full backward compatibility any time soon.

There are some additional tutorials on the BEdit page available on https://bedit.handmade.network, as well as forums and other nice projects you might find interesting.

For a complete and up-to-date documentation of the layout language, head to the public github https://github.com/Kipt/BEdit/blob/main/LayoutLanguage.md and have a look through the examples.

If you like BEdit, also consider visiting https://kipt.itch.io/bedit to purchase the GUI version, available on Windows 7 and later, and Linux (tested on Ubuntu 18).


LICENSE
Public Domain (www.unlicense.org)
This is free and unencumbered software released into the public domain.
Anyone is free to copy, modify, publish, use, compile, sell, or distribute this
software, either in source code form or as a compiled binary, for any purpose,
commercial or non-commercial, and by any means.
In jurisdictions that recognize copyright laws, the author or authors of this
software dedicate any and all copyright interest in the software to the public
domain. We make this dedication for the benefit of the public at large and to
the detriment of our heirs and successors. We intend this dedication to be an
overt act of relinquishment in perpetuity of all present and future rights to
this software under copyright law.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
